package com.sandrios.sandriosCamera.internal.imagevideoeditor.videoEditor;

/**
 * @author Varun on 01/07/15.
 */
public interface VideoProcessingCallback {

    void onVideoProcessingSuccess(String filePath);

    void onVideoProcessingFailure(String errorMessage);

    void onVideoProcessingProgress(String progressMessage);

    void onVideoProcessingStart();

    void onVideoProcessingFinish();
}
