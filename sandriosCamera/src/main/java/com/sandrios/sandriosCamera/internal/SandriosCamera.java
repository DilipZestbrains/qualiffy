package com.sandrios.sandriosCamera.internal;

import android.app.Activity;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.sandrios.sandriosCamera.internal.ui.camera.Camera1Activity;
import com.sandrios.sandriosCamera.internal.ui.camera2.Camera2Activity;
import com.sandrios.sandriosCamera.internal.utils.CameraHelper;

public class SandriosCamera {

    public static final int REQUEST_CODE_CAMERA_IMG = 201;
    public static String MEDIA = "media";
    private static SandriosCamera mInstance = null;
    private int mediaAction = CameraConfiguration.MEDIA_ACTION_BOTH;
    private boolean showPicker = true;
    public static boolean cropperSquare = false;


    public static boolean showOverlay = false;

    private boolean autoRecord = false;
    private boolean enableImageCrop = false;
    private long videoSize = -1;

    public static SandriosCamera with() {
        if (mInstance == null) {
            mInstance = new SandriosCamera();
        }
        return mInstance;
    }

    public SandriosCamera setShowPicker(boolean showPicker) {
        this.showPicker = showPicker;
        return mInstance;
    }

    public SandriosCamera setMediaAction(int mediaAction) {
        this.mediaAction = mediaAction;
        return mInstance;
    }



 public SandriosCamera setCropperSquare(boolean cropperSquare) {
        this.cropperSquare=cropperSquare;
        return mInstance;
    }

     public SandriosCamera showOverlay(boolean showOverlay) {
        this.showOverlay=showOverlay;
        return mInstance;
    }



    public SandriosCamera enableImageCropping(boolean enableImageCrop) {
        this.enableImageCrop = enableImageCrop;
        return mInstance;
    }

    @SuppressWarnings("SameParameterValue")
    public SandriosCamera setVideoFileSize(int fileSize) {
        this.videoSize = fileSize;
        return mInstance;
    }


    /**
     * Only works if Media Action is set to Video
     */
    public SandriosCamera setAutoRecord() {
        autoRecord = true;
        return mInstance;
    }

    public void launchCamera(Activity activity) {
        if (CameraHelper.hasCamera(activity)) {
            Intent cameraIntent;
            if (CameraHelper.hasCamera2(activity)) {
                cameraIntent = new Intent(activity, Camera2Activity.class);
            } else {
                cameraIntent = new Intent(activity, Camera1Activity.class);
            }
            cameraIntent.putExtra(CameraConfiguration.Arguments.SHOW_PICKER, showPicker);
            cameraIntent.putExtra(CameraConfiguration.Arguments.MEDIA_ACTION, mediaAction);
            cameraIntent.putExtra(CameraConfiguration.Arguments.ENABLE_CROP, enableImageCrop);
            cameraIntent.putExtra(CameraConfiguration.Arguments.AUTO_RECORD, autoRecord);

           /* if (videoSize > 0) {
                cameraIntent.putExtra(CameraConfiguration.Arguments.VIDEO_FILE_SIZE, videoSize * 1024 * 1024);
            }*/
            activity.startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_IMG);
        }
    }

 public void launchCamera(Fragment fragment) {
        if (CameraHelper.hasCamera(fragment.getContext())) {
            Intent cameraIntent;
            if (CameraHelper.hasCamera2(fragment.getContext())) {
                cameraIntent = new Intent(fragment.getContext(), Camera2Activity.class);
            } else {
                cameraIntent = new Intent(fragment.getContext(), Camera1Activity.class);
            }
            cameraIntent.putExtra(CameraConfiguration.Arguments.SHOW_PICKER, showPicker);
            cameraIntent.putExtra(CameraConfiguration.Arguments.MEDIA_ACTION, mediaAction);
            cameraIntent.putExtra(CameraConfiguration.Arguments.ENABLE_CROP, enableImageCrop);
            cameraIntent.putExtra(CameraConfiguration.Arguments.AUTO_RECORD, autoRecord);

           /* if (videoSize > 0) {
                cameraIntent.putExtra(CameraConfiguration.Arguments.VIDEO_FILE_SIZE, videoSize * 1024 * 1024);
            }*/
            fragment.startActivityForResult(cameraIntent, REQUEST_CODE_CAMERA_IMG);
        }
    }




    public interface IGetMedia{
      void   getMediaFile(String media_path);
    }


    public class MediaType {
        public static final int PHOTO = 0;
        public static final int VIDEO = 1;
    }
}
