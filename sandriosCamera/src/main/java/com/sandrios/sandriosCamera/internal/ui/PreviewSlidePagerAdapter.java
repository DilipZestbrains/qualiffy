package com.sandrios.sandriosCamera.internal.ui;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.List;

public  class PreviewSlidePagerAdapter extends FragmentStatePagerAdapter {
    private List<Fragment> mFragments;

    public PreviewSlidePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        if (mFragments == null) {
            return (null);
        }
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
