package com.sandrios.sandriosCamera.internal.manager.listener;

public interface RecordButtonListener {
     void onTakePhotoButtonPressed();

    void onStartRecordingButtonPressed();

    void onStopRecordingButtonPressed();



}
