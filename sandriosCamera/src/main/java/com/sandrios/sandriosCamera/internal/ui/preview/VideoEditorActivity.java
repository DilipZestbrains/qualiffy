package com.sandrios.sandriosCamera.internal.ui.preview;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.ahmedadeltito.photoeditorsdk.BrushDrawingView;
import com.ahmedadeltito.photoeditorsdk.OnPhotoEditorSDKListener;
import com.ahmedadeltito.photoeditorsdk.PhotoEditorSDK;
import com.ahmedadeltito.photoeditorsdk.ViewType;

import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.sandrios.sandriosCamera.R;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.ColorPickerAdapter;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.EmojiFragment;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.ImageFragment;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.PageIndicator;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.videoEditor.InternVideoEditor;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.videoEditor.VideoProcessingCallback;
import com.sandrios.sandriosCamera.internal.imagevideoeditor.widget.SlidingUpPanelLayout;
import com.sandrios.sandriosCamera.internal.videoTrimmer.interfaces.OnProgressVideoListener;
import com.sandrios.sandriosCamera.internal.videoTrimmer.interfaces.OnRangeSeekBarListener;
import com.sandrios.sandriosCamera.internal.videoTrimmer.view.ProgressBarView;
import com.sandrios.sandriosCamera.internal.videoTrimmer.view.RangeSeekBarView;
import com.sandrios.sandriosCamera.internal.videoTrimmer.view.Thumb;
import com.sandrios.sandriosCamera.internal.videoTrimmer.view.TimeLineView;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity.MEDIA_PATH;

public class VideoEditorActivity extends AppCompatActivity implements View.OnClickListener, OnPhotoEditorSDKListener, VideoProcessingCallback {

    private static final int TO_FILTER = 1230;
    private static final int SHOW_PROGRESS = 2;
    private final String TAG = "VideoEditorView";
    private final MessageHandler mMessageHandler = new MessageHandler(this);
    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    String imageName = "IMG_" + timeStamp + ".jpg";
    private RelativeLayout parentImageRelativeLayout;
    private RecyclerView drawingViewColorPickerRecyclerView;
    private ImageView undoTextView;
    private SlidingUpPanelLayout mLayout;
    private Typeface emojiFont;
    private View topShadow;
    private RelativeLayout topShadowRelativeLayout;
    private View bottomShadow;
    private RelativeLayout bottomShadowRelativeLayout;
    private ArrayList<Integer> colorPickerColors;
    private int colorCodeTextView = -1;
    private PhotoEditorSDK photoEditorSDK;
    private TextView undoTextTextView;
    private TextView doneDrawingTextView;
    private TextView eraseDrawingTextView;
    private ImageView photoEditImageView;
    private String selectedImagePath = "";
    private VideoView videoEditVideoView;
    private FFmpeg ffmpeg;
    private String filePath;
    private String videoPath;
    private ProgressDialog progressDialog;
    private InternVideoEditor videoEditor;
    private SeekBar mHolderTopView;
    private SeekBar previewHandlerTop;
    private RangeSeekBarView mRangeSeekBarView;
    private ProgressBarView mVideoProgressIndicator;
    private TimeLineView mTimeLineView;
    private TimeLineView previewTimeLine;


    private int mDuration = 0;
    private int mTimeVideo = 0;
    private int mStartPosition = 0;
    private int mEndPosition = 0;
    private int mMaxDuration;
    private List<OnProgressVideoListener> mListeners;
    private RelativeLayout video_rlayout;
    private Uri mSrc;
    private boolean isOpenToTrim = true;
    private RelativeLayout seekbarRlayout;
    private RelativeLayout videoPreviewlayout;
    private long mOriginSizeFile;
    private LinearLayout trimmerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_editor_layout);

        videoEditVideoView = findViewById(R.id.video_edit_vv);
        mHolderTopView = ((SeekBar) findViewById(R.id.handlerTop));
        previewHandlerTop = ((SeekBar) findViewById(R.id.previewHandlerTop));
        mVideoProgressIndicator = ((ProgressBarView) findViewById(R.id.timeVideoView));
        mRangeSeekBarView = ((RangeSeekBarView) findViewById(R.id.timeLineBar));
        mTimeLineView = ((TimeLineView) findViewById(R.id.timeLineView));
        previewTimeLine = ((TimeLineView) findViewById(R.id.previewTimeLine));
        video_rlayout = ((RelativeLayout) findViewById(R.id.video_rlayout));
        video_rlayout = ((RelativeLayout) findViewById(R.id.video_rlayout));
        seekbarRlayout = ((RelativeLayout) findViewById(R.id.seekbarRlayout));
        videoPreviewlayout = ((RelativeLayout) findViewById(R.id.videoPreviewlayout));
        trimmerLayout = ((LinearLayout) findViewById(R.id.trimmerLayout));


        BrushDrawingView brushDrawingView = (BrushDrawingView) findViewById(R.id.drawing_view);
        drawingViewColorPickerRecyclerView = (RecyclerView) findViewById(R.id.drawing_view_color_picker_recycler_view);
        parentImageRelativeLayout = (RelativeLayout) findViewById(R.id.parent_image_rl);
        ImageView closeTextView = (ImageView) findViewById(R.id.close_tv);
        ImageView addTextView = (ImageView) findViewById(R.id.add_text_tv);
        ImageView addPencil = (ImageView) findViewById(R.id.add_pencil_tv);
        RelativeLayout deleteRelativeLayout = (RelativeLayout) findViewById(R.id.delete_rl);
        ImageView addImageEmojiTextView = (ImageView) findViewById(R.id.add_image_emoji_tv);
        ImageView saveTextView = (ImageView) findViewById(R.id.save_tv);
        TextView saveTextTextView = (TextView) findViewById(R.id.save_text_tv);
        undoTextView = (ImageView) findViewById(R.id.undo_tv);
        undoTextTextView = (TextView) findViewById(R.id.undo_text_tv);
        doneDrawingTextView = (TextView) findViewById(R.id.done_drawing_tv);
        eraseDrawingTextView = (TextView) findViewById(R.id.erase_drawing_tv);
        ImageView clearAllTextView = (ImageView) findViewById(R.id.clear_all_tv);
        TextView clearAllTextTextView = (TextView) findViewById(R.id.clear_all_text_tv);
        ImageView goToNextTextView = (ImageView) findViewById(R.id.go_to_next_screen_tv);
        photoEditImageView = (ImageView) findViewById(R.id.photo_edit_iv);
        mLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        topShadow = findViewById(R.id.top_shadow);
        topShadowRelativeLayout = (RelativeLayout) findViewById(R.id.top_parent_rl);
        bottomShadow = findViewById(R.id.bottom_shadow);
        bottomShadowRelativeLayout = (RelativeLayout) findViewById(R.id.bottom_parent_rl);

        ViewPager pager = (ViewPager) findViewById(R.id.image_emoji_view_pager);
        PageIndicator indicator = (PageIndicator) findViewById(R.id.image_emoji_indicator);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                if (getIntent().getExtras().getString(MEDIA_PATH) != null) {
                    videoPath = getIntent().getExtras().getString(MEDIA_PATH);
                    photoEditImageView.setVisibility(INVISIBLE);
                    selectedImagePath = getURLForResource(R.drawable.image_req);
                    brushDrawingView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    brushDrawingView.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                    photoEditImageView.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
                    photoEditImageView.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
                    videoEditor = new InternVideoEditor(this);
                    loadVideoInit();
                }
            }
        }


        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options);

        emojiFont = Typeface.createFromAsset(getAssets(), "emojione-android.ttf");

//        photoEditImageView.getCropImageView().setImageBitmap(bitmap);
        final List<Fragment> fragmentsList = new ArrayList<>();
        fragmentsList.add(new ImageFragment());
        fragmentsList.add(new EmojiFragment());

        PreviewSlidePagerAdapter adapter = new PreviewSlidePagerAdapter(getSupportFragmentManager(), fragmentsList);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(5);
        indicator.setViewPager(pager);

        photoEditorSDK = new PhotoEditorSDK.PhotoEditorSDKBuilder(VideoEditorActivity.this)
                .parentView(parentImageRelativeLayout) // add parent image view
                .childView(photoEditImageView) // add the desired image view
                .deleteView(deleteRelativeLayout) // add the deleted view that will appear during the movement of the views
                .brushDrawingView(brushDrawingView) // add the brush drawing view that is responsible for drawing on the image view
                .buildPhotoEditorSDK(); // build photo editor sdk
        photoEditorSDK.setOnPhotoEditorSDKListener(this);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0)
                    mLayout.setScrollableView(((ImageFragment) fragmentsList.get(position)).imageRecyclerView);
                else if (position == 1)
                    mLayout.setScrollableView(((EmojiFragment) fragmentsList.get(position)).emojiRecyclerView);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        closeTextView.setOnClickListener(this);
        addImageEmojiTextView.setOnClickListener(this);
        addTextView.setOnClickListener(this);
        addPencil.setOnClickListener(this);
        saveTextView.setOnClickListener(this);
        saveTextTextView.setOnClickListener(this);
        undoTextView.setOnClickListener(this);
        undoTextTextView.setOnClickListener(this);
        doneDrawingTextView.setOnClickListener(this);
        eraseDrawingTextView.setOnClickListener(this);
        videoPreviewlayout.setOnClickListener(this);
        videoEditVideoView.setOnClickListener(this);
        clearAllTextView.setOnClickListener(this);
        clearAllTextTextView.setOnClickListener(this);
        goToNextTextView.setOnClickListener(this);

        colorPickerColors = new ArrayList<>();
        colorPickerColors.add(getResources().getColor(R.color.white));
        colorPickerColors.add(getResources().getColor(R.color.black));
        colorPickerColors.add(getResources().getColor(R.color.blue_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.brown_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.green_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.orange_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.red_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.red_orange_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.sky_blue_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.violet_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.yellow_color_picker));
        colorPickerColors.add(getResources().getColor(R.color.yellow_green_color_picker));

        new CountDownTimer(500, 100) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                mLayout.setScrollableView(((ImageFragment) fragmentsList.get(0)).imageRecyclerView);
            }

        }.start();

        setVideoURI(Uri.parse(videoPath));
        setUpListeners();
        setUpMargins();

    }

    /*********************** Trimmer work ********************/

    public void setVideoURI(final Uri videoURI) {
        mSrc = videoURI;

        if (mOriginSizeFile == 0) {
            File file = new File(mSrc.getPath());

            mOriginSizeFile = file.length();
            long fileSizeInKB = mOriginSizeFile / 1024;
        }

        videoEditVideoView.setVideoURI(mSrc);
        videoEditVideoView.requestFocus();

        mTimeLineView.setVideo(mSrc);
        previewTimeLine.setVideo(mSrc);

        videoPreviewlayout.setVisibility(VISIBLE);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                previewHandlerTop.setVisibility(VISIBLE);
            }
        }, 3000);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void setUpMargins() {
        int marge = mRangeSeekBarView.getThumbs().get(0).getWidthBitmap();
        int widthSeek = mHolderTopView.getThumb().getMinimumWidth() / 2;

        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mHolderTopView.getLayoutParams();
        lp.setMargins(marge - widthSeek, 0, marge - widthSeek, 0);
        mHolderTopView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mTimeLineView.getLayoutParams();
        lp.setMargins(marge, 20, marge, 0);
        mTimeLineView.setLayoutParams(lp);

        lp = (RelativeLayout.LayoutParams) mVideoProgressIndicator.getLayoutParams();
        lp.setMargins(marge, 20, marge, 0);
        mVideoProgressIndicator.setLayoutParams(lp);
    }

    private void updateVideoProgress(int time) {
        if (videoEditVideoView == null) {
            return;
        }

        if (time >= mEndPosition) {
            videoEditVideoView.pause();
            videoEditVideoView.seekTo(mStartPosition);
            videoEditVideoView.start();
            return;
        }


        if (mHolderTopView != null) {
            setProgressBarPosition(time);
        }
    }

    private void setUpListeners() {
        setMediaDuration();

        mListeners = new ArrayList<>();
        mListeners.add(new OnProgressVideoListener() {
            @Override
            public void updateProgress(int time, int max, float scale) {
                updateVideoProgress(time);
            }
        });

        mListeners.add(mVideoProgressIndicator);
        videoEditVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int what, int extra) {
                return false;
            }
        });


        mRangeSeekBarView.addOnRangeSeekBarListener(mVideoProgressIndicator);
        mRangeSeekBarView.addOnRangeSeekBarListener(new OnRangeSeekBarListener() {
            @Override
            public void onCreate(RangeSeekBarView rangeSeekBarView, int index, float value) {

            }

            @Override
            public void onSeek(RangeSeekBarView rangeSeekBarView, int index, float value) {
                onSeekThumbs(index, value);
            }

            @Override
            public void onSeekStart(RangeSeekBarView rangeSeekBarView, int index, float value) {

            }

            @Override
            public void onSeekStop(RangeSeekBarView rangeSeekBarView, int index, float value) {
            }
        });

        mHolderTopView.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStart();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                onPlayerIndicatorSeekStop(seekBar);
            }
        });


        videoEditVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                onVideoPrepared(mp);
            }
        });

        videoEditVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                onVideoCompleted();
            }
        });
    }

    private void onPlayerIndicatorSeekStart() {
        videoEditVideoView.pause();
//        notifyProgressUpdate(false);

    }

    private void onPlayerIndicatorSeekStop(@NonNull SeekBar seekBar) {

        int duration = (int) ((mDuration * seekBar.getProgress()) / 1000L);
        videoEditVideoView.seekTo(duration);
//        notifyProgressUpdate(false);


        if (videoEditVideoView.isPlaying())
            videoEditVideoView.pause();
        videoEditVideoView.start();
    }

    private void onVideoCompleted() {
        videoEditVideoView.seekTo(mStartPosition);
        videoEditVideoView.start();
        mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);

    }


    private void onSeekThumbs(int index, float value) {
        switch (index) {
            case Thumb.LEFT: {
                mStartPosition = (int) ((mDuration * value) / 100L);
                videoEditVideoView.seekTo(mStartPosition);
                break;
            }
            case Thumb.RIGHT: {
                mEndPosition = (int) ((mDuration * value) / 100L);
                break;
            }
        }
        setProgressBarPosition(mStartPosition);
        mTimeVideo = mEndPosition - mStartPosition;
    }

    private void setProgressBarPosition(int position) {
        if (mDuration > 0) {
            long pos = 1000L * position / mDuration;
            mHolderTopView.setProgress((int) pos);
            previewHandlerTop.setProgress((int) pos);

            if (!videoEditVideoView.isPlaying())
                videoEditVideoView.start();

        }
    }

    private void notifyProgressUpdate(boolean all) {
        if (mDuration == 0) return;

        int position = videoEditVideoView.getCurrentPosition();
        if (all) {
            for (OnProgressVideoListener item : mListeners) {
                item.updateProgress(position, mDuration, ((position * 100) / mDuration));
            }
        } else {
            mListeners.get(1).updateProgress(position, mDuration, ((position * 100) / mDuration));
        }
    }

    private void onVideoPrepared(@NonNull MediaPlayer mp) {
        // Adjust the size of the video
        // so it fits on the screen
        int videoWidth = mp.getVideoWidth();
        int videoHeight = mp.getVideoHeight();
        float videoProportion = (float) videoWidth / (float) videoHeight;
        int screenWidth = video_rlayout.getWidth();
        int screenHeight = video_rlayout.getHeight();
        float screenProportion = (float) screenWidth / (float) screenHeight;
        ViewGroup.LayoutParams lp = videoEditVideoView.getLayoutParams();

        if (videoProportion > screenProportion) {
            lp.width = screenWidth;
            lp.height = (int) ((float) screenWidth / videoProportion);
        } else {
            lp.width = (int) (videoProportion * (float) screenHeight);
            lp.height = screenHeight;
        }
        videoEditVideoView.setLayoutParams(lp);

        mDuration = videoEditVideoView.getDuration();
        mMessageHandler.sendEmptyMessage(SHOW_PROGRESS);

        setSeekBarPosition();

    }

    private void setSeekBarPosition() {

        if (mDuration >= mMaxDuration) {
            mStartPosition = mDuration / 2 - mMaxDuration / 2;
            mEndPosition = mDuration / 2 + mMaxDuration / 2;

            mRangeSeekBarView.setThumbValue(0, (mStartPosition * 100) / mDuration);
            mRangeSeekBarView.setThumbValue(1, (mEndPosition * 100) / mDuration);

        } else {
            mStartPosition = 0;
            mEndPosition = mDuration;
        }

        setProgressBarPosition(mStartPosition);
        videoEditVideoView.seekTo(mStartPosition);

        mTimeVideo = mDuration;
        mRangeSeekBarView.initMaxWidth();
    }

    private void setMediaDuration() {
        MediaPlayer mp = MediaPlayer.create(this, Uri.parse(videoPath));
        if (mp != null)
            mMaxDuration = mp.getDuration();
    }


    private void setLayoutWidth() {

        if (isOpenToTrim) {
            seekbarRlayout.setVisibility(VISIBLE);
            videoPreviewlayout.setVisibility(INVISIBLE);
            isOpenToTrim = false;
        } else {
            seekbarRlayout.setVisibility(INVISIBLE);
            videoPreviewlayout.setVisibility(VISIBLE);
            isOpenToTrim = true;
        }
    }


    /************************ End Video Trimmer ********************/


    private boolean stringIsNotEmpty(String string) {
        if (string != null && !string.equals("null")) {
            if (!string.trim().equals("")) {
                return true;
            }
        }
        return false;
    }


    public void addEmoji(String emojiName) {
        photoEditorSDK.addEmoji(emojiName, emojiFont);
        if (mLayout != null)
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    public void addImage(Bitmap image) {
        photoEditorSDK.addImage(image);
        if (mLayout != null)
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
    }

    private void addText(String text, int colorCodeTextView) {
        photoEditorSDK.addText(text, colorCodeTextView);
    }

    private void clearAllViews() {
        photoEditorSDK.clearAllViews();
    }

    private void undoViews() {
        photoEditorSDK.viewUndo();
    }

    private void eraseDrawing() {
        photoEditorSDK.brushEraser();
    }

    private void openAddTextPopupWindow(String text, int colorCode) {
        colorCodeTextView = colorCode;
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View addTextPopupWindowRootView = inflater.inflate(R.layout.add_text_popup_window, null);
        final EditText addTextEditText = (EditText) addTextPopupWindowRootView.findViewById(R.id.add_text_edit_text);
        TextView addTextDoneTextView = (TextView) addTextPopupWindowRootView.findViewById(R.id.add_text_done_tv);
        RecyclerView addTextColorPickerRecyclerView = (RecyclerView) addTextPopupWindowRootView.findViewById(R.id.add_text_color_picker_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(VideoEditorActivity.this, LinearLayoutManager.HORIZONTAL, false);
        addTextColorPickerRecyclerView.setLayoutManager(layoutManager);
        addTextColorPickerRecyclerView.setHasFixedSize(true);
        ColorPickerAdapter colorPickerAdapter = new ColorPickerAdapter(VideoEditorActivity.this, colorPickerColors);
        colorPickerAdapter.setOnColorPickerClickListener(new ColorPickerAdapter.OnColorPickerClickListener() {
            @Override
            public void onColorPickerClickListener(int colorCode) {
                addTextEditText.setTextColor(colorCode);
                colorCodeTextView = colorCode;
            }
        });
        addTextColorPickerRecyclerView.setAdapter(colorPickerAdapter);
        if (stringIsNotEmpty(text)) {
            addTextEditText.setText(text);
            addTextEditText.setTextColor(colorCode == -1 ? getResources().getColor(R.color.white) : colorCode);
        }
        final PopupWindow pop = new PopupWindow(VideoEditorActivity.this);
        pop.setContentView(addTextPopupWindowRootView);
        pop.setWidth(LinearLayout.LayoutParams.MATCH_PARENT);
        pop.setHeight(LinearLayout.LayoutParams.MATCH_PARENT);
        pop.setFocusable(true);
        pop.setBackgroundDrawable(null);
        pop.showAtLocation(addTextPopupWindowRootView, Gravity.TOP, 0, 0);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        addTextDoneTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addText(addTextEditText.getText().toString(), colorCodeTextView);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                trimmerLayout.setVisibility(VISIBLE);
                pop.dismiss();

            }
        });
    }

    private void updateView(int visibility) {
        topShadow.setVisibility(visibility);
        topShadowRelativeLayout.setVisibility(visibility);
        bottomShadow.setVisibility(visibility);
        bottomShadowRelativeLayout.setVisibility(visibility);
        trimmerLayout.setVisibility(visibility);

    }



    private void updateBrushDrawingView(boolean brushDrawingMode) {
        photoEditorSDK.setBrushDrawingMode(brushDrawingMode);
        if (brushDrawingMode) {
            updateView(View.GONE);
            drawingViewColorPickerRecyclerView.setVisibility(VISIBLE);
            doneDrawingTextView.setVisibility(VISIBLE);
            eraseDrawingTextView.setVisibility(VISIBLE);
            LinearLayoutManager layoutManager = new LinearLayoutManager(VideoEditorActivity.this, LinearLayoutManager.HORIZONTAL, false);
            drawingViewColorPickerRecyclerView.setLayoutManager(layoutManager);
            drawingViewColorPickerRecyclerView.setHasFixedSize(true);
            ColorPickerAdapter colorPickerAdapter = new ColorPickerAdapter(VideoEditorActivity.this, colorPickerColors);
            colorPickerAdapter.setOnColorPickerClickListener(new ColorPickerAdapter.OnColorPickerClickListener() {
                @Override
                public void onColorPickerClickListener(int colorCode) {
                    photoEditorSDK.setBrushColor(colorCode);
                }
            });
            drawingViewColorPickerRecyclerView.setAdapter(colorPickerAdapter);
        } else {
            updateView(VISIBLE);
            drawingViewColorPickerRecyclerView.setVisibility(View.GONE);
            doneDrawingTextView.setVisibility(View.GONE);
            eraseDrawingTextView.setVisibility(View.GONE);
        }
//        saveAndSendImagePath(getPathForImage(), true);
    }

    private void returnBackWithSavedImage() {
        updateView(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        String imgPath = photoEditorSDK.saveImage("PhotoClicked", imageName);
        parentImageRelativeLayout.setLayoutParams(layoutParams);

        videoEditor.drawTextCommandCommand(VideoEditorActivity.this, videoPath, imgPath, progressDialog);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.close_tv) {
            onBackPressed();
        } else if (v.getId() == R.id.add_image_emoji_tv) {
            mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);

        } else if (v.getId() == R.id.add_text_tv) {
            trimmerLayout.setVisibility(View.GONE);
            openAddTextPopupWindow("", -1);
        } else if (v.getId() == R.id.add_pencil_tv) {
            updateBrushDrawingView(true);

        } else if (v.getId() == R.id.done_drawing_tv) {
            updateBrushDrawingView(false);

        } else if (v.getId() == R.id.save_tv || v.getId() == R.id.save_text_tv) {
            Toast.makeText(this, "Video Saved Successfully", Toast.LENGTH_SHORT).show();
            returnBackWithSavedImage();

        } else if (v.getId() == R.id.go_to_next_screen_tv) {
            returnBackWithSavedImage();

        } else if (v.getId() == R.id.clear_all_tv || v.getId() == R.id.clear_all_text_tv) {
            clearAllViews();

        } else if (v.getId() == R.id.undo_text_tv || v.getId() == R.id.undo_tv) {
            undoViews();

        } else if (v.getId() == R.id.erase_drawing_tv) {
            eraseDrawing();

        } else if (v.getId() == R.id.videoPreviewlayout || v.getId() == R.id.video_edit_vv) {
            setLayoutWidth();

        }
    }

    @Override
    public void onEditTextChangeListener(String text, int colorCode) {
        openAddTextPopupWindow(text, colorCode);

    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {
        if (numberOfAddedViews > 0) {
            undoTextView.setVisibility(VISIBLE);
            undoTextTextView.setVisibility(VISIBLE);
        }
        switch (viewType) {
            case BRUSH_DRAWING:
                Log.i("BRUSH_DRAWING", "onAddViewListener");
                break;
            case EMOJI:
                Log.i("EMOJI", "onAddViewListener");
                break;
            case IMAGE:
                Log.i("IMAGE", "onAddViewListener");
                break;
            case TEXT:
                Log.i("TEXT", "onAddViewListener");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == TO_FILTER) {
                if (data != null) {
                    String path = data.getStringExtra("imagePathKey");
                    saveAndSendImagePath(path, false);
                }
            }
        }
    }

    private String saveAndSendImagePath(String path, boolean isSave) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
//        photoEditImageView.getCropImageView().setImageBitmap(BitmapFactory.decodeFile(path, options));
        if (isSave)
            return photoEditorSDK.saveImage("PhotoClicked", imageName);
        else return "";
    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {
        Log.i(TAG, "onRemoveViewListener");
        if (numberOfAddedViews == 0) {
            undoTextView.setVisibility(View.GONE);
            undoTextTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {
        switch (viewType) {
            case BRUSH_DRAWING:
                Log.i("BRUSH_DRAWING", "onStartViewChangeListener");
                break;
            case EMOJI:
                Log.i("EMOJI", "onStartViewChangeListener");
                break;
            case IMAGE:
                Log.i("IMAGE", "onStartViewChangeListener");
                break;
            case TEXT:
                Log.i("TEXT", "onStartViewChangeListener");
                break;
        }
    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {
        switch (viewType) {
            case BRUSH_DRAWING:
                Log.i("BRUSH_DRAWING", "onStopViewChangeListener");
                break;
            case EMOJI:
                Log.i("EMOJI", "onStopViewChangeListener");
                break;
            case IMAGE:
                Log.i("IMAGE", "onStopViewChangeListener");
                break;
            case TEXT:
                Log.i("TEXT", "onStopViewChangeListener");
                break;
        }
    }

    private class PreviewSlidePagerAdapter extends FragmentStatePagerAdapter {
        private List<Fragment> mFragments;

        PreviewSlidePagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            mFragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            if (mFragments == null) {
                return (null);
            }
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    private void loadVideoInit() {
        videoEditor.loadFFMpegBinary(this);
//        selectedImagePath = getIntent().getExtras().getString("selectedImagePath");

        videoEditVideoView = (VideoView) findViewById(R.id.video_edit_vv);
        videoEditVideoView.setVideoURI(Uri.parse(videoPath));



    /*    videoEditVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });*/
        videoEditVideoView.start();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(null);
        progressDialog.setCancelable(false);
    }

    @Override
    public void onVideoProcessingSuccess(String filePath) {
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        photoEditorSDK.deleteFile("PhotoClicked", imageName);

        Intent returnIntent = new Intent();
        returnIntent.putExtra(MEDIA_PATH, filePath);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();

    }

    @Override
    public void onVideoProcessingFailure(String errorMessage) {
        progressDialog.dismiss();
    }

    @Override
    public void onVideoProcessingProgress(String progressMessage) {
        progressDialog.setMessage("Please wait...");
    }

    @Override
    public void onVideoProcessingStart() {
        progressDialog.setMessage("Processing...");
        progressDialog.show();
    }

    @Override
    public void onVideoProcessingFinish() {
        progressDialog.dismiss();
    }

    private static class MessageHandler extends Handler {

        @NonNull
        private final WeakReference<VideoEditorActivity> mView;

        MessageHandler(VideoEditorActivity view) {
            mView = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(Message msg) {
            VideoEditorActivity view = mView.get();
            if (view == null || view.videoEditVideoView == null) {
                return;
            }

            view.notifyProgressUpdate(true);
            if (view.videoEditVideoView.isPlaying()) {
                sendEmptyMessageDelayed(0, 10);
            }
        }
    }


    private String getURLForResource(int resourceId) {
        return Uri.parse("android.resource://" + R.class.getPackage().getName() + "/" + resourceId).toString();
    }

}

