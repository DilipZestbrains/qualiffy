package com.sandrios.sandriosCamera.internal.imagevideoeditor;

public class EditorType {
    public static final int VIDEO_EDITOR = 100;
    public static final int PHOTO_EDITOR = 101;
}
