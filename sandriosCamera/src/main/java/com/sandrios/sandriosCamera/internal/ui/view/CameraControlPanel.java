package com.sandrios.sandriosCamera.internal.ui.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ThumbnailUtils;
import android.os.CountDownTimer;
import android.os.FileObserver;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.iammert.library.cameravideobuttonlib.CameraVideoButton;
import com.sandrios.sandriosCamera.R;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.sandrios.sandriosCamera.internal.manager.impl.Camera1Manager;
import com.sandrios.sandriosCamera.internal.manager.listener.RecordButtonListener;
import com.sandrios.sandriosCamera.internal.ui.model.Media;
import com.sandrios.sandriosCamera.internal.utils.DateTimeUtils;
import com.sandrios.sandriosCamera.internal.utils.RecyclerItemClickListener;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Arpit Gandhi
 */
public class CameraControlPanel extends RelativeLayout
        implements RecordButtonListener,
        MediaActionSwitchView.OnMediaActionStateChangeListener, CameraVideoButton.ActionListener {

    @MediaActionSwitchView.MediaActionState
    int mediaActionState;
    private Context context;
    private CameraSwitchView cameraSwitchView;
    private CameraVideoButton recordButton;
    //    private MediaActionSwitchView mediaActionSwitchView;
    private FlashSwitchView flashSwitchView;
    private TextView recordDurationText;
    private TextView recordSizeText;
    private ImageButton settingsButton;
    private RecyclerView slidingGalleryList;
    private RecordButtonListener recordButtonListener;
    private MediaActionSwitchView.OnMediaActionStateChangeListener onMediaActionStateChangeListener;
    private CameraSwitchView.OnCameraTypeChangeListener onCameraTypeChangeListener;
    private FlashSwitchView.FlashModeSwitchListener flashModeSwitchListener;
    private SettingsClickListener settingsClickListener;
    private OverlayClickListener overlayClickListener;
    private RecyclerItemClickListener.OnClickListener pickerItemClickListener;
    private CountDownTimer countDownTimer;
    private long maxVideoFileSize = 0;
    private String mediaFilePath;
    private boolean hasFlash = false;
    private int mediaAction;
    private boolean showImageCrop = false;
    private FileObserver fileObserver;
    private View cameraOverlay;
    private RelativeLayout topRlayout;
    private LinearLayout swipingLlayout;
    private LinearLayout videoThumbnailLayout;
    private ImageView ivCross;
    private View ivCamera;


    public CameraControlPanel(Context context) {
        this(context, null);
    }

    public CameraControlPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        hasFlash = context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        LayoutInflater.from(context).inflate(R.layout.camera_control_panel_layout, this);
        setBackgroundColor(Color.TRANSPARENT);
        settingsButton = findViewById(R.id.settings_view);
        cameraSwitchView = findViewById(R.id.front_back_camera_switcher);
//        mediaActionSwitchView = findViewById(R.id.photo_video_camera_switcher);
        recordButton = findViewById(R.id.record_button);
        flashSwitchView = findViewById(R.id.flash_switch_view);
        recordDurationText = findViewById(R.id.record_duration_text);
        recordSizeText = findViewById(R.id.record_size_mb_text);
        slidingGalleryList = findViewById(R.id.horizontal_gallery_list);
        topRlayout = findViewById(R.id.topRlayout);
        cameraOverlay = findViewById(R.id.cameraOverlay);
        swipingLlayout = findViewById(R.id.swipingLlayout);
        videoThumbnailLayout = findViewById(R.id.videoThumbnailLayout);
        ivCross = findViewById(R.id.ivCross);
        ivCamera = findViewById(R.id.ivCamera);


        slidingGalleryList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        cameraSwitchView.setOnCameraTypeChangeListener(onCameraTypeChangeListener);
//        mediaActionSwitchView.setOnMediaActionStateChangeListener(this);

        setOnCameraTypeChangeListener(onCameraTypeChangeListener);
        setOnMediaActionStateChangeListener(onMediaActionStateChangeListener);
        setFlashModeSwitchListener(flashModeSwitchListener);
        setRecordButtonListener(recordButtonListener);

        settingsButton.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_settings_white_24dp));
        settingsButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (settingsClickListener != null) settingsClickListener.onSettingsClick();
            }
        });


        swipingLlayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(overlayClickListener!=null)
                overlayClickListener.keepSwapping();
            }
        });



        ivCross.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraOverlay.setVisibility(View.GONE);
                if(overlayClickListener!=null)
                    overlayClickListener.cancelOverlay();
            }
        });
        ivCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraOverlay.setVisibility(View.GONE);
            }
        });

        if (hasFlash)
            flashSwitchView.setVisibility(VISIBLE);
        else flashSwitchView.setVisibility(GONE);

        if (SandriosCamera.showOverlay){
            cameraOverlay.setVisibility(VISIBLE);
            topRlayout.setVisibility(GONE);
        }
        else {
            cameraOverlay.setVisibility(GONE);
            topRlayout.setVisibility(VISIBLE);

        }

        recordButton.setActionListener(this);
        recordButton.setVideoDuration(40000, 10000);


    }


    public void setMediaList(List<Media> mediaList) {
        slidingGalleryList.setAdapter(new GalleryAdapter(context, GalleryAdapter.SMALL, mediaList));
        slidingGalleryList.addOnItemTouchListener(new RecyclerItemClickListener(context, pickerItemClickListener));
    }

    public void lockControls() {
        toggleControls(false);
    }

    public void unLockControls() {
        toggleControls(true);
    }

    private void toggleControls(boolean isVisible) {
        cameraSwitchView.setEnabled(isVisible);
//        recordButton.setEnabled(isVisible);
        settingsButton.setEnabled(isVisible);
        flashSwitchView.setEnabled(isVisible);
    }

    public void setup(int mediaAction) {
        this.mediaAction = mediaAction;
        if (CameraConfiguration.MEDIA_ACTION_VIDEO == mediaAction || CameraConfiguration.MEDIA_ACTION_BOTH == mediaAction) {
            recordButton.setMediaType(mediaAction);
            flashSwitchView.setVisibility(GONE);
        } else {
            recordButton.setMediaType(CameraConfiguration.MEDIA_ACTION_PHOTO);
        }

        /*if (CameraConfiguration.MEDIA_ACTION_BOTH != mediaAction) {
            mediaActionSwitchView.setVisibility(GONE);
        } else mediaActionSwitchView.setVisibility(VISIBLE);*/
    }

    public void setMediaFilePath(final File mediaFile) {
        this.mediaFilePath = mediaFile.toString();
    }

    public void setMaxVideoFileSize(long maxVideoFileSize) {
        this.maxVideoFileSize = maxVideoFileSize;
    }


    public void setFlasMode(@FlashSwitchView.FlashMode int flashMode) {
        flashSwitchView.setFlashMode(flashMode);
    }

    public void setMediaActionState(@MediaActionSwitchView.MediaActionState int actionState) {
        if (mediaActionState == actionState) return;
        /*if (MediaActionSwitchView.ACTION_PHOTO == actionState) {
            recordButton.setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO);
            if (hasFlash)
                flashSwitchView.setVisibility(VISIBLE);
        } else {
            recordButton.setMediaAction(CameraConfiguration.MEDIA_ACTION_VIDEO);
            flashSwitchView.setVisibility(GONE);
        }*/
        mediaActionState = actionState;
//        mediaActionSwitchView.setMediaActionState(actionState);
    }

    public void setRecordButtonListener(RecordButtonListener recordButtonListener) {
        this.recordButtonListener = recordButtonListener;
    }


    public void rotateControls(int rotation) {
        cameraSwitchView.setRotation(rotation);
//        mediaActionSwitchView.setRotation(rotation);
        flashSwitchView.setRotation(rotation);
        recordDurationText.setRotation(rotation);
        recordSizeText.setRotation(rotation);
    }

    public void setOnMediaActionStateChangeListener(MediaActionSwitchView.OnMediaActionStateChangeListener onMediaActionStateChangeListener) {
        this.onMediaActionStateChangeListener = onMediaActionStateChangeListener;
    }

    public void setOnCameraTypeChangeListener(CameraSwitchView.OnCameraTypeChangeListener onCameraTypeChangeListener) {
        this.onCameraTypeChangeListener = onCameraTypeChangeListener;
        if (cameraSwitchView != null)
            cameraSwitchView.setOnCameraTypeChangeListener(this.onCameraTypeChangeListener);
    }

    public void setFlashModeSwitchListener(FlashSwitchView.FlashModeSwitchListener flashModeSwitchListener) {
        this.flashModeSwitchListener = flashModeSwitchListener;
        if (flashSwitchView != null)
            flashSwitchView.setFlashSwitchListener(this.flashModeSwitchListener);
    }

    public void setSettingsClickListener(SettingsClickListener settingsClickListener) {
        this.settingsClickListener = settingsClickListener;
    }

    public void setOverlayClickListener(OverlayClickListener overlayClickListener) {
        this.overlayClickListener = overlayClickListener;
    }

    public void setPickerItemClickListener(RecyclerItemClickListener.OnClickListener pickerItemClickListener) {
        this.pickerItemClickListener = pickerItemClickListener;
    }

    @Override
    public void onTakePhotoButtonPressed() {
        if (recordButtonListener != null)
            recordButtonListener.onTakePhotoButtonPressed();
    }

    public void onStartVideoRecord(final File mediaFile) {
        setMediaFilePath(mediaFile);

        if (maxVideoFileSize > 0) {
            recordSizeText.setText("1Mb" + " / " + maxVideoFileSize / (1024 * 1024) + "Mb");
            recordSizeText.setVisibility(VISIBLE);
            try {
                fileObserver = new FileObserver(this.mediaFilePath) {
                    private long lastUpdateSize = 0;

                    @Override
                    public void onEvent(int event, String path) {
                        final long fileSize = mediaFile.length() / (1024 * 1024);
                        if ((fileSize - lastUpdateSize) >= 1) {
                            lastUpdateSize = fileSize;
                            recordSizeText.post(new Runnable() {
                                @Override
                                public void run() {
                                    recordSizeText.setText(fileSize + "Mb" + " / " + maxVideoFileSize / (1024 * 1024) + "Mb");
                                }
                            });
                        }
                    }
                };
                fileObserver.startWatching();
            } catch (Exception e) {
                Log.e("FileObserver", "setMediaFilePath: ", e);
            }
        }
    }

    public void allowRecord(boolean isAllowed) {
//        recordButton.setEnabled(isAllowed);
    }

    public void showPicker(boolean isShown) {
        if (isShown) {

        }
//        slidingGalleryList.setVisibility(isShown ? VISIBLE : GONE);
    }

    public boolean showCrop() {
        return showImageCrop;
    }

    public void shouldShowCrop(boolean showImageCrop) {
        this.showImageCrop = showImageCrop;
    }

    public void allowCameraSwitching(boolean isAllowed) {
        cameraSwitchView.setVisibility(isAllowed ? VISIBLE : GONE);
    }

    public void onStopVideoRecord() {
        if (fileObserver != null)
            fileObserver.stopWatching();
//        slidingGalleryList.setVisibility(VISIBLE);
        recordSizeText.setVisibility(GONE);
        cameraSwitchView.setVisibility(View.VISIBLE);
        settingsButton.setVisibility(VISIBLE);

      /*  if (CameraConfiguration.MEDIA_ACTION_BOTH != mediaAction) {
            mediaActionSwitchView.setVisibility(GONE);
        } else mediaActionSwitchView.setVisibility(VISIBLE);*/
//        recordButton.setRecordState(RecordButton.READY_FOR_RECORD_STATE);
    }

    @Override
    public void onStartRecordingButtonPressed() {
        cameraSwitchView.setVisibility(View.GONE);
//        mediaActionSwitchView.setVisibility(GONE);
        settingsButton.setVisibility(GONE);
//        slidingGalleryList.setVisibility(GONE);

        if (recordButtonListener != null)
            recordButtonListener.onStartRecordingButtonPressed();
    }

    @Override
    public void onStopRecordingButtonPressed() {
        onStopVideoRecord();
        if (recordButtonListener != null)
            recordButtonListener.onStopRecordingButtonPressed();
    }



    @Override
    public void onMediaActionChanged(int mediaActionState) {
        setMediaActionState(mediaActionState);
        if (onMediaActionStateChangeListener != null)
            onMediaActionStateChangeListener.onMediaActionChanged(this.mediaActionState);
    }




    @Override
    public void onStartRecord() {
        onStartRecordingButtonPressed();
        startCounter();
    }

    @Override
    public void onEndRecord() {
        onStopRecordingButtonPressed();
        if (countDownTimer != null)
            countDownTimer.onFinish();
        recordDurationText.setText("");

    }

    @Override
    public void onDurationTooShortError() {

    }

    @Override
    public void onSingleTap() {
        onTakePhotoButtonPressed();
    }

    @Override
    public void onCircleComplete() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
            startCounter();
        }
        Camera1Manager.isVideoPreviewShow = true;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = Camera1Manager.getInstance().videoPreview;
                if (getContext() != null && bitmap != null) {
                    ImageView image = new ImageView(getContext());
                    image.setLayoutParams(new android.view.ViewGroup.LayoutParams(60, 110));
                    image.setImageBitmap(bitmap);
                    videoThumbnailLayout.addView(image);
                }

            }
        }, 1000);

    }


    @Override
    public void onCancelled() {

    }

    public interface SettingsClickListener {
        void onSettingsClick();
    }

    public interface  OverlayClickListener{
        void removeOverlay();
        void cancelOverlay();
        void keepSwapping();
    }

    private void startCounter() {
        countDownTimer = new CountDownTimer(11000, 1000) {

            public void onTick(long millisUntilFinished) {
                if ((millisUntilFinished) / 1000 > 0)
                    recordDurationText.setText(String.valueOf((millisUntilFinished) / 1000));
            }

            public void onFinish() {
                recordDurationText.setText("");
            }
        }.start();
    }


}
