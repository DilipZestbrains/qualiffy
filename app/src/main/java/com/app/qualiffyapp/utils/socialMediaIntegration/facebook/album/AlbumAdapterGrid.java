package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.ArrayList;

/**
 * Created by Dell on 3/7/2017.
 */
public class AlbumAdapterGrid extends BaseAdapter {
    private Context mContext;
    private ArrayList<GenericImageModel> genericFbImageModelsAL;
    private OpponentNamee mOpponentName;

    /**
     * Instantiates a new Album adapter grid.
     *
     * @param context                the context
     * @param genericFbImageModelsAL the generic fb image models al
     * @param activityGamersList     the activity gamers list
     */
    public AlbumAdapterGrid(Context context, ArrayList<GenericImageModel> genericFbImageModelsAL, FacebookAlbumActivity activityGamersList) {
        mContext = context;
        this.genericFbImageModelsAL = genericFbImageModelsAL;
        this.mOpponentName = activityGamersList;
    }

    @Override
    public int getCount() {
        return genericFbImageModelsAL.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, ViewGroup parent) {

        // Video track = getItem(position);

        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.video_grid, parent, false);
            holder = new ViewHolder();
            holder.videoImageView = (ImageView) convertView.findViewById(R.id.track_image);
            holder.rel_albums = (RelativeLayout) convertView.findViewById(R.id.rel_albums);
            holder.rel_albums.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOpponentName.OpponentFullNamee(genericFbImageModelsAL.get(position).getSource());
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (AlbumAdapterGrid.ViewHolder) convertView.getTag();
        }
        GlideUtil.loadImage(holder.videoImageView, genericFbImageModelsAL.get(position).getSource(), R.drawable.user_placeholder, 1000, 8);


        return convertView;
    }

    /**
     * The interface Opponent namee.
     */
    public interface OpponentNamee {
        /**
         * Opponent full namee.
         *
         * @param s the s
         */
        void OpponentFullNamee(String s);
    }

    /**
     * The type View holder.
     */
    static class ViewHolder {
        /**
         * The Video image view.
         */
        ImageView videoImageView;
        /**
         * The Rel albums.
         */
        RelativeLayout rel_albums;
    }
}
