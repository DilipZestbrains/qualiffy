package com.app.qualiffyapp.utils.socialMediaIntegration.snapchat;

import com.snapchat.kit.sdk.login.models.UserDataResponse;

public interface SnapChatCallback {

    void getSnapChatData(UserDataResponse userDataResponse, String id);

}
