package com.app.qualiffyapp.utils.downloadFile;

import android.os.Environment;
import android.util.Log;

import com.app.qualiffyapp.utils.Utility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

import static com.app.qualiffyapp.constants.AppConstants.TEMP_MEDIA_FOLDER;

public class DownloadFileRx {
    private String mediaFolder;

    private String folder;
    public static final String ERROR = "Something went wrong";
    private ArrayList<String> downloadedFiles;


    public DownloadFileRx(List<String> fileMap, String mediaFolder, DownloadFileCallback listener) {
        this.mediaFolder = mediaFolder;
        this.downloadedFiles = new ArrayList<>();


        Observable.just(fileMap) //we create an Observable that emits a single array
                .concatMap(urls -> Observable.fromIterable(urls)) //map the list to an Observable that emits every item as an observable
                .concatMap(url -> getDownloadedFile(url))//download smth on every number in the array
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String s) {
                        Log.e("downloadFile", s);
                        if (downloadedFiles == null)
                            downloadedFiles = new ArrayList<>();
                        downloadedFiles.add(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("downloadFile", e.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        Log.e("downloadFile", "COMPLETE");
                        listener.getDownloadFileList(downloadedFiles);

                    }
                });
    }


    private Observable<String> getDownloadedFile(String Urls) {

        return Observable.just(Urls)
                .map(url -> downloadMedia(url))
                .subscribeOn(Schedulers.io());
    }


    private String downloadMedia(String file) {
        int count;
        String fileName = file.substring(file.lastIndexOf('/') + 1, file.length());

        File mediaFile = Utility.getFileFromDevice(fileName, TEMP_MEDIA_FOLDER);


        if (mediaFile == null || !mediaFile.exists()) {
            try {
                URL url = new URL(file);
                URLConnection connection = url.openConnection();
                connection.connect();
                int lengthOfFile = connection.getContentLength();

                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                folder = Environment.getExternalStorageDirectory() + "/" + mediaFolder + "/";

                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    Log.d("DownloadFiles", "Progress: " + (int) ((total * 100) / lengthOfFile));

                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());

            }
        } else
            return mediaFile.getPath() == null ? mediaFile.getAbsolutePath() : mediaFile.getPath();

        return ERROR;
    }


    public void reloadFile(String url,DownloadFileCallback listener) {
        Observable.just(url)
                .map(urls -> downloadfile(urls, true))
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String s) {
                        listener.reloadFile(s);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("downloadFile", e.getMessage());

                    }

                    @Override
                    public void onComplete() {
                        Log.e("downloadFile", "COMPLETE");
                    }
                })

        ;
    }


    private String downloadfile(String file, boolean isRelad) {
        int count;
        String fileName = file.substring(file.lastIndexOf('/') + 1, file.length());
        try {
            URL url = new URL(file);
            URLConnection connection = url.openConnection();
            connection.connect();
            int lengthOfFile = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            folder = Environment.getExternalStorageDirectory() + "/" + mediaFolder + "/";

            File directory = new File(folder);

            if (!directory.exists()) {
                directory.mkdirs();
            }

            File mediaFile = Utility.getFileFromDevice(fileName, TEMP_MEDIA_FOLDER);

            if (mediaFile != null)
                mediaFile.delete();

            OutputStream output = new FileOutputStream(folder + fileName);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                Log.d("DownloadFiles", "Progress: " + (int) ((total * 100) / lengthOfFile));

                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
            return folder + fileName;

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());

        }
        return ERROR;
    }


    public interface DownloadFileCallback {
        void getDownloadFileList(List<String> downloadFile);

        void reloadFile(String downloadFile);
    }
}
