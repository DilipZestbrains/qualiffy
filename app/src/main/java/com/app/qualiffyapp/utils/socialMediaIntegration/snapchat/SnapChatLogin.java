package com.app.qualiffyapp.utils.socialMediaIntegration.snapchat;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.app.qualiffyapp.utils.Utility;
import com.orhanobut.logger.Logger;
import com.snapchat.kit.sdk.SnapLogin;
import com.snapchat.kit.sdk.core.controller.LoginStateController;
import com.snapchat.kit.sdk.login.models.MeData;
import com.snapchat.kit.sdk.login.models.UserDataResponse;
import com.snapchat.kit.sdk.login.networking.FetchUserDataCallback;

import java.util.Map;

import static com.app.qualiffyapp.constants.AppConstants.SNAPCHAT;

public class SnapChatLogin implements LoginStateController.OnLoginStateChangedListener {
    private Context context;
    private SnapChatCallback snapChatCallback;

    public SnapChatLogin(Context context, SnapChatCallback snapChatCallback) {
        this.context = context;
        this.snapChatCallback = snapChatCallback;
        SnapLogin.getLoginStateController(context).addOnLoginStateChangedListener(this);

    }


    @Override
    public void onLoginSucceeded() {
        boolean isUserLoggedIn = SnapLogin.isUserLoggedIn(context);
        if (isUserLoggedIn) {
            fetchSnapChatUserInfo();
        }

    }

    @Override
    public void onLoginFailed() {
        Logger.e("Snapchat login failed.");


    }

    @Override
    public void onLogout() {
        SnapLogin.getLoginStateController(context).removeOnLoginStateChangedListener(this);
        SnapLogin.getAuthTokenManager(context).revokeToken();
    }

    private void fetchSnapChatUserInfo() {
        String query = "{me{bitmoji{avatar},displayName,externalId}}";
        Map<String, Object> variables = null;
        SnapLogin.fetchUserData(context, query, variables, new FetchUserDataCallback() {
            @Override
            public void onSuccess(@Nullable UserDataResponse userDataResponse) {
                if (userDataResponse == null || userDataResponse.getData() == null) {
                    return;
                }

                MeData meData = userDataResponse.getData().getMe();
                if (meData == null) {
                    return;
                }

                snapChatCallback.getSnapChatData(userDataResponse, userDataResponse.getData().getMe().getExternalId());
                Log.e("SnapChat User", userDataResponse.getData().getMe().getDisplayName());
                Log.e("SnapChat User ID", userDataResponse.getData().getMe().getExternalId());

                Utility.setSocialName(context, SNAPCHAT,
                        userDataResponse.getData().getMe().getDisplayName(),
                        "",
                        null);
                SnapLogin.getLoginStateController(context).removeOnLoginStateChangedListener(SnapChatLogin.this);

            }

            @Override
            public void onFailure(boolean isNetworkError, int statusCode) {

            }
        });
    }


    public void snapChatUnlink(Context context) {
        SnapLogin.getAuthTokenManager(context).revokeToken();

    }
}
