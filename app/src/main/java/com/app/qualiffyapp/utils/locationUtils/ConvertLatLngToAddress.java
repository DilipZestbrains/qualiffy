package com.app.qualiffyapp.utils.locationUtils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import java.util.List;
import java.util.Locale;

/**
 * Created by osr on 17/8/18.
 */

public class ConvertLatLngToAddress {


    public AddressBean getAddress(Context context, Double lat, Double lng) throws Exception {
        AddressBean addressBean = new AddressBean();

        Geocoder geocoder;
        List<Address> addresses;
        String address = "";
        String city = "";
        String area = "";
        geocoder = new Geocoder(context, Locale.getDefault());

        addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        addressBean.addr = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        addressBean.city = addresses.get(0).getLocality();
        area = addresses.get(0).getSubLocality();
        addressBean.lat = String.valueOf(lat);
        addressBean.lng = String.valueOf(lng);

        addressBean.state = addresses.get(0).getAdminArea();
        addressBean.cntry = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();


        return addressBean;
    }

    public static class AddressBean {

        public String addr;
        public String cntry;
        public String state;
        public String lng;
        public String lat;
        public String city;
    }
}


