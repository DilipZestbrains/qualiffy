package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.utils.GlideUtil;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

/**
 * Created by Dell on 3/7/2017.
 */
public class AlbumsAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<AlbumPictureModel> malbumPictureModelsAL;
    private OpponentName mOpponentName;

    /**
     * Instantiates a new Albums adapter.
     *
     * @param context              the context
     * @param albumPictureModelsAL the album picture models al
     * @param activityGamersList   the activity gamers list
     */
    public AlbumsAdapter(Context context, ArrayList<AlbumPictureModel> albumPictureModelsAL, FacebookAlbumActivity activityGamersList) {
        mContext = context;
        this.malbumPictureModelsAL = albumPictureModelsAL;
        this.mOpponentName = activityGamersList;
    }

    @Override
    public int getCount() {
        return malbumPictureModelsAL.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Nullable
    @Override
    public View getView(final int position, @Nullable View convertView, ViewGroup parent) {

        // Video track = getItem(position);

        AlbumsAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.video_list_row, parent, false);
            holder = new AlbumsAdapter.ViewHolder();
            holder.videoImageView = (ImageView) convertView.findViewById(R.id.track_image);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.track_tittle);
            holder.rel_albums = (RelativeLayout) convertView.findViewById(R.id.rel_albums);
            holder.rel_albums.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOpponentName.OpponentFullName(malbumPictureModelsAL.get(position).getId());
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (AlbumsAdapter.ViewHolder) convertView.getTag();
        }

        holder.titleTextView.setText(malbumPictureModelsAL.get(position).getName());
        GlideUtil.loadImage(holder.videoImageView, malbumPictureModelsAL.get(position).getUrl(), R.drawable.user_placeholder, 1000, 8);


        return convertView;
    }

    /**
     * The interface Opponent name.
     */
    public interface OpponentName {
        /**
         * Opponent full name.
         *
         * @param s the s
         */
        void OpponentFullName(String s);
    }

    /**
     * The type View holder.
     */
    static class ViewHolder {
        /**
         * The Video image view.
         */
        ImageView videoImageView;
        /**
         * The Title text view.
         */
        TextView titleTextView;
        /**
         * The Rel albums.
         */
        RelativeLayout rel_albums;
    }
}

