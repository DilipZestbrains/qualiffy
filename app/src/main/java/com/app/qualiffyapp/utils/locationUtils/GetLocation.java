package com.app.qualiffyapp.utils.locationUtils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.utils.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by osr on 20/7/18.
 */

public class GetLocation implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final int RESOLUTION_REQUEST_LOCATION = 1001;
    public static double myLatitude, myLongitude;
    private static GetLocation getLocation;
    public Location myLocation;
    Activity context;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private int playServiceStaCode;
    private boolean checkNetStatus = false;
    //    private ProgressBarHandler progressBarHandler;
    private GetLocationCallback getLocationCallback;

    LocationListener listener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            myLocation = location;
            startServices();
            Log.v("@LOCCHANGED", "YES");
        }
    };
    private SettingsClient mSettingsClient;
    private LocationSettingsRequest mLocationSettingsRequest;


    private GetLocation(Activity context) {
        this.context = context;
    }

    public static GetLocation getInstance(Activity context) {
        if (getLocation == null)
            getLocation = new GetLocation(context);

        return getLocation;
    }

    public static void requestPermission(Activity context) {
        ActivityCompat.requestPermissions(context,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                AppConstants.LOCATION_PERMISSION_REQUEST_CODE);
    }

    public static boolean CheckPermission(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public void gettingLocationWithProgressBar(GetLocationCallback getLocationCallback) {
        this.getLocationCallback = getLocationCallback;
      /*  context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBarHandler = ProgressBarHandler.getInstance(context);
                progressBarHandler.show();
            }
        });*/

//        if (checkGooglePlayServiceAvailability(context)) {
        buildGoogleApiClient();
//        }


    }

    //Checking Google Play Service and update
    public boolean checkGooglePlayServiceAvailability(final Activity context, GetLocationCallback getLocationCallback) {
        this.getLocationCallback = getLocationCallback;

        playServiceStaCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if ((playServiceStaCode == ConnectionResult.SUCCESS)) {
            return true;
        } else {
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(playServiceStaCode, context, 10, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Utility.showToast(context, context.getResources().getString(R.string.google_play_service));
//                    new AlertToastUtils(context, context.getResources().getString(R.string.google_play_service));
                }
            });
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
    }

    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest().create();
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (listener != null && mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
            checkResolutionAndProceed();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }

    private void checkResolutionAndProceed() {
        checkNetStatus = InternetAvailability.getInstance().isOnline(context);

        try {
            if (!checkNetStatus) {
                Utility.showToast(context, context.getResources().getString(R.string.internet_check));
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startGettingLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(context, RESOLUTION_REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                }
            }
        });
    }

    public void startGettingLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (mGoogleApiClient.isConnected())
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, listener, Looper.getMainLooper());

    }

    void startServices() {

        if (myLocation != null) {
            myLatitude = myLocation.getLatitude();
            myLongitude = myLocation.getLongitude();

            if (getLocationCallback != null)
                getLocationCallback.getLocation(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));

            Log.d("WORKER callback", String.valueOf(myLatitude) + "," + String.valueOf(myLongitude));
            Location temp = getLoc(new LatLng(myLatitude, myLongitude));
//            insideGeofenceLocation(temp);


            Log.e("WORKER lOACTION", "" + myLatitude + ", " + myLongitude);

        } else {

            if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
                disconnectLocation();
                buildGoogleApiClient();
            }
        }
      /*  context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (myLocation != null) {
                    myLatitude = myLocation.getLatitude();
                    myLongitude = myLocation.getLongitude();

                    getLocationCallback.getLocation(new LatLng(myLocation.getLatitude(), myLocation.getLongitude()));
                    if (progressBarHandler != null)
                        progressBarHandler.hide();

                } else {

                    if (mGoogleApiClient != null && !mGoogleApiClient.isConnected()) {
                        disconnectLocation();
                        buildGoogleApiClient();
                    }
                }
            }
        });*/
    }

    private Location getLoc(LatLng latLng) {
        Location temp = new Location(LocationManager.GPS_PROVIDER);
        temp.setLatitude(latLng.latitude);
        temp.setLongitude(latLng.longitude);
        return temp;
    }

    public void disconnectLocation() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && listener != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, listener);
            listener = null;
            mGoogleApiClient.disconnect();
        }
    /*    if (progressBarHandler != null) {
            progressBarHandler.hide();
            progressBarHandler = null;
        }*/

    }


}
