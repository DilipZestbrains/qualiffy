package com.app.qualiffyapp.utils.videoCompressUtils;

import android.graphics.Bitmap;

import java.io.File;

public interface IVideoCompressCallback {

    void showVideoCompressProgress(boolean isShown);

    void getVideoFile(Bitmap bm, File file);

    void isCompressSupport(boolean isCompressSupported);
}
