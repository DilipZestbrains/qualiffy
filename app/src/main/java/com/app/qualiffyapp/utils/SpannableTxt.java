package com.app.qualiffyapp.utils;

import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.app.qualiffyapp.callbacks.signupLogin.SpannableClickCallback;

public class SpannableTxt {

    public void setSpannableString(String str,
                                   TextView txtView,
                                   int color,
                                   int disableColor,
                                   int start,
                                   SpannableClickCallback spannableClickCallback,
                                   boolean isUnderLine,
                                   int clickFlag) {

        SpannableString spannableStr
                = new SpannableString(str);

        ClickableSpan clickableSpanTermsOfService = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                spannableClickCallback.spannableStringClick(clickFlag);
            }

            public void updateDrawState(TextPaint ds) {//
                if (isUnderLine)// override updateDrawState
                    ds.setUnderlineText(true);
                else
                    ds.setUnderlineText(false);
                // set to false to remove underline
            }
        };


        spannableStr.setSpan(clickableSpanTermsOfService, start, spannableStr.length(), 0);
        spannableStr.setSpan(new StyleSpan(Typeface.BOLD), start, spannableStr.length(), 0);
        spannableStr.setSpan(new ForegroundColorSpan(color), start, spannableStr.length(), 0);
        spannableStr.setSpan(new ForegroundColorSpan(disableColor), 0, start - 1, 0);

        txtView.setMovementMethod(LinkMovementMethod.getInstance());
        txtView.setGravity(Gravity.CENTER);
        txtView.setBackgroundColor(Color.TRANSPARENT);
        txtView.setText(spannableStr);

    }
}


