package com.app.qualiffyapp.utils;

import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;

import java.io.File;

public class GlideUtil {

    public static void loadImage(ImageView imageview, Object url, int placeholder, int crossfadeDuration, int roundedCorners) {
        RequestBuilder<android.graphics.drawable.Drawable> requestBuilder = null;
        RequestOptions requestOptions = new RequestOptions();
        if (roundedCorners > 0) requestOptions.transform(new RoundedCorners(roundedCorners));
        if (placeholder != 0)
            requestBuilder = Glide.with(imageview.getContext()).load(placeholder).apply(requestOptions);
        Glide.with(imageview.getContext()).load(url)
                .transition(DrawableTransitionOptions.withCrossFade(crossfadeDuration))
                .thumbnail(requestBuilder)
                .apply(requestOptions)
                .into(imageview);


    }

    public static void loadImageFromFile(ImageView img, String uri) {

        File file = new File(uri);
        Uri imageUri = Uri.fromFile(file);
        Glide.with(img.getContext())
                .load(imageUri)
                .into(img);
    }


    public static void loadCircularImage(ImageView imageView, Object url, int placeholder) {

        Glide.with(imageView.getContext()).load(url)
                .apply(RequestOptions.circleCropTransform().placeholder(placeholder))
                .into(imageView);


    }

}
