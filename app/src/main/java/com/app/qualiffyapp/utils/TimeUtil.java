package com.app.qualiffyapp.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TimeUtil {

    public static String getDateFromTimestamp(String timeStamp) {
        String dateFormat = "MMMM dd, yyyy";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTimeInMillis(Long.parseLong(timeStamp));
            return formatter.format(cal.getTime());

        } catch (Exception e) {
            Log.e(TimeUtil.class.getName(), e.getMessage());
        }
        return null;
    }

    public static String getDateFromTimestamp(long timeStamp, String formate) {
        SimpleDateFormat formatter = new SimpleDateFormat(formate);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeStamp);
        String date = formatter.format(cal.getTime());
        if (date.contains("70")) {
            cal.setTimeInMillis(timeStamp * 1000);
            date = formatter.format(cal.getTime());
        }
        return date;
    }

    public static String getDateFromTimeStamp(String timestamp, String formate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(formate);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTimeInMillis(Long.parseLong(timestamp));
            return simpleDateFormat.format(cal.getTime());
        } catch (Exception e) {
            Log.e(TimeUtil.class.getName(), e.getLocalizedMessage());
        }

        return null;
    }

    public static String getTimeFromMillisecond(long miliseconds) {
        int sec = 0, min = 0, h = 0;
        sec = (int) (miliseconds / 1000);
        if (sec > 60) {
            min = sec / 60;
            sec = sec % 60;
        }
        if (min > 60) {
            h = min / 60;
            min = min % 60;
        }

        String time = "";
        if (h > 0) {
            time = h + ":";
        }
        if (min > 9) {
            time = time + min + ":";
        }
        if (min <= 9) {
            time = time + "0" + min + ":";
        }
        if (sec > 9) {
            time = time + sec;
        }
        if (sec <= 9) {
            time = time + "0" + sec;
        }
        return time;
    }

    public static String getTimeFromMillisecond(String mili) {
        try {
            long miliseconds = Long.valueOf(mili);
            int sec = 0, min = 0, h = 0;
            sec = (int) (miliseconds / 1000);
            if (sec > 60) {
                min = sec / 60;
                sec = sec % 60;
            }
            if (min > 60) {
                h = min / 60;
                min = min % 60;
            }

            String time = "";
            if (h > 0) {
                time = h + ":";
            }
            if (min > 9) {
                time = time + min + ":";
            }
            if (min <= 9) {
                time = time + "0" + min + ":";
            }
            if (sec > 9) {
                time = time + sec;
            }
            if (sec <= 9) {
                time = time + "0" + sec;
            }
            return time;
        } catch (Exception e) {
            Log.e(TimeUtil.class.getName(), e.getLocalizedMessage());
            return null;
        }

    }


}
