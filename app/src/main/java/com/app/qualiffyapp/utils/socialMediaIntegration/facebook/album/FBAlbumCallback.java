package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import java.util.ArrayList;

public interface FBAlbumCallback {

    void getAlbum(ArrayList<AlbumPictureModel> albumPictureModelsAL);
  /*  void getAlbumImage(ArrayList<GenericImageModel> genericFbImageModelsAL);
    void albumError(String msg);*/
}
