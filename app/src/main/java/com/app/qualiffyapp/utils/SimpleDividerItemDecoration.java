package com.app.qualiffyapp.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;


/**
 * Created by  on 15/3/17.
 */

public class SimpleDividerItemDecoration extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    public SimpleDividerItemDecoration(Context context) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.divider_bg);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        //int left = parent.getPaddingLeft();
        int top = parent.getPaddingTop();
        int bottom = parent.getPaddingBottom();
        // int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int left = child.getLeft() + params.leftMargin;
            //int bottom = top + mDivider.getIntrinsicHeight();
            int right = left + mDivider.getIntrinsicHeight();

            mDivider.setBounds(child.getLeft() + 2, child.getTop() + 2, child.getRight() + 2, child.getBottom() + 2);
            mDivider.draw(c);
        }
    }
}