package com.app.qualiffyapp.utils.socialMediaIntegration.facebook;

import com.facebook.FacebookException;

import org.json.JSONObject;

/**
 * Created by osr on 27/6/18.
 */

public interface FacebookResultCallback {

    void onFbResponse(JSONObject jsonObject);

    void onFbError(FacebookException facebookException);
}
