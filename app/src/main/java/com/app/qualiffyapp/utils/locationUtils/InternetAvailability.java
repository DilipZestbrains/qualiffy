package com.app.qualiffyapp.utils.locationUtils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by kumar on 11/7/2017.
 */

public class InternetAvailability {
    private static InternetAvailability instance = new InternetAvailability();
    Context context;
    ConnectivityManager connectivityManager;
    NetworkInfo wifiInfo, mobileInfo;
    boolean connected = false;

    public static InternetAvailability getInstance() {
        return instance;
    }

    public boolean isOnline(Context ctx) {
        context = ctx.getApplicationContext();

        try {
            connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            connected = networkInfo != null && networkInfo.isAvailable() &&
                    networkInfo.isConnected();
            return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Log.e("connectivity", e.toString());
        }
        return connected;
    }
}

