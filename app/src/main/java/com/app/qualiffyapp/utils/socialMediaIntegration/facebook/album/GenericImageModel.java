package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import java.io.Serializable;

/**
 * Created by palash on 22/7/16.
 */
public class GenericImageModel implements Serializable {


    /**
     * The Id.
     */
    String id, /**
     * The Source.
     */
    source;

    /**
     * Gets id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets source.
     *
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets source.
     *
     * @param source the source
     */
    public void setSource(String source) {
        this.source = source;
    }
}
