package com.app.qualiffyapp.utils.locationUtils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

/**
 * Created by kumar on 1/10/2018.
 */

public class ConvertAddresstoLatLng {


    public static LatLng getlatlngFromAddress(Context context, String address) {
        LatLng latLng = null;
        Geocoder coder = new Geocoder(context);
        List<Address> addressList;

        try {
            addressList = coder.getFromLocationName(address, 5);
            if (address == null) {
                return null;
            }

            Address location = addressList.get(0);
            location.getLatitude();
            location.getLongitude();

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException e) {

        }
        return latLng;
    }
}
