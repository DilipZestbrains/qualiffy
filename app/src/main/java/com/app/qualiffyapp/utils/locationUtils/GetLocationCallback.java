package com.app.qualiffyapp.utils.locationUtils;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by osr on 20/7/18.
 */

public interface GetLocationCallback {
    void getLocation(LatLng latLng);
//    void checkResolution();
}
