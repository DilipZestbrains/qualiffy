package com.app.qualiffyapp.utils;


import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.qualiffyapp.base.BaseFragment;

import java.util.List;


/**
 * Created on 11/4/2017.
 */

public class FragmentTransactions {

    public static void replaceFragment(Context context, Fragment fragment, String tag, int layout, Boolean isAddFrag, Boolean addToBackStack) {
        FragmentTransaction ft = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
        ft.replace(layout, fragment, tag);
        ft.detach(fragment);
        ft.attach(fragment);
        if (addToBackStack)
            ft.addToBackStack(tag);

        if (!isAddFrag) {
            while (((AppCompatActivity) context).getSupportFragmentManager().getBackStackEntryCount() > 0) {
                ((AppCompatActivity) context).getSupportFragmentManager().popBackStackImmediate();
            }
        }
        ft.commitAllowingStateLoss();
    }

    public static void removeAllFragments(FragmentManager fragmentManager) {
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss();
            }
        }
    }

    public static void pushFragment(FragmentManager fragmentManager, int container, BaseFragment fragment, Boolean addToBackStack, String backStackName, Boolean animate) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (animate) {
//            transaction.setCustomAnimations(R.anim.abc_fade_in, R.anim.abc_fade_out);
        }
        if (addToBackStack)
            transaction.addToBackStack(backStackName);

        transaction.replace(container, fragment);
        transaction.commit();
    }


    public static Fragment currentFragment(FragmentManager fragmentManager, int layout_id) {
        return fragmentManager.findFragmentById(layout_id);
    }

    public static void onPopFragment(FragmentManager fragmentManager) {
        fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount());
    }

}
