package com.app.qualiffyapp.utils.animation;

import android.transition.Slide;
import android.transition.Visibility;
import android.view.Gravity;

public class CustomAnimations {

    public Visibility buildEnterTransition(int duration) {
        Slide enterTransition = new Slide();
        enterTransition.setDuration(duration);
        enterTransition.setSlideEdge(Gravity.RIGHT);
        return enterTransition;
    }
}
