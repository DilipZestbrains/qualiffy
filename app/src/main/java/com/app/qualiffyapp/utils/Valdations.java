package com.app.qualiffyapp.utils;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;

import java.util.regex.Pattern;

public class Valdations {


    public String validateUserName(String value) {
        if (value.isEmpty())
            return ApplicationController.getApplicationInstance().getString(R.string.empty_username);
        if (value.length() < 3)
            return ApplicationController.getApplicationInstance().getString(R.string.username_length);
        return null;
    }

    public String validateUserMobile(String value) {
        if (value.isEmpty())
            return ApplicationController.getApplicationInstance().getString(R.string.login_alert);
        if (value.length() < 3)
            return ApplicationController.getApplicationInstance().getString(R.string.login_length_alert);
        return null;
    }


    public String validatePhone(String value) {

        if (value == null || value.isEmpty())
            return ApplicationController.getApplicationInstance().getString(R.string.empty_phone);
        if (value.length() < 8 || value.length()>16)
            return ApplicationController.getApplicationInstance().getString(R.string.mobile_error_alert);
        String regex = "\\d{10}";
        if (!Pattern.compile(regex).matcher(value).matches())
            return ApplicationController.getApplicationInstance().getString(R.string.mobile_error_alert);
        return null;
    }


    public String validateOtp(String otp) {
        if (otp == null || otp.isEmpty())
            return ApplicationController.getApplicationInstance().getString(R.string.empty_otp);
        if (otp.length() < 4)
            return ApplicationController.getApplicationInstance().getString(R.string.otp_length);

        return null;
    }
}
