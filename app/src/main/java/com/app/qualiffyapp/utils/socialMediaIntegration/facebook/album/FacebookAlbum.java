package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FacebookAlbum {
    private ArrayList<AlbumPictureModel> albumPictureModelsAL;
    private ArrayList<GenericImageModel> genericFbImageModelsAL;
    private FBAlbumCallback albumCallback;

    public FacebookAlbum(FBAlbumCallback albumCallback) {
        this.albumCallback = albumCallback;
    }


    public void getFbAlbums(String profileUserId) {
        Bundle parameters2 = new Bundle();
        parameters2.putString("fields", "id,name,picture");
        parameters2.putString("type", "album");
        GraphRequest graphRequest1 = new GraphRequest(
                AccessToken.getCurrentAccessToken(), "/" + profileUserId + "/albums",
                parameters2,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(@NonNull GraphResponse response) {
                        JSONObject jsonObject = response.getJSONObject();
                        Log.e("MyFbFrag : ", " response of albums :" + response.toString());
                        albumPictureModelsAL = new ArrayList<AlbumPictureModel>();
                        final JSONArray jsonAlbumList = jsonObject.optJSONArray("data");
                        for (int i = 0; i < jsonAlbumList.length(); i++) {
                            try {
                                //fetching the inner data from data
                                JSONObject innerJsonObject = jsonAlbumList.getJSONObject(i);
                                final AlbumPictureModel albumPictureModel = new AlbumPictureModel();
                                albumPictureModel.setId(innerJsonObject.optString("id"));
                                albumPictureModel.setUrl(innerJsonObject.optJSONObject("picture").optJSONObject("data").optString("url"));
                                albumPictureModel.setName(innerJsonObject.optString("name"));
                                albumPictureModelsAL.add(albumPictureModel);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        if (albumPictureModelsAL.size() > 0) {
                            albumCallback.getAlbum(albumPictureModelsAL);
                        }
                    }
                }
        );
        graphRequest1.executeAsync();
        String token = AccessToken.getCurrentAccessToken().getToken();
        String url = "https://graph.facebook.com/" + profileUserId + "/picture?access_token=" + token + "&type=normal";
        Log.e("MyFbFrag : ", "Access Token :  " + token);
    }


/*
    private void getPhotosBasedOnAlbumId(String AlbumId) {
        GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + AlbumId + "/photos", null,
                HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(@NonNull GraphResponse response) {

           Log.e("MyFbFrag : ", " response get photos from album"+response.toString());
                //converting the response to jsom object
                JSONObject jsonObject = response.getJSONObject();
                JSONArray dataArray = null;
                //fethcing the data in the json object
                dataArray = jsonObject.optJSONArray("data");
                try {
                    //    nextUrlForAlbumPictures = jsonObject.optJSONObject("paging").optString("next");
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                    albumCallback.albumError("No Photos In this Album");
                }
                genericFbImageModelsAL = new ArrayList<>();
                for (int i = 0; i < dataArray.length(); i++) {
                    GenericImageModel genericImageModel = new GenericImageModel();
                    try {
                        genericImageModel.setId(dataArray.getJSONObject(i).optString("id"));
                        genericImageModel.setSource(dataArray.getJSONObject(i).optString("source"));
                        genericFbImageModelsAL.add(genericImageModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                albumCallback.getAlbumImage(genericFbImageModelsAL);

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "created_time,name,id,source");
        // parameters.putString("limit", "15");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }*/
}
