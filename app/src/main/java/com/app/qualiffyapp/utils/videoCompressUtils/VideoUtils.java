package com.app.qualiffyapp.utils.videoCompressUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.provider.MediaStore;

import com.app.qualiffyapp.customViews.customCamera.CustomCameraUtils;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;

public class VideoUtils {
    String filePath;
    private FFmpeg ffmpeg;
    private Context context;
    private IVideoCompressCallback compressCallback;


    public VideoUtils(Context context, IVideoCompressCallback compressCallback) {
        this.context = context;
        this.compressCallback = compressCallback;
        loadFFMpegBinary();
    }

    /**
     * Load FFmpeg binary
     */
    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                ffmpeg = FFmpeg.getInstance(context);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    compressCallback.isCompressSupport(false);
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                    compressCallback.isCompressSupport(true);
                }
            });
        } catch (FFmpegNotSupportedException e) {
            compressCallback.isCompressSupport(false);
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            compressCallback.isCompressSupport(false);
            e.printStackTrace();
        }

    }


    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    compressCallback.isCompressSupport(false);
                })
                .create()
                .show();
    }

    public void getCompressedVideo(String video_path) {

       /* File moviesDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES
        );
        String filePrefix = "compress_video";
        String fileExtn = ".mp4";

        File dest = new File(moviesDir, filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(moviesDir, filePrefix + fileNo + fileExtn);
        }*/

        filePath = CustomCameraUtils.getFile(true).getAbsolutePath();

        String[] complexCommand = {"-y", "-i", video_path, "-preset", "ultrafast", "-r", "16", "-ab", "64000", "-b", "2097k", "-b:v", "1000k", "-acodec", "aac", filePath};
        execFFmpegBinary(complexCommand);
    }

    /**
     * Executing ffmpeg binary
     */
    private void execFFmpegBinary(final String[] command) {

        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
//                    compressCallback.showVideoCompressProgress(false);
                }

                @Override
                public void onSuccess(String s) {
//                    compressCallback.showVideoCompressProgress(false);
                    getVideoThumbnail(filePath);
                }

                @Override
                public void onProgress(String s) {

                }

                @Override
                public void onStart() {
//                    compressCallback.showVideoCompressProgress(true);
                }

                @Override
                public void onFinish() {
//                    compressCallback.showVideoCompressProgress(false);
                }
            });
        } catch (Exception e) {
//            compressCallback.showVideoCompressProgress(false);
            e.printStackTrace();
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    public void getVideoThumbnail(String videoPath) {
        File videoFile = new File(videoPath);
        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
        compressCallback.getVideoFile(bMap, videoFile);
    }

}
