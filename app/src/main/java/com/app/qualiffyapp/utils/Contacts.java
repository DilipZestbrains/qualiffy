package com.app.qualiffyapp.utils;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

import com.app.qualiffyapp.localDatabase.entity.Contact;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Contacts {

    public static ArrayList<Contact> getPhoneContacts(ContentResolver resolver, AppRepository appRepository) {
        try {
            return new FetchContacts(resolver, appRepository).execute().get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {

        }
        return null;
    }

    private static class FetchContacts extends AsyncTask<Void, Void, ArrayList<Contact>> {
        private final ContentResolver contentResolver;
        private final AppRepository repository;
        private final String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY;
        private final String FILTER = DISPLAY_NAME + " NOT LIKE '%@%'";
        private final String ORDER = String.format("%1$s COLLATE NOCASE", DISPLAY_NAME);
        private final String[] PROJECTION = {
                ContactsContract.Contacts._ID,
                DISPLAY_NAME,
                ContactsContract.Contacts.HAS_PHONE_NUMBER
        };

        FetchContacts(ContentResolver contentResolver, AppRepository repository) {
            this.contentResolver = contentResolver;
            this.repository = repository;
        }

        @Override
        protected ArrayList<Contact> doInBackground(Void... params) {
            try {
                ArrayList<Contact> contacts = new ArrayList<>();

                Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, FILTER, null, ORDER);
                if (cursor != null && cursor.moveToFirst()) {

                    do {
                        String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                        int hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));


                        if (hasPhone > 0) {
                            Cursor cp = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                            if (cp != null && cp.moveToFirst()) {
                                do {
                                    String phone = cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                    String pno = null;
                                    if (phone != null) {
                                        pno = phone.replace(" ", "");
                                        pno = pno.replace("+", "");
                                        pno = pno.replace("-", "");
                                        pno = pno.replace("(", "");
                                        pno = pno.replace(")", "");

                                    }
                                     /*if (pno != null && pno.startsWith("+")) {
                                        Phonenumber.PhoneNumber numberProto = PhoneNumberUtil.getInstance().parse(phone, "");
                                        pno = String.valueOf(numberProto.getNationalNumber());
                                    }*/

                                    if (pno != null)
                                        contacts.add(new Contact(null, null, null, null, pno, name, null));

                                } while (cp.moveToNext());

                                cp.close();
                            }
                        }
                    } while (cursor.moveToNext());
                    cursor.close();
                }

                return contacts;
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Contact> contacts) {

        }
    }

}
