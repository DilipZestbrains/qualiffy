package com.app.qualiffyapp.utils.socialMediaIntegration.facebook;

import android.app.Activity;
import android.os.Bundle;

import com.app.qualiffyapp.utils.Utility;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static com.app.qualiffyapp.constants.AppConstants.FACEBOOK;
import static com.app.qualiffyapp.constants.AppConstants.FACEBOOK_PROFILE_ID;


public class FbLoginIntegrationWithoutLoginButton {

    private Activity activity;
    private AccessToken mAccessToken;
    private FacebookResultCallback facebookResultCallback;

    public void fbLogin(Activity context, CallbackManager callbackManager, final FacebookResultCallback facebookResultCallback) {
        this.activity = context;
        this.facebookResultCallback = facebookResultCallback;

//        , "user_friends"  add this permission after reviewing the app

        LoginManager.getInstance().logInWithReadPermissions(context, Arrays.asList("public_profile", "email", "user_photos"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        mAccessToken = loginResult.getAccessToken();
                        getUserProfile(mAccessToken);
//                        getFBFriendsList(mAccessToken);
                    }

                    @Override
                    public void onCancel() {
                        // App code

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        facebookResultCallback.onFbError(exception);
                    }
                });


    }


    private void getUserProfile(AccessToken currentAccessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                currentAccessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        facebookResultCallback.onFbResponse(object);

                        if (activity != null) {
                            Utility.setSocialName(activity, FACEBOOK,
                                    object.optString("first_name"),
                                    object.optString("last_name"),
                                    null);
                            Utility.putStringValueInSharedPreference(activity, FACEBOOK_PROFILE_ID,
                                    object.optString("id"));
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200),first_name,last_name");
        request.setParameters(parameters);
        request.executeAsync();
    }


    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    private void getFBFriendsList(AccessToken accessToken) {
        //fbToken return from login with facebook
        GraphRequestAsyncTask r = GraphRequest.newGraphPathRequest(accessToken,
                "/me/taggable_friends", new GraphRequest.Callback() {

                    @Override
                    public void onCompleted(GraphResponse response) {
                        parseResponse(response.getJSONObject());
                    }
                }

        ).executeAsync();

    }


    private void parseResponse(JSONObject friends) {
        /*List<ContactModel> contactModelList = new ArrayList<>();
        try {
            JSONArray friendsArray = (JSONArray) friends.get("data");
            if (friendsArray != null) {
                for (int i = 0; i < friendsArray.length(); i++) {
                    ContactModel item = new ContactModel();
                    try {

                        item.id = friendsArray.getJSONObject(i).getString(
                                "id");

                        item.name = friendsArray.getJSONObject(i).getString("name");

                        JSONObject picObject = new JSONObject(friendsArray
                                .getJSONObject(i).get("picture") + "");
                        String picURL = (String) (new JSONObject(picObject
                                .get("data").toString())).get("url");
//                        item.setPictureURL(picURL);
                        contactModelList.add(item);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                // facebook use paging if have "next" this mean you still have friends if not start load fbFriends list
                String next = friends.getJSONObject("paging")
                        .getString("next");
                if (next != null) {
                    getFBFriendsList(AccessToken.getCurrentAccessToken());
                } else {
//                    loadFriendsList();
                }
            }
        } catch (JSONException e1) {
//            loadFriendsList();
            e1.printStackTrace();
        }*/
    }


    private void myNewGraphReq(String friendlistId) {
        final String graphPath = "/" + friendlistId + "/members/";
        AccessToken token = AccessToken.getCurrentAccessToken();
        GraphRequest request = new GraphRequest(token, graphPath, null, HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                JSONObject object = graphResponse.getJSONObject();
                try {
                    JSONArray arrayOfUsersInFriendList = object.getJSONArray("data");
                    /* Do something with the user list */
                    /* ex: get first user in list, "name" */
                    JSONObject user = arrayOfUsersInFriendList.getJSONObject(0);
                    String usersName = user.getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle param = new Bundle();
        param.putString("fields", "name");
        request.setParameters(param);
        request.executeAsync();
    }
}
