package com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FACEBOOK_MEDIA;

public class FacebookAlbumActivity extends AppCompatActivity implements AlbumsAdapter.OpponentName, AlbumAdapterGrid.OpponentNamee {

    /**
     * The Global.
     */
    private RelativeLayout mRelativeLayoutListview, mRelativeLayoutAlbum, mRelativeLayoutImage;
    private ListView mListViewAlbums;
    private AlbumsAdapter malbumsAdapter;
    private AlbumAdapterGrid malbumAdapterGrid;
    private GridView mGridViewAlbums;
    private ArrayList<GenericImageModel> genericFbImageModelsAL;
    private String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook_album);
        init();
    }

    private void init() {
        mRelativeLayoutListview = (RelativeLayout) findViewById(R.id.rel_listview);
        mRelativeLayoutAlbum = (RelativeLayout) findViewById(R.id.rel_grid);
        mListViewAlbums = (ListView) findViewById(R.id.videolistview);
        mGridViewAlbums = (GridView) findViewById(R.id.gv_albums);
        mRelativeLayoutImage = (RelativeLayout) findViewById(R.id.rel_image);
        ImageView mfaceBookImage = (ImageView) findViewById(R.id.img_profile);
        ArrayList<AlbumPictureModel> albumPictureModelsAL = (ArrayList<AlbumPictureModel>) getIntent().getSerializableExtra("list");
        malbumsAdapter = new AlbumsAdapter(getApplicationContext(), albumPictureModelsAL, this);
        mListViewAlbums.setAdapter(malbumsAdapter);
    }

    @Override
    public void OpponentFullName(String s) {
        getPhotosBasedOnAlbumId(s);
    }

    private void getPhotosBasedOnAlbumId(String AlbumId) {
        GraphRequest graphRequest = new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + AlbumId + "/photos", null,
                HttpMethod.GET, new GraphRequest.Callback() {
            @Override
            public void onCompleted(@NonNull GraphResponse response) {
                mRelativeLayoutListview.setVisibility(View.GONE);
                mRelativeLayoutAlbum.setVisibility(View.VISIBLE);
                Log.e("MyFbFrag : ", " response get photos from album" + response.toString());
                //converting the response to jsom object
                JSONObject jsonObject = response.getJSONObject();
                JSONArray dataArray = null;
                //fethcing the data in the json object
                dataArray = jsonObject.optJSONArray("data");
                try {
                    //    nextUrlForAlbumPictures = jsonObject.optJSONObject("paging").optString("next");
                } catch (NullPointerException ex) {
                    ex.printStackTrace();
                    Toast.makeText(FacebookAlbumActivity.this, "No Photos In this Album", Toast.LENGTH_SHORT).show();
                }
                genericFbImageModelsAL = new ArrayList<>();
                for (int i = 0; i < dataArray.length(); i++) {
                    GenericImageModel genericImageModel = new GenericImageModel();
                    try {
                        genericImageModel.setId(dataArray.getJSONObject(i).optString("id"));
                        genericImageModel.setSource(dataArray.getJSONObject(i).optString("source"));
                        genericFbImageModelsAL.add(genericImageModel);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                malbumAdapterGrid = new AlbumAdapterGrid(getApplicationContext(), genericFbImageModelsAL, FacebookAlbumActivity.this);
                mGridViewAlbums.setAdapter(malbumAdapterGrid);
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "created_time,name,id,source");
        // parameters.putString("limit", "15");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    @Override
    public void OpponentFullNamee(String s) {
        Log.d("source", s);
        url = s;

        final AlertDialog.Builder builder = new AlertDialog.Builder(FacebookAlbumActivity.this);
        builder.setMessage("Add Image?")
                .setCancelable(true)
                .setPositiveButton("Add",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent();
                                intent.putExtra("facebook", url);
                                setResult(REQUEST_CODE_FACEBOOK_MEDIA, intent);

                                finish();//finishing activity
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(@NonNull DialogInterface dialog,
                                                int id) {
                                dialog.cancel();
                            }
                        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        if (mRelativeLayoutAlbum.getVisibility() == View.VISIBLE) {
            mRelativeLayoutAlbum.setVisibility(View.GONE);
            mRelativeLayoutListview.setVisibility(View.VISIBLE);
        } else if (mRelativeLayoutImage.getVisibility() == View.VISIBLE) {
            mRelativeLayoutImage.setVisibility(View.GONE);
            mRelativeLayoutAlbum.setVisibility(View.VISIBLE);
        } else if (mRelativeLayoutListview.getVisibility() == View.VISIBLE) {
            finish();
        }
    }

}
