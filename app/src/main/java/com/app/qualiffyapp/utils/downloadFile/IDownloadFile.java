package com.app.qualiffyapp.utils.downloadFile;

import java.util.List;

public interface IDownloadFile {
    void getDownloadFile(String filePath);
}
