package com.app.qualiffyapp.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import androidx.fragment.app.FragmentActivity;

import com.greentoad.turtlebody.docpicker.DocPicker;
import com.greentoad.turtlebody.docpicker.core.DocPickerConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class IntentUtil {
    private static IntentUtil instance;

    private IntentUtil() {

    }

    public static IntentUtil getInstance() {
        if (instance == null)
            instance = new IntentUtil();
        return instance;
    }
    public static Intent getPickImageChooserIntent(Context context) {


        Uri outputFileUri = getCaptureImageOutputUri(context);

        ArrayList<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (Objects.requireNonNull(intent.getComponent()).getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

        // Add all other intents
        Intent[] extraIntents = allIntents.toArray(new Intent[0]);
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);

        return chooserIntent;
    }

    public static Uri getCaptureImageOutputUri(Context context) {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    public static Intent getAttatchmentIntent(Context context) {
        ArrayList<Intent> allIntent = new ArrayList<>();
        ArrayList<String> packageName = new ArrayList<>();

        PackageManager packageManager = context.getPackageManager();


        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("*/*");
        List<ResolveInfo> galleryResolveInfo = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo r : galleryResolveInfo) {
            if (!packageName.contains(r.activityInfo.packageName)) {
                Intent intent = new Intent(galleryIntent);
                intent.setComponent(new ComponentName(r.activityInfo.packageName, r.activityInfo.name));
                intent.setPackage(r.activityInfo.packageName);
                packageName.add(r.activityInfo.packageName);
                allIntent.add(intent);
            }
        }
  /*      Intent audioIntent = new Intent(Intent.ACTION_GET_CONTENT);
        audioIntent.setType("audio/*");
        List<ResolveInfo> audioResolveInfo = packageManager.queryIntentActivities(audioIntent, 0);
        for (ResolveInfo r : audioResolveInfo) {
            if (!packageName.contains(r.activityInfo.packageName)) {
                Intent intent = new Intent(audioIntent);
                intent.setComponent(new ComponentName(r.activityInfo.packageName, r.activityInfo.name));
                intent.setPackage(r.activityInfo.packageName);
                allIntent.add(intent);
            }
        }
        Intent videoIntent = new Intent(Intent.ACTION_GET_CONTENT);
        videoIntent.setType("video/*");
        List<ResolveInfo> videoResolveInfo = packageManager.queryIntentActivities(videoIntent, 0);
        for (ResolveInfo r : videoResolveInfo) {

            if (!packageName.contains(r.activityInfo.packageName)) {
                Intent intent = new Intent(videoIntent);
                intent.setComponent(new ComponentName(r.activityInfo.packageName, r.activityInfo.name));
                intent.setPackage(r.activityInfo.packageName);
                allIntent.add(intent);
            }
        }*/
        // Create a chooser from the main  intent
        if (allIntent.size() > 0) {

            Intent mainIntent = allIntent.get(allIntent.size() - 1);
            for (Intent intent : allIntent) {
                if (Objects.requireNonNull(intent.getComponent()).getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                    mainIntent = intent;
                    break;
                }
            }
            allIntent.remove(mainIntent);
            Intent chooserIntent = Intent.createChooser(mainIntent, "Select Media File");

            // Add all other intents
            Intent[] extraIntents = allIntent.toArray(new Intent[0]);
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
            return chooserIntent;
        } else
            return null;
    }

    public static Intent getGalleryIntent(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});

        List<ResolveInfo> resolveInfos = packageManager.queryIntentActivities(intent, 0);
        if (resolveInfos != null) {
            for (ResolveInfo r : resolveInfos) {
                if (r.activityInfo.packageName.contains("gallery")) {
                    intent.setComponent(new ComponentName(r.activityInfo.packageName, r.activityInfo.name));
                    intent.setPackage(r.activityInfo.packageName);

                }
            }
        }
        return intent;
    }

    /* Get uri related content real local file path. */
    public String getUriRealPath(Context ctx, Uri uri) {
        String ret = "";

        if (isAboveKitKat()) {
            // Android OS above sdk version 19.
            ret = getUriRealPathAboveKitkat(ctx, uri);
        } else {
            // Android OS below sdk version 19
            ret = getImageRealPath(ctx.getContentResolver(), uri, null);
        }

        return ret;
    }

    private String getUriRealPathAboveKitkat(Context ctx, Uri uri) {
        String ret = "";
        if (ctx != null && uri != null) {
            if (isDocumentUri(ctx, uri)) {
                String documentId = DocumentsContract.getDocumentId(uri);
                String uriAuthority = uri.getAuthority();

                if (isMediaDoc(uriAuthority)) {
                    String[] idArr = documentId.split(":");
                    if (idArr.length == 2) {
                        // First item is document type.
                        String docType = idArr[0];

                        // Second item is document real id.
                        String realDocId = idArr[1];

                        // Get content uri by document type.
                        Uri mediaContentUri = null;
                        if ("image".equals(docType)) {
                            mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                        } else if ("video".equals(docType)) {
                            mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        } else if ("audio".equals(docType)) {
                            mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        }

                        // Get where clause with real document id.
                        String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;
                        if (mediaContentUri != null)
                            ret = getImageRealPath(ctx.getContentResolver(), mediaContentUri, whereClause);
                        if (ret != null && !TextUtils.isEmpty(ret))
                            return ret;
                    }

                }
                if (isDownloadDoc(uriAuthority)) {
                    // Build download uri.
                    Uri downloadUri = Uri.parse("content://downloads/public_downloads");

                    // Append download document id at uri end.
                    Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

                    ret = getImageRealPath(ctx.getContentResolver(), downloadUriAppendId, null);
                    if (ret != null && !TextUtils.isEmpty(ret))
                        return ret;

                }
                if (isExternalStoreDoc(uriAuthority)) {
                    String[] idArr = documentId.split(":");
                    if (idArr.length == 2) {
                        String type = idArr[0];
                        String realDocId = idArr[1];

                        if ("primary".equalsIgnoreCase(type)) {
                            ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                        }
                    }
                    if (ret != null && !TextUtils.isEmpty(ret))
                        return ret;

                }

            }
            if (isContentUri(uri)) {
                if (isGooglePhotoDoc(uri.getAuthority())) {
                    ret = uri.getLastPathSegment();
                } else {
                    ret = getImageRealPath(ctx.getContentResolver(), uri, null);
                }
                if (ret != null && !TextUtils.isEmpty(ret))
                    return ret;

            }
            if (isFileUri(uri)) {
                ret = uri.getPath();
                if (ret != null && !TextUtils.isEmpty(ret))
                    return ret;

            }
        }

        return ret;
    }

    /* Check whether current android os version is bigger than kitkat or not. */
    private boolean isAboveKitKat() {
        boolean ret = false;
        ret = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        return ret;
    }

    /* Check whether this uri represent a document or not. */
    private boolean isDocumentUri(Context ctx, Uri uri) {
        boolean ret = false;
        if (ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }

    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    private boolean isContentUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("content".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private boolean isFileUri(Uri uri) {
        boolean ret = false;
        if (uri != null) {
            String uriSchema = uri.getScheme();
            if ("file".equalsIgnoreCase(uriSchema)) {
                ret = true;
            }
        }
        return ret;
    }


    /* Check whether this document is provided by ExternalStorageProvider. */
    private boolean isExternalStoreDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.externalstorage.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private boolean isDownloadDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.downloads.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private boolean isMediaDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.android.providers.media.documents".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private boolean isGooglePhotoDoc(String uriAuthority) {
        boolean ret = false;

        if ("com.google.android.apps.photos.content".equals(uriAuthority)) {
            ret = true;
        }

        return ret;
    }

    /* Return uri represented document file real local path.*/
    private String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause) {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if (cursor != null) {
            boolean moveToFirst = cursor.moveToFirst();
            if (moveToFirst) {
                String columnName = null;
                if (cursor.getColumnIndex(MediaStore.Images.Media.DATA) >= 0)
                    columnName = MediaStore.Images.Media.DATA;
                else if (cursor.getColumnIndex(MediaStore.Video.Media.DATA) >= 0)
                    columnName = MediaStore.Video.Media.DATA;
                else if (cursor.getColumnIndex(MediaStore.Audio.Media.DATA) >= 0)
                    columnName = MediaStore.Audio.Media.DATA;

                if (columnName != null)
                    ret = cursor.getString(cursor.getColumnIndex(columnName));
            }
        }
        if (cursor != null)
            cursor.close();
        return ret;
    }

    public static String saveFileFromUri(Uri pdfUri, Activity activity) {
        try {
            InputStream is = activity.getContentResolver().openInputStream(pdfUri);
            byte[] bytesArray = new byte[is.available()];
            int read = is.read(bytesArray);
            //write to sdcard

            File dir = new File(Environment.getExternalStorageDirectory(), "/PRJ");
            boolean mkdirs = dir.mkdirs();
            File myPdf = new
                    File(Environment.getExternalStorageDirectory(), "/PRJ/myPdf.pdf");
            if (read == -1 && mkdirs) {
                return null;
            }
            FileOutputStream fos = new FileOutputStream(myPdf.getPath());
            fos.write(bytesArray);
            fos.close();
            return myPdf.getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void openDocumentPicker(FragmentActivity activity, DocumentPickListener documentPickListener) {
        ArrayList<String> docs = new ArrayList<>();
        docs.add(DocPicker.DocTypes.PDF);
        docs.add(DocPicker.DocTypes.TEXT);
        DocPickerConfig pickerConfig = new DocPickerConfig()
                .setAllowMultiSelection(false)
                .setShowConfirmationDialog(false)
                .setExtArgs(docs);

        DocPicker.with(activity)
                .setConfig(pickerConfig)
                .onResult()
                .subscribe(new Observer<ArrayList<Uri>>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        Log.e("Subscribe", "###########@@@@@@@@@2");
                    }

                    @Override
                    public void onNext(ArrayList<Uri> uris) {
                        if (uris != null & uris.size() > 0)
                            documentPickListener.onDocumentPick(uris.get(0));
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("asda", "" + e.getMessage().toString());
                    }

                    @Override
                    public void onComplete() {
                        Log.e("complete", "@@@@@@@@@@@@@@@@@");
                    }
                });

    }

    public interface DocumentPickListener {
        void onDocumentPick(Uri uri);
    }

}
