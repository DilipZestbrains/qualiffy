package com.app.qualiffyapp.utils;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.AlertCancelCallback;
import com.app.qualiffyapp.callbacks.OkCancelCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.customCamera.CustomCameraUtils;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.firebase.chat.ui.chatMessaging.ChatListActivity;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_ID;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_NAME;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.FACEBOOK_PROFILE_ID;
import static com.app.qualiffyapp.constants.AppConstants.IS_BLOCKED;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.TEMP_MEDIA_FOLDER;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.EXPERIENCE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.HAS_VIDEO;
import static com.app.qualiffyapp.constants.SharedPrefConstants.LAST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PHONE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PROFILE_PIC;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;
import static com.app.qualiffyapp.constants.SharedPrefConstants.USER_SOCIAL_TYPE;
import static java.lang.Double.parseDouble;

@SuppressLint("SimpleDateFormat")
public class Utility {
    public static String EMAIL_PATTERN = "[a-zA-Z0-9]+(?:(\\.|_)[A-Za-z0-9!#$%&'*+/=?^`{|}~-]+)*@(?!([a-zA-Z0-9]*\\.[a-zA-Z0-9]*\\.[a-zA-Z0-9]*\\.))(?:[A-Za-z0-9](?:[a-zA-Z0-9-]*[A-Za-z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?";
    public static String MOBILE_NUMBER_PATTERN = "^[0-9]*$";
    private static String NAME_NUMBER_WITHOUT_SPACE_REGEX = "^[a-zA-Z0-9\\\\s](?=\\S+$).{4,15}$";
    private static AlertDialog mAlertDialog;
    private static Gson gson;

    /**
     * Remove the outer dialog padding so that it takes full screen width
     *
     * @param dialog Instance of the dialog
     */
    public static void modifyDialogBoundsToFill(Dialog dialog) {
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.gravity = Gravity.CENTER;
        lp.width = (int) (dialog.getContext().getResources().getDisplayMetrics().widthPixels * 0.95);
        window.setAttributes(lp);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Gson gsonInstance() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }


    public static boolean checkNames(String text, Context context, String message) {
        Pattern p = Pattern.compile(NAME_NUMBER_WITHOUT_SPACE_REGEX, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(text);
        if (m.matches() && !text.isEmpty()) {
            return true;
        } else {
            showToast(context, message);
            return false;
        }
    }

    /**
     * Method to put string value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Key in which value to store
     * @param value   String value to be stored
     */
    public static void putStringValueInSharedPreference(Context context, String key, String value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public static void putIntValueInSharedPreference(Context context, String key, int value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    /**
     * Method to put long value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Key in which value to store
     * @param value   Long value to be stored
     */
    public static void putLongValueInSharedPreference(Context context, String key, long value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    /**
     * Remove the outer dialog padding so that it takes full screen width
     *
     * @param dialog Instance of the dialog
     */
    public static void modifyDialogBoundsCenter(Dialog dialog) {
        try {
            if (dialog != null) {
                if (dialog.getWindow() != null) {
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                    Window window = dialog.getWindow();
                    if (window.getAttributes() != null) {
                        lp.copyFrom(window.getAttributes());
                    }
                    //This makes the dialog take up the full width
                    DisplayMetrics displayMetrics = dialog.getContext().getResources().getDisplayMetrics();
                    lp.width = (int) (displayMetrics.widthPixels * 0.8);
                    lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                    window.setAttributes(lp);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to put boolean value in shared preference
     *
     * @param context Context of the calling class
     * @param key     Keyif in which value to store
     * @param value   Boolean value to be stored
     */
    public static void putBooleanValueInSharedPreference(Context context, String key, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

    /**
     * Method to get string value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static String getStringSharedPreference(Context context, String param) {
        if (context == null) {
            return "";
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getString(param, "");
    }


    public static int getIntFromSharedPreference(Context context, String param) {
        if (context == null) {
            return 0;
        }
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getInt(param, 0);
    }

    /**
     * Method to get long value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static long getLongSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getLong(param, 0L);
    }

    /**
     * Method to get boolean value from shared preference
     *
     * @param context Context of the calling class
     * @param param   Key from which value is retrieved
     */
    public static boolean getBooleanSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return preferences.getBoolean(param, false);
    }

    public static double round(double value, int places) {
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }


    /**
     * Method to clear shared preference key value
     *
     * @param context Context of the calling class
     * @param key     Key from which value is to be cleared
     */
    public static void clearSharedPrefData(Context context, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    /**
     * Method to clear all shared preference key value
     *
     * @param context Context of the calling class
     */
    public static void clearAllSharedPrefData(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();


    }


    /**
     * Static method to show alert dialog
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialog(Context mContext, String text) {
        if (mContext != null) {
            if (text == null)
                text = "";
            mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                    .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher).setCancelable(false)
                    .setPositiveButton(mContext.getString(R.string.ok), (dialog, which) -> mAlertDialog.dismiss()).create();
            mAlertDialog.show();
            Button button = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
            button.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }
    }

    public static boolean isDateBefore(String date1, String date2) {
        Date d1 = new Date(date1);
        Date d2 = new Date(date2);
        return d1.before(d2);
    }

    /**
     * Static method to show alert dialog
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialog(Context mContext, String text, final OkCancelCallback okCancelCallback) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher).setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCancelCallback != null)
                        okCancelCallback.onOkClicked();
                }).create();

        mAlertDialog.show();
        Button button = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        button.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */
    public static void showAlertDialogWithCallback(Context mContext, String text,
                                                   final OkCancelCallback okCancelCallback) {

        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setNegativeButton(mContext.getString(R.string.cancel), (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCancelCallback != null)
                        okCancelCallback.onCancelClicked();
                }).setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.confirm), (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCancelCallback != null)
                        okCancelCallback.onOkClicked();

                }).create();
        mAlertDialog.show();
        mAlertDialog.show();
        Button nbutton = mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        Button pbutton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext     Context of the calling class
     * @param text         Text to show in toast
     * @param positiveText Text to show on OKAY button
     * @param negativeText Text to show on CANCEL button
     */

    public static void showAlertDialogWithCallbackAndText(Context mContext, String text,
                                                          String title,
                                                          final OkCancelCallback okCancelCallback, String positiveText,
                                                          String negativeText) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(title)
                .setNegativeButton(negativeText, (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCancelCallback != null)
                        okCancelCallback.onCancelClicked();
                }).setCancelable(false)
                .setPositiveButton(positiveText, (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCancelCallback != null)
                        okCancelCallback.onOkClicked();

                }).create();

        mAlertDialog.show();
        Button nbutton = mAlertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(mContext.getResources().getColor(R.color.colorAccent));

        Button pbutton = mAlertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkButton(Context mContext, String text,
                                                          final AlertCancelCallback okCallback, int flag) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCallback != null)
                        okCallback.onAlertClick(flag);

                }).create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkButton(Context mContext, String text,
                                                          final OkCancelCallback okCallback, String positiveText) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(positiveText, (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCallback != null)
                        okCallback.onOkClicked();

                }).create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    /**
     * Static method to show alert dialog with Ok cancel buttons
     *
     * @param mContext Context of the calling class
     * @param text     Text to show in toast
     */

    public static AlertDialog showAlertDialogWithOkCancelButton(Context mContext, String text,
                                                                final OkCancelCallback okCallback,
                                                                String positiveText, String negativeText) {
        if (text == null)
            text = "";
        mAlertDialog = new AlertDialog.Builder(mContext).setMessage(text)
                .setTitle(mContext.getString(R.string.app_name)).setIcon(R.drawable.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(positiveText, (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCallback != null)
                        okCallback.onOkClicked();

                }).setNegativeButton(negativeText, (dialog, which) -> {
                    mAlertDialog.dismiss();
                    if (okCallback != null)
                        okCallback.onCancelClicked();

                })
                .create();

        mAlertDialog.show();
        return mAlertDialog;
    }

    /**
     * This method is depreciated
     * use<code>ApplicationController.getApplicationInstance().isNetworkConnected()</code>
     *
     * @see Utility#getNetworkState(Context context)
     */
    public static boolean isNetworkAvailable(Context context) {
        if (context != null) {
            try {
                ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                return connMgr.getActiveNetworkInfo().getState() == NetworkInfo.State.CONNECTED
                        || connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED
                        || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
                        || connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Static method to check network availability
     *
     * @param context Context of the calling class
     */

    public static boolean getNetworkState(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }


    public static void showKeyboard(Context context, EditText editText) {
        showKeyboard(context);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED);
            imm.showSoftInput(((AppCompatActivity) context).getCurrentFocus(), InputMethodManager.SHOW_FORCED);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to show keyboard
     *
     * @param context Context of the calling activity
     */
    public static void showKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(((AppCompatActivity) context).getCurrentFocus(), InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Method to hide keyboard
     *
     * @param mContext Context of the calling class
     */
    public static void hideKeyboard(Context mContext) {
        try {
            InputMethodManager inputManager = (InputMethodManager) mContext
                    .getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(((AppCompatActivity) mContext).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ignored) {
            Log.e("keyboard", "Hide Keyboard fail");
        }
    }

    /**
     * Method to hide keyboard on view focus
     *
     * @param context    Context of the calling class
     * @param myEditText focussed view
     */
    public static void hideKeyboard(Context context, View myEditText) {
        hideKeyboard(context);
        try {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
        } catch (Exception ignored) {
        }
    }


    public static String getLocalDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getDefault());
        String dateString = formatter.format(new Date(milliSeconds * 1000));
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));
        return dateString;
    }

    //for Chat
    public static long convertDateToMillies(String str, String dateFormat) {
        DateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = null;
        try {
            date = (Date) formatter.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            return (date.getTime() / 1000);
        }
        return 0;
    }

    public static long convertDateToMillie(String str) {
        Date date = null;
        try {
            date = new Date(str);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (date != null) {
            return (date.getTime());
        }
        return 0;
    }

    public static <T> ArrayList<T> removeDuplicates(ArrayList<T> list) {
        Set<T> set = new LinkedHashSet<>(list);
        list.clear();
        list.addAll(set);
        return list;
    }

    public static String getLocalDate2(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(new Date(milliSeconds * 1000));

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);

        calendar.add(Calendar.MILLISECOND, TimeZone.getDefault().getOffset(calendar.getTimeInMillis()));
        return dateString;
    }

    public static String getUtcDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateString = formatter.format(new Date(milliSeconds));

        return dateString;
    }


    /**
     * Method to parse date from server
     *
     * @param date         Date from the server
     * @param sourceFormat KFormatter of the date from server
     * @param targetFormat Target format in which to return the date
     * @return Formatted date
     */
    public static String parseDateTimeUtc(String date, String sourceFormat, String targetFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date strDate = new Date();

        try {
            strDate = sdf.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        SimpleDateFormat sdf2;
        sdf2 = new SimpleDateFormat(targetFormat);
        sdf2.setTimeZone(TimeZone.getDefault());
        return sdf2.format(strDate);
    }

    public static Long getLongFromDate(String date, String sourceFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date strDate = new Date();

        try {
            strDate = sdf.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate.getTime();
    }

    /**
     * Method to parse date from server
     *
     * @param date         Date from the server
     * @param sourceFormat KFormatter of the date from server
     * @return Formatted date
     */
    public static Date parseDateTimeLocal(String date, String sourceFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(sourceFormat);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date strDate = new Date();
        try {
            strDate = sdf.parse(date);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strDate;
    }

    public static String parseDateTimeUtcToGmt(String stringDate) {
        DateFormat formatter;
        Date date = null;
        formatter = new SimpleDateFormat(AppConstants.SERVER_DATE_FORMAT);
        try {
            date = formatter.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //creating DateFormat for converting time from local timezone to GMT
        DateFormat converter = new SimpleDateFormat(AppConstants.SERVER_DATE_FORMAT1);

        //getting GMT timezone, you can get any timezone e.g. UTC
        converter.setTimeZone(TimeZone.getDefault());

        System.out.println("local time : " + date);
        System.out.println("time in Default : " + converter.format(date));
        return converter.format(date);
    }

    public static String getTimeAgo(String timeString, Context ctx, String dateFormat) {

        SimpleDateFormat sourceFormat = new SimpleDateFormat(dateFormat);
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date;
        String timeAgo;
        try {
            date = sourceFormat.parse(timeString);

            TimeZone tz = TimeZone.getDefault();
            SimpleDateFormat destFormat = new SimpleDateFormat(dateFormat);
            destFormat.setTimeZone(tz);

            String result = destFormat.format(date);
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
            Date startDate = formatter.parse(result);

            long time = startDate.getTime();

            int dim = getTimeDistanceInMinutes(time);

            if (dim == 0 || dim == 1) {
                return "now";
            } else if (dim >= 2 && dim <= 44) {
                timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
            } else if (dim >= 45 && dim <= 89) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_hour);
            } else if (dim >= 90 && dim <= 1439) {

                if ((Math.round(dim / 60)) == 1)
                    timeAgo = (Math.round(dim / 60)) + " "
                            + ctx.getResources().getString(R.string.date_util_unit_hour);
                else
                    timeAgo = (Math.round(dim / 60)) + " "
                            + ctx.getResources().getString(R.string.date_util_unit_hours);
            } else if (dim >= 1440 && dim <= 2519) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
            } else if (dim >= 2520 && dim <= 43199) {
                timeAgo = (Math.round(dim / 1440)) + "";
                if (timeAgo.equals("1")) {
                    timeAgo = timeAgo + " " + ctx.getResources().getString(R.string.date_util_unit_day);
                } else {
                    timeAgo = timeAgo + " " + ctx.getResources().getString(R.string.date_util_unit_days);
                }

            } else if (dim >= 43200 && dim <= 86399) {
                timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_month);
            } else if (dim >= 86400 && dim <= 525599) {
                timeAgo = (Math.round(dim / 43200)) + " "
                        + ctx.getResources().getString(R.string.date_util_unit_months);
            } else if (dim >= 525600 && dim <= 655199) {
                timeAgo = "1 "
                        + ctx.getResources().getString(R.string.date_util_unit_year);
            } else if (dim >= 655200 && dim <= 1051199) {
                timeAgo = "2 "
                        + ctx.getResources()
                        .getString(R.string.date_util_unit_year);
            } else {
                timeAgo = (Math.round(dim / 525600))
                        + " "
                        + ctx.getResources().getString(
                        R.string.date_util_unit_years);
            }
        } catch (Exception e) {
            timeAgo = timeString;
            e.printStackTrace();
        }
        return /*ctx.getResources().getString(R.string.date_util_prefix_about) + " " + */timeAgo
                + " " + ctx.getResources().getString(R.string.date_util_suffix);
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        return calendar.getTime();
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static void setImageWithUrl(String thumbUrl, ImageView mNewsIv) {
        Glide.with(mNewsIv.getContext()).load(thumbUrl)
                .into(mNewsIv);
    }

    /**
     * General Method to generate Hash-key for facebook app.
     */
    public static void generateFBKeyHash(Context mContext) {
        try {
            PackageInfo info = mContext.getPackageManager().getPackageInfo("com.app.qualiffyapp", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static long convertStringToLongDouble(String string) {
        if (!TextUtils.isEmpty(string)) {
            return Double.valueOf(string).longValue();
        } else {
            return 0;
        }
    }

    public static double convertStringToDouble(String string) {
        if (!TextUtils.isEmpty(string)) {
            return parseDouble(string);
        } else {
            return 0;
        }
    }

    public static int convertStringToInteger(String string) {
        if (!TextUtils.isEmpty(string)) {
            int x = (int) convertStringToDouble(string);
            return x;
        } else {
            return 0;
        }
    }

    public static void showToast(Context context, String msgRes) {
        if (context != null && msgRes != null && !TextUtils.isEmpty(msgRes))
            onToast(context, msgRes, R.color.colorTextBlack);
    }

    /**
     * Method to check app is in background or not
     *
     * @param context Context
     * @return is app in background
     */
    public static boolean isApplicationInBackground(Context context) {
        final ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            final ComponentName topActivity = tasks.get(0).topActivity;
            return !topActivity.getPackageName().equals(context.getPackageName());
        }
        return false;
    }

    public static String getDateTime(String dateString, String fromDateFormat, String dateFormat) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(fromDateFormat);
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getDefault());
        return simpleDateFormat.format(date);
    }

    public static String getDateTimeGMT(String dateString, String fromDateFormat, String dateFormat) {
        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat(fromDateFormat);
        try {
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            date = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat.format(date);
    }


    public static String getDay(long milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        d.setTime(milliseconds);
        String day = sdf.format(d);
        return day;
    }


    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }

        }
        return dir.delete();
    }

    private static void onToast(Context context, String msg, int color) {
        if (context != null) {
            try {
                Toast toast = new Toast(context);
                toast.setGravity(Gravity.TOP | Gravity.FILL_HORIZONTAL, 0, 0);
                toast.setDuration(Toast.LENGTH_SHORT);
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.custom_toast_layout, null);
                TextView textView = view.findViewById(R.id.tv_custom_toast);
                textView.setText(msg);
                textView.setBackground(context.getResources().getDrawable(color));
                toast.setView(view);
                TranslateAnimation animate = new TranslateAnimation(0, 0, 0, view.getHeight());
//                animate.setDuration(3000);
                animate.setFillAfter(true);
                view.startAnimation(animate);
                toast.show();
            } catch (Exception e) {
            }
        }
    }


    public static String getFirstLetterCaps(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }


    public static String loadJSONFromAsset(InputStream inputStream) {
        String json = null;
        try {
            InputStream is = inputStream;
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    public static Bitmap rotateImage(Bitmap source, int currentCameraId) {
        Matrix matrix = new Matrix();
        if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
            matrix.postRotate(90);
        else
            matrix.postRotate(270);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }


    public static void saveProfile(Context context, VerifyResponseModel verifyResponseModel, boolean isBusinessLogin) {
//        Utility.putStringValueInSharedPreference(context, FIRST_NAME, "");
//        Utility.putStringValueInSharedPreference(context, LAST_NAME, "");
//        Utility.putStringValueInSharedPreference(context, PROFILE_PIC, "");
//        Utility.putStringValueInSharedPreference(context, UID, "");

        if (isBusinessLogin) {
            if (verifyResponseModel.name != null && !verifyResponseModel.name.equals(""))
                Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, verifyResponseModel.name);
            Utility.putIntValueInSharedPreference(context.getApplicationContext(), USER_TYPE, BUSINESS_USER);

        } else {
            Utility.putIntValueInSharedPreference(context.getApplicationContext(), USER_TYPE, JOB_SEEKER);
            Utility.putBooleanValueInSharedPreference(context.getApplicationContext(), HAS_VIDEO, verifyResponseModel.is_video);
            Utility.putStringValueInSharedPreference(context.getApplicationContext(), EXPERIENCE, new Gson().toJson(verifyResponseModel.expInfoBean));

            if (verifyResponseModel.username != null && !verifyResponseModel.username.equals(""))
                Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, verifyResponseModel.username);
            else if (verifyResponseModel.f_name != null && !verifyResponseModel.f_name.equals("")) {
                Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, verifyResponseModel.f_name);
                if (verifyResponseModel.l_name != null && !verifyResponseModel.l_name.equals(""))
                    Utility.putStringValueInSharedPreference(context.getApplicationContext(), LAST_NAME, verifyResponseModel.l_name);
            }

        }

        if (verifyResponseModel.img != null && !verifyResponseModel.img.equals(""))
            Utility.putStringValueInSharedPreference(context.getApplicationContext(), PROFILE_PIC, verifyResponseModel.img);
        saveUserId(verifyResponseModel._id, context.getApplicationContext());
        if (verifyResponseModel.pno != null)
            Utility.putStringValueInSharedPreference(context.getApplicationContext(), PHONE, verifyResponseModel.pno);

    }

    public static void saveUserId(String userId, Context context) {
        if (userId != null && !TextUtils.isEmpty(userId)) {
            Utility.putStringValueInSharedPreference(context.getApplicationContext(), UID, userId);
        }
    }

    public static void setSocialName(Context context, String userType, String fname, String lname, String userName) {
        Utility.putStringValueInSharedPreference(context.getApplicationContext(), USER_SOCIAL_TYPE, userType);
        Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, "");
        Utility.putStringValueInSharedPreference(context.getApplicationContext(), LAST_NAME, "");
        Utility.putStringValueInSharedPreference(context.getApplicationContext(), FACEBOOK_PROFILE_ID, "");

        if (userName == null) {
            if (fname != null)
                Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, fname);
            if (lname != null)
                Utility.putStringValueInSharedPreference(context.getApplicationContext(), LAST_NAME, lname);

        } else {
            Utility.putStringValueInSharedPreference(context.getApplicationContext(), FIRST_NAME, userName);

        }


    }


    public static File getFileFromDevice(String fileName, String mediafolder) {
        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/" + mediafolder + "/");


        if (!dir.exists()) {
            dir.mkdirs();
        }

        File[] files = dir.listFiles();
        if (files != null) {
            for (File f : files) {
                if (f.getName().equals(fileName))
                    return f;
            }

        }
        return null;
    }

    public static void deleteMediaDir() {
        File filepath = Environment.getExternalStorageDirectory();
        File dir = new File(filepath.getAbsolutePath() + "/" + TEMP_MEDIA_FOLDER+ "/");
        CustomCameraUtils.clearDirectory(dir);
    }


    public static String convertTimeStampTODateString(String str, String date_format) {
        String s = str;
        long timeStamp = Long.parseLong(str);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(date_format);
        Date date = new Date(timeStamp);
        s = simpleDateFormat.format(date);

        return s;

    }

    public static String convertDate(String str) {
        String date = "";
        /*Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, Integer.parseInt(str.substring(4,6)));
        System.out.println(calendar.getTime());*/
        date = str.substring(str.length() - 2) + "/" + str.substring(4, 6) + "/" + str.substring(0, 4);

        return date;
    }

    public static String singularPluralText(String str, int count) {
        String s = str;
        if (count > 1)
            s = str + "s";
        return s;
    }

    public static Intent sendMsg(String body) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Qualiffy Download Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        return sharingIntent;

    }

    public static UserConnection getUserConnection(Context context) {
        long time = System.currentTimeMillis();
        UserConnection userConnection = new UserConnection();
        String name = Utility.getStringSharedPreference(context, FIRST_NAME);
        String uid = Utility.getStringSharedPreference(context, UID);
        String img = Utility.getStringSharedPreference(context, PROFILE_PIC);
        String phone = Utility.getStringSharedPreference(context, PHONE);
        int type = Utility.getIntFromSharedPreference(context, USER_TYPE);
        userConnection.setName(name);
        userConnection.setUid(uid);
        userConnection.setImg(img);
        userConnection.setPhone(phone);
        if (type == JOB_SEEKER)
            userConnection.setType(FirebaseUserType.JOB_SEEKER.s);
        else if (type == BUSINESS_USER)
            userConnection.setType(FirebaseUserType.BUSINESS.s);
        userConnection.setBlocked(BlockStatus.NOT_BLOCKED.value);
        userConnection.setTimestamp(String.valueOf(time));
        return userConnection;
    }

    public static void startChatActivity(Context context, UserConnection connection) {
        Bundle bundle = new Bundle();
        bundle.putString(CONNECTION_ID, connection.getUid());
        bundle.putString(CONNECTION_NAME, connection.getName());
        bundle.putString(IS_BLOCKED, connection.getBlocked());
        bundle.putString(CONNECTION_TYPE, connection.getType());
        Intent intent = new Intent(context, ChatListActivity.class);
        intent.putExtra(BUNDLE, bundle);
        context.startActivity(intent);
    }


    public void onIncomingCall(FragmentManager fm) {
//        new IncomingCallBottomSheet().show(getSupportFragmentManager(), null);
    }


    public static String getExpereince(List<ExpDetailBean> expLIST) {
        String str = "";
        for (ExpDetailBean exp : expLIST) {

            if (!exp.noJobHist) {
                if (str.equals(""))
                    str = "\u2022" + exp.title + "\t\t\t" + (TextUtils.isEmpty(exp.from) ? "" : (" (" + exp.from + " - " + (exp.isWrkng ? "Current" : exp.to)) + ")\n\t" + (TextUtils.isEmpty(exp.cName) ? "" : exp.cName.toUpperCase())) + "\n";
                else
                    str = str + "\u2022" + exp.title + "\t\t\t" + (TextUtils.isEmpty(exp.from) ? "" : (" (" + exp.from + " - " + (exp.isWrkng ? "Current" : exp.to)) + ")\n\t" + (TextUtils.isEmpty(exp.cName) ? "" : exp.cName.toUpperCase())) + "\n";
            } else str = "\u2022" +exp.title+"\n";

        }
        return str;

    }




}