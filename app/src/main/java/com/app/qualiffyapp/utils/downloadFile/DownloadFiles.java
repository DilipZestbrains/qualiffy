package com.app.qualiffyapp.utils.downloadFile;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFiles extends AsyncTask<String, String, String> {

    //    private ProgressDialog progressDialog;
    private String fileName;
    private String folder;
    private String medaFolder;
    private IDownloadFile downloadFile;


    public DownloadFiles(String fileName, String mediaFolder) {
        this.fileName = fileName;
        this.medaFolder = mediaFolder;
    }

    public DownloadFiles(String fileName, String mediaFolder, IDownloadFile downloadFile) {
        this.fileName = fileName;
        this.medaFolder = mediaFolder;
        this.downloadFile = downloadFile;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    /*    this.progressDialog = new ProgressDialog(MainActivity.this);
        this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        this.progressDialog.setCancelable(false);
        this.progressDialog.show();*/
    }

    /**
     * Downloading file in background thread
     */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(f_url[0]);
            URLConnection connection = url.openConnection();
            connection.connect();
            int lengthOfFile = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            folder = Environment.getExternalStorageDirectory() + "/" + medaFolder + "/";

            File directory = new File(folder);

            if (!directory.exists()) {
                directory.mkdirs();
            }
            OutputStream output = new FileOutputStream(folder + fileName);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
//                publishProgress("" + (int) ((total * 100) / lengthOfFile));
                Log.d("DownloadFiles", "Progress: " + (int) ((total * 100) / lengthOfFile));

                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
            return folder + fileName;

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return "Something went wrong";
    }

    /**
     * Updating progress bar
     */
    protected void onProgressUpdate(String... progress) {

        // setting progress percentage
//        progressDialog.setProgress(Integer.parseInt(progress[0]));
    }


    @Override
    protected void onPostExecute(String filePath) {
        Log.d("DownloadFiles", filePath);
        if (downloadFile != null)
            downloadFile.getDownloadFile(filePath);

    }


}

