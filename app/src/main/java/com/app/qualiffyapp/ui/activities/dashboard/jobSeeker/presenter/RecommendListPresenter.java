package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.RecommendResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_BADGE_LIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_FOLLOWERS;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_RECOMMENDER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_UNRECOMMENDER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_JOB_RECOMMEND_LIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_USER_CONNECTIONS;


public class RecommendListPresenter implements ApiListener<ResponseModel<RecommendResponseModel>> {
    private SignupView signupView;
    private SignUpInteractor signUpInteractor;

    public RecommendListPresenter(SignupView signupView) {
        this.signupView = signupView;
        this.signUpInteractor = new SignUpInteractor();
    }


    public void hitApi(RequestBean requestModel, int PageNo, boolean isShowProgress) {
        if (requestModel != null && requestModel.orgid != null && requestModel.isRecommend != null) {
            if (isShowProgress)
                signupView.showProgressDialog(true);
            signUpInteractor.jobsSeekerRecommendJobListApi(this, requestModel.isRecommend.toString(), requestModel.orgid, PageNo, API_FLAG_JS_JOB_RECOMMEND_LIST);
        }


    }

    public void getOrganisationFollower(int pageNo, boolean isShowProgress) {
        if (isShowProgress)
            signupView.showProgressDialog(true);
        signUpInteractor.getOrganisationFollowers(this, pageNo, API_FLAG_BUSINESS_FOLLOWERS);
    }

    public void getUserConnection(int pageNo) {
        signupView.showProgressDialog(true);
        signUpInteractor.getUserConnection(this, pageNo, API_FLAG_USER_CONNECTIONS);
    }

    @Override
    public void onApiSuccess(ResponseModel<RecommendResponseModel> response, int apiFlag) {
        if (response != null)
            signupView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.apiError(error, apiFlag);
    }

    @Override
    public void onApiNoResponse(int apiFlag) {
        signupView.apiError(null, apiFlag);
    }

    public void getOrganisationRecommender(int pageNo, boolean isShowProgress) {
        if (isShowProgress)
            signupView.showProgressDialog(true);
        signUpInteractor.getOrganisationRecommendation(this, pageNo, API_FLAG_BUSINESS_RECOMMENDER);
    }

    public void getOrgNotRecommender(int pageNo, boolean isShowProgress) {
        if (isShowProgress)
            signupView.showProgressDialog(true);
        signUpInteractor.getOrgUnRecommendation(this, pageNo, API_FLAG_BUSINESS_UNRECOMMENDER);
    }

    public void getOrgDashBoardRecommender(String userId, int pageNo, boolean isShowProgress) {
        if (isShowProgress)
            signupView.showProgressDialog(true);
        signUpInteractor.getOrgDashboardRecommendationList(this, pageNo, userId, API_FLAG_BUSINESS_DASHBOARD_BADGE_LIST);
    }
}
