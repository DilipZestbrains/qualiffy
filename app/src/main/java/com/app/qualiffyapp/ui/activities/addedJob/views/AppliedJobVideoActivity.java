package com.app.qualiffyapp.ui.activities.addedJob.views;

import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.slidingPanel.SlidingUpPanelLayout;
import com.app.qualiffyapp.firebase.Call;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsProfileResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.storyview.StoriesData;
import com.app.qualiffyapp.ui.activities.addedJob.presenter.ApplicantsProfilePresenter;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.downloadFile.DownloadFiles;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import jp.shts.android.storiesprogressview.StoriesProgressView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_JOB_APPLICANTS_PROFILE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_FOLDER;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;

public class AppliedJobVideoActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener, SignupView {

    long pressTime = 0L;
    long limit = 500L;
    private StoriesProgressView storiesProgressView;
    private ProgressBar mProgressBar;
    private LinearLayout mVideoViewLayout;
    private LinearLayout detailLlayout;
    private ImageView userImg;
    private ImageView ivChat;
    private TextView tvUserName;
    private TextView tvExperience;
    private TextView tvExperienceDuration;
    private TextView tvEducation;
    private TextView tvSkills;
    private ImageView ivCall;
    private ImageView ivVideo;
    private int counter = 0;
    private ArrayList<StoriesData> mStoriesList = new ArrayList<>();
    private ArrayList<View> mediaPlayerArrayList = new ArrayList<>();
    private View reverse;
    private View skip;
    private SlidingUpPanelLayout slidingLayout;
    private ProgressBar progress;
    private ApplicantsProfilePresenter mPresenter;
    private HashMap<String, ApplicantsProfileResponseModel> profielMap;
    private DownloadFiles downloadFiles;
    private MediaPlayer mediaPlayerView;
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
//                    if (((VideoView) mVideoViewLayout.getChildAt(counter)) != null)
//                        if (mVideoViewLayout.getChildAt(counter) instanceof VideoView) {
//                            ((VideoView) mVideoViewLayout.getChildAt(counter)).pause();
//                        }
                    if (mediaPlayerView != null) {
                        mediaPlayerView.pause();
                    }
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
//                    if (((VideoView) mVideoViewLayout.getChildAt(counter)) != null)
//                        if (mVideoViewLayout.getChildAt(counter) instanceof VideoView) {
//                            ((VideoView) mVideoViewLayout.getChildAt(counter)).resume();
//                        }
                    storiesProgressView.resume();
                    if (mediaPlayerView != null) {
                        mediaPlayerView.start();
                    }
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_applied_job_video);
        mPresenter = new ApplicantsProfilePresenter(this);
        getIntentData();
        mProgressBar = findViewById(R.id.progressBar);
        mVideoViewLayout = findViewById(R.id.videoView);
        reverse = findViewById(R.id.reverse);
        skip = findViewById(R.id.skip);
        ivChat = findViewById(R.id.ivChat);
        ivCall = findViewById(R.id.ivCall);
        ivVideo = findViewById(R.id.ivVideo);
        userImg = findViewById(R.id.userImg);
        tvUserName = findViewById(R.id.tvUserName);
        detailLlayout = findViewById(R.id.detailLlayout);
        tvSkills = findViewById(R.id.tvSkills);
        tvEducation = findViewById(R.id.tvEducation);
        tvExperience = findViewById(R.id.tvExperience);
        tvExperienceDuration = findViewById(R.id.tvExperienceDuration);
        slidingLayout = findViewById(R.id.slidingLayout);
        progress = findViewById(R.id.progress);
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);

        if (mStoriesList != null) {
            storiesProgressView.setStoriesCount(mStoriesList.size());
        } else {
            finish();
        }

        storiesProgressView.setStoriesListener(this);
        for (int i = 0; i < mStoriesList.size(); i++) {
            if (mStoriesList.get(i).mimeType.contains("video")) {
                mediaPlayerArrayList.add(getVideoView(i));
            } else if (mStoriesList.get(i).mimeType.contains("image")) {
                mediaPlayerArrayList.add(getImageView(i));
            }
        }

        setStoryView(counter);
        setlisteners();
    }

    private void setlisteners() {
        reverse.setOnClickListener(v -> storiesProgressView.reverse());
        reverse.setOnTouchListener(onTouchListener);
        skip.setOnClickListener(v -> storiesProgressView.skip());
        findViewById(R.id.ivSliding).setOnClickListener(v -> {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
        });
        skip.setOnTouchListener(onTouchListener);
        ivChat.setOnClickListener(v -> openChatActivity());
        ivCall.setOnClickListener(v -> openAudioCall());
        ivVideo.setOnClickListener(v -> openVideoCall());

        slidingLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View view, float v) {
            }

            @Override
            public void onPanelCollapsed(View view) {
                tvExperience.setText("N/A");
                tvEducation.setText("N/A");
                tvSkills.setText("N/A");
                storiesProgressView.resume();
                if (mediaPlayerView != null) {
                    mediaPlayerView.start();
                }
//                if (mVideoViewLayout.getChildAt(counter) instanceof VideoView) {
//                    VideoView videoView = (VideoView) mVideoViewLayout.getChildAt(counter);
//                    videoView.seekTo(videoLastPausePos);
//                    videoView.resume();
//
//                }
            }

            @Override
            public void onPanelExpanded(View view) {
                if (mStoriesList.get(counter).userId != null) {
                    storiesProgressView.pause();
                    if (mediaPlayerView != null) {
                        if (mediaPlayerView.isPlaying())
                            mediaPlayerView.pause();
                    }
                    if (profielMap != null && profielMap.containsKey(mStoriesList.get(counter).userId)) {
                        setUserOtherDetails(profielMap.get(mStoriesList.get(counter).userId));
                    } else {
                        detailLlayout.setVisibility(View.GONE);
                        mPresenter.getApplicantProfileList(mStoriesList.get(counter).userId);
                        mPresenter.addViewCount(mStoriesList.get(counter).userId);
                    }
//                    if (mVideoViewLayout.getChildAt(counter) instanceof VideoView) {
//                        VideoView videoView = (VideoView) mVideoViewLayout.getChildAt(counter);
//                        if (videoView.isPlaying()) {
//                            videoLastPausePos = videoView.getCurrentPosition();
//                            videoView.pause();
//                        }
//                    }
                }
            }

            @Override
            public void onPanelAnchored(View view) {
            }

            @Override
            public void onPanelHidden(View view) {

            }
        });
    }

    private void openAudioCall() {
        if (mStoriesList.get(counter).userId != null) {
            AppFirebaseDatabase.getInstance().getUserConnection(Utility.getStringSharedPreference(this, UID), mStoriesList.get(counter).userId, connection -> {
               if(connection!=null)
                Call.getInstance((status, msg) -> {
                }).makeAudioCall(getViewContext(), connection);
            });
        }
    }


    private void openVideoCall() {
        if (mStoriesList.get(counter).userId != null) {
            AppFirebaseDatabase.getInstance().getUserConnection(Utility.getStringSharedPreference(this, UID), mStoriesList.get(counter).userId, connection -> {
                if(connection!=null)
                Call.getInstance((status, msg) -> {
                }).makeVideoCall(getViewContext(), connection);
            });

        }
    }

    private void openChatActivity() {
        if (mStoriesList.get(counter).userId != null) {
                AppFirebaseDatabase.getInstance().getUserConnection(Utility.getStringSharedPreference(this, UID), mStoriesList.get(counter).userId, connection -> {
                   if(connection!=null)
                    Utility.startChatActivity(getViewContext(), connection);
                });

        }
    }

    private void setUserOtherDetails(ApplicantsProfileResponseModel applicantsProfileResponseModel) {
        tvEducation.setText(mPresenter.setEducation(applicantsProfileResponseModel.prfl.edu));
        tvExperienceDuration.setText(mPresenter.setExpDuration(applicantsProfileResponseModel.expInfo));
        ApplicantsProfileResponseModel.PrflBean prflBean = applicantsProfileResponseModel.prfl;
        tvSkills.setText(mPresenter.setSkills(prflBean.skills));

        if (applicantsProfileResponseModel.expInfo != null && applicantsProfileResponseModel.expInfo.exps != null && applicantsProfileResponseModel.expInfo.exps.size() > 0) {
            for (ExpDetailBean exp : applicantsProfileResponseModel.expInfo.exps) {
                String expStr = exp.title;
                if (!exp.noJobHist)
                    expStr = expStr + (TextUtils.isEmpty(exp.cName) ? "" : (" in " + exp.cName)) + (TextUtils.isEmpty(exp.from) ? "" : (" (" + exp.from + "-" + (exp.isWrkng ? "Current" : exp.to)) + (TextUtils.isEmpty(exp.desc) ? "" : ("\n" + exp.desc)));

                tvExperience.setText("\u2022 " + expStr + "\n");
            }
        }
        detailLlayout.setVisibility(View.VISIBLE);
    }

    private void getIntentData() {
        ApplicantsResponseModel applicants;
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        counter = bundle.getInt("pos", 0);
        String data = bundle.getString("data", null);
        if (data != null) {
            applicants = new Gson().fromJson(data, ApplicantsResponseModel.class);
            for (ApplicantsResponseModel.UserBean user : applicants.users) {
                String url = applicants.jP + "" + user.video;
                String userName = "";
                if (user.userName != null && !user.userName.equals(""))
                    userName = user.userName;
                else userName = user.fName + " " + user.lName;

                File file = Utility.getFileFromDevice(user.userId + "_" + user.id + ".mp4", VIDEO_FOLDER);
                if (file == null) {
                    downloadFiles = new DownloadFiles(user.userId + "_" + user.id + ".mp4", VIDEO_FOLDER);
                    downloadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);

                    String mimeType = Utility.getMimeType(url);
                    mStoriesList.add(new StoriesData(url,
                            mimeType,
                            userName,
                            applicants.bP + user.img,
                            user.userId, user.id));
                } else {
                    String mimeType = Utility.getMimeType(file.getAbsolutePath());
                    mStoriesList.add(new StoriesData(file.getAbsolutePath(),
                            mimeType,
                            userName,
                            applicants.bP + user.img,
                            user.userId, user.id));
                }

            }
        }

    }


    private void setStoryView(final int counter) {
        setUserProfile(counter);
        final View view = (View) mediaPlayerArrayList.get(counter);
        mVideoViewLayout.addView(view);
        if (view instanceof VideoView) {
            final VideoView video = (VideoView) view;
            File file = Utility.getFileFromDevice(mStoriesList.get(counter).userId + "_" + mStoriesList.get(counter).jobId + ".mp4", VIDEO_FOLDER);
            if (file == null)
                video.setVideoPath(mStoriesList.get(counter).mediaUrl);
            else video.setVideoPath(file.getAbsolutePath());

            video.setOnPreparedListener(mediaPlayer -> {
                mediaPlayerView = mediaPlayer;
                mediaPlayer.setOnInfoListener((mediaPlayer1, i, i1) -> {

                    switch (i) {
                        case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                            mProgressBar.setVisibility(View.GONE);
                            storiesProgressView.resume();
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;

                        }
                        case MediaPlayer.MEDIA_ERROR_TIMED_OUT: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }

                        case MediaPlayer.MEDIA_INFO_AUDIO_NOT_PLAYING: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }
                    }
                    return false;
                });
                video.start();
                mProgressBar.setVisibility(View.GONE);
                storiesProgressView.setStoryDuration(mediaPlayer.getDuration());
                storiesProgressView.startStories(counter);
            });
        } else if (view instanceof ImageView) {
            final ImageView image = (ImageView) view;
            mProgressBar.setVisibility(View.GONE);
            Glide.with(this).load(mStoriesList.get(counter).mediaUrl).addListener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    Toast.makeText(AppliedJobVideoActivity.this, "Failed to load media...", Toast.LENGTH_SHORT).show();
                    mProgressBar.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    mProgressBar.setVisibility(View.GONE);
                    storiesProgressView.setStoryDuration(5000);
                    storiesProgressView.startStories(counter);
                    return false;
                }
            }).into(image);
        }
    }


    private void setUserProfile(int pos) {
        GlideUtil.loadCircularImage(userImg, mStoriesList.get(pos).userImg, R.drawable.user_placeholder);
        tvUserName.setText(mStoriesList.get(pos).userName);
    }


    private VideoView getVideoView(int position) {

        final VideoView video = new VideoView(this);
//        video.setVideoPath(mStoriesList.get(position).mediaUrl);
        video.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return video;
    }

    private ImageView getImageView(int position) {
        final ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return imageView;
    }

    @Override
    public void onNext() {
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        storiesProgressView.destroy();
        mVideoViewLayout.removeAllViews();
        mProgressBar.setVisibility(View.VISIBLE);
        setStoryView(++counter);

    }

    @Override
    public void onPrev() {
        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
        if ((counter - 1) < 0) return;
        storiesProgressView.destroy();
        mVideoViewLayout.removeAllViews();
        mProgressBar.setVisibility(View.VISIBLE);
        setStoryView(--counter);
    }

    @Override
    public void onComplete() {
        finish();
    }

    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_BUSINESS_JOB_APPLICANTS_PROFILE:
                showProgressDialog(false);
                if (response.getStatus() == SUCCESS) {
                    ApplicantsProfileResponseModel responseModel = (ApplicantsProfileResponseModel) response.getRes();
                    if (responseModel != null) {
                        if (profielMap == null)
                            profielMap = new HashMap<>();
                        profielMap.put(responseModel.usr.id, responseModel);
                        setUserOtherDetails(responseModel);
                    }

                } else validationFailed(response.err.msg);
                break;
        }

    }


    @Override
    public void apiError(String error, int apiFlag) {
        if (apiFlag == API_FLAG_BUSINESS_JOB_APPLICANTS_PROFILE)
            showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }


    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        if (showProgress)
            progress.setVisibility(View.VISIBLE);
        else
            progress.setVisibility(View.GONE);
    }
}
