package com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters;

import com.app.qualiffyapp.models.payment.PaymentHistoryResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.PaymentHistoryInterface;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FETCH_PAYMENTS;

public class PaymentHistoryPresenter {
    private PaymentHistoryInterface mInterface;
    private SignUpInteractor interactor;
    private ApiListener<PaymentHistoryResponse> paymentHistoryResponseApiListener = new ApiListener<PaymentHistoryResponse>() {
        @Override
        public void onApiSuccess(PaymentHistoryResponse response, int apiFlag) {
            mInterface.onPaymentFetch(response, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            mInterface.onPaymentFetch(null, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            mInterface.onPaymentFetch(null, null);
        }
    };

    public PaymentHistoryPresenter(PaymentHistoryInterface mInterface) {
        this.mInterface = mInterface;
        interactor = new SignUpInteractor();
    }

    public void getPayments() {
        interactor.getPaymentList(paymentHistoryResponseApiListener, API_FETCH_PAYMENTS);

    }
}
