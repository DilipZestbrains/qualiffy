package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IBusinessLoginFragment extends BaseInterface {


    void OnLogin(String msg, SingupResponse model);

    void OnEmailLogin(String msg, VerifyResponseModel model);
void preLogin();

//    void manageFooterVisibility(Boolean show);


}