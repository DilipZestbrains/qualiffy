package com.app.qualiffyapp.ui.activities.profile.otherUser;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.PopupMenu;

import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityOtherProfileBinding;
import com.app.qualiffyapp.firebase.Call;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.profile.OtherUserProfileResponseModel;
import com.app.qualiffyapp.ui.activities.contacts.ContactPresenter;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ACCEPT_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BLOCK_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_OTHER_USER_BADGE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OTHER_USER_BADGE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OTHER_USER_PROFILE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_REJECT_FRND;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_ACCEPTED;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_NOT_SEND;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_SEND_BY_YOU;

public class OtherUserProfile extends BaseActivity implements SignupView {
    private ActivityOtherProfileBinding mBinding;
    private OtherUserPresenter mPresenter;
    private ContactPresenter contactPresenter;
    private AppRepository appRepository;
    private String userId;
    private String userName;
    private PopupMenu popupMenu;
    private String blockkey;
    private UserConnection friendConnection;
    private UserConnection connection;
    private AppFirebaseDatabase appFirebaseDatabase;
    private boolean badge = false;

    private PopupMenu.OnMenuItemClickListener menuListener = item -> {
        switch (item.getItemId()) {
            case R.id.menuBlock:
                if (blockkey != null) {
                    if (blockkey.equals("0"))
                        mPresenter.hitBockAPI(userId, "1", API_FLAG_BLOCK_FRND);
                    else
                        mPresenter.hitBockAPI(userId, "0", API_FLAG_BLOCK_FRND);
                }
                break;
        }
        return true;

    };

    @Override
    protected void initView() {
        mBinding = (ActivityOtherProfileBinding) viewDataBinding;
        mPresenter = new OtherUserPresenter(this);
        contactPresenter = new ContactPresenter(this);
        appRepository = AppRepository.getInstance(this);
        initFirebase();
        getIntentData();
        toolbar();

    }

    private void initFirebase() {
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
    }

    private void toolbar() {
        mBinding.header.backBtnImg.setOnClickListener(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        userId = bundle.getString(USER_ID);
        mPresenter.getOtherProfile(userId, Utility.getIntFromSharedPreference(this, USER_TYPE));
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.ivChat.setOnClickListener(this);
        mBinding.ivVideo.setOnClickListener(this);
        mBinding.ivVoiceCall.setOnClickListener(this);
        mBinding.header.ivMenu.setOnClickListener(this);
        mBinding.tvAccept.setOnClickListener(this);
        mBinding.tvReject.setOnClickListener(this);
        mBinding.badgeLayout.setOnClickListener(this);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_other_profile;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.badgeLayout:
                onClickBadge();
                break;

            case R.id.ivChat:
                if (userId != null && userName != null) {
                    if (connection == null) {
                        appFirebaseDatabase.getUserConnection(Utility.getStringSharedPreference(this, UID), userId, connection -> {
                            if (connection != null) {
                                this.connection = connection;
                                Utility.startChatActivity(getViewContext(), connection);
                            } else
                                Utility.showToast(this, "Please add this user to start chatting.");
                        });
                    } else {
                        Utility.startChatActivity(getViewContext(), connection);
                    }
                }
                break;
            case R.id.ivMenu:
                popupMenu.show();

                break;
            case R.id.ivVideo:
                if (userId != null && userName != null) {
                    if (connection == null) {
                        appFirebaseDatabase.getUserConnection(Utility.getStringSharedPreference(this, UID), userId, connection -> {
                            if (connection != null) {
                                this.connection = connection;
                                Call.getInstance((status, msg) -> {

                                }).makeVideoCall(getViewContext(), connection);
                            } else
                                Utility.showToast(this, "Please add this user to start an video call.");
                        });
                    } else {
                        Call.getInstance((status, msg) -> {

                        }).makeVideoCall(getViewContext(), connection);
                    }
                }
                break;
            case R.id.ivVoiceCall:
                if (userId != null && userName != null) {
                    if (connection == null) {
                        appFirebaseDatabase.getUserConnection(Utility.getStringSharedPreference(this, UID), userId, connection -> {
                            if (connection != null) {
                                this.connection = connection;
                                Call.getInstance((status, msg) -> {

                                }).makeAudioCall(getViewContext(), connection);
                            } else
                                Utility.showToast(this, "Please add this user to start an audio call.");
                        });
                    } else {
                        Call.getInstance((status, msg) -> {

                        }).makeAudioCall(getViewContext(), connection);
                    }
                }
                break;
            case R.id.tvAccept:
                contactPresenter.hitFrndAPI(userId, API_FLAG_ACCEPT_FRND);
                break;
            case R.id.tvReject:
                contactPresenter.hitFrndAPI(userId, API_FLAG_REJECT_FRND);

                break;

        }
    }


    private void showMenu(View view) {
        if (popupMenu == null) {
            popupMenu = new PopupMenu(this, view);
            MenuInflater inflater = popupMenu.getMenuInflater();
            Menu menuOpts = popupMenu.getMenu();
            inflater.inflate(R.menu.other_user_menu, menuOpts);
            setPopupTitles();
            popupMenu.setOnMenuItemClickListener(menuListener);
        }
    }


    private void setPopupTitles() {
        Menu menuOpts = popupMenu.getMenu();
        if (blockkey != null && blockkey.equals("0"))
            menuOpts.getItem(0).setTitle("Block");
        else if (blockkey != null && blockkey.equals("1"))
            menuOpts.getItem(0).setTitle("Unblock");

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgress(false);
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_ADD_FRND:
                    manageRequest(REQUEST_SEND_BY_YOU.value, null);
                    break;
                case API_FLAG_BUSINESS_OTHER_USER_BADGE:
                case API_FLAG_JS_OTHER_USER_BADGE: {
                    MsgResponseModel msgResponse = (MsgResponseModel) response.getRes();
                    Utility.showToast(this, msgResponse.msg);
                    badge = !badge;
                    manageBadge(badge);
                }

                break;
                case API_FLAG_JS_OTHER_USER_PROFILE:
                    OtherUserProfileResponseModel responseModel = (OtherUserProfileResponseModel) response.getRes();
                    if (responseModel != null) {
                        badge = responseModel.is_bdge;
                        manageBadge(badge);
                        manageRequest(String.valueOf(responseModel.isFriend), responseModel.is_block);
                        userName = responseModel.usr.username;
                        if (!userName.equals(""))
                            mBinding.tvUserName.setText(responseModel.usr.username);
                        else
                            mBinding.tvUserName.setText(responseModel.prfl.fName + " " + responseModel.prfl.lName);
                        mBinding.tvUserAdd.setText(responseModel.prfl.state);
                 /*       if (responseModel.is_block != null) {
                            blockkey = responseModel.is_block;
                            if (blockkey.equals("2"))
                                mBinding.header.ivMenu.setVisibility(View.GONE);
                            else mBinding.header.ivMenu.setVisibility(View.VISIBLE);
                        }
*/
                        if (responseModel.isFriend == 2 && Utility.getIntFromSharedPreference(this, USER_TYPE) == JOB_SEEKER) {
                            mBinding.frndAcceptRejectLlayout.setVisibility(View.VISIBLE);
                            friendConnection = mPresenter.getFireBaseConnection(responseModel);
                        }

                        GlideUtil.loadCircularImage(mBinding.ivUserImg, responseModel.bP + responseModel.prfl.img, R.drawable.user_placeholder);
                        if (responseModel.expInfo != null && responseModel.expInfo.exps != null && responseModel.expInfo.exps.size() > 0) {
                            mBinding.expLlayout.setVisibility(View.VISIBLE);
                            mBinding.tvExp.setText(Utility.getExpereince(responseModel.expInfo.exps));
                        }



                        if (responseModel.prfl.lngs != null && responseModel.prfl.lngs.size() > 0) {
                            mBinding.langLlayout.setVisibility(View.VISIBLE);
                            String lang = "";
                            for (String lng : responseModel.prfl.lngs)
                                lang = lang + lng;
                            mBinding.tvLang.setText(lang);

                        }
                        if (responseModel.prfl.edu != null && responseModel.prfl.edu.size() > 0) {
                            mBinding.eduLlayout.setVisibility(View.VISIBLE);
                            mBinding.tvEducation.setText(mPresenter.setEducation(responseModel.prfl.edu));
                        }
                        if (responseModel.prfl.skills != null && responseModel.prfl.skills.size() > 0) {
                            mBinding.skillLlayout.setVisibility(View.VISIBLE);
                            mBinding.tvSkills.setText(mPresenter.setSkill(responseModel.prfl.skills));
                        }

                        if (Utility.getIntFromSharedPreference(this, USER_TYPE) == JOB_SEEKER)
                            showMenu(mBinding.header.ivMenu);
                        else mBinding.badgeLayout.setVisibility(View.VISIBLE);


                    }

                    break;
                case API_FLAG_BLOCK_FRND:
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    if (blockkey != null && blockkey.equals("0"))
                        blockkey = "1";
                    else blockkey = "0";
                    appFirebaseDatabase.updateBlockStatus(userId, blockkey);
                    setPopupTitles();
                    if (msgResponseModel != null)
                        validationFailed(msgResponseModel.msg);

                    break;

                case API_FLAG_ACCEPT_FRND:
                    if (friendConnection != null)
                        appRepository.updateContactStatus(REQUEST_ACCEPTED.value, friendConnection.getUid());
                    UserConnection user = Utility.getUserConnection(this);
                    appFirebaseDatabase.addFriend(user, friendConnection);
                    friendConnection = null;
                    mBinding.frndAcceptRejectLlayout.setVisibility(View.GONE);

                    break;
                case API_FLAG_REJECT_FRND:
                    if (friendConnection != null)
                        appRepository.updateContactStatus(REQUEST_NOT_SEND.value, friendConnection.getUid());
                    friendConnection = null;
                    mBinding.frndAcceptRejectLlayout.setVisibility(View.GONE);

                    break;


            }

        } else validationFailed(response.err.msg);

    }

    private void manageBadge(boolean badge) {
        if (badge) {
            mBinding.ivBadge.setActivated(true);
            mBinding.ivBadge.setImageResource(R.drawable.ic_recommended);
//            mBinding.tvBadge.setText(getResources().getString(R.string.recommended));
        } else {
            mBinding.ivBadge.setImageResource(R.drawable.ic_recommend_profile);
//            mBinding.tvBadge.setText(getResources().getString(R.string.recomend));
            mBinding.ivBadge.setActivated(false);
        }
    }

    private void onClickBadge() {
        int userType = Utility.getIntFromSharedPreference(this, USER_TYPE);
        if (mBinding.ivBadge.isActivated()) {
            mPresenter.hitBadgeApi(userId, userType, 0);//0 means unrecommend
        } else mPresenter.hitBadgeApi(userId, userType, 1);//1 means recommend
    }

    private void manageRequest(String isFriend, String blockKey) {
        if (Utility.getIntFromSharedPreference(this, USER_TYPE) == BUSINESS_USER) {
            mBinding.header.ivSendRequest.setVisibility(View.GONE);
            mBinding.header.ivMenu.setVisibility(View.GONE);
            return;

        }

        if (isFriend.equals(REQUEST_NOT_SEND.value)) {
            mBinding.header.ivSendRequest.setVisibility(View.VISIBLE);
            mBinding.header.ivMenu.setVisibility(View.GONE);
            mBinding.header.ivSendRequest.setImageResource(R.drawable.ic_add_contact);
            mBinding.header.ivSendRequest.setOnClickListener(view -> {
                mPresenter.addFriend(userId);
            });
        } else if (isFriend.equals(REQUEST_SEND_BY_YOU.value)) {
            mBinding.header.ivSendRequest.setVisibility(View.VISIBLE);
            mBinding.header.ivSendRequest.setImageResource(R.drawable.ic_sent);
            mBinding.header.ivSendRequest.setOnClickListener(null);
            mBinding.header.ivMenu.setVisibility(View.GONE);
        } else if (blockKey != null) {
            this.blockkey = blockKey;
            mBinding.header.ivSendRequest.setVisibility(View.GONE);
            mBinding.header.ivSendRequest.setOnClickListener(null);
            if (blockkey.equals("2"))
                mBinding.header.ivMenu.setVisibility(View.GONE);
            else mBinding.header.ivMenu.setVisibility(View.VISIBLE);
        } else {
            mBinding.header.ivSendRequest.setVisibility(View.GONE);
            mBinding.header.ivSendRequest.setOnClickListener(null);
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        showProgress(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
