package com.app.qualiffyapp.ui.activities.signUpFlow.business;


import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivityBusinessLoginBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ILoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.FRAGMENT;
import static com.app.qualiffyapp.constants.AppConstants.NEED_UPDATE;

public class LoginActivity extends BaseActivity implements ILoginActivity {
    public String otp, email, phoneNumber;
    public boolean isLogin;
    public boolean isUpdate = false;
    private ActivityBusinessLoginBinding mBinding;
    private BusinessLoginPresenter mPresenter;
    private RequestBean requestBean;
    private HashMap<String, String> selectedIndustry = new HashMap<>();


    @Override
    protected void initView() {
        mBinding = (ActivityBusinessLoginBinding) viewDataBinding;
        mPresenter = new BusinessLoginPresenter(this, getSupportFragmentManager());
        openDesiredFragment();
        firebaseToken();
    }

    private void firebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    if (task.getResult() != null) {
                        String token = task.getResult().getToken();
                        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN, token);
                    }

                });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_business_login;
    }

    @Override
    protected void setListener() {
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
        mBinding.toolbar.skipTxt.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.skip_txt:
                mPresenter.gotoFragment(BusinessLoginPresenter.DestinationFragment.ENABLE_LOCATION, true, true);
                break;
        }
    }

    @Override
    public void setHeader(String header) {
        if (header != null) {
            if (mBinding.toolbar.getRoot().getVisibility() != View.VISIBLE)
                mBinding.toolbar.getRoot().setVisibility(View.VISIBLE);
            mBinding.toolbar.toolbarTitleTxt.setText(header);

        } else mBinding.toolbar.getRoot().setVisibility(View.GONE);
    }

    @Override
    public RequestBean getRequestBean() {
        if (requestBean == null)
            requestBean = new RequestBean();
        return requestBean;
    }

    @Override
    public void hideKeyBoard() {
        Utility.hideKeyboard(this);
    }

    public void setRequestBean() {
        if (requestBean != null)
            requestBean = new RequestBean();
    }

    public HashMap<String, String> getSelectedIndustry() {
        if (selectedIndustry == null)
            selectedIndustry = new HashMap<>();
        return selectedIndustry;
    }

    public void enableSkip(boolean isShow) {
        if (isShow)
            mBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
        else mBinding.toolbar.skipTxt.setVisibility(View.GONE);
    }
    public BusinessLoginPresenter getPresenter() {
        return mPresenter;
    }


    private void openDesiredFragment() {
        Bundle bundle;
        if ((bundle = getIntent().getBundleExtra(BUNDLE)) != null) {
            isUpdate = bundle.getBoolean(NEED_UPDATE, false);
            if (bundle.getSerializable(FRAGMENT) != null) {
                BusinessLoginPresenter.DestinationFragment fragment = (BusinessLoginPresenter.DestinationFragment) getIntent().getBundleExtra(BUNDLE).getSerializable(FRAGMENT);
                mPresenter.gotoFragment(fragment, false, false);
                return;
            }
        }
        mPresenter.gotoFragment(BusinessLoginPresenter.DestinationFragment.LOGIN, false, false);

    }


}
