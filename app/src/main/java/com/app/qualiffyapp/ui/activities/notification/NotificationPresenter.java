package com.app.qualiffyapp.ui.activities.notification;


import com.app.qualiffyapp.models.NotificationModel;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_DELETE_BUSINESS;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_DELETE_JOB_SEEKER;

public class NotificationPresenter implements ApiListener<ResponseModel<NotificationModel>> {
    private SignUpInteractor mSignUpInteractor;
    private SignupView mView;
    private ApiListener<ResponseModel<MsgResponseModel>> msgApiListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            mView.showProgressDialog(false);
            mView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            mView.showProgressDialog(false);
            mView.apiError(error, apiFlag);
        }
    };

    public NotificationPresenter(SignupView view) {
        this.mView = view;
        this.mSignUpInteractor = new SignUpInteractor();
    }


    @Override
    public void onApiSuccess(ResponseModel<NotificationModel> response, int apiFlag) {
        if (apiFlag == API_FLAG_NOTIFICATION_DELETE_JOB_SEEKER || apiFlag == API_FLAG_NOTIFICATION_DELETE_BUSINESS)
            mView.showProgressDialog(false);
        mView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        mView.showProgressDialog(false);
        mView.apiError(error, apiFlag);
    }

    public void getNotification(String type, int apiFlag, int pageNumber) {

        if (type.equalsIgnoreCase("JOB_SEEKER")) {
            mSignUpInteractor.callNotificationApiJobSeeker(this, pageNumber, apiFlag);
        } else {
            mSignUpInteractor.callNotificationApiBusiness(this, pageNumber, apiFlag);

        }
    }

    public void callDeleteApi(String type, String id) {
        mView.showProgressDialog(true);
        if (type.equalsIgnoreCase("JOB_SEEKER")) {
            mSignUpInteractor.callNotificationDeleteJobSeeker(this, id,
                    API_FLAG_NOTIFICATION_DELETE_JOB_SEEKER);
        } else {
            mSignUpInteractor.callNotificationDeleteBusiness(this, id,
                    API_FLAG_NOTIFICATION_DELETE_BUSINESS);

        }
    }


}
