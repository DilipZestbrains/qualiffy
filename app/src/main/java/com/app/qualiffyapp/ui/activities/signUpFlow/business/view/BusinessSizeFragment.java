package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityBusinessSizeBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessSize;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessSizePresenter;

import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.SIGNUP;

public class BusinessSizeFragment extends BLoginSignUpBaseFragment implements IBusinessSize {

    private ActivityBusinessSizeBinding mBinding;
    private BusinessSizePresenter mPresenter;

    @Override
    protected void initUi() {
        mBinding = (ActivityBusinessSizeBinding) viewDataBinding;
        mPresenter = new BusinessSizePresenter(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvMoreThan:
                mPresenter.largeOrgSignup(true);
                break;
            case R.id.tvLessThan:
                mPresenter.smallOrgSignup();
                break;

        }
    }

    @Override
    protected void setListener() {
        mBinding.tvLessThan.setOnClickListener(this);
        mBinding.tvMoreThan.setOnClickListener(this);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_business_size;
    }

    @Override
    protected String getHeader() {
        return "";
    }

    @Override
    public void proceedSignup(BusinessType type) {
        if (type == BusinessType.SMALL) {
            getLoginActivity().getRequestBean().type = 0;
            getLoginActivity().getRequestBean().self = true;
        } else if (type == BusinessType.LARGE) {
            getLoginActivity().getRequestBean().type = 1;
        }

        getLoginActivity().getPresenter().gotoFragment(SIGNUP, true, false);
    }

    @Override
    public void showAdminSignupDialog() {
        CustomDialogs dialog = new CustomDialogs(getLoginActivity(), new yesNoCallback() {
            @Override
            public void onYesClicked(String from) {
                getLoginActivity().getRequestBean().self = false;
                mPresenter.largeOrgSignup(false);
            }

            @Override
            public void onNoClicked(String from) {
                getLoginActivity().getRequestBean().self = true;
                mPresenter.largeOrgSignup(false);

            }
        });
        dialog.showCustomDialogTwoButtons(getString(R.string.app_name), "Would you like to create your account by admin?", "Yes", "No", "");
        dialog.showDialog();
    }
}
