package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityAddressBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.onBoarding.OnBoardingActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IAddress;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.AddressFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobseekerAddressPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_PROFILE_EDIT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_5;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.TUTORIAL_SCREEN_SHOW;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.LAST_NAME;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter.AUTOCOMPLETE_REQUEST_CODE;

public class AddressActivity extends BaseActivity implements IAddress, SignupView {
    private ActivityAddressBinding addressBinding;
    private AddressPresenter mPresenter;
    private JobseekerAddressPresenter addressPresenter;
    private RequestBean requestBean;
    private boolean isBusinessUpdate;

    @Override
    protected void initView() {
        addressBinding = (ActivityAddressBinding) viewDataBinding;
        mPresenter = new AddressPresenter(this);
        addressPresenter = new JobseekerAddressPresenter(this);
        if (getIntent().getBundleExtra(BUNDLE) != null)
            isBusinessUpdate = getIntent().getBundleExtra(BUNDLE).getBoolean(IS_BUSINESS_UPDATE);

        toolbar();
    }


    private void toolbar() {
        addressBinding.toolbar.backBtnImg.setOnClickListener(this);
        addressBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_address;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.etAddress:
                mPresenter.initAddressPickerDialog();

                break;
            case R.id.tvDone:
                if (requestBean != null) {
                    showProgressDialog(true);
                    if (isBusinessUpdate)
                        addressPresenter.hitApi(requestBean, API_FLAG_BUSINESS_PROFILE_EDIT);
                    else if (Utility.getStringSharedPreference(this, FIRST_NAME) != null)
                        requestBean.f_name = Utility.getStringSharedPreference(this, FIRST_NAME);
                    if (Utility.getStringSharedPreference(this, LAST_NAME) != null)
                        requestBean.l_name = Utility.getStringSharedPreference(this, LAST_NAME);

                    addressPresenter.hitApi(requestBean, API_FLAG_SIGNUP_5);
                }

                break;
        }

    }

    @Override
    protected void setListener() {
        addressBinding.tvDone.setOnClickListener(this);
        addressBinding.etAddress.setOnClickListener(this);
    }


    @Override
    public void onNext() {

    }

    @Override
    public void openAutoSelectPlace(Autocomplete.IntentBuilder intent) {
        startActivityForResult(intent.build(this), AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onAddressSelect(String address, String country, String state, String lat, String lng) {
        addressBinding.etAddress.setText(state + ", " + country);
        requestBean = new RequestBean();
        requestBean.address = address;
        requestBean.cntry = country;
        requestBean.state = state;
        requestBean.lng = lng;
        requestBean.lat = lat;
    }

    @Override
    public void onUpdate(String msg, int Status) {

    }

    @Override
    public void preUpdate() {

    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.ADDRESS_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.address_not_selected));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(intent);
                mPresenter.pickAddress(place);

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(intent);
                Log.i(AddressFragment.class.getName(), status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            if (response.getStatus() == 1) {
                MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                switch (apiFlag) {
                    case API_FLAG_SIGNUP_5:
                        Utility.showToast(this, msgResponseModel.msg);
                        Utility.putIntValueInSharedPreference(getApplicationContext(), USER_TYPE, JOB_SEEKER);
                        Intent intent = null;
                        if (Utility.getBooleanSharedPreference(getApplicationContext(), TUTORIAL_SCREEN_SHOW)) {
                            intent = new Intent(this, DashboardActivity.class);
                        } else {
                            intent = new Intent(this, OnBoardingActivity.class);
                        }

                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;
                    case API_FLAG_BUSINESS_PROFILE_EDIT:
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "done");
                        setResult(RESULT_OK, returnIntent);
                        finish();
                        break;

                }
            } else
                Utility.showToast(this, response.err.msg);
        showProgressDialog(false);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
