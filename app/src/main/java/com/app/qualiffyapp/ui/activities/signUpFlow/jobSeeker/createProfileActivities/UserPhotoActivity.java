package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityUserPhotoBinding;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.AddMediaPresenter;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_PROFILE_PIC;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_GALLERY_IMG;
import static com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity.CROPPER_SQUARE;

public class UserPhotoActivity extends BaseActivity implements yesNoCallback, SignupView {
    private ActivityUserPhotoBinding userPhotoBinding;
    private AddMediaPresenter addMediaPresenter;
    private File imgFile;

    private CustomDialogs customDialogs;
    private boolean isPhotoChange;
    private boolean isJobSeekerUpdate;


    @Override
    protected void initView() {
        userPhotoBinding = (ActivityUserPhotoBinding) viewDataBinding;
        addMediaPresenter = new AddMediaPresenter(this);
        toolbar();
        getIntentData();
        initDialog();
        setListeners();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            String img = bundle.getString(JS_PROFILE_PIC);
            isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            GlideUtil.loadImage(userPhotoBinding.profileImg,
                    img,
                    R.drawable.user_placeholder,
                    100,
                    0
            );
        }
    }

    private void setListeners() {
        userPhotoBinding.capturePhotoImg.setOnClickListener(this);
        userPhotoBinding.doneBtn.btn.setOnClickListener(this);
    }


    private void initDialog() {
        customDialogs = new CustomDialogs(this, this);
        customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                getResources().getString(R.string.add_media_dialog_body),
                getResources().getString(R.string.add_media_dialog_pos_btn),
                getResources().getString(R.string.add_media_dialog_neg_btn),
                getResources().getString(R.string.USER_PHOTO_TITLE));
    }


    private void toolbar() {
        userPhotoBinding.toolbar.backBtnImg.setOnClickListener(this);
        userPhotoBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.USER_PHOTO_TITLE));
        if (Utility.getBooleanSharedPreference(UserPhotoActivity.this, AppConstants.APP_SESSION)) {
            userPhotoBinding.doneBtn.btn.setText(getResources().getString(R.string.DONE));
        } else {
            userPhotoBinding.doneBtn.btn.setText(getResources().getString(R.string.NEXT));
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_user_photo;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;

            case R.id.capture_photo_img:
                isPhotoChange = true;
                customDialogs.showDialog();
                break;

            case R.id.done_btn:
                if (imgFile != null) {
                    if (isPhotoChange) {
                        showProgressDialog(true);
                        addMediaPresenter.hitApi(imgFile, API_FLAG_ADD_MEDIA, "1");
                    } else {
                        if (isJobSeekerUpdate)
                            finish();
                        else
                            openActivity(AddMediaActivity.class, null);
                    }
                } else Utility.showToast(this, getResources().getString(R.string.empty_user_photo));

                break;
        }

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);

    }

    @Override
    public void onYesClicked(String from) {
        customDialogs.hideDialog();
        if (onAskForSomePermission(this, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE)) {
            EasyImage.openGallery(this, REQUEST_CODE_GALLERY_IMG);
        }
    }

    @Override
    public void onNoClicked(String from) {
        customDialogs.hideDialog();
        if (onAskForSomePermission(this, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE)) {
            openCameraActivity(null);
        }

    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case CAMERA_STORAGE_PERMISSION_REQUEST_CODE:
                openCameraActivity(null);
                break;
            case GALLERY_STORAGE_PERMISSION_REQUEST_CODE:
                EasyImage.openGallery(this, REQUEST_CODE_GALLERY_IMG);

                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                    Log.e("Permission", "Returned from setting");
                    break;
                case REQUEST_CODE_CAMERA_IMG:
                    List<File> list = new ArrayList<>();
                    list.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list);
                    break;
                case  REQUEST_CODE_GALLERY_IMG:
                    List<File> list1 = new ArrayList<>();
                    list1.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list1);
                    break;

            }
        }


        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                File _imgFile = list.get(0);
                if(_imgFile.getPath()!=null) {
                    openEditor(_imgFile.getPath());
                   /* Intent intent = new Intent(UserPhotoActivity.this, PhotoEditorActivity.class);
                    intent.putExtra(MEDIA_PATH, _imgFile.getPath());
                    startActivityForResult(intent, REQUEST_CODE_GALLERY_IMG);*/
                }

            }
        });
    }

    private void onPhotosReturned(List<File> list) {

        File _imgFile = list.get(0);
        try {
            isPhotoChange = true;
            imgFile = new Compressor(this).compressToFile(_imgFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        GlideUtil.loadImageFromFile(userPhotoBinding.profileImg, imgFile.getAbsolutePath());
    }


    private void openEditor(String imgPath) {


        try {
            Intent myIntent = new Intent(this, Class.forName(PHOTO_EDITOR_ACTIVITY));
                myIntent.putExtra(MEDIA_PATH, imgPath);
                myIntent.putExtra(CROPPER_SQUARE,true );
                startActivityForResult(myIntent, REQUEST_CODE_CAMERA_IMG);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }



    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        isPhotoChange = false;

        if (response.getStatus() == 1) {
            AddMediaResponseModel mediaResponseModel = (AddMediaResponseModel) response.getRes();
            if (isJobSeekerUpdate) {
                Intent intent = new Intent();
                intent.putExtra(JS_PROFILE_PIC, mediaResponseModel.img_id);
                setResult(RESULT_OK, intent);
                finish();

            } else
                openActivity(AddMediaActivity.class, null);
        } else {
            Utility.showToast(this, response.err.msg);
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }


    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    private void openCameraActivity(String imgPath) {
        SandriosCamera.showOverlay=false;
        SandriosCamera
                .with()
                .setShowPicker(true)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                .setCropperSquare(true)
                .launchCamera(this);

     /*   Intent i = new Intent(this, CameraActivity.class);
        i.putExtra(MEDIA_TYPE, 1);
        i.putExtra(MEDIA_PATH, imgPath);
        startActivityForResult(i, REQUEST_CODE_CAMERA_IMG);*/
    }
}
