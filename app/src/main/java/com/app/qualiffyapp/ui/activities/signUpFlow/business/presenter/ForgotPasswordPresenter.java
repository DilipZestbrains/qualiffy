package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;
import android.util.Patterns;

import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IForgotPassword;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ForgotPasswordPresenter {
    private IForgotPassword forgotPassword;
    private SignUpInteractor interactor;
    private ApiListener<MsgResponseModel> forgotPassApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            forgotPassword.onForgotPass(SUCCESS, response.msg);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            forgotPassword.onForgotPass(FAILURE, error);
        }
    };

    public ForgotPasswordPresenter(IForgotPassword forgotPassword) {
        this.forgotPassword = forgotPassword;
        interactor = new SignUpInteractor();
    }


    public void onForgotPass(String email) {
        if (isValid(email)) {
            forgotPassword.preForgotPass();
            interactor.forgotPassword(forgotPassApiListener, email, API_FLAG_BUSINESS);
        }
    }

    private boolean isValid(String email) {
        if (email == null || TextUtils.isEmpty(email)) {
            forgotPassword.showToast(ToastType.EMAIL_EMPTY);
            return false;
        }else if (email != null && !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            forgotPassword.showToast(ToastType.EMAIL_FORMAT_ERROR);
            return false;
        }
        return true;
    }


}
