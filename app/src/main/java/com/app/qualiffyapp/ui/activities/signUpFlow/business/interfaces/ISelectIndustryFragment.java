package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

import java.util.ArrayList;
import java.util.List;

public interface ISelectIndustryFragment extends BaseInterface {

    void onSearchResult(String message, List<SuggestedListResponseModel> filterList);

    void searchResultError();

    void proceedToNext(ArrayList<String> selectedIndustryList);


}
