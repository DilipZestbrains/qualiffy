package com.app.qualiffyapp.ui.activities.contacts;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.widget.PopupMenu;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.FragmentActivity;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.contact.ContactRecyclerAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.contact.IContactClick;
import com.app.qualiffyapp.databinding.ActivityContactBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.localDatabase.entity.Contact;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.profile.otherUser.OtherUserProfile;
import com.app.qualiffyapp.utils.Contacts;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import java.util.ArrayList;
import java.util.List;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ACCEPT_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_REJECT_FRND;
import static com.app.qualiffyapp.constants.AppConstants.ACCEPT_OR_REJECT;
import static com.app.qualiffyapp.constants.AppConstants.CONTACT_INVITE;
import static com.app.qualiffyapp.constants.AppConstants.FRND;
import static com.app.qualiffyapp.constants.AppConstants.FRND_DETAIL;
import static com.app.qualiffyapp.constants.AppConstants.SEND_FRIEND_REQUEST;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_ACCEPTED;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_NOT_SEND;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_SEND_BY_YOU;

public class ContactActivity extends BaseActivity implements IContactClick, SignupView, OnContactSyncListener {
    private static final int CONTACT_PERMISSION_CODE = 123;
    private ContactRecyclerAdapter adapter;
    private String[] contactPermissions = {Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS};
    private ActivityContactBinding mBinding;
    private AppRepository appRepository;
    private PopupMenu popupMenu;
    private ContactPresenter mPresenter;
    private AppFirebaseDatabase appFirebaseDatabase;
    private UserConnection friendConnection;
    private boolean hasSynced = false;
    private boolean isSyncing = false;

    private PopupMenu.OnMenuItemClickListener menuListener = item -> {
        switch (item.getItemId()) {
            case R.id.menuBlockedContact:
                openActivity(BlockContactActivity.class, null);
                break;
            case R.id.menuSyncContacts:
                showContactList(true);
                break;
        }
        return true;

    };


    @Override
    protected void initView() {
        mBinding = (ActivityContactBinding) viewDataBinding;
        mPresenter = new ContactPresenter(this);
        adapter = new ContactRecyclerAdapter(new ArrayList<>(), this);
        mBinding.rvContact.setAdapter(adapter);
        mBinding.contactHeader.tvHeader.setText(getResources().getString(R.string.contact));
        appRepository = AppRepository.getInstance(this);
        setObserver();
        initFirebase();

    }

    private void initFirebase() {
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
    }

    public void showContactList(boolean isSync) {
        if (!isSyncing) {
            if (onAskForSomePermission(this, contactPermissions, CONTACT_PERMISSION_CODE)) {
                showProgress(true);
                isSyncing = true;
                HandlerThread handlerThread = new HandlerThread("ContactThread");
                handlerThread.start();
                Looper looper = handlerThread.getLooper();
                Handler handler = new Handler(looper);
                handler.post(() -> {
                    if (isSync) {
                        ArrayList<Contact> contacts =
                                Contacts.getPhoneContacts(getContentResolver(), AppRepository.getInstance(ContactActivity.this));
                        if (contacts != null) {
                            appRepository.insertContactList(contacts, this);
                        } else {
                            isSyncing = false;
                            onContactSync();
                            Log.e("Contacts", "no contacts found in directory");
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void setListener() {
        mBinding.contactHeader.ivSetting.setOnClickListener(this);
        mBinding.contactHeader.ivBack.setOnClickListener(this);
        mBinding.svContact.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.filter(newText);
                return false;
            }
        });

    }


    private void setObserver() {
        AppRepository.getInstance(this).contacts.observe(this, contacts -> {
            adapter.refreshList((ArrayList<Contact>) contacts);
            if (contacts.size() <= 0 && !hasSynced) {
                hasSynced = true;
                showContactList(true);
            }
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        super.onPermissionsGranted(requestCode, list);
        if (requestCode == CONTACT_PERMISSION_CODE) {
            showContactList(true);
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_contact;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivSetting:
                showMenu(mBinding.contactHeader.ivSetting);
                break;
            case R.id.ivBack:
                onBackPressed();
        }

    }

    private void showMenu(View view) {
        if (popupMenu == null) {
            popupMenu = new PopupMenu(this, view);
            MenuInflater inflater = popupMenu.getMenuInflater();
            inflater.inflate(R.menu.menu_contact, popupMenu.getMenu());
            popupMenu.setOnMenuItemClickListener(menuListener);
        }
        popupMenu.show();
    }

    public void acceptRequest(String id) {
        mPresenter.hitFrndAPI(id, API_FLAG_ACCEPT_FRND);

/*       friendConnection = new UserConnection();
        friendConnection.setUid(contact.get_id());
        friendConnection.setName(contact.getF_name());
        friendConnection.setImg(contact.getImg());
        friendConnection.setPhone(contact.getPno());
        friendConnection.setBlocked(BlockStatus.NOT_BLOCKED.value);*/
    }

    public void rejectRequest(String id) {

        mPresenter.hitFrndAPI(id, API_FLAG_REJECT_FRND);
    }

    @Override
    public void onContactClick(Contact contact, int flag) {
        switch (flag) {
            case CONTACT_INVITE:
                startActivity(Intent.createChooser(Utility.sendMsg("Invitation for Qualiffy"), getResources().getString(R.string.app_name)));
                break;
            case FRND:
                if (contact.get_id() != null) {
                    if (contact.getIs_frnd().equals(REQUEST_NOT_SEND.value))
                        mPresenter.hitFrndAPI(contact.get_id(), API_FLAG_ADD_FRND);
                    else if (contact.getIs_frnd().equals(REQUEST_ACCEPTED.value)) {
                        Bundle b = new Bundle();
                        b.putString(USER_ID, contact.get_id());
                        openActivity(OtherUserProfile.class, b);
                    }

                }
                break;
            case FRND_DETAIL:
                if (contact.get_id() != null)
                    openContactProfile(contact.get_id());
                break;

            case ACCEPT_OR_REJECT:
               /* friendConnection = new UserConnection();
                friendConnection.setUid(contact.get_id());
                friendConnection.setName(contact.getUsername());
                friendConnection.setPhone(contact.getPno());
                friendConnection.setImg(contact.getImg());
                friendConnection.setType(FirebaseUserType.JOB_SEEKER.s);
                friendConnection.setBlocked(BlockStatus.NOT_BLOCKED.value);

                Bundle b = new Bundle();
                b.putString(CONNECTION_ID, contact.get_id());
                AcceptRejectBottomSheet bottomSheet = new AcceptRejectBottomSheet();
                bottomSheet.setArguments(b);
                bottomSheet.show(getSupportFragmentManager(), "TAG");*/
                break;

            case SEND_FRIEND_REQUEST:
                if (contact.get_id() != null) {
                    mPresenter.hitFrndAPI(contact.get_id(), API_FLAG_ADD_FRND);
                    friendConnection = new UserConnection();
                    friendConnection.setUid(contact.get_id());
                    break;
                }


        }
    }

    @Override
    public void openContactProfile(String id) {
        Bundle b = new Bundle();
        b.putString(USER_ID, id);
        openActivity(OtherUserProfile.class, b);

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {

        if (response.getStatus() == SUCCESS) {
            MsgResponseModel responseModel = (MsgResponseModel) response.getRes();
            validationFailed(responseModel.msg);
            switch (apiFlag) {
                case API_FLAG_ACCEPT_FRND:
                    if (friendConnection != null)
                        appRepository.updateContactStatus(REQUEST_ACCEPTED.value, friendConnection.getUid());
                    UserConnection user = Utility.getUserConnection(this);
                    appFirebaseDatabase.addFriend(user, friendConnection);
                    friendConnection = null;
                    break;
                case API_FLAG_REJECT_FRND:
                    if (friendConnection != null)
                        appRepository.updateContactStatus(REQUEST_NOT_SEND.value, friendConnection.getUid());
                    friendConnection = null;
                    break;

                case API_FLAG_ADD_FRND:
                    if (friendConnection != null) {
                        appRepository.updateContactStatus(REQUEST_SEND_BY_YOU.value, friendConnection.getUid());
                        friendConnection = null;
                    }
            }

        } else validationFailed(response.err.msg);
        showProgress(false);
    }


    @Override
    public void apiError(String error, int apiFlag) {
        validationFailed(error);
        showProgress(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);

    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onContactSync() {
        showProgress(false);
        isSyncing = false;
    }
}
