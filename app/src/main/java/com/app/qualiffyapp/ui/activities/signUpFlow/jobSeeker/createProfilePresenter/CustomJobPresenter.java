package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;


import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

public class CustomJobPresenter implements ApiListener<ResponseModel<AddCustomeJobModel>> {


    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public CustomJobPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApiAddJobs(String name, String cat_ID, int apiFlagJsAddJobTitle) {
        if (cat_ID == null) {
            mSignUpInteractor.putCustomJobWtCat(this, name, apiFlagJsAddJobTitle);
        } else {
            mSignUpInteractor.putCustomJob(this, cat_ID, name, apiFlagJsAddJobTitle);

        }

    }

}
