package com.app.qualiffyapp.ui.activities.addJob.business.views;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import androidx.fragment.app.FragmentActivity;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.FragmentAddJobBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.addJob.business.presenter.AddJobPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.AddressFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.CustomJobPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.locationUtils.ConvertLatLngToAddress;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_INDUSTRY;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_ROLE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_ADD_JOB_TITLE;
import static com.app.qualiffyapp.constants.AppConstants.DATE_FORMAT4;
import static com.app.qualiffyapp.constants.AppConstants.SERVER_JOB_DATE_FORMAT;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter.AUTOCOMPLETE_REQUEST_CODE;

public class AddJobFragment extends BaseFragment implements AddJobPresenter.IAddress, SignupView, AdapterView.OnItemSelectedListener {
    private FragmentAddJobBinding mBinding;
    private DatePickerDialog datePicker;
    private AddJobPresenter mPresenter;
    private CustomJobPresenter jobPresenter;
    private HashMap<String, String> indusList;
    private HashMap<String, String> roleList;
    private RequestBean bean;
    private ArrayList<SuggestedListResponseModel> selectedlist;
    private ArrayList<String> jobTypeList;


    @Override
    protected void initUi() {
        mBinding = (FragmentAddJobBinding) viewDataBinding;
        mPresenter = new AddJobPresenter(this, this);
        jobPresenter = new CustomJobPresenter(this);
        selectedlist = new ArrayList<>();
        mBinding.submitBtn.btn.setText(getResources().getString(R.string.done));
        bean = new RequestBean();
        setSpinner();
        initDatePicker();

    }

    private void setSpinner() {
        jobTypeList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.job_type)));
        Collections.sort(jobTypeList, String.CASE_INSENSITIVE_ORDER);

        jobTypeList.add(0, getContext().getResources().getString(R.string.select_job_type));
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, jobTypeList);
        spinnerAdapter.setDropDownViewResource(R.layout.item_spinner_layout);
        mBinding.searchSpnr.setAdapter(spinnerAdapter);


        ArrayList<String> expList = new ArrayList<>();
        for (int i = 0; i <= 50; i++) {
            expList.add(i + " " + Utility.singularPluralText("year", i));
        }

        ArrayAdapter<String> expAdapter = new ArrayAdapter<String>
                (getContext(), android.R.layout.simple_spinner_item, expList);
        expAdapter.setDropDownViewResource(R.layout.item_spinner_layout);
        mBinding.expSpnr.setAdapter(expAdapter);

    }


    private void initDatePicker() {
        Calendar newCalendar = Calendar.getInstance();
        if (getContext() != null)
            datePicker = new DatePickerDialog(getContext(), (view, year, monthOfYear, dayOfMonth) -> {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                mBinding.edtDate.setText(Utility.getUtcDate(newDate.getTimeInMillis(), DATE_FORMAT4));
                bean.date = Utility.getUtcDate(newDate.getTimeInMillis(), SERVER_JOB_DATE_FORMAT);
            }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        datePicker.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

    }


    @Override
    protected void setListener() {
        super.setListener();

        mBinding.edtDate.setOnClickListener((view) -> {
            Utility.hideKeyboard(getContext());
            datePicker.show();
        });
        mBinding.edtAddress.setOnClickListener((view) -> {
            Utility.hideKeyboard(getContext());
            mPresenter.initAddressPickerDialog();
        });
        mBinding.submitBtn.btn.setOnClickListener((view) -> isValid(bean));
        mBinding.addJobs.setOnClickListener(v -> {
            Utility.hideKeyboard(getContext());
            addJobTitle();
        });
        mBinding.searchSpnr.setOnItemSelectedListener(this);
        mBinding.searchSpnr.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(getContext()!=null)
                    Utility.hideKeyboard(getContext());
                return false;
            }
        }) ;
        mBinding.expSpnr.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(getContext()!=null)
                    Utility.hideKeyboard(getContext());
                return false;
            }
        }) ;
        mBinding.edtJobCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() >= 1)
                    mPresenter.hitApi(s.toString());
                else bean.jobC_id = null;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mBinding.searchSpnr.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(getContext()!=null)
               Utility.hideKeyboard(getContext());
                return false;
            }
        }) ;
        mBinding.edtJobCategory.setOnItemClickListener((parent, view, position, rowId) -> {
            Utility.hideKeyboard(getContext());
            String selectedStr = (String) parent.getItemAtPosition(position);
            if (selectedStr != null && indusList != null && indusList.size() > 0)
                bean.jobC_id = indusList.get(selectedStr);

        });
        mBinding.edtJobTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0)
                    bean.myrole = null;
                else if (s.toString().length() >= 2) {
                    if (bean.jobC_id != null)
                        mPresenter.getRole(s.toString(), bean.jobC_id);
                    else validationFailed("Please select Job Category");
                } else {
                    mBinding.addJobs.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.edtJobTitle.setOnItemClickListener((parent, view, position, id) -> {
            Utility.hideKeyboard(getContext());
            String selectedStr = (String) parent.getItemAtPosition(position);
            if (selectedStr != null && roleList != null && roleList.size() > 0)
                bean.myrole = roleList.get(selectedStr);
        });


    }

    private void addJobTitle() {
        if (bean.jobC_id != null)
            jobPresenter.hitApiAddJobs(mBinding.edtJobTitle.getText().toString(), bean.jobC_id, API_FLAG_JS_ADD_JOB_TITLE);
        else validationFailed(getResources().getString(R.string.select_job_category));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_add_job;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
//                Place place = Autocomplete.getPlaceFromIntent(intent);
//                mPresenter.pickAddress(place);
                Place place = Autocomplete.getPlaceFromIntent(intent);
                if (place.getLatLng() != null) {
                    ConvertLatLngToAddress convertLatLngToAddress = new ConvertLatLngToAddress();
                    try {
                        ConvertLatLngToAddress.AddressBean addressBean = convertLatLngToAddress.getAddress(getContext(), place.getLatLng().latitude, place.getLatLng().longitude);
                        String state = addressBean.city + ", " + addressBean.state;
                        onAddressSelect(addressBean.addr, addressBean.cntry, state, addressBean.lat, addressBean.lng);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(intent);
                Log.i(AddressFragment.class.getName(), status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }


    @Override
    public void onClick(View v) {
    }

    @Override
    public void openAutoSelectPlace(Autocomplete.IntentBuilder intent) {
        startActivityForResult(intent.build(context), AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onAddressSelect(String address, String country, String state, String lat, String lng) {
        mBinding.edtAddress.setText(state);
        bean.cntry = country;
        bean.state = state;
        bean.lat = lat;
        bean.lng = lng;
        bean.address = address;

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {

        if (response.getStatus() == 1) {
            switch (apiFlag) {
                case API_FLAG_GET_INDUSTRY:
                    IndustryResponseModel getIndustryResponseModel = (IndustryResponseModel) response.getRes();
                    List<SuggestedListResponseModel> suggestList = getIndustryResponseModel.jobC;
                    if (suggestList != null && suggestList.size() > 0) {
                        if (indusList == null)
                            indusList = new HashMap<>();
                        if (!indusList.isEmpty()) indusList.clear();
                        for (SuggestedListResponseModel indus : suggestList) {
                            indusList.put(indus.name, indus.id);
                        }
                        if (getContext() != null) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (getContext(), android.R.layout.simple_list_item_1, new ArrayList<String>(indusList.keySet()));
                            mBinding.edtJobCategory.setAdapter(adapter);
                        }
                    }
                    break;
                case API_FLAG_GET_ROLE:
                    IndustryResponseModel roleResponseModel = (IndustryResponseModel) response.getRes();
                    List<SuggestedListResponseModel> suggestRoleList = roleResponseModel.jobT;
                    if (suggestRoleList != null && suggestRoleList.size() > 0) {
                        mBinding.addJobs.setVisibility(View.GONE);
                        if (roleList == null)
                            roleList = new HashMap<>();
                        if (!roleList.isEmpty()) roleList.clear();
                        for (SuggestedListResponseModel indus : suggestRoleList) {
                            roleList.put(indus.name, indus.id);
                        }
                        if (getContext() != null) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                    (getContext(), android.R.layout.simple_list_item_1, new ArrayList<String>(roleList.keySet()));
                            mBinding.edtJobTitle.setAdapter(adapter);
                        }
                    } else {
                        mBinding.addJobs.setVisibility(View.VISIBLE);
                        validationFailed("Not Data Found Please add job title");
                    }

                    break;
                case API_FLAG_JS_ADD_JOB_TITLE:
                    AddCustomeJobModel model = (AddCustomeJobModel) response.getRes();
                    if (model != null)
                        bean.myrole = model.job._id;
                    else
                        bean.myrole = null;
                    break;


                case API_FLAG_BUSINESS_ADD_JOB:
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    showProgressDialog(false);
                    validationFailed(msgResponseModel.msg);
                    if (getActivity() != null)
                        getActivity().onBackPressed();
                    break;
            }

        } else {
            validationFailed(response.err.msg);
            if (apiFlag == API_FLAG_BUSINESS_ADD_JOB)
                showProgressDialog(false);

        }


    }

    @Override
    public void apiError(String error, int apiFlag) {
        if (apiFlag == API_FLAG_BUSINESS_ADD_JOB)
            showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    private void isValid(RequestBean bean) {
        if (bean != null) {


            if (bean.jobC_id == null) {
                validationFailed(getResources().getString(R.string.select_industry));
                return;
            }
            if (bean.myrole == null) {
                validationFailed(getResources().getString(R.string.select_job_title));
                return;
            }
            if (bean.date == null) {
                validationFailed(getResources().getString(R.string.select_date));
                return;
            }
            if (bean.address == null) {
                validationFailed(getResources().getString(R.string.select_address));
                return;
            }
            if (mBinding.etDescription.getText() == null || TextUtils.isEmpty(mBinding.etDescription.getText().toString().trim())) {
                validationFailed(getResources().getString(R.string.please_add_job_desc));
                return;
            }
            String[] expStr = mBinding.expSpnr.getSelectedItem().toString().split(" ");
            bean.exp = expStr[0];
            if (mBinding.edtSalary.getText() != null)
                bean.slry = mBinding.edtSalary.getText().toString();
            bean.prty = String.valueOf(mBinding.prioritySwitch.isChecked());
            bean.j_type = new ArrayList<>();
            if (mBinding.etDescription.getText() != null)
                bean.desc = mBinding.etDescription.getText().toString();
            for (SuggestedListResponseModel model : selectedlist)
                bean.j_type.add(model.name);

            if (bean.j_type.size() == 0) {
                validationFailed(getResources().getString(R.string.please_select_job_type));
                return;
            }

            showProgressDialog(true);
            mPresenter.addJobApi(bean);

        }
    }


    private void addChip(String txt, String id) {
        if (selectedlist.size() >= 3) {
            Utility.showToast(getContext(), getResources().getString(R.string.job_type_limit_alert));
            return;
        }
        Utility.hideKeyboard(getContext());
        if (!iscontain(txt)) {
            CustomChip chip = new CustomChip(getContext(), txt);
            chip.setId(Integer.parseInt(id));
            chip.setOnCloseIconClickListener(view -> {
                mBinding.chipgroup.removeView(view);
                for (int i = 0; i < selectedlist.size(); i++) {
                    if (selectedlist.get(i).id.equals(String.valueOf(chip.getId()))) {
                        selectedlist.remove(i);
                    }
                }
            });
            mBinding.chipgroup.addView(chip);
            SuggestedListResponseModel suggestedListResponseModel = new SuggestedListResponseModel();
            suggestedListResponseModel.id = id;
            suggestedListResponseModel.name = txt;
            selectedlist.add(suggestedListResponseModel);
        }
    }

    private boolean iscontain(String txt) {
        boolean isContainKey = false;
        for (SuggestedListResponseModel suggestedListResponseModel : selectedlist) {
            if (txt.equals(suggestedListResponseModel.name))
                return true;
        }
        return isContainKey;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            addChip(jobTypeList.get(position), String.valueOf(position));
            mBinding.searchSpnr.setSelection(0);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

