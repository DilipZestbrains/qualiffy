package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;


import com.app.qualiffyapp.models.RequestBean;

public interface ILoginActivity {
    void setHeader(String header);

    RequestBean getRequestBean();

    void hideKeyBoard();
}