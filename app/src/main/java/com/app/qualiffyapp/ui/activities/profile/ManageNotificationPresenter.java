package com.app.qualiffyapp.ui.activities.profile;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_TOGGLE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MANAGE_NOTIFICATION;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ManageNotificationPresenter {
    private final IManageNotification manageNotification;
    private final SignUpInteractor signUpInteractor;

    private ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            manageNotification.onUpdateNotification(response.msg, SUCCESS, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            manageNotification.onUpdateNotification(error, FAILURE, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            manageNotification.onUpdateNotification(null, -12, apiFlag);

        }
    };

    public ManageNotificationPresenter(IManageNotification manageNotification) {
        this.manageNotification = manageNotification;
        signUpInteractor = new SignUpInteractor();
    }


    public void updateBusinessNotificationStatus(RequestBean isNotify, int apiFlag) {
        manageNotification.showProgressDialog();
        if (apiFlag == API_FLAG_BUSINESS_TOGGLE)
            signUpInteractor.manageBusinessToggle(msgApiListener, isNotify, API_FLAG_BUSINESS_TOGGLE);
        else
            signUpInteractor.jobsSeekerProfileToogle(msgApiListener, isNotify, API_FLAG_JS_MANAGE_NOTIFICATION);

    }

    public void updateNotificationStatus(RequestBean bean, int apiFlag) {
        manageNotification.showProgressDialog();
        signUpInteractor.jobsSeekerProfileToogle(msgApiListener, bean, API_FLAG_JS_MANAGE_NOTIFICATION);

    }
}
