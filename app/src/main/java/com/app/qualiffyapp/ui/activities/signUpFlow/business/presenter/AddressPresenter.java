package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;
import android.util.Patterns;

import com.app.qualiffyapp.customViews.PlacesFieldSelector;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IAddress;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.google.android.libraries.places.api.model.AddressComponent;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import static com.app.qualiffyapp.constants.AppConstants.COUNTRY;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.LOCALITY;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.SOMETHING_WENT_WRONG;

public class AddressPresenter extends UpdateBusinessDetailsPresenter {
    public static final int AUTOCOMPLETE_REQUEST_CODE = 1;
    private IAddress address;
    private ApiListener<MsgResponseModel> msgResponseModelApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            address.onUpdate(response.msg, SUCCESS);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            address.onUpdate(error, FAILURE);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            address.onUpdate(null, FAILURE);
        }
    };




    public AddressPresenter(IAddress address) {
        super(address);
        this.address = address;
    }

    public void pickAddress(Place place) {
        if (place != null && place.getLatLng() != null) {
            String state = "", country = "";
            double lat, lng;
            lat = place.getLatLng().latitude;
            lng = place.getLatLng().longitude;
            if (place.getAddressComponents() != null)
                for (AddressComponent com : place.getAddressComponents().asList()) {
                    for (String type : com.getTypes()) {
                        if (type.equals(LOCALITY)) {
                            state = com.getName();
                            break;
                        } else if (type.equals(COUNTRY))
                            country = com.getName();
                    }
                }

            address.onAddressSelect(place.getAddress(), country, state, Double.toString(lat), Double.toString(lng));
        }
    }

    public void proceed(RequestBean location) {
        if (isValid(location)) {
            address.onNext();
        }
    }

    private boolean isValid(RequestBean requestBean) {
        if (requestBean == null) {
            address.showToast(SOMETHING_WENT_WRONG);
            return false;
        }
        if (requestBean.address == null || TextUtils.isEmpty(requestBean.address)) {
            address.showToast(ToastType.ADDRESS_EMPTY);
            return false;
        }
        return true;
    }


    public void initAddressPickerDialog() {
        PlacesFieldSelector fieldSelector;
        fieldSelector = new PlacesFieldSelector();
        Autocomplete.IntentBuilder intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldSelector.getAllFields());
        address.openAutoSelectPlace(intent);
    }

    public void updateAddress(RequestBean requestBean) {
        if (isValid(requestBean)) {
            address.preUpdate();
            updateDetails(requestBean);
        }

    }


}


