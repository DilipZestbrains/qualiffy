package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.signupLogin.SpannableClickCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.FragmentBusinessLoginBinding;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.ForgotPasswordActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessLoginFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginFragmentPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.utils.SpannableTxt;
import com.app.qualiffyapp.utils.Utility;
import java.util.Objects;
import static com.app.qualiffyapp.constants.AppConstants.IS_SMALL_ORG;
import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;


public class BusinessLoginFragment extends BLoginSignUpBaseFragment implements SpannableClickCallback, IBusinessLoginFragment, View.OnFocusChangeListener {
    private FragmentBusinessLoginBinding mBinding;
    private TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mBinding != null) {
                if (!Objects.requireNonNull(mBinding.etPhone.getText()).toString().isEmpty())
                    mBinding.etPhone.setText("");
            }
        }
    };
    private TextWatcher passwordWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mBinding != null) {
                if (!Objects.requireNonNull(mBinding.etPhone.getText()).toString().isEmpty())
                    mBinding.etPhone.setText("");
            }
        }
    };
    private TextWatcher phoneWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mBinding != null) {
                if (!Objects.requireNonNull(mBinding.etEmail.getText()).toString().isEmpty() || !Objects.requireNonNull(mBinding.etPassword.getText()).toString().isEmpty()) {
                    mBinding.etEmail.setText("");
                    mBinding.etPassword.setText("");
                }
            }
        }
    };
    private BusinessLoginFragmentPresenter mPresenter;

    @Override
    protected void initUi() {
        mBinding = (FragmentBusinessLoginBinding) viewDataBinding;
        mPresenter = new BusinessLoginFragmentPresenter(this);
        setSpan();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_business_login;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvLogin) {
            if (mBinding.etPhone.getText() != null)
                if (!mBinding.etPhone.getText().toString().isEmpty()) {
                    getLoginActivity().setRequestBean();
                    getLoginActivity().getRequestBean().pno = mBinding.etPhone.getText().toString();
                    getLoginActivity().getRequestBean().is_login = false;
                    getLoginActivity().getRequestBean().dev_tkn = Utility.getStringSharedPreference(getActivity(),
                                    AppConstants.PREFS_ACCESS_TOKEN);
                    if(mBinding.etPhone.getText().toString().length()>=8 && mBinding.etPhone.getText().toString().length()<=16) {
                        mPresenter.loginWithPhone(getLoginActivity().getRequestBean());
                    }else Utility.showToast(getContext(),getResources().getString(R.string.mobile_error_alert));
                } else
                    mPresenter.login(Objects.requireNonNull(mBinding.etEmail.getText()).toString(),
                                mBinding.etPassword.getText().toString(),
                                Utility.getStringSharedPreference(getActivity(),
                                        AppConstants.PREFS_ACCESS_TOKEN));

        }else if (v.getId() == R.id.tvForgotPass) {
            getLoginActivity().openActivity(ForgotPasswordActivity.class, null);
        }
    }

    @Override
    protected void setListener() {
        mBinding.tvLogin.setOnClickListener(this);
        mBinding.tvForgotPass.setOnClickListener(this);
        mBinding.etEmail.addTextChangedListener(emailWatcher);
        mBinding.etPhone.addTextChangedListener(phoneWatcher);
        mBinding.etPassword.addTextChangedListener(passwordWatcher);
        mBinding.etPhone.setOnFocusChangeListener(this);
        mBinding.etEmail.setOnFocusChangeListener(this);
        mBinding.etPassword.setOnFocusChangeListener(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBinding.etEmail != null)
            mBinding.etEmail.removeTextChangedListener(emailWatcher);

        if (mBinding.etPhone != null)
            mBinding.etPhone.removeTextChangedListener(phoneWatcher);

        if (mBinding.etPassword != null)
            mBinding.etPassword.removeTextChangedListener(passwordWatcher);
    }

    @Override
    public void spannableStringClick(int flag) {
        if (flag == SPAN_SIGN_UP) {
            getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.BUSINESS_SIZE, true, true);
        }
    }

    @Override
    public void OnLogin(String msg, SingupResponse model) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (model != null) {
            if (model.getOtp() != null) {
                getLoginActivity().isLogin = true;
                getLoginActivity().otp = model.getOtp();
                getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.VERIFY, true, true);
            }
        }
    }

    @Override
    public void OnEmailLogin(String msg, VerifyResponseModel model) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (model != null) {
            if (model.acsTkn != null) {
                Utility.putStringValueInSharedPreference(context, LOGIN_ACCESS_TOKEN, model.acsTkn);
                Utility.putIntValueInSharedPreference(context, AppConstants.ORG_TYPE,model.orgType);
                Utility.saveUserId(model._id, getContext());            }
            switch (model.code) {
                case 2:
                    if(model.orgType==2){
                        openDashBoard(model);
                    }else {
                        Bundle bundle = new Bundle();
                        bundle.putInt(IS_SMALL_ORG, model.orgType);
                        getLoginActivity().getPresenter().gotoFragmentWithBundle(BusinessLoginPresenter.DestinationFragment.COMPANY_NAME, true, true, bundle);
                    } break;

                case 3:
                    openDashBoard(model);
                    break;
                default:
                    if(model.orgType==2){
                        openDashBoard(model);
                    }else {
                        getLoginActivity().finish();
                        getLoginActivity().openActivity(LoginActivity.class, null);
                    }
                    break;

            }
        }
    }

    private void openDashBoard(VerifyResponseModel model) {
        if (getActivity() != null)
        Utility.saveProfile(getContext(),model,true);
        getLoginActivity().finishAffinity();
        getLoginActivity().openActivity(DashboardActivity.class, null);
    }

    @Override
    public void preLogin() {
        showProgress(true);
    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.LOGIN_TITLE);
    }

    private void setSpan() {
        new SpannableTxt().setSpannableString(getString(R.string.dont_have_account),
                mBinding.signupTxt,
                getResources().getColor(R.color.colorTextBlack),
                getResources().getColor(R.color.grey_6b6d6d),
                23, this, true, SPAN_SIGN_UP);
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.MOBILE_EMPTY)
            Utility.showToast(context, getResources().getString(R.string.please_enter_mobile_number));
        if (toastType == ToastType.PASSWORD_EMPTY)
            Utility.showToast(context, getResources().getString(R.string.please_enter_pass));
        if (toastType == ToastType.PASSWORD_ERROR)
            Utility.showToast(getContext(), getContext().getResources().getString(R.string.pass_length));
        if (toastType == ToastType.EMAIL_FORMAT_ERROR)
            Utility.showToast(context, getResources().getString(R.string.please_enter_valid_email));
        if (toastType == ToastType.EMAIL_EMPTY)
            Utility.showToast(context, getResources().getString(R.string.please_enter_email));
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.etEmail:
                mBinding.etPhone.removeTextChangedListener(phoneWatcher);
                mBinding.etPassword.removeTextChangedListener(passwordWatcher);
                mBinding.etEmail.addTextChangedListener(emailWatcher);
                break;
            case R.id.etPhone:
                mBinding.etEmail.removeTextChangedListener(emailWatcher);
                mBinding.etPassword.removeTextChangedListener(passwordWatcher);
                mBinding.etPhone.addTextChangedListener(phoneWatcher);
                break;
            case R.id.etPassword:
                mBinding.etPhone.removeTextChangedListener(phoneWatcher);
                mBinding.etEmail.addTextChangedListener(emailWatcher);
                mBinding.etPassword.addTextChangedListener(passwordWatcher);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getLoginActivity().enableSkip(false);

    }
}
