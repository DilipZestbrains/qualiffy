package com.app.qualiffyapp.ui.activities.dashboard.business.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.business.ViewerResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_VIEWER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MY_FOLLOWING;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MY_RECOMMENDATION;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_USER_VIEWER;

public class ViewerPresenter implements ApiListener<ResponseModel<ViewerResponseModel>> {
    private SignupView signupView;
    private SignUpInteractor interactor;

    public ViewerPresenter(SignupView signupView) {
        this.signupView = signupView;
        interactor = new SignUpInteractor();
    }

    public void hitViewerApi(String userId, int pageNo, boolean isShowProgress) {
        if (userId != null) {
            if (isShowProgress)
                signupView.showProgressDialog(true);
            interactor.businessViewerdApi(this, userId, pageNo, API_FLAG_BUSINESS_DASHBOARD_VIEWER);
        }
    }

    public void getRecommendList(boolean isShowProgress, int pageNo, int apiFlag) {
        if (isShowProgress)
            signupView.showProgressDialog(true);
        switch (apiFlag) {
            case API_FLAG_JS_MY_RECOMMENDATION:
                interactor.jobSeekerOwnRecmdation(this, pageNo, apiFlag);
                break;
            case API_FLAG_JS_MY_FOLLOWING:
                interactor.jobSeekerOwnFollowing(this, pageNo, apiFlag);

                break;
        }

    }

    public void getUserViewer(int pageNo) {
        signupView.showProgressDialog(true);
        interactor.getUserViewer(this, pageNo, API_FLAG_USER_VIEWER);
    }

    @Override
    public void onApiSuccess(ResponseModel<ViewerResponseModel> response, int apiFlag) {
        signupView.showProgressDialog(false);
        if (response != null)
            signupView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiError(error, apiFlag);
    }
}
