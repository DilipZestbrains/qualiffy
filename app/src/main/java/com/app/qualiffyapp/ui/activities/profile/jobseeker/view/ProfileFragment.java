package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.EducationAdapter;
import com.app.qualiffyapp.adapter.ExperienceAdapter;
import com.app.qualiffyapp.adapter.ImagesAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.constants.CompanyPortfolioType;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.FragmentProfileBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.createProfile.JobInfo;
import com.app.qualiffyapp.models.eventBusModel.Profile;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerProfileResponse;
import com.app.qualiffyapp.models.profile.jobseeker.User;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.WebViewActivity;
import com.app.qualiffyapp.ui.activities.dashboard.business.views.ViewerActivity;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JobSeekerProfilePresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.PreActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddCertificateActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddExperienceActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.JobTypeActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.LanguageActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.SkillActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserPhotoActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserVideoActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerEducation;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.greenrobot.eventbus.EventBus;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_FAQ;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_HELP;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_DELETE_ACCOUNT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OPEN_RELOCATE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_PROFILE_OFFLINE;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT_UNREGISTER;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.FAQ;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.HEADER;
import static com.app.qualiffyapp.constants.AppConstants.HELP;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUSTRY_;
import static com.app.qualiffyapp.constants.AppConstants.JS_JOB_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.JS_LANGS;
import static com.app.qualiffyapp.constants.AppConstants.JS_RECOMMANDATION;
import static com.app.qualiffyapp.constants.AppConstants.JS_PROFILE_PIC;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLE;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLESEEKING;
import static com.app.qualiffyapp.constants.AppConstants.JS_SKILLS;
import static com.app.qualiffyapp.constants.AppConstants.JS_VIEWER;
import static com.app.qualiffyapp.constants.AppConstants.NOTIFICATION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.PRIVACY_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.RELOCATE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_ADD_CERTIFICATE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_ADD_EXPERIENCE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JOB_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_LANGS;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_PROFILE_PIC;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_SETTING;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_SKILLS;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_UPDATE_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.THUMBNAIL;
import static com.app.qualiffyapp.constants.SharedPrefConstants.EXPERIENCE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.HAS_VIDEO;
import static com.app.qualiffyapp.constants.SharedPrefConstants.LAST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PROFILE_PIC;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;
import static com.app.qualiffyapp.customViews.DialogInitialize.DELETE_ACCOUNT_FLAG;
import static com.app.qualiffyapp.customViews.DialogInitialize.LOGOUT_FLAG;
import static com.app.qualiffyapp.customViews.DialogInitialize.MAKE_OFFLINE_FLAG;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JobSeekerProfilePresenter.REQUEST_CODE_ADD_EDUCATION;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JobSeekerProfilePresenter.REQUEST_CODE_UPDATE_IMAGES;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JobSeekerProfilePresenter.REQUEST_CODE_UPDATE_JOB_SEEKER_INFO;

public class ProfileFragment extends BaseFragment implements IJobSeekerProfile, yesNoCallback, CompoundButton.OnCheckedChangeListener {
    public static final String INFORMATION = "information";
    public static final String EDUCATION = "education";
    private final String EXPERIRNCE = "experience";
    private final String CERTIFICATE = "certificate";
    public final String VIDEO = "video";
    private final String VIDEO_BASE_URL = "videoBaseUrl";
    private final String MEDIA = "imgMedia";
    private FragmentProfileBinding mBinding;
    private JobSeekerProfilePresenter mPresenter;
    private ExperienceAdapter experienceAdapter;
    private EducationAdapter educationAdapter;
    private ExperienceAdapter certificateAdapter;
    private ExperienceAdapter skillsAdapter;
    private ExperienceAdapter languageAdapter;
    private ExperienceAdapter jobTypeAdapter;
    private ImagesAdapter imagesRecyclerView;
    private JobSeekerProfileResponse jobSeekerProfile;
    private DialogInitialize dialogInitialize;
    private com.app.qualiffyapp.firebase.model.User firebaseuser;
    private int viewerCount, frndCount, recommendCount;


    @Override
    protected void initUi() {
        mBinding = (FragmentProfileBinding) viewDataBinding;
        mPresenter = new JobSeekerProfilePresenter(this);
        firebaseuser = new com.app.qualiffyapp.firebase.model.User();
        mPresenter.fetchUserDetails();
        dialogInitialize = new DialogInitialize();
        createAndBindAdapters();
        setListeners();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onClick(View v) {
        Bundle bundle;

        switch (v.getId()) {
            case R.id.tvCertificateHeading:
            case R.id.ivECCertificate:
                handleExpand(v, mBinding.rvCertificate);
                break;
            case R.id.tvEducationHeading:
            case R.id.ivECEducation:
                handleExpand(v, mBinding.rvEducation);
                break;

            case R.id.tvIntroductionHeading:
            case R.id.ivECIntroducation:
                handleExpand(v, mBinding.tvIntroduction);
                break;

            case R.id.tvImageHeading:
            case R.id.ivECImages:
                handleExpand(v, mBinding.imgRlayout);
                break;

            case R.id.tvVideoHeading:
            case R.id.ivECVideo:
                handleExpand(v, mBinding.ivVideo);
                break;

            case R.id.tvExperienceHeading:
            case R.id.ivECExperience:
                handleExpand(v, mBinding.rvExperience);
                break;

            case R.id.tvSkillsHeading:
            case R.id.ivEcSkills:
                handleExpand(v, mBinding.rvSkills);
                break;

            case R.id.lngsHeading:
            case R.id.ivEclngs:
                handleExpand(v, mBinding.rvlngs);

                break;

            case R.id.tvjobTypeHeading:
            case R.id.ivECjobType:
                handleExpand(v, mBinding.rvjobType);
                break;


            case R.id.ivEditPencilIntroduction:
                mPresenter.editInfo(mBinding.tvIntroduction.getText().toString());
                break;
            case R.id.ivVideo:
                if (jobSeekerProfile != null) {
                    bundle = new Bundle();
                    if (jobSeekerProfile.prfl.video != null && jobSeekerProfile.bP != null) {
                        bundle.putString(VIDEO, jobSeekerProfile.prfl.video);
                        bundle.putString(VIDEO_BASE_URL, jobSeekerProfile.bP);
                    }
                    bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                    openActivityForResult(UserVideoActivity.class, bundle, REQUEST_CODE_UPDATE_VIDEO);
                }
                break;
            case R.id.ivEditExperience:
                bundle = new Bundle();
                bundle.putString(EXPERIRNCE, new Gson().toJson(jobSeekerProfile.expInfo));
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(AddExperienceActivity.class, bundle, REQUEST_CODE_ADD_EXPERIENCE);
                break;

            case R.id.ivEditSkills:
                bundle = new Bundle();
                bundle.putStringArrayList(JS_SKILLS, jobSeekerProfile.prfl.skills);
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(SkillActivity.class, bundle, REQUEST_CODE_SKILLS);
                break;

            case R.id.ivEditlngs:
                bundle = new Bundle();
                if (jobSeekerProfile.prfl.lngs != null && jobSeekerProfile.prfl.lngs.size() > 0)
                    bundle.putStringArrayList(JS_LANGS, jobSeekerProfile.prfl.lngs);
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(LanguageActivity.class, bundle, REQUEST_CODE_LANGS);
                break;

            case R.id.ivEditjobType:
                bundle = new Bundle();
                bundle.putStringArrayList(JS_JOB_TYPE, jobSeekerProfile.prfl.job_type);
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(JobTypeActivity.class, bundle, REQUEST_CODE_JOB_TYPE);
                break;


            case R.id.ivEditCertificate:
                bundle = new Bundle();
                bundle.putStringArrayList(CERTIFICATE, jobSeekerProfile.crt);
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(AddCertificateActivity.class, bundle, REQUEST_CODE_ADD_CERTIFICATE);
                break;
            case R.id.ivAddEducation:
                mPresenter.addEducation();
                break;

            case R.id.tvManageNotification:
                openNotificationActivity(jobSeekerProfile.usr.notif);
                break;

            case R.id.tvLogOut:
                dialogInitialize.prepareLogoutDialog(new CustomDialogs(getActivity(), this));
                break;
            case R.id.tvDeleteAccount:
                if (jobSeekerProfile.usr.offline || mBinding.toggleMakeProfileOffline.isChecked())
                    dialogInitialize.prepareDeleteDialog(new CustomDialogs(getActivity(), this));
                else dialogInitialize.makeOfflineDialog(new CustomDialogs(getActivity(), this));
                break;

            case R.id.add_img_card:
                LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
                for (int i = 0; i < jobSeekerProfile.prfl.imgs.size(); i++) {
                    linkedHashMap.put(jobSeekerProfile.prfl.imgs.get(i), jobSeekerProfile.bP);
                }
                mPresenter.openEditImageScreen(jobSeekerProfile.bP, linkedHashMap);
                break;

            case R.id.tvMySubscription:
                openActivity(JsSubscriptionActivity.class, null);
                break;
            case R.id.ivRecommand:
                if ( recommendCount== 0) {
                if (getContext() != null)
                    Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_recommend_for_you));
                return;
            }
                bundle = new Bundle();
                bundle.putInt(FROM, JS_RECOMMANDATION);
                openActivity(ViewerActivity.class, bundle);
                break;
   /*         case R.id.tvMyFollowing:
                bundle = new Bundle();
                bundle.putInt(FROM, JS_FOLLOWING);
                openActivity(ViewerActivity.class, bundle);
                break;

        */    case R.id.tvPrivacySetting:
                openPrivacyActivity(jobSeekerProfile.usr.pub);
                break;

            case R.id.tvFAQ:
                mPresenter.getCompanyPortfolio(CompanyPortfolioType.FAQ, API_FLAG_BUSINESS_FAQ);
                break;
            case R.id.tvHelp:
                mPresenter.getCompanyPortfolio(CompanyPortfolioType.HELP, API_FLAG_BUSINESS_HELP);
                break;
            case R.id.tvSetting:
                openSetting();
                break;
            case R.id.editUserProfile:
                openProfilePicActivity();
                break;
            case R.id.tvMyPayments:
                openActivity(PaymentHistoryActivity.class, null);
                break;
            case R.id.ivViewer:
                if (viewerCount == 0) {
                    if (getContext() != null)
                        Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_viewer_for_you));
                    return;
                }
                Bundle b1 = new Bundle();
                b1.putInt(FROM, JS_VIEWER);
                openActivity(ViewerActivity.class, b1);
                break;
            case R.id.ivConnection:
                if (frndCount == 0) {
                    if (getContext() != null)
                        Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_frnd_for_you));
                    return;
                }
                Bundle b = new Bundle();
                b.putSerializable(FROM, RecommendListActivity.DataType.JOBSEEKER_CONNECTION);
                openActivity(RecommendListActivity.class, b);
                break;
        }
    }

    private void openProfilePicActivity() {
        Bundle bundle = new Bundle();
        bundle.putString(JS_PROFILE_PIC, jobSeekerProfile.bP + jobSeekerProfile.prfl.img);
        bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
        openActivityToUpdate(UserPhotoActivity.class, bundle, REQUEST_CODE_PROFILE_PIC);
    }

    private void openSetting() {
        Bundle bundle = new Bundle();
        if (jobSeekerProfile.prfl.jobCInfo.size() > 0)
            bundle.putString(JS_INDUSTRY_, Utility.gsonInstance().toJson(jobSeekerProfile.prfl.jobCInfo.get(0)));
        if (jobSeekerProfile.prfl.jobTInfo.size() > 0)
            bundle.putString(JS_ROLE, Utility.gsonInstance().toJson(jobSeekerProfile.prfl.jobTInfo.get(0)));
        if (jobSeekerProfile.prfl.rolSkngInfo.size() > 0)
            bundle.putString(JS_ROLESEEKING, Utility.gsonInstance().toJson(jobSeekerProfile.prfl.rolSkngInfo));
        openActivityToUpdate(SettingActivity.class, bundle, REQUEST_CODE_SETTING);

    }

    private void openNotificationActivity(User.NotifBean notificationStatus) {
        Bundle bundle = new Bundle();
        bundle.putString(FROM, ProfileFragment.class.getName());
        bundle.putString(DATA_TO_SEND, new Gson().toJson(notificationStatus));
        openActivityToUpdate(JsManageNotificationActivity.class, bundle, NOTIFICATION_REQUEST_CODE);
    }

    private void openPrivacyActivity(boolean privacyStatus) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(DATA_TO_SEND, privacyStatus);
        openActivityToUpdate(PrivacyActivity.class, bundle, PRIVACY_REQUEST_CODE);
    }


    private void setListeners() {
        mBinding.ivECCertificate.setOnClickListener(this);
        mBinding.ivECEducation.setOnClickListener(this);
        mBinding.ivECExperience.setOnClickListener(this);
        mBinding.ivECImages.setOnClickListener(this);
        mBinding.ivECIntroducation.setOnClickListener(this);
        mBinding.ivECVideo.setOnClickListener(this);
        mBinding.ivEditPencilIntroduction.setOnClickListener(this);
        mBinding.ivVideo.setOnClickListener(this);
        mBinding.ivEditExperience.setOnClickListener(this);
        mBinding.ivEditSkills.setOnClickListener(this);
        mBinding.ivEditCertificate.setOnClickListener(this);
        mBinding.ivAddEducation.setOnClickListener(this);
        mBinding.tvManageNotification.setOnClickListener(this);
        mBinding.tvLogOut.setOnClickListener(this);
        mBinding.addImgCard.setOnClickListener(this);
        mBinding.tvMySubscription.setOnClickListener(this);
        mBinding.tvPrivacySetting.setOnClickListener(this);
        mBinding.tvDeleteAccount.setOnClickListener(this);
        mBinding.tvFAQ.setOnClickListener(this);
        mBinding.tvHelp.setOnClickListener(this);
        mBinding.tvSetting.setOnClickListener(this);
        mBinding.tvIntroductionHeading.setOnClickListener(this);
        mBinding.tvImageHeading.setOnClickListener(this);
        mBinding.tvVideoHeading.setOnClickListener(this);
        mBinding.tvExperienceHeading.setOnClickListener(this);
        mBinding.tvEducationHeading.setOnClickListener(this);
        mBinding.tvCertificateHeading.setOnClickListener(this);
        mBinding.editUserProfile.setOnClickListener(this);
        mBinding.ivEcSkills.setOnClickListener(this);
        mBinding.ivEditSkills.setOnClickListener(this);
        mBinding.tvSkillsHeading.setOnClickListener(this);
        mBinding.ivEclngs.setOnClickListener(this);
        mBinding.ivEditlngs.setOnClickListener(this);
        mBinding.lngsHeading.setOnClickListener(this);
        mBinding.ivECjobType.setOnClickListener(this);
        mBinding.tvjobTypeHeading.setOnClickListener(this);
        mBinding.ivEditjobType.setOnClickListener(this);
        mBinding.tvMyPayments.setOnClickListener(this);
        mBinding.ivRecommand.setOnClickListener(this);
//        mBinding.tvMyFollowing.setOnClickListener(this);
        mBinding.ivViewer.setOnClickListener(this);
        mBinding.ivConnection.setOnClickListener(this);
    }


    @Override
    public void onFetchProfileDetails(JobSeekerProfileResponse profile) {

        showProgress(false);
        if (profile != null) {
            AppFirebaseDatabase.getInstance().updateUser(TextUtils.isEmpty(profile.usr.username)?profile.prfl.f_name+" "+profile.prfl.l_name:profile.usr.username, profile.bP + profile.prfl.img, profile.usr.pno);
            if (getContext() != null)
                Utility.putStringValueInSharedPreference(getContext(), UID, profile.usr._id);

            this.jobSeekerProfile = profile;
            if (profile.prfl.img != null) {
                firebaseuser.setImg(profile.bP + profile.prfl.img);
                GlideUtil.loadCircularImage(mBinding.ivProfilePic, profile.bP + profile.prfl.img, R.drawable.profile_placeholder);
            } else setProfilePic();
            if (profile.usr.pno != null) {
                firebaseuser.setPhone(profile.usr.pno);
            }
            if (profile.usr.username != null && !profile.usr.username.equals("")) {
                firebaseuser.setName(profile.usr.username);
                mBinding.tvName.setText(profile.usr.username);
            } else getName();
            if (profile.prfl.state != null)
                mBinding.tvAddress.setText(profile.prfl.state);
            if (profile.expInfo.exps != null) {
                List<String> title = new ArrayList<>();
                for (ExpDetailBean detailBean : profile.expInfo.exps)
                    title.add(detailBean.title);
                experienceAdapter.refreshList(title);
            }
            if (profile.crt != null)
                certificateAdapter.refreshList(profile.crt);

            if (profile.prfl.skills != null)
                skillsAdapter.refreshList(profile.prfl.skills);

            if (profile.prfl.lngs != null)
                languageAdapter.refreshList(profile.prfl.lngs);

            if (profile.prfl.job_type != null && profile.prfl.job_type.size() > 0)
                jobTypeAdapter.refreshList(profile.prfl.job_type);
//                mBinding.rvjobType.setText(profile.prfl.job_type);


            if (profile.prfl.imgs != null) {
            /*    if (profile.prfl.imgs.size() >= 10)
                    mBinding.addImgCard.setVisibility(View.GONE);
                else*/
                mBinding.addImgCard.setVisibility(View.VISIBLE);
                imagesRecyclerView.refreshList(profile.prfl.imgs, profile.bP);
            }
            if (profile.prfl.edu != null)
                educationAdapter.refreshList(profile.prfl.edu);
            if (profile.prfl.video != null)
                GlideUtil.loadImage(mBinding.ivVideo, profile.bP + profile.prfl.video, R.drawable.ic_video_placeholder, 1000, 28);
            if (profile.prfl.bio != null)
                mBinding.tvIntroduction.setText(profile.prfl.bio);


            mBinding.toggleOpenToRelocate.setChecked(profile.prfl.reloc);
            mBinding.toggleMakeProfileOffline.setChecked(profile.usr.offline);


            mBinding.toggleOpenToRelocate.setOnCheckedChangeListener(this);
            mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(this);

            mBinding.tvRecommend.setText(String.valueOf(profile.r_count));
            mBinding.tvViewCount.setText(String.valueOf(profile.v_count));
            mBinding.tvConnection.setText(String.valueOf(profile.f_count));


            viewerCount = profile.v_count;
            frndCount = profile.f_count;
            recommendCount = profile.r_count;


        }

    }


    private void setProfilePic() {
        if (!Utility.getStringSharedPreference(getContext(), PROFILE_PIC).equals("")) {
            firebaseuser.setImg(Utility.getStringSharedPreference(getContext(), PROFILE_PIC));
            GlideUtil.loadCircularImage(mBinding.ivProfilePic,
                    Utility.getStringSharedPreference(getContext(), PROFILE_PIC),
                    R.drawable.profile_placeholder
            );
        }
    }

    private void getName() {
        String name = "";
        if (!Utility.getStringSharedPreference(getContext(), FIRST_NAME).equals(""))
            name = Utility.getStringSharedPreference(getContext(), FIRST_NAME);
        if (Utility.getStringSharedPreference(getContext(), LAST_NAME) != null && !Utility.getStringSharedPreference(getContext(), LAST_NAME).equals(""))
            name = Utility.getStringSharedPreference(getContext(), FIRST_NAME) + " " + Utility.getStringSharedPreference(getContext(), LAST_NAME);


        mBinding.tvName.setText(name);
    }


    @Override
    public void preFetch() {
        showProgress(true);
    }

    @Override
    public void setExpandState(ViewType type, ViewVisibility viewVisibility) {

    }


    private void handleExpand(View imgExpan, View expandedView) {
        if (imgExpan.isSelected()) {
            expandedView.setVisibility(View.GONE);
            imgExpan.setSelected(false);
        } else {
            expandedView.setVisibility(View.VISIBLE);
            imgExpan.setSelected(true);

        }
    }

    @Override
    public void expandableMode(ViewType type, ViewVisibility visibility) {
        switch (type) {
            case INTRODUCTION:
                mBinding.ivECIntroducation.setVisibility(visibility.getVisibility());
                break;
            case IMAGES:
                mBinding.ivECImages.setVisibility(visibility.getVisibility());
                break;
            case VIDEO:
                mBinding.ivECVideo.setVisibility(visibility.getVisibility());
                break;
            case EXPERIENCE:
                mBinding.ivECExperience.setVisibility(visibility.getVisibility());
                break;
            case CERTIFICATE:
                mBinding.ivECCertificate.setVisibility(visibility.getVisibility());
                break;
            case EDUCATION:
                mBinding.ivECEducation.setVisibility(visibility.getVisibility());
                break;
            case SKILLS:
                mBinding.ivEcSkills.setVisibility(visibility.getVisibility());
                break;
            case LANGUAGES:
                mBinding.ivEclngs.setVisibility(visibility.getVisibility());
                break;
            case JOB_TYPE:
                mBinding.ivECjobType.setVisibility(visibility.getVisibility());
                break;
        }
    }

    @Override
    public void openActivityToUpdate(Class<?> screen, Bundle bundle, int requestCode) {
        openActivityForResult(screen, bundle, requestCode);
    }

    @Override
    public void onFetchPortfolio(String msg, WebResponseModel model, int apiFlag) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (model != null) {
            Bundle bundle = new Bundle();
            bundle.putString(DATA_TO_SEND, model.webpage.content);
            switch (apiFlag) {
                case API_FLAG_BUSINESS_FAQ:
                    bundle.putString(HEADER, FAQ);
                    break;
                case API_FLAG_BUSINESS_HELP:
                    bundle.putString(HEADER, HELP);
                    break;

            }
            openActivity(WebViewActivity.class, bundle);
        }
    }

    @Override
    public void logoutResponse(int apiFlag) {
        showProgress(false);
        if (apiFlag == API_FLAG_JS_DELETE_ACCOUNT) {

        }
        Utility.clearAllSharedPrefData(ApplicationController.getApplicationInstance());
        AppFirebaseDatabase.clearFirebaseInstance();
        if (getContext() != null)
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(CALL_INTENT_UNREGISTER));
        openActivity(PreActivity.class, null);
        if (getActivity() != null)
            getActivity().finishAffinity();
    }

    @Override
    public void apiError(String msg) {
        showProgress(false);
        Utility.showToast(getContext(), msg);
    }

    @Override
    public void toogleResponse(String msg, int status, int apiFlag) {
        showProgress(false);

        switch (apiFlag) {
            case API_FLAG_JS_OPEN_RELOCATE:
                if (status != SUCCESS)
                    mBinding.toggleOpenToRelocate.setChecked(!mBinding.toggleOpenToRelocate.isChecked());
                mBinding.toggleOpenToRelocate.setOnCheckedChangeListener(this);

                break;
            case API_FLAG_JS_PROFILE_OFFLINE:
                if (status != SUCCESS) {
                    mBinding.toggleMakeProfileOffline.setChecked(!mBinding.toggleMakeProfileOffline.isChecked());
                } else jobSeekerProfile.usr.offline = mBinding.toggleMakeProfileOffline.isChecked();
                mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(this);
                break;
        }
    }


    private void createAndBindAdapters() {
        experienceAdapter = new ExperienceAdapter(null);
        educationAdapter = new EducationAdapter(null);
        skillsAdapter = new ExperienceAdapter(null);
        languageAdapter = new ExperienceAdapter(null);
        jobTypeAdapter = new ExperienceAdapter(null);
        educationAdapter.setEducationEditListener(mPresenter);
        certificateAdapter = new ExperienceAdapter(null);
        imagesRecyclerView = new ImagesAdapter(new LinkedList<>(), null);
        imagesRecyclerView.setAddImageListener(mPresenter);
        mBinding.rvImages.setAdapter(imagesRecyclerView);
        mBinding.rvExperience.setAdapter(experienceAdapter);
        mBinding.rvEducation.setAdapter(educationAdapter);
        mBinding.rvCertificate.setAdapter(certificateAdapter);
        mBinding.rvlngs.setAdapter(languageAdapter);
        mBinding.rvjobType.setAdapter(jobTypeAdapter);
        mBinding.rvSkills.setAdapter(skillsAdapter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_UPDATE_JOB_SEEKER_INFO:
                    if (data != null && data.getExtras() != null) {
                        String info = data.getExtras().getString(INFORMATION);
                        mBinding.tvIntroduction.setText(info);
                        jobSeekerProfile.prfl.bio = info;
                        expandableMode(ViewType.INTRODUCTION, ViewVisibility.MAKE_VISIBLE);

                    }
                    break;
                case REQUEST_CODE_ADD_EDUCATION:
                    if (data != null && data.getExtras() != null) {
                        IJobSeekerEducation.Action action = (IJobSeekerEducation.Action) data.getSerializableExtra(IJobSeekerEducation.ACTION);
                        JobSeekerEducation edu = data.getExtras().getParcelable(EDUCATION);
                        if (edu != null && action != null) {
                            switch (action) {
                                case ADD:
                                    educationAdapter.addEducation(edu);
                                    expandableMode(ViewType.EDUCATION, ViewVisibility.MAKE_VISIBLE);
                                    break;
                                case DELETE:
                                    educationAdapter.deleteEducation(edu);
                                    break;
                                case UPDATE:
                                    educationAdapter.updateEducation(edu);
                                    break;
                            }

                        }


                    }
                    break;
                case REQUEST_CODE_ADD_EXPERIENCE:
                    if (data != null && data.getExtras() != null) {
                        jobSeekerProfile.expInfo = new Gson().fromJson(data.getStringExtra(EXPERIRNCE), ExpInfoBean.class);
                        if (jobSeekerProfile.expInfo.exps != null) {
                            Utility.putStringValueInSharedPreference(context, EXPERIENCE, new Gson().toJson(jobSeekerProfile.expInfo));
                            List<String> title = new ArrayList<>();
                            for (ExpDetailBean detailBean : jobSeekerProfile.expInfo.exps)
                                title.add(detailBean.title);
                            experienceAdapter.refreshList(title);
                            if (jobSeekerProfile.expInfo.exps.size() > 0)
                                expandableMode(ViewType.EXPERIENCE, ViewVisibility.MAKE_VISIBLE);
                            else
                                expandableMode(ViewType.EXPERIENCE, ViewVisibility.MAKE_GONE);
                        }
                    }
                    break;
                case REQUEST_CODE_ADD_CERTIFICATE:
                    if (data != null && data.getExtras() != null) {
                        jobSeekerProfile.crt = data.getStringArrayListExtra(CERTIFICATE);
                        certificateAdapter.refreshList(jobSeekerProfile.crt);
                        if (jobSeekerProfile.crt.size() > 0)
                            expandableMode(ViewType.CERTIFICATE, ViewVisibility.MAKE_VISIBLE);
                        else
                            expandableMode(ViewType.CERTIFICATE, ViewVisibility.MAKE_GONE);

                    }
                    break;

                case REQUEST_CODE_UPDATE_IMAGES:
                    if (data != null && data.getExtras() != null) {
                        LinkedList<String> imgs = Utility.gsonInstance().fromJson(data.getStringExtra(MEDIA), LinkedList.class);

                        jobSeekerProfile.prfl.imgs = imgs;
                        imagesRecyclerView.refreshList(imgs, jobSeekerProfile.bP);

                    }
                    break;

                case NOTIFICATION_REQUEST_CODE:
                    User.NotifBean notifBean = new Gson().fromJson(data.getStringExtra(DATA_TO_SEND), User.NotifBean.class);
                    if (notifBean != null)
                        jobSeekerProfile.usr.notif = notifBean;

                    break;
                case PRIVACY_REQUEST_CODE:
                    jobSeekerProfile.usr.pub = data.getBooleanExtra(DATA_TO_SEND, false);

                    break;

                case REQUEST_CODE_SETTING:
                    if (data.getStringExtra(JS_INDUSTRY_) != null) {
                        ArrayList<JobInfo> ijobInfo = new ArrayList<>();
                        ijobInfo.add(new Gson().fromJson(data.getStringExtra(JS_INDUSTRY_), JobInfo.class));
                        jobSeekerProfile.prfl.jobCInfo = ijobInfo;
                    }

                    if (data.getStringExtra(JS_ROLE) != null) {
                        ArrayList<JobInfo> rjobInfo = new ArrayList<>();
                        rjobInfo.add(new Gson().fromJson(data.getStringExtra(JS_ROLE), JobInfo.class));
                        jobSeekerProfile.prfl.jobTInfo = rjobInfo;
                    } else {
                        if (jobSeekerProfile.prfl.jobTInfo != null)
                            jobSeekerProfile.prfl.jobTInfo.clear();
                    }

                    if (data.getStringExtra(JS_ROLESEEKING) != null) {
                        Type roleSeektype = new TypeToken<ArrayList<JobInfo>>() {
                        }.getType();

                        jobSeekerProfile.prfl.rolSkngInfo = new Gson().fromJson(data.getStringExtra(JS_ROLESEEKING), roleSeektype);
                    }
                    break;

                case REQUEST_CODE_UPDATE_VIDEO:
                    jobSeekerProfile.prfl.video = data.getStringExtra(THUMBNAIL);
                    if (TextUtils.isEmpty(jobSeekerProfile.prfl.video)) {
                        if (getContext() != null)
                            Utility.putBooleanValueInSharedPreference(getContext(), HAS_VIDEO, false);
                        mBinding.ivVideo.setImageResource(R.drawable.ic_video_placeholder);

                    } else {
                        GlideUtil.loadImage(mBinding.ivVideo, jobSeekerProfile.bP + jobSeekerProfile.prfl.video, R.drawable.ic_video_placeholder, 1000, 28);
                        if (getContext() != null)
                            Utility.putBooleanValueInSharedPreference(getContext(), HAS_VIDEO, true);
                    }
                    break;

                case REQUEST_CODE_PROFILE_PIC:
                    jobSeekerProfile.prfl.img = data.getStringExtra(JS_PROFILE_PIC);
                    GlideUtil.loadCircularImage(mBinding.ivProfilePic, jobSeekerProfile.bP + jobSeekerProfile.prfl.img, R.drawable.profile_placeholder);
                    fireEvent(null, jobSeekerProfile.bP + jobSeekerProfile.prfl.img);
                    break;

                case REQUEST_CODE_SKILLS:
                    jobSeekerProfile.prfl.skills = data.getStringArrayListExtra(JS_SKILLS);
                    skillsAdapter.refreshList(jobSeekerProfile.prfl.skills);
                    if (jobSeekerProfile.prfl.skills.size() > 0)
                        expandableMode(ViewType.SKILLS, ViewVisibility.MAKE_VISIBLE);
                    else
                        expandableMode(ViewType.SKILLS, ViewVisibility.MAKE_GONE);
                    break;
                case REQUEST_CODE_LANGS:
                    jobSeekerProfile.prfl.lngs = data.getStringArrayListExtra(JS_LANGS);
                    languageAdapter.refreshList(jobSeekerProfile.prfl.lngs);
                    if (jobSeekerProfile.prfl.lngs.size() > 0)
                        expandableMode(ViewType.LANGUAGES, ViewVisibility.MAKE_VISIBLE);
                    else
                        expandableMode(ViewType.LANGUAGES, ViewVisibility.MAKE_GONE);
                    break;

                case REQUEST_CODE_JOB_TYPE:
                    jobSeekerProfile.prfl.job_type = data.getStringArrayListExtra(JS_JOB_TYPE);
                    jobTypeAdapter.refreshList(jobSeekerProfile.prfl.job_type);
                    if (jobSeekerProfile.prfl.job_type.size() > 0)
                        expandableMode(ViewType.JOB_TYPE, ViewVisibility.MAKE_VISIBLE);
                    else
                        expandableMode(ViewType.JOB_TYPE, ViewVisibility.MAKE_GONE);

                    break;

            }

        }

    }


    @Override
    public void onYesClicked(String from) {
        switch (from) {
            case LOGOUT_FLAG:
                preFetch();
                mPresenter.logoutHitApi();
                AppRepository.getInstance(getContext()).deleteContacts();
                break;
            case DELETE_ACCOUNT_FLAG:
                preFetch();
                mPresenter.deleteAccountHitApi();
                break;
            case MAKE_OFFLINE_FLAG:
                preFetch();
                mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(null);
                mBinding.toggleMakeProfileOffline.setChecked(true);
                mPresenter.hitapiForToogles(mBinding.toggleMakeProfileOffline.isChecked(), API_FLAG_JS_PROFILE_OFFLINE);
                break;

        }


    }

    @Override
    public void onNoClicked(String from) {
        switch (from) {
            case MAKE_OFFLINE_FLAG:
                preFetch();
                mPresenter.deleteAccountHitApi();
                break;
        }


    }

    private void fireEvent(String name, String img) {
        Profile profile = new Profile();
        boolean isReady = false;
        if (name != null) {
            profile.setName(name);
            isReady = true;
        }
        if (img != null) {
            profile.setImg(img);
            isReady = true;
        }
        if (isReady)
            EventBus.getDefault().postSticky(profile);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {

            case R.id.toggleOpenToRelocate:
                preFetch();
                mPresenter.hitapiForToogles(isChecked, RELOCATE);
                mBinding.toggleOpenToRelocate.setOnCheckedChangeListener(null);
                break;

            case R.id.toggleMakeProfileOffline:
                preFetch();
                mPresenter.hitapiForToogles(isChecked, API_FLAG_JS_PROFILE_OFFLINE);
                mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(null);
                break;

        }
    }

    @Override
    public void openActivity(String baseUrl, LinkedList<String> imgs, int pos) {
        Intent viewMedia = new Intent(getContext(), StoryViewActivity.class);
        viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(imgs));
        viewMedia.putExtra("BASE_URL_IMAGE", baseUrl);
        startActivity(viewMedia);
    }


}
