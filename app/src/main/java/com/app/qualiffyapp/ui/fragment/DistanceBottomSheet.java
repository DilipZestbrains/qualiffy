package com.app.qualiffyapp.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.BottomSheetDistanceDialogBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;


public class DistanceBottomSheet extends BottomSheetDialogFragment {
    private BottomSheetDistanceDialogBinding mBinding;
    private final int DEFAULT_DISTANCE = 50;
    private DistanceFilterListener distanceFilterListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_distance_dialog, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initView();
        setListener();
    }

    private void initView() {
        getBundleData();
    }

    public void setDistanceFilterListener(DistanceFilterListener distanceFilterListener) {
        this.distanceFilterListener = distanceFilterListener;
    }

    private void getBundleData() {
        if (distanceFilterListener != null) {
            mBinding.distanceSeekbar.setProgress(distanceFilterListener.getDistance());
            mBinding.tvDistance.setText(String.valueOf(distanceFilterListener.getDistance()));
        } else setDefault();
    }


    private void setDefault() {
        mBinding.distanceSeekbar.setProgress(DEFAULT_DISTANCE);
        mBinding.tvDistance.setText(String.valueOf(DEFAULT_DISTANCE));
    }

    private void setListener() {
        mBinding.tvApply.setOnClickListener(this::applyFilter);
        mBinding.distanceSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mBinding.tvDistance.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void applyFilter(View v) {
        if (distanceFilterListener != null) {
            try {
                String d = mBinding.tvDistance.getText().toString();
                int distance = Integer.parseInt(d);
                distanceFilterListener.onApplyFilter(distance);

            } catch (Exception e) {
                distanceFilterListener.onApplyFilter(DEFAULT_DISTANCE);
            }
            dismiss();
        }
    }

    public interface DistanceFilterListener {
        void onApplyFilter(int distance);
        int getDistance();
    }


}
