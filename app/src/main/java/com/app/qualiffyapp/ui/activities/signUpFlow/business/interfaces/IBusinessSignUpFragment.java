package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IBusinessSignUpFragment extends BaseInterface {
    void onLogin(String message, String orgID);

    void onPreLogin();


}
