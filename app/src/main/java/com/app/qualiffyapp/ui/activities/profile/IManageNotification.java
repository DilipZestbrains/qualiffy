package com.app.qualiffyapp.ui.activities.profile;

public interface IManageNotification {
    void showProgressDialog();

    void onUpdateNotification(String msg, int status, int apiFlag);
}
