package com.app.qualiffyapp.ui.activities;

import android.os.Bundle;
import android.text.Html;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityWebviewBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.network.AppRetrofit;
import com.app.qualiffyapp.utils.Utility;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.HEADER;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class WebViewActivity extends BaseActivity {
    private ActivityWebviewBinding mBinding;

    @Override
    protected void initView() {
        mBinding = (ActivityWebviewBinding) viewDataBinding;
        setIntentData();
    }

    private void setIntentData() {
        Bundle bundle;
        if ((bundle = getIntent().getBundleExtra(BUNDLE)) != null) {
            String data = bundle.getString(DATA_TO_SEND, null);
            if (data != null)
                mBinding.webView.setText(Html.fromHtml(data));
            String header = bundle.getString(HEADER, null);
            if (header != null) {
                mBinding.header.toolbarTitleTxt.setText(header);
            }
        } else {
            if (Utility.isNetworkAvailable(this)) {
                mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.terms_and_conditions));
                callApiGetTermsOfUse();
            } else {
                Utility.showToast(this, getResources().getString(R.string.internet_check));
            }
        }
    }

    private void callApiGetTermsOfUse() {
        AppRetrofit.getInstance().apiServices.getCompanyPortfolio("toc").enqueue(new Callback<ResponseModel<WebResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<WebResponseModel>> call, Response<ResponseModel<WebResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        mBinding.webView.setText(Html.fromHtml(response.body().getRes().webpage.content));
                    } else if (response.body().err != null) {
                        Utility.showToast(WebViewActivity.this,
                                getResources().getString(R.string.places_try_again));
                    } else {
                        Utility.showToast(WebViewActivity.this,
                                getResources().getString(R.string.places_try_again));

                    }
                } else {
                    Utility.showToast(WebViewActivity.this,
                            getResources().getString(R.string.places_try_again));

                }
            }

            @Override
            public void onFailure(Call<ResponseModel<WebResponseModel>> call, Throwable t) {
                Utility.showToast(WebViewActivity.this, t.getLocalizedMessage());
            }
        });
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_webview;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_btn_img) {
            onBackPressed();
        }
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
    }
}
