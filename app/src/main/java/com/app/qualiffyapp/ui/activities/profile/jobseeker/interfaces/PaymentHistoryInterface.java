package com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces;

import com.app.qualiffyapp.models.payment.PaymentHistoryResponse;

public interface PaymentHistoryInterface {
    void onPaymentFetch(PaymentHistoryResponse res, String msg);
}
