package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityJobSeekerEducationBinding;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.ProfileFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobSeekerEducationPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerEducation;
import com.app.qualiffyapp.utils.Utility;

public class JobSeekerEducationActivity extends BaseActivity implements IJobSeekerEducation {
    private ActivityJobSeekerEducationBinding mBinding;
    private JobSeekerEducationPresenter mPresenter;
    private String eduID = null;
    private JobSeekerEducation educationDetail;

    @Override
    protected void initView() {
        mBinding = (ActivityJobSeekerEducationBinding) viewDataBinding;
        mPresenter = new JobSeekerEducationPresenter(this);
        setHeader(true);

        mPresenter.getIntentData(getIntent());

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_job_seeker_education;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.tvDone:
                mPresenter.addEducation(mBinding.etUniversityName.getText().toString(), mBinding.etCity.getText().toString(), mBinding.etSession.getText().toString(),mBinding.etMajor.getText().toString(), eduID);
                break;
            case R.id.skip_txt:
                preAdd();
                mPresenter.deleteEducation(eduID);
        }
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
        mBinding.header.skipTxt.setOnClickListener(this);

    }

    private void setHeader(boolean isAddEdu) {
        if(isAddEdu)
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.add_education));
      else  mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.edit_education));
    }

    @Override
    public void showToast(ToastType toastType) {
        switch (toastType) {
            case EDUCATION_NAME_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_education_name));
                break;
            case EDUCATION_SESSION_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_education_session));

                break;
            case EDUCATION_ADDRESS_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_education_address));
                break;
        }
    }

    @Override
    public void preAdd() {
        showProgress(true);
    }

    @Override
    public void onAddEducation(String msg, JobSeekerEducation edu, Action action) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (edu != null) {
            Intent intent = new Intent();
            intent.putExtra(ACTION, action);
            if (action == Action.DELETE)
                intent.putExtra(ProfileFragment.EDUCATION, educationDetail);
            else
                intent.putExtra(ProfileFragment.EDUCATION, edu);
            setResult(Activity.RESULT_OK, intent);
            finish();

        }
    }

    @Override
    public void applyDeleteAction() {
        mBinding.header.skipTxt.setVisibility(View.VISIBLE);
        mBinding.header.skipTxt.setText(getResources().getString(R.string.delete));
    }


    @Override
    public void fillDetails(JobSeekerEducation edu) {
        this.educationDetail = edu;
        mBinding.etCity.setText(edu.addr);
        mBinding.etSession.setText(edu.sess);
        mBinding.etUniversityName.setText(edu.name);
        mBinding.etMajor.setText(edu.major);
        eduID = edu._id;
        mBinding.etUniversityName.requestFocus();
        setHeader(false);
    }


}
