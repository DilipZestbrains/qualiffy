package com.app.qualiffyapp.ui.activities.addedJob.views;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.addedJob.JobApplicantAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.addedJob.AddedJobItemClick;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityApplicantsBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.addJob.business.views.AddJobFragment;
import com.app.qualiffyapp.ui.activities.addedJob.presenter.ApplicantsPresenter;
import com.app.qualiffyapp.utils.EndlessRecyclerViewScrollListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.downloadFile.DownloadFiles;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;

import org.w3c.dom.Text;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADDED_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_EDIT_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_JOB_APPLICANTS;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DESC;
import static com.app.qualiffyapp.constants.AppConstants.JOB_ID;
import static com.app.qualiffyapp.constants.AppConstants.JS_JOB_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_PERMISSION;
import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_FOLDER;

public class ActivityApplicants extends BaseActivity implements SignupView, AddedJobItemClick, yesNoCallback {
    private ActivityApplicantsBinding mBinding;
    private JobApplicantAdapter mAdapter;
    private ApplicantsPresenter mPresenter;
    private ApplicantsResponseModel responseModel;
    private String orgName;
    private String desc;
    private GridLayoutManager gridLayoutManager;
    private DownloadFiles downloadFiles;
    private String jobId;
    private int pageNo = 1;
    private int totalItem;
    private List<ApplicantsResponseModel.UserBean> jobList;
    private DialogInitialize dialogInitialize;


    @Override
    protected void initView() {
        mBinding = (ActivityApplicantsBinding) viewDataBinding;
        mPresenter = new ApplicantsPresenter(this);
        initImgGrid();
        toolBar();
        getIntentData();
        prepareDialog();

    }

    private void prepareDialog() {
        dialogInitialize=new DialogInitialize();
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.skipTxt.setOnClickListener(this);
        mBinding.toolbar.deleteImg.setOnClickListener(this);
    }

    private void initImgGrid() {
        gridLayoutManager = new GridLayoutManager(this, 3);
        mBinding.rvApplicants.setLayoutManager(gridLayoutManager);

        mBinding.rvApplicants.setOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (totalItemsCount < totalItem && jobId != null) {
                    pageNo = page;
                    mPresenter.getApplicantsList(jobId, pageNo);
                }


            }

        });

    }

    private void toolBar() {
        mBinding.toolbar.toolbarTitleTxt.setText(Utility.singularPluralText(getResources().getString(R.string.applicants), 2));
        mBinding.toolbar.backBtnImg.setOnClickListener((view) -> onBackPressed());
        mBinding.toolbar.deleteImg.setVisibility(View.VISIBLE);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            orgName = bundle.getString(JS_JOB_TYPE, "");
            mPresenter.getApplicantsList(bundle.getString(JOB_ID, null), pageNo);
            desc = bundle.getString(DESC);
            jobId = bundle.getString(JOB_ID, null);
        }


    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_applicants;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.skip_txt:
                if (jobId != null && !TextUtils.isEmpty(jobId))
                    mPresenter.repostJob(jobId);

                break;
            case R.id.deleteImg:
                dialogInitialize.prepareDeleteJobDialog(new CustomDialogs(this,this));


                break;
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_BUSINESS_JOB_APPLICANTS:
                    responseModel = (ApplicantsResponseModel) response.getRes();
                    if (responseModel != null) {
                        totalItem = responseModel.ttl;

                        if (mAdapter == null) {
                            jobList = responseModel.users;
                            mAdapter = new JobApplicantAdapter(this, responseModel, this);
                            mBinding.rvApplicants.setAdapter(mAdapter);
                        } else {
                            if (jobList == null)
                                jobList = new ArrayList<>();

                            jobList.addAll(responseModel.users);
                            responseModel.users = jobList;
                            mAdapter.notifyDataSetChanged();
                        }


                        if (onAskForSomePermission(this, READ_WRITE_PERMISSION, READ_WRITE_REQUEST_CODE)) {
                            downloadFile();
                        }

                        mBinding.tvApplicants.setText(responseModel.ttl + " " + Utility.singularPluralText(getResources().getString(R.string.applicants), responseModel.ttl)
                                + " " + getResources().getString(R.string.for_txt) + " " + orgName);

                        if (!TextUtils.isEmpty(responseModel.jobsInfo.exp)) {
                            mBinding.tvExp.setText(responseModel.jobsInfo.exp + " " + Utility.singularPluralText(getResources().getString(R.string.date_util_unit_year),
                                    Integer.parseInt(responseModel.jobsInfo.exp)));
                            mBinding.tvExpTitle.setVisibility(View.VISIBLE);
                        }
                        if (!TextUtils.isEmpty(responseModel.jobsInfo.exp)) {
                            mBinding.tvDate.setText(Utility.convertDate(responseModel.jobsInfo.date));
                            mBinding.tvDateTitle.setVisibility(View.VISIBLE);
                        }

                        if (!TextUtils.isEmpty(responseModel.jobsInfo.state) && !TextUtils.isEmpty(responseModel.jobsInfo.cntry)) {
                            String add = responseModel.jobsInfo.state;
                            mBinding.tvAddress.setText(add);
                            mBinding.tvAddTitle.setVisibility(View.VISIBLE);
                        }
                        repostCheck(Utility.convertDateToMillies(mBinding.tvDate.getText().toString(), "dd/MM/yyyy"));

                        if (!TextUtils.isEmpty(responseModel.jobsInfo.slry)) {
                            mBinding.tvSalary.setText(responseModel.jobsInfo.slry);
                            mBinding.tvSalaryTitle.setVisibility(View.VISIBLE);
                        }

                        if (desc != null && !TextUtils.isEmpty(desc)) {
                            mBinding.titleDesc.setVisibility(View.VISIBLE);
                            mBinding.tvDescription.setVisibility(View.VISIBLE);
                            mBinding.tvDescription.setText(desc);
                        }
                    }
                    break;

                case API_FLAG_BUSINESS_EDIT_JOB:
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    if (msgResponseModel != null) {
                        validationFailed(msgResponseModel.msg);
                        mBinding.toolbar.skipTxt.setVisibility(View.GONE);
                        mBinding.tvDate.setText(Utility.convertDate(responseModel.jobsInfo.date));
                    }
                    break;
                case API_FLAG_BUSINESS_DELETE_JOB:
                    Intent intent = new Intent();
                    intent.putExtra("deleteJob", jobId);
                    setResult(101, intent);
                    finish();

                    break;
            }
        } else validationFailed(response.err.msg);
    }

    private void repostCheck(long jobDate) {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c);

        long currentMili = Utility.convertDateToMillies(formattedDate, "dd/MM/yyyy");

        if ((currentMili * 1000 <= (jobDate * 1000 + 14 * 24 * 60 * 60 * 1000)) && (currentMili * 1000 >= (jobDate * 1000 + 12 * 24 * 60 * 60 * 1000))) {
            mBinding.toolbar.skipTxt.setText(getResources().getString(R.string.repost));
            mBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
        }
    }


    private void downloadFile() {
        if (responseModel != null) {
            for (ApplicantsResponseModel.UserBean user : responseModel.users) {
                File file = Utility.getFileFromDevice(user.userId + "_" + user.id + ".mp4", VIDEO_FOLDER);
                if (file == null) {
                    downloadFiles = new DownloadFiles(user.userId + "_" + user.id + ".mp4", VIDEO_FOLDER);
                    downloadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, responseModel.jP + user.video);
                }
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case READ_WRITE_REQUEST_CODE:
                downloadFile();
                break;

        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Toast.makeText(this, "Returned from setting", Toast.LENGTH_SHORT).show();
                break;
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);

    }

    @Override
    public void itemClick(int pos) {
        if (responseModel != null) {
            Bundle bundle = new Bundle();
            bundle.putString("data", new Gson().toJson(responseModel));
            bundle.putInt("pos", pos);
            openActivity(AppliedJobVideoActivity.class, bundle);
        }
    }

    @Override
    public void onYesClicked(String from) {
        if (jobId != null && !TextUtils.isEmpty(jobId))
            mPresenter.deleteJob(jobId);
    }

    @Override
    public void onNoClicked(String from) {

    }
}
