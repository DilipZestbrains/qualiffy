package com.app.qualiffyapp.ui.activities.onBoarding;

import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.viewpager.widget.ViewPager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.SliderAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityTutorialBinding;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.utils.Utility;
import static com.app.qualiffyapp.constants.AppConstants.TUTORIAL_SCREEN_SHOW;

public class OnBoardingActivity extends BaseActivity {

    private ActivityTutorialBinding mActivityTutorialBinding;
    private LinearLayout mDotsLayout;

    private TextView[] mDots;

    private int mCurrentPage;

    ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            addDotsIndicator(position);

            mCurrentPage = position;

            if (position == 0) {//we are on first page
                mActivityTutorialBinding.btnNext.setEnabled(true);
                mActivityTutorialBinding.btnNext.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnPrevious.setVisibility(View.INVISIBLE);
                mActivityTutorialBinding.btnSkip.setText("SKIP");
                mActivityTutorialBinding.btnSkip.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnFinish.setVisibility(View.GONE);
                mActivityTutorialBinding.dotsLayout.setVisibility(View.VISIBLE);
            } else if (position == mDots.length - 1) { //last page
                mActivityTutorialBinding.btnPrevious.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnNext.setVisibility(View.GONE);
                mActivityTutorialBinding.btnSkip.setVisibility(View.GONE);
                mActivityTutorialBinding.dotsLayout.setVisibility(View.GONE);
                mActivityTutorialBinding.btnFinish.setVisibility(View.VISIBLE);
//                animateFinish();
            } else { //neither on first nor on last page
                mActivityTutorialBinding.btnNext.setEnabled(true);
                mActivityTutorialBinding.btnPrevious.setEnabled(true);
                mActivityTutorialBinding.btnPrevious.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnNext.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnSkip.setText("SKIP");
                mActivityTutorialBinding.btnSkip.setVisibility(View.VISIBLE);
                mActivityTutorialBinding.btnFinish.setVisibility(View.GONE);
                mActivityTutorialBinding.dotsLayout.setVisibility(View.VISIBLE);
            }

        }


        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    protected void initView() {
        mActivityTutorialBinding = (ActivityTutorialBinding) viewDataBinding;
        mDotsLayout = mActivityTutorialBinding.dotsLayout;
        SliderAdapter sliderAdapter = new SliderAdapter(this);
        mActivityTutorialBinding.slideViewpager.setAdapter(sliderAdapter);
        addDotsIndicator(0);
        mActivityTutorialBinding.slideViewpager.addOnPageChangeListener(viewListener);
        mActivityTutorialBinding.slideViewpager.setOffscreenPageLimit(3);
        mActivityTutorialBinding.btnNext.setOnClickListener(this);
        mActivityTutorialBinding.btnPrevious.setOnClickListener(this);
        mActivityTutorialBinding.btnSkip.setOnClickListener(this);
        mActivityTutorialBinding.btnFinish.setOnClickListener(this);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_tutorial;
    }

    public void addDotsIndicator(int position) {

        mDots = new TextView[2];
        mDotsLayout.removeAllViews(); //without this multiple number of dots will be created

        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226;")); //code for the dot icon like thing
            mDots[i].setTextSize(55);
            mDots[i].setTextColor(Color.parseColor("#508e6700"));

            mDotsLayout.addView(mDots[i]);
        }

        if (mDots.length > 0) {
            mDots[position].setTextColor(Color.parseColor("#8e6700")); //setting currently
            // selected dot to white
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_next:
                if (mCurrentPage == (mDots.length - 1)) {
                    startActivity(new Intent(OnBoardingActivity.this, DashboardActivity.class));
                    finish();
                } else {
                    mActivityTutorialBinding.slideViewpager.setCurrentItem(mCurrentPage + 1);
                }
                break;
            case R.id.btn_previous:
                mActivityTutorialBinding.slideViewpager.setCurrentItem(mCurrentPage - 1);
                break;
            case R.id.btn_skip:
                startActivity(new Intent(OnBoardingActivity.this, DashboardActivity.class));
                finish();
                break;
            case R.id.btn_finish:
                Utility.putBooleanValueInSharedPreference(getApplicationContext(), TUTORIAL_SCREEN_SHOW, true);
                startActivity(new Intent(OnBoardingActivity.this, DashboardActivity.class));
                finish();
                break;
            default:
                break;
        }
    }

    private void animateFinish() {
        mActivityTutorialBinding.btnFinish.animate().translationY(0 - Utility.dpToPixels(10,
                this)).setInterpolator(new DecelerateInterpolator()).setDuration(400).start();
    }

}