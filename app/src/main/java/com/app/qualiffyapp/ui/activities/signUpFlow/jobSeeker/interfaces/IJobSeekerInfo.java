package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IJobSeekerInfo extends BaseInterface {

    void preUpdate();

    void onUpdate(int Status, String msg);


}
