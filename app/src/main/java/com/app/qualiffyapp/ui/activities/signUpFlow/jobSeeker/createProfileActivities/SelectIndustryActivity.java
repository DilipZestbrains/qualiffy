package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SuggestedListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.ActivitySelectIndustryBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.JobInfo;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.SelectIndusPresenter;
import com.app.qualiffyapp.utils.RecyclerItemClickListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;

import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUSTRY_;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JS_ROLE;

public class SelectIndustryActivity extends BaseActivity implements TextWatcher, SignupView, RecyclerItemClickListener.OnItemClickListener , TextView.OnEditorActionListener {
    private ActivitySelectIndustryBinding selectIndustryBinding;
    private List<SuggestedListResponseModel> seggestList;
    private SelectIndusPresenter selectIndusPresenter;
    private SuggestedListAdapter suggestedListAdapter;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private boolean isJobSeekerUpdate;
    private JobInfo jobInfo;

    @Override
    protected void initView() {

        selectIndustryBinding = (ActivitySelectIndustryBinding) viewDataBinding;
        selectIndusPresenter = new SelectIndusPresenter(this);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        selectIndustryBinding.tvNote.setVisibility(View.VISIBLE);
        selectIndustryBinding.addJobs.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            selectIndustryBinding.tvNote.setText(Html.fromHtml(getResources().getString(R.string.note), Html.FROM_HTML_MODE_COMPACT));
        } else {
            selectIndustryBinding.tvNote.setText(Html.fromHtml(getResources().getString(R.string.note)));
        }
        getIntentData();
        toolbar();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            jobInfo = new Gson().fromJson(bundle.getString(JS_INDUSTRY_), JobInfo.class);
            if (jobInfo != null)
                addChip(jobInfo.getName(), jobInfo.get_id());
        }

    }

    @Override
    protected void setListener() {
        selectIndustryBinding.searchEdt.addTextChangedListener(this);
        selectIndustryBinding.indusBtn.btn.setOnClickListener(this);
        selectIndustryBinding.searchEdt.setOnEditorActionListener(this);
        selectIndustryBinding.suggestedRcv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, this));
    }

    private void toolbar() {
        selectIndustryBinding.toolbar.backBtnImg.setOnClickListener(this);
        if(isJobSeekerUpdate)
        selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_industry));
      else  selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
        selectIndustryBinding.titleTxt.setText(getResources().getString(R.string.indus));
        selectIndustryBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_select_industry;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.indus_btn:
                if (selectIndustryBinding.chipgroup.getChildCount() == 0)
                    validationFailed(ApplicationController.getApplicationInstance().getString(R.string.industry_alert));
                else {
                    if (profileInputModel.jobC_id != null) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(IS_JOBSEEKER_UPDATE, isJobSeekerUpdate);
                        openActivityForResult(UserRoleActivity.class, bundle, REQUEST_CODE_JS_ROLE);
                    }
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);
        if (s.length() == 2 || s.length() == 5 || s.length() == 7 || s.length() == 11 || s.length() > 15) {
            selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
            selectIndusPresenter.hitApi(s.toString());
        } else {
            if (seggestList != null) {
                selectIndustryBinding.progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == 1) {
            IndustryResponseModel getIndustryResponseModel = (IndustryResponseModel) response.getRes();
            seggestList = getIndustryResponseModel.jobC;
            suggestedListAdapter = new SuggestedListAdapter(seggestList);
            selectIndustryBinding.suggestedRcv.setAdapter(suggestedListAdapter);

            if (getIndustryResponseModel.ttl == 0) {
                selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.VISIBLE);
            } else
                selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);

        } else {
            Utility.showToast(this, response.err.msg);
        }
        selectIndustryBinding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        selectIndustryBinding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_JS_ROLE:
                    JobInfo role = new JobInfo();
                    role.set_id(profileInputModel.jobC_id);
                    role.setName(((Chip) selectIndustryBinding.chipgroup.getChildAt(0)).getText().toString());
                    data.putExtra(JS_INDUSTRY_, new Gson().toJson(role));
                    setResult(RESULT_OK, data);
                    finish();
                    break;
            }
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        selectIndustryBinding.chipgroup.removeAllViews();
        Utility.hideKeyboard(this);
        addChip(Utility.getFirstLetterCaps(seggestList.get(position).name), seggestList.get(position).id);
    }

    private void addChip(String txt, String id) {
        if (profileInputModel.jobC_id != null && profileInputModel.jobC_id.equals(id))
            profileInputModel.isSignup1Change = false;
        else
            profileInputModel.isSignup1Change = true;


        profileInputModel.jobC_id = id;

        CustomChip chip = new CustomChip(this, txt);

        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectIndustryBinding.chipgroup.removeView(view);
                profileInputModel.jobC_id = null;
            }
        });
        selectIndustryBinding.chipgroup.addView(chip);

        selectIndustryBinding.searchEdt.getText().clear();
//        selectIndustryBinding.suggestedRcv.removeAllViewsInLayout();
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {

            if(!TextUtils.isEmpty(selectIndustryBinding.searchEdt.getText().toString()))
                selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
            selectIndusPresenter.hitApi( selectIndustryBinding.searchEdt.getText().toString());
            Utility.hideKeyboard(this);

            return true;
        }
        return false;
    }
}
