package com.app.qualiffyapp.ui.activities.profile.businessProfile.view;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.profile.JsSubscriptionAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivitySubscriptionBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters.SubscriptionPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;
import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class SubscriptionActivity extends BaseActivity implements SignupView {
    private ActivitySubscriptionBinding mBinding;
    private JsSubscriptionAdapter subscriptionAdapter;
    private SubscriptionPresenter subscriptionPresenter;

    private void bindAdapters() {

    }

    @Override
    protected void initView() {
        mBinding = (ActivitySubscriptionBinding) viewDataBinding;
        setToolbar();
        subscriptionPresenter = new SubscriptionPresenter(this);
        showProgressDialog(true);
        subscriptionPresenter.hitApi();
        bindAdapters();

    }


    private void setToolbar() {
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.my_subscriptions));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_subscription;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_btn_img) {
            onBackPressed();
        }
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        if (response.getStatus() == SUCCESS || Utility.getIntFromSharedPreference(this, SUBSCRIPTION_FLAG) == 4) {
            JsSubscriptionModel subscriptionModel = (JsSubscriptionModel) response.getRes();
            if (Utility.getIntFromSharedPreference(this, SUBSCRIPTION_FLAG) == 4) {
                JsSubscriptionModel.SubsBean subsBean = new JsSubscriptionModel.SubsBean();
                subsBean.price = "FREE Subscription";
                subsBean.name = "Admin provide you the free subscription plan to access all the functionality.";

                if (subscriptionModel.subs == null || subscriptionModel.subs.size() == 0)
                    subscriptionModel.subs = new ArrayList<>();

                subscriptionModel.subs.add(subsBean);

            }
            if (subscriptionModel.subs != null && subscriptionModel.subs.size() > 0) {
                subscriptionAdapter = new JsSubscriptionAdapter(subscriptionModel.subs);
                mBinding.rvSubscriptionList.setAdapter(subscriptionAdapter);
                return;
            }
        }
//        Utility.showToast(getViewContext(), response.err.msg);
        mBinding.noDataTxt.setVisibility(View.VISIBLE);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        mBinding.noDataTxt.setVisibility(View.VISIBLE);

    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getViewContext(), msg);
    }


    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

}
