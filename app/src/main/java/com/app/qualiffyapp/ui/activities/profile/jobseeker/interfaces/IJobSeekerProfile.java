package com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces;

import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerProfileResponse;

import java.util.LinkedList;

public interface IJobSeekerProfile {

    void onFetchProfileDetails(JobSeekerProfileResponse profile);

    void preFetch();

    void setExpandState(ViewType type, ViewVisibility viewVisibility);

    void expandableMode(ViewType type, ViewVisibility visibility);

    void openActivityToUpdate(Class<?> screen, Bundle bundle, int RequestCode);
    void openActivity(String baseUrl, LinkedList<String> imgs, int pos);

    void onFetchPortfolio(String msg, WebResponseModel model, int apiFlag);

    void logoutResponse(int apiFlag);

    void apiError(String msg);

    void toogleResponse(String msg, int status, int apiFlag);

    enum ViewType {
        INTRODUCTION,
        IMAGES,
        VIDEO,
        EXPERIENCE,
        CERTIFICATE,
        EDUCATION,
        SKILLS,
        LANGUAGES,
        JOB_TYPE,
    }

    enum ViewVisibility {
        MAKE_VISIBLE(View.VISIBLE),
        MAKE_GONE(View.GONE),
        MAKE_INVISIBLE(View.GONE);
        int visibility;

        ViewVisibility(int visibility) {
            this.visibility = visibility;
        }

        public int getVisibility() {
            return visibility;
        }
    }


}
