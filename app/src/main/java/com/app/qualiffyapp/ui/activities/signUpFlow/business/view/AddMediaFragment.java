package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.AddMediaAdapter;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.FragmentAddMediaOrgBinding;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddMediaOrgPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE_ALL_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_IMAGE;
import static com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity.CROPPER_SQUARE;

public class AddMediaFragment extends BLoginSignUpBaseFragment implements yesNoCallback, RecyclerItemClick, SignupView {
    private final int CAMERA_IMG = 201;
    List<String> imageList = new ArrayList<>();
    private FragmentAddMediaOrgBinding mediaBinding;
    private AddMediaOrgPresenter addMediaPresenter;
    private GridLayoutManager gridLayoutManager;
    private CustomDialogs customDialogs;
    private LinkedList<AddMediaModel> mediaList;
    private AddMediaAdapter addMediaAdapter;
    private int GALLERY_IMG = 202;
    private boolean isHitApi;
    private String baseUrl;


    @Override
    protected void initUi() {
        mediaBinding = (FragmentAddMediaOrgBinding) viewDataBinding;
        addMediaPresenter = new AddMediaOrgPresenter(this);
        initImgGrid();
        initDialog();
        toolbar();

        deleteAllMediahitApi();
    }

    private void deleteAllMediahitApi() {
        showProgressDialog(true);
        addMediaPresenter.deleteAllMediaApi();
    }

    private void initImgGrid() {
        gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        mediaBinding.mediaGrid.setLayoutManager(gridLayoutManager);
        mediaList = new LinkedList<>();
        AddMediaModel addMediaModel = new AddMediaModel();
        addMediaModel.setDrawable_img(R.drawable.ic_add_placeholder);
        mediaList.add(addMediaModel);
        addMediaAdapter = new AddMediaAdapter(mediaList, this);
        mediaBinding.mediaGrid.setAdapter(addMediaAdapter);
    }

    private void initDialog() {
        customDialogs = new CustomDialogs(getActivity(), this);
        customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                getResources().getString(R.string.add_media_dialog_body),
                getResources().getString(R.string.add_media_dialog_pos_btn),
                getResources().getString(R.string.add_media_dialog_neg_btn),
                getResources().getString(R.string.ADD_MEDIA_TITLE));
    }

    @Override
    protected void setListener() {
        super.setListener();
        mediaBinding.expBtn.btn.setOnClickListener(this);

    }

    private void toolbar() {
        mediaBinding.expBtn.btn.setText(getResources().getString(R.string.NEXT));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_add_media_org;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exp_btn:
                if (mediaList == null || mediaList.size() == 1)
                    validationFailed(getResources().getString(R.string.empty_media));
                else {
                    if (getLoginActivity() != null) {
                        getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.ENABLE_LOCATION, true, true);
                        getLoginActivity().enableSkip(false);
                    }
                }
                break;
        }

    }

    @Override
    public void onYesClicked(String from) {
        onAskForSomePermission(context, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onNoClicked(String from) {
        onAskForSomePermission(context, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onClick(int flag, int pos) {
        switch (flag) {
            case ADD_IMG_CLICK_FLAG:
                if (!isHitApi)
                    customDialogs.showDialog();
                break;
            case DELETE_FLAG:
                showProgressDialog(true);
                AddMediaModel addMediaModel = mediaList.get(pos);
                addMediaPresenter.deleteMediaApi(addMediaModel.getImg_id(), 0);
                mediaList.remove(pos);
                if (mediaList.get(mediaList.size() - 1).getDrawable_img() == 0)
                    setAddImage();
                if (mediaBinding.mediaGrid.getAdapter() != null)
                    mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
                break;
            case VIEW_IMG_CLICK_FLAG:
                Intent viewMedia = new Intent(getActivity(), StoryViewActivity.class);
                viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA,
                        Utility.gsonInstance().toJson(imageList));
                viewMedia.putExtra("BASE_URL_IMAGE", baseUrl);
                startActivity(viewMedia);
                break;
        }
    }

    private void setAddImage() {
        AddMediaModel addMediaModel = new AddMediaModel();
        addMediaModel.setDrawable_img(R.drawable.add_placeholder);
        mediaList.add(mediaList.size(), addMediaModel);
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_ADD_MEDIA:
                if (response.getStatus() == 1) {
                    AddMediaResponseModel mediaResponseModel = (AddMediaResponseModel) response.getRes();
                    baseUrl = mediaResponseModel.bP;
                    AddMediaModel addMediaModel = mediaList.get(0);
                    addMediaModel.setImg_id(mediaResponseModel.img_id);
                    imageList.add(mediaResponseModel.img_id);
                    if (mediaBinding.mediaGrid.getAdapter() != null)
                        mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();

                } else {
                    if (mediaList.size() == 10)
                        setAddImage();
                    mediaList.remove(0);
                    Utility.showToast(getActivity(), response.err.msg);
                }


                if (mediaList.size() > 1)
                    getLoginActivity().enableSkip(false);

                isHitApi = false;
                break;

            case API_FLAG_DELETE_MEDIA:
                showProgressDialog(false);
                if (mediaList.size() <= 1)
                    getLoginActivity().enableSkip(true);
                break;
            case API_FLAG_BUSINESS_DELETE_ALL_MEDIA:
                showProgressDialog(false);
                break;
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(getActivity(), error);
        switch (apiFlag) {
            case API_FLAG_ADD_MEDIA:
                setAddImage();
                isHitApi = false;
                break;
            case API_FLAG_DELETE_MEDIA:
                showProgressDialog(false);
                break;
            case API_FLAG_BUSINESS_DELETE_ALL_MEDIA:
                showProgressDialog(false);
                break;
        }
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getActivity(), msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return getLoginActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mediaList == null || mediaList.size() <= 1)
            getLoginActivity().enableSkip(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        getLoginActivity().enableSkip(false);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case CAMERA_STORAGE_PERMISSION_REQUEST_CODE:
                SandriosCamera
                        .with()
                        .setCropperSquare(false)
                        .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                        .launchCamera(this);



              /*  Intent i = new Intent(getActivity(), CameraActivity.class);
                i.putExtra("media_type", 1);
                startActivityForResult(i, CAMERA_IMG);*/
                break;
            case GALLERY_STORAGE_PERMISSION_REQUEST_CODE:
                EasyImage.openGallery(this, GALLERY_IMG);

                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Denied");// Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                    Log.e("Permission", "Returned from setting");
                    break;
                case REQUEST_CODE_CAMERA_IMG:
                    List<File> list = new ArrayList<>();
                    list.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list);
                    break;
                case REQUEST_CODE_FILTER_IMAGE:
                    List<File> list1 = new ArrayList<>();
                    list1.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list1);
                    break;

            }

        }


        EasyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                File imgFile = list.get(0);
                openEditor(imgFile.getPath());
               /* Intent intent = new Intent(getContext(), PhotoEditorActivity.class);
                intent.putExtra(MEDIA_PATH, imgFile.getPath());
                startActivityForResult(intent, REQUEST_CODE_FILTER_IMAGE);*/

            }
        });
    }


    private void openEditor(String imgPath) {


        try {
            if (getContext() != null) {
                Intent myIntent = new Intent(getContext(), Class.forName(PHOTO_EDITOR_ACTIVITY));
                myIntent.putExtra(MEDIA_PATH, imgPath);
                myIntent.putExtra(CROPPER_SQUARE,false);

                startActivityForResult(myIntent, REQUEST_CODE_CAMERA_IMG);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void onPhotosReturned(List<File> list) {
        if (mediaList.size() == 10)
            mediaList.remove(mediaList.size() - 1);

        File imgFile = list.get(0);
        AddMediaModel addMediaModel = new AddMediaModel();
        addMediaModel.setImgFile(imgFile.getAbsolutePath());
        mediaList.add(0, addMediaModel);
        if (mediaBinding.mediaGrid.getAdapter() != null)
            mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
        try {
            isHitApi = true;
            if (getActivity() != null) {
                File compressedImageFile = new Compressor(getActivity()).compressToFile(imgFile);
                addMediaPresenter.hitApi(compressedImageFile, API_FLAG_ADD_MEDIA);
            }
        } catch (IOException e) {
            isHitApi = false;
        }


    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.ADD_MEDIA_TITLE);
    }


}


