package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ICompanyName;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;

import java.util.ArrayList;

public class CompanyNamePresenter extends UpdateBusinessDetailsPresenter {
    private ICompanyName companyNameFragment;

    public CompanyNamePresenter(ICompanyName companyName) {
        super(companyName);
        this.companyNameFragment = companyName;
    }

    public void proceed(String companyName, String companyStrength) {
        if (isValid(companyName, companyStrength)) {
            companyNameFragment.onNext(companyName, companyStrength);
        }
    }

    private boolean isValid(String companyName, String companyStrength) {
        if (companyName == null || TextUtils.isEmpty(companyName.trim())) {
            companyNameFragment.showToast(ToastType.NAME_EMPTY);
            return false;
        }
        if (companyStrength == null || TextUtils.isEmpty(companyStrength)) {
            companyNameFragment.showToast(ToastType.COMPANY_STRENGTH_EMPTY);
            return false;
        }

        return true;
    }

    private boolean isComapnyNameValid(String companyName) {
        if (companyName == null || TextUtils.isEmpty(companyName.trim())) {
            companyNameFragment.showToast(ToastType.NAME_EMPTY);
            return false;
        }
        return true;
    }

    public void openCompanyStrenthDialog() {
        final ArrayList<String> arrayAdapter = new ArrayList<>();
        arrayAdapter.add("10 - 50");
        arrayAdapter.add("50 - 200");
        arrayAdapter.add("200 - 500");
        arrayAdapter.add("500 - 1000");
        arrayAdapter.add("1000+");
        companyNameFragment.showCompanyStrengthDialog(arrayAdapter);


    }

    public void updateCompanyName(String name) {
        if (isComapnyNameValid(name)) {
            companyNameFragment.preUpdate();
            RequestBean requestBean = new RequestBean();
            requestBean.name = name;
            updateDetails(requestBean);
        }
    }
}
