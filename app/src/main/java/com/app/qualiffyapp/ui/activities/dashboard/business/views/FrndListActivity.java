package com.app.qualiffyapp.ui.activities.dashboard.business.views;

import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.business.FrndListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.databinding.ActivityViewerBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.business.UserFrndListResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.business.presenter.FrndListPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class FrndListActivity extends BaseActivity implements SignupView {
    private ActivityViewerBinding mBinding;
    private FrndListPresenter mPresenter;
    private FrndListAdapter mAdapter;
    private int pageNo = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;
    private String userId;


    @Override
    protected void initView() {
        mBinding = (ActivityViewerBinding) viewDataBinding;
        mPresenter = new FrndListPresenter(this);
        toolbar();
        getIntentData();

    }

    private void getIntentData() {
        userId = getIntent().getStringExtra(USER_ID);
        mPresenter.hitApi(userId, pageNo, true);

    }

    private void toolbar() {
        mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.frnds));
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_viewer;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }


    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.backBtnImg.setOnClickListener(this);

        mBinding.rcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.rcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    if (userId != null) {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        mPresenter.hitApi(userId, pageNo, false);
                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
        {
            if (response.getStatus() == SUCCESS) {
                UserFrndListResponseModel responseModel = (UserFrndListResponseModel) response.getRes();
                if (responseModel != null && responseModel.usr != null && responseModel.usr.size() > 0) {
                    setVisiblity(true);
                    if (mAdapter == null) {
                        mAdapter = new FrndListAdapter(responseModel.usr, responseModel.bP, this);
                        mBinding.rcv.setAdapter(mAdapter);

                    } else {
                        mAdapter.addAll(responseModel.usr);
                    }

                    if (responseModel.ttl > PAGINATION_LIMIT) {
                        totalPage = responseModel.ttl / PAGINATION_LIMIT;
                        if (responseModel.ttl % PAGINATION_LIMIT != 0)
                            totalPage = totalPage + 1;
                    }
                    if (pageNo == totalPage) isLastPage = true;
                    isLoading = false;

                } else setVisiblity(false);


            } else validationFailed(response.err.msg);
        }
    }


    void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.rcv.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {


        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
