package com.app.qualiffyapp.ui.activities.plans.views;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.plans.IUgradePlan;
import com.app.qualiffyapp.utils.Utility;

public class PremiumPlanDialog {

    private Dialog dialog;
    private TextView tvSubscriptionDetail;
    private TextView tvPackage;

    public void prepareDialog(Context context, IUgradePlan ugradePlan, String detailStr, String pacName) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_premium_plan);

        tvSubscriptionDetail = dialog.findViewById(R.id.tvSubscriptionDetail);
        tvSubscriptionDetail.setMovementMethod(new ScrollingMovementMethod());

        tvPackage = dialog.findViewById(R.id.tvPackage);
        dialog.setCancelable(true);

        tvSubscriptionDetail.setText(Html.fromHtml(detailStr));
        tvPackage.setText(pacName);

        dialog.findViewById(R.id.tvRequestQuote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ugradePlan.requestQuoteClick();
                dialog.hide();
            }
        });
        dialog.findViewById(R.id.tvCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();
            }
        });

        dialog.show();


    }
}
