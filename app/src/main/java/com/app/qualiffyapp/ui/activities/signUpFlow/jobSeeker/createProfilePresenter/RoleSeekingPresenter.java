package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ROLE_SEEKING;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_1;

public class RoleSeekingPresenter implements ApiListener<ResponseModel<IndustryResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public RoleSeekingPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }


    @Override
    public void onApiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void hitApi(String skey, String indusId) {
        mSignUpInteractor.getRoleApi(this, skey, indusId, API_FLAG_ROLE_SEEKING);

//        mSignUpInteractor.getRoleSeekingApi(this, skey, API_FLAG_ROLE_SEEKING);
    }

    public void signUp1Api(CreateJobSeekerProfileInputModel profileInputModel) {

        mSignUpInteractor.getSignUp1Api(this, profileInputModel, API_FLAG_SIGNUP_1);
    }

}
