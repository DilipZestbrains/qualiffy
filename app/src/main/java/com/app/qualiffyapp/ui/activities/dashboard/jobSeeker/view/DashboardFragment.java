package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.jobSeeker.CardStackAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.dashboard.IDashboardItemClickListener;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.CircleMenuLayout;
import com.app.qualiffyapp.customViews.CustomCardStackLayoutManager;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.ApplyJobDialog;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.customViews.customDialog.IApplyJobDialog;
import com.app.qualiffyapp.databinding.FragmentDashboardBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter.DashboardPresenter;
import com.app.qualiffyapp.ui.activities.recruiter.views.RecruiterFragment;
import com.app.qualiffyapp.ui.fragment.DistanceBottomSheet;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.videoCompressUtils.IVideoCompressCallback;
import com.app.qualiffyapp.utils.videoCompressUtils.VideoUtils;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.Direction;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DASHBOARD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB_VIDEO;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_FOLLOW;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_NOT_RECOMMEND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_RECOMMEND;
import static com.app.qualiffyapp.constants.AppConstants.CUSTOMER_ID;
import static com.app.qualiffyapp.constants.AppConstants.IS_UPLOAD_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_SUBSCRIPTION;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.ORG_BIO;
import static com.app.qualiffyapp.constants.AppConstants.ORG_ID;
import static com.app.qualiffyapp.constants.AppConstants.ORG_IMG;
import static com.app.qualiffyapp.constants.AppConstants.ORG_NAME;
import static com.app.qualiffyapp.constants.AppConstants.RECOMMEND;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CAMERA_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.EXPERIENCE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.HAS_VIDEO;
import static com.app.qualiffyapp.customViews.DialogInitialize.PURCHASE_PLAN_FLAG;
import static com.app.qualiffyapp.ui.activities.recruiter.views.RecruiterFragment.ADD_RECRUITER_RESULT_CODE;

public class DashboardFragment extends BaseFragment implements CardStackListener, SignupView, IApplyJobDialog, IDashboardItemClickListener, IVideoCompressCallback, yesNoCallback, DistanceBottomSheet.DistanceFilterListener {
    private static final String RIGHT = "right";
    private FragmentDashboardBinding mDashboardBinding;
    private CardStackLayoutManager mCardStackLayoutManager;
    private CardStackAdapter mCardStackAdapter;
    private DashboardPresenter dashboardPresenter;
    private List<Object> jobList;
    private boolean hasData = true;
    private int pageNo = 1;
    private ApplyJobDialog applyJobDialog;
    private DialogInitialize dialogInitialize;
    private boolean isSearch;
    private String basePath;
    private int subscribeFlag;
    private VideoUtils videoUtils;
    private String videoPath;
    private int DEFAULT_DISTANCE = 50;
    private JsJobsResponseModel jobsResponseModel;


    @Override
    protected void initUi() {
        mDashboardBinding = (FragmentDashboardBinding) viewDataBinding;
        dashboardPresenter = new DashboardPresenter(this);
        applyJobDialog = new ApplyJobDialog(getContext(), this);
        dialogInitialize = new DialogInitialize();
        setupCardStackView();
        fetchData();
    }


    private void fetchData() {
        if (pageNo == 1) {
            showProgressDialog(true);
            isSearch = false;
        }
        dashboardPresenter.dashBoardApi(pageNo, null, DEFAULT_DISTANCE);
    }

    private void fetchData(int distance) {
        if (pageNo == 1) {
            showProgressDialog(true);
            isSearch = false;
        }
        dashboardPresenter.dashBoardApi(pageNo, null, distance);
    }

    private void setupCardStackView() {
        mCardStackLayoutManager = CustomCardStackLayoutManager.getCardStackLayoutManager(context, this);
        mDashboardBinding.cardStackView.setLayoutManager(mCardStackLayoutManager);
        DefaultItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setSupportsChangeAnimations(false);
        mDashboardBinding.cardStackView.setItemAnimator(itemAnimator);

        String[] mItemTexts = new String[]{"安全中心 ", "特色服务", "投资理财", "投资理财"};

        int[] mItemImgs = new int[]{R.drawable.ic_share_home,
                R.drawable.ic_noun, R.drawable.ic_thumb, R.drawable.ic_multiple_users};

        mDashboardBinding.idMenulayout.setMenuItemIconsAndTexts(mItemImgs, mItemTexts);

        mDashboardBinding.ivAddFriend.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
        mDashboardBinding.ivCamera.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
        mDashboardBinding.ivChat.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());

        mDashboardBinding.idMenulayout.setOnMenuItemClickListener(new CircleMenuLayout.OnMenuItemClickListener() {
            @Override
            public void itemClick(View view, int pos) {
                Toast.makeText(context, "Click  " + pos, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void itemCenterClick(View view) {

            }
        });

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_dashboard;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {
        Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = " + ratio);
    }

    @Override
    public void onCardSwiped(Direction direction) {
        int lastCardPos = mCardStackLayoutManager.getTopPosition() - 1;

        Log.d("CardStackView", "onCardSwiped: p = " + mCardStackLayoutManager.getTopPosition() + ", d =" + direction);

        if (hasData) {
            if (mCardStackLayoutManager.getTopPosition() == jobList.size() - 2) {
                pageNo = pageNo + 1;
                isSearch = false;
                fetchData();
            }
        } else {
            if (mCardStackLayoutManager.getTopPosition() == jobList.size())
                dataVisible(false);
        }

        if (direction.toString().toLowerCase().equals(RIGHT)) {
            if (jobList.get(lastCardPos) instanceof JsJobsResponseModel.JobBean) {
                JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) jobList.get(lastCardPos);
//                dashboardPresenter.hitApi(jobBean.orgId, 1, API_FLAG_JS_IS_RECOMMEND);
                dashboardPresenter.hitApi(jobBean.orgId, 1, API_FLAG_JS_IS_FOLLOW);
            } else {
                JsJobsResponseModel.Promotions jobBean = (JsJobsResponseModel.Promotions) jobList.get(lastCardPos);
                if (jobBean != null)
                    dashboardPresenter.hitApi(jobBean.org_id, 1, API_FLAG_JS_IS_FOLLOW);

            }
            switch (IS_UPLOAD_VIDEO) {
                case 0:
                    String exp = Utility.getStringSharedPreference(getContext(), EXPERIENCE);
                    ExpInfoBean expInfoBean = new Gson().fromJson(exp, ExpInfoBean.class);

                    if (expInfoBean != null) {
                        if (jobList.get(lastCardPos) instanceof JsJobsResponseModel.JobBean) {
                            JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) jobList.get(lastCardPos);
                            if ((jobBean.exp <= expInfoBean.yrs) && (expInfoBean.yrs <= (jobBean.exp == 0 ? 1 : jobBean.exp) * 2)) {
                                Bundle bundle = new Bundle();
                                bundle.putString(ORG_ID, jobBean.orgId);
                                bundle.putString(ORG_NAME, jobBean.orgInfo.name);
                                bundle.putString(ORG_IMG, basePath + jobBean.orgInfo.img);
                                openActivity(MatchJobActivity.class, bundle);

                            }
                        }
                    }
                    break;
                case 1:
                    openJobList();
                    break;
                case 2:
                    showProgressDialog(true);
                    if (jobList.get(lastCardPos) instanceof JsJobsResponseModel.JobBean) {
                        JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) jobList.get(lastCardPos);
                        dashboardPresenter.ApplyJobApi(null, null, jobBean.id);
                    }
                    break;
                case 3:
                    if (getContext() != null)
                        if (Utility.getBooleanSharedPreference(getContext(), HAS_VIDEO)) {
                            showProgressDialog(true);
                            if (jobList.get(lastCardPos) instanceof JsJobsResponseModel.JobBean) {
                                JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) jobList.get(lastCardPos);
                                dashboardPresenter.ApplyJobApi(null, "1", jobBean.id);
                            }
                        } else
                            validationFailed(getContext().getResources().getString(R.string.add_profile_video));
                    break;

            }
        } else {
            if (jobList.get(lastCardPos) instanceof JsJobsResponseModel.JobBean) {
                JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) jobList.get(lastCardPos);
//                dashboardPresenter.hitApi(jobBean.orgId, 0, API_FLAG_JS_IS_NOT_RECOMMEND);
                dashboardPresenter.hitApi(jobBean.orgId, 0, API_FLAG_JS_IS_FOLLOW);
            } else {
                JsJobsResponseModel.Promotions jobBean = (JsJobsResponseModel.Promotions) jobList.get(lastCardPos);
                if (jobBean != null)
                    dashboardPresenter.hitApi(jobBean.org_id, 0, API_FLAG_JS_IS_FOLLOW);

            }
        }


    }

    @Override
    public void onCardRewound() {
        Log.d("CardStackView", "onCardRewound:" + mCardStackLayoutManager.getTopPosition());
    }

    @Override
    public void onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: " + mCardStackLayoutManager.getTopPosition());
    }

    @Override
    public void onCardAppeared(View view, int position) {
        TextView textView = view.findViewById(R.id.item_name);
        Log.d("CardStackView", "onCardAppeared: " + position + textView.getText());
    }

    @Override
    public void onCardDisappeared(View view, int position) {
        TextView textView = view.findViewById(R.id.item_name);


        Log.d("CardStackView", "onCardDisappeared: " + position + textView.getText());
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_DASHBOARD:

                    jobsResponseModel = (JsJobsResponseModel) response.getRes();
                    if (getContext() != null)
                        Utility.putStringValueInSharedPreference(getContext(), CUSTOMER_ID, jobsResponseModel.cus_id);

                    if (jobsResponseModel != null) {
                        subscribeFlag = jobsResponseModel.isSubscribe;
                        if (getContext() != null)
                            Utility.putIntValueInSharedPreference(getContext(), SUBSCRIPTION_FLAG, jobsResponseModel.isSubscribe);

                        if (jobsResponseModel.job != null && jobsResponseModel.job.size() > 0) {
                            dataVisible(true);
                            hasData = jobsResponseModel.job.size() >= jobsResponseModel.lmt;

                            if (jobList == null || isSearch) {
                                jobList = shuffleJobsPromotions(jobsResponseModel.job, jobsResponseModel.prmt);
                                mCardStackAdapter = new CardStackAdapter(jobList, jobsResponseModel.bP, jobsResponseModel.pP, context, this);
//                                mCardStackAdapter = new CardStackAdapter(jobList, jobsResponseModel.bP, context, this);
                                basePath = jobsResponseModel.bP;
                                mDashboardBinding.cardStackView.setAdapter(mCardStackAdapter);
                            } else {
                                if (mCardStackAdapter != null) {
                                    if (!jobList.isEmpty())
                                        jobList.subList(0, jobList.size() - 2).clear();

                                    jobList.addAll(shuffleJobsPromotions(jobsResponseModel.job, jobsResponseModel.prmt));
                                    mCardStackAdapter.notifyDataSetChanged();
                                }
                            }

                        } else
                            dataVisible(false);

                    } else {
                        dataVisible(false);
                    }
                    if (pageNo == 1)
                        showProgressDialog(false);

                    break;

                case API_FLAG_JS_APPLY_JOB:
                    showProgressDialog(false);
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel.msg);
                    break;

                case API_FLAG_JS_IS_RECOMMEND:
                    showProgressDialog(false);
                    MsgResponseModel msgResponseModel0 = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel0.msg);
                    setRecommendCount(1);

                    break;

                case API_FLAG_JS_IS_NOT_RECOMMEND:
                    showProgressDialog(false);
                    MsgResponseModel msgResponseModel3 = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel3.msg);
                    setRecommendCount(0);

                    break;

                case API_FLAG_JS_APPLY_JOB_VIDEO:
                    showProgressDialog(false);
                    UserConnection business = new UserConnection();
                    if (jobList.get(mCardStackLayoutManager.getTopPosition() - 1) instanceof JsJobsResponseModel.JobBean) {
                        JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition() - 1);
                        business.setUid(bean.orgId);
                        business.setImg(basePath + bean.orgInfo.img);
                        business.setName(bean.orgInfo.name);
                        business.setPhone(bean.orgInfo.pno);
                        business.setTimestamp(String.valueOf(System.currentTimeMillis()));
                        business.setType(FirebaseUserType.BUSINESS.s);
                        AppFirebaseDatabase.getInstance().addFriend(Utility.getUserConnection(getContext()), business);
                        MsgResponseModel msgResponseModel1 = (MsgResponseModel) response.getRes();
                        validationFailed(msgResponseModel1.msg);
                    }
                    break;


            }
        } else {
            if (apiFlag == API_FLAG_DASHBOARD) {
                validationFailed(response.err.msg);
                dataVisible(false);
            } else if (apiFlag == API_FLAG_JS_APPLY_JOB) {
                validationFailed(response.err.msg);
            } else if (apiFlag == API_FLAG_JS_IS_RECOMMEND || apiFlag == API_FLAG_JS_IS_NOT_RECOMMEND) {
                showProgress(false);
                validationFailed(response.err.msg);

            }

        }
    }

    private void setRecommendCount(int recommendFLag) {
        if (jobList.get(mCardStackLayoutManager.getTopPosition()) instanceof JsJobsResponseModel.JobBean) {
            JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition());
            if (recommendFLag == 0) {
                if (bean.rec > 0)
                    bean.rec = bean.rec - 1;
                bean.unrec = bean.unrec + 1;
                bean.is_recmd = 0;
            } else {
                bean.rec = bean.rec + 1;
                if (bean.unrec > 0)
                    bean.unrec = bean.unrec - 1;
                bean.is_recmd = 1;

            }
            bean.orgInfo.rec = String.valueOf(bean.rec);
            bean.orgInfo.unrec = String.valueOf(bean.unrec);
            bean.orgInfo.isRec = bean.is_recmd;
            mCardStackAdapter.notifyItemChanged(mCardStackLayoutManager.getTopPosition());
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        if (apiFlag == API_FLAG_DASHBOARD)
            dataVisible(false);

        validationFailed(error);
    }


    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    private void dataVisible(boolean isJobAvailable) {
        if (isJobAvailable) {
            mDashboardBinding.noDataTxt.setVisibility(View.GONE);
            mDashboardBinding.cardStackView.setVisibility(View.VISIBLE);
        } else {
            mDashboardBinding.noDataTxt.setVisibility(View.VISIBLE);
            if (getContext() != null)
                mDashboardBinding.noDataTxt.setText(getContext().getResources().getString(R.string.no_job));
            mDashboardBinding.cardStackView.setVisibility(View.GONE);
        }
    }

    private void openJobList() {
        String exp = Utility.getStringSharedPreference(getContext(), EXPERIENCE);
        ExpInfoBean expInfoBean = new Gson().fromJson(exp, ExpInfoBean.class);

        if (expInfoBean != null) {
            int pos = mCardStackLayoutManager.getTopPosition() - 1;
            if (jobList.get(pos) instanceof JsJobsResponseModel.JobBean) {
                JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(pos);
                if ((bean.exp >= expInfoBean.yrs) && (bean.exp <= (expInfoBean.yrs == 0 ? 1 : expInfoBean.yrs) * 2)) {
                    Bundle bundle = new Bundle();
                    bundle.putString(ORG_ID, bean.orgId);
                    bundle.putString(ORG_NAME, bean.orgInfo.name);
                    bundle.putString(ORG_BIO, bean.orgInfo.bio);
                    bundle.putString(ORG_IMG, basePath + bean.orgInfo.img);
                    openActivity(MatchJobActivity.class, bundle);
                }
            }
        }

    }


    @Override
    public void recomendJobClick(int isRecommend, String orgId) {
        if (subscribeFlag == 0 || subscribeFlag == 4) {
            showProgress(true);
            if (isRecommend == 1)
                dashboardPresenter.hitApi(orgId, 1, API_FLAG_JS_IS_RECOMMEND);
            else
                dashboardPresenter.hitApi(orgId, 0, API_FLAG_JS_IS_NOT_RECOMMEND);

        } else
            dialogInitialize.subscriptionDialog(new CustomDialogs(getContext(), this), subscribeFlag);
    }

    @Override
    public void recomendCount(int isRecommend, String jobId) {
        if (subscribeFlag == 0 || subscribeFlag == 4) {
            Bundle bundle = new Bundle();
            bundle.putInt(RECOMMEND, isRecommend);
            bundle.putString(ORG_ID, jobId);
            openActivity(RecommendListActivity.class, bundle);
        } else
            dialogInitialize.subscriptionDialog(new CustomDialogs(getContext(), this), subscribeFlag);


    }

    @Override
    public void mediaPlay() {
        JsJobsResponseModel.JobBean jobBean = null;
        if (jobList != null)
            jobBean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition());
        List<String> imgs = new ArrayList<>();
        if (jobBean.orgInfo != null && jobBean.orgInfo.img != null)
            imgs.add(basePath + jobBean.orgInfo.img);

        if (jobBean.orgInfo.imgs != null) {
            for (String img : jobBean.orgInfo.imgs)
                imgs.add(basePath + img);
        }
        if (jobBean.orgInfo.video != null && !TextUtils.isEmpty(jobBean.orgInfo.video) && !imgs.contains(jobBean.orgInfo.video))
            imgs.add(basePath + jobBean.orgInfo.video);


        if (imgs != null && imgs.size() > 0 && basePath != null) {
            Intent viewMedia = new Intent(getContext(), StoryViewActivity.class);
            viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(imgs));
            startActivity(viewMedia);
        } else {
            if (getContext() != null)
                Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_media_to_play));

        }

    }


    public void searchJobs(String skey) {
        isSearch = true;
        pageNo = 1;
        dashboardPresenter.dashBoardApi(pageNo, skey, DEFAULT_DISTANCE);

    }


    @Override
    public void laterClick() {
        IS_UPLOAD_VIDEO = 2;
        showProgressDialog(true);
        if (jobList.get(mCardStackLayoutManager.getTopPosition() - 1) instanceof JsJobsResponseModel.JobBean) {
            JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition() - 1);

            dashboardPresenter.ApplyJobApi(null, null, bean.id);
        }
    }

    @Override
    public void profileVideoClick() {
        IS_UPLOAD_VIDEO = 3;
        if (getContext() != null)
            if (Utility.getBooleanSharedPreference(getContext(), HAS_VIDEO)) {
                showProgressDialog(true);
                if (jobList.get(mCardStackLayoutManager.getTopPosition() - 1) instanceof JsJobsResponseModel.JobBean) {
                    JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition() - 1);

                    dashboardPresenter.ApplyJobApi(null, "1", bean.id);
                }
            } else
                validationFailed(getContext().getResources().getString(R.string.add_profile_video));
    }

    @Override
    public void ApplyJobClick() {
        IS_UPLOAD_VIDEO = 1;
        openJobList();
    }

    @Override
    public void onYesClicked(String from) {
        if (PURCHASE_PLAN_FLAG.equals(from)) {
            DashboardActivity dashboardActivity = (DashboardActivity) getActivity();
            if (dashboardActivity != null)
                dashboardActivity.fireEvent(JS_POS_SUBSCRIPTION, null);
        }

    }

    @Override
    public void onNoClicked(String from) {

    }

    private List shuffleJobsPromotions(List<JsJobsResponseModel.JobBean> jobList, List<JsJobsResponseModel.Promotions> promotions) {
        int jobIndex = 0;
        int index = 0;
        List<Object> list = new ArrayList<>();
        Collections.shuffle(promotions);

        for (int i = jobIndex; i < jobList.size(); i++) {
            if (i == 0) {
                list.add(jobList.get(i));
                if (getPromotion(index, promotions) != null) {
                    list.add(getPromotion(index, promotions));
                    index = index + 1;

                }
            } else if (jobIndex < 2) {
                list.add(jobList.get(i));
                jobIndex = jobIndex + 1;
            } else {
                if (getPromotion(index, promotions) != null) {
                    list.add(getPromotion(index, promotions));
                    index = index + 1;
                }
                jobIndex = 0;
                break;
            }

        }
        return list;
    }


    private JsJobsResponseModel.Promotions getPromotion(int index, List<JsJobsResponseModel.Promotions> promotions) {
        if (promotions.size() - 1 >= index && promotions.get(index) != null)
            return promotions.get(index);
        else return null;
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");

        if (requestCode == VIDEO_CAMERA_PERMISSION_REQUEST_CODE) {
            SandriosCamera
                    .with()
                    .setMediaAction(CameraConfiguration.MEDIA_ACTION_VIDEO)
                    .showOverlay(true)
                    .launchCamera(this);


           /* Intent intent = new Intent(context, CameraActivity.class);
            intent.putExtra("isApplyJob", true);
            startActivityForResult(intent, REQUEST_CODE_CAMERA_VIDEO);*/
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Utility.showToast(context, "Returned from setting");
                break;

            case REQUEST_CODE_CAMERA_IMG:
                if (resultCode == Activity.RESULT_OK) {
                    videoPath = data.getStringExtra(MEDIA_PATH);
                    if (videoPath != null)
                        videoUtils = new VideoUtils(getContext(), this);
                    else {
                        showProgressDialog(true);
                        if (jobList.get(mCardStackLayoutManager.getTopPosition() - 1) instanceof JsJobsResponseModel.JobBean) {
                            JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition() - 1);
                            dashboardPresenter.ApplyJobApi(null, null, bean.id);
                        }
                    }
                }
                break;
            case ADD_RECRUITER_RESULT_CODE:
                if (getFragmentManager() != null)
                    if (getFragmentManager().findFragmentById(R.id.fragment_container) instanceof RecruiterFragment) {
                        RecruiterFragment recruiterFragment = (RecruiterFragment) getFragmentManager().findFragmentById(R.id.fragment_container);
                        assert recruiterFragment != null;
                        recruiterFragment.onAdd();
                    }
        }


    }

    @Override
    public void showVideoCompressProgress(boolean isShown) {
        if (isShown)
            showProgressDialog(true);
        else
            showProgressDialog(false);
    }

    @Override
    public void getVideoFile(Bitmap bm, File videoFile) {
        if (videoFile != null) {
            if (jobList.get(mCardStackLayoutManager.getTopPosition() - 1) instanceof JsJobsResponseModel.JobBean) {
                JsJobsResponseModel.JobBean bean = (JsJobsResponseModel.JobBean) jobList.get(mCardStackLayoutManager.getTopPosition() - 1);
                dashboardPresenter.ApplyJobApi(videoFile, null, bean.id);
            }
        }
    }

    @Override
    public void isCompressSupport(boolean isCompressSupported) {
        if (videoUtils != null) {
            showProgressDialog(true);
            if (isCompressSupported)
                videoUtils.getCompressedVideo(videoPath);
            else videoUtils.getVideoThumbnail(videoPath);

        }

    }

    @Override
    public void onApplyFilter(int distance) {
        pageNo = 1;
        DEFAULT_DISTANCE = distance;
        if (jobList != null)
            jobList.clear();
        fetchData(distance);
    }

    @Override
    public int getDistance() {
        return DEFAULT_DISTANCE;
    }

}
