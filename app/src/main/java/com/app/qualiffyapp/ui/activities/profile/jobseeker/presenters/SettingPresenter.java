package com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.viewCallbacks.SignupView;

public class SettingPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {

    private SignupView signupView;

    public SettingPresenter(SignupView signupView) {
        this.signupView = signupView;
    }


    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {

    }

    @Override
    public void onApiError(String error, int apiFlag) {

    }
}
