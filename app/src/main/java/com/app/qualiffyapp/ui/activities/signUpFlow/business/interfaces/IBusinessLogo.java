package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IBusinessLogo extends BaseInterface, yesNoCallback {
    void onNext();

    void preUpdate();

    void onUpdate(String msg, AddMediaResponseModel res);


}
