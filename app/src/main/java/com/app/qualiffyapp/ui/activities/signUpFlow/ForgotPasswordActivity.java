package com.app.qualiffyapp.ui.activities.signUpFlow;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityForgotPasswordBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IForgotPassword;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.ForgotPasswordPresenter;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ForgotPasswordActivity extends BaseActivity implements IForgotPassword {
    private ActivityForgotPasswordBinding mBinding;
    private ForgotPasswordPresenter mPresenter;

    @Override
    protected int getLayoutById() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected void initView() {
        mBinding = (ActivityForgotPasswordBinding) viewDataBinding;
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.reset));
        mPresenter = new ForgotPasswordPresenter(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSend:
                Utility.hideKeyboard(this);
                mPresenter.onForgotPass(mBinding.etEmail.getText().toString());
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }

    @Override
    public void preForgotPass() {
        showProgress(true);
    }

    @Override
    protected void setListener() {
        mBinding.tvSend.setOnClickListener(this);
        mBinding.header.backBtnImg.setOnClickListener(this);
    }

    @Override
    public void onForgotPass(int status, String msg) {
        showProgress(false);
        Utility.showToast(this, msg);
        mBinding.etEmail.getText().clear();
        if (status == SUCCESS) {
            finish();
        }
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.EMAIL_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.enter_your_email));
        } else if (toastType == ToastType.EMAIL_FORMAT_ERROR) {
            Utility.showToast(this, getResources().getString(R.string.please_enter_valid_email));
        }
    }
}
