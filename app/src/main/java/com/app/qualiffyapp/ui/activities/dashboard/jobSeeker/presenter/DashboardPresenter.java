package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DASHBOARD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_FOLLOW;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_NOT_RECOMMEND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_IS_RECOMMEND;

public class DashboardPresenter extends ApplyJobPresenter {
    private SignupView signupView;
    ApiListener<ResponseModel<JsJobsResponseModel>> jobResponseModel = new ApiListener<ResponseModel<JsJobsResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<JsJobsResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };
    ApiListener<ResponseModel<MsgResponseModel>> msgResponseModel = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };
    private SignUpInteractor interactor;


    public DashboardPresenter(SignupView signupView) {
        super(signupView);
        this.signupView = signupView;
        this.interactor = new SignUpInteractor();

    }

    public void hitApi(String orgId, int isRight, int apiFlag) {
        switch (apiFlag) {

            case API_FLAG_JS_IS_RECOMMEND:
            case API_FLAG_JS_IS_NOT_RECOMMEND:
                interactor.jobSekekrIsRecommendApi(msgResponseModel, orgId, isRight, apiFlag);
                break;
            case API_FLAG_JS_IS_FOLLOW:
                interactor.jobSekekrIsFollowApi(msgResponseModel, orgId, isRight, apiFlag);
                break;
        }
    }


    public void dashBoardApi(int page, String searchKey,int distance) {
        interactor.jobsSeekerJobsApi(jobResponseModel, page, searchKey,distance, API_FLAG_DASHBOARD);
    }


}
