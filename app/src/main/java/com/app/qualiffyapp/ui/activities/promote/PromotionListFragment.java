package com.app.qualiffyapp.ui.activities.promote;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.promotion.PromotionAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.ItemAddListener;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.FragmentBusinessPromotionBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_PROMOTION_LIST;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_BASE_URL;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_PROMOTION;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class PromotionListFragment extends BaseFragment implements SignupView, PromotionAdapter.clickPromotion, ItemAddListener {

    private FragmentBusinessPromotionBinding mBinding;
    private RecyclerView recyclerView;
    private PromotionAdapter mAdapter;
    private LinearLayoutManager manager;
    private PromotionPresenter mPromotionPresenter;
    private String BASE_URL;
    private ArrayList<PromotionListResponseModel.Prmtn> mPromotionList = new ArrayList<>();


    @Override
    protected void initUi() {
        mBinding = (FragmentBusinessPromotionBinding) viewDataBinding;
        recyclerView = mBinding.rvPromotionList;
        mPromotionPresenter = new PromotionPresenter(this);
        mPromotionPresenter.getPromotions(API_FLAG_PROMOTION_LIST);
        manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_business_promotion;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean progress) {
        showProgress(progress);
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            PromotionListResponseModel model = (PromotionListResponseModel) response.getRes();
            if (model.getPrmtn().getCreated() != null) {
                if (getActivity() != null) {
                    ((DashboardActivity) getActivity()).setPromoteHeader(false,getResources().getString(R.string.promote));
                }
                recyclerView.setVisibility(View.VISIBLE);
                mBinding.tvNoPromotionFound.setVisibility(View.GONE);
                mPromotionList.clear();
                BASE_URL = model.getBp();
                mPromotionList.add(model.getPrmtn());
                mAdapter = new PromotionAdapter(getActivity(), mPromotionList,
                        PromotionListFragment.this, BASE_URL);
                recyclerView.setAdapter(mAdapter);
                recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down));
            } else {
                recyclerView.setVisibility(View.GONE);
                mBinding.tvNoPromotionFound.setVisibility(View.VISIBLE);
            }
        }else {
            recyclerView.setVisibility(View.GONE);
            mBinding.tvNoPromotionFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(getActivity(), error);
    }

    @Override
    public void validationFailed(String msg) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_PROMOTION) {
                mPromotionPresenter.getPromotions(API_FLAG_PROMOTION_LIST);
            }
        }
    }

    @Override
    public void onPromotionClick(String id, int pos) {
        if (mPromotionList.size() > 0) {
            Bundle bundle = new Bundle();
            bundle.putString(IMAGE_BASE_URL, BASE_URL);
            bundle.putString(AppConstants.PROMOTION_EDIT_DATA,
                    Utility.gsonInstance().toJson(mPromotionList.get(pos)));
            openActivityForResult(PromotionActivity.class, bundle, REQUEST_CODE_PROMOTION);
        }
    }

    @Override
    public void onAdd() {
        openActivityForResult(PromotionActivity.class, null, REQUEST_CODE_PROMOTION);
    }
}
