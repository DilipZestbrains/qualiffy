package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SuggestedListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.ActivityJobTypeBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobSeekerInfoPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobTypePresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerInfo;
import com.app.qualiffyapp.utils.RecyclerItemClickListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_JOB_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;


public class JobTypeActivity extends BaseActivity implements SignupView, IJobSeekerInfo, TextWatcher, RecyclerItemClickListener.OnItemClickListener {
    List<SuggestedListResponseModel> modelList;
    private ActivityJobTypeBinding jobTypeBinding;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private JobTypePresenter jobTypePresenter;
    private boolean isUpdate;
    private JobSeekerInfoPresenter presenter;
    private SuggestedListAdapter suggestedListAdapter;
    private List<String> jobTypeList;
    private List<SuggestedListResponseModel> selectedlist;


    @Override
    protected void initView() {
        jobTypeBinding = (ActivityJobTypeBinding) viewDataBinding;
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        profileInputModel.job_type = new ArrayList<>();
        jobTypePresenter = new JobTypePresenter(this);
        presenter = new JobSeekerInfoPresenter(this);
        jobTypeList = Arrays.asList(getResources().getStringArray(R.array.job_type));
        Collections.sort(jobTypeList, String.CASE_INSENSITIVE_ORDER);


        modelList = new ArrayList<>();
        selectedlist = new ArrayList<>();
        setAdapter();
        getIntentData();
    }

    private void setAdapter() {

        for (int i = 0; i < jobTypeList.size(); i++) {
            SuggestedListResponseModel model = new SuggestedListResponseModel();
            model.name = jobTypeList.get(i);
            model.id = String.valueOf(i);
            modelList.add(model);
        }
        suggestedListAdapter = new SuggestedListAdapter(modelList);
        jobTypeBinding.suggestedRcv.setAdapter(suggestedListAdapter);
    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            isUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            ArrayList<String> job_type = bundle.getStringArrayList(JS_JOB_TYPE);
            if (job_type != null && job_type.size() > 0) {
                for (int i = 0; i < job_type.size(); i++)
                    addChip(job_type.get(i), String.valueOf(i));
            }
            toolbar(true);
        } else {
            toolbar(false);

        }
    }


    @Override
    protected void setListener() {
        jobTypeBinding.searchEdt.addTextChangedListener(this);
        jobTypeBinding.indusBtn.btn.setOnClickListener(this);
        jobTypeBinding.suggestedRcv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, this));
    }


    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }

        return 0;
    }

    private void toolbar(boolean isUpdate) {
        jobTypeBinding.toolbar.backBtnImg.setOnClickListener(this);
        jobTypeBinding.titleTxt.setText(getResources().getString(R.string.select_job_type));

        if (isUpdate) {
            jobTypeBinding.indusBtn.btn.setText(getResources().getString(R.string.DONE));
            jobTypeBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_interest));
        } else {
            jobTypeBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
            jobTypeBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));

        }
        jobTypeBinding.indusBtn.btn.setOnClickListener(this);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_job_type;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;

            case R.id.indus_btn:
                if (selectedlist.size() > 0) {
                    profileInputModel.job_type.clear();
                    for (int i = 0; i < selectedlist.size(); i++)
                        profileInputModel.job_type.add(selectedlist.get(i).name);

                    if (profileInputModel.isSignup2Change) {
                        if (isUpdate) {
                            RequestBean bean = new RequestBean();
                            bean.job_type = profileInputModel.job_type;
                            presenter.updateProfile(bean);
                        } else
                            hitApi();
                    } else startActivity(new Intent(this, AddExperienceActivity.class));

                } else {
                    Utility.showToast(this, "Please select atleast one JobType");
                }
                break;
        }

    }

    private void hitApi() {
        if (isUpdate) {
            RequestBean bean = new RequestBean();
            bean.job_type = profileInputModel.job_type;
            presenter.updateProfile(bean);
        } else {
            showProgressDialog(true);
            jobTypePresenter.hitApi(profileInputModel);
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        profileInputModel.isSignup2Change = false;

        if (response.getStatus() == 1) {
            MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
            startActivity(new Intent(this, AddExperienceActivity.class));
        } else
            Utility.showToast(this, response.err.msg);
        showProgressDialog(false);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        profileInputModel.isSignup2Change = false;

        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(int status, String msg) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            Intent returnIntent = new Intent();
            returnIntent.putStringArrayListExtra(JS_JOB_TYPE, profileInputModel.job_type);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.JOB_SEEKER_DESCRIPTION_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.please_enter_job_seeker_intro));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        jobTypeBinding.noDataTxt.noDataFound.setVisibility(View.GONE);


        {
            modelList.clear();
            for (int i = 0; i < jobTypeList.size(); i++) {
                if (jobTypeList.get(i).toLowerCase().startsWith(s.toString().toLowerCase())) {
                    SuggestedListResponseModel model = new SuggestedListResponseModel();
                    model.name = jobTypeList.get(i);
                    model.id = String.valueOf(i);
                    modelList.add(model);
                }
            }

            suggestedListAdapter.notifyDataSetChanged();


        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void onItemClick(View view, int position) {
        addChip(Utility.getFirstLetterCaps(modelList.get(position).name), modelList.get(position).id);
    }

    private void addChip(String txt, String id) {
        profileInputModel.isSignup2Change = true;

        if (!iscontain(txt)) {
            CustomChip chip = new CustomChip(this, txt);
            chip.setId(Integer.parseInt(id));
            chip.setOnCloseIconClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    jobTypeBinding.chipgroup.removeView(view);
                    for (int i = 0; i < selectedlist.size(); i++) {
                        if (selectedlist.get(i).id.equals(String.valueOf(chip.getId()))) {
                            selectedlist.remove(i);
                        }
                        profileInputModel.isSignup2Change = true;
                    }
                }
            });
            jobTypeBinding.chipgroup.addView(chip);

            SuggestedListResponseModel suggestedListResponseModel = new SuggestedListResponseModel();
            suggestedListResponseModel.id = id;
            suggestedListResponseModel.name = txt;
            selectedlist.add(suggestedListResponseModel);

            jobTypeBinding.searchEdt.getText().clear();
        }
    }


    private boolean iscontain(String txt) {
        Boolean isContainKey = false;
        for (SuggestedListResponseModel suggestedListResponseModel : selectedlist) {
            if (txt.equals(suggestedListResponseModel.name))
                return true;
        }
        return isContainKey;
    }
}
