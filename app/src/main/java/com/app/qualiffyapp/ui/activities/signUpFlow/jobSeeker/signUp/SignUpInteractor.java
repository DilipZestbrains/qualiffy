package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp;

import android.content.Intent;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import androidx.annotation.NonNull;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.constants.CompanyPortfolioType;
import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.localDatabase.entity.Contact;
import com.app.qualiffyapp.models.ConnectionResponseModel;
import com.app.qualiffyapp.models.ContactSyncResponse;
import com.app.qualiffyapp.models.NotificationModel;
import com.app.qualiffyapp.models.PhoneRequestBean;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsProfileResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;
import com.app.qualiffyapp.models.contact.BlockContactResonseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;
import com.app.qualiffyapp.models.home.business.UserFrndListResponseModel;
import com.app.qualiffyapp.models.home.business.ViewerResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.RecommendResponseModel;
import com.app.qualiffyapp.models.payment.PaymentHistoryResponse;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.models.profile.OtherUserProfileResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerProfileResponse;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.models.recruiter.RecruiterListResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.network.AppRetrofit;
import com.app.qualiffyapp.ui.activities.signUpFlow.PreActivity;
import com.app.qualiffyapp.utils.Utility;

import java.io.File;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.DEVICE_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.ERROR_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_CLEAR;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.utils.Utility.getMimeType;

public class SignUpInteractor {
    public static final int PAGINATION_LIMIT = 10;
    private final String SEARCH_LIMIT = "50";
    ApplicationController controller;

    public SignUpInteractor() {
        controller = ApplicationController.getApplicationInstance();
    }

    private void clearData(int errorCode, Boolean isBusiness) {
        Intent myIntent = new Intent(ApplicationController.getApplicationInstance(), PreActivity.class);
        myIntent.putExtra(ERROR_CODE, errorCode);
        myIntent.putExtra(IS_BUSINESS_CLEAR, isBusiness);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ApplicationController.getApplicationInstance().startActivity(myIntent);
    }


    public void callApi(final ApiListener<ResponseModel<VerifyResponseModel>> apiListener, RequestBean bean, final int apiFlag) {
        Call<ResponseModel<VerifyResponseModel>> call = AppRetrofit.getInstance().apiServices.singUpApi(bean);
        call.enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }

            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                }
                apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void socailSignupApi(final ApiListener<ResponseModel<VerifyResponseModel>> apiListener, RequestBean bean, final int apiFlag) {
        Call<ResponseModel<VerifyResponseModel>> call = AppRetrofit.getInstance().apiServices.socialSingUpApi(bean);
        call.enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void loginApi(final ApiListener<ResponseModel<VerifyResponseModel>> apiListener,
                         String phnNum, String deviceToken, final int apiFlag) {
        Call<ResponseModel<VerifyResponseModel>> call =
                AppRetrofit.getInstance().apiServices.loginApi(phnNum, deviceToken, DEVICE_TYPE);
        call.enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void verifyApi(final ApiListener<ResponseModel<VerifyResponseModel>> apiListener,
                          String userId, String otp, String token, final int apiFlag) {
        Call<ResponseModel<VerifyResponseModel>> call =
                AppRetrofit.getInstance().apiServices.verifyOtpApi(userId, otp, token);
        call.enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getIndustryyApi(final ApiListener<ResponseModel<IndustryResponseModel>> apiListener, String searckKey, final int apiFlag) {
        Call<ResponseModel<IndustryResponseModel>> call = AppRetrofit.getInstance().apiServices.getIndustryApi(SEARCH_LIMIT, searckKey);
        call.enqueue(new Callback<ResponseModel<IndustryResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<IndustryResponseModel>> call, Response<ResponseModel<IndustryResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<IndustryResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void getRoleSeekingApi(final ApiListener<ResponseModel<IndustryResponseModel>> apiListener, String searckKey, final int apiFlag) {
        Call<ResponseModel<IndustryResponseModel>> call = AppRetrofit.getInstance().apiServices.getRoleSeekingApi(SEARCH_LIMIT, searckKey);
        call.enqueue(new Callback<ResponseModel<IndustryResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<IndustryResponseModel>> call, Response<ResponseModel<IndustryResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<IndustryResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getRoleApi(final ApiListener<ResponseModel<IndustryResponseModel>> apiListener, String searckKey, String indusId, final int apiFlag) {
        Call<ResponseModel<IndustryResponseModel>> call = AppRetrofit.getInstance().apiServices.getRoleApi(indusId, SEARCH_LIMIT, searckKey);
        call.enqueue(new Callback<ResponseModel<IndustryResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<IndustryResponseModel>> call, Response<ResponseModel<IndustryResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<IndustryResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getSignUp1Api(final ApiListener<ResponseModel<IndustryResponseModel>> apiListener, CreateJobSeekerProfileInputModel profileInputModel, final int apiFlag) {
        Call<ResponseModel<IndustryResponseModel>> call = AppRetrofit.getInstance().apiServices.signUp1Api(profileInputModel);
        call.enqueue(new Callback<ResponseModel<IndustryResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<IndustryResponseModel>> call, Response<ResponseModel<IndustryResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<IndustryResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void signUp2Api(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, CreateJobSeekerProfileInputModel profileInputModel, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.signUp2Api(profileInputModel);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void signUp3Api(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, CreateJobSeekerProfileInputModel profileInputModel, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.signUp3Api(profileInputModel);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void signUp4Api(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, CreateJobSeekerProfileInputModel profileInputModel, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.signUp4Api(profileInputModel);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void signUp5Api(final ApiListener<ResponseModel<VerifyResponseModel>> apiListener, RequestBean bean, final int apiFlag) {

        Call<ResponseModel<VerifyResponseModel>> call = AppRetrofit.getInstance().apiServices.signUp5Api(bean);
        call.enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void updateJobSeekerDetails(final ApiListener<MsgResponseModel> apiListener, RequestBean bean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.updateJobSeekerDetails(bean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void addMediaApi(final ApiListener<ResponseModel<AddMediaResponseModel>> apiListener, File file, String uid, String isProfile, final int apiFlag) {
        MultipartBody.Part filePart;
        RequestBody requestFile = RequestBody.create(MediaType.parse(getMimeType(file.getAbsolutePath())), file);
        if (apiFlag == API_FLAG_ADD_MEDIA) {
            filePart = MultipartBody.Part.createFormData("img", file.getName(), requestFile);
        } else {
            filePart = MultipartBody.Part.createFormData("video", file.getName(), requestFile);
        }
        RequestBody _uid = RequestBody.create(MediaType.parse("text/plain"), uid);
        RequestBody _isProfile = RequestBody.create(MediaType.parse("text/plain"), isProfile);
        Call<ResponseModel<AddMediaResponseModel>> call = AppRetrofit.getInstance().apiServices.addMedia(filePart, _uid, _isProfile);
        call.enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void addMediaOrgApi(final ApiListener<ResponseModel<AddMediaResponseModel>> apiListener, File file, RequestBean requestBean, final int apiFlag) {
        MultipartBody.Part filePart = null;
        HashMap<String, RequestBody> requestBodyHashMap = new HashMap<>();
        if (file != null) {
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file.getAbsolutePath())))), file);
            if (apiFlag == API_FLAG_BUSINESS_ADD_VIDEO) {
                filePart = MultipartBody.Part.createFormData("video", file.getName(), fileBody);
            } else {
                filePart = MultipartBody.Part.createFormData("img", file.getName(), fileBody);

            }
        }
        if (requestBean.uid != null)
            requestBodyHashMap.put("uid", createPartFromString(requestBean.uid));
        if (requestBean.isProfile != null)
            requestBodyHashMap.put("isProfile", createPartFromString(requestBean.isProfile));

        Call<ResponseModel<AddMediaResponseModel>> call = AppRetrofit.getInstance().apiServices.addMediaOrganization(filePart, requestBodyHashMap);
        call.enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, true);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void deleteApi(final ApiListener<ResponseModel<AddMediaResponseModel>> apiListener, String mediaId, String mediaType, final int apiFlag) {
        Call<ResponseModel<AddMediaResponseModel>> call = AppRetrofit.getInstance().apiServices.deleteMedia(mediaId, mediaType);
        call.enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }
    //****************************************** Job Seeker signup Apis finish *******************************************


    //****************************************** Job Seeker profile Apis *******************************************

    public void getJobSeekerDetails(final ApiListener<JobSeekerProfileResponse> apiListener, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.getJobSeekerDetails().enqueue(new Callback<ResponseModel<JobSeekerProfileResponse>>() {
            @Override
            public void onResponse(Call<ResponseModel<JobSeekerProfileResponse>> call, Response<ResponseModel<JobSeekerProfileResponse>> response) {
                if (response.body() != null) {
                    {
                        if (response.body().getStatus() == SUCCESS) {
                            apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                        } else if (response.body().err != null) {
                            if ((response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7))
                                clearData(response.body().err.errCode, false);
                            else apiListener.onApiError(response.body().err.msg, apiFlag);
                        } else {
                            apiListener.onApiNoResponse(apiFlag);
                        }
                    }
                } else apiListener.onApiNoResponse(apiFlag);


            }

            @Override
            public void onFailure(Call<ResponseModel<JobSeekerProfileResponse>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void addJobSeekerEducation(
            final ApiListener<VerifyResponseModel> apiListener, RequestBean bean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.addJobSeekerEducation(bean).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void updateJobSeekerEducation(final ApiListener<VerifyResponseModel> apiListener, String eduId, RequestBean bean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.updateJobSeekerEducaiton(eduId, bean).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void deleteJobSeekerEducation(final ApiListener<VerifyResponseModel> apiListener, String eduId, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.deleteEducation(eduId).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerlogout(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerLogout();
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);

                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerSubscription(final ApiListener<ResponseModel<JsSubscriptionModel>> apiListener, int pageNo, int limit, final int apiFlag) {
        Call<ResponseModel<JsSubscriptionModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerSubscription(pageNo, limit);
        call.enqueue(new Callback<ResponseModel<JsSubscriptionModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<JsSubscriptionModel>> call, Response<ResponseModel<JsSubscriptionModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<JsSubscriptionModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessSubscription(final ApiListener<ResponseModel<JsSubscriptionModel>> apiListener, int pageNo, int limit, final int apiFlag) {
        Call<ResponseModel<JsSubscriptionModel>> call = AppRetrofit.getInstance().apiServices.businessSubscription(pageNo, limit);
        call.enqueue(new Callback<ResponseModel<JsSubscriptionModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<JsSubscriptionModel>> call, Response<ResponseModel<JsSubscriptionModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<JsSubscriptionModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerSubscriptionCurrent(final ApiListener<ResponseModel<JsSubscriptionModel>> apiListener, final int apiFlag) {
        Call<ResponseModel<JsSubscriptionModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerSubscriptionCurrent();
        call.enqueue(new Callback<ResponseModel<JsSubscriptionModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<JsSubscriptionModel>> call, Response<ResponseModel<JsSubscriptionModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<JsSubscriptionModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessSubscriptionCurrent(final ApiListener<ResponseModel<JsSubscriptionModel>> apiListener, final int apiFlag) {
        Call<ResponseModel<JsSubscriptionModel>> call = AppRetrofit.getInstance().apiServices.businessSubscriptionCurrent();
        call.enqueue(new Callback<ResponseModel<JsSubscriptionModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<JsSubscriptionModel>> call, Response<ResponseModel<JsSubscriptionModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<JsSubscriptionModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerProfileToogle(final ApiListener<MsgResponseModel> apiListener, RequestBean bean, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.JobSeekerProfileToogle(bean);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerDeleteAccount(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerDelete();
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void jobsSeekerOtherUser(final ApiListener<ResponseModel<OtherUserProfileResponseModel>> apiListener, String userId, final int apiFlag) {
        Call<ResponseModel<OtherUserProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerOtherUserProfile(userId);
        call.enqueue(new Callback<ResponseModel<OtherUserProfileResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<OtherUserProfileResponseModel>> call, Response<ResponseModel<OtherUserProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<OtherUserProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerOtherBadge(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, int key, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerOtherUserBadge(userId, key);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    //JobSeeker Dashboard
    public void jobsSeekerJobsApi(final ApiListener<ResponseModel<JsJobsResponseModel>> apiListener, int page, String searchKey, int distance, final int apiFlag) {
        Call<ResponseModel<JsJobsResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerJobs(page, searchKey, PAGINATION_LIMIT, distance);
        call.enqueue(new Callback<ResponseModel<JsJobsResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<JsJobsResponseModel>> call, Response<ResponseModel<JsJobsResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<JsJobsResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobsSeekerRecommendJobListApi(final ApiListener<ResponseModel<RecommendResponseModel>> apiListener, String isRecommend, String jobId, int pageNo, final int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerRecommendJobList(jobId, isRecommend, pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jsApplyJobWithVideoApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, File file, String jobId, final int apiFlag) {
        MultipartBody.Part filePart = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse(getMimeType(file.getAbsolutePath())), file);
            filePart = MultipartBody.Part.createFormData("video", file.getName(), requestFile);
        }

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerApplyJobWithVideo(filePart, jobId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jsApplyJobApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String jobId, String isProfileVideo, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerApplyJob(jobId, isProfileVideo);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobListApi(final ApiListener<ResponseModel<OrgJobListResponseModel>> apiListener, String orgId, int pageNo, final int apiFlag) {

        Call<ResponseModel<OrgJobListResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerJobList(orgId, pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<OrgJobListResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<OrgJobListResponseModel>> call, Response<ResponseModel<OrgJobListResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<OrgJobListResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void jobSekekrIsRecommendApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String orgId, int isRecommend, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrIsRecommend(orgId, String.valueOf(isRecommend));
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSeekerOwnRecmdation(final ApiListener<ResponseModel<ViewerResponseModel>> apiListener, int pageNo, final int apiFlag) {

        Call<ResponseModel<ViewerResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerOwnRecmdation(pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<ViewerResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<ViewerResponseModel>> call, Response<ResponseModel<ViewerResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ViewerResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSeekerOwnFollowing(final ApiListener<ResponseModel<ViewerResponseModel>> apiListener, int pageNo, final int apiFlag) {

        Call<ResponseModel<ViewerResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerOwnFollowing(pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<ViewerResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<ViewerResponseModel>> call, Response<ResponseModel<ViewerResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ViewerResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void jobSekekrIsFollowApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String orgId, int isFollow, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrIsFollow(orgId, String.valueOf(isFollow));
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrAddfrndApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrAddFrnd(userId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrAcceptFrndApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String useriD, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrAcceptFrnd(useriD);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrRejectFrndApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrRejectFrnd(userId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrBlockFrndApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, String b_key, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrblockUnblockFrnd(userId, b_key);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrBlockFrndListApi(final ApiListener<ResponseModel<BlockContactResonseModel>> apiListener, int pageNo, final int apiFlag) {

        Call<ResponseModel<BlockContactResonseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekrblockFrndList(pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<BlockContactResonseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<BlockContactResonseModel>> call, Response<ResponseModel<BlockContactResonseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<BlockContactResonseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void jobSekekrAppliedJobApi(final ApiListener<ResponseModel<AppliedJobResponseModel>> apiListener, int pageNo, String isShortlist, final int apiFlag) {

        Call<ResponseModel<AppliedJobResponseModel>> call = AppRetrofit.getInstance().apiServices.jobSeekerAppliedJob(pageNo, PAGINATION_LIMIT, isShortlist);
        call.enqueue(new Callback<ResponseModel<AppliedJobResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AppliedJobResponseModel>> call, Response<ResponseModel<AppliedJobResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 47 || response.body().err.errCode == 7 || response.body().err.errCode == 7))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AppliedJobResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


//****************************Business Signup Apis **********************************************************

    public void deleteOrgMediaApi(final ApiListener<ResponseModel<AddMediaResponseModel>> apiListener, String mediaId, int type,final int apiFlag) {
        Call<ResponseModel<AddMediaResponseModel>> call = AppRetrofit.getInstance().apiServices.deleteMediaOrganization(mediaId,type);
        call.enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, true);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessSignUp(final ApiListener<SingupResponse> apiListener, RequestBean requestBean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.businessSignUp(requestBean).enqueue(new Callback<ResponseModel<SingupResponse>>() {
            @Override
            public void onResponse(Call<ResponseModel<SingupResponse>> call, Response<ResponseModel<SingupResponse>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else if (response.body().err.errs != null && response.body().err.errs.get(0) != null) {
                            apiListener.onApiError(response.body().err.errs.get(0).message, apiFlag);
                        } else if (response.body().err.msg != null)
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<SingupResponse>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);

            }
        });
    }

    public void businessSignup2(final ApiListener<VerifyResponseModel> apiListener, RequestBean requestBean, final int apiFlag) {
        HashMap<String, RequestBody> requestBeanHashMap = new HashMap<>();
        MultipartBody.Part part = null;
        if (!TextUtils.isEmpty(requestBean.imgUrl)) {
            File file = new File(requestBean.imgUrl);
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(requestBean.imgUrl)))), file);
            part = MultipartBody.Part.createFormData("img", file.getName(), fileBody);
        }
        if (requestBean.name != null) {
            requestBeanHashMap.put("name", createPartFromString(requestBean.name));
        }
        if (requestBean.bio != null) {
            requestBeanHashMap.put("bio", createPartFromString(requestBean.bio));
        }
        if (requestBean.jobC != null) {
            requestBeanHashMap.put("jobC", createPartFromString(TextUtils.join(",", requestBean.jobC)));
        }
        if (requestBean.address != null) {
            requestBeanHashMap.put("address", createPartFromString(requestBean.address));
        }
        if (requestBean.cntry != null) {
            requestBeanHashMap.put("cntry", createPartFromString(requestBean.cntry));
        }
        if (requestBean.state != null) {
            requestBeanHashMap.put("state", createPartFromString(requestBean.state));
        }
        if (requestBean.lat != null) {
            requestBeanHashMap.put("lat", createPartFromString(requestBean.lat));
        }
        if (requestBean.lng != null) {
            requestBeanHashMap.put("lng", createPartFromString(requestBean.lng));
        }
//        if (requestBean.reg != null) {
//            requestBeanHashMap.put("reg", createPartFromString(requestBean.reg));
//        }
        if (requestBean.employStrength != null) {
            requestBeanHashMap.put("empls", createPartFromString(requestBean.employStrength));
        }
        AppRetrofit.getInstance().apiServices.businessSignUp2(part, requestBeanHashMap).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);

            }
        });
    }

    public void businessVerifyOtp(final ApiListener<VerifyResponseModel> apiListener, RequestBean requestBean, final int apiFlag) {

        AppRetrofit.getInstance().apiServices.businessVerify(requestBean).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getLocalizedMessage(), apiFlag);

            }
        });
    }


    public void businessLogin(final ApiListener<VerifyResponseModel> apiListener, String pno, String pass, String dev_tkn, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.businessLogin(pno, pass, dev_tkn).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                } else apiListener.onApiError(null, apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);

            }
        });
    }

    public void businessLargeOrgLogin(final ApiListener<VerifyResponseModel> apiListener, String email, String pass, String dev_tkn, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.largeBusinessLoginEmail(email, pass, dev_tkn).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);

                    } else if (response.body().err != null) {
                        if (response.body().err.errs != null && response.body().err.errs.get(0) != null) {
                            apiListener.onApiError(response.body().err.errs.get(0).message, apiFlag);
                        } else if (response.body().err.msg != null)
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                        else apiListener.onApiNoResponse(apiFlag);
                    }

                } else apiListener.onApiError(null, apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                }
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });

    }

    public void businessLargeOrgLoginWithPhone(final ApiListener<SingupResponse> apiListener, RequestBean requestBean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.largeBusinessLogin(requestBean).enqueue(new Callback<ResponseModel<SingupResponse>>() {
            @Override
            public void onResponse(Call<ResponseModel<SingupResponse>> call, Response<ResponseModel<SingupResponse>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if (response.body().err.errs != null && response.body().err.errs.get(0) != null) {
                            apiListener.onApiError(response.body().err.errs.get(0).message, apiFlag);
                        } else if (response.body().err.msg != null)
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                } else apiListener.onApiError(null, apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<SingupResponse>> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);

            }
        });
    }


    public void businessLoginWithPhoneVerify(final ApiListener<VerifyResponseModel> apiListener, RequestBean requestBean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.largeBusinessLoginVerify(requestBean).enqueue(new Callback<ResponseModel<VerifyResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<VerifyResponseModel>> call, Response<ResponseModel<VerifyResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if (response.body().err.errs != null && response.body().err.errs.get(0) != null) {
                            apiListener.onApiError(response.body().err.errs.get(0).message, apiFlag);
                        } else if (response.body().err.msg != null)
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                } else apiListener.onApiError(null, apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<VerifyResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else
                    apiListener.onApiError(t.getMessage(), apiFlag);

            }
        });
    }

    public void forgotPassword(final ApiListener<MsgResponseModel> apiListener, String email, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.forgotPass(email).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if (response.body().err.errs != null && response.body().err.errs.get(0) != null) {
                            apiListener.onApiError(response.body().err.errs.get(0).message, apiFlag);
                        } else if (response.body().err.msg != null)
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    }
                } else apiListener.onApiError(null, apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }


    public void getOrgProfile(final ApiListener<BusinessProfileResponseModel> apiListener, final int apiFlag) {
        Call<ResponseModel<BusinessProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.getBusinessProfile();
        call.enqueue(new Callback<ResponseModel<BusinessProfileResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<BusinessProfileResponseModel>> call, Response<ResponseModel<BusinessProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<BusinessProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void businessProfileEdit(final ApiListener<MsgResponseModel> apiListener, RequestBean bean, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessProfileEdit(bean);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });

    }

    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(MediaType.parse("multipart/form-data"), descriptionString);

    }


    public void changePasswordBusiness(final ApiListener<MsgResponseModel> apiListener, String oldPass, String newPass, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.changePasswordBusiness(oldPass, newPass).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void deleteOrganisation(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.deleteOrganisation().enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void logoutOrganisation(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String device_tkn, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.logoutOrganisation(device_tkn).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, true);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);

                /*    if (response.body().getStatus() == SUCCESS) {
                    } else if (response.body().err != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);*/
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getCompanyPortFolio(final ApiListener<WebResponseModel> apiListener, CompanyPortfolioType type, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.getCompanyPortfolio(type.value).enqueue(new Callback<ResponseModel<WebResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<WebResponseModel>> call, Response<ResponseModel<WebResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<WebResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });

    }

    public void manageBusinessToggle(final ApiListener<MsgResponseModel> apiListener, RequestBean bean, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.manageBusinessToggle(bean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void deleteAllMediaApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, final int apiFlag) {
        AppRetrofit.getInstance().apiServices.deleteAllBusinessMedia().enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void putCustomJob(final ApiListener<ResponseModel<AddCustomeJobModel>> apiListener,
                             String cat_id, String name, int apiFlag) {
        AppRetrofit.getInstance().apiServices.addCustomJobTitle(cat_id, name).enqueue(new Callback<ResponseModel<AddCustomeJobModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<AddCustomeJobModel>> call, Response<ResponseModel<AddCustomeJobModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<AddCustomeJobModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void putCustomJobWtCat(final ApiListener<ResponseModel<AddCustomeJobModel>> apiListener, String name, int apiFlag) {
        AppRetrofit.getInstance().apiServices.addCustomJobTitleCustom(name).enqueue(new Callback<ResponseModel<AddCustomeJobModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<AddCustomeJobModel>> call, Response<ResponseModel<AddCustomeJobModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<AddCustomeJobModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    //chat

    public void uploadChatMedia(String path, String uniqueName, ApiListener<AddMediaResponseModel> apiListener, int apiFlag) {
        MultipartBody.Part part = null;
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path));
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(mime)), file);
            part = MultipartBody.Part.createFormData("chat_media", file.getName(), fileBody);
        }
        AppRetrofit.getInstance().apiServices.uploadChatMedia(part, createPartFromString(uniqueName)).enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void uploadOrgChatMedia(String path, String uniqueName, ApiListener<AddMediaResponseModel> apiListener, int apiFlag) {
        MultipartBody.Part part = null;
        if (!TextUtils.isEmpty(path)) {
            File file = new File(path);
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(path));
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(mime)), file);
            part = MultipartBody.Part.createFormData("chat_media", file.getName(), fileBody);
        }
        AppRetrofit.getInstance().apiServices.uploadOrgChatMedia(part, createPartFromString(uniqueName)).enqueue(new Callback<ResponseModel<AddMediaResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<AddMediaResponseModel>> call, Response<ResponseModel<AddMediaResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<AddMediaResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessDashBoardApi(final ApiListener<ResponseModel<BusinessHomeResponseModel>> apiListener, int page, String skey, int distance, final int apiFlag) {
        Call<ResponseModel<BusinessHomeResponseModel>> call = AppRetrofit.getInstance().apiServices.businessHomeApi(page, PAGINATION_LIMIT, skey, distance);
        call.enqueue(new Callback<ResponseModel<BusinessHomeResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<BusinessHomeResponseModel>> call, Response<ResponseModel<BusinessHomeResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<BusinessHomeResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessViewerdApi(final ApiListener<ResponseModel<ViewerResponseModel>> apiListener, String userId, int page, final int apiFlag) {
        Call<ResponseModel<ViewerResponseModel>> call = AppRetrofit.getInstance().apiServices.businessUserViewerApi(userId, page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<ViewerResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<ViewerResponseModel>> call, Response<ResponseModel<ViewerResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ViewerResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void businessRecruiterListApi(final ApiListener<ResponseModel<RecruiterListResponseModel>> apiListener, int page, final int apiFlag) {
        Call<ResponseModel<RecruiterListResponseModel>> call = AppRetrofit.getInstance().apiServices.businessRecruiterListApi(page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecruiterListResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<RecruiterListResponseModel>> call, Response<ResponseModel<RecruiterListResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46)) {
                        clearData(response.body().err.errCode, false);
                    } else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<RecruiterListResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void UserFrndListApi(final ApiListener<ResponseModel<UserFrndListResponseModel>> apiListener, String userId, int page, final int apiFlag) {
        Call<ResponseModel<UserFrndListResponseModel>> call = AppRetrofit.getInstance().apiServices.businessFrndListApi(userId, page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<UserFrndListResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<UserFrndListResponseModel>> call, Response<ResponseModel<UserFrndListResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<UserFrndListResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void businessAddJobApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, RequestBean bean, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessAddJobApi(bean);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessDeleteJobApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String jobId, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessDeleteJobApi(jobId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessEditJobApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String jobId, String date, final int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessEditJobApi(jobId, date);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else
                        apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    public void businessIsFollowApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, int isFollow, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessIsFollow(userId, String.valueOf(isFollow));
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessAddedJobApi(final ApiListener<ResponseModel<AddedJobResponseModel>> apiListener, int page, final int apiFlag) {

        Call<ResponseModel<AddedJobResponseModel>> call = AppRetrofit.getInstance().apiServices.businessAddedJobApi(page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<AddedJobResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<AddedJobResponseModel>> call, Response<ResponseModel<AddedJobResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<AddedJobResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessJobApplicantsApi(final ApiListener<ResponseModel<ApplicantsResponseModel>> apiListener, String jobId, int page, final int apiFlag) {

        Call<ResponseModel<ApplicantsResponseModel>> call = AppRetrofit.getInstance().apiServices.businessJobApplicantsApi(jobId, page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<ApplicantsResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<ApplicantsResponseModel>> call, Response<ResponseModel<ApplicantsResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ApplicantsResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessJobApplicantsProfileApi(final ApiListener<ResponseModel<ApplicantsProfileResponseModel>> apiListener, String userId, final int apiFlag) {

        Call<ResponseModel<ApplicantsProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.businessApplicantProfileApi(userId);
        call.enqueue(new Callback<ResponseModel<ApplicantsProfileResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<ApplicantsProfileResponseModel>> call, Response<ResponseModel<ApplicantsProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ApplicantsProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessOtherUserProfileApi(final ApiListener<ResponseModel<OtherUserProfileResponseModel>> apiListener, String userId, final int apiFlag) {

        Call<ResponseModel<OtherUserProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.businessOtherUserProfileApi(userId);
        call.enqueue(new Callback<ResponseModel<OtherUserProfileResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<OtherUserProfileResponseModel>> call, Response<ResponseModel<OtherUserProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<OtherUserProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessOtherUserProfileBadge(final ApiListener<ResponseModel<MsgResponseModel>> apiListener,
                                              String userId, int key, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call =
                AppRetrofit.getInstance().apiServices.businessOtherUserBadge(userId, String.valueOf(key));
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessIsShortlistApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, String jobId, int isFollow, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessIsShortList(jobId, userId, String.valueOf(isFollow));
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void businessAddViewCountApi(final ApiListener<ResponseModel<MsgResponseModel>> apiListener, String userId, final int apiFlag) {

        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.businessApplicantAddViewCountApi(userId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {

            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().err != null && (response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                        clearData(response.body().err.errCode, false);
                    else apiListener.onApiSuccess(response.body(), apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


    //Business Chat
    public void deleteChatMedia(final ApiListener<MsgResponseModel> apiListener, ArrayList<Chat> chats, final int apiFlag) {
        final ArrayList<String> chatIdList = new ArrayList<>();

        for (Chat c : chats) {
            if (!c.getMedia_type().equals(com.app.qualiffyapp.firebase.chat.types.MediaType.TEXT.value)) {
                String id = TextUtils.substring(c.getMedia_url(), TextUtils.lastIndexOf(c.getMedia_url(), '/'), c.getMedia_url().length());
                chatIdList.add(id);
            }
        }
        if (chatIdList.size() <= 0) {
            apiListener.onApiSuccess(null, apiFlag);
            return;
        }
        RequestBean requestBean = new RequestBean();
        requestBean.chat_media = chatIdList;
        AppRetrofit.getInstance().apiServices.deleteChatMedia(requestBean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void deleteOrgChatMedia(final ApiListener<MsgResponseModel> apiListener, ArrayList<Chat> chats, final int apiFlag) {
        final ArrayList<String> chatIdList = new ArrayList<>();

        for (Chat c : chats) {
            if (!c.getMedia_type().equals(com.app.qualiffyapp.firebase.chat.types.MediaType.TEXT.value)) {
                String id = TextUtils.substring(c.getMedia_url(), TextUtils.lastIndexOf(c.getMedia_url(), '/'), c.getMedia_url().length());
                chatIdList.add(id);
            }
        }
        if (chatIdList.size() <= 0) {
            apiListener.onApiSuccess(null, apiFlag);
            return;
        }
        RequestBean requestBean = new RequestBean();
        requestBean.chat_media = chatIdList;
        AppRetrofit.getInstance().apiServices.deleteOrgChatMedia(requestBean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getSyncedContactList(ApiListener<ArrayList<Contact>> apiListener, ArrayList<Contact> contactList, int apiFlag) {
        PhoneRequestBean requestBean = new PhoneRequestBean();
        requestBean.pno = new ArrayList<>();
        for (Contact c : contactList) {
            try {
                requestBean.pno.add(c.getPno());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        AppRetrofit.getInstance().apiServices.syncContacts(requestBean).enqueue(new Callback<ResponseModel<ContactSyncResponse>>() {
            @Override
            public void onResponse(Call<ResponseModel<ContactSyncResponse>> call, Response<ResponseModel<ContactSyncResponse>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        ContactSyncResponse contactSyncResponse = response.body().getRes();
                        apiListener.onApiSuccess(contactSyncResponse.getSyncedContacts(contactList), apiFlag);
                    } else if (response.body().err != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<ContactSyncResponse>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void callPromotionList(ApiListener<ResponseModel<PromotionListResponseModel>> apiListener, int flagSeasonList) {
        Call<ResponseModel<PromotionListResponseModel>> call = AppRetrofit.getInstance().apiServices.getAllPromotions();
        call.enqueue(new Callback<ResponseModel<PromotionListResponseModel>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel<PromotionListResponseModel>> call, @NonNull Response<ResponseModel<PromotionListResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), flagSeasonList);
                } else {
                    apiListener.onApiError("Response is null.", flagSeasonList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseModel<PromotionListResponseModel>> call, Throwable t) {
                if (!TextUtils.isEmpty(t.getMessage()))
                    if (t.getMessage().contains("Failed to connect to")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", flagSeasonList);
                    } else if (t.getMessage().contains("associated with hostname")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", flagSeasonList);
                    } else {
                        apiListener.onApiError(t.getMessage(), flagSeasonList);
                    }
            }
        });
    }

    public void updatePromotion(ApiListener<ResponseModel<MsgResponseModel>> apiListener, String promotionId, RequestBean requestBean, int apiFlag) {
        HashMap<String, RequestBody> requestBeanHashMap = new HashMap<>();
        MultipartBody.Part part = null;
        if (!TextUtils.isEmpty(requestBean.imgUrl) && !requestBean.imgUrl.contains("http")) {
            File file = new File(requestBean.imgUrl);
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(requestBean.imgUrl)))), file);
            part = MultipartBody.Part.createFormData("img", file.getName(), fileBody);
        }
        if (requestBean.n_loc != null) {
            requestBeanHashMap.put("n_loc", createPartFromString(requestBean.n_loc));
        }
        if (requestBean.desc != null) {
            requestBeanHashMap.put("desc", createPartFromString(requestBean.desc));
        }


        AppRetrofit.getInstance().apiServices.updatePromotion(promotionId, part, requestBeanHashMap).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                } else {
                    apiListener.onApiError("Response is null.", apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (!TextUtils.isEmpty(t.getMessage()))
                    if (t.getMessage().contains("Failed to connect to")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else if (t.getMessage().contains("associated with hostname")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else {
                        apiListener.onApiError(t.getMessage(), apiFlag);
                    }
            }

        });


    }

    public void addPromotion(ApiListener<ResponseModel<MsgResponseModel>> apiListener, RequestBean requestBean, int apiFlag) {
        HashMap<String, RequestBody> requestBeanHashMap = new HashMap<>();
        MultipartBody.Part part = null;
        if (!TextUtils.isEmpty(requestBean.imgUrl)) {
            File file = new File(requestBean.imgUrl);
            RequestBody fileBody = RequestBody.create(MediaType.parse(Objects.requireNonNull(MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(requestBean.imgUrl)))), file);
            part = MultipartBody.Part.createFormData("img", file.getName(), fileBody);
        }
        if (requestBean.n_loc != null) {
            requestBeanHashMap.put("n_loc", createPartFromString(requestBean.n_loc));
        }
        if (requestBean.desc != null) {
            requestBeanHashMap.put("desc", createPartFromString(requestBean.desc));
        }


        AppRetrofit.getInstance().apiServices.addPromotion(part, requestBeanHashMap).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                } else {
                    apiListener.onApiError("Response is null.", apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (!TextUtils.isEmpty(t.getMessage()))
                    if (t.getMessage().contains("Failed to connect to")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else if (t.getMessage().contains("associated with hostname")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else {
                        apiListener.onApiError(t.getMessage(), apiFlag);
                    }
            }

        });


    }

    public void callNotificationApiJobSeeker(ApiListener<ResponseModel<NotificationModel>> apiListener,
                                             int pageNumber, int apiFlag) {

        Call<ResponseModel<NotificationModel>> call =
                AppRetrofit.getInstance().apiServices.getAllNotificationsJobSeeker(String.valueOf(pageNumber), "20");
        call.enqueue(new Callback<ResponseModel<NotificationModel>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel<NotificationModel>> call, @NonNull Response<ResponseModel<NotificationModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                } else {
                    apiListener.onApiError("Response is null.", apiFlag);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseModel<NotificationModel>> call, Throwable t) {
                if (!TextUtils.isEmpty(t.getMessage()))
                    if (t.getMessage().contains("Failed to connect to")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else if (t.getMessage().contains("associated with hostname")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else {
                        apiListener.onApiError(t.getMessage(), apiFlag);
                    }
            }
        });
    }

    public void callNotificationApiBusiness(ApiListener<ResponseModel<NotificationModel>> apiListener, int pageNumber, int apiFlag) {
        Call<ResponseModel<NotificationModel>> call =
                AppRetrofit.getInstance().apiServices.getAllNotificationsBusiness(String.valueOf(pageNumber), "20");
        call.enqueue(new Callback<ResponseModel<NotificationModel>>() {
            @Override
            public void onResponse(@NonNull Call<ResponseModel<NotificationModel>> call, @NonNull Response<ResponseModel<NotificationModel>> response) {
                if (response.body() != null) {
                    apiListener.onApiSuccess(response.body(), apiFlag);
                } else {
                    apiListener.onApiError("Response is null.", apiFlag);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseModel<NotificationModel>> call, Throwable t) {
                if (!TextUtils.isEmpty(t.getMessage()))
                    if (t.getMessage().contains("Failed to connect to")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else if (t.getMessage().contains("associated with hostname")) {
                        apiListener.onApiError("Your network connection is too slow, Please check your connection.", apiFlag);
                    } else {
                        apiListener.onApiError(t.getMessage(), apiFlag);
                    }
            }
        });

    }

    public void callNotificationDeleteJobSeeker(ApiListener<ResponseModel<NotificationModel>> apiListener, String id, int apiFlag) {

        AppRetrofit.getInstance().apiServices.deleteNotificationJobSeeker(id).enqueue(new Callback<ResponseModel<NotificationModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<NotificationModel>> call, Response<ResponseModel<NotificationModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<NotificationModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void callNotificationDeleteBusiness(ApiListener<ResponseModel<NotificationModel>> apiListener, String id, int apiFlag) {
        AppRetrofit.getInstance().apiServices.deleteNotificationBusiness(id).enqueue(new Callback<ResponseModel<NotificationModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<NotificationModel>> call, Response<ResponseModel<NotificationModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                            clearData(response.body().err.errCode, false);
                        else apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else {
                        apiListener.onApiNoResponse(apiFlag);
                    }
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<NotificationModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });

    }

    public void sendNotification(ApiListener<MsgResponseModel> apiListener, int userType, String senderType, String id, RequestBean bean, int apiFlag) {
        if (userType == 1) {
            AppRetrofit.getInstance().apiServices.sendNotificationByJobSeeker(id, senderType, bean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
                @Override
                public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == SUCCESS) {
                            apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                        } else if (response.body().err != null) {
                            if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                                clearData(response.body().err.errCode, false);
                            else apiListener.onApiError(response.body().err.msg, apiFlag);
                        } else {
                            apiListener.onApiNoResponse(apiFlag);
                        }
                    } else apiListener.onApiNoResponse(apiFlag);
                }

                @Override
                public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                    if (t instanceof UnknownHostException) {
                        apiListener.onApiError("Please check internet connection.", apiFlag);

                    } else apiListener.onApiError(t.getMessage(), apiFlag);
                }
            });
        } else {
            AppRetrofit.getInstance().apiServices.sendNotificationByOrganisation(id, senderType, bean).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
                @Override
                public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() == SUCCESS) {
                            apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                        } else if (response.body().err != null) {
                            if ((response.body().err.errCode == 2 || response.body().err.errCode == 47))
                                clearData(response.body().err.errCode, false);
                            else apiListener.onApiError(response.body().err.msg, apiFlag);
                        } else {
                            apiListener.onApiNoResponse(apiFlag);
                        }
                    } else apiListener.onApiNoResponse(apiFlag);
                }

                @Override
                public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                    if (t instanceof UnknownHostException) {
                        apiListener.onApiError("Please check internet connection.", apiFlag);

                    } else apiListener.onApiError(t.getMessage(), apiFlag);
                }
            });
        }
    }

    public void getPaymentList(ApiListener<PaymentHistoryResponse> apiListener, int apiFlag) {
        int userType = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), USER_TYPE);
        Call<ResponseModel<PaymentHistoryResponse>> call;
        if (userType == AppConstants.JOB_SEEKER)
            call = AppRetrofit.getInstance().apiServices.getUserPayments();
        else call = AppRetrofit.getInstance().apiServices.getOrgPayments();

        call.enqueue(new Callback<ResponseModel<PaymentHistoryResponse>>() {
            @Override
            public void onResponse(Call<ResponseModel<PaymentHistoryResponse>> call, Response<ResponseModel<PaymentHistoryResponse>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<PaymentHistoryResponse>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getMessage(), apiFlag);
            }

        });


    }

    public void getOrganisationFollowers(ApiListener<ResponseModel<RecommendResponseModel>> apiListener, int page, int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.getBusinessFollower(page, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getOrganisationRecommendation(ApiListener<ResponseModel<RecommendResponseModel>> apiListener, int pageNo, int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.getBusinessRecommendation(pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }
    public void getOrgUnRecommendation(ApiListener<ResponseModel<RecommendResponseModel>> apiListener, int pageNo, int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.orgUnRecommendListApi(pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getOrgDashboardRecommendationList(ApiListener<ResponseModel<RecommendResponseModel>> apiListener, int pageNo, String userId, int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.businessDashBoardRecommendList(userId, pageNo, PAGINATION_LIMIT);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void addRecruiter(ApiListener<MsgResponseModel> apiListener, RequestBean requestBody, int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.addRecruiter(requestBody);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void deleteRecruiter(ApiListener<ResponseModel<MsgResponseModel>> apiListener, String recruiterId, int apiFlag) {
        Call<ResponseModel<MsgResponseModel>> call = AppRetrofit.getInstance().apiServices.deleteRecruiter(recruiterId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);

            }
        });
    }

    public void getRecruiterBusinessProfile(final ApiListener<BusinessProfileResponseModel> apiListener, final int apiFlag) {
        Call<ResponseModel<BusinessProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.getRecruiterBusinessProfile();
        call.enqueue(new Callback<ResponseModel<BusinessProfileResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<BusinessProfileResponseModel>> call, Response<ResponseModel<BusinessProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<BusinessProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getUserConnection(final ApiListener<ResponseModel<RecommendResponseModel>> apiListener, int pageNo, final int apiFlag) {
        Call<ResponseModel<RecommendResponseModel>> call = AppRetrofit.getInstance().apiServices.getUserConnection(pageNo);
        call.enqueue(new Callback<ResponseModel<RecommendResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<RecommendResponseModel>> call, Response<ResponseModel<RecommendResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<RecommendResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getUserViewer(final ApiListener<ResponseModel<ViewerResponseModel>> apiListener, int pageNo, final int apiFlag) {
        Call<ResponseModel<ViewerResponseModel>> call = AppRetrofit.getInstance().apiServices.getUserViewer(pageNo);
        call.enqueue(new Callback<ResponseModel<ViewerResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<ViewerResponseModel>> call, Response<ResponseModel<ViewerResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<ViewerResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }

    public void getOrgProfile(final ApiListener<ResponseModel<BusinessProfileResponseModel>> apiListener, String userId, final int apiFlag) {
        Call<ResponseModel<BusinessProfileResponseModel>> call = AppRetrofit.getInstance().apiServices.getOrgProfileDetails(userId);
        call.enqueue(new Callback<ResponseModel<BusinessProfileResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<BusinessProfileResponseModel>> call, Response<ResponseModel<BusinessProfileResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null) {
                        if ((response.body().err.errCode == 2 || response.body().err.errCode == 41 || response.body().err.errCode == 46))
                            clearData(response.body().err.errCode, true);
                        else
                            apiListener.onApiError(response.body().err.msg, apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<BusinessProfileResponseModel>> call, Throwable t) {
                if (t instanceof UnknownHostException) {
                    apiListener.onApiError("Please check internet connection.", apiFlag);

                } else apiListener.onApiError(t.getLocalizedMessage(), apiFlag);
            }
        });
    }


}

