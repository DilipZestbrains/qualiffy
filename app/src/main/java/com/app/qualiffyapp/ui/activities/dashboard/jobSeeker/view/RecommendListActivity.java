package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.jobSeeker.RecommendListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.databinding.ActivityRcommendListBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.RecommendResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter.RecommendListPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.ORG_ID;
import static com.app.qualiffyapp.constants.AppConstants.RECOMMEND;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class RecommendListActivity extends BaseActivity implements SignupView {
    RequestBean requestBean;
    private ActivityRcommendListBinding mBinding;
    private RecommendListPresenter mPresenter;
    private RecommendListAdapter mAdapter;
    private int pageNo = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;
    private DataType dataType;
    private String msg;
    private String userId;

    @Override
    protected void initView() {
        mBinding = (ActivityRcommendListBinding) viewDataBinding;
        mPresenter = new RecommendListPresenter(this);
        getIntentData();
    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {

            try {
                dataType = (DataType) bundle.getSerializable(FROM);
                if (dataType != null)
                    switch (dataType) {
                        case USER_RECOMMENDED:
                            toolbar(DataType.USER_RECOMMENDED.value);
                            msg = getResources().getString(R.string.not_recommended_any_org_yet);
                            fetchUserRecommends(bundle);
                            break;
                        case ORGANISATION_FOLLOWERS:
                            toolbar(DataType.ORGANISATION_FOLLOWERS.value);
                            msg = getResources().getString(R.string.you_have_no_followers);
                            mPresenter.getOrganisationFollower(pageNo, true);
                            break;
                        case ORGANISATION_RECOMMEND:
                            toolbar(DataType.ORGANISATION_RECOMMEND.value);
                            msg = getResources().getString(R.string.not_recommended_any_org_yet);
                            mPresenter.getOrganisationRecommender(pageNo, true);
                            break;
                        case ORGANISATION_UNRECOMMEND:
                            toolbar(DataType.ORGANISATION_UNRECOMMEND.value);
                            mPresenter.getOrgNotRecommender(pageNo, true);
                            break;
                        case ORG_DASHBOARD_RECOMMEND:
                            toolbar(DataType.ORG_DASHBOARD_RECOMMEND.value);
                            userId = bundle.getString(USER_ID);
                            if (userId != null && !TextUtils.isEmpty(userId)) {
                                mPresenter.getOrgDashBoardRecommender(userId, pageNo, true);
                            }
                            break;
                        case JOBSEEKER_VIEWER:
                            toolbar(DataType.JOBSEEKER_VIEWER.value);
                            mPresenter.getUserConnection(pageNo);
                            break;
                        case JOBSEEKER_CONNECTION:
                            mPresenter.getUserConnection(pageNo);
                            toolbar(DataType.JOBSEEKER_CONNECTION.value);
                            break;

                        default:
                            fetchUserRecommends(bundle);
                    }
                else fetchUserRecommends(bundle);
            } catch (Exception e) {
                fetchUserRecommends(bundle);
            }


        }
    }

    private void fetchUserRecommends(Bundle bundle) {
        toolbar(bundle.getInt(RECOMMEND, 0));
        requestBean = new RequestBean();
        requestBean.isRecommend = bundle.getInt(RECOMMEND, 0);
        requestBean.orgid = bundle.getString(ORG_ID, null);
        mPresenter.hitApi(requestBean, pageNo, true);
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.backBtnImg.setOnClickListener(this);

        mBinding.rcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.rcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    if (requestBean != null) {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        if (dataType != null)
                            switch (dataType) {
                                case USER_RECOMMENDED:
                                    msg = getResources().getString(R.string.not_recommended_org_yet);
                                    mPresenter.hitApi(requestBean, pageNo, false);
                                    break;
                                case ORGANISATION_FOLLOWERS:
                                    msg = getResources().getString(R.string.you_have_no_followers);
                                    mPresenter.getOrganisationFollower(pageNo, false);
                                    break;
                                case ORGANISATION_RECOMMEND:
                                    msg = getResources().getString(R.string.no_one_recommended_you_yet);
                                    mPresenter.getOrganisationRecommender(pageNo, false);
                                    break;
                                case ORG_DASHBOARD_RECOMMEND:
                                    if (userId != null && !TextUtils.isEmpty(userId)) {
                                        mPresenter.getOrgDashBoardRecommender(userId, pageNo, true);
                                    }
                                    break;
                                case JOBSEEKER_CONNECTION:
                                    mPresenter.getUserConnection(pageNo);

                                    break;
                                case JOBSEEKER_VIEWER:
                                    mPresenter.getUserConnection(pageNo);
                                    break;
                                case ORGANISATION_UNRECOMMEND:
                                    mPresenter.getOrgNotRecommender(pageNo, false);
                                    break;
                            }
                        else mPresenter.hitApi(requestBean, pageNo, false);

                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    private void toolbar(int isRecommend) {
        if (isRecommend == DataType.USER_RECOMMENDED.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.RECOMMEND_TITLE));
        } else if (isRecommend == DataType.ORGANISATION_FOLLOWERS.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.my_following));
        } else if (isRecommend == DataType.ORGANISATION_RECOMMEND.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.my_recommendees));
        } else if (isRecommend == DataType.ORG_DASHBOARD_RECOMMEND.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.RECOMMEND_TITLE));

        }else if(isRecommend == DataType.ORGANISATION_UNRECOMMEND.value){
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.not_recommendees));
        }
        else if (isRecommend == DataType.JOBSEEKER_VIEWER.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.viewer));
        } else if (isRecommend == DataType.JOBSEEKER_CONNECTION.value) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.connection));
        } else {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.NOT_RECOMMEND_TITLE));
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_rcommend_list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            RecommendResponseModel responseModel = (RecommendResponseModel) response.getRes();
            if (responseModel != null && ((responseModel.usr != null && responseModel.usr.size() > 0) || (responseModel.bdgs != null && responseModel.bdgs.size() > 0))) {
                setVisiblity(true);
                if (mAdapter == null) {
                    if (responseModel.usr != null)
                        mAdapter = new RecommendListAdapter(responseModel.usr, responseModel.bP, this);
                    else
                        mAdapter = new RecommendListAdapter(responseModel.bdgs, responseModel.org_bP, responseModel.usr_bP, this);
                    mBinding.rcv.setAdapter(mAdapter);
                } else {
                    mAdapter.addAll(responseModel.usr);
                }

                if (responseModel.ttl > PAGINATION_LIMIT) {
                    totalPage = responseModel.ttl / PAGINATION_LIMIT;
                    if (responseModel.ttl % PAGINATION_LIMIT != 0)
                        totalPage = totalPage + 1;
                }
                if (pageNo == totalPage) isLastPage = true;
                isLoading = false;

            } else if (mAdapter == null) {
                setVisiblity(false);
            }

        } else
            validationFailed(response.err.msg);

        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);

    }

    void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.tvNoDataFound.setText(msg);
            mBinding.rcv.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        validationFailed(error);
        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, "No Data Found");

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    public enum DataType {
        USER_RECOMMENDED(1), ORGANISATION_FOLLOWERS(2), ORGANISATION_RECOMMEND(3), ORG_DASHBOARD_RECOMMEND(4), JOBSEEKER_CONNECTION(5), JOBSEEKER_VIEWER(6), ORGANISATION_UNRECOMMEND(7);
        int value;

        DataType(int value) {
            this.value = value;
        }
    }

}
