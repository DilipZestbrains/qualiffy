package com.app.qualiffyapp.ui.activities.promote;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityPromotionEditBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_PROMOTION_ADD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_PROMOTION_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_BASE_URL;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class PromotionActivity extends BaseActivity implements SignupView, yesNoCallback {
    private final int CAMERA_REQUEST_CODE = 122;
    private final int GALLARY_REQUEST_CODE = 123;
    private ActivityPromotionEditBinding mBinding;
    private PromotionAddPresenter mPresenter;
    private boolean isAdd = false;
    private String promotionId;
    private RequestBean bean;

    @Override
    protected void initView() {
        mBinding = (ActivityPromotionEditBinding) viewDataBinding;
        mPresenter = new PromotionAddPresenter(this);
        bean = new RequestBean();
        getIntentData();
    }

    private void getIntentData() {
        Bundle bundle;
        if ((bundle = getIntent().getBundleExtra(BUNDLE)) != null && bundle.getString(AppConstants.PROMOTION_EDIT_DATA) != null) {
            PromotionListResponseModel.Prmtn data = new Gson().fromJson(bundle.getString(AppConstants.PROMOTION_EDIT_DATA), PromotionListResponseModel.Prmtn.class);
            String baseUrl = bundle.getString(IMAGE_BASE_URL);
            fillDetails(data, baseUrl);
            setHeader(false);
            isAdd = false;
        } else {
            setHeader(true);
            isAdd = true;
        }
    }

    private void fillDetails(PromotionListResponseModel.Prmtn data, String baseUrl) {

        if (data != null && baseUrl != null) {
            promotionId = data.getId();
            if (data.getPromotionImage() != null) {
                bean.imgUrl = baseUrl + data.getPromotionImage();
                GlideUtil.loadCircularImage(mBinding.ivCompanyLogo, baseUrl + data.getPromotionImage(), R.drawable.profile_placeholder);
            }
            if (data.getDesc() != null) {
                bean.desc = data.getDesc();
                mBinding.etDescription.setText(data.getDesc());
            }
            if (data.getnLoc() != null) {
                bean.n_loc = data.getnLoc();
                int index = data.getnLoc().indexOf(",");
                if (index >= 0)
                    mBinding.etCompanyName.setText(data.getnLoc().substring(0, index));
                mBinding.etLocation.setText(data.getnLoc().substring(index + 1));
            }
        }
    }

    private void setHeader(boolean isAdd) {
        if (isAdd) {
            mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.add_promotion));
        } else {
            mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.edit_promotion));

        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_promotion_edit;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.editUserProfile:
                showCameraDialog();
                break;
            case R.id.tvDone:
                if (mBinding.etCompanyName.getText() != null && mBinding.etDescription.getText() != null && mBinding.etLocation.getText() != null) {
                    bean.n_loc = mBinding.etCompanyName.getText().toString() + ", " + mBinding.etLocation.getText().toString();
                    bean.desc = mBinding.etDescription.getText().toString();
                }
                if (isAdd) {
                    mPresenter.addPromotion(bean);
                } else
                    mPresenter.updatePromotion(promotionId, bean);
                break;
        }
    }

    private void showCameraDialog() {
        CustomDialogs customDialogs = new CustomDialogs(this, this);
        customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                getResources().getString(R.string.add_media_dialog_body),
                getResources().getString(R.string.add_media_dialog_pos_btn),
                getResources().getString(R.string.add_media_dialog_neg_btn),
                getResources().getString(R.string.ADD_MEDIA_TITLE));
        customDialogs.showDialog();
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
        mBinding.editUserProfile.setOnClickListener(this);
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_PROMOTION_UPDATE:
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                    break;

                case API_FLAG_PROMOTION_ADD:
                    Intent intent2 = new Intent();
                    setResult(RESULT_OK, intent2);
                    finish();
                    break;
            }
        } else {
            if (response.err != null) {
                validationFailed(response.err.msg);
            }
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onYesClicked(String from) {
        if (onAskForSomePermission(this, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE))
            requestGallary();
    }

    @Override
    public void onNoClicked(String from) {
        if (onAskForSomePermission(this, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE))
            requestCamera();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        super.onPermissionsDenied(requestCode, list);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        if (requestCode == CAMERA_STORAGE_PERMISSION_REQUEST_CODE) {
            requestCamera();
        } else if (requestCode == GALLERY_STORAGE_PERMISSION_REQUEST_CODE) {
            requestGallary();
        }
    }

    private void requestCamera() {
        SandriosCamera
                .with()
                .setShowPicker(true)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                .setCropperSquare(false)
                .launchCamera(this);

/*
        Intent i = new Intent(this, CameraActivity.class);
        i.putExtra("media_type", 1);
        startActivityForResult(i, REQUEST_CODE_CAMERA_IMG);*/
    }

    private void requestGallary() {
        EasyImage.openGallery(this, GALLARY_REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //to pick image from gallery
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                if (list.get(0) != null) {
                    openEditor(list.get(0).getAbsolutePath());

                }
            }
        });
        //to pick images from camera
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_CAMERA_IMG) {
                bean.imgUrl = data.getStringExtra(MEDIA_PATH);
                //to set image to businessLogo
                if (bean != null)
                    GlideUtil.loadCircularImage(mBinding.ivCompanyLogo, bean.imgUrl, R.drawable.photo_placeholder);
            }
        }



    }

    private void openEditor(String imgPath) {


        try {
            Intent myIntent = new Intent(this,Class.forName(PHOTO_EDITOR_ACTIVITY));
            myIntent.putExtra(MEDIA_PATH,imgPath);
            startActivityForResult(myIntent,REQUEST_CODE_CAMERA_IMG);
            SandriosCamera.cropperSquare=false;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
