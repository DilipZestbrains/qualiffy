package com.app.qualiffyapp.ui.activities.promote;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

public class PromotionPresenter implements ApiListener<ResponseModel<PromotionListResponseModel>> {

    private SignUpInteractor mPromotionInteractor;
    private SignupView mView;

    public PromotionPresenter(SignupView view) {
        this.mView = view;
        this.mPromotionInteractor = new SignUpInteractor();
    }


    @Override
    public void onApiSuccess(ResponseModel<PromotionListResponseModel> response, int apiFlag) {
        mView.showProgressDialog(false);
        mView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        mView.showProgressDialog(false);
        mView.apiError(error, apiFlag);
    }


    public void getPromotions(int flag) {
        mView.showProgressDialog(true);
        mPromotionInteractor.callPromotionList(this, flag);
    }

}
