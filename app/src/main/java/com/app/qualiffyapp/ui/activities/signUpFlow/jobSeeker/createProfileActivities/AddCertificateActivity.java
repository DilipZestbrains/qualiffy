package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SkillCertificateListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.databinding.ActivityAddExperienceBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.ExperiencePresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_4;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;

public class AddCertificateActivity extends BaseActivity implements SignupView, RecyclerItemClick {
    private final String CERTIFICATE = "certificate";
    private ActivityAddExperienceBinding experienceBinding;
    private ExperiencePresenter experiencePresenter;
    private SkillCertificateListAdapter listAdapter;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private boolean isJobSeekerUpdate;


    @Override
    protected void initView() {
        experienceBinding = (ActivityAddExperienceBinding) viewDataBinding;
        experiencePresenter = new ExperiencePresenter(this);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        getIntentData();

    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            profileInputModel.certs = bundle.getStringArrayList(CERTIFICATE);
            listAdapter = new SkillCertificateListAdapter(profileInputModel.certs, this);
            experienceBinding.expRcv.setAdapter(listAdapter);
            isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            toolbar(true);
        } else {

            toolbar(false);

        }

    }

    private void toolbar(boolean isupdate) {
        experienceBinding.toolbar.backBtnImg.setOnClickListener(this);
        experienceBinding.titleTxt.setText(getResources().getString(R.string.add_certificate_title));
        experienceBinding.searchEdt.setHint(getResources().getString(R.string.add_certificate_hint));
        if (isupdate) {
            experienceBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_certificate));
            experienceBinding.expBtn.btn.setText(getResources().getString(R.string.DONE));
            experienceBinding.toolbar.skipTxt.setVisibility(View.GONE);

        } else {
            experienceBinding.expBtn.btn.setText(getResources().getString(R.string.NEXT));
            experienceBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
            experienceBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));


        }
    }

    protected void setListener() {
        experienceBinding.addImg.setOnClickListener(this);
        experienceBinding.toolbar.backBtnImg.setOnClickListener(this);
        experienceBinding.expBtn.btn.setOnClickListener(this);
        experienceBinding.toolbar.skipTxt.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_add_experience;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                profileInputModel.certs = null;
                onBackPressed();
                break;
            case R.id.exp_btn:
                if (profileInputModel.certs == null || profileInputModel.certs.size() == 0)
                    if (isJobSeekerUpdate) {
                        profileInputModel.is_rem = true;
                        showProgressDialog(true);
                        experiencePresenter.hitApi(profileInputModel, API_FLAG_SIGNUP_4);
                    } else
                        validationFailed(getResources().getString(R.string.empty_certificate_alert));
                else {
                    if (profileInputModel.isCertChange) {
                        showProgressDialog(true);
                        experiencePresenter.hitApi(profileInputModel, API_FLAG_SIGNUP_4);
                    } else {
                        if (isJobSeekerUpdate)
                            finish();
                        else
                            startActivity(new Intent(this, UserPhotoActivity.class));
                    }

                }

                break;
            case R.id.add_img:
                if (!TextUtils.isEmpty(experienceBinding.searchEdt.getText().toString()))
                    addExperience();
                break;
            case R.id.skip_txt:
                startActivity(new Intent(this, UserPhotoActivity.class));
                break;
        }

    }

    private void addExperience() {
        if (profileInputModel.certs == null) {
            profileInputModel.isCertChange = true;
            profileInputModel.certs = new ArrayList<>();
            profileInputModel.certs.add(experienceBinding.searchEdt.getText().toString());
            listAdapter = new SkillCertificateListAdapter(profileInputModel.certs, this);
            experienceBinding.expRcv.setAdapter(listAdapter);
        } else if (profileInputModel.certs.size() < 10) {
            profileInputModel.isCertChange = true;
            profileInputModel.certs.add(experienceBinding.searchEdt.getText().toString());
            experienceBinding.expRcv.getAdapter().notifyDataSetChanged();
        } else
            validationFailed(getResources().getString(R.string.certificate_limit_alert));


        experienceBinding.searchEdt.getText().clear();
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        profileInputModel.is_rem = false;

        if (response.getStatus() == 1) {
            MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
            profileInputModel.isCertChange = false;
            if (isJobSeekerUpdate) {
                Intent returnIntent = new Intent();
                returnIntent.putStringArrayListExtra(CERTIFICATE, new ArrayList<>(profileInputModel.certs));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else
                startActivity(new Intent(this, UserPhotoActivity.class));

        } else {
            Utility.showToast(this, response.err.msg);
        }

        showProgressDialog(false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);

        profileInputModel.is_rem = false;


    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onClick(int flag, int pos) {
        switch (flag) {
            case DELETE_FLAG:
                profileInputModel.isCertChange = true;
                profileInputModel.certs.remove(pos);
                break;
            case EDIT_FLAG:
                profileInputModel.isCertChange = true;
                experienceBinding.searchEdt.setText(profileInputModel.certs.get(pos));
                profileInputModel.certs.remove(pos);
                break;
        }
        experienceBinding.expRcv.getAdapter().notifyDataSetChanged();

    }
}
