package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IUpdatableDetail;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_PROFILE;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class UpdateBusinessDetailsPresenter {
    private IUpdatableDetail updatableDetail;
    private SignUpInteractor interactor;
    private ApiListener<MsgResponseModel> msgResponseModelApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            updatableDetail.onUpdate(response.msg, SUCCESS);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            updatableDetail.onUpdate(error, FAILURE);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            updatableDetail.onUpdate(null, -12);
        }
    };

    public UpdateBusinessDetailsPresenter(IUpdatableDetail updatableDetail) {
        this.updatableDetail = updatableDetail;
        interactor = new SignUpInteractor();
    }

    //before calling this method make sure to validate
    public void updateDetails(RequestBean requestBean) {
        interactor.businessProfileEdit(msgResponseModelApiListener, requestBean, API_FLAG_BUSINESS_PROFILE);
    }

}
