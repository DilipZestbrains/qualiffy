package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.MyPaymentAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityPaymentHistoryBinding;
import com.app.qualiffyapp.models.payment.PaymentHistoryResponse;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.PaymentHistoryInterface;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.PaymentHistoryPresenter;

import java.util.ArrayList;

public class PaymentHistoryActivity extends BaseActivity implements PaymentHistoryInterface {
    private ActivityPaymentHistoryBinding mBinding;
    private PaymentHistoryPresenter mPresenter;
    private MyPaymentAdapter mAdapter;

    @Override
    protected void initView() {
        mBinding = (ActivityPaymentHistoryBinding) viewDataBinding;
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.my_payments));
        mBinding.swipeToRefresh.setRefreshing(true);
        mPresenter = new PaymentHistoryPresenter(this);
        mAdapter = new MyPaymentAdapter(new ArrayList<>());
        mBinding.rvPayment.setAdapter(mAdapter);
        fetchData();
    }

    private void fetchData() {
        mPresenter.getPayments();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_payment_history;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_btn_img)
            onBackPressed();
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
        mBinding.swipeToRefresh.setOnRefreshListener(this::fetchData);
    }

    @Override
    public void onPaymentFetch(PaymentHistoryResponse res, String msg) {
        mBinding.swipeToRefresh.setRefreshing(false);
        if (res != null) {
            if (res.pmts != null && res.pmts.size() > 0) {
                mBinding.noPaymentYet.setVisibility(View.GONE);
                mAdapter.refreshList(res.pmts);
            } else {
                mBinding.noPaymentYet.setVisibility(View.VISIBLE);
            }
        }
    }


}
