package com.app.qualiffyapp.ui.activities.signUpFlow;

import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_LOGOUT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_LOGOUT;


public class PreActivityPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {
    private SignUpInteractor interactor;
    private SignupView view;


    public PreActivityPresenter(SignupView view) {
        interactor = new SignUpInteractor();
        this.view = view;
    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);

    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApi(int apiFlag) {
        switch (apiFlag) {

            case API_FLAG_BUSINESS_LOGOUT:
                interactor.logoutOrganisation(this, Utility.getStringSharedPreference(view.getViewContext(),
                        AppConstants.PREFS_ACCESS_TOKEN), API_FLAG_BUSINESS_LOGOUT);
                break;
            case API_FLAG_JS_LOGOUT:
                interactor.jobsSeekerlogout(this, API_FLAG_JS_LOGOUT);
                break;
        }

    }
}
