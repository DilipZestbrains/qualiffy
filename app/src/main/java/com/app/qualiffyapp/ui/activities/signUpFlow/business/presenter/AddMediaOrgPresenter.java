package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.io.File;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE_ALL_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.FALSE;

public class AddMediaOrgPresenter implements ApiListener<ResponseModel<AddMediaResponseModel>> {

    private SignupView view;
    ApiListener<ResponseModel<MsgResponseModel>> msgApiListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {

            if (response != null)
                view.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            view.apiError(error, apiFlag);
        }
    };
    private SignUpInteractor mSignUpInteractor;

    public AddMediaOrgPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<AddMediaResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void hitApi(File file, int apifLAG) {
        long time = System.currentTimeMillis();
        RequestBean requestBean = new RequestBean();
        requestBean.isProfile = FALSE;
        requestBean.uid = String.valueOf(time);
        mSignUpInteractor.addMediaOrgApi(this, file, requestBean, apifLAG);
    }

    public void deleteMediaApi(String imgId,int type) {
        mSignUpInteractor.deleteOrgMediaApi(this, imgId,type, API_FLAG_DELETE_MEDIA);
    }

    public void deleteAllMediaApi() {
        mSignUpInteractor.deleteAllMediaApi(msgApiListener, API_FLAG_BUSINESS_DELETE_ALL_MEDIA);
    }

}
