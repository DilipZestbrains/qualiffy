package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_PROFILE_EDIT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_5;

public class JobseekerAddressPresenter implements ApiListener<ResponseModel<VerifyResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public JobseekerAddressPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<VerifyResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApi(RequestBean bean, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_BUSINESS_PROFILE_EDIT:
//                mSignUpInteractor.businessProfileEdit(this, bean, apiFlag);

                break;

            case API_FLAG_SIGNUP_5:
                mSignUpInteractor.signUp5Api(this, bean, apiFlag);
                break;
        }

    }
}
