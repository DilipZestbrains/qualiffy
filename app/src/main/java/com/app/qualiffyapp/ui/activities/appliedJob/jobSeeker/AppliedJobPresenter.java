package com.app.qualiffyapp.ui.activities.appliedJob.jobSeeker;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter.ApplyJobPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLIED_JOB;

public class AppliedJobPresenter extends ApplyJobPresenter {
    private SignUpInteractor interactor;
    private SignupView view;
    ApiListener<ResponseModel<AppliedJobResponseModel>> appliedJobListener = new ApiListener<ResponseModel<AppliedJobResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<AppliedJobResponseModel> response, int apiFlag) {
            if (response != null)
                view.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            view.apiError(error, apiFlag);
        }
    };

    public AppliedJobPresenter(SignupView view) {
        super(view);
        interactor = new SignUpInteractor();
        this.view = view;
    }

    public void getAppliedJoblist(int pageNo, String isShortList) {
        interactor.jobSekekrAppliedJobApi(appliedJobListener, pageNo, isShortList, API_FLAG_JS_APPLIED_JOB);
    }
}
