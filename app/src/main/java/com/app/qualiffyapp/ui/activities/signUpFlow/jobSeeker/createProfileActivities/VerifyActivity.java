package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityVerifyBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.verify.VerifyPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_LOGIN;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_VERIFY;
import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;

public class VerifyActivity extends BaseActivity implements SignupView {
    private ActivityVerifyBinding verifyBinding;
    private VerifyPresenter mVerifyPresenter;
    private String userId;
    private String userPhnNum;
    private CountDownTimer timer;


    @Override
    protected void initView() {
        verifyBinding = (ActivityVerifyBinding) viewDataBinding;
        mVerifyPresenter = new VerifyPresenter(this);
        userId = getIntent().getStringExtra("user_id");
        userPhnNum = getIntent().getStringExtra("user_phn_num");
        toolbar();
        setTimer();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_verify;
    }


    private void toolbar() {
        verifyBinding.toolbar.backBtnImg.setOnClickListener(this);
        verifyBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.VERIFY_TITLE));
        verifyBinding.verifyBtn.btn.setText(getResources().getString(R.string.CONTINUE));
        verifyBinding.verifyBtn.btn.setOnClickListener(this);

    }

    protected void setListener() {
        verifyBinding.resendOtpTxt.setOnClickListener(this);
    }

    private void setTimer() {
        verifyBinding.resendOtpTxt.setEnabled(false);
        verifyBinding.verifyBtn.btn.setEnabled(true);

        timer = new CountDownTimer(120000, 1000) {

            public void onTick(long millisUntilFinished) {
                String time = String.valueOf(millisUntilFinished / 1000);

                if ((millisUntilFinished / 1000) >= 60)
                    time = " 01:" + ((millisUntilFinished / 1000) - 60);

                if (((millisUntilFinished / 1000) - 60) < 10)
                    time = " 01:0" + ((millisUntilFinished / 1000) - 60);


                if ((millisUntilFinished / 1000) < 60)
                    time = " 00:" + ((millisUntilFinished / 1000));

                if ((millisUntilFinished / 1000) < 10)
                    time = " 00:0" + millisUntilFinished / 1000;

                verifyBinding.timerTxt.setText(" " +getResources().getString(R.string.otp_verify) + time);
            }

            @Override
            public void onFinish() {
                verifyBinding.timerTxt.setVisibility(View.GONE);
                verifyBinding.resendOtpTxt.setEnabled(true);
                verifyBinding.verifyBtn.btn.setEnabled(false);
            }


        }.start();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.resend_otp_txt:
                verifyBinding.otpEdt.getText().clear();
                verifyBinding.timerTxt.setVisibility(View.VISIBLE);
                showProgressDialog(true);
                mVerifyPresenter.resendOtpApi(userPhnNum);
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.verify_btn:
                if (!TextUtils.isEmpty(verifyBinding.otpEdt.getText().toString()))
                    mVerifyPresenter.validate(userId, verifyBinding.otpEdt.getText().toString());
                break;
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == 1) {
            VerifyResponseModel verifyResponseModel = (VerifyResponseModel) response.getRes();
            switch (apiFlag) {
                case API_FLAG_VERIFY:
                    Utility.putStringValueInSharedPreference(this, LOGIN_ACCESS_TOKEN, verifyResponseModel.acsTkn);
                    Utility.saveUserId(((VerifyResponseModel) response.getRes())._id, getViewContext());
                    switch (verifyResponseModel.code) {
                        case 1:
                            openActivity(SelectIndustryActivity.class, null);
                            break;
                        case 2:
                            openActivity(LanguageActivity.class, null);
                            break;
                        case 3:
                            openActivity(AddExperienceActivity.class, null);
                            break;
                        case 4:
                            openActivity(AddCertificateActivity.class, null);
                            break;
                        case 5:
                            openActivity(AddMediaActivity.class, null);
                            break;
                        case 6:
                            openActivity(UserVideoActivity.class, null);
                            break;
                        case 7:
                            openActivity(LocationActivity.class, null);
                            break;
                        case 8:
                            saveName(verifyResponseModel);
                            Intent intent = new Intent(this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        case 9:
                            openActivity(UserPhotoActivity.class, null);
                            break;
                    }

                    finish();
                    break;

                case API_FLAG_LOGIN:
                    userId = verifyResponseModel._id;
                    Utility.showToast(this, ApplicationController.getApplicationInstance().getString(R.string.verification_code_sent));
                    setTimer();
                    break;

            }
        } else {
            Utility.showToast(this, response.err.msg);
            verifyBinding.otpEdt.getText().clear();
          /*  timer.cancel();
            verifyBinding.timerTxt.setText(getResources().getString(R.string.otp_verify) + " 00:00");
            verifyBinding.resendOtpTxt.setEnabled(true);
            verifyBinding.verifyBtn.btn.setEnabled(false);*/
        }
        showProgressDialog(false);
    }

    private void saveName(VerifyResponseModel verifyResponseModel) {
        Utility.saveProfile(this, verifyResponseModel, false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);

        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
