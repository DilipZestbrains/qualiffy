package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ArrayAdapter;
import androidx.appcompat.app.AlertDialog;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.FragmentCompanyNameBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ICompanyName;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.CompanyNamePresenter;
import com.app.qualiffyapp.utils.Utility;
import java.util.ArrayList;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.COMPANY_STRENGTH;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.IS_SMALL_ORG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class CompanyNameFragment extends BLoginSignUpBaseFragment implements ICompanyName {

    private CompanyNamePresenter mPresenter;
    private FragmentCompanyNameBinding mBinding;
    private String header;
    private int isSmallCompany;

    @Override
    protected void initUi() {
        mBinding = (FragmentCompanyNameBinding) viewDataBinding;
        mBinding.etCompanyStrength.setText("10 - 50");
        mPresenter = new CompanyNamePresenter(this);
        manageAction(getLoginActivity().isUpdate);

    }

    @Override
    protected String getHeader() {
        return header;
    }


    @Override
    protected int getLayoutById() {
        return R.layout.fragment_company_name;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNext:
                if (mBinding.etCompanyName.getText() != null && mBinding.etCompanyStrength.getText() != null)
                    mPresenter.proceed(mBinding.etCompanyName.getText().toString(), mBinding.etCompanyStrength.getText().toString());
                break;
            case R.id.etCompanyStrength:
                if (isSmallCompany == 1)
                    mPresenter.openCompanyStrenthDialog();
                break;
            case R.id.tvDone:
                if (mBinding.etCompanyName.getText() != null)
                    mPresenter.updateCompanyName(mBinding.etCompanyName.getText().toString());
                break;
        }
    }

    @Override
    protected void setListener() {
        mBinding.tvNext.setOnClickListener(this);
        mBinding.etCompanyStrength.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
    }

    @Override
    public void onNext(String companyName, String comStrength) {
        Utility.hideKeyboard(context);
        getLoginActivity().getRequestBean().name = companyName;
        getLoginActivity().getRequestBean().employStrength = comStrength;
        getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.CHOOSE_INDUSTRY, true, true);
    }

    @Override
    public void showCompanyStrengthDialog(ArrayList<String> list) {
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.select_dialog_singlechoice
        );
        arrayAdapter.addAll(list);
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context);
        builderSingle.setTitle(getResources().getString(R.string.company_strength));
        builderSingle.setCancelable(true);
        builderSingle.setAdapter(arrayAdapter, (dialog, which) -> {
            String strName = arrayAdapter.getItem(which);
            mBinding.etCompanyStrength.setText(strName);
            getLoginActivity().getRequestBean().employStrength = strName;
            dialog.dismiss();
        });
        builderSingle.show();
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(String msg, int status) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (status == SUCCESS) {
            Intent intent = new Intent();
            if (mBinding.etCompanyName.getText() != null)
                intent.putExtra(DATA_TO_SEND, mBinding.etCompanyName.getText().toString());
            if (mBinding.etCompanyStrength.getText() != null)
                intent.putExtra(COMPANY_STRENGTH, mBinding.etCompanyStrength.getText().toString());

            getLoginActivity().setResult(Activity.RESULT_OK, intent);
            getLoginActivity().finish();
        }
    }

    @Override
    public void showToast(ToastType type) {
        if (type == ToastType.NAME_EMPTY)
            Utility.showToast(context, getResources().getString(R.string.company_name_can_not_empty));
        else if (type == ToastType.COMPANY_STRENGTH_EMPTY)
            Utility.showToast(context, getResources().getString(R.string.please_select_company_strength));
    }

    private void manageAction(boolean isUpdate) {
        if (isUpdate) {
            mBinding.tvDone.setVisibility(View.VISIBLE);
            mBinding.tvNext.setVisibility(View.GONE);
            String name = getLoginActivity().getIntent().getBundleExtra(BUNDLE).getString(DATA_TO_SEND, "");
            String strength = getLoginActivity().getIntent().getBundleExtra(BUNDLE).getString(COMPANY_STRENGTH, "");
            if (strength.equals("0 - 10") || strength.equals("0-10"))
                isSmallCompany = 0;
            else {
                isSmallCompany = 1;
            }
            setCompanyStrength();
            mBinding.etlCompanyStrength.setVisibility(View.GONE);
            mBinding.etCompanyName.setText(name);
            mBinding.etCompanyStrength.setText(strength);
            mBinding.etCompanyName.requestFocus();
            header = getResources().getString(R.string.edit_company_name);
        } else {
            isSmallCompany = getArguments().getInt(IS_SMALL_ORG, 0);
            setCompanyStrength();
           mBinding.tvNext.setVisibility(View.VISIBLE);
            mBinding.tvDone.setVisibility(View.GONE);
            header = getResources().getString(R.string.company_name);

        }
    }


 private void setCompanyStrength()
    {
        if (isSmallCompany == 0) {
            mBinding.etCompanyStrength.setText("0 - 10");
            mBinding.etlCompanyStrength.setVisibility(View.INVISIBLE);
        }
        else {
            mBinding.etCompanyStrength.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0);
            mBinding.etlCompanyStrength.setVisibility(View.VISIBLE);

        }
    }

}
