package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.jobSeeker.OrgnaizationJobListAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationJobItemClick;
import com.app.qualiffyapp.databinding.ActivityOrganizationJobsBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter.OrganizationJobListPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.videoCompressUtils.IVideoCompressCallback;
import com.app.qualiffyapp.utils.videoCompressUtils.VideoUtils;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.List;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_JOB_LIST;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_VIDEO_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.ORG_BIO;
import static com.app.qualiffyapp.constants.AppConstants.ORG_ID;
import static com.app.qualiffyapp.constants.AppConstants.ORG_IMG;
import static com.app.qualiffyapp.constants.AppConstants.ORG_NAME;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CAMERA_PERMISSION_REQUEST_CODE;

public class OrganizationJobListActivity extends BaseFragment implements SignupView, IOrganizationJobItemClick, IVideoCompressCallback {
    private ActivityOrganizationJobsBinding mBinding;
    private GridLayoutManager gridLayoutManager;
    private OrgnaizationJobListAdapter mAdapter;
    private OrganizationJobListPresenter joblistPresenter;
    private String orgId;
    private String orgImg;
    private String orgName;
    private VideoUtils videoUtils;
    private String videoPath;
    private String jobId;
    private int pageNo = 1;
    private AppFirebaseDatabase firebaseDatabase;


    @Override
    protected void initUi() {
        mBinding = (ActivityOrganizationJobsBinding) viewDataBinding;
        joblistPresenter = new OrganizationJobListPresenter(this);
        initImgGrid();
        getIntentData();
        initFirebase();
    }

    private void initFirebase() {
        firebaseDatabase = AppFirebaseDatabase.getInstance();
    }


    private void initImgGrid() {
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mBinding.rvJob.setLayoutManager(gridLayoutManager);
    }


    private void getIntentData() {
        Bundle bundle = getArguments();
        orgId = bundle.getString(ORG_ID, "");
        orgName = bundle.getString(ORG_NAME, "");
        mBinding.tvCompanyName.setText(orgName);
        mBinding.tvComIntro.setText(bundle.getString(ORG_BIO, ""));
        orgImg = bundle.getString(ORG_IMG, "");
        hitApi(API_FLAG_JS_JOB_LIST);

    }

    private void hitApi(int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_JS_JOB_LIST:
                if (orgId != null) {
                    showProgress(true);
                    joblistPresenter.getJobList(orgId, pageNo);
                }
                break;
        }

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_organization_jobs;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_JS_JOB_LIST:
                    OrgJobListResponseModel jobListResponseModel = (OrgJobListResponseModel) response.getRes();
                    if (jobListResponseModel != null) {
                        if (jobListResponseModel.job != null && jobListResponseModel.job.size() > 0) {
                            mAdapter = new OrgnaizationJobListAdapter(getContext(), jobListResponseModel.job, this);
                            mBinding.rvJob.setAdapter(mAdapter);
                        }
                        mBinding.tvJobCount.setText(jobListResponseModel.ttl + " " + Utility.singularPluralText(getResources().getString(R.string.job), jobListResponseModel.ttl) + " " + getResources().getString(R.string.job_available));

                    }
                    break;
                case API_FLAG_JS_APPLY_JOB:
                    setChatThread();
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel.msg);
                    break;

            }
        } else validationFailed(response.err.msg);
        showProgress(false);

    }

    private void setChatThread() {
        UserConnection user = Utility.getUserConnection(getContext());
        UserConnection userConnection = new UserConnection();
        userConnection.setUid(orgId);
        userConnection.setName(orgName);
        userConnection.setBlocked(BlockStatus.NOT_BLOCKED.value);
        userConnection.setImg(orgImg);
        userConnection.setType(FirebaseUserType.BUSINESS.s);
        userConnection.setTimestamp(user.getTimestamp());
        firebaseDatabase.addFriend(user, userConnection);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgress(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case VIDEO_CAMERA_PERMISSION_REQUEST_CODE:
                SandriosCamera
                        .with()
                        .setMediaAction(CameraConfiguration.MEDIA_ACTION_VIDEO)
                        .launchCamera(getActivity());
//                startActivityForResult(new Intent(context, CameraActivity.class), REQUEST_CODE_CAMERA_VIDEO);
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Utility.showToast(context, "Returned from setting");
                break;

            case REQUEST_CODE_CAMERA_IMG:
                if (resultCode == Activity.RESULT_OK) {
                    videoPath = data.getStringExtra(MEDIA_PATH);
                    videoUtils = new VideoUtils(getContext(), this);
                }
                break;


        }
    }

    @Override
    public void showVideoCompressProgress(boolean isShown) {
        if (isShown)
            showProgress(true);
        else
            showProgress(false);
    }

    @Override
    public void getVideoFile(Bitmap bm, File videoFile) {
        if (jobId != null && videoFile != null) {
            joblistPresenter.ApplyJobApi(videoFile, null, jobId);
        }
    }

    @Override
    public void isCompressSupport(boolean isCompressSupported) {
        if (videoUtils != null) {
            showProgressDialog(true);
            if (isCompressSupported)
                videoUtils.getCompressedVideo(videoPath);
            else videoUtils.getVideoThumbnail(videoPath);

        }

    }

    @Override
    public void onItemClick(int pos, String jobId, String name,String des) {
        this.jobId = jobId;
        onAskForSomePermission(context, CAMERA_VIDEO_PERMISION, VIDEO_CAMERA_PERMISSION_REQUEST_CODE);
    }
}
