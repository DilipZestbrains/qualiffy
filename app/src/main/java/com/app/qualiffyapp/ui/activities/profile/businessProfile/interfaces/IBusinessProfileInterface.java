package com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces;

import android.os.Bundle;

import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

import java.util.LinkedList;

public interface IBusinessProfileInterface extends BaseInterface {

    void openActivityForUpdate(Class<?> activity, Bundle bundle, int requestCode);
    void openActivity(String baseUrl, LinkedList<String> imgs, int pos);


    void setTextViewValue(TextViewType type, Object data);

    void setAdapter(AdapterType type, LinkedList<String> list);

    void onFetchProfile(String msg, BusinessProfileResponseModel res);

    void showProgressDialog();

    void onDelete(String msg, int status, int apiFlag);

    void profileVisibility(String msg, int status);

    void onFetchPortfolio(String msg, WebResponseModel model, int apiFlag);

    void setNotificaitonToggle(String toggleStatus);

    enum TextViewType {
        DESCRIPTION, COMPANY_NAME, REGISTRATION_NUMBER, ADDRESS, CITY, LOGO
    }
    enum AdapterType {
        IMAGES, ViewMatches
    }

}

