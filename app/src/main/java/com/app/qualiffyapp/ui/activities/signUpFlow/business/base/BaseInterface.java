package com.app.qualiffyapp.ui.activities.signUpFlow.business.base;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;

public interface BaseInterface {
    void showToast(ToastType toastType);
}
