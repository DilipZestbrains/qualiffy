package com.app.qualiffyapp.ui.activities.businessProfileUserView;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityViewBusinessProfileBinding;

public class BusinessProfileUserViewActivity extends BaseActivity {

    private ActivityViewBusinessProfileBinding mViewBusinessProfileBinding;

    @Override
    protected void initView() {
        mViewBusinessProfileBinding = (ActivityViewBusinessProfileBinding) viewDataBinding;
        setImagesAdapter();
    }

    private void setImagesAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_view_business_profile;
    }

    @Override
    public void onClick(View v) {

    }
}
