package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.CommonView;
import com.app.qualiffyapp.databinding.ActivityLanguageBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobSeekerInfoPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerInfo;
import com.app.qualiffyapp.utils.Utility;
import com.google.android.material.chip.Chip;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_LANGS;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class LanguageActivity extends BaseActivity implements CommonView, AdapterView.OnItemSelectedListener, IJobSeekerInfo {
    private ActivityLanguageBinding languageBinding;
    private List<String> langList;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private JobSeekerInfoPresenter presenter;
    private boolean isUpdate;


    @Override
    protected void initView() {
        languageBinding = (ActivityLanguageBinding) viewDataBinding;
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        languageBinding.searchSpnr.setOnItemSelectedListener(this);
        presenter = new JobSeekerInfoPresenter(this);
        getIntentData();
        loadLang();

    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            profileInputModel.lngs = bundle.getStringArrayList(JS_LANGS);
            isUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            if (profileInputModel.lngs != null && profileInputModel.lngs.size() > 0) {
                for (String skill : profileInputModel.lngs)
                    addChip(skill);
            }
            toolbar(true);
        } else {
            toolbar(false);

        }
    }


    private void toolbar(boolean isUpdate) {
        languageBinding.toolbar.backBtnImg.setOnClickListener(this);
        languageBinding.titleTxt.setText(getResources().getString(R.string.language));

        if (isUpdate) {
            languageBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_language));
            languageBinding.indusBtn.btn.setText(getResources().getString(R.string.DONE));
            languageBinding.toolbar.skipTxt.setVisibility(View.GONE);
        } else {
            languageBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
            languageBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
            languageBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
        }
        languageBinding.indusBtn.btn.setOnClickListener(this);
        languageBinding.toolbar.skipTxt.setOnClickListener(this);
    }


    private void loadLang() {
        showProgressDialog(true);
        langList = new ArrayList<>();
        try {
            String langJson = Utility.loadJSONFromAsset(getAssets().open("language.json"));
            JSONObject langobj = new JSONObject(langJson);
            Iterator<String> keys = langobj.keys();
            while (keys.hasNext()) {
                langList.add(langobj.getString(keys.next()));
            }
            langList.add(0, "Choose Language");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, langList);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            languageBinding.searchSpnr.setAdapter(dataAdapter);
        } catch (Exception e) {

        }
        showProgressDialog(false);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_language;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                profileInputModel.lngs = null;
                onBackPressed();
                break;
            case R.id.indus_btn:
                if (profileInputModel.lngs == null || profileInputModel.lngs.size() == 0)
                    Utility.showToast(this, getResources().getString(R.string.lang_alert));
                else {
                    if (isUpdate) {
                        RequestBean bean = new RequestBean();
                        bean.lngs = profileInputModel.lngs;
                        presenter.updateProfile(bean);
                    } else
                        startActivity(new Intent(this, SkillActivity.class));
                }

                break;
            case R.id.skip_txt:
                startActivity(new Intent(this, SkillActivity.class));
                break;
        }


    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            if (profileInputModel.lngs == null) {
                profileInputModel.isSignup2Change = true;
                profileInputModel.lngs = new ArrayList<>();
                profileInputModel.lngs.add(langList.get(position));
                addChip(langList.get(position));
            } else if (profileInputModel.lngs.size() == 10) {
                Utility.showToast(this, ApplicationController.getApplicationInstance().getString(R.string.lang_limit_alert));
            } else {
                if (!profileInputModel.lngs.contains(langList.get(position))) {
                    profileInputModel.lngs.add(langList.get(position));
                    addChip(langList.get(position));
                    profileInputModel.isSignup2Change = true;
                }
            }
        }
    }

    private void addChip(String roleseek) {

        Chip chip = new Chip(this);
        chip.setText(Utility.getFirstLetterCaps(roleseek));
        chip.setCloseIconEnabled(true);
        chip.setCloseIconResource(R.drawable.ic_clear_black);
        chip.setChipBackgroundColorResource(R.color.light_grey);
        chip.setTextAppearanceResource(R.style.ChipTextStyle);
        chip.setOnCloseIconClickListener(view -> {
            if (languageBinding.searchSpnr.getSelectedItem().toString().equals(chip.getText()))
                languageBinding.searchSpnr.setSelection(0);

            profileInputModel.lngs.remove(chip.getText());
            languageBinding.chipgroup.removeView(view);
            profileInputModel.isSignup2Change = true;

        });
        //chip.setElevation(15);
        languageBinding.chipgroup.addView(chip);
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(int status, String msg) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            Intent returnIntent = new Intent();
            returnIntent.putStringArrayListExtra(JS_LANGS, profileInputModel.lngs);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.JOB_SEEKER_DESCRIPTION_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.please_enter_job_seeker_intro));
        }
    }

}
