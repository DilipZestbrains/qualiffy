package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SuggestedListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.ActivitySelectIndustryBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.JobInfo;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.CustomJobPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.UserRolePresenter;
import com.app.qualiffyapp.utils.RecyclerItemClickListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_ROLE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_ADD_JOB_TITLE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUS_ID;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JS_ROLESEEKING;

public class UserRoleActivity extends BaseActivity implements TextWatcher, SignupView, RecyclerItemClickListener.OnItemClickListener, TextView.OnEditorActionListener {
    private ActivitySelectIndustryBinding selectIndustryBinding;
    private List<SuggestedListResponseModel> seggestList;
    private UserRolePresenter userRolePresenter;
    private CustomJobPresenter jobPresenter;
    private SuggestedListAdapter suggestedListAdapter;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private boolean isJobseekerUpdate;


    @Override
    protected void initView() {
        selectIndustryBinding = (ActivitySelectIndustryBinding) viewDataBinding;
        userRolePresenter = new UserRolePresenter(this);
        jobPresenter = new CustomJobPresenter(this);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        selectIndustryBinding.tvNote.setVisibility(View.GONE);
        selectIndustryBinding.addJobs.setVisibility(View.GONE);
        getIntenetData();
        toolbar();
        setListeners();
    }

    private void getIntenetData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle.getString(JS_INDUS_ID) != null)
            profileInputModel.jobC_id = bundle.getString(JS_INDUS_ID);

        isJobseekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
        if (bundle.getString(JS_ROLE) != null) {
            JobInfo jobInfo = new Gson().fromJson(bundle.getString(JS_ROLE), JobInfo.class);
            if (jobInfo != null)
                addChip(jobInfo.getName(), jobInfo.get_id());
        }


    }

    private void setListeners() {
        selectIndustryBinding.searchEdt.addTextChangedListener(this);
        selectIndustryBinding.suggestedRcv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, this));
        selectIndustryBinding.addJobs.setOnClickListener(this);
        selectIndustryBinding.searchEdt.setOnEditorActionListener(this);

    }

    private void toolbar() {
        selectIndustryBinding.toolbar.backBtnImg.setOnClickListener(this);
         if (isJobseekerUpdate) {
             selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_role));
        } else {
             selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
         }
        selectIndustryBinding.titleTxt.setText(getResources().getString(R.string.user_role));

        selectIndustryBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
        selectIndustryBinding.indusBtn.btn.setOnClickListener(this);
        selectIndustryBinding.toolbar.skipTxt.setOnClickListener(this);
        selectIndustryBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_select_industry;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                profileInputModel.myrole = null;
                onBackPressed();
                break;
            case R.id.add_jobs:
                if (!TextUtils.isEmpty(selectIndustryBinding.searchEdt.getText().toString())) {
                    selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
                    jobPresenter.hitApiAddJobs(selectIndustryBinding.searchEdt.getText().toString().toUpperCase(), profileInputModel.jobC_id, API_FLAG_JS_ADD_JOB_TITLE);
                }
                break;
            case R.id.indus_btn:
                if (selectIndustryBinding.chipgroup.getChildCount() == 0) {
                    validationFailed(ApplicationController.getApplicationInstance().getString(R.string.role_alert));
                    return;
                } else if (isJobseekerUpdate && profileInputModel.jobC_id != null) {
                    Bundle bundle = new Bundle();
                    bundle.putString(JS_INDUS_ID, profileInputModel.jobC_id);
                    bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                    openActivityForResult(RoleSeekingActivity.class, bundle, REQUEST_CODE_JS_ROLESEEKING);
                } else
                    startActivity(new Intent(this, RoleSeekingActivity.class));

                break;
            case R.id.skip_txt:
                if (isJobseekerUpdate && profileInputModel.jobC_id != null) {
                    profileInputModel.myrole = null;
                    Bundle bundle = new Bundle();
                    bundle.putString(JS_INDUS_ID, profileInputModel.jobC_id);
                    bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                    openActivityForResult(RoleSeekingActivity.class, bundle, REQUEST_CODE_JS_ROLESEEKING);
                } else
                    startActivity(new Intent(this, RoleSeekingActivity.class));
                break;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_JS_ROLESEEKING) {
            if (resultCode == RESULT_OK) {
                if (profileInputModel.myrole != null) {
                    JobInfo role = new JobInfo();
                    role.set_id(profileInputModel.myrole);
                    role.setName(((Chip) selectIndustryBinding.chipgroup.getChildAt(0)).getText().toString());
                    data.putExtra(JS_ROLE, new Gson().toJson(role));
                }
                setResult(RESULT_OK, data);
                finish();
            }
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);
        if (s.length() == 2 || s.length() == 5 || s.length() == 7 || s.length() == 11 || s.length() > 15) {
            selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
            userRolePresenter.hitApi(s.toString(), profileInputModel.jobC_id);
        } else {
            if (seggestList != null) {
                selectIndustryBinding.progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (apiFlag == API_FLAG_GET_ROLE) {
            if (response.getStatus() == 1) {
                IndustryResponseModel getIndustryResponseModel = (IndustryResponseModel) response.getRes();
                seggestList = getIndustryResponseModel.jobT;
                suggestedListAdapter = new SuggestedListAdapter(seggestList);
                selectIndustryBinding.suggestedRcv.setAdapter(suggestedListAdapter);

                if (getIndustryResponseModel.ttl == 0) {
                    selectIndustryBinding.addJobs.setVisibility(View.VISIBLE);
                    selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.VISIBLE);
                    selectIndustryBinding.noDataTxt.noDataFound.setText(getResources().getString(R.string.no_data_found_press_add_btn));
                } else
                    selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);

            } else {
                Utility.showToast(this, response.err.msg);
            }
        } else if (apiFlag == API_FLAG_JS_ADD_JOB_TITLE) {
            if (response.getStatus() == 1) {
                AddCustomeJobModel model = (AddCustomeJobModel) response.getRes();
                SuggestedListResponseModel modell = new SuggestedListResponseModel();
                modell.id = model.job._id;
                modell.name = model.job.name;
                if (seggestList != null) {

                } else {
                    seggestList = new ArrayList<>();
                }
                seggestList.add(modell);
                suggestedListAdapter = new SuggestedListAdapter(seggestList);
                selectIndustryBinding.suggestedRcv.setAdapter(suggestedListAdapter);
                selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);
                selectIndustryBinding.searchEdt.setText("");
                selectIndustryBinding.addJobs.setVisibility(View.GONE);
            } else {
                Utility.showToast(this, response.err.msg);
            }
        }
        selectIndustryBinding.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        selectIndustryBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
    }


    @Override
    public void onItemClick(View view, int position) {
        Utility.hideKeyboard(this);
        addChip(Utility.getFirstLetterCaps(seggestList.get(position).name), seggestList.get(position).id);
    }

    private void addChip(String roleStr, String id) {
        if (profileInputModel.myrole != null && profileInputModel.myrole.equals(id))
            profileInputModel.isSignup1Change = false;
        else
            profileInputModel.isSignup1Change = true;


        profileInputModel.myrole = id;

        selectIndustryBinding.chipgroup.removeAllViews();

        CustomChip chip = new CustomChip(this, roleStr);
        chip.setOnCloseIconClickListener(view -> {
            selectIndustryBinding.chipgroup.removeView(view);
            profileInputModel.myrole = null;
        });
        //chip.setElevation(15);
        selectIndustryBinding.chipgroup.addView(chip);

        selectIndustryBinding.searchEdt.getText().clear();
//        selectIndustryBinding.suggestedRcv.removeAllViewsInLayout();
    }


    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            if(!TextUtils.isEmpty(selectIndustryBinding.searchEdt.getText().toString()))
                selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
            userRolePresenter.hitApi(selectIndustryBinding.searchEdt.toString(), profileInputModel.jobC_id);
            Utility.hideKeyboard(this);
            return true;
        }
        return false;    }
}
