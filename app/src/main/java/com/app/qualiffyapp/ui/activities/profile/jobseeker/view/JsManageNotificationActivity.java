package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityJsManageNotificationBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.profile.jobseeker.User;
import com.app.qualiffyapp.ui.activities.profile.IManageNotification;
import com.app.qualiffyapp.ui.activities.profile.ManageNotificationPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.google.gson.Gson;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MANAGE_NOTIFICATION;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class JsManageNotificationActivity extends BaseActivity implements IManageNotification {
    private ManageNotificationPresenter mPresenter;
    private ActivityJsManageNotificationBinding mBinding;
    private User.NotifBean notifyBean;

    @Override
    protected void initView() {
        mBinding = (ActivityJsManageNotificationBinding) viewDataBinding;
        mPresenter = new ManageNotificationPresenter(this);
        getIntentData();
        toolbar();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            String data = bundle.getString(FROM);
            if (data != null) {
                String notificationData = bundle.getString(DATA_TO_SEND, null);
                if (notificationData != null) {
                    notifyBean = new Gson().fromJson(notificationData, User.NotifBean.class);
                    manageNotification(notifyBean);
                }

            }
        }
    }

    private void manageNotification(User.NotifBean notifyBean) {
        mBinding.toggleExpire.setChecked(notifyBean.mExp);
        mBinding.toggleFollowOrg.setChecked(notifyBean.orgF);
        mBinding.toggleViewOrg.setChecked(notifyBean.orgV);
        mBinding.toggleNotification.setChecked(notifyBean.chat);
        mBinding.callNotification.setChecked(notifyBean.calls);
        mBinding.shortListSwitch.setChecked(notifyBean.shortl);
    }


    private void toolbar() {
        mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.MANAGE_NOIFICATION_TITLE));
        mBinding.btn.btn.setText(getResources().getString(R.string.done));

    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
        mBinding.btn.btn.setOnClickListener(this);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_js_manage_notification;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.btn:
                RequestBean bean = new RequestBean();
                RequestBean.notify notify = new RequestBean.notify();
                notify.org_f = mBinding.toggleFollowOrg.isChecked();
                notify.org_v = mBinding.toggleViewOrg.isChecked();
                notify.m_exp = mBinding.toggleExpire.isChecked();
                notify.calls = mBinding.callNotification.isChecked();
                notify.chat = mBinding.toggleNotification.isChecked();
                notify.shortl = mBinding.shortListSwitch.isChecked();
                bean.notif = notify;
                mPresenter.updateNotificationStatus(bean, API_FLAG_JS_MANAGE_NOTIFICATION);
                break;
        }
    }

    @Override
    public void showProgressDialog() {
        showProgress(true);
    }

    @Override
    public void onUpdateNotification(String msg, int status, int apiFlag) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            if (notifyBean != null) {
                notifyBean.orgF = mBinding.toggleFollowOrg.isChecked();
                notifyBean.orgV = mBinding.toggleViewOrg.isChecked();
                notifyBean.chat = mBinding.toggleNotification.isChecked();
                notifyBean.mExp = mBinding.toggleExpire.isChecked();
                notifyBean.calls = mBinding.callNotification.isChecked();
                Intent intent = new Intent();
                intent.putExtra(DATA_TO_SEND, new Gson().toJson(notifyBean));
                setResult(RESULT_OK, intent);
                finish();
            }
        }

    }
}
