package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerInfo;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class JobSeekerInfoPresenter {

    private final IJobSeekerInfo jobSeekerInfo;
    private final SignUpInteractor interactor;
    private final ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            jobSeekerInfo.onUpdate(SUCCESS, response.msg);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            jobSeekerInfo.onUpdate(FAILURE, error);
        }
    };
    private RequestBean requestBean;

    public JobSeekerInfoPresenter(IJobSeekerInfo jobSeekerInfo) {
        this.jobSeekerInfo = jobSeekerInfo;
        interactor = new SignUpInteractor();
    }

    public void updateJobSeekerIntro(String intro) {
        if (isValid(intro)) {
            jobSeekerInfo.preUpdate();
            getRequestBean().bio = intro;
            interactor.updateJobSeekerDetails(msgApiListener, requestBean, API_FLAG_JOB_SEEKER);
        }
    }


    public void updateProfile(RequestBean bean) {
        jobSeekerInfo.preUpdate();
        interactor.updateJobSeekerDetails(msgApiListener, bean, API_FLAG_JOB_SEEKER);

    }


    private boolean isValid(String intro) {
        if (intro == null || TextUtils.isEmpty(intro.trim())) {
            jobSeekerInfo.showToast(ToastType.JOB_SEEKER_DESCRIPTION_EMPTY);
            return false;
        }
        return true;
    }

    private RequestBean getRequestBean() {
        if (requestBean == null)
            requestBean = new RequestBean();
        return requestBean;
    }
}

