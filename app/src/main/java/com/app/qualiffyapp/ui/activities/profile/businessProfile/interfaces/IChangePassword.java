package com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IChangePassword extends BaseInterface {

    void preChange();

    void onChange(String msg, int status);
}
