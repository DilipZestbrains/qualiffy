package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.signupLogin.SpannableClickCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivitySignUpBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.ui.activities.WebViewActivity;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpPresenter;
import com.app.qualiffyapp.utils.SpannableTxt;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.animation.CustomAnimations;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.FacebookResultCallback;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.FbLoginIntegrationWithoutLoginButton;
import com.app.qualiffyapp.utils.socialMediaIntegration.linkedIn.LinkedInActivity;
import com.app.qualiffyapp.utils.socialMediaIntegration.snapchat.SnapChatCallback;
import com.app.qualiffyapp.utils.socialMediaIntegration.snapchat.SnapChatLogin;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.snapchat.kit.sdk.SnapLogin;
import com.snapchat.kit.sdk.login.models.UserDataResponse;
import org.json.JSONObject;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SOCIAL_SIGNUP;
import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;
import static com.app.qualiffyapp.constants.AppConstants.NORMAL;

public class SignUpActivity extends BaseActivity implements SpannableClickCallback, SignupView, FacebookResultCallback, SnapChatCallback {
    private final int LINKEDIN_REQUEST = 101;
    private Transition transition;
    private CustomAnimations customAnimations;
    private ActivitySignUpBinding signupBinding;
    private SpannableTxt spannableTxt;
    private SignUpPresenter mSignUpPresenter;
    private CallbackManager callbackManager;
    private FbLoginIntegrationWithoutLoginButton fbLoginIntegration;
    private SnapChatLogin snapChatLogin;
    private CreateJobSeekerProfileInputModel profileInputModel;

    @Override
    protected void initView() {
        signupBinding = (ActivitySignUpBinding) viewDataBinding;
        callbackManager = CallbackManager.Factory.create();
        profileInputModel = CreateJobSeekerProfileInputModel.refreshInputModel();
        fbLoginIntegration = new FbLoginIntegrationWithoutLoginButton();
        fbLoginIntegration.disconnectFromFacebook();
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        mSignUpPresenter = new SignUpPresenter(this);
        toolbar();
        snapableStringTxt();

    }

    @Override
    protected void setListener() {
        super.setListener();
        signupBinding.signupBtn.setOnClickListener(this);
        signupBinding.fbBtn.setOnClickListener(this);
        signupBinding.linkedInBtn.setOnClickListener(this);
        signupBinding.snapchatBtn.setOnClickListener(this);
    }

    private void toolbar() {
        signupBinding.toolbar.backBtnImg.setOnClickListener(this);
        signupBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.SIGNUP_TITLE));
    }

    private void snapableStringTxt() {
        spannableTxt = new SpannableTxt();
        spannableTxt.setSpannableString(getString(R.string.already_have_account),
                signupBinding.signupTxt,
                getResources().getColor(R.color.colorTextBlack),
                getResources().getColor(R.color.grey_6b6d6d),
                25, this,
                true,
                SPAN_LOGIN);

        spannableTxt.setSpannableString(getString(R.string.tnc),
                signupBinding.tncTxt,
                getResources().getColor(R.color.progress_red),
                getResources().getColor(R.color.colorTextBlack),
                13, this,
                false,
                SPAN_TERMS_N_COND);
    }


    private void setAnimation(){
        customAnimations = new CustomAnimations();
        transition = customAnimations.buildEnterTransition(getResources().getInteger(R.integer.anim_duration_long));
        getWindow().setEnterTransition(transition);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_sign_up;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.signup_btn:
                Utility.setSocialName(this, NORMAL, null, null, signupBinding.usernameEdt.getText().toString());
                mSignUpPresenter.validate(signupBinding.usernameEdt.getText().toString().trim(),
                        signupBinding.mobileNumEdt.getText().toString().trim(),
                        signupBinding.tncCheckbox.isChecked(),
                        signupBinding.etlPhone.getSelectedCountryCode().trim()
                );
                break;

            case R.id.fb_btn:
                fbLoginIntegration.fbLogin(this, callbackManager, this);
                break;

            case R.id.linked_in_btn:
                startActivityForResult(new Intent(this, LinkedInActivity.class), LINKEDIN_REQUEST);

                break;
            case R.id.snapchat_btn:
                SnapLogin.getAuthTokenManager(this).startTokenGrant();
                snapChatLogin = new SnapChatLogin(this, this);
                break;
        }
    }

    @Override
    public void spannableStringClick(int flag) {
        switch (flag) {
            case SPAN_LOGIN:
                onBackPressed();
                break;
            case SPAN_TERMS_N_COND:
                startActivity(new Intent(this, WebViewActivity.class));
                break;
        }

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }


    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        VerifyResponseModel verifyResponseModel = (VerifyResponseModel) response.getRes();

        if (response.getStatus() == 1) {
            switch (apiFlag) {
                case API_FLAG_SIGNUP:
                    Intent i = new Intent(this, VerifyActivity.class);
                    i.putExtra("user_id", verifyResponseModel._id);
                    i.putExtra("user_phn_num", signupBinding.mobileNumEdt.getText().toString());
                    startActivity(i);
                    break;
                case API_FLAG_SOCIAL_SIGNUP:
                    Utility.putStringValueInSharedPreference(this, LOGIN_ACCESS_TOKEN, verifyResponseModel.acsTkn);
                    switch (verifyResponseModel.code) {
                        case 1:
                            openActivity(SelectIndustryActivity.class, null);
                            break;
                        case 2:
                            openActivity(LanguageActivity.class, null);
                            break;
                        case 3:
                            openActivity(AddExperienceActivity.class, null);
                            break;
                        case 4:
                            openActivity(AddCertificateActivity.class, null);
                            break;
                        case 5:
                            openActivity(AddMediaActivity.class, null);
                            break;
                        case 6:
                            openActivity(UserVideoActivity.class, null);
                            break;
                        case 7:
                            openActivity(LocationActivity.class, null);
                            break;
                        case 8:
                            saveName(verifyResponseModel);
                            Intent intent = new Intent(this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        case 9:
                            openActivity(UserPhotoActivity.class, null);
                            break;
                    }
                    break;
            }

        } else {
            Utility.showToast(this, response.err.msg);
        }
        showProgressDialog(false);
    }


    private void saveName(VerifyResponseModel verifyResponseModel) {
        Utility.saveProfile(this, verifyResponseModel, false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }


    @Override
    public void onFbResponse(JSONObject jsonObject) {
        Log.e("facebook response", jsonObject.toString());
        String fb_id = jsonObject.optString("id");
        RequestBean requestBean = new RequestBean();
        requestBean.type = AppConstants.FACEBOOK_TYPE;
        requestBean.social_id = fb_id;
        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                AppConstants.PREFS_ACCESS_TOKEN);

       /* Utility.putStringValueInSharedPreference(this, FIRST_NAME, jsonObject.optString("first_name"));
        Utility.putStringValueInSharedPreference(this, LAST_NAME, jsonObject.optString("last_name"));*/

        showProgressDialog(true);
        mSignUpPresenter.hitApi(API_FLAG_SOCIAL_SIGNUP, requestBean);

//        fbLoginIntegration.disconnectFromFacebook();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                switch (requestCode) {
                    case LINKEDIN_REQUEST:
                        RequestBean requestBean = new RequestBean();
                        requestBean.type = AppConstants.LINKEDIN_TYPE;
                        requestBean.social_id = data.getStringExtra("_id");
                        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                                AppConstants.PREFS_ACCESS_TOKEN);
                        showProgressDialog(true);

                       /* Utility.putStringValueInSharedPreference(this, FIRST_NAME, data.getStringExtra("fname"));
                        Utility.putStringValueInSharedPreference(this, LAST_NAME, data.getStringExtra("lname"));
*/
                        mSignUpPresenter.hitApi(API_FLAG_SOCIAL_SIGNUP, requestBean);
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onFbError(FacebookException facebookException) {
        Log.e("facebook error", facebookException.toString());

    }

    @Override
    public void getSnapChatData(UserDataResponse userDataResponse, String id) {
        RequestBean requestBean = new RequestBean();
        requestBean.type = AppConstants.SNAPCHAT_TYPE;
        requestBean.social_id = id;
        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                AppConstants.PREFS_ACCESS_TOKEN);
//        Utility.putStringValueInSharedPreference(this, FIRST_NAME, userDataResponse.getData().getMe().getDisplayName());
        showProgressDialog(true);
        mSignUpPresenter.hitApi(API_FLAG_SOCIAL_SIGNUP, requestBean);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (snapChatLogin != null)
            snapChatLogin.snapChatUnlink(this);
    }
}
    
