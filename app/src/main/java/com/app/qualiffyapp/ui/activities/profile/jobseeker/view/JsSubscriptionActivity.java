package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.profile.JsSubscriptionAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivitySubscriptionBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JsSubscriptionPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;

public class JsSubscriptionActivity extends BaseActivity implements SignupView {
    private ActivitySubscriptionBinding mBinding;
    private JsSubscriptionAdapter subscriptionAdapter;
    private JsSubscriptionPresenter subscriptionPresenter;

    @Override
    protected void initView() {
        mBinding = (ActivitySubscriptionBinding) viewDataBinding;
        subscriptionPresenter = new JsSubscriptionPresenter(this);
        showProgressDialog(true);
        subscriptionPresenter.hitApi();
        setHeader();
        bindAdapters();
    }

    private void setHeader() {
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.my_subscriptions));
    }

    private void bindAdapters() {

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_subscription;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_btn_img) {
            onBackPressed();
        }
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        if (response.getStatus() == 1) {
            JsSubscriptionModel subscriptionModel = (JsSubscriptionModel) response.getRes();
            if(Utility.getIntFromSharedPreference(this,SUBSCRIPTION_FLAG)==4) {
                JsSubscriptionModel.SubsBean subsBean=new JsSubscriptionModel.SubsBean();
                subsBean.price="FREE Subscription";
                subsBean.name="Admin provide you the free subscription plan to access all the functionality.";

                if(subscriptionModel.subs==null || subscriptionModel.subs.size()==0)
                    subscriptionModel.subs=new ArrayList<>();

                subscriptionModel.subs.add(subsBean);
            }

            if(subscriptionModel.subs.size()==0)
            {
                mBinding.rvSubscriptionList.setVisibility(View.GONE);
                mBinding.noDataTxt.setVisibility(View.VISIBLE);
            }else {
                subscriptionAdapter = new JsSubscriptionAdapter(subscriptionModel.subs);
                mBinding.rvSubscriptionList.setAdapter(subscriptionAdapter);
                mBinding.rvSubscriptionList.setVisibility(View.VISIBLE);
                mBinding.noDataTxt.setVisibility(View.GONE);
            }
        } else {
            Utility.showToast(this, response.err.msg);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
//        validationFailed(error);
        mBinding.rvSubscriptionList.setVisibility(View.GONE);
        mBinding.noDataTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
