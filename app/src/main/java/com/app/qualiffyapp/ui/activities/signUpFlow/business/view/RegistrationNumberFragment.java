package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.app.Activity;
import android.content.Intent;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.FragmentRegistrationNumberBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IRegistrationNumber;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.RegistrationNumberPresenter;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class RegistrationNumberFragment extends BLoginSignUpBaseFragment implements IRegistrationNumber {
    private FragmentRegistrationNumberBinding mBinding;
    private RegistrationNumberPresenter mPresenter;
    private String header;

    @Override
    protected void initUi() {
        mBinding = (FragmentRegistrationNumberBinding) viewDataBinding;
        mPresenter = new RegistrationNumberPresenter(this);
        manageAction(getLoginActivity().isUpdate);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_registration_number;
    }

    @Override
    public void onClick(View v) {
        Utility.hideKeyboard(context);
        switch (v.getId()) {
            case R.id.tvNext:
                if (mBinding.etRegistrationNumber.getText() != null)
                    mPresenter.proceed(mBinding.etRegistrationNumber.getText().toString());
                break;
            case R.id.tvDone:
                if (mBinding.etRegistrationNumber.getText() != null)
                    mPresenter.updateRegistrationNumber(mBinding.etRegistrationNumber.getText().toString());

        }
    }

    @Override
    protected void setListener() {
        mBinding.tvNext.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
    }

    @Override
    protected String getHeader() {
        return header;
    }


    @Override
    public void onNext(String registrationNumber) {
//        getLoginActivity().getRequestBean().reg = registrationNumber;
//        getLoginActivity().getPresenter().gotoFragment(COMPANY_LOGO, true, true);
    }

    @Override
    public void onUpdate(String msg, int status) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (status == SUCCESS) {
            Intent intent = new Intent();
            if (mBinding.etRegistrationNumber.getText() != null)
                intent.putExtra(DATA_TO_SEND, mBinding.etRegistrationNumber.getText().toString());
            getLoginActivity().setResult(Activity.RESULT_OK, intent);
            getLoginActivity().finish();
        }
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.REGISTRATION_NUMBER_EMPTY) {
            Utility.showToast(context, getResources().getString(R.string.registration_number_not_entered));
        }
    }

    private void manageAction(boolean isUpdate) {
        if (isUpdate) {
            mBinding.tvDone.setVisibility(View.VISIBLE);
            mBinding.tvNext.setVisibility(View.GONE);
            mBinding.etRegistrationNumber.setText(getLoginActivity().getIntent().getBundleExtra(BUNDLE).getString(DATA_TO_SEND, ""));
            mBinding.etRegistrationNumber.requestFocus();
            header = getResources().getString(R.string.edit_registration_no);
        } else {
            mBinding.tvNext.setVisibility(View.VISIBLE);
            mBinding.tvDone.setVisibility(View.GONE);
            header = getResources().getString(R.string.registration_no);

        }
    }

}
