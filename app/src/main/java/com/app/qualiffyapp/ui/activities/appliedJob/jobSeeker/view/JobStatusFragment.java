package com.app.qualiffyapp.ui.activities.appliedJob.jobSeeker.view;

import android.view.View;
import androidx.viewpager.widget.ViewPager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.ViewPagerAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.databinding.FragmentJobStatusBinding;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.utils.Utility;
import static com.app.qualiffyapp.constants.AppConstants.IS_SHORTLIST;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_SUBSCRIPTION;
import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;

public class JobStatusFragment extends BaseFragment {
    private FragmentJobStatusBinding mBinding;


    @Override
    protected void initUi() {
        mBinding = (FragmentJobStatusBinding) viewDataBinding;
//        getBundle();
        setupViewPager(mBinding.jobViewpager);

    }


//    private void getBundle() {
//        int isSubscribed = Utility.getIntFromSharedPreference(getContext(), SUBSCRIPTION_FLAG);
//        if (isSubscribed == 0) {
//            mBinding.noSubscriptionMessage.setVisibility(View.GONE);
//            mBinding.tvSubscribe.setVisibility(View.GONE);
//        } else {
//            mBinding.jobTabs.setVisibility(View.GONE);
//            mBinding.jobViewpager.setVisibility(View.GONE);
//            mBinding.noSubscriptionMessage.setVisibility(View.VISIBLE);
//            mBinding.tvSubscribe.setVisibility(View.VISIBLE);
//            return;
//        }
//        setupViewPager(mBinding.jobViewpager);
//    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_job_status;
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new AppliedJobFragment(), getContext().getResources().getString(R.string.applied_job));
        adapter.addFragment(new ShortlistedJobFragment(), getContext().getResources().getString(R.string.shortList_job));
        viewPager.setAdapter(adapter);
        mBinding.jobTabs.setupWithViewPager(mBinding.jobViewpager);
        if (getArguments() != null && getArguments().getString(IS_SHORTLIST) != null) {
            String is_ShortList = getArguments().getString(IS_SHORTLIST);
            if (is_ShortList != null)
                mBinding.jobViewpager.setCurrentItem(1);
        }
    }


    @Override
    public void onClick(View v) {
//        if (v.getId() == R.id.tvSubscribe) {
//            if (getActivity() instanceof DashboardActivity) {
//                ((DashboardActivity) getActivity()).fireEvent(JS_POS_SUBSCRIPTION, null);
//            }
//        }
    }
//
//    @Override
//    protected void setListener() {
//        mBinding.tvSubscribe.setOnClickListener(this);
//    }
}
