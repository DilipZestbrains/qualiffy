package com.app.qualiffyapp.ui.activities.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.SearchView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.app.qualiffyapp.AppService;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.ItemAddListener;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivityDashboardBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.chat.ui.recentChatList.RecentChatListFragment;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.ui.activities.addJob.business.views.AddJobFragment;
import com.app.qualiffyapp.ui.activities.addedJob.views.AddedJobFragment;
import com.app.qualiffyapp.ui.activities.appliedJob.jobSeeker.view.JobStatusFragment;
import com.app.qualiffyapp.ui.activities.contacts.ContactActivity;
import com.app.qualiffyapp.ui.activities.dashboard.business.views.BusinessDashboardFragment;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.DashboardFragment;
import com.app.qualiffyapp.ui.activities.notification.NotificationFragment;
import com.app.qualiffyapp.ui.activities.plans.views.PlansFragment;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment;
import com.app.qualiffyapp.ui.activities.promote.PromotionListFragment;
import com.app.qualiffyapp.ui.activities.recruiter.views.RecruiterFragment;
import com.app.qualiffyapp.ui.activities.subscription.BusinessSubscriptionFragment;
import com.app.qualiffyapp.ui.fragment.DistanceBottomSheet;
import com.app.qualiffyapp.ui.fragment.DrawerFragment;
import com.app.qualiffyapp.utils.Utility;
import com.stripe.android.PaymentConfiguration;

import java.io.File;
import java.util.Objects;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_ADD_ADDED_JOB;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_ADD_JOB;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_CHAT;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_HOME;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_MY_ACCOUNT;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_NOTIFICATIONS;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_PROMOTE;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_RECRUITER;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_SUBSCRIBE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT;
import static com.app.qualiffyapp.constants.AppConstants.IS_SHORTLIST;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.JS_APPLIED_JOB;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_CHAT;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_HOME;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_MY_ACCOUNT;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_NOTIFICATIONS;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_SUBSCRIPTION;
import static com.app.qualiffyapp.constants.AppConstants.POSITION;
import static com.app.qualiffyapp.constants.AppConstants.TEMP_MEDIA_FOLDER;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PHONE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PROFILE_PIC;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;
import static com.app.qualiffyapp.ui.activities.recruiter.views.RecruiterFragment.ADD_RECRUITER_RESULT_CODE;

public class
DashboardActivity extends BaseActivity {
    private ActivityDashboardBinding mainBinding;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mBarDrawerToggle;
    private int userType;
    private int exitFlag = 0;
    private long backTime;
    private DistanceBottomSheet distanceBottomSheet;


    @Override
    protected void initView() {
        mainBinding = (ActivityDashboardBinding) viewDataBinding;
        mDrawerLayout = mainBinding.drawerLayout;
        mBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.Open, R.string.Close);
        mDrawerLayout.addDrawerListener(mBarDrawerToggle);
        mBarDrawerToggle.syncState();
        mainBinding.toolbar.ivArrowRegistration.setOnClickListener(this);
        styleSearchView();
        Utility.putBooleanValueInSharedPreference(DashboardActivity.this,
                AppConstants.APP_SESSION, true);
        getPrefData();
        initPayment();

        Log.e("ACCETOKEN   : ",AppConstants.LOGIN_ACCESS_TOKEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getIntentData();
    }

    private void getIntentData() {

        Bundle b = getIntent().getBundleExtra(BUNDLE);
        if (b != null) {
            if (b.getInt(POSITION, -1) >= 0) {
                int position = b.getInt(POSITION);
                fireEvent(position, null);
            } else if (b.getString(IS_SHORTLIST) != null) {
                Bundle bundle = new Bundle();
                bundle.putString(IS_SHORTLIST, b.getString(IS_SHORTLIST));
                fireEvent(3, bundle);

            }
        }

    }

    private void initPayment() {
        PaymentConfiguration.init(getResources().getString(R.string.stripe_publish_key));
    }

    private void getPrefData() {
        String firebaseUserType;
        userType = Utility.getIntFromSharedPreference(this, AppConstants.USER_TYPE);
        String firstName = Utility.getStringSharedPreference(this, FIRST_NAME);
        String uid = Utility.getStringSharedPreference(this, UID);
        String img = Utility.getStringSharedPreference(this, PROFILE_PIC);
        String phone = Utility.getStringSharedPreference(this, PHONE);
        if (userType == AppConstants.JOB_SEEKER) {
            firebaseUserType = FirebaseUserType.JOB_SEEKER.s;
            onAddFragmentToStack(new DashboardFragment());
        } else {
            firebaseUserType = FirebaseUserType.BUSINESS.s;
            onAddFragmentToStack(new BusinessDashboardFragment());
        }

        if (uid != null) {
            AppFirebaseDatabase.getInstance().registerUser(new User(uid, firstName, firebaseUserType, img, phone));
        }
        startService(new Intent(this, AppService.class));
        //register for incoming calls in application controller
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(CALL_INTENT));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_dashboard;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_arrow_registration:
                Utility.hideKeyboard(this);
                if (mDrawerLayout != null) {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                break;
            case R.id.ivContact:
                openActivity(ContactActivity.class, null);
                break;
            case R.id.ivAddPromotion:
                if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) instanceof ItemAddListener)
                    ((ItemAddListener) Objects.requireNonNull(getSupportFragmentManager().findFragmentById(R.id.fragment_container))).onAdd();
                break;
            case R.id.ivFilter:
                openFilterSheet();
                break;
        }
    }

    private void openFilterSheet() {
        if (distanceBottomSheet == null)
            distanceBottomSheet = new DistanceBottomSheet();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof DistanceBottomSheet.DistanceFilterListener)
            distanceBottomSheet.setDistanceFilterListener((DistanceBottomSheet.DistanceFilterListener) fragment);
        (distanceBottomSheet).show(getSupportFragmentManager(), "Distance Filter");

    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawers();
    }

    public void setFragment(int position, Bundle bundle) {
        switch (position) {
            case JS_POS_HOME:
                setHomeHeader();
                onAddFragmentToStack(new DashboardFragment());
                break;
            case JS_POS_MY_ACCOUNT:
                setHeader(getResources().getString(R.string.account), false);
                onAddFragmentToStack(new com.app.qualiffyapp.ui.activities.profile.jobseeker.view.ProfileFragment());
                break;
            case JS_POS_CHAT:
                setChatHeader();
                onAddFragmentToStack(new RecentChatListFragment());
//                onAddFragmentToStack(new DummyFragment());
                break;
            case JS_POS_NOTIFICATIONS:
                setHeader(getResources().getString(R.string.notification), false);
                onAddFragmentToStack(new NotificationFragment());
//                onAddFragmentToStack(new DummyFragment());
                break;
            case JS_POS_SUBSCRIPTION:
            /*    setHeader(getResources().getString(R.string.subscribe), false);
                onAddFragmentToStack(new JsSubscriptionFragment());*/
                setHeader(getResources().getString(R.string.subscription), false);
                onAddFragmentToStack(new PlansFragment());
                break;

            case JS_APPLIED_JOB:
                setHeader(getResources().getString(R.string.applied_job), false);
//                onAddFragmentToStack(new AppliedJobFragment());
                JobStatusFragment statusFragment = new JobStatusFragment();
                statusFragment.setArguments(bundle);
                onAddFragmentToStack(statusFragment);
                break;
        }
    }

    public void setBusinessFragment(int position) {
        switch (position) {
            case BS_POS_HOME:
                setHomeHeader();
                onAddFragmentToStack(new BusinessDashboardFragment());
                break;
            case BS_POS_MY_ACCOUNT:
                setHeader(getResources().getString(R.string.profile), false);
                onAddFragmentToStack(new BusinessProfileFragment());
                break;
            case BS_POS_CHAT:
                setHeader(getResources().getString(R.string.chats), true);
                onAddFragmentToStack(new RecentChatListFragment());
                break;
            case BS_POS_NOTIFICATIONS:
                setHeader(getResources().getString(R.string.notification), false);
                onAddFragmentToStack(new NotificationFragment());
                break;
            case BS_POS_SUBSCRIBE:
                setHeader(getResources().getString(R.string.subscription), false);
                onAddFragmentToStack(new BusinessSubscriptionFragment());
                break;
            case BS_POS_ADD_JOB:
                setHeader(getResources().getString(R.string.add_job), false);
                onAddFragmentToStack(new AddJobFragment());
                break;
            case BS_POS_ADD_ADDED_JOB:
                setHeader(getResources().getString(R.string.added_job), false);
                onAddFragmentToStack(new AddedJobFragment());
                break;
            case BS_POS_PROMOTE:
                setPromoteHeader(true, getResources().getString(R.string.promote));
                onAddFragmentToStack(new PromotionListFragment());
                break;
            case BS_POS_RECRUITER:
                setPromoteHeader(true, getResources().getString(R.string.recruiter));
                onAddFragmentToStack(new RecruiterFragment());
                break;
        }
    }

    public void onAddFragmentToStack(Fragment fragment) {
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();
    }

    public void setHeader(String header, boolean isSearchable) {
        mainBinding.toolbar.ivContact.setVisibility(View.GONE);
        mainBinding.toolbar.ivAddPromotion.setVisibility(View.GONE);
        mainBinding.toolbar.ivFilter.setVisibility(View.GONE);
        mainBinding.toolbar.tvTitle.setText(header);
        if (isSearchable)
            mainBinding.toolbar.ivSearch.setVisibility(View.VISIBLE);
        else mainBinding.toolbar.ivSearch.setVisibility(View.INVISIBLE);
    }

    public void setHomeHeader() {
        mainBinding.toolbar.ivContact.setVisibility(View.GONE);
        mainBinding.toolbar.ivAddPromotion.setVisibility(View.GONE);
        mainBinding.toolbar.ivFilter.setVisibility(View.VISIBLE);
        mainBinding.toolbar.tvTitle.setText(getResources().getString(R.string.home));
        mainBinding.toolbar.ivSearch.setVisibility(View.VISIBLE);

    }

    public void setChatHeader() {
        mainBinding.toolbar.tvTitle.setText(getResources().getString(R.string.chats));
        mainBinding.toolbar.ivAddPromotion.setVisibility(View.GONE);
        mainBinding.toolbar.ivFilter.setVisibility(View.GONE);
        mainBinding.toolbar.ivSearch.setVisibility(View.VISIBLE);
        mainBinding.toolbar.ivContact.setVisibility(View.VISIBLE);
    }

    public void setPromoteHeader(boolean b, String title) {
        mainBinding.toolbar.ivFilter.setVisibility(View.GONE);
        mainBinding.toolbar.ivSearch.setVisibility(View.GONE);
        mainBinding.toolbar.ivContact.setVisibility(View.GONE);
        mainBinding.toolbar.tvTitle.setText(title);
        if (b) {
            mainBinding.toolbar.ivAddPromotion.setVisibility(View.VISIBLE);
        } else {
            mainBinding.toolbar.ivAddPromotion.setVisibility(View.GONE);
        }
    }

    private void styleSearchView() {
        View searchPlate = mainBinding.toolbar.ivSearch.findViewById(R.id.search_plate);
        searchPlate.setBackgroundResource(R.color.colorWhite);

    }

    @Override
    protected void setListener() {
        mainBinding.toolbar.ivAddPromotion.setOnClickListener(this);
        mainBinding.toolbar.ivContact.setOnClickListener(this);
        mainBinding.toolbar.ivSearch.setOnSearchClickListener(v -> {
            mainBinding.toolbar.ivSearch.setMaxWidth(Integer.MAX_VALUE);
            mainBinding.toolbar.ivFilter.setVisibility(View.GONE);

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (currentFragment instanceof RecentChatListFragment) {
                mainBinding.toolbar.ivContact.setVisibility(View.GONE);

            }
        });
        mainBinding.toolbar.ivFilter.setOnClickListener(this);
        mainBinding.toolbar.ivSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);

                if (newText.length() >= 2) {
                    if (currentFragment instanceof DashboardFragment) {
                        DashboardFragment dashboardFragment = (DashboardFragment) currentFragment;
                        dashboardFragment.searchJobs(newText);
                    } else if (currentFragment instanceof RecentChatListFragment) {
                        RecentChatListFragment recentChatListFragment = (RecentChatListFragment) currentFragment;
                        recentChatListFragment.searchChats(newText);
                    } else if (currentFragment instanceof BusinessDashboardFragment) {
                        BusinessDashboardFragment dashboardFragment = (BusinessDashboardFragment) currentFragment;
                        dashboardFragment.searchJobs(newText);
                    }
                } else {
                    if (currentFragment instanceof DashboardFragment) {
                        DashboardFragment dashboardFragment = (DashboardFragment) currentFragment;
                        dashboardFragment.searchJobs(null);
                    } else if (currentFragment instanceof RecentChatListFragment) {
                        RecentChatListFragment recentChatListFragment = (RecentChatListFragment) currentFragment;
                        recentChatListFragment.searchChats(newText);
                    } else if (currentFragment instanceof BusinessDashboardFragment) {
                        BusinessDashboardFragment dashboardFragment = (BusinessDashboardFragment) currentFragment;
                        dashboardFragment.searchJobs(null);
                    }
                }
                return false;
            }
        });


        ImageView closeButton = mainBinding.toolbar.ivSearch.findViewById(R.id.search_close_btn);
        // Set on click listener
        closeButton.setOnClickListener(v -> {
            EditText et = findViewById(R.id.search_src_text);
            et.setText("");
            mainBinding.toolbar.ivSearch.setQuery("", false);
            mainBinding.toolbar.ivSearch.onActionViewCollapsed();
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (currentFragment instanceof RecentChatListFragment && (Utility.getIntFromSharedPreference(this, USER_TYPE) == JOB_SEEKER)) {
                mainBinding.toolbar.ivContact.setVisibility(View.VISIBLE);
                ((RecentChatListFragment) currentFragment).onCancelSearch();
            } else if (currentFragment instanceof DashboardFragment) {
                mainBinding.toolbar.ivFilter.setVisibility(View.VISIBLE);

            } else if (currentFragment instanceof BusinessDashboardFragment) {
                mainBinding.toolbar.ivFilter.setVisibility(View.VISIBLE);
                ((BusinessDashboardFragment) currentFragment).stopSearch();
            }

        });
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if ((currentFragment instanceof BusinessDashboardFragment) ||
                (currentFragment instanceof DashboardFragment) ||
                (currentFragment instanceof DummyFragment)
        ) {
            if (exitFlag == 0) {
                exitFlag = 1;
                backTime = System.currentTimeMillis();
                Utility.showToast(this, getResources().getString(R.string.exit_alert));

            } else {
                if (System.currentTimeMillis() <= backTime + 3000) {
                    finish();
                } else {
                    Utility.showToast(this, getResources().getString(R.string.exit_alert));
                    exitFlag = 1;
                    backTime = System.currentTimeMillis();
                }
            }
        } else {
            if (Utility.getIntFromSharedPreference(this, USER_TYPE) == JOB_SEEKER) {
                fireEvent(JS_POS_HOME, null);
            } else
                fireEvent(BS_POS_HOME, null);
        }
    }

    public void fireEvent(int pos, Bundle bundle) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.navFragment);
        if (fragment instanceof DrawerFragment) {
            DrawerFragment drawerFragment = (DrawerFragment) fragment;
            drawerFragment.setItemSelection(pos, bundle);
        }
    }


    @Override
    protected void onDestroy() {
        Utility.deleteMediaDir();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_RECRUITER_RESULT_CODE) {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof RecruiterFragment) {
                ((RecruiterFragment) fragment).isAdd = true;
                if (resultCode == RESULT_OK)
                    ((RecruiterFragment) fragment).hitApi(true);
            }
        }
    }
}
