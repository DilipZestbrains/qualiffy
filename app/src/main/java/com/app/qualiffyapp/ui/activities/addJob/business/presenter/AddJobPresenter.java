package com.app.qualiffyapp.ui.activities.addJob.business.presenter;

import android.widget.Spinner;

import com.app.qualiffyapp.customViews.PlacesFieldSelector;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.SelectIndusPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.libraries.places.api.model.AddressComponent;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.lang.reflect.Field;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_JOB;
import static com.app.qualiffyapp.constants.AppConstants.COUNTRY;
import static com.app.qualiffyapp.constants.AppConstants.LOCALITY;

public class AddJobPresenter extends SelectIndusPresenter {
    private IAddress address;
    private SignupView signupView;
    ApiListener<ResponseModel<MsgResponseModel>> msgResponseModel = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);

        }
    };
    private SignUpInteractor interactor;


    public AddJobPresenter(IAddress address, SignupView view) {
        super(view);
        this.address = address;
        this.signupView = view;
        interactor = new SignUpInteractor();
    }

    public void pickAddress(Place place) {
        if (place != null) {
            String state = "", country = "";
            double lat, lng;
            lat = place.getLatLng().latitude;
            lng = place.getLatLng().longitude;
            for (AddressComponent com : place.getAddressComponents().asList()) {
                for (String type : com.getTypes()) {
                    if (type.equals(LOCALITY)) {
                        state = com.getName();
                        break;
                    } else if (type.equals(COUNTRY))
                        country = com.getName();
                }
            }

            address.onAddressSelect(place.getAddress(), country, state, Double.toString(lat), Double.toString(lng));
        }
    }

    public void initAddressPickerDialog() {
        PlacesFieldSelector fieldSelector;
        fieldSelector = new PlacesFieldSelector();
        Autocomplete.IntentBuilder intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fieldSelector.getAllFields());
        address.openAutoSelectPlace(intent);
    }

    public void addJobApi(RequestBean bean) {
        if (bean != null)
            interactor.businessAddJobApi(msgResponseModel, bean, API_FLAG_BUSINESS_ADD_JOB);

    }

    public void setHeighofSpinner(Spinner spinner) {
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight(300);
            popupWindow.setWidth(200);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
            e.printStackTrace();
        }
    }


    public interface IAddress {
        void openAutoSelectPlace(Autocomplete.IntentBuilder intent);

        void onAddressSelect(String name, String country, String state, String lat, String lng);
    }


}
