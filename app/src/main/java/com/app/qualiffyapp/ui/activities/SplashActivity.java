package com.app.qualiffyapp.ui.activities;

import android.os.Handler;
import android.view.View;
import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivitySplashBinding;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.PreActivity;
import com.app.qualiffyapp.utils.Utility;
import com.google.firebase.iid.FirebaseInstanceId;
import static com.app.qualiffyapp.constants.AppConstants.IS_APP_VISIBLE;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;

public class SplashActivity extends BaseActivity {

    private ActivitySplashBinding mActivitySplashBinding;
    private Handler mHandler;
    private Runnable runnable = () -> {
        int userType = Utility.getIntFromSharedPreference(getApplicationContext(), USER_TYPE);
        switch (userType) {
            case 1:
                openActivity(DashboardActivity.class, null);
                break;
            case 4:
                openActivity(DashboardActivity.class, null);
                break;
            default:
                openActivity(PreActivity.class, null);
                break;
        }
        finish();
    };

    @Override
    protected void initView() {
        mActivitySplashBinding = (ActivitySplashBinding) viewDataBinding;
        Utility.putBooleanValueInSharedPreference(this, IS_APP_VISIBLE, true);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    if (task.getResult() != null) {
                        String token = task.getResult().getToken();
                        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN, token);
                    }

                });
        ApplicationController.getGlideInstance().load(R.drawable.qualiffy_splash).fitCenter().into(mActivitySplashBinding.splashLogo);
        mHandler = new Handler();
        mHandler.postDelayed(runnable, 3000);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacks(runnable);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_splash;
    }

    @Override
    public void onClick(View v) {

    }
}
