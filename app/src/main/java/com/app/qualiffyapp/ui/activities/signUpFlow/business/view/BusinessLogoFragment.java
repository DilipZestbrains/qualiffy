package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import androidx.annotation.NonNull;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.FragmentAddCompanyLogoBinding;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessLogo;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLogoPresenter;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.List;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_IMAGE;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.ADD_MEDIA;
import static com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity.CROPPER_SQUARE;

public class BusinessLogoFragment extends BLoginSignUpBaseFragment implements IBusinessLogo {
    private final int CAMERA_REQUEST_CODE = 122;
    private final int GALLARY_REQUEST_CODE = 123;
    private FragmentAddCompanyLogoBinding mBinding;
    private BusinessLogoPresenter mPresenter;
    private String header;


    @Override
    protected void initUi() {
        mBinding = (FragmentAddCompanyLogoBinding) viewDataBinding;
        mPresenter = new BusinessLogoPresenter(this);
        getIntentData();
        if (getLoginActivity().getRequestBean().imgUrl != null)
            mBinding.ivBusinessLogo.setImageURI(Uri.parse(getLoginActivity().getRequestBean().imgUrl));
    }

    private void getIntentData() {
        if (getLoginActivity().isUpdate) {
            changeViewForUpdation();
            String url;
            if ((url = getLoginActivity().getIntent().getBundleExtra(BUNDLE).getString(DATA_TO_SEND)) != null) {
                GlideUtil.loadImage(mBinding.ivBusinessLogo, url, R.drawable.photo_placeholder, 1000, 0);
            }
            return;
        }
        header = getResources().getString(R.string.company_logo);
    }

    private void changeViewForUpdation() {
        mBinding.tvNext.setVisibility(View.GONE);
        mBinding.tvDone.setVisibility(View.VISIBLE);
        header = getResources().getString(R.string.edit_Company_logo);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_add_company_logo;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivCamera:
                showCameraDialog();
//                onAskForSomePermission(context, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE);
                break;
            case R.id.tvNext:
                mPresenter.proceed(getLoginActivity().getRequestBean().imgUrl);
                break;
            case R.id.tvDone:
                mPresenter.update(getLoginActivity().getRequestBean().imgUrl);
        }
    }

    private void showCameraDialog() {
        CustomDialogs customDialogs = new CustomDialogs(getActivity(), this);
        customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                getResources().getString(R.string.add_media_dialog_body),
                getResources().getString(R.string.add_media_dialog_pos_btn),
                getResources().getString(R.string.add_media_dialog_neg_btn),
                getResources().getString(R.string.ADD_MEDIA_TITLE));
        customDialogs.showDialog();
    }

    @Override
    protected String getHeader() {
        return header;
    }

    @Override
    protected void setListener() {
        mBinding.ivCamera.setOnClickListener(this);
        mBinding.tvNext.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
    }

    @Override
    public void showToast(ToastType toastType) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        super.onPermissionsDenied(requestCode, list);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        if (requestCode == CAMERA_STORAGE_PERMISSION_REQUEST_CODE) {
            requestCamera();
        } else if (requestCode == GALLERY_STORAGE_PERMISSION_REQUEST_CODE) {
            requestGallary();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //to pick image from gallery
        EasyImage.handleActivityResult(requestCode, resultCode, data, getLoginActivity(), new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                if (list.get(0) != null) {
                    File imgFile = list.get(0);
                    openEditor(imgFile.getPath());
                    /*Intent intent = new Intent(getContext(), PhotoEditorActivity.class);
                    intent.putExtra(MEDIA_PATH, imgFile.getPath());
                    startActivityForResult(intent, REQUEST_CODE_FILTER_IMAGE);*/

                }
            }
        });
        //to pick images from camera
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_CAMERA_IMG) {
                getLoginActivity().getRequestBean().imgUrl = data.getStringExtra(MEDIA_PATH);
            }else if(requestCode == REQUEST_CODE_FILTER_IMAGE ){
                getLoginActivity().getRequestBean().imgUrl = data.getStringExtra(MEDIA_PATH);
            }
        }

        //to set image to businessLogo
        if (getLoginActivity().getRequestBean().imgUrl != null)
            GlideUtil.loadImage(mBinding.ivBusinessLogo, getLoginActivity().getRequestBean().imgUrl, R.drawable.photo_placeholder, 1000, 0);

    }

    @Override
    public void onNext() {
        getLoginActivity().enableSkip(true);
        getLoginActivity().getPresenter().gotoFragment(ADD_MEDIA, true, true);
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(String msg, AddMediaResponseModel model) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (model != null) {
            Intent intent = new Intent();
            intent.putExtra(DATA_TO_SEND, model.bP + model.img_id);
            getLoginActivity().setResult(RESULT_OK, intent);
            getLoginActivity().finish();
        }
    }

    @Override
    public void onYesClicked(String from) {
        onAskForSomePermission(context, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onNoClicked(String from) {
        onAskForSomePermission(context, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE);


    }


    private void requestCamera() {
        openCameraActivity(null);
    }

    private void requestGallary() {
        EasyImage.openGallery(this, GALLARY_REQUEST_CODE);

    }


    private void openEditor(String imgPath) {


        try {
            SandriosCamera.cropperSquare = true;

            if (getContext() != null) {
                Intent myIntent = new Intent(getContext(), Class.forName(PHOTO_EDITOR_ACTIVITY));
                myIntent.putExtra(MEDIA_PATH, imgPath);
                myIntent.putExtra(CROPPER_SQUARE,true );
                startActivityForResult(myIntent, REQUEST_CODE_CAMERA_IMG);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    private void openCameraActivity(String imgPath) {
      /*  Intent i = new Intent(context, CameraActivity.class);
        i.putExtra(MEDIA_TYPE, 1);
        i.putExtra(MEDIA_PATH, imgPath);
        startActivityForResult(i, CAMERA_REQUEST_CODE);*/
          SandriosCamera
                  .with()
                  .setShowPicker(true)
                  .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                  .setCropperSquare(true)
                  .launchCamera(this);

    }
}
