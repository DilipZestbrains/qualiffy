package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_INDUSTRY;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_ROLE;

public class SelectIndusPresenter implements ApiListener<ResponseModel<IndustryResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public SelectIndusPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }


    @Override
    public void onApiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void hitApi(String skey) {
        mSignUpInteractor.getIndustryyApi(this, skey, API_FLAG_GET_INDUSTRY);
    }

    public void getRole(String skey, String indusId) {
        mSignUpInteractor.getRoleApi(this, skey, indusId, API_FLAG_GET_ROLE);

    }


}
