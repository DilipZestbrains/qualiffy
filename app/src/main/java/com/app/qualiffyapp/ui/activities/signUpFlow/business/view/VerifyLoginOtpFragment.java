package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.FragmentVerifyOtpLoginBinding;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IVerifyFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.PresenterVerifyFragmnet;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.IS_SMALL_ORG;
import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.COMPANY_NAME;

public class VerifyLoginOtpFragment extends BLoginSignUpBaseFragment implements IVerifyFragment {

    private FragmentVerifyOtpLoginBinding mBinding;
    private PresenterVerifyFragmnet mPresenter;

    @Override
    protected void initUi() {
        mBinding = (FragmentVerifyOtpLoginBinding) viewDataBinding;
        mPresenter = new PresenterVerifyFragmnet(this);
        mBinding.resendOtpTxt.setEnabled(false);
        mPresenter.startTick();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_verify_otp_login;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvVerify:
                if (getLoginActivity() != null) {
                    Utility.hideKeyboard(context);
                    if (mBinding.otpEdt.getText().toString().equals(getLoginActivity().otp)) {
                        if (getLoginActivity().isLogin) {
                            mPresenter.verifyOtpPhoneLogin(getLoginActivity().getRequestBean(), false);
                        } else
                            mPresenter.verifyOtpPhoneSignUp(getLoginActivity().getRequestBean(), false);
                    } else {
                        Utility.showToast(context, getString(R.string.otp_verify_error_alert));
                    }
                }
                break;
            case R.id.resend_otp_txt:
                if (getLoginActivity() != null) {
                    mBinding.timerTxt.setVisibility(View.VISIBLE);

                    Utility.hideKeyboard(context);
                    if (getLoginActivity().isLogin)
                        mPresenter.verifyOtpPhoneLogin(getLoginActivity().getRequestBean(), true);
                    else mPresenter.verifyOtpPhoneSignUp(getLoginActivity().getRequestBean(), true);
                }
                break;
        }
    }

    @Override
    protected void setListener() {
        mBinding.resendOtpTxt.setOnClickListener(this);
        mBinding.tvVerify.setOnClickListener(this);
    }

    @Override
    public void updateTime(String time) {
        if (getLoginActivity() != null) {
            if (time.equals("00:00")) {
                mBinding.timerTxt.setVisibility(View.GONE);
            } else {
                mBinding.timerTxt.setVisibility(View.VISIBLE);
                String timer = " "+getLoginActivity().getResources().getString(R.string.otp_verify) + time;
                mBinding.timerTxt.setText(timer);
            }
        }
    }

    @Override
    public void onTimeCompletion() {
        mBinding.resendOtpTxt.setEnabled(true);
    }

    @Override
    public void onOtpVerification(String msg, VerifyResponseModel model) {
        getLoginActivity().showProgress(false);
        Utility.showToast(context, msg);
        mBinding.otpEdt.getText().clear();
        if (model != null) {
            Utility.putStringValueInSharedPreference(context, LOGIN_ACCESS_TOKEN, model.acsTkn);
            Utility.putIntValueInSharedPreference(context, AppConstants.ORG_TYPE,model.orgType);
            Utility.saveUserId(model._id, getContext());
            switch (model.code) {
                case 2:
                    if(model.orgType==2){
                        openDashBoard(model);
                    }else {
                        Bundle bundle = new Bundle();
                        bundle.putInt(IS_SMALL_ORG, model.orgType);
                        getLoginActivity().getPresenter().gotoFragmentWithBundle(COMPANY_NAME, true, true, bundle);
                    }
                    break;
                case 3:
                   openDashBoard(model);
                    break;
                default:
                    if(model.orgType==2){
                        openDashBoard(model);
                    }else {
                        getLoginActivity().finish();
                        getLoginActivity().openActivity(LoginActivity.class, null);
                    }
                    break;
            }
        }
    }

    private void openDashBoard(VerifyResponseModel model) {
        saveName(model);
        getLoginActivity().finishAffinity();
        getLoginActivity().openActivity(DashboardActivity.class, null);
    }

    private void saveName(VerifyResponseModel verifyResponseModel) {
        Utility.saveProfile(context, verifyResponseModel, true);
    }

    @Override
    public void onOtpResend(String msg, SingupResponse model) {
        getLoginActivity().showProgress(false);
        mBinding.resendOtpTxt.setEnabled(false);
        mPresenter.startTick();
        Utility.showToast(context, msg);
    }

    @Override
    public void showToast(ToastType type) {
        switch (type) {
            case EMAIL_OTP_EMPTY:
                Utility.showToast(context, getResources().getString(R.string.otp_length_email));
                break;
            case MOBILE_OTP_EMPTY:
                Utility.showToast(context, getResources().getString(R.string.otp_length_mobile));
                break;
            case MOBILE_OTP_NOT_COMPLETE:
                Utility.showToast(context, getResources().getString(R.string.empty_otp));
                break;
            case EMAIL_OTP_NOT_COMPLETE:
                Utility.showToast(context, getResources().getString(R.string.empty_otp));
        }
    }

    @Override
    public void preOtpVerification() {
        getLoginActivity().showProgress(true);
    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.VERIFY_TITLE);
    }
}
