package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityJobSeekerInformationBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobSeekerInfoPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerInfo;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.view.ProfileFragment.INFORMATION;

public class JobSeekerInfoActivity extends BaseActivity implements IJobSeekerInfo {
    private ActivityJobSeekerInformationBinding mBinding;
    private JobSeekerInfoPresenter mPresenter;


    @Override

    protected void initView() {
        mBinding = (ActivityJobSeekerInformationBinding) viewDataBinding;
        mPresenter = new JobSeekerInfoPresenter(this);
        useIntentData();
        setHeader();
    }

    private void useIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null && bundle.getString(INFORMATION) != null && !bundle.getString(INFORMATION).equals("")) {

            mBinding.etDescription.setText(bundle.getString(INFORMATION));
            mBinding.etDescription.requestFocus();
        } else
            mBinding.etDescription.setHint(getResources().getString(R.string.write_a_brief_description));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_job_seeker_information;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDone:
                mPresenter.updateJobSeekerIntro(mBinding.etDescription.getText().toString());
                break;
            case R.id.back_btn_img:
                onBackPressed();
        }

    }

    @Override
    protected void setListener() {
        mBinding.tvDone.setOnClickListener(this);
        mBinding.header.backBtnImg.setOnClickListener(this);
    }

    private void setHeader() {
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.introduction));
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(int status, String msg) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra(INFORMATION, mBinding.etDescription.getText().toString());
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.JOB_SEEKER_DESCRIPTION_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.please_enter_job_seeker_intro));
        }
    }


}
