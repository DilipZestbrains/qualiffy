package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

public interface IBusinessSize {
    String BUSINESS_TYPE = "businessType";

    void proceedSignup(BusinessType type);

    void showAdminSignupDialog();

    enum BusinessType {
        LARGE, SMALL
    }


}
