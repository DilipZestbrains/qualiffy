package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface ILocationFragment extends BaseInterface {

    void proceed();

    void askLocationPermission();

}
