package com.app.qualiffyapp.ui.activities.profile.otherUser;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityBusinessProfileBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_VIEW_BUSINESS_PROFILE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;

public class BusinessProfileActivity extends BaseActivity implements SignupView {
    private ActivityBusinessProfileBinding mBinding;
    private OtherBusinessPresenter mPresenter;

    @Override
    protected void initView() {
        mBinding = (ActivityBusinessProfileBinding) viewDataBinding;
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.profile));
        mPresenter = new OtherBusinessPresenter(this);
        getIntentData();

    }

    private void getIntentData() {
        Bundle b = getIntent().getBundleExtra(BUNDLE);
        if (b != null) {
            String userId = b.getString(USER_ID);
            if (userId != null)
                loadData(userId);
        }
    }

    private void loadData(String userId) {
        mPresenter.getBusinessProfile(userId);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_business_profile;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.back_btn_img) {
            onBackPressed();
        }
    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_VIEW_BUSINESS_PROFILE:
                if (response.getStatus() == SUCCESS) {
                    if (response.getRes() != null) {
                        BusinessProfileResponseModel res = (BusinessProfileResponseModel) response.getRes();
                        setViewDetails(res);
                    }
                }
                break;
        }
    }

    private void setViewDetails(BusinessProfileResponseModel res) {
        GlideUtil.loadCircularImage(mBinding.ivCompanyLogo, res.bP + res.org.img, R.drawable.profile_placeholder);
        mBinding.tvCompanyName.setText(res.org.name);
        mBinding.tvCompanyLocation.setText(res.org.state);
        mBinding.tvProfileDetails.setText(res.org.bio);
        mBinding.tvFollowCount.setText(String.valueOf(res.fl_count));
        mBinding.tvRecomendCount.setText(String.valueOf(res.r_count));
        mBinding.tvUnRecommandCount.setText(String.valueOf(res.unr_count));
        mBinding.tvEmployeeStrengthDetails.setText(res.org.empls);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
