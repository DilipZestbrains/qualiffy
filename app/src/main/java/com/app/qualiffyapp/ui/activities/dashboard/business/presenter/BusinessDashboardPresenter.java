package com.app.qualiffyapp.ui.activities.dashboard.business.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_VIEW_COUNT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_IS_FOLLOW;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_IS_SHORTLIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_OTHER_USER_BADGE;

public class BusinessDashboardPresenter {
    private SignupView signupView;
    ApiListener<ResponseModel<BusinessHomeResponseModel>> responseModel = new ApiListener<ResponseModel<BusinessHomeResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<BusinessHomeResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };
    ApiListener<ResponseModel<MsgResponseModel>> msgResponseModel = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };
    private SignUpInteractor interactor;


    public void addBusinessCount(String id) {
        if (id != null) {
            interactor.businessAddViewCountApi(msgResponseModel, id, API_FLAG_BUSINESS_ADD_VIEW_COUNT);
        }
    }
    public BusinessDashboardPresenter(SignupView signupView) {
        this.signupView = signupView;
        this.interactor = new SignUpInteractor();

    }

    public void hitApis(String userId, String jobId, int isRight, int apiflag) {
        if (userId != null) {
            switch (apiflag) {
                case API_FLAG_BUSINESS_DASHBOARD_IS_FOLLOW:
                    interactor.businessIsFollowApi(msgResponseModel, userId, isRight, apiflag);
                    break;
                case API_FLAG_BUSINESS_DASHBOARD_IS_SHORTLIST:
                    if (jobId != null)
                        interactor.businessIsShortlistApi(msgResponseModel, userId, jobId, isRight, apiflag);
                    break;
                    case API_FLAG_BUSINESS_OTHER_USER_BADGE:
                        interactor.businessOtherUserProfileBadge(msgResponseModel,userId,isRight,apiflag);
                        break;

            }
        }
    }

    public void hitDashBoardApi(int page, String skey, int distance) {
        interactor.businessDashBoardApi(responseModel, page, skey, distance, API_FLAG_BUSINESS_DASHBOARD);
    }


}
