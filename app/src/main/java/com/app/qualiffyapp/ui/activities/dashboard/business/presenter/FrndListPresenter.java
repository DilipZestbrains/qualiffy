package com.app.qualiffyapp.ui.activities.dashboard.business.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.business.UserFrndListResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_FRND_LIST;

public class FrndListPresenter implements ApiListener<ResponseModel<UserFrndListResponseModel>> {
    private SignupView signupView;
    private SignUpInteractor interactor;

    public FrndListPresenter(SignupView signupView) {
        this.signupView = signupView;
        interactor = new SignUpInteractor();
    }

    public void hitApi(String userId, int pageNo, boolean isShowProgress) {
        if (userId != null) {
            if (isShowProgress)
                signupView.showProgressDialog(true);
            interactor.UserFrndListApi(this, userId, pageNo, API_FLAG_BUSINESS_DASHBOARD_FRND_LIST);
        }
    }


    @Override
    public void onApiSuccess(ResponseModel<UserFrndListResponseModel> response, int apiFlag) {
        if (response != null)
            signupView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.apiError(error, apiFlag);
    }

    @Override
    public void onApiNoResponse(int apiFlag) {

    }
}
