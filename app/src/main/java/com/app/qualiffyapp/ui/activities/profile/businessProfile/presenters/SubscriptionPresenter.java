package com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_SUBSCRIPTION;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_SUBSCRIPITON;

public class SubscriptionPresenter implements ApiListener<ResponseModel<JsSubscriptionModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public SubscriptionPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<JsSubscriptionModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void hitApi(int pageNo, int limit) {
        mSignUpInteractor.businessSubscription(this, pageNo, limit, API_FLAG_JS_SUBSCRIPITON);
    }

    public void hitApi() {
        mSignUpInteractor.businessSubscriptionCurrent(this, API_FLAG_BUSINESS_SUBSCRIPTION);
    }
}
