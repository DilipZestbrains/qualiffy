package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.io.File;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB_VIDEO;

public class ApplyJobPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {
    private SignupView view;
    private SignUpInteractor interactor;

    public ApplyJobPresenter(SignupView view) {
        this.view = view;
        this.interactor = new SignUpInteractor();

    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void ApplyJobApi(File file, String isProfileVideo, String jobId) {
        if (file == null)
            interactor.jsApplyJobApi(this, jobId, isProfileVideo, API_FLAG_JS_APPLY_JOB);
        else
            interactor.jsApplyJobWithVideoApi(this, file, jobId, API_FLAG_JS_APPLY_JOB_VIDEO);


    }
}
