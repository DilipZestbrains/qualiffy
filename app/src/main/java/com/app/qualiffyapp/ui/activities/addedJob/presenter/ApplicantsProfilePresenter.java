package com.app.qualiffyapp.ui.activities.addedJob.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsProfileResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_VIEW_COUNT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_JOB_APPLICANTS_PROFILE;

public class ApplicantsProfilePresenter {
    private SignUpInteractor interactor;
    private SignupView signupView;
    ApiListener<ResponseModel<ApplicantsProfileResponseModel>> apiListener = new ApiListener<ResponseModel<ApplicantsProfileResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<ApplicantsProfileResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };
    ApiListener<ResponseModel<MsgResponseModel>> msgApiListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };


    public ApplicantsProfilePresenter(SignupView view) {
        this.signupView = view;
        this.interactor = new SignUpInteractor();
    }

    public void getApplicantProfileList(String userId) {
        if (userId != null) {
            signupView.showProgressDialog(true);
            interactor.businessJobApplicantsProfileApi(apiListener, userId, API_FLAG_BUSINESS_JOB_APPLICANTS_PROFILE);
        }

    }

    public void addViewCount(String userId) {
        if (userId != null) {
            interactor.businessAddViewCountApi(msgApiListener, userId, API_FLAG_BUSINESS_ADD_VIEW_COUNT);
        }

    }

    public String setEducation(List<ApplicantsProfileResponseModel.PrflBean.EduBean> eduBean) {
        if (eduBean != null && eduBean.size() > 0) {
            String education = "";
            for (ApplicantsProfileResponseModel.PrflBean.EduBean edu : eduBean) {
                if (education.equals(""))
                    education = "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + "  " + edu.addr + "\n";
                else
                    education = education + "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + "  " + edu.addr + "\n";
            }
            return education;
        } else return "N/A";
    }


    public String setExpDuration(ApplicantsProfileResponseModel.ExpInfoBean expInfo) {
        if (expInfo != null) {
            String duration = expInfo.yrs + Utility.singularPluralText("year",expInfo.yrs);
            return duration;
        } else return "N/A";
    }

    public String setSkills(List<String> skills) {
        if (skills != null && skills.size()>0) {
            String str = "";
            for (String skill : skills) {
                if (str.equals(""))
                    str = skill;
                else
                    str = str + ", " + skill;

            }
            return str;
        } else return "N/A";
    }

}