package com.app.qualiffyapp.ui.activities.signUpFlow.business.base;

import android.util.Log;

import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity;

public abstract class BLoginSignUpBaseFragment extends BaseFragment {

    protected LoginActivity getLoginActivity() {
        try {
            return ((LoginActivity) getActivity());
        } catch (Exception e) {
            Log.e(BLoginSignUpBaseFragment.class.getName(), e.getMessage());
            return null;
        }
    }

    protected abstract String getHeader();

    @Override
    public void onResume() {
        super.onResume();
        if (getLoginActivity() != null) {
            getLoginActivity().setHeader(getHeader());
        }


    }
}
