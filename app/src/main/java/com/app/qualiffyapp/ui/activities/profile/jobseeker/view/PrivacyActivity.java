package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityPrivacyBinding;
import com.app.qualiffyapp.ui.activities.profile.IManageNotification;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.PrivacyPresenter;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class PrivacyActivity extends BaseActivity implements IManageNotification {
    private ActivityPrivacyBinding privacyBinding;
    private PrivacyPresenter mPresenter;
    CompoundButton.OnCheckedChangeListener toogleListener = (buttonView, isChecked) -> {
        mPresenter.hitapi(isChecked);
        privacyBinding.tooglePrivcy.setOnCheckedChangeListener(null);
    };

    @Override
    protected void initView() {
        privacyBinding = (ActivityPrivacyBinding) viewDataBinding;
        mPresenter = new PrivacyPresenter(this);
        getIntentData();
        toolbar();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            managePrivacy(bundle.getBoolean(DATA_TO_SEND, false));

        }
    }

    private void managePrivacy(boolean aBoolean) {
        privacyBinding.tooglePrivcy.setChecked(aBoolean);
        privacyBinding.tooglePrivcy.setOnCheckedChangeListener(toogleListener);
    }

    @Override
    public void showProgressDialog() {
        showProgress(true);
    }

    @Override
    public void onUpdateNotification(String msg, int status, int apiFlag) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status != SUCCESS)
            privacyBinding.tooglePrivcy.setChecked(!privacyBinding.tooglePrivcy.isChecked());
        privacyBinding.tooglePrivcy.setOnCheckedChangeListener(toogleListener);


    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(DATA_TO_SEND, privacyBinding.tooglePrivcy.isChecked());
        setResult(RESULT_OK, intent);
        finish();
    }


    private void toolbar() {
        privacyBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.PRIVACY_TITLE));
    }

    @Override
    protected void setListener() {
        super.setListener();
        privacyBinding.toolbar.backBtnImg.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_privacy;
    }


}
