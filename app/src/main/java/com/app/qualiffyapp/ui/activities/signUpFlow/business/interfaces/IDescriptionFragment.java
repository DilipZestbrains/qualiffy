package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IDescriptionFragment extends BaseInterface, IUpdatableDetail {

    void onRegister(VerifyResponseModel responseModel);

    void showToast(String errorMsg);

}
