package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ISelectIndustryFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class SelectIndustryPresenter {
    private ISelectIndustryFragment selectIndustryFragment;
    private SignUpInteractor interactor;
    private ApiListener<ResponseModel<IndustryResponseModel>> industryApiListener = new ApiListener<ResponseModel<IndustryResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<IndustryResponseModel> response, int apiFlag) {
            if (response != null) {
                if (response.getStatus() == SUCCESS) {
                    selectIndustryFragment.onSearchResult(response.getRes().msg, response.getRes().jobC);
                } else if (response.err != null) {
                    selectIndustryFragment.onSearchResult(response.err.msg, null);
                }
            } else selectIndustryFragment.searchResultError();
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            selectIndustryFragment.onSearchResult(error, null);

        }
    };

    public SelectIndustryPresenter(ISelectIndustryFragment selectIndustryFragment) {
        this.selectIndustryFragment = selectIndustryFragment;
        interactor = new SignUpInteractor();
    }

    public void getIndustryList(String str) {
        interactor.getIndustryyApi(industryApiListener, str, API_FLAG_BUSINESS);

    }


    private boolean isValid(HashMap<String, String> str) {
        if (str == null || str.size() <= 0) {
            selectIndustryFragment.showToast(ToastType.INDUSTRY_NOT_SELECTED);
            return false;

        }
        return true;
    }


    public void proceed(HashMap<String, String> selectedIndustry) {

        if (isValid(selectedIndustry)) {
            ArrayList<String> arr = new ArrayList<>();
            for (Map.Entry<String, String> entry : selectedIndustry.entrySet()) {
                arr.add(entry.getValue());
            }
            selectIndustryFragment.proceedToNext(arr);
        }
    }

}
