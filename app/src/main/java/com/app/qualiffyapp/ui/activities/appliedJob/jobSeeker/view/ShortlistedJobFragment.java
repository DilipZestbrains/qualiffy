package com.app.qualiffyapp.ui.activities.appliedJob.jobSeeker.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.appliedJob.AppliedJobAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.appliedJob.AppliedJobItemClickListener;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.FragmentAppliedJobBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.appliedJob.jobSeeker.AppliedJobPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.videoCompressUtils.IVideoCompressCallback;
import com.app.qualiffyapp.utils.videoCompressUtils.VideoUtils;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLIED_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_APPLY_JOB_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_VIDEO_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CAMERA_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.HAS_VIDEO;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class ShortlistedJobFragment extends BaseFragment implements SignupView, AppliedJobItemClickListener, IVideoCompressCallback, yesNoCallback {
    private int pageNo = 1;
    private FragmentAppliedJobBinding mBinding;
    private AppliedJobPresenter mPresenter;
    private AppliedJobAdapter mAdapter;
    private String jobId;
    private VideoUtils videoUtils;
    private String videoPath;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;
    private boolean clearList = false;
    private AppliedJobResponseModel.JobBean.OrgInfoBean orgInfoBean;
    private String basePath;
    private String msg;
    private AppFirebaseDatabase firebaseDatabase;
    private DialogInitialize dialogInitialize;


    @Override
    protected void initUi() {
        mBinding = (FragmentAppliedJobBinding) viewDataBinding;
        mPresenter = new AppliedJobPresenter(this);
        dialogInitialize = new DialogInitialize();
        msg = getResources().getString(R.string.no_conmapny_shortlist_you_yet);
        initFirebase();
        getAppliedJob(true);
    }

    private void initFirebase() {
        firebaseDatabase = AppFirebaseDatabase.getInstance();
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.appliedJobRcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.appliedJobRcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    mBinding.progressBar.setVisibility(View.VISIBLE);
                    getAppliedJob(false);
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        mBinding.swipeToRefresh.setOnRefreshListener(this::getAppliedJob);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_applied_job;
    }

    @Override
    public void onClick(View v) {

    }


    private void getAppliedJob(boolean isShowProgress) {
        if (isShowProgress)
            showProgressDialog(true);
        mPresenter.getAppliedJoblist(pageNo, "1");
    }

    private void getAppliedJob() {
        clearList = true;
        mPresenter.getAppliedJoblist(pageNo, "1");
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getRes() != null) {
            switch (apiFlag) {
                case API_FLAG_JS_APPLIED_JOB:
                    AppliedJobResponseModel responseModel = (AppliedJobResponseModel) response.getRes();
                    if (responseModel != null && responseModel.job.size() > 0) {
                        setVisiblity(true);

                        if (mAdapter == null || pageNo==1) {
                            this.basePath = responseModel.bP;
                            mAdapter = new AppliedJobAdapter(responseModel.bP,responseModel.jP, responseModel.job, getViewContext(), this);
                            mBinding.appliedJobRcv.setAdapter(mAdapter);
                        } else {
                            if (clearList) {
                                mAdapter.appliedJobList.clear();
                                clearList = false;
                            }
                            mAdapter.addAll(responseModel.job);
                        }

                        if (responseModel.ttl > PAGINATION_LIMIT) {
                            totalPage = responseModel.ttl / PAGINATION_LIMIT;
                            if (responseModel.ttl % PAGINATION_LIMIT != 0)
                                totalPage = totalPage + 1;
                        }
                        if (pageNo == totalPage) isLastPage = true;
                        isLoading = false;


                    } else {
                        if ( pageNo==1)
                            setVisiblity(false);
                    }
                    break;
                case API_FLAG_JS_APPLY_JOB_VIDEO:
                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel.msg);
                    pageNo = 1;
                    setChatThread();
                    getAppliedJob(false);
                    break;
            case API_FLAG_JS_APPLY_JOB:
                    MsgResponseModel msgResponseModel1 = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel1.msg);
                    pageNo = 1;
                    getAppliedJob(false);
                    break;
            }
        } else validationFailed(response.err.msg);


        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);

        showProgressDialog(false);

    }


    private void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.appliedJobRcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.tvNoDataFound.setText(msg);
            mBinding.appliedJobRcv.setVisibility(View.GONE);
        }
    }

    private void setChatThread() {
        if (orgInfoBean != null && basePath != null) {
            UserConnection user = Utility.getUserConnection(getContext());
            UserConnection userConnection = new UserConnection();
            userConnection.setUid(orgInfoBean.id);
            userConnection.setName(orgInfoBean.name);
            userConnection.setPhone(String.valueOf(orgInfoBean.pno));
            userConnection.setBlocked(BlockStatus.NOT_BLOCKED.value);
            userConnection.setImg(basePath + orgInfoBean.img);
            userConnection.setType(FirebaseUserType.BUSINESS.s);
            userConnection.setTimestamp(user.getTimestamp());
            firebaseDatabase.addFriend(user, userConnection);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }


    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        mBinding.swipeToRefresh.setRefreshing(showProgress);
//        showProgress(showProgress);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case VIDEO_CAMERA_PERMISSION_REQUEST_CODE:
                    SandriosCamera
                            .with()
                            .setMediaAction(CameraConfiguration.MEDIA_ACTION_VIDEO)
                            .showOverlay(true)
                            .launchCamera(this);


             /*   Intent intent = new Intent(context, CameraActivity.class);
                intent.putExtra("isApplyJob", true);
                intent.putExtra("isShareEx", true);
                startActivityForResult(intent, REQUEST_CODE_CAMERA_VIDEO);*/
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Utility.showToast(context, "Returned from setting");
                break;

            case REQUEST_CODE_CAMERA_IMG:
                if (resultCode == Activity.RESULT_OK) {
                    videoPath = data.getStringExtra(MEDIA_PATH);
                    if (jobId != null && !TextUtils.isEmpty(videoPath)) {
                        refreshPage();
                        mPresenter.ApplyJobApi(new File(videoPath), null, jobId);
                    }
//                    videoUtils = new VideoUtils(getContext(), this);
                }
                break;


        }
    }


    @Override
    public void showVideoCompressProgress(boolean isShown) {
        if (isShown)
            showProgressDialog(true);
        else
            showProgressDialog(false);
    }

    @Override
    public void getVideoFile(Bitmap bm, File videoFile) {
        if (jobId != null && videoFile != null) {
            refreshPage();
            mPresenter.ApplyJobApi(videoFile, null, jobId);
        }
    }

    @Override
    public void isCompressSupport(boolean isCompressSupported) {
        if (videoUtils != null) {
            showProgressDialog(true);
            if (isCompressSupported)
                videoUtils.getCompressedVideo(videoPath);
            else videoUtils.getVideoThumbnail(videoPath);

        }

    }

    private void refreshPage() {
        isLastPage = false;
        isLoading = false;
        totalPage = 1;
        mAdapter = null;

    }

    @Override
    public void onjobItemClick(String jobId, AppliedJobResponseModel.JobBean.OrgInfoBean orgInfoBean) {
        this.jobId = jobId;
        this.orgInfoBean = orgInfoBean;
        if (this.jobId != null)
            dialogInitialize.videoUploadOnApplyJob(new CustomDialogs(getContext(), this));
    }

    @Override
    public void onVideoClick(String videoUrl) {

    }

    @Override
    public void onYesClicked(String from) {
        onAskForSomePermission(getContext(), CAMERA_VIDEO_PERMISION, VIDEO_CAMERA_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onNoClicked(String from) {
        if (getContext() != null)
            if (Utility.getBooleanSharedPreference(getContext(), HAS_VIDEO)) {
                if (jobId != null) {
                    mPresenter.ApplyJobApi(null, "1", jobId);
                }
            } else validationFailed(getResources().getString(R.string.add_profile_video));


    }
}

