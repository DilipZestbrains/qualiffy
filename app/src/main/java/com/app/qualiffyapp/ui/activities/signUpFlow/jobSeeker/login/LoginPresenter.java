package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.login;

import android.text.TextUtils;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.Valdations;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_LOGIN;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SOCIAL_SIGNUP;

public class LoginPresenter implements ApiListener<ResponseModel<VerifyResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;
    private Valdations valdations;

    public LoginPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
        valdations = new Valdations();
    }

    @Override
    public void onApiSuccess(ResponseModel<VerifyResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
        else
            view.validationFailed(ApplicationController.getApplicationInstance().getString(R.string.something_went_wrong));
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);

    }


    public void validate(String value) {
        if (isValid(value))
            hitApi(value, API_FLAG_LOGIN);
    }

    private void hitApi(String value, int apiflag) {
        view.showProgressDialog(true);
        mSignUpInteractor.loginApi(this, value,
                Utility.getStringSharedPreference(view.getViewContext(),
                        AppConstants.PREFS_ACCESS_TOKEN), API_FLAG_LOGIN);
    }

    private boolean isValid(String value) {
        if (TextUtils.isEmpty(value)) {
            view.validationFailed(ApplicationController.getApplicationInstance().getString(R.string.mobile_number_is_mandetory));
            return false;
        }
        return true;
    }

    public void sociallogin(RequestBean bean) {
        mSignUpInteractor.socailSignupApi(this, bean, API_FLAG_SOCIAL_SIGNUP);

    }
}