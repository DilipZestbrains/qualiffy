package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.os.Build;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SuggestedListAdapter;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.FragmentChooseIndustryBinding;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ISelectIndustryFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.SelectIndustryPresenter;
import com.app.qualiffyapp.utils.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.COMPANY_LOGO;

public class SelectIndustryFragment extends BLoginSignUpBaseFragment implements ISelectIndustryFragment,TextWatcher, SuggestedListAdapter.OnIndustryClickListener {
    private SelectIndustryPresenter mPresenter;
    private FragmentChooseIndustryBinding mBinding;
    private SuggestedListAdapter filteredIndustry;


    @Override
    protected void initUi() {
        mBinding = (FragmentChooseIndustryBinding) viewDataBinding;
        mPresenter = new SelectIndustryPresenter(this);
        initChip();
        filteredIndustry = new SuggestedListAdapter(new ArrayList<>());
        filteredIndustry.setListener(this);
        mBinding.rvSearchList.setAdapter(filteredIndustry);
        mBinding.tvNote.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mBinding.tvNote.setText(Html.fromHtml(getResources().getString(R.string.note), Html.FROM_HTML_MODE_COMPACT));
        } else {
            mBinding.tvNote.setText(Html.fromHtml(getResources().getString(R.string.note)));
        }

    }

    @Override
    protected void setListener() {
        mBinding.tvNext.setOnClickListener(this);
        mBinding.etSearch.addTextChangedListener(this);

        mBinding.etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mBinding.noIndustryFound.setVisibility(View.GONE);
                String s = mBinding.etSearch.getText().toString();
                if (s.length() > 0) {
                    mPresenter.getIndustryList(s);
                    mBinding.ProgressBar.setVisibility(View.VISIBLE);
                } else
                    filteredIndustry.clearList();

            }
            return false;
        });



    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tvNext) {
            Utility.hideKeyboard(context);
            mPresenter.proceed(getLoginActivity().getSelectedIndustry());
        }
    }


    @Override
    protected int getLayoutById() {
        return R.layout.fragment_choose_industry;
    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.industry);
    }

    @Override
    public void onSearchResult(String message, List<SuggestedListResponseModel> filterList) {
        mBinding.ProgressBar.setVisibility(View.GONE);

        if (!TextUtils.isEmpty(message))
            Utility.showToast(context, message);
        if (filterList.size() > 0) {
            mBinding.noIndustryFound.setVisibility(View.GONE);
            filteredIndustry.refereshList(filterList);
        } else {
            filteredIndustry.clearList();
            mBinding.noIndustryFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void searchResultError() {
        mBinding.ProgressBar.setVisibility(View.GONE);
        mBinding.noIndustryFound.setVisibility(View.VISIBLE);
        Utility.showToast(context, getResources().getString(R.string.something_went_wrong));
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.INDUSTRY_NOT_SELECTED) {
            Utility.showToast(context, getResources().getString(R.string.industry_alert));
        }
    }

    @Override
    public void proceedToNext(ArrayList<String> selectedIndustryList) {
        getLoginActivity().getRequestBean().jobC = selectedIndustryList;
        getLoginActivity().getPresenter().gotoFragment(COMPANY_LOGO, true, true);
    }

    @Override
    public void onIndustryClick(SuggestedListResponseModel responseModel) {
        filteredIndustry.clearList();
        if (getLoginActivity().getSelectedIndustry().size() < 7) {
            if (!getLoginActivity().getSelectedIndustry().containsKey(responseModel.name)) {
                getLoginActivity().getSelectedIndustry().put(responseModel.name, responseModel.id);
                mBinding.etSearch.getText().clear();
                addChip(responseModel.name);
                Utility.hideKeyboard(getContext());
            } else {
                Utility.showToast(context, getResources().getString(R.string.already_industry_selected));
            }
        } else {
            Utility.showToast(context, getResources().getString(R.string.can_not_select_more_than_7_industry));
        }
    }

    private void initChip() {
        mBinding.chipgroup.removeAllViews();
        for (Map.Entry<String, String> entry : getLoginActivity().getSelectedIndustry().entrySet()) {
            addChip(entry.getKey());
        }
    }

    private void addChip(String key) {
        CustomChip chip = new CustomChip(context, key);
        chip.setOnCloseIconClickListener(v -> {
            getLoginActivity().getSelectedIndustry().remove(((CustomChip) v).getText().toString().trim());
            mBinding.chipgroup.removeView(v);
        });
        mBinding.chipgroup.addView(chip);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mBinding.noIndustryFound.setVisibility(View.GONE);
        if (s.length() == 2 || s.length() == 5 || s.length() == 7 || s.length() == 11 || s.length() > 15) {
            mBinding.ProgressBar.setVisibility(View.VISIBLE);
            mPresenter.getIndustryList(s.toString());
        }  else if(s.length()==0){
            if(filteredIndustry!=null)
            filteredIndustry.clearList();
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
