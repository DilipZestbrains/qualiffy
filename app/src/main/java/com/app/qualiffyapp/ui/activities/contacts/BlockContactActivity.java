package com.app.qualiffyapp.ui.activities.contacts;

import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.contact.BlockContactAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.databinding.ActivityRcommendListBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.contact.BlockContactResonseModel;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class BlockContactActivity extends BaseActivity implements SignupView {
    RequestBean requestBean;
    private ActivityRcommendListBinding mBinding;
    private BlockContactPresenter mPresenter;
    private BlockContactAdapter mAdapter;
    private int pageNo = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;

    @Override
    protected void initView() {
        mBinding = (ActivityRcommendListBinding) viewDataBinding;
        mPresenter = new BlockContactPresenter(this);
        setToolbar();
        setListener();

        mPresenter.blockContactListApi(pageNo, true);

    }

    private void setToolbar() {
        mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.block_contact));
        mBinding.toolbar.backBtnImg.setOnClickListener((v) -> onBackPressed());
    }


    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.backBtnImg.setOnClickListener(this);

        mBinding.rcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.rcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    if (requestBean != null) {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        mPresenter.blockContactListApi(pageNo, false);
                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_rcommend_list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            BlockContactResonseModel responseModel = (BlockContactResonseModel) response.getRes();
            if (responseModel != null && responseModel.usr.size() > 0) {
                setVisiblity(true);
                if (mAdapter == null) {
                    mAdapter = new BlockContactAdapter(responseModel.usr, responseModel.bP, this);
                    mBinding.rcv.setAdapter(mAdapter);
                } else {
                    mAdapter.addAll(responseModel.usr);
                }

                if (responseModel.ttl > PAGINATION_LIMIT) {
                    totalPage = responseModel.ttl / PAGINATION_LIMIT;
                    if (responseModel.ttl % PAGINATION_LIMIT != 0)
                        totalPage = totalPage + 1;
                }
                if (pageNo == totalPage) isLastPage = true;
                isLoading = false;

            } else if (mAdapter == null) {
                setVisiblity(false);
            }

        } else
            validationFailed(response.err.msg);

        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);

    }

    void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.rcv.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        validationFailed(error);
        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
