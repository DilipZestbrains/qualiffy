package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

public interface IUpdatableDetail {

    void preUpdate();

    void onUpdate(String msg, int status);
}
