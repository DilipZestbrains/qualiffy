package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.FragmentAddressBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IAddress;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.locationUtils.ConvertLatLngToAddress;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.gson.Gson;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.COUNTRY;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.LAT;
import static com.app.qualiffyapp.constants.AppConstants.LNG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter.AUTOCOMPLETE_REQUEST_CODE;
import static com.facebook.appevents.UserDataStore.STATE;

public class AddressFragment extends BLoginSignUpBaseFragment implements IAddress {
    private FragmentAddressBinding mBinding;
    private AddressPresenter mPresenter;
    private String header;
    @Override
    protected void initUi() {
        mBinding = (FragmentAddressBinding) viewDataBinding;
        mPresenter = new AddressPresenter(this);
        getLoginActivity().enableSkip(false);
        manageAction(getLoginActivity().isUpdate);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_address;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNext:
                mPresenter.proceed(getLoginActivity().getRequestBean());
                break;
            case R.id.etAddress:
                mPresenter.initAddressPickerDialog();
                break;
            case R.id.tvDone:
                mPresenter.updateAddress(getLoginActivity().getRequestBean());
                break;
        }
    }

    @Override
    protected void setListener() {
        mBinding.tvNext.setOnClickListener(this);
        mBinding.etAddress.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
    }

    @Override
    protected String getHeader() {
        return header;
    }

    @Override
    public void onAddressSelect(String address, String country, String state, String lat, String lng) {
        mBinding.etAddress.setText(state);
        getLoginActivity().getRequestBean().cntry =country;
        getLoginActivity().getRequestBean().state = state;
        getLoginActivity().getRequestBean().lat =lat;
        getLoginActivity().getRequestBean().lng = lng;
        getLoginActivity().getRequestBean().address = address;
    }

    @Override
    public void onUpdate(String msg, int Status) {
        getLoginActivity().showProgress(false);
        Utility.showToast(context, msg);
        if (Status == SUCCESS) {
            Intent intent = new Intent();
            ConvertLatLngToAddress.AddressBean addressBean = new ConvertLatLngToAddress.AddressBean();
            addressBean.cntry = getLoginActivity().getRequestBean().cntry;
            addressBean.state = getLoginActivity().getRequestBean().state;
            addressBean.lat = getLoginActivity().getRequestBean().lat;
            addressBean.lng = getLoginActivity().getRequestBean().lng;
            addressBean.addr = getLoginActivity().getRequestBean().addr;
            intent.putExtra(DATA_TO_SEND, new Gson().toJson(addressBean));
            getLoginActivity().setResult(Activity.RESULT_OK, intent);
            getLoginActivity().finish();
        }
    }

    @Override
    public void preUpdate() {
        getLoginActivity().showProgress(true);
    }

    @Override
    public void onNext() {

        getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.COMPANY_DESCRIPTION, true, true);
    }

    @Override
    public void openAutoSelectPlace(Autocomplete.IntentBuilder intent) {
        startActivityForResult(intent.build(context), AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.ADDRESS_EMPTY) {
            Utility.showToast(context, getResources().getString(R.string.address_not_selected));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(intent);
                if (place.getLatLng() != null) {
                    ConvertLatLngToAddress convertLatLngToAddress = new ConvertLatLngToAddress();
                    try {
                        ConvertLatLngToAddress.AddressBean addressBean = convertLatLngToAddress.getAddress(getContext(), place.getLatLng().latitude, place.getLatLng().longitude);
                        String state = addressBean.city + ", " + addressBean.state;
                        onAddressSelect(addressBean.addr, addressBean.cntry, state, addressBean.lat, addressBean.lng);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(intent);
                Log.i(AddressFragment.class.getName(), status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

    private void manageAction(boolean isUpdate) {
        if (isUpdate) {
            mBinding.tvDone.setVisibility(View.VISIBLE);
            mBinding.tvNext.setVisibility(View.GONE);
            Bundle bundle = getLoginActivity().getIntent().getBundleExtra(BUNDLE);
            if (bundle != null) {
                getLoginActivity().getRequestBean().cntry = bundle.getString(COUNTRY, null);
                getLoginActivity().getRequestBean().lat = bundle.getString(LAT, null);
                getLoginActivity().getRequestBean().lng = bundle.getString(LNG, null);
                getLoginActivity().getRequestBean().state = bundle.getString(STATE, null);
                getLoginActivity().getRequestBean().address = bundle.getString(DATA_TO_SEND, null);
                mBinding.etAddress.setText(getLoginActivity().getRequestBean().state);
            }

            mBinding.etAddress.requestFocus();
            header = getResources().getString(R.string.edit_address);
        } else {
            mBinding.tvNext.setVisibility(View.VISIBLE);
            mBinding.tvDone.setVisibility(View.GONE);
            header = getResources().getString(R.string.address);
        }
    }



}
