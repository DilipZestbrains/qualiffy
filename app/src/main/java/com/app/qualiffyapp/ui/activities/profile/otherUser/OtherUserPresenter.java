package com.app.qualiffyapp.ui.activities.profile.otherUser;

import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.OtherUserProfileResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.contacts.BlockContactPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_OTHER_USER_BADGE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OTHER_USER_BADGE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OTHER_USER_PROFILE;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;

public class OtherUserPresenter extends BlockContactPresenter {
    private SignupView signupView;
    ApiListener<ResponseModel<OtherUserProfileResponseModel>> userProfileListener = new ApiListener<ResponseModel<OtherUserProfileResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<OtherUserProfileResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);

        }
    };
    private SignUpInteractor interactor;


    public OtherUserPresenter(SignupView signupView) {
        super(signupView);
        this.signupView = signupView;
        this.interactor = new SignUpInteractor();
    }

    public void getOtherProfile(String userId, int userType) {
        if (userId != null) {
            signupView.showProgressDialog(true);

            if (userType == JOB_SEEKER)
                interactor.jobsSeekerOtherUser(userProfileListener, userId, API_FLAG_JS_OTHER_USER_PROFILE);
            else
                interactor.businessOtherUserProfileApi(userProfileListener, userId, API_FLAG_JS_OTHER_USER_PROFILE);
        }
    }

    public void hitBadgeApi(String userId, int usertype,int key) {
        if (userId != null) {
            signupView.showProgressDialog(true);
            if (usertype == JOB_SEEKER)
                interactor.jobsSeekerOtherBadge(this, userId,key, API_FLAG_JS_OTHER_USER_BADGE);
            else
                interactor.businessOtherUserProfileBadge(this, userId,key ,API_FLAG_BUSINESS_OTHER_USER_BADGE);
        }
    }

    public UserConnection getFireBaseConnection(OtherUserProfileResponseModel profileModel) {
        UserConnection friendConnection = new UserConnection();
        friendConnection.setUid(profileModel.usr.id);
        friendConnection.setName(profileModel.usr.username);
        friendConnection.setPhone(profileModel.usr.pno);
        friendConnection.setImg(profileModel.bP + profileModel.prfl.img);
        friendConnection.setType(FirebaseUserType.JOB_SEEKER.s);
        friendConnection.setBlocked(profileModel.is_block);
        return friendConnection;
    }

    public String setEducation(List<OtherUserProfileResponseModel.EduBean> eduBean) {
        if (eduBean != null && eduBean.size() > 0) {
            String education = "";
            for (OtherUserProfileResponseModel.EduBean edu : eduBean) {
                if (education.equals(""))
                    education = "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + "  " + edu.addr + "\n";
                else
                    education = education + "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + "  " + edu.addr + "\n";
            }
            return education;
        } else return "N/A";
    }

    public String setExpDuration(OtherUserProfileResponseModel.ExpInfoBean expInfo) {
        if (expInfo != null) {
            String duration = expInfo.yrs + "Years";
            return duration;
        } else return "N/A";
    }

    public String setSkill(List<String> list) {
        if (list != null && list.size() > 0) {
            String skillstr = "";
            for (String skill : list) {
                if (skillstr.equals(""))
                    skillstr = skill;
                else
                    skillstr = skillstr + ", " + skill;
            }
            return skillstr;
        } else return "N/A";
    }
    public void addFriend(String userId){
        if(userId!=null) {
         signupView.showProgressDialog(true);
            interactor.jobSekekrAddfrndApi(this, userId, API_FLAG_ADD_FRND);
        }
    }

}
