package com.app.qualiffyapp.ui.fragment;

import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.DrawerAdapter;
import com.app.qualiffyapp.adapter.DrawerItem;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.navDrawer.SimpleItem;
import com.app.qualiffyapp.databinding.FragmentNavigationBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.models.eventBusModel.Profile;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.Arrays;
import java.util.List;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_MY_ACCOUNT;
import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_MY_ACCOUNT;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.LAST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PROFILE_PIC;


public class DrawerFragment extends BaseFragment implements DrawerAdapter.OnItemSelectedListener, View.OnClickListener {
    private FragmentNavigationBinding mFragmentNavigationBinding;
    private String[] screenTitles;
    private Drawable[] screenIcons;
    private DrawerAdapter adapter;
    private int userType;
    private int oldPosition = 0;
    private List<DrawerItem> orgDrawerItemList;


    @Override
    protected void initUi() {
        EventBus.getDefault().register(this);
        mFragmentNavigationBinding = (FragmentNavigationBinding) viewDataBinding;
        userType = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), AppConstants.USER_TYPE);
        screenIcons = loadScreenIcons();
        screenTitles = loadScreenTitles();
        setHeader();

//        mFragmentNavigationBinding.tvUserName.setText(appPreferences.getStringFromSharedPreferce(AppConstants.PREFS_USERNAME));
//        if (!TextUtils.isEmpty(appPreferences.getStringFromSharedPreferce(AppConstants.PREFS_PICs))) {
//            Picasso.with(getActivity())
//                    .load(appPreferences.getStringFromSharedPreferce(AppConstants.PREFS_PICs))
//                    .into(mFragmentNavigationBinding.imgProfile);
//        } else {
//            mFragmentNavigationBinding.imgProfile.setImageDrawable(getResources().getDrawable(R.drawable.place_holder));
//        }

        if (userType == AppConstants.JOB_SEEKER) {
            adapter = new DrawerAdapter(Arrays.asList(
                    createItemFor(AppConstants.JS_POS_HOME).setChecked(true).setItemPos(AppConstants.JS_POS_HOME),
                    createItemFor(JS_POS_MY_ACCOUNT).setItemPos(JS_POS_MY_ACCOUNT),
                    createItemFor(AppConstants.JS_POS_CHAT).setItemPos(AppConstants.JS_POS_CHAT),
                    createItemFor(AppConstants.JS_APPLIED_JOB).setItemPos(AppConstants.JS_APPLIED_JOB),
                    createItemFor(AppConstants.JS_POS_NOTIFICATIONS).setItemPos(AppConstants.JS_POS_NOTIFICATIONS),
                    createItemFor(AppConstants.JS_POS_SUBSCRIPTION).setItemPos(AppConstants.JS_POS_SUBSCRIPTION)
            ));
        } else {
              int  orgType = Utility.getIntFromSharedPreference(getContext(), AppConstants.ORG_TYPE);

          if(orgType==2)
          { orgDrawerItemList=Arrays.asList(
                      createItemFor(AppConstants.BS_POS_HOME).setChecked(true).setItemPos(AppConstants.BS_POS_HOME),
                      createItemFor(BS_POS_MY_ACCOUNT).setItemPos(BS_POS_MY_ACCOUNT),
                      createItemFor(AppConstants.BS_POS_CHAT).setItemPos(AppConstants.BS_POS_CHAT),
                      createItemFor(AppConstants.BS_POS_NOTIFICATIONS).setItemPos(AppConstants.BS_POS_NOTIFICATIONS),
                      createItemFor(AppConstants.BS_POS_ADD_JOB).setItemPos(AppConstants.BS_POS_ADD_JOB),
                      createItemFor(AppConstants.BS_POS_ADD_ADDED_JOB).setItemPos(AppConstants.BS_POS_ADD_ADDED_JOB)
              );
          }else {
              orgDrawerItemList = Arrays.asList(
                      createItemFor(AppConstants.BS_POS_HOME).setChecked(true).setItemPos(AppConstants.BS_POS_HOME),
                      createItemFor(BS_POS_MY_ACCOUNT).setItemPos(BS_POS_MY_ACCOUNT),
                      createItemFor(AppConstants.BS_POS_CHAT).setItemPos(AppConstants.BS_POS_CHAT),
                      createItemFor(AppConstants.BS_POS_NOTIFICATIONS).setItemPos(AppConstants.BS_POS_NOTIFICATIONS),
                      createItemFor(AppConstants.BS_POS_SUBSCRIBE).setItemPos(AppConstants.BS_POS_SUBSCRIBE),
                      createItemFor(AppConstants.BS_POS_ADD_JOB).setItemPos(AppConstants.BS_POS_ADD_JOB),
                      createItemFor(AppConstants.BS_POS_ADD_ADDED_JOB).setItemPos(AppConstants.BS_POS_ADD_ADDED_JOB),
                      createItemFor(AppConstants.BS_POS_PROMOTE).setItemPos(AppConstants.BS_POS_PROMOTE),
                      createItemFor(AppConstants.BS_POS_RECRUITER).setItemPos(AppConstants.BS_POS_RECRUITER)
              );
          }
            adapter = new DrawerAdapter(orgDrawerItemList);
        }

        adapter.setListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        mFragmentNavigationBinding.recyclerDrawerItem.setLayoutManager(linearLayoutManager);
        mFragmentNavigationBinding.recyclerDrawerItem.setAdapter(adapter);
        mFragmentNavigationBinding.relTopProfile.setOnClickListener(this);
    }


    @Override
    protected void setListener() {
        super.setListener();
        mFragmentNavigationBinding.imgProfile.setOnClickListener(v->onProfileCLICK());
    }

    private void onProfileCLICK() {
        if(userType==BUSINESS_USER){
            setItemSelection(BS_POS_MY_ACCOUNT,null);
        }else {
            setItemSelection(JS_POS_MY_ACCOUNT,null);

        }

    }

    private void setHeader() {
        setProfileImage();
        String name = "";
        if (!Utility.getStringSharedPreference(getContext(), FIRST_NAME).equals(""))
            name = Utility.getStringSharedPreference(getContext(), FIRST_NAME);
        if (Utility.getStringSharedPreference(getContext(), LAST_NAME) != null && !Utility.getStringSharedPreference(getContext(), LAST_NAME).equals(""))
            name = Utility.getStringSharedPreference(getContext(), FIRST_NAME) + " " + Utility.getStringSharedPreference(getContext(), LAST_NAME);

        mFragmentNavigationBinding.tvUserName.setText(name);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_navigation;
    }

    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(context, res);
    }

    private void setProfileImage() {
        if (!Utility.getStringSharedPreference(getContext(), PROFILE_PIC).equals("")) ;
        GlideUtil.loadCircularImage(mFragmentNavigationBinding.imgProfile,
                Utility.getStringSharedPreference(getContext(), PROFILE_PIC),
                R.drawable.profile_placeholder
        );
    }


    @Override
    public void onItemSelected(final int position, Bundle bundle) {

        ((DashboardActivity) context).closeDrawer();
        Utility.hideKeyboard(context);
        if (oldPosition != position) {
            oldPosition = position;
            new Handler().postDelayed(() -> {
                if (userType == AppConstants.JOB_SEEKER) {
                    ((DashboardActivity) context).setFragment(position,bundle);
                } else
                    ((DashboardActivity) context).setBusinessFragment(orgDrawerItemList.get(position).getItemPos());
            }, 200);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_top_profile:
                Utility.hideKeyboard(context);
                ((DashboardActivity) context).closeDrawer();
                break;
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(@NonNull Profile profile) {
        AppFirebaseDatabase.getInstance().updateUser(profile.getName(),profile.getImg(),null);
        if (!TextUtils.isEmpty(profile.getImg())) {
            if (profile.getImg().contains("http")) {
                if (getActivity() != null) {
                    Utility.putStringValueInSharedPreference(context, PROFILE_PIC, profile.getImg());

                    GlideUtil.loadCircularImage(mFragmentNavigationBinding.imgProfile, profile.getImg(), R.drawable.user_placeholder);
                }
            }
        }
        if (!TextUtils.isEmpty(profile.getName())) {
            Utility.putStringValueInSharedPreference(context, FIRST_NAME, profile.getName());
            mFragmentNavigationBinding.tvUserName.setText(profile.getName());
        }

    }


    public void setItemSelection(int p, Bundle bundle) {
        int pos = -1;
        if (adapter != null) {
            for (DrawerItem s : adapter.items) {
                if (s.getItemPos() == p) {
                    pos = adapter.items.indexOf(s);
                    break;
                }
            }
            if (pos >= 0)
                adapter.setSelected(pos, bundle);
        }
    }


    private DrawerItem createItemFor(int position) {
        return new SimpleItem(screenIcons[position], screenTitles[position])
                .withIconTint(color(R.color.colorTextBlack))
                .withTextTint(color(R.color.colorTextBlack))
                .withSelectedIconTint(color(R.color.progress_yellow))
                .withSelectedTextTint(color(R.color.progress_yellow));
    }

    private String[] loadScreenTitles() {
        if (userType == AppConstants.JOB_SEEKER)
            return getResources().getStringArray(R.array.js_drawer_item);
        else
            return getResources().getStringArray(R.array.bs_drawer_item);

    }

    private Drawable[] loadScreenIcons() {
        TypedArray ta;
        if (userType == AppConstants.JOB_SEEKER)
            ta = getResources().obtainTypedArray(R.array.js_drawer_image);
        else
            ta = getResources().obtainTypedArray(R.array.bs_drawer_image);

        Drawable[] icons = new Drawable[ta.length()];
        for (int i = 0; i < ta.length(); i++) {
            int id = ta.getResourceId(i, 0);
            if (id != 0) {
                icons[i] = ContextCompat.getDrawable(context, id);
            }
        }
        ta.recycle();
        return icons;
    }


}

