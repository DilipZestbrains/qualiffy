package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.ExperienceListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.databinding.ActivityAddExperienceBinding;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.ExperiencePresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_3;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_ADD_EXPERIENCE_DETAIL;

public class AddExperienceActivity extends BaseActivity implements SignupView, RecyclerItemClick, View.OnTouchListener {
    public final String EXPERIRNCE = "experience";
    public final String POSTION = "position";
    public boolean isJobSeekerUpdate;
    private ActivityAddExperienceBinding experienceBinding;
    private ExperiencePresenter mPresenter;
    private ExperienceListAdapter listAdapter;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private ExpInfoBean expInfo;
    private List<ExpDetailBean> expList;


    @Override
    protected void initView() {
        experienceBinding = (ActivityAddExperienceBinding) viewDataBinding;
        mPresenter = new ExperiencePresenter(this);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        experienceBinding.searchRlayout.setVisibility(GONE);
        experienceBinding.edtSearch.setVisibility(View.VISIBLE);
        experienceBinding.ttlExpLlayout.setVisibility(View.VISIBLE);
        experienceBinding.addImg.setVisibility(GONE);
        expList = new ArrayList<>();
        setSpinnerData();
        getIntentData();
        setAdapter();
    }

    private void setAdapter() {
        listAdapter = new ExperienceListAdapter(expList, this);
        experienceBinding.expRcv.setAdapter(listAdapter);
    }

    private void setSpinnerData() {

        experienceBinding.ttlExpLlayout.setVisibility(View.VISIBLE);
        ArrayList<String> yrs = new ArrayList<>();
        for (int j = 0; j < 51; j++) {
            yrs.add(String.valueOf(j));
        }
        ArrayAdapter<String> yrsArrayAdapter = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, yrs);
        experienceBinding.yearsSpn.setAdapter(yrsArrayAdapter);
        mPresenter.setHeighofSpinner(experienceBinding.yearsSpn);
        mPresenter.setHeighofSpinner(experienceBinding.monthsSpn);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            expInfo = new Gson().fromJson(bundle.getString(EXPERIRNCE), ExpInfoBean.class);
            if (expInfo != null) {
                experienceBinding.yearsSpn.setSelection(expInfo.yrs);
                expList.addAll(expInfo.exps);
                isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
                toolbar(true);
            }
        } else {
            toolbar(false);

        }

    }


    private void toolbar(boolean isUpdate) {
        experienceBinding.toolbar.backBtnImg.setOnClickListener(this);
        experienceBinding.titleTxt.setText(getResources().getString(R.string.add_exp_title));
        if (isUpdate) {
            experienceBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_experience));
            experienceBinding.expBtn.btn.setText(getResources().getString(R.string.DONE));
        } else {
            experienceBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
            experienceBinding.expBtn.btn.setText(getResources().getString(R.string.NEXT));
        }
    }

    protected void setListener() {
        experienceBinding.toolbar.backBtnImg.setOnClickListener(this);
        experienceBinding.expBtn.btn.setOnClickListener(this);
        experienceBinding.toolbar.skipTxt.setOnClickListener(this);
        experienceBinding.yearsSpn.setOnTouchListener(this);
        experienceBinding.monthsSpn.setOnTouchListener(this);
        experienceBinding.edtSearch.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_add_experience;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.exp_btn:
                if (expList == null || expList.size() == 0) {
                    validationFailed(getResources().getString(R.string.empty_exp_alert));
                    return;
                }
                profileInputModel.exps = expList;
                if (isJobSeekerUpdate) {
                    profileInputModel.yrs = experienceBinding.yearsSpn.getSelectedItem().toString();
                    showProgressDialog(true);
                    mPresenter.hitApi(profileInputModel, API_FLAG_SIGNUP_3);
                } else {
                    if (profileInputModel.isExpChange) {
                        profileInputModel.yrs = experienceBinding.yearsSpn.getSelectedItem().toString();
                        showProgressDialog(true);
                        mPresenter.hitApi(profileInputModel, API_FLAG_SIGNUP_3);

                    } else {
                        startActivity(new Intent(this, AddCertificateActivity.class));
                    }

                }
                break;

            case R.id.edtSearch:
                if ((expList == null) || (expList.size() < 3)) {
                    profileInputModel.isExpChange = true;
                    openActivityForResult(ExperienceDetailActivity.class, getIntent().getBundleExtra(BUNDLE), REQUEST_CODE_ADD_EXPERIENCE_DETAIL);
                } else if (expList.size() >= 3) {
                    Utility.showToast(this, getResources().getString(R.string.exp_limit_alert));
                }

                break;
            case R.id.skip_txt:
                startActivity(new Intent(this, AddCertificateActivity.class));
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_ADD_EXPERIENCE_DETAIL:

                    if (data != null) {
                        ExpDetailBean expData = new Gson().fromJson(data.getStringExtra("exp"), ExpDetailBean.class);
                        String pos = data.getStringExtra(POSTION);
                        if (expList != null) {
                            if (expData != null && expData.noJobHist) {
                                expList.clear();
                                expList.add(expData);
                                experienceBinding.ttlExpLlayout.setVisibility(GONE);
                                experienceBinding.monthsSpn.setSelection(0);

                            } else {
                                if (TextUtils.isEmpty(pos))
                                    expList.add(expData);
                                else expList.set(Integer.parseInt(pos), expData);
                                int index = -1;
                                for (ExpDetailBean bean : expList) {
                                    if (bean.noJobHist) {
                                        index = expList.indexOf(bean);
                                        break;
                                    }

                                }
                                if (index >= 0) {
                                    expList.remove(index);
                                    index = -1;
                                }
                                experienceBinding.ttlExpLlayout.setVisibility(View.VISIBLE);

                            }
                        }

//                        if (pos != null && !TextUtils.isEmpty(pos) && expData != null)
//                            expList.set(Integer.parseInt(pos), expData);
//                        else {
//                            if (expData != null) {
//                                int index = -1;
//                                for (ExpDetailBean bean : expList) {
//
//                                    if (bean.isEqual(expData)) {
//                                        index = expList.indexOf(bean);
//                                            expList.set(index, expData);
//                                        break;
//                                    }
//                                }
////
//                                if (index == -1)
//                                    expList.add(expData);
//                            }
//                        }
                        listAdapter.notifyDataSetChanged();
                    }
                    break;
            }

        }

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        profileInputModel.isExpChange = false;
        if (response.getStatus() == 1) {
            MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
            if (isJobSeekerUpdate) {
                Intent returnIntent = new Intent();
                expInfo.exps = profileInputModel.exps;
                expInfo.yrs = Integer.parseInt(profileInputModel.yrs);
                returnIntent.putExtra(EXPERIRNCE, new Gson().toJson(expInfo));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else
                startActivity(new Intent(this, AddCertificateActivity.class));

        } else {
            Utility.showToast(this, response.err.msg);
        }

        showProgressDialog(false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onClick(int flag, int pos) {
        switch (flag) {
            case DELETE_FLAG:
                if (expList.size() > 1) {
                    profileInputModel.isExpChange = true;
                    expList.remove(pos);
                    listAdapter.notifyDataSetChanged();
                } else Utility.showToast(this, getResources().getString(R.string.exp_mandatory));
                break;
            case EDIT_FLAG:

                Bundle bundle = new Bundle();
                bundle.putString(EXPERIRNCE, new Gson().toJson(expList.get(pos)));
                bundle.putString(POSTION, String.valueOf(pos));
                bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
                openActivityForResult(ExperienceDetailActivity.class, bundle, REQUEST_CODE_ADD_EXPERIENCE_DETAIL);
                profileInputModel.isExpChange = true;
            /*    expList.remove(pos);
                listAdapter.notifyDataSetChanged();*/


                break;
        }
        experienceBinding.expRcv.getAdapter().notifyDataSetChanged();

    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Utility.hideKeyboard(this);
        profileInputModel.isExpChange = true;
        return false;
    }
}
