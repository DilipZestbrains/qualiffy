package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessLogo;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import java.io.File;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_PROFILE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.TRUE;

public class BusinessLogoPresenter {
    private final SignUpInteractor interactor;
    private IBusinessLogo businessLogo;
    private ApiListener<ResponseModel<AddMediaResponseModel>> msgApiListener = new ApiListener<ResponseModel<AddMediaResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<AddMediaResponseModel> response, int apiFlag) {
            if (response.getStatus() == SUCCESS) {
                businessLogo.onUpdate(response.getRes().msg, response.getRes());
            } else if (response.err != null) {
                businessLogo.onUpdate(response.err.msg, response.getRes());
            } else businessLogo.onUpdate(null, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessLogo.onUpdate(error, null);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            businessLogo.onUpdate(null, null);
        }
    };


    public BusinessLogoPresenter(IBusinessLogo businessLogo) {
        this.businessLogo = businessLogo;
        interactor = new SignUpInteractor();
    }

    public void proceed(String imgUrl) {
        if (isValid(imgUrl)) {
            businessLogo.onNext();
        }
    }

    private boolean isValid(String url) {
        if (url == null || TextUtils.isEmpty(url)) {
            businessLogo.showToast(ToastType.EMAIL_EMPTY);
            return false;
        }
        return true;
    }

    public void update(String imgUrl) {
        if (isValid(imgUrl)) {
            businessLogo.preUpdate();
            File file = new File(imgUrl);
            RequestBean requestBean = new RequestBean();
            requestBean.isProfile = TRUE;
            interactor.addMediaOrgApi(msgApiListener, file, requestBean, API_FLAG_BUSINESS_PROFILE);
        }
    }
}
