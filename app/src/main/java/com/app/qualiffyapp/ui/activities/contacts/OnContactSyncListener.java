package com.app.qualiffyapp.ui.activities.contacts;

public interface OnContactSyncListener {
    void onContactSync();
}
