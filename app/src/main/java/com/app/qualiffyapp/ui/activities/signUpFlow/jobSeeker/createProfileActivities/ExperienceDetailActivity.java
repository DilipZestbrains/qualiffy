package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityAddExperienceFormBinding;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.ExperiencePresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_3;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.MONTH_YEAR_DATE_FORMAT;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ExperienceDetailActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, SignupView {
    public final String EXPERIRNCE = "experience";
    public final String POSTION = "position";
    public boolean isJobSeekerUpdate;
    private ActivityAddExperienceFormBinding mBinding;
    private CreateJobSeekerProfileInputModel profileInputModel;

    private ExpDetailBean expInfo;
    private DatePickerDialog datePicker;
    private ExperiencePresenter mPresenter;

    private String pos;

    @Override
    protected void initView() {
        mBinding = (ActivityAddExperienceFormBinding) viewDataBinding;
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        mPresenter = new ExperiencePresenter(this);

        getIntentData();
    }


    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {

            pos = bundle.getString(POSTION, "");
            expInfo = new Gson().fromJson(bundle.getString(EXPERIRNCE), ExpDetailBean.class);
            if (expInfo != null) {
                setData(expInfo);
                isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
                toolbar(true);
            }
        } else {
            toolbar(false);

        }

    }

    private void setData(ExpDetailBean expInfo) {
        if (expInfo.noJobHist) {
            mBinding.noJobHistorySwitch.setChecked(expInfo.noJobHist);
            mBinding.jobDescription.setVisibility(View.GONE);
        } else {
            mBinding.jobDescription.setVisibility(View.VISIBLE);
            mBinding.swtCurrentWorking.setChecked(expInfo.isWrkng);
            mBinding.tvEndDate.setEnabled(!expInfo.isWrkng);
            if (expInfo.title != null && !TextUtils.isEmpty(expInfo.title))
                mBinding.edtJbTitle.setText(expInfo.title);
            if (expInfo.cName != null && !TextUtils.isEmpty(expInfo.cName))
                mBinding.etCompanyName.setText(expInfo.cName);
            if (expInfo.from != null && !TextUtils.isEmpty(expInfo.from))
                mBinding.tvStartDate.setText(expInfo.from);
            if (expInfo.to != null && !TextUtils.isEmpty(expInfo.to))
                mBinding.tvEndDate.setText(expInfo.to);
            if (expInfo.desc != null && !TextUtils.isEmpty(expInfo.desc))
                mBinding.etDescription.setText(expInfo.desc);

        }
    }


    private void initDatePicker(TextView textView, String date, boolean isMinTime, boolean isStartDate) {
        Calendar newCalendar = Calendar.getInstance();

        MonthYearPickerDialogFragment dialogFragment = MonthYearPickerDialogFragment
                .getInstance(newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.YEAR), -1L, System.currentTimeMillis());

        dialogFragment.show(getSupportFragmentManager(), null);

        dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int year, int monthOfYear) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(Calendar.YEAR, year);
                newDate.set(Calendar.MONTH, monthOfYear);
                textView.setText(Utility.getUtcDate(newDate.getTimeInMillis(), MONTH_YEAR_DATE_FORMAT));
                if (!TextUtils.isEmpty(mBinding.tvEndDate.getText().toString()) && !TextUtils.isEmpty(mBinding.tvStartDate.getText().toString())) {

                    if ((Utility.convertDateToMillies(mBinding.tvStartDate.getText().toString(), MONTH_YEAR_DATE_FORMAT)) >= (Utility.convertDateToMillies(mBinding.tvEndDate.getText().toString(), MONTH_YEAR_DATE_FORMAT))) {
                        mBinding.tvEndDate.setText("");
                        Utility.showToast(ExperienceDetailActivity.this, "End Date should be greater than Start Date");
                    }

                }
            }
        });
    }


    private void toolbar(boolean isUpdate) {
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
        mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.add_experience));
        mBinding.doneBtn.setText(getResources().getString(R.string.add));
       /* if (isUpdate) {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.add_experience));
            mBinding.doneBtn.btn.setText(getResources().getString(R.string.DONE));
        } else {
            mBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.add_experience));
            mBinding.doneBtn.btn.setText(getResources().getString(R.string.add));
        }*/
    }

    protected void setListener() {
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
        mBinding.doneBtn.setOnClickListener(this);
        mBinding.tvStartDate.setOnClickListener(this);
        mBinding.tvEndDate.setOnClickListener(this);
        mBinding.swtCurrentWorking.setOnCheckedChangeListener(this);
        mBinding.noJobHistorySwitch.setOnCheckedChangeListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_add_experience_form;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
          /*      if (expInfo != null) {
                    Intent intent = new Intent();
                    intent.putExtra("exp", new Gson().toJson(expInfo));
                    setResult(RESULT_OK, intent);
                    finish();
                } else onBackPressed();*/
                break;
            case R.id.doneBtn:
                if (isValid()) {
                    ExpDetailBean bean = isValidate();
                        if (bean != null) {
                            Intent intent = new Intent();
                            intent.putExtra("exp", new Gson().toJson(bean));
                            intent.putExtra(POSTION, pos);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                }
                break;
            case R.id.tvStartDate:
                initDatePicker(mBinding.tvStartDate, null, false, true);

                break;

            case R.id.tvEndDate:
                if (TextUtils.isEmpty(mBinding.tvStartDate.getText().toString())) {
                    Utility.showToast(this, "Please add start Date");
                    return;
                }
                initDatePicker(mBinding.tvEndDate, mBinding.tvStartDate.getText().toString(), true, false);
                break;


        }

    }

    private boolean isValid() {
        if (mBinding.noJobHistorySwitch.isChecked()) {
            return true;
        } else {
            if (mBinding.edtJbTitle.getText() == null || TextUtils.isEmpty(mBinding.edtJbTitle.getText().toString())) {
                Utility.showToast(this, "Please enter job tilte.");
                return false;
            }
            if (mBinding.tvStartDate.getText() != null && !TextUtils.isEmpty(mBinding.tvStartDate.getText().toString())) {
                if ((mBinding.tvEndDate.getText() == null || TextUtils.isEmpty(mBinding.tvEndDate.getText().toString())) && !mBinding.swtCurrentWorking.isChecked()) {
                    Utility.showToast(this, "Please enter end date.");
                    return false;
                } else return true;
            }

            return true;
        }
    }

    private ExpDetailBean isValidate() {
        ExpDetailBean bean = new ExpDetailBean();
        if (mBinding.noJobHistorySwitch.isChecked()) {
            bean.noJobHist = mBinding.noJobHistorySwitch.isChecked();
            bean.title = "Fresher";
        } else {
            if (TextUtils.isEmpty(mBinding.edtJbTitle.getText().toString())) {
                Utility.showToast(this, getResources().getString(R.string.add_job_title));
                return null;
            }
            bean.isWrkng = mBinding.swtCurrentWorking.isChecked();
            bean.title = mBinding.edtJbTitle.getText().toString();
            bean.from = mBinding.tvStartDate.getText().toString();
            bean.to = mBinding.tvEndDate.getText().toString();
            bean.cName = mBinding.etCompanyName.getText().toString();
            bean.desc = mBinding.etDescription.getText().toString();

        }
        return bean;

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.swtCurrentWorking) {
            if (isChecked) {
                mBinding.tvEndDate.setText("");
                mBinding.tvEndDate.setEnabled(false);
            } else {
                mBinding.tvEndDate.setEnabled(true);
            }
        } else if (buttonView.getId() == R.id.noJobHistorySwitch) {
            if (isChecked)
                mBinding.jobDescription.setVisibility(View.GONE);
            else mBinding.jobDescription.setVisibility(View.VISIBLE);

            if (!isJobSeekerUpdate) {
                ExpDetailBean detailBean=isValidate();
                List<ExpDetailBean> detail=new ArrayList<>();
                detail.add(detailBean);
                profileInputModel.exps=detail;
                profileInputModel.is_rem=false;
                showProgressDialog(true);
                mPresenter.hitApi(profileInputModel, API_FLAG_SIGNUP_3);
            }


        }

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        switch (apiFlag) {
            case API_FLAG_SIGNUP_3:
                if(response.getStatus()==SUCCESS){
                    startActivity(new Intent(this, AddCertificateActivity.class));
                    finish();
                }else validationFailed(response.err.msg);
                break;
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
