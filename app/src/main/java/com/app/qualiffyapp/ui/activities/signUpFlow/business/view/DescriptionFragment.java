package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.FragmentBriefDescriptionBinding;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IDescriptionFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessDescriptionPresenter;
import com.app.qualiffyapp.utils.Utility;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class DescriptionFragment extends BLoginSignUpBaseFragment implements IDescriptionFragment {
    private FragmentBriefDescriptionBinding mBinding;
    private BusinessDescriptionPresenter mPresenter;
    private String header;

    @Override
    protected void initUi() {
        mBinding = (FragmentBriefDescriptionBinding) viewDataBinding;
        mPresenter = new BusinessDescriptionPresenter(this);
        manageAction(getLoginActivity().isUpdate);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_brief_description;
    }

    @Override
    public void onClick(View v) {
        Utility.hideKeyboard(context);
        switch (v.getId()) {
            case R.id.tvRegister:
                if (mBinding.etDescription.getText() != null)
                    getLoginActivity().getRequestBean().bio = mBinding.etDescription.getText().toString();
                mPresenter.registerBusiness(getLoginActivity().getRequestBean());
                break;
            case R.id.tvDone:
                if (mBinding.etDescription.getText() != null)
                    mPresenter.updateCompanyDescription(mBinding.etDescription.getText().toString());
        }

    }

    @Override
    protected void setListener() {
        mBinding.tvRegister.setOnClickListener(this);
        mBinding.tvDone.setOnClickListener(this);
    }

    @Override
    protected String getHeader() {
        return header;
    }


    @Override
    public void onRegister(VerifyResponseModel response) {
        showProgress(false);
        Utility.saveProfile(context, response, true);
        Utility.showToast(context, response.msg);
        getLoginActivity().finishAffinity();
        getLoginActivity().openActivity(DashboardActivity.class, null);
    }

    @Override
    public void showToast(String msg) {
        showProgress(false);
        Utility.showToast(context, msg);

    }

//    private void saveName(VerifyResponseModel verifyResponseModel) {
//        Utility.saveProfile(context, verifyResponseModel, true);
////        if (verifyResponseModel.username != null && !verifyResponseModel.username.equals(""))
////            Utility.putStringValueInSharedPreference(getContext(), FIRST_NAME, verifyResponseModel.username);
////        Utility.putStringValueInSharedPreference(getContext(), LAST_NAME, "");
////
////        if (verifyResponseModel.img != null && !verifyResponseModel.img.equals(""))
////            Utility.putStringValueInSharedPreference(getContext(), PROFILE_PIC, verifyResponseModel.img);
//    }


    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(String msg, int status) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (status == SUCCESS) {
            Intent intent = new Intent();
            if (mBinding.etDescription.getText() != null)
                intent.putExtra(DATA_TO_SEND, mBinding.etDescription.getText().toString());
            getLoginActivity().setResult(Activity.RESULT_OK, intent);
            getLoginActivity().finish();
        }
    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.COMPANY_DESCRIPTION_EMPTY) {
            Utility.showToast(context, getResources().getString(R.string.please_add_company_description));
        }
    }

    private void manageAction(boolean isUpdate) {
        if (isUpdate) {
            mBinding.tvDone.setVisibility(View.VISIBLE);
            mBinding.tvRegister.setVisibility(View.GONE);


            Bundle bundle = getLoginActivity().getIntent().getBundleExtra(BUNDLE);
            if (bundle != null && bundle.getString(DATA_TO_SEND) != null && !TextUtils.isEmpty(bundle.getString(DATA_TO_SEND))) {
                mBinding.etDescription.setText(bundle.getString(DATA_TO_SEND, ""));
                mBinding.etDescription.requestFocus();
            } else
                mBinding.etDescription.setHint(getResources().getString(R.string.write_a_brief_description));
/*
            mBinding.etDescription.setText(getLoginActivity().getIntent().getBundleExtra(BUNDLE).getString(DATA_TO_SEND, ""));
            mBinding.etDescription.requestFocus();*/
            header = getResources().getString(R.string.edit_description);
        } else {
            mBinding.tvRegister.setVisibility(View.VISIBLE);
            mBinding.tvDone.setVisibility(View.GONE);
            header = getResources().getString(R.string.description);
            mBinding.etDescription.setHint(getResources().getString(R.string.write_a_brief_description));
        }
    }

}
