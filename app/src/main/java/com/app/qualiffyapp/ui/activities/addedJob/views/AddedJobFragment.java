package com.app.qualiffyapp.ui.activities.addedJob.views;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.addedJob.AddedJobAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationJobItemClick;
import com.app.qualiffyapp.databinding.FragmentAddedJobBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.ui.activities.addedJob.presenter.AddedJobPresenter;
import com.app.qualiffyapp.utils.EndlessRecyclerViewScrollListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADDED_JOB;
import static com.app.qualiffyapp.constants.AppConstants.DESC;
import static com.app.qualiffyapp.constants.AppConstants.JOB_ID;
import static com.app.qualiffyapp.constants.AppConstants.JS_JOB_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class AddedJobFragment extends BaseFragment implements SignupView, IOrganizationJobItemClick {
    private FragmentAddedJobBinding mBinding;
    private GridLayoutManager gridLayoutManager;
    private AddedJobAdapter mAdapter;
    private AddedJobPresenter joblistPresenter;
    private int pageNo = 1;
   private List<AddedJobResponseModel.JobBean> jobList;
    private int totalItem;



    @Override
    protected void initUi() {
        mBinding = (FragmentAddedJobBinding) viewDataBinding;
        joblistPresenter = new AddedJobPresenter(this);
        initImgGrid();
        hitApi(API_FLAG_BUSINESS_ADDED_JOB);
    }


    private void initImgGrid() {
        gridLayoutManager = new GridLayoutManager(getContext(), 3);
        mBinding.rvJob.setLayoutManager(gridLayoutManager);

        mBinding.rvJob.setOnScrollListener(new EndlessRecyclerViewScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if(totalItemsCount<totalItem) {
                    pageNo = page;
                    hitApi(API_FLAG_BUSINESS_ADDED_JOB);
                }


            }

        });
    }


    private void hitApi(int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_BUSINESS_ADDED_JOB:
                showProgress(true);
                joblistPresenter.getJobList(pageNo);
                break;
        }

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_added_job;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            switch (apiFlag) {
                case API_FLAG_BUSINESS_ADDED_JOB:
                    AddedJobResponseModel responseModel = (AddedJobResponseModel) response.getRes();
                    if (responseModel.job != null && responseModel.job.size() > 0) {
                        setVisiblity(true);
                        if (pageNo == 1) {
                            jobList = responseModel.job;
                            mAdapter = new AddedJobAdapter(getContext(), jobList, this);
                            mBinding.rvJob.setAdapter(mAdapter);
                        } else {
                            if (jobList == null)
                                jobList = responseModel.job;
                            else jobList.addAll(responseModel.job);

                            mAdapter.notifyDataSetChanged();
                        }
                        totalItem=Integer.parseInt(responseModel.ttl);
                        mBinding.tvJobCount.setText(responseModel.ttl+ " " +
                                Utility.singularPluralText(context.getResources().getString(R.string.job),
                                        Integer.parseInt(responseModel.ttl)) + " " +
                                getResources().getString(R.string.job_available));

                    } else {
                        mBinding.tvJobCount.setText("0"+ " " +
                                Utility.singularPluralText(context.getResources().getString(R.string.job),
                                        0) + " " +
                                getResources().getString(R.string.job_available));
                        setVisiblity(false);
                    }
                    break;
            }
        } else validationFailed(response.err.msg);

        showProgressDialog(false);


    }

    private void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rvJob.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.rvJob.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgress(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getViewContext();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onItemClick(int pos, String jobId, String jobName, String desc) {
        if (jobId != null) {
            Bundle bundle = new Bundle();
            bundle.putString(JOB_ID, jobId);
            bundle.putString(JS_JOB_TYPE, jobName);
            bundle.putString(DESC, desc);
            openActivityForResult(ActivityApplicants.class, bundle,101);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==101)
        {
            if(data !=null && data.getStringExtra("deleteJob")!=null) {
                showProgress(true);
                pageNo=1;
                joblistPresenter.getJobList(pageNo);

            }
        }


    }
}
