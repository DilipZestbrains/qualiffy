package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.FragmentVerifyOtpBinding;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IVerifyFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.PresenterVerifyFragmnet;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.COMPANY_NAME;

public class VerifyFragment extends BLoginSignUpBaseFragment implements IVerifyFragment {

    private FragmentVerifyOtpBinding mBinding;
    private PresenterVerifyFragmnet mPresenter;

    @Override
    protected void initUi() {
        mBinding = (FragmentVerifyOtpBinding) viewDataBinding;
        mPresenter = new PresenterVerifyFragmnet(this);
        mBinding.resendOtpTxt.setEnabled(false);
        mPresenter.startTick();

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_verify_otp;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvVerify:
                if (getLoginActivity() != null) {
                    Utility.hideKeyboard(context);
//                    mPresenter.verifyOtp(getLoginActivity().orgID, mBinding.otpEdtPhone.getText().toString(), mBinding.otpEdtEmail.getText().toString());
                }
                break;
            case R.id.resend_otp_txt:
                if (getLoginActivity() != null) {
                    Utility.hideKeyboard(context);
//                    mPresenter.resendOtp(getLoginActivity().orgID);
                }
                break;
        }
    }

    @Override
    protected void setListener() {
        mBinding.resendOtpTxt.setOnClickListener(this);
        mBinding.tvVerify.setOnClickListener(this);
    }

    @Override
    public void updateTime(String time) {
        if (getLoginActivity() != null) {
            String timer = getLoginActivity().getResources().getString(R.string.otp_verify) + time;
            mBinding.timerTxt.setText(timer);
        }
    }

    @Override
    public void onTimeCompletion() {
        mBinding.resendOtpTxt.setEnabled(true);
//        mBinding.tvVerify.setEnabled(false);
    }

    @Override
    public void onOtpVerification(String msg, VerifyResponseModel model) {
        getLoginActivity().showProgress(false);
        Utility.showToast(context, msg);
        mBinding.otpEdtPhone.getText().clear();
        mBinding.otpEdtEmail.getText().clear();
        if (model != null) {
            Utility.putStringValueInSharedPreference(context, LOGIN_ACCESS_TOKEN, model.acsTkn);

            if (getLoginActivity().getRequestBean().self != null) {
                getLoginActivity().finish();
                getLoginActivity().openActivity(LoginActivity.class, null);
            } else {
                getLoginActivity().getPresenter().gotoFragment(COMPANY_NAME, true, true);
            }
        }
    }

    @Override
    public void onOtpResend(String msg, SingupResponse model) {
        getLoginActivity().showProgress(false);
        mBinding.resendOtpTxt.setEnabled(false);
        mPresenter.startTick();
        Utility.showToast(context, msg);
    }

    @Override
    public void showToast(ToastType type) {
        switch (type) {
            case EMAIL_OTP_EMPTY:
                Utility.showToast(context, getResources().getString(R.string.otp_length_email));
                break;
            case MOBILE_OTP_EMPTY:
                Utility.showToast(context, getResources().getString(R.string.otp_length_mobile));
                break;
            case MOBILE_OTP_NOT_COMPLETE:
                Utility.showToast(context, getResources().getString(R.string.empty_otp));
                break;
            case EMAIL_OTP_NOT_COMPLETE:
                Utility.showToast(context, getResources().getString(R.string.empty_otp));
        }
    }

    @Override
    public void preOtpVerification() {
        getLoginActivity().showProgress(true);
    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.VERIFY_TITLE);
    }
}
