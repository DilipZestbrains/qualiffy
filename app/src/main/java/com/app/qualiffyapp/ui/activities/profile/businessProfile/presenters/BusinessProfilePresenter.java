package com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters;


import android.os.Bundle;
import android.text.TextUtils;

import com.app.qualiffyapp.adapter.ImagesAdapter;
import com.app.qualiffyapp.constants.CompanyPortfolioType;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.models.profile.NotifBean;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.profile.ManageNotificationActivity;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces.IBusinessProfileInterface;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.locationUtils.ConvertLatLngToAddress;
import com.google.gson.Gson;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_LOGOUT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_PROFILE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_TOGGLE;
import static com.app.qualiffyapp.constants.AppConstants.ADDRESS_UPDATE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.COMPANY_NAME_UPDATE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.COMPANY_STRENGTH;
import static com.app.qualiffyapp.constants.AppConstants.COUNTRY;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.DESCRIPTION_UPDATE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.FRAGMENT;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.IMAGES;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_BASE_URL;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.LAT;
import static com.app.qualiffyapp.constants.AppConstants.LNG;
import static com.app.qualiffyapp.constants.AppConstants.NEED_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment.NOTIFICATION_REQUEST_CODE;
import static com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment.REGISTRATION_NUMBER_UPDATE_REQUEST_CODE;
import static com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment.REQUEST_CODE_UPDATE_IMAGES;
import static com.app.qualiffyapp.ui.activities.profile.businessProfile.view.BusinessProfileFragment.UPDATE_BUSINESS_LOGO;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.SOMETHING_WENT_WRONG;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity.MEDIA;
import static com.facebook.appevents.UserDataStore.STATE;

public class BusinessProfilePresenter implements ImagesAdapter.AddImageListener {

    private final IBusinessProfileInterface businessInterface;
    private SignUpInteractor interactor;

    private ApiListener<BusinessProfileResponseModel> businessProfileApiListener = new ApiListener<BusinessProfileResponseModel>() {
        @Override
        public void onApiSuccess(BusinessProfileResponseModel response, int apiFlag) {
            businessInterface.onFetchProfile(response.msg, response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessInterface.onFetchProfile(error, null);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            businessInterface.onFetchProfile(null, null);

        }
    };

    private ApiListener<ResponseModel<MsgResponseModel>> msgApiListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response.getRes() != null) {
                switch (apiFlag) {
                    case API_FLAG_BUSINESS_TOGGLE:
                        businessInterface.profileVisibility(response.getRes().msg, SUCCESS);
                        break;
                    case API_FLAG_BUSINESS_DELETE:
                        businessInterface.onDelete(response.getRes().msg, SUCCESS, apiFlag);
                        break;
                    case API_FLAG_BUSINESS_LOGOUT:
                        businessInterface.onDelete(response.getRes().msg, SUCCESS, apiFlag);
                        break;
                }
            } else {
                if (apiFlag == API_FLAG_BUSINESS_LOGOUT && response.err.errCode == 2)
                    businessInterface.onDelete(response.err.msg, SUCCESS, apiFlag);
                else if (apiFlag == API_FLAG_BUSINESS_TOGGLE)
                    onApiError(response.err.msg, apiFlag);

            }

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            switch (apiFlag) {
                case API_FLAG_BUSINESS_TOGGLE:
                    businessInterface.profileVisibility(error, FAILURE);
                    break;
                case API_FLAG_BUSINESS_DELETE:
                    businessInterface.onDelete(error, FAILURE, apiFlag);
                    break;
                case API_FLAG_BUSINESS_LOGOUT:
                    businessInterface.onDelete(error, FAILURE, apiFlag);
                    break;
            }
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            switch (apiFlag) {
                case API_FLAG_BUSINESS_TOGGLE:
                    businessInterface.profileVisibility(null, -12);
                    break;
                case API_FLAG_BUSINESS_DELETE:
                    businessInterface.onDelete(null, -12,apiFlag);
                    break;
                case API_FLAG_BUSINESS_LOGOUT:
                    businessInterface.onDelete(null, -12,apiFlag);
                    break;
            }
        }
    };
    private ApiListener<MsgResponseModel> toogleApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            businessInterface.profileVisibility(response.msg, SUCCESS);

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessInterface.profileVisibility(error, FAILURE);

        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            businessInterface.profileVisibility(null, -12);
        }
    };


    private ApiListener<WebResponseModel> webApiListener = new ApiListener<WebResponseModel>() {
        @Override
        public void onApiSuccess(WebResponseModel response, int apiFlag) {
            businessInterface.onFetchPortfolio(null, response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessInterface.onFetchPortfolio(error, null, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            businessInterface.onFetchPortfolio(null, null, -12);
        }
    };


    public BusinessProfilePresenter(IBusinessProfileInterface businessInterface) {
        this.businessInterface = businessInterface;
        interactor = new SignUpInteractor();
    }

    public void openNotificationActivity(NotifBean notificationStatus) {
        Bundle bundle = new Bundle();
        bundle.putString(FROM, BusinessProfileFragment.class.getName());
        bundle.putString(DATA_TO_SEND, new Gson().toJson(notificationStatus));
        businessInterface.openActivityForUpdate(ManageNotificationActivity.class, bundle, NOTIFICATION_REQUEST_CODE);
    }

    public void fetchUserDetails(int orgType) {
        businessInterface.showProgressDialog();
        if (orgType == 2) {
            interactor.getRecruiterBusinessProfile(businessProfileApiListener, API_FLAG_BUSINESS_PROFILE);
        } else
            interactor.getOrgProfile(businessProfileApiListener, API_FLAG_BUSINESS_PROFILE);
    }


    public void openLoginActivityFrag(BusinessLoginPresenter.DestinationFragment frag, String data, int requestCode) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NEED_UPDATE, true);
        bundle.putSerializable(FRAGMENT, frag);
        bundle.putString(DATA_TO_SEND, data);
        businessInterface.openActivityForUpdate(LoginActivity.class, bundle, requestCode);

    }

    public void openCompanyDetails(String name, String strength) {
        Bundle bundle = new Bundle();
        bundle.putBoolean(NEED_UPDATE, true);
        bundle.putSerializable(FRAGMENT, BusinessLoginPresenter.DestinationFragment.COMPANY_NAME);
        bundle.putString(DATA_TO_SEND, name);
        bundle.putString(COMPANY_STRENGTH, strength);
        businessInterface.openActivityForUpdate(LoginActivity.class, bundle, COMPANY_NAME_UPDATE_REQUEST_CODE);

    }

    public void openAddressFragment(String add, String cntry, String state, String lat, String lng) {
        Bundle bundle = new Bundle();
        bundle.putString(DATA_TO_SEND, add);
        bundle.putString(COUNTRY, cntry);
        bundle.putString(STATE, state);
        bundle.putString(LAT, lat);
        bundle.putString(LNG, lng);
        bundle.putSerializable(FRAGMENT, BusinessLoginPresenter.DestinationFragment.COMPANY_ADDRESS);
        bundle.putBoolean(NEED_UPDATE, true);
        businessInterface.openActivityForUpdate(LoginActivity.class, bundle, ADDRESS_UPDATE_REQUEST_CODE);

    }


    public void setOnActivityResultData(int requestCode, Bundle data) {
        if (data != null) {
            String result;
            switch (requestCode) {
                case ADDRESS_UPDATE_REQUEST_CODE:
                    ConvertLatLngToAddress.AddressBean bean = new Gson().fromJson(data.getString(DATA_TO_SEND), ConvertLatLngToAddress.AddressBean.class);
                    if (bean != null) {
                        businessInterface.setTextViewValue(IBusinessProfileInterface.TextViewType.ADDRESS, bean);
                    }
                    break;
                case REGISTRATION_NUMBER_UPDATE_REQUEST_CODE:
                    result = data.getString(DATA_TO_SEND, "");
                    businessInterface.setTextViewValue(IBusinessProfileInterface.TextViewType.REGISTRATION_NUMBER, result);
                    break;
                case DESCRIPTION_UPDATE_REQUEST_CODE:
                    result = data.getString(DATA_TO_SEND, "");
                    businessInterface.setTextViewValue(IBusinessProfileInterface.TextViewType.DESCRIPTION, result);
                    break;
                case NOTIFICATION_REQUEST_CODE:
                    String toggleStatus = data.getString(DATA_TO_SEND);
                    businessInterface.setNotificaitonToggle(toggleStatus);
                    break;
                case REQUEST_CODE_UPDATE_IMAGES:
                    LinkedList<String> imgs = Utility.gsonInstance().fromJson(data.getString(MEDIA), LinkedList.class);
                    businessInterface.setAdapter(IBusinessProfileInterface.AdapterType.IMAGES, imgs);
                    break;
                case UPDATE_BUSINESS_LOGO:
                    result = data.getString(DATA_TO_SEND);
                    businessInterface.setTextViewValue(IBusinessProfileInterface.TextViewType.LOGO, result);

            }
        }
    }

    public void deleteAccount() {
        businessInterface.showProgressDialog();
        interactor.deleteOrganisation(msgApiListener, API_FLAG_BUSINESS_DELETE);
    }

    public void logoutAccount(String device_tkn) {
        businessInterface.showProgressDialog();
        interactor.logoutOrganisation(msgApiListener, device_tkn, API_FLAG_BUSINESS_LOGOUT);
    }

    private boolean isValidId(String orgId) {
        if (orgId == null || TextUtils.isEmpty(orgId.trim())) {
            businessInterface.showToast(SOMETHING_WENT_WRONG);
            return false;
        }
        return true;
    }


    public void getCompanyPortfolio(CompanyPortfolioType type, int apiFlag) {
        businessInterface.showProgressDialog();
        interactor.getCompanyPortFolio(webApiListener, type, apiFlag);

    }


    public void manageProfileVisibility(boolean isVisible) {
        businessInterface.showProgressDialog();
        RequestBean bean = new RequestBean();
        bean.offline = isVisible;
        interactor.manageBusinessToggle(toogleApiListener, bean, API_FLAG_BUSINESS_TOGGLE);
    }

    public void openEditImageScreen(LinkedList<String> imgs, String baseUrl) {
        LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
        for (int i = 0; i < imgs.size(); i++) {
            linkedHashMap.put(imgs.get(i), baseUrl);
        }
        Bundle b = new Bundle();
        b.putString(IMAGE_BASE_URL, baseUrl);
        b.putString(IMAGES, Utility.gsonInstance().toJson(linkedHashMap));
        b.putBoolean(IS_BUSINESS_UPDATE, true);
        businessInterface.openActivityForUpdate(AddMediaActivity.class, b, REQUEST_CODE_UPDATE_IMAGES);

    }

    @Override
    public void openEditImageScreen(String baseUrl, LinkedHashMap<String, String> imageList) {
        Bundle b = new Bundle();
        b.putString(IMAGE_BASE_URL, baseUrl);
        b.putString(IMAGES, Utility.gsonInstance().toJson(imageList));
        b.putBoolean(IS_BUSINESS_UPDATE, true);
        businessInterface.openActivityForUpdate(AddMediaActivity.class, b, REQUEST_CODE_UPDATE_IMAGES);
    }

    @Override
    public void openViewImage(String baseUrl, LinkedList<String> imgs, int pos) {
        businessInterface.openActivity(baseUrl, imgs, pos);
    }
}
