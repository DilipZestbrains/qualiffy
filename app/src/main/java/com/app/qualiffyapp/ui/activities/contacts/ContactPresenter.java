package com.app.qualiffyapp.ui.activities.contacts;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ACCEPT_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_REJECT_FRND;

public class ContactPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {
    private SignupView signupView;
    private SignUpInteractor interactor;

    public ContactPresenter(SignupView signupView) {
        this.signupView = signupView;
        this.interactor = new SignUpInteractor();
    }


    public void hitFrndAPI(String userId, int apiFlag) {
        if (userId != null) {
            signupView.showProgressDialog(true);
            switch (apiFlag) {
                case API_FLAG_ADD_FRND:
                    interactor.jobSekekrAddfrndApi(this, userId, apiFlag);
                    break;
                case API_FLAG_ACCEPT_FRND:
                    interactor.jobSekekrAcceptFrndApi(this, userId, apiFlag);
                    break;
                case API_FLAG_REJECT_FRND:
                    interactor.jobSekekrRejectFrndApi(this, userId, apiFlag);
                    break;
            }
        }
    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        signupView.showProgressDialog(false);
        if (response != null) {
            signupView.apiSuccess(response, apiFlag);
            signupView.validationFailed(response.getRes().msg);
        }
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiError(error, apiFlag);
    }
}
