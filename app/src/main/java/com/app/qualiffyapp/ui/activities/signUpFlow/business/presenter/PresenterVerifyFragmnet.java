package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.os.CountDownTimer;
import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IVerifyFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.EMAIL_OTP_NOT_COMPLETE;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.MOBILE_OTP_NOT_COMPLETE;

public class PresenterVerifyFragmnet {
    private IVerifyFragment verifyFragment;
    private SignUpInteractor interactor;
    private ApiListener<VerifyResponseModel> verifyApiListener = new ApiListener<VerifyResponseModel>() {
        @Override
        public void onApiSuccess(VerifyResponseModel response, int apiFlag) {
            if (response != null)
                verifyFragment.onOtpVerification(response.msg, response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            verifyFragment.onOtpVerification(error, null);

        }
    };

    private ApiListener<SingupResponse> resendOtpApiListener = new ApiListener<SingupResponse>() {
        @Override
        public void onApiSuccess(SingupResponse response, int apiFlag) {
            if (response != null)
                verifyFragment.onOtpResend(response.getMsg(), response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            verifyFragment.onOtpResend(error, null);

        }
    };


    public PresenterVerifyFragmnet(IVerifyFragment verifyFragment) {
        this.verifyFragment = verifyFragment;
        interactor = new SignUpInteractor();
    }

    public void startTick() {
        CountDownTimer timer = new CountDownTimer(120000, 1000) {
            public void onTick(long millisUntilFinished) {
                String time = String.valueOf(millisUntilFinished / 1000);

                if ((millisUntilFinished / 1000) >= 60)
                    time = " 01:" + ((millisUntilFinished / 1000) - 60);

                if (((millisUntilFinished / 1000) - 60) < 10)
                    time = " 01:0" + ((millisUntilFinished / 1000) - 60);


                if ((millisUntilFinished / 1000) < 60)
                    time = " 00:" + ((millisUntilFinished / 1000));

                if ((millisUntilFinished / 1000) < 10)
                    time = " 00:0" + millisUntilFinished / 1000;

                verifyFragment.updateTime(time);
            }

            @Override
            public void onFinish() {
                verifyFragment.updateTime("00:00");
                verifyFragment.onTimeCompletion();
            }

        }.start();
    }

    public void verifyOtpPhoneSignUp(RequestBean requestBean, boolean isResendOtp) {
        verifyFragment.preOtpVerification();
        if (isResendOtp) {
            requestBean.is_reg = false;
            interactor.businessSignUp(resendOtpApiListener, requestBean, API_FLAG_BUSINESS);
        } else {
            requestBean.is_reg = true;
            interactor.businessVerifyOtp(verifyApiListener, requestBean, API_FLAG_BUSINESS);
        }
    }


    public void verifyOtpPhoneLogin(RequestBean requestBean, boolean isResendOtp) {
        verifyFragment.preOtpVerification();
        if (isResendOtp) {
            requestBean.is_login = false;
            interactor.businessLargeOrgLoginWithPhone(resendOtpApiListener, requestBean, API_FLAG_BUSINESS);

        } else {
            requestBean.is_login = true;
            interactor.businessLoginWithPhoneVerify(verifyApiListener, requestBean, API_FLAG_BUSINESS);
        }

    }


    private boolean isValid(String mobileOtp, String emailOtp) {
        if (mobileOtp == null || TextUtils.isEmpty(mobileOtp)) {
            verifyFragment.showToast(ToastType.MOBILE_OTP_EMPTY);
            return false;
        } else if (mobileOtp.length() < 3) {
            verifyFragment.showToast(MOBILE_OTP_NOT_COMPLETE);
            return false;
        } else if (emailOtp == null || TextUtils.isEmpty(emailOtp)) {
            verifyFragment.showToast(ToastType.EMAIL_OTP_EMPTY);
            return false;
        } else if (emailOtp.length() < 3) {
            verifyFragment.showToast(EMAIL_OTP_NOT_COMPLETE);
            return false;
        }
        return true;
    }

    private boolean isValidOtp(String mobileOtp) {
        if (mobileOtp == null || TextUtils.isEmpty(mobileOtp)) {
            verifyFragment.showToast(ToastType.MOBILE_OTP_EMPTY);
            return false;
        } else if (mobileOtp.length() < 4) {
            verifyFragment.showToast(MOBILE_OTP_NOT_COMPLETE);
            return false;
        }
        return true;
    }
}
