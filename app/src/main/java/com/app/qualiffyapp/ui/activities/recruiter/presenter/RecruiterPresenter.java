package com.app.qualiffyapp.ui.activities.recruiter.presenter;

import android.text.TextUtils;
import android.util.Patterns;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IAddress;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_RECRUITER;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class RecruiterPresenter {
    private IAddress address;
    private SignUpInteractor interactor;
    private ApiListener<MsgResponseModel> msgResponseModelApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            address.onUpdate(response.msg, SUCCESS);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            address.onUpdate(error, FAILURE);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            address.onUpdate(null, FAILURE);
        }
    };

    public RecruiterPresenter(IAddress updatableDetail) {
        this.address = updatableDetail;
        interactor = new SignUpInteractor();
    }


    public boolean isValidRecruiter(RequestBean requestBean) {
        if (TextUtils.isEmpty(requestBean.name)) {
            address.showToast(ToastType.NAME_EMPTY);
            return false;
        }
        if (TextUtils.isEmpty(requestBean.lng) || TextUtils.isEmpty(requestBean.lat)) {
            address.showToast(ToastType.ADDRESS_EMPTY);
            return false;
        }
        if (TextUtils.isEmpty(requestBean.email)) {
            address.showToast(ToastType.EMAIL_EMPTY);
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(requestBean.email).matches()) {
            address.showToast(ToastType.EMAIL_FORMAT_ERROR);
            return false;
        }
        if (TextUtils.isEmpty(requestBean.pno)) {
            address.showToast(ToastType.MOBILE_EMPTY);
            return false;
        }
        if (requestBean.pno.length() < 8) {
            address.showToast(ToastType.MOBILE_ERROR);
            return false;
        }
        if (TextUtils.isEmpty(requestBean.pass)) {
            address.showToast(ToastType.PASSWORD_EMPTY);
            return false;
        }
        if (requestBean.pass.length() < 6) {
            address.showToast(ToastType.INVALID_PASSWORD);
            return false;
        }
        if (TextUtils.isEmpty(requestBean.c_code)) {
            address.showToast(ToastType.MOBILE_ERROR);
            return false;
        }

        return true;
    }

    public void addRecruiter(RequestBean requestBean) {
        if (isValidRecruiter(requestBean)) {
            address.preUpdate();
            interactor.addRecruiter(msgResponseModelApiListener, requestBean, API_FLAG_ADD_RECRUITER);
        }
    }


}
