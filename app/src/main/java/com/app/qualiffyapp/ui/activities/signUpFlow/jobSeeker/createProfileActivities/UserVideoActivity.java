package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.customCamera.CustomCameraUtils;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityUserVideoBinding;
import com.app.qualiffyapp.imagevideoeditor.videoEditor.VideoEditorView;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.AddMediaPresenter;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_VIDEO;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_VIDEO;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_VIDEO_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_ID;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.THUMBNAIL;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CAMERA_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_EDITOR_ACTIVITY;

public class UserVideoActivity extends BaseActivity implements yesNoCallback, SignupView {
    public final String VIDEO = "video";
    public final String VIDEO_BASE_URL = "videoBaseUrl";
    private final int CAMERA_VIDEO = 201;
    private final int GALLERY_VIDEO = 202;
    private ActivityUserVideoBinding videoBinding;
    private AddMediaPresenter addMediaPresenter;
    private CustomDialogs customDialogs;
    private File videoFile;
    private boolean isVideoChange;
    private boolean isJobseekerUpdate;
    private boolean isOrgnizationUpdate;
    private FFmpeg ffmpeg;
    private boolean isCompressSupport = true;
    private String filePath;
    private String baseUrl;
    private List list = new ArrayList();
    private boolean isUpdateImage;
    private boolean isdelete;
    private String videoId;

    @Override
    protected void initView() {
        videoBinding = (ActivityUserVideoBinding) viewDataBinding;
        addMediaPresenter = new AddMediaPresenter(this);
        toolbar();
        loadFFMpegBinary();
        setListeners();
        getIntentData();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            isJobseekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            isOrgnizationUpdate = bundle.getBoolean(IS_BUSINESS_UPDATE);
            if (bundle.getString(VIDEO_BASE_URL) != null && bundle.getString(VIDEO) != null) {
                String videoUrl = bundle.getString(VIDEO_BASE_URL) + bundle.getString(VIDEO);
                videoId = bundle.getString(VIDEO);
                list.clear();
                if (!TextUtils.isEmpty(bundle.getString(VIDEO))) {
                    list.add(bundle.getString(VIDEO));
                }
                baseUrl = bundle.getString(VIDEO_BASE_URL);
                GlideUtil.loadImage(videoBinding.videoImg, videoUrl, R.drawable.ic_video_placeholder, 1000, 28);
            }
        }

    }

    private void setListeners() {
        videoBinding.captureVideoImg.setOnClickListener(this);
        videoBinding.doneBtn.btn.setOnClickListener(this);
        videoBinding.videoImg.setOnClickListener(this);
    }


    private void initDialog() {
        customDialogs = new CustomDialogs(this, this);
        if (list.size() == 0) {
            customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                    getResources().getString(R.string.add_media_dialog_body),
                    getResources().getString(R.string.add_media_dialog_pos_btn),
                    getResources().getString(R.string.add_media_dialog_neg_btn),
                    getResources().getString(R.string.ADD_MEDIA_TITLE));
        } else {
            customDialogs.showCustomDialogThreeButtons(getResources().getString(R.string.add_media_dialog_title),
                    getResources().getString(R.string.add_media_dialog_body),
                    "deleteVideo",
                    getResources().getString(R.string.add_media_dialog_pos_btn),
                    getResources().getString(R.string.add_media_dialog_neg_btn),
                    getResources().getString(R.string.delete_media_dialog_neg_btn)
            );

        }
        customDialogs.showDialog();
    }

    /**
     * Load FFmpeg binary
     */
    private void loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                ffmpeg = FFmpeg.getInstance(this);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    isCompressSupport = false;
                    showUnsupportedExceptionDialog();
                }

                @Override
                public void onSuccess() {
                }
            });
        } catch (FFmpegNotSupportedException e) {
            isCompressSupport = false;
            showUnsupportedExceptionDialog();
        } catch (Exception e) {
            isCompressSupport = false;
            e.printStackTrace();
        }
    }

    private void showUnsupportedExceptionDialog() {
        new AlertDialog.Builder(UserVideoActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    isCompressSupport = false;
                })
                .create()
                .show();
    }

    private void toolbar() {
        videoBinding.toolbar.backBtnImg.setOnClickListener(this);
        videoBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.USER_VIDEO_TITLE));
        videoBinding.doneBtn.btn.setText(getResources().getString(R.string.DONE));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_user_video;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;

            case R.id.capture_video_img:
                isVideoChange = true;
                initDialog();
                break;
            case R.id.video_img:
                if (list.size() > 0) {
                    Intent intent = new Intent(UserVideoActivity.this, StoryViewActivity.class);
                    if (!isUpdateImage)
                        intent.putExtra("BASE_URL_IMAGE", baseUrl);
                    intent.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(list));
                    startActivity(intent);
                }
                break;

            case R.id.done_btn:
                if (isVideoChange) {
                    if (videoFile == null && !isJobseekerUpdate && !isOrgnizationUpdate) {
                        Utility.showToast(this, getResources().getString(R.string.empty_video));
                        return;
                    } else if (videoFile == null && (isJobseekerUpdate || isOrgnizationUpdate)) {
                        if (isdelete) {
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra(THUMBNAIL, "");
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();
                        } else onBackPressed();
                    } else if (videoFile != null) {
                        showProgressDialog(true);
                        if (isOrgnizationUpdate)
                            addMediaPresenter.orgVideoApi(videoFile, API_FLAG_BUSINESS_ADD_VIDEO);
                        else addMediaPresenter.hitApi(videoFile, API_FLAG_ADD_VIDEO, "2");
                    }
                } else {
                    if (isOrgnizationUpdate || isJobseekerUpdate)
                        onBackPressed();
                    else
                        openActivity(LocationActivity.class, null);

                }

                break;
        }

    }


    @Override
    public void onYesClicked(String from) {

        if (from.equals(getResources().getString(R.string.delete_media_dialog_neg_btn))) {
            if (list.size() > 0) {
                list.clear();
                isdelete = true;
                if (!videoId.equals("")) {
                    showProgressDialog(true);
                    if (isOrgnizationUpdate)
                        addMediaPresenter.businessDeleteMedia(videoId, 1);
                    else
                        addMediaPresenter.deleteMediaApi(videoId, "1");
                } else {
                    videoFile = null;
                    videoBinding.videoImg.setImageResource(R.drawable.ic_video_placeholder);
                }
            }
        } else if (onAskForSomePermission(this, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE)) {
            getVideofromGallery();
        }
    }


    @Override
    public void onNoClicked(String from) {
        if (onAskForSomePermission(this, CAMERA_VIDEO_PERMISION, VIDEO_CAMERA_PERMISSION_REQUEST_CODE)) {
            openCamera();
        }

    }

    private void openCamera() {
        SandriosCamera
                .with()
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_VIDEO)
                .launchCamera(this);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case GALLERY_STORAGE_PERMISSION_REQUEST_CODE:
                getVideofromGallery();
                break;
            case VIDEO_CAMERA_PERMISSION_REQUEST_CODE:
                openCamera();
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }


    private void getVideofromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
        intent.setType("video/*");
        startActivityForResult(intent, GALLERY_VIDEO);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Toast.makeText(this, "Returned from setting", Toast.LENGTH_SHORT).show();
                break;

            case REQUEST_CODE_CAMERA_IMG:
                if (resultCode == Activity.RESULT_OK) {
                    getVideoThumbnail(data.getStringExtra(MEDIA_PATH));
                }
                break;

            case REQUEST_CODE_FILTER_VIDEO:
                if (resultCode == Activity.RESULT_OK) {
                    getVideoThumbnail(data.getStringExtra(MEDIA_PATH));
                }
                break;

            case GALLERY_VIDEO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    String path = getPath(selectedImageUri);
                    MediaPlayer mp = MediaPlayer.create(this, Uri.parse(path));
                    int duration = mp.getDuration();
                    mp.release();
                    if ((duration / 1000) > 40) {
                        Utility.showToast(UserVideoActivity.this, "Please select video less than 40 sec.");
                    } else {

                        if (path != null) {
                            openEditor(path);
                           /* Intent intent = new Intent(this, VideoEditorView.class);
                            intent.putExtra("videoPath", path);
                            startActivityForResult(intent, REQUEST_CODE_FILTER_VIDEO);*/
                        }

                    }
                }

                break;

        }
    }

    public void getVideoThumbnail(String videoPath) {
        videoFile = new File(videoPath);
        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(videoPath, MediaStore.Video.Thumbnails.MINI_KIND);
        videoBinding.videoImg.setImageBitmap(bMap);
        if (list != null) {
            list.clear();
            list.add(Uri.parse(new File(videoPath).toString()).toString());
            isUpdateImage = true;
        }

    }



    private void openEditor(String imgPath) {


        try {
                Intent myIntent = new Intent(this, Class.forName(VIDEO_EDITOR_ACTIVITY));
                myIntent.putExtra(MEDIA_PATH, imgPath);
                startActivityForResult(myIntent, REQUEST_CODE_CAMERA_IMG);
                SandriosCamera.cropperSquare = true;

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }



    private void getCompressedVideo(String video_path) {
     /*   File moviesDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MOVIES
        );
        String filePrefix = "compress_video";
        String fileExtn = ".mp4";

        File dest = new File(moviesDir, filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(moviesDir, filePrefix + fileNo + fileExtn);
        }


        filePath = dest.getAbsolutePath();*/

        filePath = CustomCameraUtils.getFile(true).getAbsolutePath();

        String[] complexCommand = {"-y", "-i", video_path, "-preset", "ultrafast", "-r", "16", "-ab", "64000", "-b", "2097k", "-b:v", "1000k", "-acodec", "aac", filePath};
        execFFmpegBinary(complexCommand);
    }

    /**
     * Executing ffmpeg binary
     */
    private void execFFmpegBinary(final String[] command) {

        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    showProgress(false);
                }

                @Override
                public void onSuccess(String s) {
                    showProgress(false);
                    getVideoThumbnail(filePath);
                }

                @Override
                public void onProgress(String s) {

                }

                @Override
                public void onStart() {
                    showProgress(true);
                }

                @Override
                public void onFinish() {
                    showProgress(false);
                }
            });
        } catch (Exception e) {
            showProgress(false);
            e.printStackTrace();
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        if (response.getStatus() == 1) {
            AddMediaResponseModel mediaResponseModel = (AddMediaResponseModel) response.getRes();
            switch (apiFlag) {
                case API_FLAG_DELETE_MEDIA:
                    validationFailed(mediaResponseModel.msg);
                    videoFile = null;
                    videoId = "";
                    videoBinding.videoImg.setImageResource(R.drawable.ic_video_placeholder);
                    break;
                case API_FLAG_ADD_VIDEO:
                    if (isJobseekerUpdate) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(THUMBNAIL, mediaResponseModel.img_id);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        openActivity(LocationActivity.class, null);
                    }
                    break;
                case API_FLAG_BUSINESS_ADD_VIDEO:
                    if (isOrgnizationUpdate) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(THUMBNAIL, mediaResponseModel.img_id);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        openActivity(LocationActivity.class, null);
                    }
                    break;
            }

        } else {
            Utility.showToast(this, response.err.msg);
        }
    }


    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }


    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


}
