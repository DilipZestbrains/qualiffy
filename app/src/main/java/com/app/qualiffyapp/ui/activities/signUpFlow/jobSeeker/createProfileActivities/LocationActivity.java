package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityLocationBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.onBoarding.OnBoardingActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobseekerAddressPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.locationUtils.ConvertLatLngToAddress;
import com.app.qualiffyapp.utils.locationUtils.GetLocation;
import com.app.qualiffyapp.utils.locationUtils.GetLocationCallback;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_5;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.LOCATION_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.LOCATION_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.TUTORIAL_SCREEN_SHOW;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.LAST_NAME;
import static com.app.qualiffyapp.utils.locationUtils.GetLocation.RESOLUTION_REQUEST_LOCATION;

public class LocationActivity extends BaseActivity implements GetLocationCallback, SignupView {
    private ActivityLocationBinding locationBinding;
    private GetLocation getLocation;
    private ConvertLatLngToAddress convertLatLngToAddress;
    private JobseekerAddressPresenter addressPresenter;
    private boolean isHitApi;

    @Override
    protected void initView() {
        locationBinding = (ActivityLocationBinding) viewDataBinding;
        getLocation = GetLocation.getInstance(this);
        convertLatLngToAddress = new ConvertLatLngToAddress();
        addressPresenter = new JobseekerAddressPresenter(this);


    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_location;
    }

    @Override
    protected void setListener() {
        super.setListener();
        locationBinding.notNowTxt.setVisibility(View.VISIBLE);
        locationBinding.notNowTxt.setOnClickListener(this);
        locationBinding.enableLocationTxt.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.not_now_txt:
            case R.id.enable_location_txt:

                if (onAskForSomePermission(this, LOCATION_PERMISION, LOCATION_PERMISSION_REQUEST_CODE)) {
//                    getLocation.gettingLocationWithProgressBar(this);
                    showProgressDialog(true);
                    if (getLocation.checkGooglePlayServiceAvailability(this, this))
                        getLocation.buildGoogleApiClient();
                    isHitApi = true;
                }
                break;
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
//        getLocation.gettingLocationWithProgressBar(this);
        if (getLocation.checkGooglePlayServiceAvailability(this, this)) {
            showProgressDialog(true);
            getLocation.buildGoogleApiClient();
        }
        isHitApi = true;

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLUTION_REQUEST_LOCATION && resultCode == RESULT_OK) {
            getLocation.startGettingLocation();
        } else if (requestCode == RESOLUTION_REQUEST_LOCATION) {
            showProgressDialog(false);

        }

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        Utility.showToast(this, getResources().getString(R.string.permission_dinied));
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void getLocation(LatLng latLng) {
        showProgressDialog(false);
        try {
            if (isHitApi) {
                ConvertLatLngToAddress.AddressBean addressBean = convertLatLngToAddress.getAddress(this, latLng.latitude, latLng.longitude);
                RequestBean requestBean = new RequestBean();
                requestBean.address = addressBean.addr;
                requestBean.cntry = addressBean.cntry;
                requestBean.state = addressBean.city + "' " + addressBean.state;
                requestBean.lng = addressBean.lng;
                requestBean.lat = addressBean.lat;

                showProgressDialog(true);

                if (Utility.getStringSharedPreference(this, FIRST_NAME) != null && !TextUtils.isEmpty(Utility.getStringSharedPreference(this, FIRST_NAME).trim()))
                    requestBean.f_name = Utility.getStringSharedPreference(this, FIRST_NAME);
                if (Utility.getStringSharedPreference(this, LAST_NAME) != null && !Utility.getStringSharedPreference(this, LAST_NAME).equals(""))
                    requestBean.l_name = Utility.getStringSharedPreference(this, LAST_NAME);
                addressPresenter.hitApi(requestBean, API_FLAG_SIGNUP_5);
                isHitApi = false;


            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            if (response.getStatus() == 1) {
                VerifyResponseModel msgResponseModel = (VerifyResponseModel) response.getRes();
                switch (apiFlag) {
                    case API_FLAG_SIGNUP_5:
                        Utility.showToast(this, msgResponseModel.msg);
                        Utility.putIntValueInSharedPreference(getApplicationContext(), USER_TYPE, JOB_SEEKER);
                        Intent intent = null;
                        if (Utility.getBooleanSharedPreference(getApplicationContext(), TUTORIAL_SCREEN_SHOW)) {
                            intent = new Intent(this, DashboardActivity.class);
                        } else {
                            intent = new Intent(this, OnBoardingActivity.class);
                        }
                        saveName(msgResponseModel);
                        CreateJobSeekerProfileInputModel inputModel = CreateJobSeekerProfileInputModel.getInstance();
                        inputModel.clearInputModel();
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        break;

                }
            } else
                Utility.showToast(this, response.err.msg);
        showProgressDialog(false);

    }


    private void saveName(VerifyResponseModel verifyResponseModel) {
        Utility.saveProfile(this, verifyResponseModel, false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {

    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    protected void onDestroy() {
        getLocation.disconnectLocation();
        super.onDestroy();
    }
}
