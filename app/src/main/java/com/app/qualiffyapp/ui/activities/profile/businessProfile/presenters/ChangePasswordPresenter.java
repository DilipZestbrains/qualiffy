package com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters;

import android.text.TextUtils;

import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces.IChangePassword;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CHANGE_PASS;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.INVALID_PASSWORD;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.NEW_PASS_OLD_PASS_SAME;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.OLD_PASS_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.PASS1_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.PASS2_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.PASS_MISMATCH;

public class ChangePasswordPresenter {
    private final IChangePassword changePassword;
    private final SignUpInteractor interactor;
    private ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            changePassword.onChange(response.msg, SUCCESS);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            changePassword.onChange(error, FAILURE);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            changePassword.onChange(null, -12);
        }
    };


    public ChangePasswordPresenter(IChangePassword pass) {
        this.changePassword = pass;
        interactor = new SignUpInteractor();
    }

    public void changePassword(String oldPass, String pass1, String pass2) {
        if (isValid(oldPass, pass1, pass2)) {
            changePassword.preChange();
            interactor.changePasswordBusiness(msgApiListener, oldPass, pass2, API_FLAG_CHANGE_PASS);
        }
    }

    private boolean isValid(String oldPass, String pass1, String pass2) {

        if (oldPass == null || TextUtils.isEmpty(oldPass)) {
            changePassword.showToast(OLD_PASS_EMPTY);
            return false;
        }
        if (pass1 == null || TextUtils.isEmpty(pass1.trim())) {
            changePassword.showToast(PASS1_EMPTY);
            return false;
        }
        if (pass1.length() < 6) {
            changePassword.showToast(INVALID_PASSWORD);
            return false;
        }
        if (pass2 == null || TextUtils.isEmpty(pass2.trim())) {
            changePassword.showToast(PASS2_EMPTY);
            return false;
        }

        if (!pass1.equals(pass2)) {
            changePassword.showToast(PASS_MISMATCH);
            return false;
        }
        if (oldPass.equals(pass1)) {
            changePassword.showToast(NEW_PASS_OLD_PASS_SAME);
            return false;
        }
        return true;
    }
}
