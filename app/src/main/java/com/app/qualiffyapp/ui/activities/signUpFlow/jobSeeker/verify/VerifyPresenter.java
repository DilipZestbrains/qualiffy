package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.verify;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.Valdations;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_LOGIN;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_VERIFY;

public class VerifyPresenter implements ApiListener<ResponseModel<VerifyResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;
    private Valdations valdations;

    public VerifyPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
        valdations = new Valdations();
    }


    @Override
    public void onApiSuccess(ResponseModel<VerifyResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
        else
            view.validationFailed(ApplicationController.getApplicationInstance().getString(R.string.something_went_wrong));

    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);

    }

    public void resendOtpApi(String phnnum) {
        mSignUpInteractor.loginApi(this, phnnum,
                Utility.getStringSharedPreference(view.getViewContext(),
                        AppConstants.PREFS_ACCESS_TOKEN), API_FLAG_LOGIN);
    }


    public void validate(String user_id, String otp) {
        if (valdations.validateOtp(otp) != null) {
            view.validationFailed(valdations.validateOtp(otp));
            return;
        } else if (user_id == null) {
            view.validationFailed(ApplicationController.getApplicationInstance().getString(R.string.something_went_wrong));
            return;
        } else {
            view.showProgressDialog(true);
            mSignUpInteractor.verifyApi(this, user_id, otp,
                    Utility.getStringSharedPreference(view.getViewContext(), AppConstants.PREFS_ACCESS_TOKEN),
                    API_FLAG_VERIFY);
        }
    }


}
