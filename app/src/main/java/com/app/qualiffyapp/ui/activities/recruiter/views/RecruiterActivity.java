package com.app.qualiffyapp.ui.activities.recruiter.views;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityRecruiterBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.ui.activities.recruiter.presenter.RecruiterPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IAddress;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.AddressFragment;
import com.app.qualiffyapp.utils.Utility;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;

import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.AddressPresenter.AUTOCOMPLETE_REQUEST_CODE;

public class RecruiterActivity extends BaseActivity implements IAddress {

    private ActivityRecruiterBinding mBinding;
    private AddressPresenter addressPresenter;
    private RecruiterPresenter recruiterPresenter;
    private RequestBean requestBean;

    @Override
    protected void initView() {
        mBinding = (ActivityRecruiterBinding) viewDataBinding;
        mBinding.header.toolbarTitleTxt.setText("Add Recruiter");
        addressPresenter = new AddressPresenter(this);
        recruiterPresenter = new RecruiterPresenter(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_recruiter;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.etAddress:
                addressPresenter.initAddressPickerDialog();
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.tvAdd:
                addRecruiter();
                break;

        }
    }

    @Override
    protected void setListener() {
        mBinding.etPhone.setOnFocusChangeListener((v1, hasFocus) -> {
            if (hasFocus) {
                mBinding.tvPhoneHint.setTextColor(getResources().getColor(R.color.text_label_color_light_black));
            } else mBinding.tvPhoneHint.setTextColor(getResources().getColor(R.color.light_grey));
        });
        mBinding.etAddress.setOnClickListener(this);
        mBinding.tvAdd.setOnClickListener(this);
        mBinding.header.backBtnImg.setOnClickListener(this);

    }

    private void addRecruiter() {
        if (requestBean == null)
            requestBean = new RequestBean();
        if (mBinding.etName.getText() != null)
            requestBean.name = mBinding.etName.getText().toString();
        if (mBinding.etEmail.getText() != null)
            requestBean.email = mBinding.etEmail.getText().toString();
        if (mBinding.etPass.getText() != null)
            requestBean.pass = mBinding.etPass.getText().toString();
        requestBean.c_code = mBinding.etlPhone.getSelectedCountryCode();
        if (mBinding.etPhone.getText() != null)
            requestBean.pno = mBinding.etPhone.getText().toString();
        recruiterPresenter.addRecruiter(requestBean);

    }

    @Override
    public void onNext() {

    }

    @Override
    public void openAutoSelectPlace(Autocomplete.IntentBuilder intent) {
        startActivityForResult(intent.build(this), AUTOCOMPLETE_REQUEST_CODE);
    }

    @Override
    public void onAddressSelect(String name, String country, String state, String lat, String lng) {
        mBinding.etAddress.setText(state + ", " + country);
        if (requestBean == null)
            requestBean = new RequestBean();
        requestBean.lat = lat;
        requestBean.lng = lng;
        requestBean.address = name;
        requestBean.cntry = country;
        requestBean.state = state;
    }

    @Override
    public void showToast(ToastType toastType) {
        switch (toastType) {
            case EMAIL_EMPTY:
                Utility.showToast(this, "Enter recruiter email id.");
                break;
            case EMAIL_FORMAT_ERROR:
                Utility.showToast(this, "Enter valid email id.");
                break;
            case PASSWORD_EMPTY:
                Utility.showToast(this, "Password can't be empty.");
                break;
            case INVALID_PASSWORD:
                Utility.showToast(this, getResources().getString(R.string.pass_length));
                break;
            case MOBILE_EMPTY:
                Utility.showToast(this, "Enter recruiter phone number.");
                break;
            case MOBILE_ERROR:
                Utility.showToast(this, "Enter valid phone number.");
                break;
            case NAME_EMPTY:
                Utility.showToast(this, "Enter recruiter name.");
                break;
            case ADDRESS_EMPTY:
                Utility.showToast(this, "Select your address.");
                break;
            case ADDRESS_ERROR:
                Utility.showToast(this, "Address is not valid.");

        }
    }

    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(String msg, int status) {
        showProgress(false);
        Utility.showToast(this, msg);

        if (status == SUCCESS) {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(intent);
                addressPresenter.pickAddress(place);

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(intent);
                Log.i(AddressFragment.class.getName(), status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
            }
        }
    }

}
