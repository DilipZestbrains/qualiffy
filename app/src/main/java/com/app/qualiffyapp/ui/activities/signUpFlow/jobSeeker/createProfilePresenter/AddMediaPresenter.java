package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.io.File;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_DELETE_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.FALSE;

public class AddMediaPresenter implements ApiListener<ResponseModel<AddMediaResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public AddMediaPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<AddMediaResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApi(File file, int apifLAG, String isProfile) {
        long time = System.currentTimeMillis();
        mSignUpInteractor.addMediaApi(this, file, String.valueOf(time), isProfile, apifLAG);
    }

    public void deleteMediaApi(String imgId, String imgType) {
        mSignUpInteractor.deleteApi(this, imgId, imgType, API_FLAG_DELETE_MEDIA);
    }


    public void addBusinessMedia(File compressedImageFile) {
        long time = System.currentTimeMillis();
        RequestBean requestBean = new RequestBean();
        requestBean.isProfile = FALSE;
        requestBean.uid = String.valueOf(time);
        mSignUpInteractor.addMediaOrgApi(this, compressedImageFile, requestBean, API_FLAG_ADD_MEDIA);
    }

    public void businessDeleteMedia(String imgId,int type) {
        mSignUpInteractor.deleteOrgMediaApi(this, imgId,type, API_FLAG_DELETE_MEDIA);

    }

    public void orgVideoApi(File videoFile, int apifLAG) {
        long time = System.currentTimeMillis();
        RequestBean bean=new RequestBean();
        bean.uid=String.valueOf(time);
        mSignUpInteractor.addMediaOrgApi(this, videoFile,bean , apifLAG);
    }

  /*  public void orgVideoApi(File videoFile, int apifLAG) {
        long time = System.currentTimeMillis();
        RequestBean bean=new RequestBean();
        bean.uid=String.valueOf(time);
        mSignUpInteractor.addMediaOrgApi(this, videoFile,bean , apifLAG);
    }*/
}
