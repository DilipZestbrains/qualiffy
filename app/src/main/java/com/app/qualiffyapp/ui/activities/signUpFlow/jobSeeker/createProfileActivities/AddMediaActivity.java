package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.AddMediaAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityAddMediaBinding;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddMediaModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.AddMediaPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.downloadFile.DownloadFiles;
import com.app.qualiffyapp.utils.downloadFile.IDownloadFile;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album.AlbumPictureModel;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album.FBAlbumCallback;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album.FacebookAlbum;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.album.FacebookAlbumActivity;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import id.zelory.compressor.Compressor;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.FACEBOOK_PROFILE_ID;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IMAGES;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_BASE_URL;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_DELETE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_PERMISSION;
import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FACEBOOK_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_IMAGE;
import static com.app.qualiffyapp.constants.AppConstants.TEMP_MEDIA_FOLDER;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.customViews.DialogInitialize.MEDIA_WITH_FB;
import static com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity.CROPPER_SQUARE;
import static java.security.AccessController.getContext;

public class AddMediaActivity extends BaseActivity implements yesNoCallback, RecyclerItemClick, SignupView, FBAlbumCallback, IDownloadFile {

    public final static String MEDIA = "imgMedia";
    private final int CAMERA_IMG = 201;
    DialogInitialize dialogInitialize;
    private ActivityAddMediaBinding mediaBinding;
    private AddMediaPresenter addMediaPresenter;
    private GridLayoutManager gridLayoutManager;
    private CustomDialogs customDialogs;
    private LinkedList<AddMediaModel> mediaList;
    private AddMediaAdapter addMediaAdapter;
    private int GALLERY_IMG = 202;
    private boolean isHitApi;
    private boolean isJobSeekerUpdate;
    private boolean isBusinessUpdate;
    private List<String> stringList;
    private String baseUrl;
    private String facebookImg;

    @Override
    protected void initView() {
        mediaBinding = (ActivityAddMediaBinding) viewDataBinding;
        addMediaPresenter = new AddMediaPresenter(this);
        mediaList = new LinkedList<>();
        getIntentData();
        initImgGrid();
        initDialog();
        toolbar();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            isBusinessUpdate = bundle.getBoolean(IS_BUSINESS_UPDATE);

            baseUrl = bundle.getString(IMAGE_BASE_URL);
            String img = bundle.getString(IMAGES);

            LinkedHashMap<String, String> imgs = new Gson().fromJson(img, LinkedHashMap.class);
            if (stringList != null) {
                stringList.clear();
            }
            if (imgs != null) {
                stringList = new ArrayList<String>(imgs.keySet());
            }

            if (stringList != null) {
                for (int i = 0; i < stringList.size(); i++) {
                    String url = baseUrl + stringList.get(i);
                    String img_id = stringList.get(i);

                    AddMediaModel addMediaModel = new AddMediaModel();
                    addMediaModel.setImgFile(url);
                    addMediaModel.setImg_id(img_id);
                    mediaList.add(addMediaModel);
                }

            }
        }

    }


    private void initImgGrid() {
        gridLayoutManager = new GridLayoutManager(this, 3);
        mediaBinding.mediaGrid.setLayoutManager(gridLayoutManager);
        if (mediaList.size() < 10)
            setAddImage();
        addMediaAdapter = new AddMediaAdapter(mediaList, this);
        mediaBinding.mediaGrid.setAdapter(addMediaAdapter);
    }

    private void initDialog() {
        customDialogs = new CustomDialogs(this, this);
        dialogInitialize = new DialogInitialize();

    }

    @Override
    protected void setListener() {
        super.setListener();
        mediaBinding.toolbar.backBtnImg.setOnClickListener(this);
        mediaBinding.expBtn.btn.setOnClickListener(this);

    }

    private void toolbar() {
        mediaBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.ADD_MEDIA_TITLE));
        if (isJobSeekerUpdate || isBusinessUpdate)
            mediaBinding.expBtn.btn.setText(getResources().getString(R.string.DONE));
        else
            mediaBinding.expBtn.btn.setText(getResources().getString(R.string.NEXT));

        if(!isBusinessUpdate)
            mediaBinding.tvImg.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_add_media;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.exp_btn:
                if (!isHitApi) {
                    if ((mediaList == null || mediaList.size() == 1 )&& isBusinessUpdate) {
                        validationFailed(getResources().getString(R.string.empty_media));
                    } else if((mediaList == null||mediaList.size()<=3 )&& !isBusinessUpdate){
                        validationFailed(getResources().getString(R.string.media_limit));
                    } else {
                        if (isJobSeekerUpdate || isBusinessUpdate) {
                            sendBackData();
                        } else
                            startActivity(new Intent(this, LocationActivity.class));
                    }
                }
                break;
        }

    }

    private void sendBackData() {
        LinkedList<String> imgUrl = new LinkedList<>();

        for (int i = 0; i < mediaList.size(); i++) {
            if (mediaList.get(i).getImg_id() != null)
                imgUrl.add(mediaList.get(i).getImg_id());
        }
        Intent returnIntent = new Intent();
        returnIntent.putExtra(MEDIA, new Gson().toJson(imgUrl));
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!isHitApi) {
            if (isJobSeekerUpdate || isBusinessUpdate) {
                sendBackData();
            } else super.onBackPressed();
        }
    }

    @Override
    public void onYesClicked(String from) {
        if (from.equals(getResources().getString(R.string.FACEBOOK))) {
            FacebookAlbum facebookAlbum = new FacebookAlbum(this);
            facebookAlbum.getFbAlbums(Utility.getStringSharedPreference(this, FACEBOOK_PROFILE_ID));

        } else if (onAskForSomePermission(this, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE)) {
            EasyImage.openGallery(this, GALLERY_IMG);
        }
    }

    @Override
    public void onNoClicked(String from) {
        if (onAskForSomePermission(this, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE)) {
           /* Intent i = new Intent(this, CameraActivity.class);
            i.putExtra("media_type", 1);
            startActivityForResult(i, CAMERA_IMG);*/
            openCameraActivity(null);

        }


    }


    @Override
    public void onClick(int flag, int pos) {
        switch (flag) {
            case ADD_IMG_CLICK_FLAG:
                if (isHitApi == false)
                    dialogInitialize.captureMediaDialog(customDialogs, MEDIA_WITH_FB);
                break;
            case DELETE_FLAG:
                if (isBusinessUpdate && mediaList.size() == 2) {
                    Utility.showToast(this, getResources().getString(R.string.media_alert_dialog_body));
                } else if( !isBusinessUpdate && mediaList.size() <= 4){
                    Utility.showToast(this, getResources().getString(R.string.media_delete_limit));
                }
                else {
                    showProgressDialog(true);
                    AddMediaModel addMediaModel = mediaList.get(pos);
                    if (isBusinessUpdate)
                        addMediaPresenter.businessDeleteMedia(addMediaModel.getImg_id(),0);
                    else
                        addMediaPresenter.deleteMediaApi(addMediaModel.getImg_id(), String.valueOf(IMAGE_DELETE));

                    mediaList.remove(pos);
                    stringList.remove(pos);
                    if (mediaList.get(mediaList.size() - 1).getDrawable_img() == 0)
                        setAddImage();
                    mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
                }
                break;
            case VIEW_IMG_CLICK_FLAG:
                Intent viewMedia = new Intent(AddMediaActivity.this, StoryViewActivity.class);
                viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(stringList));
                viewMedia.putExtra("BASE_URL_IMAGE", baseUrl);
                startActivity(viewMedia);
                break;
        }

    }


    private void setAddImage() {
        AddMediaModel addMediaModel = new AddMediaModel();
        addMediaModel.setDrawable_img(R.drawable.add_placeholder);
        mediaList.add(mediaList.size(), addMediaModel);
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_ADD_MEDIA:
                if (response.getStatus() == 1) {
                    AddMediaResponseModel mediaResponseModel = (AddMediaResponseModel) response.getRes();
                    baseUrl = mediaResponseModel.bP;
                    AddMediaModel addMediaModel;
                    if (mediaList.size() == 10 && mediaList.get(mediaList.size() - 1).getDrawable_img() == 0)
                        addMediaModel = mediaList.get(mediaList.size() - 1);
                    else
                        addMediaModel = mediaList.get(mediaList.size() - 2);
                    addMediaModel.setImg_id(mediaResponseModel.img_id);
                    if (stringList == null) {
                        stringList = new ArrayList<String>();
                    }
                    stringList.add(mediaResponseModel.img_id);
                    mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
                } else {
                    if (mediaList.size() == 10) {
                        mediaList.remove(9);
                        setAddImage();
                        mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
                    }
                    validationFailed(response.err.msg);
                }
                isHitApi = false;
                break;

            case API_FLAG_DELETE_MEDIA:
                if (response.getStatus() == 1) {
                    AddMediaResponseModel mediaResponseModel = (AddMediaResponseModel) response.getRes();
                    validationFailed(mediaResponseModel.msg);
                }
                showProgressDialog(false);

                break;
        }

    }


    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        switch (apiFlag) {
            case API_FLAG_ADD_MEDIA:
                setAddImage();
                isHitApi = false;
                break;
            case API_FLAG_DELETE_MEDIA:
                showProgressDialog(false);
                break;
        }


    }


    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        super.onPermissionsGranted(requestCode, list);

        Log.e("Permissions :  ", "Permission Granted Activity");
        switch (requestCode) {
            case CAMERA_STORAGE_PERMISSION_REQUEST_CODE:

                openCameraActivity(null);
//                EasyImage.openCameraForImage(this, CAMERA_IMG);
                break;
            case GALLERY_STORAGE_PERMISSION_REQUEST_CODE:
                EasyImage.openGallery(this, GALLERY_IMG);
                break;
            case READ_WRITE_REQUEST_CODE:
                if (facebookImg != null && !TextUtils.isEmpty(facebookImg))
                    downloadFile(facebookImg);
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        super.onPermissionsDenied(requestCode, list);
        Log.e("Permissions :  ", "Permission Denied Activity");// Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                    Log.e("Permission", "Returned from setting");
                    break;
                case REQUEST_CODE_CAMERA_IMG:
                    List<File> list = new ArrayList<>();
                    list.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list);
                    break;

                    case REQUEST_CODE_FILTER_IMAGE:
                    List<File> list1 = new ArrayList<>();
                    list1.add(new File(data.getStringExtra(MEDIA_PATH)));
                    onPhotosReturned(list1);
                    break;
                case REQUEST_CODE_FACEBOOK_MEDIA:
                    if (onAskForSomePermission(this, READ_WRITE_PERMISSION, READ_WRITE_REQUEST_CODE)) {
                        facebookImg = data.getStringExtra("facebook");
                        if (facebookImg != null && !TextUtils.isEmpty(facebookImg))
                            downloadFile(facebookImg);
                    }
                    break;
            }

        }

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagesPicked(@NonNull List<File> list, EasyImage.ImageSource imageSource, int i) {
                File imgFile = list.get(0);
                openEditor(imgFile.getPath());
            }
        });
    }


    private void openEditor(String imgPath) {


        try {
            Intent myIntent = new Intent(this, Class.forName(PHOTO_EDITOR_ACTIVITY));
                myIntent.putExtra(MEDIA_PATH, imgPath);
            myIntent.putExtra(CROPPER_SQUARE,false);
            startActivityForResult(myIntent, REQUEST_CODE_CAMERA_IMG);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
    private void downloadFile(String url) {
        showProgressDialog(true);
        DownloadFiles downloadFiles = new DownloadFiles(url, TEMP_MEDIA_FOLDER, this);
        downloadFiles.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);

    }


    private void onPhotosReturned(List<File> list) {

        File imgFile = list.get(0);
        AddMediaModel addMediaModel = new AddMediaModel();
        addMediaModel.setImgFile(imgFile.getAbsolutePath());
        if (mediaList.size() < 10)
            mediaList.add(mediaList.size() - 1, addMediaModel);
        else {
            mediaList.remove(mediaList.size() - 1);
            mediaList.add(addMediaModel);
        }

        mediaBinding.mediaGrid.getAdapter().notifyDataSetChanged();
        try {
            isHitApi = true;
            File compressedImageFile = new Compressor(this).compressToFile(imgFile);
            if (isBusinessUpdate) {
                addMediaPresenter.addBusinessMedia(compressedImageFile);
            } else
                addMediaPresenter.hitApi(compressedImageFile, API_FLAG_ADD_MEDIA, "0");
        } catch (IOException e) {
            isHitApi = false;
        }
    }

    @Override
    public void getAlbum(ArrayList<AlbumPictureModel> albumPictureModelsAL) {
        Intent intent = new Intent(this, FacebookAlbumActivity.class);
        intent.putExtra("list", albumPictureModelsAL);
        startActivityForResult(intent, REQUEST_CODE_FACEBOOK_MEDIA);
    }

    @Override
    public void getDownloadFile(String filePath) {
        showProgressDialog(false);
        List<File> list = new ArrayList<>();
        list.add(new File(filePath));
        onPhotosReturned(list);
    }


    private void openCameraActivity(String imgPath) {
        SandriosCamera
                .with()
                .setShowPicker(true)
                .setCropperSquare(false)
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_PHOTO)
                .launchCamera(this);
      /*  Intent i = new Intent(this, CameraActivity.class);
        i.putExtra(MEDIA_TYPE, 1);
        i.putExtra(MEDIA_PATH, imgPath);
        startActivityForResult(i, CAMERA_IMG);*/
    }
}


