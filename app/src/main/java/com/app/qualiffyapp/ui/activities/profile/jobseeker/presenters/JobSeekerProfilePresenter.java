package com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.app.qualiffyapp.adapter.EducationAdapter;
import com.app.qualiffyapp.adapter.ImagesAdapter;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.constants.CompanyPortfolioType;
import com.app.qualiffyapp.models.ConnectionResponseModel;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerProfileResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.ProfileFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.JobSeekerEducationActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.JobSeekerInfoActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JOB_SEEKER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_DELETE_ACCOUNT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_LOGOUT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_OPEN_RELOCATE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_PROFILE_OFFLINE;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.IMAGES;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_BASE_URL;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.RELOCATE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.CERTIFICATE;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.EDUCATION;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.EXPERIENCE;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.INTRODUCTION;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.JOB_TYPE;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.LANGUAGES;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewType.SKILLS;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewVisibility.MAKE_GONE;
import static com.app.qualiffyapp.ui.activities.profile.jobseeker.interfaces.IJobSeekerProfile.ViewVisibility.MAKE_VISIBLE;

public class JobSeekerProfilePresenter implements EducationAdapter.OnEducationEditListener, ImagesAdapter.AddImageListener {

    public static final int REQUEST_CODE_ADD_EDUCATION = 46;
    public static final int REQUEST_CODE_UPDATE_IMAGES = 42;
    public static final int REQUEST_CODE_UPDATE_JOB_SEEKER_INFO = 44;
    private IJobSeekerProfile jobseekerProfile;
    private SignUpInteractor interactor;

    private ApiListener<JobSeekerProfileResponse> jobSeekerApiListener = new ApiListener<JobSeekerProfileResponse>() {
        @Override
        public void onApiSuccess(JobSeekerProfileResponse response, int apiFlag) {
            if (response.prfl.edu != null && response.prfl.edu.size() > 0)
                jobseekerProfile.expandableMode(EDUCATION, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(EDUCATION, MAKE_GONE);

            if (response.crt != null && response.crt.size() > 0)
                jobseekerProfile.expandableMode(CERTIFICATE, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(CERTIFICATE, MAKE_GONE);

            if (response.expInfo.exps != null && response.expInfo.exps.size() > 0)
                jobseekerProfile.expandableMode(EXPERIENCE, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(EXPERIENCE, MAKE_GONE);
            if (response.prfl.bio != null && !TextUtils.isEmpty(response.prfl.bio.trim())) {
                jobseekerProfile.expandableMode(INTRODUCTION, MAKE_VISIBLE);
            } else jobseekerProfile.expandableMode(INTRODUCTION, MAKE_GONE);

            if (response.prfl.skills != null && response.prfl.skills.size() > 0)
                jobseekerProfile.expandableMode(SKILLS, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(SKILLS, MAKE_GONE);

            if (response.prfl.lngs != null && response.prfl.lngs.size() > 0)
                jobseekerProfile.expandableMode(LANGUAGES, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(LANGUAGES, MAKE_GONE);

            if (response.prfl.job_type != null && response.prfl.job_type.size() > 0)
                jobseekerProfile.expandableMode(JOB_TYPE, MAKE_VISIBLE);
            else jobseekerProfile.expandableMode(JOB_TYPE, MAKE_GONE);


            if (response.prfl != null)
                jobseekerProfile.onFetchProfileDetails(response);

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            jobseekerProfile.apiError(error);
            jobseekerProfile.onFetchProfileDetails(null);
        }
    };
    private ApiListener<ResponseModel<MsgResponseModel>> apiListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response.getRes() != null) {
                jobseekerProfile.logoutResponse(apiFlag);
            } else {
                if (apiFlag == API_FLAG_JS_LOGOUT && response.err.errCode == 2)
                    jobseekerProfile.logoutResponse(apiFlag);
                else
                    jobseekerProfile.apiError(response.err.msg);
            }
        }


        @Override
        public void onApiError(String error, int apiFlag) {
            jobseekerProfile.apiError(error);
        }
    };
    private ApiListener<MsgResponseModel> toogleApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            jobseekerProfile.toogleResponse(response.msg, SUCCESS, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            jobseekerProfile.toogleResponse(error, FAILURE, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            jobseekerProfile.toogleResponse(null, -12, apiFlag);

        }
    };
    private ApiListener<WebResponseModel> webApiListener = new ApiListener<WebResponseModel>() {
        @Override
        public void onApiSuccess(WebResponseModel response, int apiFlag) {
            jobseekerProfile.onFetchPortfolio(null, response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            jobseekerProfile.onFetchPortfolio(error, null, apiFlag);
        }


    };


    public JobSeekerProfilePresenter(IJobSeekerProfile jobseekerProfile) {
        this.jobseekerProfile = jobseekerProfile;
        interactor = new SignUpInteractor();
    }

    public void logoutHitApi() {
        interactor.jobsSeekerlogout(apiListener, API_FLAG_JS_LOGOUT);
    }

    public void deleteAccountHitApi() {
        interactor.jobsSeekerDeleteAccount(apiListener, API_FLAG_JS_DELETE_ACCOUNT);
    }

    public void fetchUserDetails() {
        jobseekerProfile.preFetch();
        interactor.getJobSeekerDetails(jobSeekerApiListener, API_FLAG_JOB_SEEKER);
    }

    public void manageVisibility(IJobSeekerProfile.ViewType type, View view) {
        if (view.getVisibility() == View.VISIBLE) {
            jobseekerProfile.setExpandState(type, MAKE_GONE);
        } else {
            jobseekerProfile.setExpandState(type, MAKE_VISIBLE);

        }

    }

    public Bundle getBundle() {
        Bundle bundle = new Bundle();
        bundle.putString(FROM, ProfileFragment.class.getName());
        return bundle;
    }


    @Override
    public void onEditPencilClick(JobSeekerEducation edu) {
        Bundle bundle = getBundle();
        bundle.putParcelable(ProfileFragment.EDUCATION, edu);
        jobseekerProfile.openActivityToUpdate(JobSeekerEducationActivity.class, bundle, REQUEST_CODE_ADD_EDUCATION);
    }

    public void addEducation() {
        jobseekerProfile.openActivityToUpdate(JobSeekerEducationActivity.class, getBundle(), REQUEST_CODE_ADD_EDUCATION);
    }

    @Override
    public void openEditImageScreen(String baseUrl, LinkedHashMap<String, String> imageList) {
        Bundle b = new Bundle();
        b.putString(IMAGE_BASE_URL, baseUrl);
        b.putString(IMAGES, Utility.gsonInstance().toJson(imageList));
        b.putBoolean(IS_JOBSEEKER_UPDATE, true);
        jobseekerProfile.openActivityToUpdate(AddMediaActivity.class, b, REQUEST_CODE_UPDATE_IMAGES);
    }

    @Override
    public void openViewImage(String baseUrl, LinkedList<String> imgs, int pos) {


        jobseekerProfile.openActivity(baseUrl, imgs, pos);

    }

    public void editInfo(String info) {
        Bundle bundle = getBundle();
        bundle.putString(ProfileFragment.INFORMATION, info);
        jobseekerProfile.openActivityToUpdate(JobSeekerInfoActivity.class, bundle, REQUEST_CODE_UPDATE_JOB_SEEKER_INFO);
    }


    // hit api for HELP & FAQ
    public void getCompanyPortfolio(CompanyPortfolioType type, int apiFlag) {
        jobseekerProfile.preFetch();
        interactor.getCompanyPortFolio(webApiListener, type, apiFlag);
    }


    //hitApi for Relocate,publicProfile,manage Notification,profile offline
    public void hitapiForToogles(Boolean isChecked, int toogleType) {
        RequestBean bean = new RequestBean();
        switch (toogleType) {
            case RELOCATE:
                bean.reloc = isChecked;
                interactor.jobsSeekerProfileToogle(toogleApiListener, bean, API_FLAG_JS_OPEN_RELOCATE);
                break;
            case API_FLAG_JS_PROFILE_OFFLINE:
                bean.offline = isChecked;
                interactor.jobsSeekerProfileToogle(toogleApiListener, bean, API_FLAG_JS_PROFILE_OFFLINE);
                break;

        }

    }


}
