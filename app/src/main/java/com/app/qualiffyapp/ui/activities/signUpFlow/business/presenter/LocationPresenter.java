package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ILocationFragment;

public class LocationPresenter {
    private ILocationFragment locationFragment;

    public LocationPresenter(ILocationFragment locationFragment) {
        this.locationFragment = locationFragment;

    }

    public void proceedWithLocation() {
        locationFragment.askLocationPermission();
    }

    public void gotoNextStep() {
        locationFragment.proceed();
    }
}
