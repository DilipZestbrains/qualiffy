package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.content.Intent;
import android.view.View;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.signupLogin.SpannableClickCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.FragmentBusinessSignupBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.ui.activities.WebViewActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessSignUpFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessSignUpPresenter;
import com.app.qualiffyapp.utils.SpannableTxt;
import com.app.qualiffyapp.utils.Utility;
import com.google.i18n.phonenumbers.PhoneNumberUtil;

public class BusinessSignupFragment extends BLoginSignUpBaseFragment implements IBusinessSignUpFragment, SpannableClickCallback {
    private FragmentBusinessSignupBinding mBinding;
    private BusinessSignUpPresenter mPresenter;

    @Override
    protected void initUi() {
        mBinding = (FragmentBusinessSignupBinding) viewDataBinding;
        mPresenter = new BusinessSignUpPresenter(this);

        setSpan();
        setListener();

    }

    protected void setListener() {
        mBinding.signupBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup_btn) {
            getLoginActivity().isLogin = false;
            mPresenter.SignUp(getBean(), mBinding.tncCheckbox.isChecked());
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_business_signup;
    }


    private void setSpan() {
        new SpannableTxt().setSpannableString(getString(R.string.already_have_account),
                mBinding.signupTxt,
                getResources().getColor(R.color.colorTextBlack),
                getResources().getColor(R.color.grey_6b6d6d),
                25, this,
                true,
                SPAN_LOGIN);
        new SpannableTxt().setSpannableString(getString(R.string.tnc),
                mBinding.tncTxt,
                getResources().getColor(R.color.progress_red),
                getResources().getColor(R.color.colorTextBlack),
                13, this,
                false,
                SPAN_TERMS_N_COND);
    }

    @Override
    public void spannableStringClick(int flag) {
        if (flag == SpannableClickCallback.SPAN_LOGIN) {
            if (getLoginActivity() != null)
                getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.LOGIN, false, true);
        }
        if (flag == SpannableClickCallback.SPAN_TERMS_N_COND) {
            if (getActivity() != null)
                startActivity(new Intent(getActivity(), WebViewActivity.class));
        }
    }

    @Override
    public void onLogin(String message, String otp) {
        Utility.hideKeyboard(context);
        showProgress(false);
        Utility.showToast(context, message);

        if (otp != null) {
            if (getLoginActivity() != null) {
                getLoginActivity().otp = otp;
                getLoginActivity().getPresenter().gotoFragment(BusinessLoginPresenter.DestinationFragment.VERIFY, true, true);
            }
        }
    }

    @Override
    public void onPreLogin() {
        showProgress(true);
    }


    @Override
    public void showToast(ToastType status) {
        switch (status) {
            case EMAIL_EMPTY:
                Utility.showToast(context, "Email can't be empty.");
                break;
            case EMAIL_FORMAT_ERROR:
                Utility.showToast(context, getContext().getResources().getString(R.string.please_enter_valid_email));

                break;
            case PASSWORD_EMPTY:
                Utility.showToast(context, "Password can't be empty.");

                break;
                case INVALID_PASSWORD:
                Utility.showToast(context, getContext().getResources().getString(R.string.pass_length));

                break;
            case MOBILE_EMPTY:
                Utility.showToast(context, "Mobile number can't be empty.");

                break;
            case MOBILE_ERROR:

                Utility.showToast(context, getResources().getString(R.string.mobile_error_alert));

                break;
            case TC_NOT_SELECTED:
                Utility.showToast(context, getResources().getString(R.string.please_select_term_and_condition));

                break;
        }
    }

    private RequestBean getBean() {
        RequestBean requestBean = getLoginActivity().getRequestBean();
        requestBean.pno = mBinding.etPhone.getText().toString();
        requestBean.c_code = mBinding.etlPhone.getSelectedCountryCode();
        requestBean.c_name =  mBinding.etlPhone.getSelectedCountryNameCode();
        if (mBinding.etEmail.getText() != null)
            requestBean.email = mBinding.etEmail.getText().toString();
        if (mBinding.etPassword.getText() != null)
            requestBean.pass = mBinding.etPassword.getText().toString();
        requestBean.dev_tkn = Utility.getStringSharedPreference(getActivity(), AppConstants.PREFS_ACCESS_TOKEN);
        return requestBean;
    }

    @Override
    protected String getHeader() {
        return getResources().getString(R.string.SIGNUP_TITLE);
    }
}
