package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessLoginFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS;

public class BusinessLoginFragmentPresenter {
    private IBusinessLoginFragment businessLoginFragment;
    private SignUpInteractor interactor;

    private ApiListener<SingupResponse> loginApiListener = new ApiListener<SingupResponse>() {
        @Override
        public void onApiSuccess(SingupResponse response, int apiFlag) {
            businessLoginFragment.OnLogin(response.getMsg(), response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessLoginFragment.OnLogin(error, null);
        }
    };

    private ApiListener<VerifyResponseModel> loginEmailApiListener = new ApiListener<VerifyResponseModel>() {
        @Override
        public void onApiSuccess(VerifyResponseModel response, int apiFlag) {
            businessLoginFragment.OnEmailLogin(response.msg, response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            businessLoginFragment.OnLogin(error, null);
        }
    };


    public BusinessLoginFragmentPresenter(IBusinessLoginFragment businessLoginFragment) {
        this.businessLoginFragment = businessLoginFragment;
        interactor = new SignUpInteractor();
    }

//    public void organisationStrength(IBusinessSize.BusinessType type) {
//        switch (type) {
//            case LARGE:
//                businessLoginFragment.manageFooterVisibility(false);
//                break;
//            case SMALL:
//                businessLoginFragment.manageFooterVisibility(true);
//                break;
//        }
//    }

    public void login(String email, String pass, String accessToken) {
        if (isValid(email, pass)) {
            businessLoginFragment.preLogin();
            interactor.businessLargeOrgLogin(loginEmailApiListener, email, pass, accessToken
                    , API_FLAG_BUSINESS);
        }
    }

    public void loginWithPhone(RequestBean requestBean) {
        if (isValidPhone(requestBean.pno)) {
            businessLoginFragment.preLogin();
            interactor.businessLargeOrgLoginWithPhone(loginApiListener, requestBean, API_FLAG_BUSINESS);
        }
    }

    private boolean isValid(String email, String pass) {

        if (email == null || TextUtils.isEmpty(email)) {
            businessLoginFragment.showToast(ToastType.EMAIL_EMPTY);
            return false;
        }
        if (!email.matches(Utility.EMAIL_PATTERN)) {
            businessLoginFragment.showToast(ToastType.EMAIL_FORMAT_ERROR);
            return false;
        }
        if (pass == null || TextUtils.isEmpty(pass)) {
            businessLoginFragment.showToast(ToastType.PASSWORD_EMPTY);
            return false;
        }
         if (pass != null && pass.length()<6) {
            businessLoginFragment.showToast(ToastType.PASSWORD_ERROR);
            return false;
        }



        return true;
    }

    private boolean isValidPhone(String pno) {

        if (pno == null || TextUtils.isEmpty(pno)) {
            businessLoginFragment.showToast(ToastType.MOBILE_EMPTY);
            return false;
        }
        return true;
    }
}
