package com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_JOB_LIST;

public class OrganizationJobListPresenter extends ApplyJobPresenter {
    private SignUpInteractor interactor;
    private SignupView signupView;
    ApiListener<ResponseModel<OrgJobListResponseModel>> apiListener = new ApiListener<ResponseModel<OrgJobListResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<OrgJobListResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };


    public OrganizationJobListPresenter(SignupView view) {
        super(view);
        this.signupView = view;
        this.interactor = new SignUpInteractor();
    }

    public void getJobList(String orgId, int pageNo) {
        if (orgId != null)
            interactor.jobListApi(apiListener, orgId, pageNo, API_FLAG_JS_JOB_LIST);

    }

}
