package com.app.qualiffyapp.ui.activities.profile.businessProfile.view;

import android.content.Intent;
import android.media.audiofx.DynamicsProcessing;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.ImagesAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.constants.CompanyPortfolioType;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.BusinessProfileFragBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.eventBusModel.Profile;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.models.profile.NotifBean;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.WebViewActivity;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces.IBusinessProfileInterface;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters.BusinessProfilePresenter;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.PaymentHistoryActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.PreActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserVideoActivity;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.locationUtils.ConvertLatLngToAddress;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.LinkedList;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_FAQ;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_HELP;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT_UNREGISTER;
import static com.app.qualiffyapp.constants.AppConstants.COMPANY_STRENGTH;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.FAQ;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.HEADER;
import static com.app.qualiffyapp.constants.AppConstants.HELP;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_UPDATE_VIDEO;
import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.THUMBNAIL;
import static com.app.qualiffyapp.constants.SharedPrefConstants.HAS_VIDEO;
import static com.app.qualiffyapp.customViews.DialogInitialize.DELETE_ACCOUNT_FLAG;
import static com.app.qualiffyapp.customViews.DialogInitialize.LOGOUT_FLAG;
import static com.app.qualiffyapp.customViews.DialogInitialize.MAKE_OFFLINE_FLAG;
import static com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity.DataType.ORGANISATION_FOLLOWERS;
import static com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity.DataType.ORGANISATION_RECOMMEND;
import static com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity.DataType.ORGANISATION_UNRECOMMEND;

public class BusinessProfileFragment extends BaseFragment implements IBusinessProfileInterface, yesNoCallback {
    public static final int REGISTRATION_NUMBER_UPDATE_REQUEST_CODE = 102;
    public static final int NOTIFICATION_REQUEST_CODE = 105;
    public static final int REQUEST_CODE_UPDATE_IMAGES = 106;
    public static final int UPDATE_BUSINESS_LOGO = 1007;
    private static final int DESCRIPTION_UPDATE_REQUEST_CODE = 103;
    private static final int COMPANY_NAME_UPDATE_REQUEST_CODE = 104;
    private BusinessProfileFragBinding mBinding;
    private BusinessProfilePresenter mPresenter;
    private ImagesAdapter imagesAdapter;
    private String localImage = null;
    private BusinessProfileResponseModel profile;
    private DialogInitialize dialogInitialize;
    private User user;
    private int orgType = -1;
    private final String VIDEO = "video";
    private final String VIDEO_BASE_URL = "videoBaseUrl";
    int rec, unRec, follower;

    private CompoundButton.OnCheckedChangeListener profileToggleListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            mPresenter.manageProfileVisibility(isChecked);
            mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(null);
        }
    };

    @Override
    protected void initUi() {
        mBinding = (BusinessProfileFragBinding) viewDataBinding;
        mPresenter = new BusinessProfilePresenter(this);
        dialogInitialize = new DialogInitialize();
        orgType = Utility.getIntFromSharedPreference(getContext(), AppConstants.ORG_TYPE);
        user = new User();
        bindAdapters();
        mPresenter.fetchUserDetails(orgType);
    }

    private void bindAdapters() {
        imagesAdapter = new ImagesAdapter(new LinkedList<>(), null);
        imagesAdapter.setAddImageListener(mPresenter);
        mBinding.rvImages.setAdapter(imagesAdapter);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.business_profile_frag;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAddressTitle:
                manageExpandable(mBinding.ivExpandAddress, mBinding.tvAdrs);
                break;
            case R.id.tv_descriptionTitle:
                manageExpandable(mBinding.ivExpDesc, mBinding.tvDescription);
                break;
            case R.id.tvRegisterNoTitle:
                manageExpandable(mBinding.ivExpRegistration, mBinding.tvRegisteration);
                break;
            case R.id.tv_companyTitle:
                manageExpandable(mBinding.ivExpandCompany, mBinding.tvCompany);
                break;
            case R.id.tvImageHeading:
                manageExpandable(mBinding.ivECImages, mBinding.imgRlayout);
                break;
            case R.id.ivEditAddressNo:
                if (profile != null)
                    mPresenter.openAddressFragment(profile.org.address, profile.org.cntry, profile.org.state, profile.org.loc.get(0).toString(), profile.org.loc.get(1).toString());
                break;
            case R.id.tvLogOut:
                dialogInitialize.prepareLogoutDialog(new CustomDialogs(getActivity(), this));
                break;
            case R.id.ivEditPencilIntroduction:
                mPresenter.openLoginActivityFrag(BusinessLoginPresenter.DestinationFragment.COMPANY_DESCRIPTION, mBinding.tvDescription.getText().toString(), DESCRIPTION_UPDATE_REQUEST_CODE);
                break;
            case R.id.ivEditCompany:
                mPresenter.openCompanyDetails(profile.org.name, profile.org.empls);
                break;
            case R.id.tvChngPass:
                openActivity(ChangePasswordActivity.class, null);
                break;
            case R.id.tvSubscription:
                openActivity(SubscriptionActivity.class, null);
                break;
            case R.id.tvMyPayments:
                openActivity(PaymentHistoryActivity.class, null);
                break;
            case R.id.tvDeleteAccount:
                if (profile.org.offline || mBinding.toggleMakeProfileOffline.isChecked())
                    dialogInitialize.prepareDeleteDialog(new CustomDialogs(getActivity(), this));
                else dialogInitialize.makeOfflineDialog(new CustomDialogs(getActivity(), this));

                break;
            case R.id.tvFAQ:
                mPresenter.getCompanyPortfolio(CompanyPortfolioType.FAQ, API_FLAG_BUSINESS_FAQ);
                break;
            case R.id.tvHelp:
                mPresenter.getCompanyPortfolio(CompanyPortfolioType.HELP, API_FLAG_BUSINESS_HELP);
                break;
            case R.id.tvManageNotifiaction:
                mPresenter.openNotificationActivity(profile.org.notif);
                break;
            case R.id.add_img_card:
                mPresenter.openEditImageScreen(profile.org.imgs, profile.bP);
                break;
            case R.id.editLogo:
                String url;
                if (localImage != null && !TextUtils.isEmpty(localImage))
                    url = localImage;
                else url = profile.bP + profile.org.img;
                mPresenter.openLoginActivityFrag(BusinessLoginPresenter.DestinationFragment.COMPANY_LOGO, url, UPDATE_BUSINESS_LOGO);
                break;
            case R.id.ivFollower:
                if (follower != 0) {
                    Bundle b = new Bundle();
                    b.putSerializable(FROM, ORGANISATION_FOLLOWERS);
                    openActivity(RecommendListActivity.class, b);
                } else
                    Utility.showToast(getContext(), getContext().getResources().getString(R.string.you_have_no_followers));
                break;
            case R.id.ivRecommand:
                if (orgType == 2) {
                    if (follower != 0) {
                        Bundle b = new Bundle();
                        b.putSerializable(FROM, ORGANISATION_FOLLOWERS);
                        openActivity(RecommendListActivity.class, b);
                    } else if (getContext() != null)
                        Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_followers));
                } else {
                    if (rec != 0) {
                        Bundle b1 = new Bundle();
                        b1.putSerializable(FROM, ORGANISATION_RECOMMEND);
                        openActivity(RecommendListActivity.class, b1);
                    } else if (getContext() != null)
                        Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_recommend));

                }
                break;
            case R.id.ivNotRecommand:
                if (unRec != 0) {
                    int subscribeFlag=Utility.getIntFromSharedPreference(getContext(),SUBSCRIPTION_FLAG);
                    if(subscribeFlag==0 || subscribeFlag==4) {
                        Bundle b1 = new Bundle();
                        b1.putSerializable(FROM, ORGANISATION_UNRECOMMEND);
                        openActivity(RecommendListActivity.class, b1);
                    }else{
                        String msg="";
                        if (subscribeFlag == 2)
                            msg = getContext().getResources().getString(R.string.purchase_plan_dialog_body);
                        else if (subscribeFlag == 1)
                            msg = getContext().getResources().getString(R.string.expire_plan_dialog_body);
                        Utility.showToast(getContext(), msg);                    }

                } else
                    Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_nonrecommend));
                break;

            case R.id.tvVideoHeading:
            case R.id.ivECVideo:
                manageExpandable(mBinding.ivECVideo, mBinding.ivVideo);
                break;
            case R.id.ivVideo:
                if (profile != null) {
                    Bundle bundle = new Bundle();
                    if (profile.org.video != null && profile.bP != null) {
                        bundle.putString(VIDEO, profile.org.video);
                        bundle.putString(VIDEO_BASE_URL, profile.bP);
                    }
                    bundle.putBoolean(IS_BUSINESS_UPDATE, true);
                    openActivityForResult(UserVideoActivity.class, bundle, REQUEST_CODE_UPDATE_VIDEO);
                }
                break;
        }
    }

    private void manageExpandable(View arrow, View textView) {
        if (arrow.isSelected()) {
            textView.setVisibility(View.GONE);
            arrow.setSelected(false);
        } else {
            textView.setVisibility(View.VISIBLE);
            arrow.setSelected(true);

        }
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.tvLogOut.setOnClickListener(this);
        mBinding.tvRegisterNoTitle.setOnClickListener(this);
        mBinding.tvAddressTitle.setOnClickListener(this);
        mBinding.tvDescriptionTitle.setOnClickListener(this);
        mBinding.tvImageHeading.setOnClickListener(this);
        mBinding.tvCompanyTitle.setOnClickListener(this);
        mBinding.ivEditAddressNo.setOnClickListener(this);
        mBinding.ivEditPencilIntroduction.setOnClickListener(this);
        mBinding.ivEditCompany.setOnClickListener(this);
        mBinding.tvChngPass.setOnClickListener(this);
        mBinding.tvSubscription.setOnClickListener(this);
        mBinding.tvMyPayments.setOnClickListener(this);
        mBinding.tvDeleteAccount.setOnClickListener(this);
        mBinding.tvFAQ.setOnClickListener(this);
        mBinding.tvHelp.setOnClickListener(this);
        mBinding.tvManageNotifiaction.setOnClickListener(this);
        mBinding.addImgCard.setOnClickListener(this);
        mBinding.editLogo.setOnClickListener(this);
        mBinding.ivFollower.setOnClickListener(this);
        mBinding.ivRecommand.setOnClickListener(this);
        mBinding.ivNotRecommand.setOnClickListener(this);
        mBinding.tvVideoHeading.setOnClickListener(this);
        mBinding.ivECVideo.setOnClickListener(this);
        mBinding.ivVideo.setOnClickListener(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_UPDATE_VIDEO:
                    profile.org.video = data.getStringExtra(THUMBNAIL);
                    if (TextUtils.isEmpty(profile.org.video)) {
                        if (getContext() != null)
                            Utility.putBooleanValueInSharedPreference(getContext(), HAS_VIDEO, false);
                        mBinding.ivVideo.setImageResource(R.drawable.ic_video_placeholder);

                    } else {
                        GlideUtil.loadImage(mBinding.ivVideo, profile.bP + profile.org.video, R.drawable.ic_video_placeholder, 1000, 28);
                        if (getContext() != null)
                            Utility.putBooleanValueInSharedPreference(getContext(), HAS_VIDEO, true);
                    }
                    break;

                case COMPANY_NAME_UPDATE_REQUEST_CODE:
                    if (data.getExtras() != null) {
                        String name = data.getExtras().getString(DATA_TO_SEND, "");
                        String strength = data.getExtras().getString(COMPANY_STRENGTH);
                        fireEvent(name, null);
                        String companyDetails = name + " (" + strength + ")";
                        mBinding.tvCompany.setText(companyDetails);
                        mBinding.tvName.setText(name);
                        profile.org.empls = strength;
                        profile.org.name = name;
                    }
                    break;
                default:
                    mPresenter.setOnActivityResultData(requestCode, data.getExtras());
                    break;
            }

        }
    }

    @Override
    public void onYesClicked(String from) {
        switch (from) {
            case LOGOUT_FLAG:
                mPresenter.logoutAccount(Utility.getStringSharedPreference(getActivity(),
                        AppConstants.PREFS_ACCESS_TOKEN));
                AppRepository.getInstance(getContext()).deleteContacts();
                break;
            case DELETE_ACCOUNT_FLAG:
                mPresenter.deleteAccount();
                break;
            case MAKE_OFFLINE_FLAG:
                mPresenter.manageProfileVisibility(true);
                mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(null);
                mBinding.toggleMakeProfileOffline.setChecked(true);
                break;

        }
    }

    @Override
    public void onNoClicked(String from) {
        if (MAKE_OFFLINE_FLAG.equals(from)) {
            mPresenter.deleteAccount();
        }
    }

    private void clearPrefAndLogout() {
        Utility.clearAllSharedPrefData(context);
        AppFirebaseDatabase.clearFirebaseInstance();
        if (getContext() != null)
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(new Intent(CALL_INTENT_UNREGISTER));
        openActivity(PreActivity.class, null);
        if (getActivity() != null)
            getActivity().finishAffinity();

    }

    @Override
    public void openActivityForUpdate(Class<?> activity, Bundle bundle, int requestCode) {
        openActivityForResult(activity, bundle, requestCode);
    }

    @Override
    public void openActivity(String baseUrl, LinkedList<String> imgs, int pos) {
        Intent viewMedia = new Intent(getContext(), StoryViewActivity.class);
        viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(imgs));
        viewMedia.putExtra("BASE_URL_IMAGE", baseUrl);
        startActivity(viewMedia);

    }

    @Override
    public void setTextViewValue(TextViewType type, Object data1) {
        String data = String.valueOf(data1);
        switch (type) {
            case DESCRIPTION:
                mBinding.tvDescription.setText(data);
                break;
            case COMPANY_NAME:
                fireEvent(data, null);
                mBinding.tvCompany.setText(data);
                mBinding.tvName.setText(data);
                break;
            case REGISTRATION_NUMBER:
                mBinding.tvRegisteration.setText(data);
                break;
            case ADDRESS:
                if (data1 instanceof ConvertLatLngToAddress.AddressBean) {
                    ConvertLatLngToAddress.AddressBean addressBean = (ConvertLatLngToAddress.AddressBean) data1;
                    mBinding.tvAdrs.setText(addressBean.state);
                    mBinding.tvAddress.setText(addressBean.state);
                    profile.org.state = addressBean.state;
                    profile.org.cntry = addressBean.cntry;
                    profile.org.address = addressBean.addr;
                    profile.org.loc = new ArrayList<>();
                    if (addressBean.lat != null)
                        profile.org.loc.add(0, Double.valueOf(addressBean.lat));
                    if (addressBean.lng != null)
                        profile.org.loc.add(1, Double.valueOf(addressBean.lng));
                }
                break;
            case LOGO:
                localImage = data;
                fireEvent(null, data);
                GlideUtil.loadCircularImage(mBinding.ivProfilePic, data, R.drawable.profile_placeholder);
                break;
        }
    }

    private void fireEvent(String name, String img) {
        Profile profile = new Profile();
        boolean isReady = false;
        if (name != null) {
            profile.setName(name);
            isReady = true;
        }
        if (img != null) {
            profile.setImg(img);
            isReady = true;
        }
        if (isReady)
            EventBus.getDefault().postSticky(profile);
    }

    @Override
    public void setAdapter(AdapterType type, LinkedList<String> list) {
        switch (type) {
            case IMAGES:
                profile.org.imgs = list;
                imagesAdapter.refreshList(list, profile.bP);
                if (list.size() >= 10) {
                    hideAddImage(true);
                } else {
                    hideAddImage(false);
                }
                break;
            case ViewMatches:

                break;
        }
    }

    private void hideAddImage(boolean isHide) {
        if (isHide) {
            mBinding.addImgCard.setVisibility(View.GONE);
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mBinding.rvImages.getLayoutParams();
            lp.constrainedWidth = false;
        } else {
            mBinding.addImgCard.setVisibility(View.VISIBLE);
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) mBinding.rvImages.getLayoutParams();
            lp.constrainedWidth = true;
        }
    }

    @Override
    public void onFetchProfile(String msg, BusinessProfileResponseModel res) {
        showProgress(false);
        if (res != null) {
            this.profile = res;
            if (orgType != 2)
                fireEvent(res.org.name, res.bP + res.org.img);
            else fireEvent(null, res.bP + res.org.img);
            rec = res.r_count;
            unRec = res.unr_count;
            follower = res.fl_count;

            user.setName(res.org.name);
            user.setPhone(String.valueOf(res.org.pno));
            user.setImg(res.bP + res.org.img);
            mBinding.tvName.setText(res.org.name);
            mBinding.tvCompany.setText(res.org.name);
            String add = res.org.state;
            mBinding.tvAddress.setText(add);
//            mBinding.tvAdrs.setText(res.org.address);
            mBinding.tvAdrs.setText(add);
            if (res.org == null || res.org.bio == null || TextUtils.isEmpty(res.org.bio)) {
                mBinding.ivExpDesc.setVisibility(View.GONE);
            } else
                mBinding.tvDescription.setText(res.org.bio);
            mBinding.tvRegisteration.setText(res.org.reg);
            mBinding.toggleMakeProfileOffline.setChecked(res.org.offline);
            mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(profileToggleListener);
            String companyDetails = res.org.name + " (" + res.org.empls + ")";
            mBinding.tvCompany.setText(companyDetails);
            imagesAdapter.refreshList(res.org.imgs, res.bP);
            if (res.org.imgs.size() >= 10)
                hideAddImage(true);
            GlideUtil.loadCircularImage(mBinding.ivProfilePic, res.bP + res.org.img, R.drawable.profile_placeholder);
            if (res.org.video != null)
                GlideUtil.loadImage(mBinding.ivVideo, profile.bP +res.org.video, R.drawable.ic_video_placeholder, 1000, 28);

            AppFirebaseDatabase.getInstance().updateUser(res.org.name, res.bP + res.org.img, String.valueOf(res.org.pno));
            setRecruiterData();


            if (orgType == 2) {
                mBinding.tvRecommend.setText(String.valueOf(res.fl_count));
                mBinding.ivRecommand.setImageResource(R.drawable.ic_profile_followers);

            } else {
                mBinding.tvNotRecommand.setText(String.valueOf(res.unr_count));
                mBinding.tvFollower.setText(String.valueOf(res.fl_count));
                mBinding.tvRecommend.setText(String.valueOf(res.r_count));
            }


        }
    }

    private void setRecruiterData() {
        if (orgType == 2) {
            mBinding.ivEditCompany.setVisibility(View.GONE);
            mBinding.ivEditAddressNo.setVisibility(View.GONE);
            mBinding.ivEditPencilIntroduction.setVisibility(View.GONE);
            mBinding.editLogo.setVisibility(View.GONE);
            mBinding.tvSubscription.setVisibility(View.GONE);
            mBinding.tvMyPayments.setVisibility(View.GONE);
            mBinding.addImgCard.setVisibility(View.GONE);
//


            mBinding.ivFollower.setVisibility(View.INVISIBLE);
            mBinding.ivNotRecommand.setVisibility(View.INVISIBLE);
            mBinding.tvNotRecommand.setVisibility(View.INVISIBLE);
            mBinding.tvFollower.setVisibility(View.INVISIBLE);
            mBinding.deviderRF.setVisibility(View.INVISIBLE);
            mBinding.devider1.setVisibility(View.INVISIBLE);


        }
    }

    @Override
    public void showProgressDialog() {
        showProgress(true);
    }

    @Override
    public void onDelete(String msg, int status, int flag) {
        showProgress(false);

      /*  if (flag == API_FLAG_BUSINESS_DELETE) {
            Utility.showToast(getContext(), getResources().getString(R.string.account_deactivated_please_contact_admin));
        }*/
        clearPrefAndLogout();
    }

    @Override
    public void profileVisibility(String msg, int status) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (status != SUCCESS) {
            mBinding.toggleMakeProfileOffline.setChecked(!mBinding.toggleMakeProfileOffline.isChecked());
        } else
            profile.org.offline = mBinding.toggleMakeProfileOffline.isChecked();
        mBinding.toggleMakeProfileOffline.setOnCheckedChangeListener(profileToggleListener);

    }

    @Override
    public void onFetchPortfolio(String msg, WebResponseModel model, int apiFlag) {
        showProgress(false);
        Utility.showToast(context, msg);
        if (model != null) {
            Bundle bundle = new Bundle();
            bundle.putString(DATA_TO_SEND, model.webpage.content);
            switch (apiFlag) {
                case API_FLAG_BUSINESS_FAQ:
                    bundle.putString(HEADER, FAQ);
                    break;
                case API_FLAG_BUSINESS_HELP:
                    bundle.putString(HEADER, HELP);
                    break;

            }
            openActivity(WebViewActivity.class, bundle);
        }
    }

    @Override
    public void setNotificaitonToggle(String toggleStatus) {
        profile.org.notif = new Gson().fromJson(toggleStatus, NotifBean.class);
    }


    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.SOMETHING_WENT_WRONG) {
            Utility.showToast(context, getResources().getString(R.string.something_went_wrong));
        }
    }


}
