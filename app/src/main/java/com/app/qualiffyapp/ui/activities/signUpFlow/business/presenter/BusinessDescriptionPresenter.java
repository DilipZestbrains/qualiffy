package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IDescriptionFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.COMPANY_DESCRIPTION_EMPTY;

public class BusinessDescriptionPresenter extends UpdateBusinessDetailsPresenter {
    private IDescriptionFragment descriptionFragment;
    private SignUpInteractor interactor;
    private ApiListener<VerifyResponseModel> singupApiListener = new ApiListener<VerifyResponseModel>() {
        @Override
        public void onApiSuccess(VerifyResponseModel response, int apiFlag) {
            descriptionFragment.onRegister(response);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            descriptionFragment.showToast(error);
        }
    };


    public BusinessDescriptionPresenter(IDescriptionFragment descriptionFragment) {
        super(descriptionFragment);
        this.descriptionFragment = descriptionFragment;
        interactor = new SignUpInteractor();
    }

    public void registerBusiness(RequestBean bean) {
        if (isValid(bean.bio)) {
            descriptionFragment.preUpdate();
            interactor.businessSignup2(singupApiListener, bean, API_FLAG_BUSINESS);
        }
    }

    private boolean isValid(String bio) {
        if (bio == null || TextUtils.isEmpty(bio)) {
            descriptionFragment.showToast(COMPANY_DESCRIPTION_EMPTY);
            return false;
        }
        return true;
    }

    public void updateCompanyDescription(String des) {
        if (isValid(des)) {
            descriptionFragment.preUpdate();
            RequestBean requestBean = new RequestBean();
            requestBean.bio = des;
            updateDetails(requestBean);
        }
    }

}
