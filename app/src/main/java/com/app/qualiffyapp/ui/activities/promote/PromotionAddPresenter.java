package com.app.qualiffyapp.ui.activities.promote;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_PROMOTION_ADD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_PROMOTION_UPDATE;

public class PromotionAddPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {
    private SignUpInteractor mPromotionInteractor;
    private SignupView mView;

    public PromotionAddPresenter(SignupView view) {
        this.mView = view;
        this.mPromotionInteractor = new SignUpInteractor();
    }


    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        mView.showProgressDialog(false);
        mView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        mView.showProgressDialog(false);
        mView.apiError(error, apiFlag);
    }


    public void updatePromotion(String id, RequestBean bean) {
        mView.showProgressDialog(true);
        mPromotionInteractor.updatePromotion(this, id, bean, API_FLAG_PROMOTION_UPDATE);
    }

    public void addPromotion(RequestBean bean) {
        mView.showProgressDialog(true);
        mPromotionInteractor.addPromotion(this, bean, API_FLAG_PROMOTION_ADD);
    }

}
