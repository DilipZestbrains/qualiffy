package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.Valdations;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SOCIAL_SIGNUP;


public class SignUpPresenter implements ApiListener<ResponseModel<VerifyResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;
    private Valdations valdations;

    public SignUpPresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
        valdations = new Valdations();
    }

    @Override
    public void onApiSuccess(ResponseModel response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }

    public void hitApi(int apiFlag, RequestBean bean) {
        switch (apiFlag) {
            case API_FLAG_SIGNUP:
                mSignUpInteractor.callApi(this, bean, apiFlag);
                break;
            case API_FLAG_SOCIAL_SIGNUP:
                mSignUpInteractor.socailSignupApi(this, bean, apiFlag);
                break;
        }
    }


    public void validate(String name, String phnNum, boolean isAcceptTnC, String c_code) {
        RequestBean bean = new RequestBean();
        if (valdations.validateUserName(name) != null) {
            view.validationFailed(valdations.validateUserName(name));
            return;
        } else if (valdations.validatePhone(phnNum) != null) {
            view.validationFailed(valdations.validatePhone(phnNum));
            return;
        } else if (!isAcceptTnC) {
            view.validationFailed(ApplicationController.getApplicationInstance().getString(R.string.tNc_valid));
        } else {
            bean.username = name;
            bean.pno = phnNum;
            bean.dev_tkn = Utility.getStringSharedPreference(view.getViewContext(), AppConstants.PREFS_ACCESS_TOKEN);
            bean.c_code = c_code;
            view.showProgressDialog(true);
            hitApi(API_FLAG_SIGNUP, bean);
        }
    }
}
