package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.login;

import android.content.Intent;
import android.transition.Transition;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.signupLogin.SpannableClickCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivityLoginBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddCertificateActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddExperienceActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.LanguageActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.LocationActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.SelectIndustryActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.SignUpActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserPhotoActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserVideoActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.VerifyActivity;
import com.app.qualiffyapp.utils.SpannableTxt;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.animation.CustomAnimations;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.FacebookResultCallback;
import com.app.qualiffyapp.utils.socialMediaIntegration.facebook.FbLoginIntegrationWithoutLoginButton;
import com.app.qualiffyapp.utils.socialMediaIntegration.linkedIn.LinkedInActivity;
import com.app.qualiffyapp.utils.socialMediaIntegration.snapchat.SnapChatCallback;
import com.app.qualiffyapp.utils.socialMediaIntegration.snapchat.SnapChatLogin;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.google.firebase.iid.FirebaseInstanceId;
import com.snapchat.kit.sdk.SnapLogin;
import com.snapchat.kit.sdk.login.models.UserDataResponse;

import org.json.JSONObject;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_LOGIN;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SOCIAL_SIGNUP;
import static com.app.qualiffyapp.constants.AppConstants.LOGIN_ACCESS_TOKEN;

public class LoginActivity extends BaseActivity implements SpannableClickCallback, SignupView, FacebookResultCallback, SnapChatCallback {
    private final int LINKEDIN_REQUEST = 101;
    private Transition transition;
    private CustomAnimations customAnimations;
    private ActivityLoginBinding loginBinding;
    private SpannableTxt spannableTxt;
    private LoginPresenter loginPresenter;
    private CallbackManager callbackManager;
    private FbLoginIntegrationWithoutLoginButton fbLoginIntegration;
    private SnapChatLogin snapChatLogin;


    @Override
    protected void initView() {
        loginBinding = (ActivityLoginBinding) viewDataBinding;
        callbackManager = CallbackManager.Factory.create();
        loginPresenter = new LoginPresenter(this);

        fbLoginIntegration = new FbLoginIntegrationWithoutLoginButton();
        fbLoginIntegration.disconnectFromFacebook();

        toolbar();

        spannableTxt = new SpannableTxt();
        spannableTxt.setSpannableString(getString(R.string.dont_have_account),
                loginBinding.signupTxt,
                getResources().getColor(R.color.colorTextBlack),
                getResources().getColor(R.color.grey_6b6d6d),
                23, this, true, SPAN_SIGN_UP);
//        loginBinding.mobileNumEdt.setHint(getResources().getString(R.string.login_hint));

        firebaseToken();
    }

    private void firebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }

                    if (task.getResult() != null) {
                        String token = task.getResult().getToken();
                        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN, token);
                    }

                });
    }


    @Override
    protected void setListener() {
        super.setListener();
        loginBinding.fbBtn.setOnClickListener(this);
        loginBinding.linkedInBtn.setOnClickListener(this);
        loginBinding.snapchatBtn.setOnClickListener(this);
        loginBinding.loginBtn.setOnClickListener(this);
        loginBinding.toolbar.backBtnImg.setOnClickListener(this);
    }


    private void toolbar() {
        loginBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.LOGIN));
    }

    private void setAnimation() {
        customAnimations = new CustomAnimations();
        transition = customAnimations.buildEnterTransition(getResources().getInteger(R.integer.anim_duration_long));
        getWindow().setEnterTransition(transition);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_login;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.login_btn:
                loginPresenter.validate(loginBinding.mobileNumEdt.getText().toString());
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.fb_btn:

                fbLoginIntegration.fbLogin(this, callbackManager, this);
                break;

            case R.id.linked_in_btn:
                startActivityForResult(new Intent(this, LinkedInActivity.class), LINKEDIN_REQUEST);
                break;
            case R.id.snapchat_btn:
                SnapLogin.getAuthTokenManager(this).startTokenGrant();
                snapChatLogin = new SnapChatLogin(this, this);
                break;
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                switch (requestCode) {
                    case LINKEDIN_REQUEST:
                        RequestBean requestBean = new RequestBean();
                        requestBean.type = AppConstants.LINKEDIN_TYPE;
                        requestBean.social_id = data.getStringExtra("_id");
                        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                                AppConstants.PREFS_ACCESS_TOKEN);
//                        Utility.putStringValueInSharedPreference(this, FIRST_NAME, data.getStringExtra("fname"));
//                        Utility.putStringValueInSharedPreference(this, LAST_NAME, data.getStringExtra("lname"));
                        showProgressDialog(true);
                        loginPresenter.sociallogin(requestBean);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    public void spannableStringClick(int flag) {
        openActivity(SignUpActivity.class, null);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (snapChatLogin != null)
            snapChatLogin.snapChatUnlink(this);
        if (response.getStatus() == 1) {
            VerifyResponseModel verifyResponseModel = (VerifyResponseModel) response.getRes();
            switch (apiFlag) {
                case API_FLAG_LOGIN:
                    String userId = verifyResponseModel._id;
                    Intent i = new Intent(this, VerifyActivity.class);
                    i.putExtra("user_id", userId);
                    i.putExtra("user_phn_num", loginBinding.mobileNumEdt.getText().toString());
                    startActivity(i);
                    break;
                case API_FLAG_SOCIAL_SIGNUP:
                    Utility.putStringValueInSharedPreference(this, LOGIN_ACCESS_TOKEN, verifyResponseModel.acsTkn);
                    switch (verifyResponseModel.code) {
                        case 1:
                            openActivity(SelectIndustryActivity.class, null);
                            break;
                        case 2:
                            openActivity(LanguageActivity.class, null);
                            break;
                        case 3:
                            openActivity(AddExperienceActivity.class, null);
                            break;
                        case 4:
                            openActivity(AddCertificateActivity.class, null);
                            break;
                        case 5:
                            openActivity(AddMediaActivity.class, null);
                            break;
                        case 6:
                            openActivity(UserVideoActivity.class, null);
                            break;
                        case 7:
                            openActivity(LocationActivity.class, null);
                            break;
                        case 8:
                            saveName(verifyResponseModel);
                            Intent intent = new Intent(this, DashboardActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            break;
                        case 9:
                            openActivity(UserPhotoActivity.class, null);
                            break;

                    }
                    break;
            }
        } else {
            Utility.showToast(this, response.err.msg);
        }
        showProgressDialog(false);
    }

    private void saveName(VerifyResponseModel verifyResponseModel) {
        Utility.saveProfile(this, verifyResponseModel, false);
    }

    @Override
    public void apiError(String error, int apiFlag) {
        if (snapChatLogin != null)
            snapChatLogin.snapChatUnlink(this);
        Utility.showToast(this, error);
        showProgressDialog(false);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onFbResponse(JSONObject jsonObject) {
        Log.e("facebook response", jsonObject.toString());
        String fb_id = jsonObject.optString("id");
        RequestBean requestBean = new RequestBean();
        requestBean.type = AppConstants.FACEBOOK_TYPE;
        requestBean.social_id = fb_id;
        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                AppConstants.PREFS_ACCESS_TOKEN);
 /*       Utility.putStringValueInSharedPreference(this, FIRST_NAME, jsonObject.optString("first_name"));
        Utility.putStringValueInSharedPreference(this, LAST_NAME, jsonObject.optString("last_name"));*/

        showProgressDialog(true);
        loginPresenter.sociallogin(requestBean);
//        fbLoginIntegration.disconnectFromFacebook();
    }


    @Override
    public void onFbError(FacebookException facebookException) {
        Log.e("facebook error", facebookException.toString());

    }


    @Override
    public void getSnapChatData(UserDataResponse userDataResponse, String id) {
        RequestBean requestBean = new RequestBean();
        requestBean.type = AppConstants.SNAPCHAT_TYPE;
        requestBean.social_id = id;
        requestBean.dev_tkn = Utility.getStringSharedPreference(this,
                AppConstants.PREFS_ACCESS_TOKEN);
//        Utility.putStringValueInSharedPreference(this, FIRST_NAME, userDataResponse.getData().getMe().getDisplayName());

        showProgressDialog(true);
        loginPresenter.sociallogin(requestBean);


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (snapChatLogin != null)
            snapChatLogin.snapChatUnlink(this);
    }
}
