package com.app.qualiffyapp.ui.activities.profile.otherUser;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_VIEW_BUSINESS_PROFILE;

public class OtherBusinessPresenter implements ApiListener<ResponseModel<BusinessProfileResponseModel>> {
    private SignupView signupView;
    private SignUpInteractor signUpInteractor;

    public OtherBusinessPresenter(SignupView signupView) {
        this.signupView = signupView;
        signUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<BusinessProfileResponseModel> response, int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiError(error, apiFlag);
    }

    @Override
    public void onApiNoResponse(int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiError(null, apiFlag);
    }

    public void getBusinessProfile(String userId) {
        if (userId != null) {
            signupView.showProgressDialog(true);
            signUpInteractor.getOrgProfile(this, userId, API_FLAG_VIEW_BUSINESS_PROFILE);
        }
    }
}
