package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;


import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IRegistrationNumber extends BaseInterface, IUpdatableDetail {

    void onNext(String companyName);

}
