package com.app.qualiffyapp.ui.activities.profile.jobseeker.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivitySetting2Binding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.JobInfo;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.RoleSeekingActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.SelectIndustryActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.UserRoleActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.RoleSeekingPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_1;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUSTRY_;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUS_ID;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLE;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLESEEKING;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JS_INDUSTRY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JS_ROLE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_JS_ROLESEEKING;

public class SettingActivity extends BaseActivity implements SignupView {
    CreateJobSeekerProfileInputModel profileInputModel;
    private ActivitySetting2Binding setting2Binding;
    private JobInfo indusJobInfo;
    private JobInfo roleJobInfo;
    private ArrayList<JobInfo> roleSeekingJobInfo;
    private String roleSeekingstr;
    private RoleSeekingPresenter roleSeekingPresenter;
    private boolean isHitApi;


    @Override
    protected void initView() {
        setting2Binding = (ActivitySetting2Binding) viewDataBinding;
        roleSeekingPresenter = new RoleSeekingPresenter(this);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        getIntentData();
        toolbar();

    }

    public void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            indusJobInfo = new Gson().fromJson(bundle.getString(JS_INDUSTRY_), JobInfo.class);
            roleJobInfo = new Gson().fromJson(bundle.getString(JS_ROLE), JobInfo.class);
            roleSeekingstr = bundle.getString(JS_ROLESEEKING);

            Type roleSeektype = new TypeToken<ArrayList<JobInfo>>() {
            }.getType();

            roleSeekingJobInfo = new Gson().fromJson(roleSeekingstr, roleSeektype);

            if (indusJobInfo != null)
                profileInputModel.jobC_id = indusJobInfo.get_id();

            if (roleJobInfo != null)
                profileInputModel.myrole = roleJobInfo.get_id();
            if (profileInputModel.roleSeek == null)
                profileInputModel.roleSeek = new LinkedHashMap<>();

            if (roleSeekingJobInfo != null) {
                for (JobInfo roleseekId : roleSeekingJobInfo) {
                    profileInputModel.roleSeek.put(Utility.getFirstLetterCaps(roleseekId.getName()), roleseekId.get_id());
                }
                profileInputModel.role_skng = new ArrayList<String>(profileInputModel.roleSeek.values());
            }

            if (indusJobInfo != null && roleSeekingJobInfo != null) {
                if (roleJobInfo != null)
                    setData(indusJobInfo.getName(), roleJobInfo.getName(), roleSeekingJobInfo);
                else
                    setData(indusJobInfo.getName(), null, roleSeekingJobInfo);
            }


        }
    }


    private void setData(String indus, String role, ArrayList<JobInfo> roleSeekingList) {
        setting2Binding.currentIndusEdt.setText(Utility.getFirstLetterCaps(indus));
        if (role != null)
            setting2Binding.currentRoleEdit.setText(Utility.getFirstLetterCaps(role));

        String roleSeek = "";
        for (JobInfo jobInfo : roleSeekingList) {
            if (roleSeek == "")
                roleSeek = Utility.getFirstLetterCaps(jobInfo.getName());
            else
                roleSeek = roleSeek + ", " + Utility.getFirstLetterCaps(jobInfo.getName());
        }
        setting2Binding.currentJobSeekEdit.setText(roleSeek);

    }

    private void toolbar() {
        setting2Binding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.settings));
        setting2Binding.btn.btn.setText(getResources().getString(R.string.done));
    }

    @Override
    protected void setListener() {
        super.setListener();
        setting2Binding.toolbar.backBtnImg.setOnClickListener(this);
        setting2Binding.btn.btn.setOnClickListener(this);
        setting2Binding.currentRoleEdit.setOnClickListener(this);
        setting2Binding.currentJobSeekEdit.setOnClickListener(this);
        setting2Binding.currentIndusEdt.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_setting2;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.btn:
                if (isHitApi) {
                    showProgress(true);
                    setProfileInutModel();
                    roleSeekingPresenter.signUp1Api(profileInputModel);
                } else
                    finish();
                break;
            case R.id.currentIndusEdt:
                isHitApi = true;
                openIndustryActivity();
                break;
            case R.id.currentJobSeekEdit:
                isHitApi = true;
                openRoleSeekingActivity();
                break;
            case R.id.currentRoleEdit:
                isHitApi = true;
                openRoleActivity();
                break;
        }

    }

    private void setProfileInutModel() {
        if (profileInputModel == null)
            profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        profileInputModel.jobC_id = indusJobInfo.get_id();
        if (roleJobInfo != null)
            profileInputModel.myrole = roleJobInfo.get_id();
        else  profileInputModel.myrole="";
        profileInputModel.role_skng = new ArrayList<>();
        for (JobInfo jobInfo : roleSeekingJobInfo)
            profileInputModel.role_skng.add(jobInfo.get_id());


    }

    private void openRoleSeekingActivity() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
        if (indusJobInfo.get_id() != null)
            bundle.putString(JS_INDUS_ID, indusJobInfo.get_id());
        if (roleSeekingstr != null)
            bundle.putString(JS_ROLESEEKING, roleSeekingstr);

        openActivityForResult(RoleSeekingActivity.class, bundle, REQUEST_CODE_JS_ROLESEEKING);
    }

    private void openRoleActivity() {
        Bundle bundle = new Bundle();
        bundle.putString(JS_INDUS_ID, indusJobInfo.get_id());
        if (roleJobInfo != null)
            bundle.putString(JS_ROLE, new Gson().toJson(roleJobInfo));
        bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
        openActivityForResult(UserRoleActivity.class, bundle, REQUEST_CODE_JS_ROLE);
    }


    void openIndustryActivity() {
        Bundle bundle = new Bundle();
        bundle.putString(JS_INDUSTRY_, new Gson().toJson(indusJobInfo));
        bundle.putBoolean(IS_JOBSEEKER_UPDATE, true);
        openActivityForResult(SelectIndustryActivity.class, bundle, REQUEST_CODE_JS_INDUSTRY);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case REQUEST_CODE_JS_INDUSTRY:
                    JobInfo indussJobInfo = new Gson().fromJson(data.getStringExtra(JS_INDUSTRY_), JobInfo.class);
                    if (indussJobInfo != null) {
                        profileInputModel.jobC_id = indussJobInfo.get_id();
                        indusJobInfo.set_id(indussJobInfo.get_id());
                        indusJobInfo.setName(indussJobInfo.getName());
                        setting2Binding.currentIndusEdt.setText(indussJobInfo.getName());

                        if (data.getStringExtra(JS_ROLE) == null) {
                            this.roleJobInfo = null;
                            setting2Binding.currentRoleEdit.setText("");
                        }

                        setProfileData(data);
                    }
                    break;

                case REQUEST_CODE_JS_ROLE:
                    profileInputModel.jobC_id = indusJobInfo.get_id();
                    setProfileData(data);
                    break;
                case REQUEST_CODE_JS_ROLESEEKING:
                    setRoleseek();
                    break;
            }
        }

    }


    private void setRoleseek() {
        if (profileInputModel.roleSeek != null) {
            profileInputModel.role_skng = new ArrayList<String>(profileInputModel.roleSeek.values());
            String roleSeek = "";
            for (String rleSeekValues : profileInputModel.roleSeek.keySet()) {
                if (roleSeek == "")
                    roleSeek = rleSeekValues;
                else
                    roleSeek = roleSeek + ", " + rleSeekValues;
            }
            ArrayList<String> rs_name = new ArrayList<>(profileInputModel.roleSeek.keySet());
            ArrayList<String> rs_id = new ArrayList<>(profileInputModel.roleSeek.values());
            if (roleSeekingJobInfo == null)
                roleSeekingJobInfo = new ArrayList<>();
            else roleSeekingJobInfo.clear();
            for (int i = 0; i < rs_name.size(); i++) {
                JobInfo rsJobInfo = new JobInfo();
                rsJobInfo.setName(rs_name.get(i));
                rsJobInfo.set_id(rs_id.get(i));
                roleSeekingJobInfo.add(rsJobInfo);
            }
            roleSeekingstr = new Gson().toJson(roleSeekingJobInfo);
            setting2Binding.currentJobSeekEdit.setText(roleSeek);
        }
    }


    private void setProfileData(Intent data) {
        if (data.getStringExtra(JS_ROLE) != null) {
            JobInfo roleJobInfo = new Gson().fromJson(data.getStringExtra(JS_ROLE), JobInfo.class);
            profileInputModel.myrole = roleJobInfo.get_id();
            if (this.roleJobInfo == null)
                this.roleJobInfo = new JobInfo();
            this.roleJobInfo.setName(roleJobInfo.getName());
            this.roleJobInfo.set_id(roleJobInfo.get_id());
            setting2Binding.currentRoleEdit.setText(roleJobInfo.getName());
        }


        if (data.getStringExtra(JS_ROLESEEKING) != null) {
            setRoleseek();
        } else {

            ArrayList<String> roleSeekingArray = new ArrayList<>();
            if (roleSeekingJobInfo != null) {
                for (JobInfo roleSeekingJobInfo : roleSeekingJobInfo) {
                    roleSeekingArray.add(roleSeekingJobInfo.get_id());
                }
            }
            profileInputModel.role_skng = roleSeekingArray;
        }


    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        isHitApi = false;
        if (response.getStatus() == 1) {
            switch (apiFlag) {
                case API_FLAG_SIGNUP_1:
                    Intent intent = new Intent();

                    if (indusJobInfo != null)
                        intent.putExtra(JS_INDUSTRY_, new Gson().toJson(indusJobInfo));

                    if (roleJobInfo != null)
                        intent.putExtra(JS_ROLE, new Gson().toJson(roleJobInfo));

                    if (roleSeekingJobInfo != null)
                        intent.putExtra(JS_ROLESEEKING, new Gson().toJson(roleSeekingJobInfo));

                    setResult(RESULT_OK, intent);
                    finish();
                    break;
            }
        } else {
            Utility.showToast(this, response.err.msg);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        isHitApi = false;
        Utility.showToast(this, error);
        showProgressDialog(false);


    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
