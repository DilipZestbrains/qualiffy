package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessSize;

public class BusinessSizePresenter {

    private IBusinessSize businessSize;

    public BusinessSizePresenter(IBusinessSize businessSize) {
        this.businessSize = businessSize;
    }

    public void largeOrgSignup(Boolean isAsk) {
        if (isAsk)
            businessSize.showAdminSignupDialog();
        else businessSize.proceedSignup(IBusinessSize.BusinessType.LARGE);
    }

    public void smallOrgSignup() {
        businessSize.proceedSignup(IBusinessSize.BusinessType.SMALL);
    }


}
