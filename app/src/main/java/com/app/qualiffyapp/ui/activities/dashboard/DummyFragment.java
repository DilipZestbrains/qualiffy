package com.app.qualiffyapp.ui.activities.dashboard;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseFragment;

public class DummyFragment extends BaseFragment {
    @Override
    protected void initUi() {

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_blank;
    }

    @Override
    public void onClick(View v) {

    }
}
