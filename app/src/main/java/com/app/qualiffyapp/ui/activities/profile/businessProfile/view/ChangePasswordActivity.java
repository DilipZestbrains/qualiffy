package com.app.qualiffyapp.ui.activities.profile.businessProfile.view;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityChangePasswordBinding;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.interfaces.IChangePassword;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters.ChangePasswordPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ChangePasswordActivity extends BaseActivity implements IChangePassword {
    private ActivityChangePasswordBinding mBinding;
    private ChangePasswordPresenter mPresenter;

    @Override
    protected void initView() {
        mBinding = (ActivityChangePasswordBinding) viewDataBinding;
        mPresenter = new ChangePasswordPresenter(this);

    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_change_password;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDone:
                mPresenter.changePassword(mBinding.etOldPass.getText().toString(), mBinding.etNewPass.getText().toString(), mBinding.etConfirmPass.getText().toString());
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void setListener() {
        mBinding.tvDone.setOnClickListener(this);
        mBinding.header.backBtnImg.setOnClickListener(this);
    }

    private void setHeader() {
        mBinding.header.toolbarTitleTxt.setText(getResources().getString(R.string.chng_password));
    }

    @Override
    public void preChange() {
        showProgress(true);
    }

    @Override
    public void onChange(String msg, int status) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            finish();
        }
    }

    @Override
    public void showToast(ToastType toastType) {
        switch (toastType) {
            case PASS1_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_new_pass));
                break;
            case PASS2_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_confirm_pass));
                break;
            case INVALID_PASSWORD:
                Utility.showToast(this, getResources().getString(R.string.pass_length));
                break;
            case PASS_MISMATCH:
                Utility.showToast(this, getResources().getString(R.string.new_pass_confirm_pass_mismatch));
                break;
            case OLD_PASS_EMPTY:
                Utility.showToast(this, getResources().getString(R.string.please_enter_old_pass));
                break;
            case NEW_PASS_OLD_PASS_SAME:
                Utility.showToast(this, getResources().getString(R.string.new_pass_should_not_be_same_as_old_pass));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeader();
    }
}
