package com.app.qualiffyapp.ui.activities.contacts;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.contact.BlockContactResonseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BLOCK_FRND_LIST;


public class BlockContactPresenter implements ApiListener<ResponseModel<MsgResponseModel>> {
    private SignupView signupView;
    ApiListener<ResponseModel<BlockContactResonseModel>> blockContactlistener = new ApiListener<ResponseModel<BlockContactResonseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<BlockContactResonseModel> response, int apiFlag) {
            signupView.showProgressDialog(false);
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.showProgressDialog(false);
            signupView.apiError(error, apiFlag);
        }
    };
    private SignUpInteractor interactor;


    public BlockContactPresenter(SignupView signupView) {
        this.signupView = signupView;
        this.interactor = new SignUpInteractor();
    }

    public void hitBockAPI(String userId, String blockKey, int apiFlag) {
        if (userId != null) {
            signupView.showProgressDialog(true);
            interactor.jobSekekrBlockFrndApi(this, userId, blockKey, apiFlag);
        }
    }

    public void blockContactListApi(int pageNo, boolean isShownProgress) {
        if (isShownProgress)
            signupView.showProgressDialog(true);
        interactor.jobSekekrBlockFrndListApi(blockContactlistener, pageNo, API_FLAG_BLOCK_FRND_LIST);

    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        signupView.showProgressDialog(false);
        if (response != null)
            signupView.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        signupView.showProgressDialog(false);
        signupView.apiError(error, apiFlag);

    }
}
