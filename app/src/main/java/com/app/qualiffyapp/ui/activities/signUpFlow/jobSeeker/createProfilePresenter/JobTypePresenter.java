package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_2;

public class JobTypePresenter implements ApiListener<ResponseModel<MsgResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public JobTypePresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApi(CreateJobSeekerProfileInputModel profileInputModel) {
        mSignUpInteractor.signUp2Api(this, profileInputModel, API_FLAG_SIGNUP_2);

    }
}
