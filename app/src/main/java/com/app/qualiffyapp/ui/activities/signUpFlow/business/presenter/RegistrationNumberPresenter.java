package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IRegistrationNumber;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;

public class RegistrationNumberPresenter extends UpdateBusinessDetailsPresenter {
    private IRegistrationNumber registrationNumberFragment;

    public RegistrationNumberPresenter(IRegistrationNumber registrationNumberFragment) {
        super(registrationNumberFragment);
        this.registrationNumberFragment = registrationNumberFragment;
    }

    public void proceed(String registrationNumber) {
        if (isValid(registrationNumber)) {
            registrationNumberFragment.onNext(registrationNumber);
        }
    }

    private boolean isValid(String registrationNumber) {
        if (registrationNumber == null || TextUtils.isEmpty(registrationNumber.trim())) {
            registrationNumberFragment.showToast(ToastType.REGISTRATION_NUMBER_EMPTY);
            return false;
        }
        return true;
    }

    public void updateRegistrationNumber(String reg) {
//        if (isValid(reg)) {
//            registrationNumberFragment.preUpdate();
//            RequestBean requestBean = new RequestBean();
//            requestBean.reg = reg;
//            updateDetails(requestBean);
//        }

    }

}
