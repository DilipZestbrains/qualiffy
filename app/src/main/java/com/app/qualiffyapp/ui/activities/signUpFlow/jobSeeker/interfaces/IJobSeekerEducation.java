package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces;

import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IJobSeekerEducation extends BaseInterface {
    String ACTION = "action";

    void preAdd();

    void onAddEducation(String msg, JobSeekerEducation edu, Action action);

    void applyDeleteAction();


    void fillDetails(JobSeekerEducation education);

    enum Action {
        ADD, DELETE, UPDATE
    }
}
