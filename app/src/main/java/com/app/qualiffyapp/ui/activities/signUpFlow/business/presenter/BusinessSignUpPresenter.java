package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.text.TextUtils;
import android.util.Patterns;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.IBusinessSignUpFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_SIGNUP_1;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.EMAIL_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.EMAIL_FORMAT_ERROR;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.INVALID_PASSWORD;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.MOBILE_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.MOBILE_ERROR;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.PASSWORD_EMPTY;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType.TC_NOT_SELECTED;

public class BusinessSignUpPresenter {
    private IBusinessSignUpFragment iBusinessSignUpFragment;
    private ApiListener<SingupResponse> signUpResponse = new ApiListener<SingupResponse>() {
        @Override
        public void onApiSuccess(SingupResponse response, int apiFlag) {
            if (response != null) {
                iBusinessSignUpFragment.onLogin(response.getMsg(), response.getOtp());
            }
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            iBusinessSignUpFragment.onLogin(error, null);
        }
    };
    private SignUpInteractor interacter;


    public BusinessSignUpPresenter(IBusinessSignUpFragment iBusinessSignUpFragment) {
        this.iBusinessSignUpFragment = iBusinessSignUpFragment;
        interacter = new SignUpInteractor();
    }

    public void SignUp(RequestBean requestBean, Boolean isTCAgreed) {
        if (isValid(requestBean, isTCAgreed)) {
            iBusinessSignUpFragment.onPreLogin();
            interacter.businessSignUp(signUpResponse, requestBean, API_FLAG_BUSINESS_SIGNUP_1);
        }
    }

    private boolean isValid(RequestBean requestBean, Boolean agreeTC) {
        if (requestBean.email == null || TextUtils.isEmpty(requestBean.email)) {
            iBusinessSignUpFragment.showToast(EMAIL_EMPTY);
            return false;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(requestBean.email).matches()) {
            iBusinessSignUpFragment.showToast(EMAIL_FORMAT_ERROR);
            return false;
        }
        if (requestBean.pno == null || TextUtils.isEmpty(requestBean.pno)) {
            iBusinessSignUpFragment.showToast(MOBILE_EMPTY);
            return false;
        }
        if (requestBean.pno != null) {
            try {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                Phonenumber.PhoneNumber usNumberProto = phoneUtil.parse(requestBean.pno, requestBean.c_name);            //with default country
                boolean isValid = phoneUtil.isValidNumber(usNumberProto);
                if(!isValid) {
                    iBusinessSignUpFragment.showToast(MOBILE_ERROR);

                    return false;
                }

            }catch (Exception e){

            }
        }
        if (requestBean.pass == null || TextUtils.isEmpty(requestBean.pass)) {
            iBusinessSignUpFragment.showToast(PASSWORD_EMPTY);
            return false;
        }
        if (requestBean.pass!=null && requestBean.pass.length()<6 ) {
            iBusinessSignUpFragment.showToast(INVALID_PASSWORD);
            return false;
        }
        if (!agreeTC) {
            iBusinessSignUpFragment.showToast(TC_NOT_SELECTED);
            return false;
        }

        return true;
    }


}
