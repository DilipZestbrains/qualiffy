package com.app.qualiffyapp.ui.activities.subscription;

import android.view.View;

import androidx.fragment.app.FragmentActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.profile.JsSubscriptionAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.databinding.FragmentJsSubscriptionBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters.JsSubscriptionPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

public class JsSubscriptionFragment extends BaseFragment implements SignupView {
    private FragmentJsSubscriptionBinding mBinding;
    private JsSubscriptionAdapter subscriptionAdapter;
    private JsSubscriptionPresenter subscriptionPresenter;

    private void bindAdapters() {

    }

    @Override
    protected void initUi() {
        mBinding = (FragmentJsSubscriptionBinding) viewDataBinding;
        subscriptionPresenter = new JsSubscriptionPresenter(this);
        showProgressDialog(true);
        subscriptionPresenter.hitApi(1, 20);
        bindAdapters();

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_js_subscription;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    protected void setListener() {
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        if (response.getStatus() == 1) {
            JsSubscriptionModel subscriptionModel = (JsSubscriptionModel) response.getRes();
            subscriptionAdapter = new JsSubscriptionAdapter(subscriptionModel.subs);
            mBinding.rvSubscriptionList.setAdapter(subscriptionAdapter);

        } else {
            Utility.showToast(getViewContext(), response.err.msg);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getViewContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }
}
