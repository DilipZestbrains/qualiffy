package com.app.qualiffyapp.ui.activities.addedJob.presenter;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DELETE_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_EDIT_JOB;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_JOB_APPLICANTS;

public class ApplicantsPresenter {
    private SignUpInteractor interactor;
    private SignupView signupView;
    ApiListener<ResponseModel<ApplicantsResponseModel>> apiListener = new ApiListener<ResponseModel<ApplicantsResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<ApplicantsResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };

    ApiListener<ResponseModel<MsgResponseModel>> msgListener = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };





    public ApplicantsPresenter(SignupView view) {
        this.signupView = view;
        this.interactor = new SignUpInteractor();
    }

    public void getApplicantsList(String jobId, int pageNo) {
        if (jobId != null) {
            signupView.showProgressDialog(true);
            interactor.businessJobApplicantsApi(apiListener, jobId, pageNo, API_FLAG_BUSINESS_JOB_APPLICANTS);
        }

    }

    public void repostJob(String jobId) {
        if (jobId != null) {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("YYYYMMdd");
            String formattedDate = df.format(c);

            signupView.showProgressDialog(true);
            interactor.businessEditJobApi(msgListener, jobId, formattedDate, API_FLAG_BUSINESS_EDIT_JOB);
        }

    }

    public void deleteJob(String jobId) {
        if (jobId != null) {
            signupView.showProgressDialog(true);
            interactor.businessDeleteJobApi(msgListener, jobId, API_FLAG_BUSINESS_DELETE_JOB);
        }

    }
}
