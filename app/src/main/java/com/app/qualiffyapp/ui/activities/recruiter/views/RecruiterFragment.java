package com.app.qualiffyapp.ui.activities.recruiter.views;

import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.recruiter.RecruiterListAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.ItemAddListener;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.RecruiterListFragmentBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.recruiter.RecruiterListResponseModel;
import com.app.qualiffyapp.ui.activities.recruiter.presenter.RecruiterListPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_RECRUITER_LIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_RECRUITER;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class RecruiterFragment extends BaseFragment implements SignupView, ItemAddListener, RecruiterListAdapter.RecruiterListener {
    private RecruiterListFragmentBinding mBinding;
    public static final int ADD_RECRUITER_RESULT_CODE = 12123;
    private int pageNo = 1;
    private RecruiterListPresenter mPreseneter;
    private RecruiterListAdapter mAdapter;
    private List<RecruiterListResponseModel.RecrBean> recruiterList;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;
    public boolean isAdd = true;
    private RecruiterListResponseModel.RecrBean bean;
    private int adapterPosition = -1;
    DialogInitialize dialogInitialize;

    @Override
    protected void initUi() {
        mBinding = (RecruiterListFragmentBinding) viewDataBinding;
        mPreseneter = new RecruiterListPresenter(this);
        dialogInitialize = new DialogInitialize();
        hitApi(true);
    }

    @Override
    protected void setListener() {
        super.setListener();
        mBinding.rcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.rcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    mBinding.progressBar.setVisibility(View.VISIBLE);
                    hitApi(false);
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    public void hitApi(boolean fromStart) {
        setProgress(true);
        if (fromStart)
            pageNo = 1;
        mPreseneter.getrecruiterList(pageNo);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.recruiter_list_fragment;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {

        switch (apiFlag) {
            case API_FLAG_BUSINESS_RECRUITER_LIST:
                if (response.getStatus() == SUCCESS) {
                    RecruiterListResponseModel responseModel = (RecruiterListResponseModel) response.getRes();
                    if (responseModel != null && responseModel.recr != null && responseModel.recr.size() > 0) {
                        setVisiblity(true);
                        if (pageNo == 1) {
                            recruiterList = responseModel.recr;
                            mAdapter = new RecruiterListAdapter(recruiterList, getContext());
                            mAdapter.setRecruiterListener(this);
                            mBinding.rcv.setAdapter(mAdapter);

                        } else {
                            if (recruiterList == null)
                                recruiterList = responseModel.recr;

                            recruiterList.addAll(responseModel.recr);
                            if (mAdapter != null)
                                mAdapter.notifyDataSetChanged();
                        }


                        if (responseModel.ttl > PAGINATION_LIMIT) {
                            totalPage = responseModel.ttl / PAGINATION_LIMIT;
                            if (responseModel.ttl % PAGINATION_LIMIT != 0)
                                totalPage = totalPage + 1;
                        }
                        if (pageNo == totalPage) isLastPage = true;
                        isLoading = false;

                    } else {
                        if (pageNo == 1)
                            setVisiblity(false);
                    }

                } else {
                    mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
                    mBinding.tvNoDataFound.setText(getResources().getString(R.string.not_added_recruiter_yet));
                    mAdapter.recruiterList.clear();
                    mAdapter.notifyDataSetChanged();
                    if (response.err.errs.get(0) != null && response.err.errs.get(0).message != null)
                        validationFailed(response.err.errs.get(0).message);
                    else
                        validationFailed(response.err.msg);
                    setVisiblity(false);
                }


                break;
            case API_FLAG_DELETE_RECRUITER:
                if (mAdapter.recruiterList.size() <= 1)
                    mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
                mAdapter.deleteRecruiter(bean, adapterPosition);
                bean = null;
                adapterPosition = -1;
                break;

        }
        setProgress(false);


    }

    private void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.rcv.setVisibility(View.GONE);
        }
    }

    private void setProgress(boolean showFlag) {
        if (pageNo == 1) {
            mBinding.progressBar.setVisibility(View.GONE);
            if (showFlag)
                showProgress(true);
            else
                showProgress(false);
        } else {
            if (showFlag)
                mBinding.progressBar.setVisibility(View.VISIBLE);
            else
                mBinding.progressBar.setVisibility(View.GONE);


        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        setProgress(false);
        mBinding.progressBar.setVisibility(View.GONE);
        if (apiFlag == API_FLAG_DELETE_RECRUITER) {
            Utility.showToast(getViewContext(), error);
            bean = null;
            adapterPosition = -1;
        }

    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ADD_RECRUITER_RESULT_CODE) {
            isAdd = true;
            if (resultCode == RESULT_OK)
                hitApi(true);
        }

    }

    @Override
    public void onAdd() {
        if (isAdd) {
            openActivityForResult(RecruiterActivity.class, null, ADD_RECRUITER_RESULT_CODE);
            isAdd = false;
        }
    }

    @Override
    public void onDeleteRecruiter(RecruiterListResponseModel.RecrBean bean, int adapterPostion) {
        this.bean = bean;
        this.adapterPosition = adapterPostion;
        dialogInitialize.prepareDeleteRecriterDialog(new CustomDialogs(getContext(), new yesNoCallback() {

            @Override
            public void onYesClicked(String from) {
                setProgress(true);
                mPreseneter.deleteRecruiter(bean);

            }

            @Override
            public void onNoClicked(String from) {

            }
        }));

    }

}

