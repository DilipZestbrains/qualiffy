package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;
import com.google.android.libraries.places.widget.Autocomplete;

public interface IAddress extends BaseInterface, IUpdatableDetail {

    void onNext();

    void openAutoSelectPlace(Autocomplete.IntentBuilder intent);

    void onAddressSelect(String name, String country, String state, String lat, String lng);


}
