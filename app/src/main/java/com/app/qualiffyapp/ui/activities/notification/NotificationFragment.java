package com.app.qualiffyapp.ui.activities.notification;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.NotificationAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.databinding.FragmentNotificationBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnUserListener;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.localDatabase.repository.AppRepository;
import com.app.qualiffyapp.models.NotificationModel;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.ui.activities.contacts.ContactPresenter;
import com.app.qualiffyapp.ui.activities.profile.otherUser.OtherUserProfile;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ACCEPT_FRND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_BUSINESS;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_DELETE_BUSINESS;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_DELETE_JOB_SEEKER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_JOBSEEKER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_REJECT_FRND;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_ACCEPTED;
import static com.app.qualiffyapp.firebase.chat.types.FriendStatus.REQUEST_NOT_SEND;


public class NotificationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, SignupView, NotificationAdapter.DeleteNotificationListener, NotificationAdapter.FriendRequestListener {

    private FragmentNotificationBinding mNotificationBinding;
    private String from;
    private NotificationPresenter notificationPresenter;
    private int PAGE_NUMBER = 1;
    private NotificationAdapter adapter;
    private ArrayList<NotificationModel.Notif> notificationList = new ArrayList<>();
    private int position = Integer.MAX_VALUE;
    private int user;
    private AppRepository appRepository;
    private AppFirebaseDatabase appFirebaseDatabase;
    private ContactPresenter contactPresenter;
    private NotificationModel.Notif notif;

    @Override
    protected void initUi() {
        mNotificationBinding = (FragmentNotificationBinding) viewDataBinding;
        notificationPresenter = new NotificationPresenter(this);
        contactPresenter = new ContactPresenter(this);
        user = Utility.getIntFromSharedPreference(getActivity(), USER_TYPE);
        adapter = new NotificationAdapter(new ArrayList<>(), this);
        adapter.setRequestListener(this);
        appRepository = AppRepository.getInstance(getContext());
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
        mNotificationBinding.recyclerNotification.setAdapter(adapter);
        mNotificationBinding.recyclerNotification.setLayoutManager(new LinearLayoutManager(context));
        mNotificationBinding.pullRefresh.setRefreshing(true);
        loadData();
    }

    private void loadData() {
        if (user == JOB_SEEKER) {
            from = "JOB_SEEKER";
            notificationPresenter.getNotification(from, API_FLAG_NOTIFICATION_JOBSEEKER, PAGE_NUMBER);
        } else {
            from = "BUSINESS_USER";
            notificationPresenter.getNotification(from, API_FLAG_NOTIFICATION_BUSINESS, PAGE_NUMBER);
        }

    }


    @Override
    protected int getLayoutById() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void setListener() {
        mNotificationBinding.pullRefresh.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == SUCCESS) {
            if (response.getRes() != null) {
                if (apiFlag == API_FLAG_NOTIFICATION_JOBSEEKER || apiFlag == API_FLAG_NOTIFICATION_BUSINESS) {
                    NotificationModel model = (NotificationModel) response.getRes();
                    if (PAGE_NUMBER == 1) {
                        if (model.getNotif().size() > 0) {
                            mNotificationBinding.recyclerNotification.setVisibility(View.VISIBLE);
                            mNotificationBinding.tvEmptyText.setVisibility(View.GONE);
                            notificationList.clear();
                            notificationList.addAll(model.getNotif());
                            adapter.refreshList(notificationList);
//                            mNotificationBinding.recyclerNotification.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down));
                        } else {
                            mNotificationBinding.recyclerNotification.setVisibility(View.GONE);
                            mNotificationBinding.tvEmptyText.setVisibility(View.VISIBLE);
                        }

                    }
                } else if (apiFlag == API_FLAG_NOTIFICATION_DELETE_JOB_SEEKER || apiFlag == API_FLAG_NOTIFICATION_DELETE_BUSINESS) {
                    if (position != Integer.MAX_VALUE) {
                        notificationList.remove(position);
                        adapter.notifyItemRemoved(position);
                    }
                } else if (apiFlag == API_FLAG_ACCEPT_FRND) {
                    if (notif != null) {
                        appFirebaseDatabase.getUser(notif.getFrom(), user -> {
                            UserConnection friendConnection = new UserConnection();
                            friendConnection.setImg(user.getImg());
                            friendConnection.setName(user.getName());
                            friendConnection.setPhone(user.getPhone());
                            friendConnection.setUid(user.getUid());
                            friendConnection.setType(user.getUser_type());
                            friendConnection.setTimestamp(String.valueOf(System.currentTimeMillis()));
                            appRepository.updateContactStatus(REQUEST_ACCEPTED.value, notif.getFrom());
                            UserConnection currentUser = Utility.getUserConnection(getContext());
                            appFirebaseDatabase.addFriend(currentUser, friendConnection);
                            notif = null;
                        });

                    }
                } else if (apiFlag == API_FLAG_REJECT_FRND) {
                    if (notif != null) {
                        appRepository.updateContactStatus(REQUEST_NOT_SEND.value, notif.getFrom());
                        notif = null;
                    }
                }

            }
            mNotificationBinding.pullRefresh.setRefreshing(false);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        if (getActivity() != null)
            Utility.showToast(getActivity(), error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {

        showProgress(showProgress);
    }

    @Override
    public void onDeleteClick(int pos) {
        this.position = pos;
        notificationPresenter.callDeleteApi(from, notificationList.get(pos).getId());
    }

    @Override
    public void acceptRequest(NotificationModel.Notif notif) {
        this.notif = notif;
        if (notif.getFrom() != null)
            contactPresenter.hitFrndAPI(notif.getFrom(), API_FLAG_ACCEPT_FRND);
    }

    @Override
    public void rejectRequest(NotificationModel.Notif notif) {
        this.notif = notif;
        if (notif.getFrom() != null)
            contactPresenter.hitFrndAPI(notif.getFrom(), API_FLAG_REJECT_FRND);
    }

    @Override
    public void viewProfile(NotificationModel.Notif notif) {
        this.notif = notif;
        if (notif.getFrom() != null) {
            Bundle b = new Bundle();
            b.putString(USER_ID, notif.getFrom());
            openActivity(OtherUserProfile.class, b);
        }
    }
}
