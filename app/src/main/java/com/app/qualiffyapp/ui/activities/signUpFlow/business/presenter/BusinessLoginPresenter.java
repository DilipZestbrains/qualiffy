package com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter;

import android.os.Bundle;

import androidx.fragment.app.FragmentManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ILoginActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.AddMediaFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.AddressFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.BusinessLoginFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.BusinessLogoFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.BusinessSignupFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.BusinessSizeFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.CompanyNameFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.DescriptionFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.LocationFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.SelectIndustryFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.view.VerifyLoginOtpFragment;
import com.app.qualiffyapp.utils.FragmentTransactions;


public class BusinessLoginPresenter {
    private ILoginActivity loginActivity;
    private FragmentManager fm;

    public BusinessLoginPresenter(ILoginActivity loginActivity, FragmentManager fm) {
        this.loginActivity = loginActivity;
        this.fm = fm;
    }

    public void gotoFragmentWithBundle(DestinationFragment fragment, Boolean isAdd, Boolean isAnimate, Bundle bundle) {
        loginActivity.hideKeyBoard();
        BLoginSignUpBaseFragment f = null;
        switch (fragment) {
            case LOGIN:
                f = new BusinessLoginFragment();
                break;
            case SIGNUP:
                f = new BusinessSignupFragment();
                break;
            case CHOOSE_INDUSTRY:

                f = new SelectIndustryFragment();

                break;
            case COMPANY_NAME:
                f = new CompanyNameFragment();

                break;
            case ENABLE_LOCATION:

                f = new LocationFragment();

                break;
            case COMPANY_ADDRESS:

                f = new AddressFragment();

                break;
            case COMPANY_DESCRIPTION:

                f = new DescriptionFragment();

                break;
            case BUSINESS_SIZE:

                f = new BusinessSizeFragment();
                break;
            case VERIFY:
//                f = new VerifyFragment();
//                break;
            case VERIFY_PHONE_LOGIN_OTP:
                f = new VerifyLoginOtpFragment();
                break;
            case COMPANY_LOGO:
                f = new BusinessLogoFragment();
                break;
//            case REGISTERATION_NUMBER:
//                f = new RegistrationNumberFragment();
//                break;
            case ADD_MEDIA:
                f = new AddMediaFragment();
                break;
        }

        if (bundle != null)
            f.setArguments(bundle);

        if (!isAdd)
            clearFragmentBackStack();
        if (f instanceof CompanyNameFragment) {
            clearFragmentBackStack();
            BLoginSignUpBaseFragment fragments = new BusinessLoginFragment();
            FragmentTransactions.pushFragment(fm, R.id.fragment_container, new BusinessLoginFragment(), isAdd,
                    fragments.getClass().getName(), isAnimate);
        }
        FragmentTransactions.pushFragment(fm, R.id.fragment_container, f, isAdd, f.getClass().getName(), isAnimate);


    }

    public void gotoFragment(DestinationFragment fragment, Boolean isAdd, Boolean isAnimate) {
        loginActivity.hideKeyBoard();
        BLoginSignUpBaseFragment f = null;
        switch (fragment) {
            case LOGIN:
                f = new BusinessLoginFragment();
                break;
            case SIGNUP:
                f = new BusinessSignupFragment();
                break;
            case CHOOSE_INDUSTRY:

                f = new SelectIndustryFragment();

                break;
            case COMPANY_NAME:
                f = new CompanyNameFragment();

                break;
            case ENABLE_LOCATION:

                f = new LocationFragment();

                break;
            case COMPANY_ADDRESS:

                f = new AddressFragment();

                break;
            case COMPANY_DESCRIPTION:

                f = new DescriptionFragment();

                break;
            case BUSINESS_SIZE:

                f = new BusinessSizeFragment();
                break;
            case VERIFY:
//                f = new VerifyFragment();
//                break;
            case VERIFY_PHONE_LOGIN_OTP:
                f = new VerifyLoginOtpFragment();
                break;
            case COMPANY_LOGO:
                f = new BusinessLogoFragment();
                break;
//            case REGISTERATION_NUMBER:
//                f = new RegistrationNumberFragment();
//                break;

            case ADD_MEDIA:
                f = new AddMediaFragment();
                break;
        }


        if (!isAdd)
            clearFragmentBackStack();
        if (f instanceof CompanyNameFragment) {
            clearFragmentBackStack();
            BLoginSignUpBaseFragment fragments = new BusinessLoginFragment();
            FragmentTransactions.pushFragment(fm, R.id.fragment_container, new BusinessLoginFragment(), isAdd,
                    fragments.getClass().getName(), isAnimate);
        }
        FragmentTransactions.pushFragment(fm, R.id.fragment_container, f, isAdd, f.getClass().getName(), isAnimate);
    }

    private void clearFragmentBackStack() {
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public enum DestinationFragment {
        LOGIN,
        SIGNUP,
        CHOOSE_INDUSTRY,
        COMPANY_NAME,
        ENABLE_LOCATION,
        COMPANY_ADDRESS,
        COMPANY_DESCRIPTION,
        BUSINESS_SIZE,
        VERIFY,
        VERIFY_PHONE_LOGIN_OTP,
        COMPANY_LOGO,
        ADD_MEDIA
    }

}
