package com.app.qualiffyapp.ui.activities.recruiter.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.recruiter.RecruiterListResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_RECRUITER_LIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_RECRUITER;

public class RecruiterListPresenter {
    private SignUpInteractor interactor;
    private SignupView signupView;

    public RecruiterListPresenter(SignupView signupView) {
        interactor = new SignUpInteractor();
        this.signupView = signupView;
    }


    private ApiListener<ResponseModel<RecruiterListResponseModel>> recruiterListener = new ApiListener<ResponseModel<RecruiterListResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<RecruiterListResponseModel> response, int apiFlag) {
            signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            signupView.apiError(null, apiFlag);
        }
    };
    private ApiListener<ResponseModel<MsgResponseModel>> msgResponseModel = new ApiListener<ResponseModel<MsgResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
            signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            signupView.apiError(null, apiFlag);
        }
    };

    public void getrecruiterList(int pageNo) {
        interactor.businessRecruiterListApi(recruiterListener, pageNo, API_FLAG_BUSINESS_RECRUITER_LIST);
    }

    public void deleteRecruiter(RecruiterListResponseModel.RecrBean recrBean) {
        if (recrBean != null && recrBean._id != null) {
            interactor.deleteRecruiter(msgResponseModel, recrBean._id, API_FLAG_DELETE_RECRUITER);
        }
    }

}
