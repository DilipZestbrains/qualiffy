package com.app.qualiffyapp.ui.activities.dashboard.business.views;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.business.ViewerAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.PaginationScrollListener;
import com.app.qualiffyapp.databinding.ActivityViewerBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.home.business.ViewerResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.business.presenter.ViewerPresenter;
import com.app.qualiffyapp.ui.activities.profile.otherUser.BusinessProfileActivity;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MY_FOLLOWING;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_MY_RECOMMENDATION;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.JS_FOLLOWING;
import static com.app.qualiffyapp.constants.AppConstants.JS_RECOMMANDATION;
import static com.app.qualiffyapp.constants.AppConstants.JS_VIEWER;
import static com.app.qualiffyapp.constants.AppConstants.ORG_DASHBOARD;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor.PAGINATION_LIMIT;

public class ViewerActivity extends BaseActivity implements SignupView, ViewerAdapter.UserProfileListener {
    private ActivityViewerBinding mBinding;
    private ViewerPresenter mPresenter;
    private ViewerAdapter mAdapter;
    private int pageNo = 1;
    private boolean isLastPage = false;
    private boolean isLoading = false;
    private int totalPage = 1;
    private String userId;
    int from;
    private String msg;


    @Override
    protected void initView() {
        mBinding = (ActivityViewerBinding) viewDataBinding;
        mPresenter = new ViewerPresenter(this);
        getIntentData();

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            from = bundle.getInt(FROM);
            switch (from) {
                case JS_RECOMMANDATION:
                    toolbar(getResources().getString(R.string.my_recommendation));
                    msg = "You have not recommended any organization yet!";
                    mPresenter.getRecommendList(true, pageNo, API_FLAG_JS_MY_RECOMMENDATION);
                    break;
                case JS_FOLLOWING:
                    msg = "You have not followed anyone yet.";
                    toolbar(getResources().getString(R.string.my_following));
                    mPresenter.getRecommendList(true, pageNo, API_FLAG_JS_MY_FOLLOWING);
                    break;
                case ORG_DASHBOARD:
                    toolbar(getResources().getString(R.string.viewer));
                    userId = bundle.getString(USER_ID);
                    if (userId != null)
                        mPresenter.hitViewerApi(userId, pageNo, true);
                    break;

                case JS_VIEWER:
                    toolbar(getResources().getString(R.string.viewer));
                    mPresenter.getUserViewer(pageNo);
                    break;
            }

        }

    }

    private void toolbar(String title) {
        mBinding.toolbar.toolbarTitleTxt.setText(title);
        mBinding.toolbar.backBtnImg.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_viewer;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
        }
    }


    @Override
    protected void setListener() {
        super.setListener();
        mBinding.toolbar.backBtnImg.setOnClickListener(this);

        mBinding.rcv.addOnScrollListener(new PaginationScrollListener((LinearLayoutManager) mBinding.rcv.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (!isLastPage) {
                    isLoading = true;
                    pageNo = pageNo + 1;
                    if (userId != null) {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        switch (from) {
                            case JS_RECOMMANDATION:
                                mPresenter.getRecommendList(false, pageNo, API_FLAG_JS_MY_RECOMMENDATION);
                                break;
                            case JS_FOLLOWING:
                                mPresenter.getRecommendList(false, pageNo, API_FLAG_JS_MY_FOLLOWING);
                                break;
                            case ORG_DASHBOARD:
                                if (userId != null)
                                    mPresenter.hitViewerApi(userId, pageNo, false);
                                break;
                            case JS_VIEWER:
                                toolbar(getResources().getString(R.string.viewer));
                                mPresenter.getUserViewer(pageNo);
                                break;

                        }
                    }
                }
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
        {
            if (response.getStatus() == SUCCESS) {
                ViewerResponseModel responseModel = (ViewerResponseModel) response.getRes();
                if (responseModel != null && responseModel.org != null && responseModel.org.size() > 0) {
                    setVisiblity(true);
                    if (mAdapter == null) {
                        mAdapter = new ViewerAdapter(responseModel.bP, responseModel.org);
                        mAdapter.setProfileListener(from, this);
                        mBinding.rcv.setAdapter(mAdapter);
                    } else {
                        mAdapter.addAll(responseModel.org);
                    }

                    if (responseModel.ttl > PAGINATION_LIMIT) {
                        totalPage = responseModel.ttl / PAGINATION_LIMIT;
                        if (responseModel.ttl % PAGINATION_LIMIT != 0)
                            totalPage = totalPage + 1;
                    }
                    if (pageNo == totalPage) isLastPage = true;
                    isLoading = false;

                } else
                    setVisiblity(false);


            } else validationFailed(response.err.msg);
        }
    }


    void setVisiblity(boolean isData) {
        if (isData) {
            mBinding.tvNoDataFound.setVisibility(View.GONE);
            mBinding.rcv.setVisibility(View.VISIBLE);
        } else {
            mBinding.tvNoDataFound.setVisibility(View.VISIBLE);
            mBinding.tvNoDataFound.setText(msg);
            mBinding.rcv.setVisibility(View.GONE);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {


        if (mBinding.progressBar.isShown())
            mBinding.progressBar.setVisibility(View.GONE);
        else
            showProgressDialog(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void openUserProfile(String id, int type) {
        Bundle b = new Bundle();
        b.putString(USER_ID, id);
        openActivity(BusinessProfileActivity.class, b);
    }
}
