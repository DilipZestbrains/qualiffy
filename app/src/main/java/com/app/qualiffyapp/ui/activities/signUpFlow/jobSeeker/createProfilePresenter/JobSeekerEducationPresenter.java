package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.ProfileFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerEducation;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_EDUCATION;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_EDUCATION;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_UPDATE_EDUCATION;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.FROM;

public class JobSeekerEducationPresenter implements ApiListener<VerifyResponseModel> {
    private final IJobSeekerEducation jobSeekerEducation;
    private final SignUpInteractor interactor;
    private JobSeekerEducation edu;


    public JobSeekerEducationPresenter(IJobSeekerEducation jobSeekerEducation) {
        this.jobSeekerEducation = jobSeekerEducation;
        interactor = new SignUpInteractor();
    }


    public void addEducation(String name, String addr, String sess, String major, String id) {
        RequestBean bean = new RequestBean();
        bean.addr = addr;
        bean.sess = sess;
        bean.major = major;
        bean.name = name;
        if (isValid(bean)) {
            jobSeekerEducation.preAdd();
            setEducation(addr, sess, name,major, id);
            if (id == null)
                interactor.addJobSeekerEducation(this, bean, API_FLAG_ADD_EDUCATION);
            else {
                interactor.updateJobSeekerEducation(this, id, bean, API_FLAG_UPDATE_EDUCATION);
            }
        }
    }


    private boolean isValid(RequestBean bean) {
        if (bean.name == null || TextUtils.isEmpty(bean.name)) {
            jobSeekerEducation.showToast(ToastType.EDUCATION_NAME_EMPTY);
            return false;
        }
        if (bean.addr == null || TextUtils.isEmpty(bean.addr)) {
            jobSeekerEducation.showToast(ToastType.EDUCATION_ADDRESS_EMPTY);
            return false;
        }
        if (bean.sess == null || TextUtils.isEmpty(bean.sess)) {
            jobSeekerEducation.showToast(ToastType.EDUCATION_SESSION_EMPTY);
            return false;
        }
        return true;
    }

    private JobSeekerEducation getEdu() {
        if (edu == null)
            edu = new JobSeekerEducation();
        return edu;
    }

    public void setEducation(String add, String session, String name,String major, String id) {
        getEdu().addr = add;
        getEdu().sess = session;
        getEdu().name = name;
        getEdu().major = major;
        if (id != null)
            getEdu()._id = id;
    }

    public void getIntentData(Intent intent) {
        Bundle bundle;
        if ((bundle = intent.getBundleExtra(BUNDLE)) != null) {
            if (bundle.getString(FROM, "").equals(ProfileFragment.class.getName())) {
                JobSeekerEducation education;
                if ((education = bundle.getParcelable(ProfileFragment.EDUCATION)) != null) {
                    if (education._id != null)
                        jobSeekerEducation.applyDeleteAction();
                    jobSeekerEducation.fillDetails(education);

                }
            }
        }
    }

    public void deleteEducation(String eduID) {
        if (eduID != null) {
            interactor.deleteJobSeekerEducation(this, eduID, API_FLAG_DELETE_EDUCATION);
        }

    }

    @Override
    public void onApiSuccess(VerifyResponseModel response, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_ADD_EDUCATION:
                getEdu()._id = response._id;
                jobSeekerEducation.onAddEducation(response.msg, getEdu(), IJobSeekerEducation.Action.ADD);
                break;
            case API_FLAG_UPDATE_EDUCATION:
                jobSeekerEducation.onAddEducation(response.msg, getEdu(), IJobSeekerEducation.Action.UPDATE);
                break;
            case API_FLAG_DELETE_EDUCATION:
                jobSeekerEducation.onAddEducation(response.msg, getEdu(), IJobSeekerEducation.Action.DELETE);

                break;
        }

    }

    @Override
    public void onApiError(String error, int apiFlag) {
        jobSeekerEducation.onAddEducation(error, null, null);

    }
}
