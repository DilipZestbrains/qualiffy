package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.ActivitySkillBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.JobSeekerInfoPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.interfaces.IJobSeekerInfo;
import com.app.qualiffyapp.utils.Utility;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_SKILLS;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class SkillActivity extends BaseActivity implements IJobSeekerInfo {
    private ActivitySkillBinding skillBinding;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private JobSeekerInfoPresenter presenter;
    private boolean isUpdate;


    @Override
    protected void initView() {
        skillBinding = (ActivitySkillBinding) viewDataBinding;
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        presenter = new JobSeekerInfoPresenter(this);
        setListener();

        getIntentData();

    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            profileInputModel.skills = null;
            isUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            ArrayList<String> skillsArray;
            skillsArray = bundle.getStringArrayList(JS_SKILLS);

            if (skillsArray != null && skillsArray.size() > 0) {
                for (String skill : skillsArray)
                    addskill(skill);
            }
            toolbar(true);
        } else {
            toolbar(false);

        }
    }


    private void toolbar(boolean isUpdate) {
        skillBinding.toolbar.backBtnImg.setOnClickListener(this);
        skillBinding.toolbar.skipTxt.setOnClickListener(this);
        skillBinding.titleTxt.setText(getResources().getString(R.string.skill));

        if (isUpdate) {
            skillBinding.indusBtn.btn.setText(getResources().getString(R.string.DONE));
            skillBinding.toolbar.skipTxt.setVisibility(View.GONE);
            skillBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.EDIT_SKILL));
        } else {
            skillBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
            skillBinding.toolbar.skipTxt.setVisibility(View.VISIBLE);
            skillBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
        }

    }

    protected void setListener() {
        skillBinding.addImg.setOnClickListener(this);
        skillBinding.toolbar.backBtnImg.setOnClickListener(this);
        skillBinding.indusBtn.btn.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_skill;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                profileInputModel.skills = null;
                onBackPressed();
                break;
            case R.id.indus_btn:
                if (profileInputModel.skills == null || profileInputModel.skills.size() == 0 && !isUpdate)
                    Utility.showToast(this, getResources().getString(R.string.skill));
                else {
                    if (isUpdate) {
                        RequestBean bean = new RequestBean();
                        bean.skills = profileInputModel.skills;
                        presenter.updateProfile(bean);
                    } else
                        startActivity(new Intent(this, JobTypeActivity.class));
                }
                break;
            case R.id.add_img:
                if (!skillBinding.searchEdt.getText().toString().isEmpty())
                    addskill(skillBinding.searchEdt.getText().toString());
                break;
            case R.id.skip_txt:
                profileInputModel.skills = null;
                if(skillBinding.chipgroup.getChildCount()>0)
                skillBinding.chipgroup.removeAllViews();
                startActivity(new Intent(this, JobTypeActivity.class));
                break;
        }
    }

    private void addskill(String str) {
        if (profileInputModel.skills == null) {
            profileInputModel.skills = new ArrayList<>();
            profileInputModel.isSignup2Change = true;
            addChip(str.toUpperCase());
        } else if (profileInputModel.skills.size() == 10) {
            Utility.showToast(this, getResources().getString(R.string.skill_limit));
            skillBinding.searchEdt.getText().clear();
        } else if (profileInputModel.skills.contains(Utility.getFirstLetterCaps(str))) {
            skillBinding.searchEdt.getText().clear();
        } else {
            addChip(str.toUpperCase());
            profileInputModel.isSignup2Change = true;
        }
    }

    private void addChip(String str) {
        profileInputModel.skills.add(Utility.getFirstLetterCaps(str));
        CustomChip chip = new CustomChip(this, str);
        chip.setOnCloseIconClickListener(view -> {
            profileInputModel.isSignup2Change = true;
            profileInputModel.skills.remove(chip.getText());
            skillBinding.chipgroup.removeView(view);
        });

        skillBinding.chipgroup.addView(chip);
        skillBinding.searchEdt.getText().clear();

    }


    @Override
    public void preUpdate() {
        showProgress(true);
    }

    @Override
    public void onUpdate(int status, String msg) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            Intent returnIntent = new Intent();
            returnIntent.putStringArrayListExtra(JS_SKILLS, profileInputModel.skills);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }

    }

    @Override
    public void showToast(ToastType toastType) {
        if (toastType == ToastType.JOB_SEEKER_DESCRIPTION_EMPTY) {
            Utility.showToast(this, getResources().getString(R.string.please_enter_job_seeker_intro));
        }
    }

}
