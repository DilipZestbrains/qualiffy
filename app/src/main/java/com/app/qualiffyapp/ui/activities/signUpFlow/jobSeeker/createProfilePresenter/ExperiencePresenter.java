package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter;

import android.widget.Spinner;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import java.lang.reflect.Field;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_3;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_4;

public class ExperiencePresenter implements ApiListener<ResponseModel<MsgResponseModel>> {

    private SignupView view;
    private SignUpInteractor mSignUpInteractor;

    public ExperiencePresenter(SignupView signupView) {
        this.view = signupView;
        this.mSignUpInteractor = new SignUpInteractor();
    }

    @Override
    public void onApiSuccess(ResponseModel<MsgResponseModel> response, int apiFlag) {
        if (response != null)
            view.apiSuccess(response, apiFlag);
    }

    @Override
    public void onApiError(String error, int apiFlag) {
        view.apiError(error, apiFlag);
    }


    public void hitApi(CreateJobSeekerProfileInputModel profileInputModel, int apiFlag) {
        switch (apiFlag) {
            case API_FLAG_SIGNUP_3:
                mSignUpInteractor.signUp3Api(this, profileInputModel, apiFlag);
                break;
            case API_FLAG_SIGNUP_4:
                mSignUpInteractor.signUp4Api(this, profileInputModel, apiFlag);
                break;
        }
    }


    public void setHeighofSpinner(Spinner spinner) {
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spinner);

            // Set popupWindow height to 500px
            popupWindow.setHeight(500);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
    }
}
