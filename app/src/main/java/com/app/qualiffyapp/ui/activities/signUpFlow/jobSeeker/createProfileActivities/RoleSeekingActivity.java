package com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.signup.SuggestedListAdapter;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.CustomChip;
import com.app.qualiffyapp.databinding.ActivitySelectIndustryBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.JobInfo;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.CustomJobPresenter;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfilePresenter.RoleSeekingPresenter;
import com.app.qualiffyapp.utils.RecyclerItemClickListener;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_ADD_JOB_TITLE;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ROLE_SEEKING;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SIGNUP_1;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.IS_JOBSEEKER_UPDATE;
import static com.app.qualiffyapp.constants.AppConstants.JS_INDUS_ID;
import static com.app.qualiffyapp.constants.AppConstants.JS_ROLESEEKING;

public class RoleSeekingActivity extends BaseActivity implements TextWatcher, SignupView, RecyclerItemClickListener.OnItemClickListener, TextView.OnEditorActionListener {
    private ActivitySelectIndustryBinding selectIndustryBinding;
    private List<SuggestedListResponseModel> seggestList;
    private RoleSeekingPresenter roleSeekingPresenter;
    private CustomJobPresenter jobPresenter;

    private SuggestedListAdapter suggestedListAdapter;
    private CreateJobSeekerProfileInputModel profileInputModel;
    private boolean isJobSeekerUpdate;

    @Override
    protected void initView() {

        selectIndustryBinding = (ActivitySelectIndustryBinding) viewDataBinding;
        roleSeekingPresenter = new RoleSeekingPresenter(this);
        jobPresenter = new CustomJobPresenter(this);
        selectIndustryBinding.tvNote.setVisibility(View.GONE);
        selectIndustryBinding.addJobs.setVisibility(View.GONE);
        profileInputModel = CreateJobSeekerProfileInputModel.getInstance();
        profileInputModel.role_skng = new ArrayList<>();
        profileInputModel.roleSeek = new LinkedHashMap<>();
        getIntentData();
        toolbar();
        setListeners();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            isJobSeekerUpdate = bundle.getBoolean(IS_JOBSEEKER_UPDATE);
            String roleSeekingstr = bundle.getString(JS_ROLESEEKING);
            if (bundle.getString(JS_INDUS_ID) != null)
                profileInputModel.jobC_id = bundle.getString(JS_INDUS_ID);

            Type roleSeektype = new TypeToken<ArrayList<JobInfo>>() {
            }.getType();

            ArrayList<JobInfo> roleSeekingJobInfo = new Gson().fromJson(roleSeekingstr, roleSeektype);

            if (roleSeekingJobInfo != null) {
                profileInputModel.roleSeek = new LinkedHashMap<>();
                for (int i = 0; i < roleSeekingJobInfo.size(); i++) {
                    addChip(Utility.getFirstLetterCaps(roleSeekingJobInfo.get(i).getName()), roleSeekingJobInfo.get(i).get_id());
                }
            }
        }
    }

    private void setListeners() {
        selectIndustryBinding.searchEdt.addTextChangedListener(this);
        selectIndustryBinding.suggestedRcv.addOnItemTouchListener(
                new RecyclerItemClickListener(this, this));
        selectIndustryBinding.addJobs.setOnClickListener(this);
        selectIndustryBinding.searchEdt.setOnEditorActionListener(this);

    }

    private void toolbar() {
        selectIndustryBinding.toolbar.backBtnImg.setOnClickListener(this);
        selectIndustryBinding.titleTxt.setText(getResources().getString(R.string.role_seeking));
        if (isJobSeekerUpdate) {
            selectIndustryBinding.indusBtn.btn.setText(getResources().getString(R.string.DONE));
            selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.edit_role_seeking));
        }
        else {
            selectIndustryBinding.indusBtn.btn.setText(getResources().getString(R.string.NEXT));
            selectIndustryBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.CREATE_PROFILE_TITLE));
        }
        selectIndustryBinding.indusBtn.btn.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_select_industry;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_jobs:
                if (profileInputModel.jobC_id != null && !(TextUtils.isEmpty(selectIndustryBinding.searchEdt.getText().toString()))) {
                    selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
                    jobPresenter.hitApiAddJobs(selectIndustryBinding.searchEdt.getText().toString().toUpperCase(), profileInputModel.jobC_id, API_FLAG_JS_ADD_JOB_TITLE);
                }
                break;
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.indus_btn:
                if (selectIndustryBinding.chipgroup.getChildCount() == 0)
                    validationFailed(ApplicationController.getApplicationInstance().getString(R.string.role_seeking_alert));
                else {
                    if (isJobSeekerUpdate) {
                        Intent intent = new Intent();
                        intent.putExtra(JS_ROLESEEKING, new Gson().toJson(profileInputModel.roleSeek));
                        setResult(RESULT_OK, intent);
                        finish();
                    } else {
                        if (profileInputModel.isSignup1Change) {
                            showProgressDialog(true);
                            profileInputModel.role_skng = new ArrayList<String>(profileInputModel.roleSeek.values());
                            roleSeekingPresenter.signUp1Api(profileInputModel);
                        } else {
                            startActivity(new Intent(this, LanguageActivity.class));
                        }

                    }
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);
        if (s.length() == 2 || s.length() == 5 || s.length() == 7 || s.length() == 11 || s.length() > 15) {
            if (profileInputModel != null && profileInputModel.jobC_id != null) {
                selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
                roleSeekingPresenter.hitApi(s.toString(), profileInputModel.jobC_id);
            }
        } else {
            if (seggestList != null) {
                selectIndustryBinding.progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        if (response.getStatus() == 1) {
            switch (apiFlag) {
                case API_FLAG_ROLE_SEEKING:
                    IndustryResponseModel getIndustryResponseModel = (IndustryResponseModel) response.getRes();
                    seggestList = getIndustryResponseModel.jobT;
                    suggestedListAdapter = new SuggestedListAdapter(seggestList);
                    selectIndustryBinding.suggestedRcv.setAdapter(suggestedListAdapter);

                    if (getIndustryResponseModel.ttl == 0) {
                        selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.VISIBLE);
                        selectIndustryBinding.noDataTxt.noDataFound.setText(getResources().getString(R.string.no_data_found_press_add_btn));
                        selectIndustryBinding.addJobs.setVisibility(View.VISIBLE);
                    } else
                        selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);

                    break;
                case API_FLAG_SIGNUP_1:
                    profileInputModel.isSignup1Change = false;
                    startActivity(new Intent(this, LanguageActivity.class));
                    break;

                case API_FLAG_JS_ADD_JOB_TITLE:
                    AddCustomeJobModel model = (AddCustomeJobModel) response.getRes();
                    SuggestedListResponseModel modell = new SuggestedListResponseModel();
                    modell.id = model.job._id;
                    modell.name = model.job.name;
                    if (seggestList != null) {

                    } else {
                        seggestList = new ArrayList<>();
                    }
                    seggestList.add(modell);
                    suggestedListAdapter = new SuggestedListAdapter(seggestList);
                    selectIndustryBinding.suggestedRcv.setAdapter(suggestedListAdapter);
                    selectIndustryBinding.noDataTxt.noDataFound.setVisibility(View.GONE);
                    selectIndustryBinding.searchEdt.setText("");
                    selectIndustryBinding.addJobs.setVisibility(View.GONE);
                    break;
            }

        } else {
            Utility.showToast(this, response.err.msg);
        }
        selectIndustryBinding.progressBar.setVisibility(View.GONE);

        if (apiFlag == API_FLAG_SIGNUP_1)
            showProgressDialog(false);


    }

    @Override
    public void apiError(String error, int apiFlag) {
        Utility.showToast(this, error);
        selectIndustryBinding.progressBar.setVisibility(View.GONE);
        if (apiFlag == API_FLAG_SIGNUP_1)
            showProgressDialog(false);


    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(this, msg);
    }

    @Override
    public AppCompatActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void onItemClick(View view, int position) {
        Utility.hideKeyboard(this);
        if (profileInputModel.roleSeek == null) {
            profileInputModel.roleSeek = new LinkedHashMap<>();
            addChip(Utility.getFirstLetterCaps(seggestList.get(position).name), seggestList.get(position).id);
        } else if (profileInputModel.roleSeek.size() == 2) {
            validationFailed(ApplicationController.getApplicationInstance().getString(R.string.role_seeking_limit_alert));
            clearList();
        } else {
            if (profileInputModel.roleSeek.containsKey(Utility.getFirstLetterCaps(seggestList.get(position).name))) {
                clearList();
            } else {
                addChip(Utility.getFirstLetterCaps(seggestList.get(position).name), seggestList.get(position).id);
                profileInputModel.isSignup1Change = true;
            }

        }

    }

    private void addChip(String roleseek, String id) {
        if (profileInputModel.roleSeek == null)
            profileInputModel.roleSeek = new LinkedHashMap<>();

        profileInputModel.roleSeek.put(roleseek, id);


        CustomChip chip = new CustomChip(this, roleseek);
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (profileInputModel.roleSeek.containsKey(chip.getText().toString())) {
                    profileInputModel.roleSeek.remove(chip.getText().toString());
                    profileInputModel.isSignup1Change = true;
                }

                selectIndustryBinding.chipgroup.removeView(view);
            }
        });
        //chip.setElevation(15);
        selectIndustryBinding.chipgroup.addView(chip);

        clearList();

    }

    private void clearList() {
        selectIndustryBinding.searchEdt.getText().clear();
//        selectIndustryBinding.suggestedRcv.removeAllViewsInLayout();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            if(!TextUtils.isEmpty(selectIndustryBinding.searchEdt.getText().toString()))
                selectIndustryBinding.progressBar.setVisibility(View.VISIBLE);
            roleSeekingPresenter.hitApi(selectIndustryBinding.searchEdt.toString(), profileInputModel.jobC_id);
            Utility.hideKeyboard(this);
            return true;
        }
        return false;
    }
}
