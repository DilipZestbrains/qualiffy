package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IForgotPassword extends BaseInterface {
    void preForgotPass();

    void onForgotPass(int Status, String msg);

}
