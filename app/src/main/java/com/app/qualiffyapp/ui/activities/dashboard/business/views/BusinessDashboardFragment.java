package com.app.qualiffyapp.ui.activities.dashboard.business.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.dashboard.business.BusinessCardstackAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationDasbboardItemClick;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.customViews.CustomCardStackLayoutManager;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.FragmentDashboardBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.dashboard.business.presenter.BusinessDashboardPresenter;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity;
import com.app.qualiffyapp.ui.fragment.DistanceBottomSheet;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.Direction;
import java.util.ArrayList;
import java.util.List;
import pub.devrel.easypermissions.AppSettingsDialog;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_IS_FOLLOW;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_DASHBOARD_IS_SHORTLIST;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_OTHER_USER_BADGE;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_ADD_JOB;
import static com.app.qualiffyapp.constants.AppConstants.BS_POS_SUBSCRIBE;
import static com.app.qualiffyapp.constants.AppConstants.CUSTOMER_ID;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.ORG_DASHBOARD;
import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PHONE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.PROFILE_PIC;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;
import static com.app.qualiffyapp.customViews.DialogInitialize.PURCHASE_PLAN_FLAG;
import static com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity.DataType.ORG_DASHBOARD_RECOMMEND;


public class BusinessDashboardFragment extends BaseFragment implements CardStackListener, SignupView, IOrganizationDasbboardItemClick, yesNoCallback, DistanceBottomSheet.DistanceFilterListener, BusinessCardstackAdapter.OnScrollListener {

    private static final String RIGHT = "right";
    private FragmentDashboardBinding mDashboardBinding;
    private CardStackLayoutManager mCardStackLayoutManager;
    private BusinessCardstackAdapter mCardStackAdapter;
    private BusinessDashboardPresenter mPresenter;
    private boolean hasData = true;
    private int pageNo = 1;
    private List<BusinessHomeResponseModel.JobBean> jobList;
    private boolean isSearch;
    private int subscribeFlag = -12;
    private int DEFAULT_DISTANCE = 50;
    private String basePath;


    @Override
    protected void initUi() {
        mDashboardBinding = (FragmentDashboardBinding) viewDataBinding;
        mPresenter = new BusinessDashboardPresenter(this);
        setupCardStackView();
        fetchData();
        getPrefData();

    }

    private void getPrefData() {
        String firebaseUserType;
        int userType = Utility.getIntFromSharedPreference(getContext(), AppConstants.USER_TYPE);
        String firstName = Utility.getStringSharedPreference(getContext(), FIRST_NAME);
        String uid = Utility.getStringSharedPreference(getContext(), UID);
        String img = Utility.getStringSharedPreference(getContext(), PROFILE_PIC);
        String phone = Utility.getStringSharedPreference(getContext(), PHONE);
        if (userType == AppConstants.JOB_SEEKER) {
            firebaseUserType = FirebaseUserType.JOB_SEEKER.s;

        } else {
            firebaseUserType = FirebaseUserType.BUSINESS.s;
        }
        if (uid != null) {
            AppFirebaseDatabase.getInstance().registerUser(new User(uid, firstName, firebaseUserType, img, phone));
        }
    }

    private void setupCardStackView() {
        mCardStackLayoutManager = CustomCardStackLayoutManager.getCardStackLayoutManager(context, this);
        mDashboardBinding.cardStackView.setLayoutManager(mCardStackLayoutManager);
        DefaultItemAnimator itemAnimator = new DefaultItemAnimator();
        itemAnimator.setSupportsChangeAnimations(false);
        mDashboardBinding.cardStackView.setItemAnimator(itemAnimator);


        String[] mItemTexts = new String[]{"安全中心 ", "特色服务", "投资理财", "投资理财"};

        int[] mItemImgs = new int[]{R.drawable.ic_share_home,
                R.drawable.ic_noun, R.drawable.ic_thumb, R.drawable.ic_multiple_users};

        mDashboardBinding.idMenulayout.setMenuItemIconsAndTexts(mItemImgs, mItemTexts);
    }


    @Override
    protected int getLayoutById() {
        return R.layout.fragment_dashboard;
    }


    private void fetchData() {
        if (pageNo == 1) {
            showProgressDialog(true);
//            isSearch = false;
        }
        mPresenter.hitDashBoardApi(pageNo, null, DEFAULT_DISTANCE);

    }

    public void stopSearch() {
        isSearch = false;
    }

    private void fetchData(int distance) {
        if (pageNo == 1) {
            showProgressDialog(true);
//            isSearch = false;

        }
        mPresenter.hitDashBoardApi(pageNo, null, distance);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAddJob:
                if (getActivity() instanceof DashboardActivity) {
                    DashboardActivity dashboardActivity = (DashboardActivity) getActivity();
                    dashboardActivity.fireEvent(BS_POS_ADD_JOB, null);
                }

        }
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {
        Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = " + ratio);
    }

    @Override
    public void onCardSwiped(Direction direction) {
        Log.d("CardStackView", "onCardSwiped: p = " + mCardStackLayoutManager.getTopPosition() + ", d =" + direction);
        int lastCardPostion = mCardStackLayoutManager.getTopPosition() - 1;
        if (hasData) {
            if (mCardStackLayoutManager.getTopPosition() == jobList.size() - 2) {
                pageNo = pageNo + 1;
                fetchData();
            }
        } else {
            if (mCardStackLayoutManager.getTopPosition() == jobList.size())
                dataVisible(false);
        }


        if (direction.toString().toLowerCase().equals(RIGHT)) {
            mPresenter.hitApis(jobList.get(lastCardPostion).id, null, 1, API_FLAG_BUSINESS_DASHBOARD_IS_FOLLOW);
            mPresenter.hitApis(jobList.get(lastCardPostion).id, jobList.get(lastCardPostion).job_id, 1, API_FLAG_BUSINESS_DASHBOARD_IS_SHORTLIST);
        } else {
            mPresenter.hitApis(jobList.get(lastCardPostion).id, null, 0, API_FLAG_BUSINESS_DASHBOARD_IS_FOLLOW);
            mPresenter.hitApis(jobList.get(lastCardPostion).id, jobList.get(lastCardPostion).job_id, 0, API_FLAG_BUSINESS_DASHBOARD_IS_SHORTLIST);

        }

    }

    @Override
    public void onCardRewound() {
        Log.d("CardStackView", "onCardRewound:" + mCardStackLayoutManager.getTopPosition());
    }

    @Override
    public void onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled: " + mCardStackLayoutManager.getTopPosition());
    }

    @Override
    public void onCardAppeared(View view, int position) {
        TextView textView = view.findViewById(R.id.item_name);
        Log.d("CardStackView", "onCardAppeared: " + position + textView.getText());
    }

    @Override
    public void onCardDisappeared(View view, int position) {
        TextView textView = view.findViewById(R.id.item_name);
        Log.d("CardStackView", "onCardDisappeared: " + position + textView.getText());
    }


    private void setProgress() {
        if (pageNo == 1)
            showProgressDialog(false);
    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        setProgress();

        switch (apiFlag) {
            case API_FLAG_BUSINESS_DASHBOARD:
                setProgress();
                if (response.getStatus() == SUCCESS) {
                    BusinessHomeResponseModel jobsResponseModel = (BusinessHomeResponseModel) response.getRes();
                    if (getContext() != null)
                        Utility.putStringValueInSharedPreference(getContext(), CUSTOMER_ID, jobsResponseModel.cus_id);

                    if (jobsResponseModel != null) {
                        this.subscribeFlag = jobsResponseModel.subscribeFlag;
                        if (getContext() != null)
                            Utility.putIntValueInSharedPreference(getContext(), SUBSCRIPTION_FLAG, subscribeFlag);
                        this.basePath = jobsResponseModel.bp;

                        if (jobsResponseModel.isJobAdd) {
                            if (isSearch)
                                mDashboardBinding.noDataTxt.setText(getResources().getString(R.string.no_record_found));
                            else
                                if(getContext()!=null)
                                mDashboardBinding.noDataTxt.setText(getContext().getResources().getString(R.string.no_jobSeeker));
                            mDashboardBinding.ivAddJob.setVisibility(View.GONE);
                        } else {
                            mDashboardBinding.noDataTxt.setText(getResources().getString(R.string.please_add_job_to_find_jobseeker));
                            mDashboardBinding.ivAddJob.setVisibility(View.VISIBLE);
                            mDashboardBinding.ivAddJob.setOnClickListener(this);
                        }
                        if (jobsResponseModel.job != null && jobsResponseModel.job.size() > 0) {
                            dataVisible(true);
                            hasData = jobsResponseModel.job.size() >= jobsResponseModel.lmt;

                            if (jobList == null || isSearch) {
                                jobList = jobsResponseModel.job;
                                mCardStackAdapter = new BusinessCardstackAdapter(jobList, jobsResponseModel.bp, this, context);
                                mCardStackAdapter.setOnScrollListener(this);
                                mDashboardBinding.cardStackView.setAdapter(mCardStackAdapter);
                            } else {
                                if (mCardStackAdapter != null) {
                                    if (!jobList.isEmpty())
                                        jobList.subList(0, jobList.size() - 1).clear();

                                    jobList.addAll(jobsResponseModel.job);

                                    mCardStackAdapter.notifyDataSetChanged();
                                }
                            }


                            return;
                        }

                    }
                    dataVisible(false);

                } else {
                    dataVisible(false);
                    validationFailed(response.err.msg);
                }
                break;
            case API_FLAG_BUSINESS_OTHER_USER_BADGE:
                showProgressDialog(false);

                if (response.getStatus() == SUCCESS) {
                    BusinessHomeResponseModel.JobBean jobBean = jobList.get(mCardStackLayoutManager.getTopPosition());
                    int badgeCount = 0;
                    if (jobBean.bdgs_cnt != null)
                        badgeCount = Integer.parseInt(jobBean.bdgs_cnt);
                    if (jobBean.is_bdge) {
                        jobBean.is_bdge = false;
                        if (badgeCount > 0)
                            badgeCount = badgeCount - 1;
                    } else {
                        jobList.get(mCardStackLayoutManager.getTopPosition()).is_bdge = true;
                        badgeCount = badgeCount + 1;
                    }
                    jobBean.bdgs_cnt = String.valueOf(badgeCount);

                    if (mCardStackAdapter != null)
                        mCardStackAdapter.notifyItemChanged(mCardStackLayoutManager.getTopPosition());

                    MsgResponseModel msgResponseModel = (MsgResponseModel) response.getRes();
                    validationFailed(msgResponseModel.msg);
                } else
                    validationFailed(response.err.msg);
                break;

        }

    }


    @Override
    public void apiError(String error, int apiFlag) {
        if (apiFlag == API_FLAG_BUSINESS_DASHBOARD) {
            dataVisible(false);
            setProgress();
        }
        validationFailed(error);
    }


    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getContext(), msg);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                Utility.showToast(context, "Returned from setting");
                break;
        }

        }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    private void dataVisible(boolean isJobAvailable) {

        if (isJobAvailable) {
            mDashboardBinding.noDataTxt.setVisibility(View.GONE);
            if(getContext()!=null)
            mDashboardBinding.noDataTxt.setText(getContext().getResources().getString(R.string.no_jobSeeker));
            mDashboardBinding.cardStackView.setVisibility(View.VISIBLE);
        } else {
            mDashboardBinding.noDataTxt.setVisibility(View.VISIBLE);
            mDashboardBinding.cardStackView.setVisibility(View.GONE);
        }
    }

    public void searchJobs(String skey) {
        isSearch = true;
        pageNo = 1;
        mPresenter.hitDashBoardApi(pageNo, skey, DEFAULT_DISTANCE);

    }

    @Override
    public void videoClick(String video, String basepath) {
        List<String> videoList = new ArrayList<>();
        videoList.add(video);
        Intent intent = new Intent(context, StoryViewActivity.class);
        intent.putExtra("BASE_URL_IMAGE", basepath);
        intent.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(videoList));
        context.startActivity(intent);
    }

    @Override
    public void viewerClick(String jobId) {
        if (subscribeFlag == 0 || subscribeFlag == 4) {
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.USER_ID, jobId);
            bundle.putInt(FROM, ORG_DASHBOARD);
            openActivity(ViewerActivity.class, bundle);

        } else {
            int orgType = Utility.getIntFromSharedPreference(getContext(), AppConstants.ORG_TYPE);
            if (orgType == 2) {
                String msg = "";
                if (subscribeFlag == 2)
                    msg = getContext().getResources().getString(R.string.purchase_plan_dialog_body);
                else if (subscribeFlag == 1)
                    msg = getContext().getResources().getString(R.string.expire_plan_dialog_body);
                Utility.showToast(getContext(), msg);
            } else
                new DialogInitialize().subscriptionDialog(new CustomDialogs(getContext(), this), subscribeFlag);

        }
    }

    @Override
    public void FrndClick(String jobId) {
        Intent intent1 = new Intent(context, FrndListActivity.class);
        intent1.putExtra(AppConstants.USER_ID, jobId);
        context.startActivity(intent1);

    }

    @Override
    public void badgeClick(String userId, int isRecommend) {
        if (subscribeFlag == 0 || subscribeFlag == 4) {
            showProgressDialog(true);
            mPresenter.hitApis(userId, null, isRecommend, API_FLAG_BUSINESS_OTHER_USER_BADGE);
        } else {
            int orgType = Utility.getIntFromSharedPreference(getContext(), AppConstants.ORG_TYPE);
            if (orgType == 2) {
                String msg = "";
                if (getContext() != null) {
                    if (subscribeFlag == 2)
                        msg = getContext().getResources().getString(R.string.purchase_plan_dialog_body);
                    else if (subscribeFlag == 1)
                        msg = getContext().getResources().getString(R.string.expire_plan_dialog_body);
                    Utility.showToast(getContext(), msg);
                }
            } else
                new DialogInitialize().subscriptionDialog(new CustomDialogs(getContext(), this), subscribeFlag);

        }

    }

    @Override
    public void badgeCountclick(String userId) {
        if (subscribeFlag == 0 || subscribeFlag == 4) {
            Bundle b = new Bundle();
            b.putString(AppConstants.USER_ID, userId);
            b.putSerializable(FROM, ORG_DASHBOARD_RECOMMEND);
            openActivity(RecommendListActivity.class, b);

        } else {
            int orgType = Utility.getIntFromSharedPreference(getContext(), AppConstants.ORG_TYPE);
            if (orgType == 2) {
                String msg = "";
                if (getContext() != null) {
                    if (subscribeFlag == 2)
                        msg = getContext().getResources().getString(R.string.purchase_plan_dialog_body);
                    else if (subscribeFlag == 1)
                        msg = getContext().getResources().getString(R.string.expire_plan_dialog_body);
                    Utility.showToast(getContext(), msg);
                }
            } else
                new DialogInitialize().subscriptionDialog(new CustomDialogs(getContext(), this), subscribeFlag);

        }
    }

    @Override
    public void mediaClick() {
        BusinessHomeResponseModel.JobBean jobBean = null;
        if (jobList != null)
            jobBean = jobList.get(mCardStackLayoutManager.getTopPosition());
        List<String> imgs = new ArrayList<>();
if(jobBean.usrPrfl.img!=null)
    imgs.add(jobBean.usrPrfl.img);

if (jobBean.usrPrfl.imgs!=null && jobBean.usrPrfl.imgs.size()>0)
    imgs.addAll(jobBean.usrPrfl.imgs);

        if (jobBean.usrPrfl.video != null && !TextUtils.isEmpty(jobBean.usrPrfl.video) && !imgs.contains(jobBean.usrPrfl.video))
            imgs.add(jobBean.usrPrfl.video);

        if (imgs != null && imgs.size() > 0 && basePath != null) {
            Intent viewMedia = new Intent(getContext(), StoryViewActivity.class);
            viewMedia.putExtra(AppConstants.IMAGE_VIEW_DATA, Utility.gsonInstance().toJson(imgs));
            viewMedia.putExtra("BASE_URL_IMAGE", basePath);
            startActivity(viewMedia);
        }else {
            if(getContext()!=null)
            Utility.showToast(getContext(), getContext().getResources().getString(R.string.no_media_to_play));
        }
    }

    @Override
    public void onYesClicked(String from) {

        switch (from) {
            case PURCHASE_PLAN_FLAG:
                DashboardActivity dashboardActivity = (DashboardActivity) getActivity();
                if (dashboardActivity != null)
                    dashboardActivity.fireEvent(BS_POS_SUBSCRIBE, null);
                break;


        }
    }

    @Override
    public void onNoClicked(String from) {

    }

    @Override
    public void onApplyFilter(int distance) {
        if (jobList != null)
            jobList.clear();
        DEFAULT_DISTANCE = distance;
        pageNo = 1;
        fetchData(distance);
    }

    @Override
    public int getDistance() {
        return DEFAULT_DISTANCE;
    }


    @Override
    public void onScroll(BusinessHomeResponseModel.JobBean jobBean) {
        mPresenter.addBusinessCount(jobBean.id);
    }


}
