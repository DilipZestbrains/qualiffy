package com.app.qualiffyapp.ui.activities.signUpFlow.business.view;

import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ActivityLocationBinding;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BLoginSignUpBaseFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ILocationFragment;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.LocationPresenter;
import com.app.qualiffyapp.utils.Utility;

import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.AppConstants.LOCATION_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.LOCATION_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter.DestinationFragment.COMPANY_ADDRESS;

public class LocationFragment extends BLoginSignUpBaseFragment implements ILocationFragment {
    private ActivityLocationBinding mBinding;
    private LocationPresenter locationPresenter;

    @Override
    protected void initUi() {
        mBinding = (ActivityLocationBinding) viewDataBinding;
        locationPresenter = new LocationPresenter(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.not_now_txt:
                locationPresenter.gotoNextStep();
                break;

            case R.id.enable_location_txt:
                locationPresenter.proceedWithLocation();
        }
    }

    @Override
    protected void setListener() {
        mBinding.notNowTxt.setOnClickListener(this);
        mBinding.enableLocationTxt.setOnClickListener(this);
    }

    @Override
    protected String getHeader() {
        return null;
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_location;
    }


    @Override
    public void showToast(ToastType toastType) {

    }

    @Override
    public void proceed() {
        Utility.hideKeyboard(context);
        getLoginActivity().getPresenter().gotoFragment(COMPANY_ADDRESS, true, true);
    }

    @Override
    public void askLocationPermission() {
        onAskForSomePermission(context, LOCATION_PERMISION, LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        locationPresenter.gotoNextStep();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {

        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.permission_dialog),
                    requestCode, LOCATION_PERMISION);
        }

    }
}
