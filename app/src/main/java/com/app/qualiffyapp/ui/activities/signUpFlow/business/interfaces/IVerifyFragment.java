package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

public interface IVerifyFragment extends BaseInterface {

    void updateTime(String time);

    void onTimeCompletion();

    void onOtpVerification(String msg, VerifyResponseModel accessToken);

    void onOtpResend(String msg, SingupResponse model);

    void showToast(ToastType type);

    void preOtpVerification();


}
