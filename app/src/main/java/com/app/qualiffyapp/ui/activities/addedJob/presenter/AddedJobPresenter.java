package com.app.qualiffyapp.ui.activities.addedJob.presenter;

import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.viewCallbacks.SignupView;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADDED_JOB;

public class AddedJobPresenter {
    private SignUpInteractor interactor;
    private SignupView signupView;
    ApiListener<ResponseModel<AddedJobResponseModel>> apiListener = new ApiListener<ResponseModel<AddedJobResponseModel>>() {
        @Override
        public void onApiSuccess(ResponseModel<AddedJobResponseModel> response, int apiFlag) {
            if (response != null)
                signupView.apiSuccess(response, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            signupView.apiError(error, apiFlag);
        }
    };


    public AddedJobPresenter(SignupView view) {
        this.signupView = view;
        this.interactor = new SignUpInteractor();
    }

    public void getJobList(int pageNo) {
        interactor.businessAddedJobApi(apiListener, pageNo, API_FLAG_BUSINESS_ADDED_JOB);
    }


}
