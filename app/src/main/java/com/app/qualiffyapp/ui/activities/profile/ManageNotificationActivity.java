package com.app.qualiffyapp.ui.activities.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivitySettingBinding;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.profile.NotifBean;
import com.app.qualiffyapp.utils.Utility;
import com.google.gson.Gson;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_TOGGLE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.DATA_TO_SEND;
import static com.app.qualiffyapp.constants.AppConstants.FROM;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ManageNotificationActivity extends BaseActivity implements IManageNotification {
    private ActivitySettingBinding settingBinding;
    private ManageNotificationPresenter mPresenter;
    private String preActivity;
    private NotifBean notifyBean;


    @Override
    protected void initView() {
        settingBinding = (ActivitySettingBinding) viewDataBinding;
        mPresenter = new ManageNotificationPresenter(this);
        getIntentData();
        toolbar();
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            String data = bundle.getString(FROM);
            if (data != null) {
                preActivity = data;
                manageBusinessNotification(bundle.getString(DATA_TO_SEND, null));
            }
        }
    }

    private void toolbar() {
        settingBinding.toolbar.toolbarTitleTxt.setText(getResources().getString(R.string.manage_notifications));
        settingBinding.btn.btn.setText(getResources().getString(R.string.done));
    }

    @Override
    protected void setListener() {
        super.setListener();
        settingBinding.toolbar.backBtnImg.setOnClickListener(this);
        settingBinding.btn.btn.setOnClickListener(this);
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_setting;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.btn:
                RequestBean bean = new RequestBean();
                RequestBean.notify notify = new RequestBean.notify();
                notify.usr_f = settingBinding.toggleFollowUser.isChecked();
                notify.usr_rec = settingBinding.toggleRecommendOrg.isChecked();
                notify.calls = settingBinding.callNotification.isChecked();
                notify.chat = settingBinding.toggleChat.isChecked();
                bean.notif = notify;
                mPresenter.updateBusinessNotificationStatus(bean, API_FLAG_BUSINESS_TOGGLE);
                break;
        }
    }

    private void manageBusinessNotification(String notificationStatus) {
        if (notificationStatus != null) {
            notifyBean = new Gson().fromJson(notificationStatus, NotifBean.class);
            settingBinding.toggleFollowUser.setChecked(notifyBean.usr_f);
            settingBinding.toggleRecommendOrg.setChecked(notifyBean.usr_rec);
            settingBinding.toggleChat.setChecked(notifyBean.chat);
            settingBinding.callNotification.setChecked(notifyBean.calls);
        }
    }

    @Override
    public void showProgressDialog() {
        showProgress(true);
    }

    @Override
    public void onUpdateNotification(String msg, int status, int apiFlag) {
        showProgress(false);
        Utility.showToast(this, msg);
        if (status == SUCCESS) {
            if (notifyBean != null) {
                notifyBean.usr_f = settingBinding.toggleFollowUser.isChecked();
                notifyBean.usr_rec = settingBinding.toggleRecommendOrg.isChecked();
                notifyBean.chat = settingBinding.toggleChat.isChecked();
                notifyBean.calls = settingBinding.callNotification.isChecked();
                Intent intent = new Intent();
                intent.putExtra(DATA_TO_SEND, new Gson().toJson(notifyBean));
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
