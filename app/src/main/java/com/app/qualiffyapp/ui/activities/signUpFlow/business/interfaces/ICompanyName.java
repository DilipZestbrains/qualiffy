package com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;

import java.util.ArrayList;

public interface ICompanyName extends BaseInterface, IUpdatableDetail {

    void onNext(String companyName, String comStrength);

    void showCompanyStrengthDialog(ArrayList<String> list);

}
