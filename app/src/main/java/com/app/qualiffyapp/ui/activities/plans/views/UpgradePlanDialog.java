package com.app.qualiffyapp.ui.activities.plans.views;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.plans.IUgradePlan;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.utils.Utility;

public class UpgradePlanDialog {
    private Dialog dialog;
    private TextView tvSubscriptionDetail;
    private TextView tvPackage;
    private TextView tvPrice;

    public void prepareDialog(Context context, IUgradePlan ugradePlan, JsSubscriptionModel.SubsBean bean) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dailog_upgrade_plan);

        tvSubscriptionDetail = dialog.findViewById(R.id.tvSubscriptionDetail);
        tvSubscriptionDetail.setMovementMethod(new ScrollingMovementMethod());

        tvPrice = dialog.findViewById(R.id.tvPrice);
        tvPackage = dialog.findViewById(R.id.tvPackage);
        dialog.setCancelable(true);

        tvSubscriptionDetail.setText(Html.fromHtml(bean.details));
        tvPackage.setText(bean.name);
        tvPrice.setText(context.getString(R.string.dollar) + bean.price);

        dialog.findViewById(R.id.proceedPayBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ugradePlan.proceedPay(bean);
                dialog.hide();
            }
        });

        dialog.show();


    }
}
