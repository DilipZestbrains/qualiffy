package com.app.qualiffyapp.ui.activities.signUpFlow;

import android.content.Intent;
import android.view.View;
import androidx.fragment.app.FragmentActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.AlertCancelCallback;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivityPreBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.login.LoginActivity;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.firebase.iid.FirebaseInstanceId;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_LOGOUT;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_LOGOUT;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT_UNREGISTER;
import static com.app.qualiffyapp.constants.AppConstants.ERROR_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BUSINESS_CLEAR;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;


public class PreActivity extends BaseActivity implements SignupView, AlertCancelCallback {
    private ActivityPreBinding preBinding;
    private PreActivityPresenter presenter;


    @Override
    protected void initView() {
        preBinding = (ActivityPreBinding) viewDataBinding;
        preBinding.jobSeekerBtn.setOnClickListener(this);
        preBinding.businessBtn.setOnClickListener(this);
        presenter = new PreActivityPresenter(this);
        Utility.putBooleanValueInSharedPreference(PreActivity.this,
                AppConstants.APP_SESSION, false);

        getIntentData();
        firebaseToken();
    }

    private void firebaseToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    if (task.getResult() != null) {
                        String token = task.getResult().getToken();
                        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN, token);
                    }
                });
    }

    private void getIntentData() {
        Intent intent = getIntent();
        int clearDataValue = intent.getIntExtra(ERROR_CODE, 0);
        updateUserOnFirebase(clearDataValue);
        boolean isBusinessRemove = intent.getBooleanExtra(IS_BUSINESS_CLEAR, false);
        if ((clearDataValue == 2 || clearDataValue == 41) && isBusinessRemove) {
            Utility.showAlertDialogWithOkButton(this, getString(R.string.seesion_expired), this, API_FLAG_BUSINESS_LOGOUT);
        } else if (clearDataValue == 46 && isBusinessRemove)
            Utility.showAlertDialogWithOkButton(this, getString(R.string.account_deactivated_please_contact_admin), this, API_FLAG_BUSINESS_LOGOUT);
        else if (clearDataValue == 47 && !isBusinessRemove) {
            Utility.showAlertDialogWithOkButton(this, getString(R.string.account_deactivated_please_contact_admin), this, API_FLAG_JS_LOGOUT);
        } else if (clearDataValue != 0)
            Utility.clearAllSharedPrefData(this);

    }

    private void updateUserOnFirebase(int errorCode) {
        int userType = Utility.getIntFromSharedPreference(getViewContext(), USER_TYPE);
        if (userType != 0) {
            if ((errorCode == AppConstants.JOB_SEEKER_DELETED && userType == AppConstants.JOB_SEEKER) || (errorCode == AppConstants.ORGANISATION_DELETED &&userType == AppConstants.BUSINESS_USER)) {
                AppFirebaseDatabase.getInstance().deleteUser();
            }
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_pre;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.job_seeker_btn:
                openActivity(LoginActivity.class, null);
                break;

            case R.id.business_btn:
                openActivity(com.app.qualiffyapp.ui.activities.signUpFlow.business.LoginActivity.class, null);
                break;
        }

    }

    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        showProgressDialog(false);
        Utility.clearAllSharedPrefData(this);
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(CALL_INTENT_UNREGISTER));
    }

    @Override
    public void apiError(String error, int apiFlag) {
        showProgressDialog(false);
        Utility.clearAllSharedPrefData(this);
    }

    @Override
    public void validationFailed(String msg) {
    }

    @Override
    public FragmentActivity getViewContext() {
        return this;
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }

    @Override
    public void onAlertClick(int flag) {
        switch (flag) {
            case API_FLAG_BUSINESS_LOGOUT:
                showProgressDialog(true);
                AppFirebaseDatabase.clearFirebaseInstance();
                presenter.hitApi(API_FLAG_BUSINESS_LOGOUT);
                break;

            case API_FLAG_JS_LOGOUT:
                showProgressDialog(true);
                AppFirebaseDatabase.clearFirebaseInstance();
                presenter.hitApi(API_FLAG_JS_LOGOUT);
                break;
        }
    }
}
