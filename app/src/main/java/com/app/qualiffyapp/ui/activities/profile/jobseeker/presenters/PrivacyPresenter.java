package com.app.qualiffyapp.ui.activities.profile.jobseeker.presenters;

import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.profile.IManageNotification;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_JS_PRIVACY_TOOGLE;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class PrivacyPresenter {
    private final IManageNotification manageNotification;
    private final SignUpInteractor signUpInteractor;

    private ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            manageNotification.onUpdateNotification(response.msg, SUCCESS, apiFlag);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            manageNotification.onUpdateNotification(error, FAILURE, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            manageNotification.onUpdateNotification(null, -12, apiFlag);

        }
    };

    public PrivacyPresenter(IManageNotification manageNotification) {
        this.manageNotification = manageNotification;
        signUpInteractor = new SignUpInteractor();
    }


    public void hitapi(boolean isOffline) {
        manageNotification.showProgressDialog();
        RequestBean bean = new RequestBean();
        bean.pub = isOffline;
        signUpInteractor.jobsSeekerProfileToogle(msgApiListener, bean, API_FLAG_JS_PRIVACY_TOOGLE);

    }
}

