package com.app.qualiffyapp.ui.activities.subscription;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.adapter.plans.AdapterPlansList;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.callbacks.plans.IUgradePlan;
import com.app.qualiffyapp.databinding.FragmentPlansBinding;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.payment.ui.PaymentActivity;
import com.app.qualiffyapp.ui.activities.plans.views.PremiumPlanDialog;
import com.app.qualiffyapp.ui.activities.plans.views.UpgradePlanDialog;
import com.app.qualiffyapp.ui.activities.profile.businessProfile.presenters.SubscriptionPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.viewCallbacks.SignupView;
import com.google.gson.Gson;

import static com.app.qualiffyapp.constants.AppConstants.MONEY_TO_PAY;

public class BusinessSubscriptionFragment extends BaseFragment implements SignupView, IUgradePlan {
    private FragmentPlansBinding mBinding;
    private AdapterPlansList subscriptionAdapter;
    private SubscriptionPresenter subscriptionPresenter;
    private UpgradePlanDialog planDailog;
    private PremiumPlanDialog premiumPlanDialog;


    @Override
    protected void initUi() {
        mBinding = (FragmentPlansBinding) viewDataBinding;
        subscriptionPresenter = new SubscriptionPresenter(this);
        planDailog = new UpgradePlanDialog();
        premiumPlanDialog = new PremiumPlanDialog();
        mBinding.swipeToRefresh.setRefreshing(true);
        fetchData();
    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_plans;
    }

    @Override
    public void onClick(View v) {
    }

    @Override
    protected void setListener() {
        mBinding.swipeToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchData();
            }
        });


    }

    private void fetchData() {
        subscriptionPresenter.hitApi(1, 20);

    }


    @Override
    public void apiSuccess(ResponseModel response, int apiFlag) {
        mBinding.swipeToRefresh.setRefreshing(false);
        if (response.getStatus() == 1) {
            JsSubscriptionModel subscriptionModel = (JsSubscriptionModel) response.getRes();
            subscriptionAdapter = new AdapterPlansList(getContext(), subscriptionModel.subs, this);
            mBinding.rvPlans.setAdapter(subscriptionAdapter);

        } else {
            Utility.showToast(getViewContext(), response.err.msg);
        }
    }

    @Override
    public void apiError(String error, int apiFlag) {
        mBinding.swipeToRefresh.setRefreshing(false);
        validationFailed(error);
    }

    @Override
    public void validationFailed(String msg) {
        Utility.showToast(getViewContext(), msg);
    }

    @Override
    public FragmentActivity getViewContext() {
        return getActivity();
    }

    @Override
    public void showProgressDialog(boolean showProgress) {
        showProgress(showProgress);
    }


    @Override
    public void upgradeClick(int pos, JsSubscriptionModel.SubsBean bean) {
        planDailog.prepareDialog(getContext(), this, bean);
    }

    @Override
    public void premiumClick(int pos, JsSubscriptionModel.SubsBean bean) {
        premiumPlanDialog.prepareDialog(getContext(), this, bean.details, bean.name);
    }

    @Override
    public void requestQuoteClick() {

    }

    @Override
    public void proceedPay(JsSubscriptionModel.SubsBean bean) {
        Bundle bundle = new Bundle();
        bundle.putString(MONEY_TO_PAY, new Gson().toJson(bean));
        openActivity(PaymentActivity.class, bundle);
    }
}
