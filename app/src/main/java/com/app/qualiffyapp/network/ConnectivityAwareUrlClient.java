package com.app.qualiffyapp.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


/**
 *
 */
public class ConnectivityAwareUrlClient implements Interceptor {
    private Context mContext;

    /**
     * @param context
     */
    public ConnectivityAwareUrlClient(Context context) {
        mContext = context;
    }

    /**
     * @param context
     */
    public static boolean isOnline(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    /**
     * @param chain
     */
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!isOnline(mContext)) {
            throw new NoConnectivityException();
        }
        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }

    /**
     *
     */
    public class NoConnectivityException extends IOException {

        /**
         *
         */
        @Override
        public String getMessage() {
            return "Please check internet connection.";
        }

    }
}


