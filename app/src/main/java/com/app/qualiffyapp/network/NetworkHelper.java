package com.app.qualiffyapp.network;

import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.utils.Utility;

import java.net.UnknownHostException;

public class NetworkHelper {
    public static void onFailure(Throwable t, AppCompatActivity mContext) {
        if (t instanceof UnknownHostException)
            Utility.showToast(mContext, "Network unavailable");
        else
            Utility.showToast(mContext, "Something went wrong");
    }
}
