package com.app.qualiffyapp.network;

/**
 * API listener interface with one generic type
 */
public interface ApiListener<T> {
    /**
     * Callback for success response
     *
     * @param response Response object
     * @param apiFlag
     */
    void onApiSuccess(T response, int apiFlag);

    /**
     * Callback for error response
     *
     * @param error   cause of error
     * @param apiFlag
     */
    void onApiError(String error, int apiFlag);

    default void onApiNoResponse(int apiFlag) {
    }

    ;


}
