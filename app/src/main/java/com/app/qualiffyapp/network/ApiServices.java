package com.app.qualiffyapp.network;

import com.app.qualiffyapp.models.ConnectionResponseModel;
import com.app.qualiffyapp.models.ContactSyncResponse;
import com.app.qualiffyapp.models.NotificationModel;
import com.app.qualiffyapp.models.PhoneRequestBean;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.ResendOtpResponse;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.VerifyResponseModel;
import com.app.qualiffyapp.models.WebResponseModel;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsProfileResponseModel;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;
import com.app.qualiffyapp.models.contact.BlockContactResonseModel;
import com.app.qualiffyapp.models.createProfile.AddCustomeJobModel;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.models.createProfile.CreateJobSeekerProfileInputModel;
import com.app.qualiffyapp.models.createProfile.IndustryResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;
import com.app.qualiffyapp.models.home.business.UserFrndListResponseModel;
import com.app.qualiffyapp.models.home.business.ViewerResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;
import com.app.qualiffyapp.models.home.jobSeeker.RecommendResponseModel;
import com.app.qualiffyapp.models.payment.PaymentHistoryResponse;
import com.app.qualiffyapp.models.payment.PaymentReceiptRequest;
import com.app.qualiffyapp.models.profile.BusinessProfileResponseModel;
import com.app.qualiffyapp.models.profile.OtherUserProfileResponseModel;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerProfileResponse;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.models.recruiter.RecruiterListResponseModel;
import com.app.qualiffyapp.models.signup.SingupResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.app.qualiffyapp.network.Urls.*;

public interface ApiServices {

    @POST(SINGUP)
    Call<ResponseModel<VerifyResponseModel>> singUpApi(@Body RequestBean bean);

    @POST(SOCIAL_SIGNUP)
    Call<ResponseModel<VerifyResponseModel>> socialSingUpApi(@Body RequestBean bean);

    @FormUrlEncoded
    @POST(VERIFY_OTP)
    Call<ResponseModel<VerifyResponseModel>> verifyOtpApi(@Path("userid") String userId, @Field(
            "otp") String otp, @Field("dev_tkn") String devTk);

    @FormUrlEncoded
    @POST(LOGIN)
    Call<ResponseModel<VerifyResponseModel>> loginApi(@Field("upno") String phnNum, @Field("dev_tkn") String devTkn, @Field("dev_type") String devType);

    @GET(GET_INDUSTRY)
    Call<ResponseModel<IndustryResponseModel>> getIndustryApi(@Query("limit") String limit, @Query("sKey") String sKey);

    @GET(GET_ROLE_SEEKING)
    Call<ResponseModel<IndustryResponseModel>> getRoleSeekingApi(@Query("limit") String limit, @Query("sKey") String sKey);

    @GET(GET_ROLE)
    Call<ResponseModel<IndustryResponseModel>> getRoleApi(@Path("jobcategoryid") String indusId, @Query("limit") String limit, @Query("sKey") String sKey);

    @POST(SINGUP_1)
    Call<ResponseModel<IndustryResponseModel>> signUp1Api(@Body CreateJobSeekerProfileInputModel profileInputModel);

    @POST(SINGUP_2)
    Call<ResponseModel<MsgResponseModel>> signUp2Api(@Body CreateJobSeekerProfileInputModel profileInputModel);

    @POST(SINGUP_3)
    Call<ResponseModel<MsgResponseModel>> signUp3Api(@Body CreateJobSeekerProfileInputModel profileInputModel);

    @POST(SINGUP_4)
    Call<ResponseModel<MsgResponseModel>> signUp4Api(@Body CreateJobSeekerProfileInputModel profileInputModel);

    @POST(SINGUP_5)
    Call<ResponseModel<VerifyResponseModel>> signUp5Api(@Body RequestBean requestBean);

    @Multipart
    @POST(ADD_MEDIA)
    Call<ResponseModel<AddMediaResponseModel>> addMedia(@Part MultipartBody.Part filePart, @Part("uni") RequestBody id, @Part("isProfile") RequestBody isProfile);


    @DELETE(DELETE_MEDIA)
    Call<ResponseModel<AddMediaResponseModel>> deleteMedia(@Query("name") String mediaId, @Query("type") String mediaType);


    //*********************** Job Seeker Profile ********************

    @PUT(JOB_SEEKER_DETAILS_UPDATE)
    Call<ResponseModel<MsgResponseModel>> updateJobSeekerDetails(@Body RequestBean requestBean);

    @GET(JOBSEEKER_INFO)
    Call<ResponseModel<JobSeekerProfileResponse>> getJobSeekerDetails();

    @POST(JOB_SEEKER_EDUCATION)
    Call<ResponseModel<VerifyResponseModel>> addJobSeekerEducation(@Body RequestBean bean);

    @DELETE(JOB_SEEKER_EDUCATION_UPDATE)
    Call<ResponseModel<MsgResponseModel>> deleteJobSeekerEducation(@Path("educationid") String eduId);

    @PUT(JOB_SEEKER_EDUCATION_UPDATE)
    Call<ResponseModel<VerifyResponseModel>> updateJobSeekerEducaiton(@Path("educationid") String eduId, @Body RequestBean bean);

    @DELETE(JOB_SEEKER_EDUCATION_UPDATE)
    Call<ResponseModel<VerifyResponseModel>> deleteEducation(@Path("educationid") String eduId);

    @POST(JOB_SEEKER_LOGOUT)
    Call<ResponseModel<MsgResponseModel>> jobSeekerLogout();

    @GET(JOB_SEEKER_SUBSCRIPTION)
    Call<ResponseModel<JsSubscriptionModel>> jobSeekerSubscription(@Query("page") int page, @Query("limit") int limit);

    @GET(BUSINESS_SUBSCRIPTION)
    Call<ResponseModel<JsSubscriptionModel>> businessSubscription(@Query("page") int page, @Query("limit") int limit);

    @GET(JOB_SEEKER_SUBSCRIPTION_CURRENT)
    Call<ResponseModel<JsSubscriptionModel>> jobSeekerSubscriptionCurrent();

    @PUT(JOB_SEEKER_PROFILE_TOOGLE)
    Call<ResponseModel<MsgResponseModel>> JobSeekerProfileToogle(@Body RequestBean requestBean);

    @POST(JOB_SEEKER_PROFILE_DELETE)
    Call<ResponseModel<MsgResponseModel>> jobSeekerDelete();

    @GET(JOB_SEEKER_OWN_RECOMMENDATION_LIST)
    Call<ResponseModel<ViewerResponseModel>> jobSeekerOwnRecmdation(@Query("page") int page, @Query("limit") int limit);

    @GET(JOB_SEEKER_OWN_FOLLOWING_LIST)
    Call<ResponseModel<ViewerResponseModel>> jobSeekerOwnFollowing(@Query("page") int page, @Query("limit") int limit);


    // Job Seeker Home
    @GET(JOB_SEEKER_HOME)
    Call<ResponseModel<JsJobsResponseModel>> jobSeekerJobs(@Query("page") int page, @Query("sKey") String searchKey, @Query("limit") int limit, @Query("distance") int distance);

    @Multipart
    @POST(JOB_SEEKER_APPLY_JOB)
    Call<ResponseModel<MsgResponseModel>> jobSeekerApplyJobWithVideo(@Part MultipartBody.Part filePart, @Path("jobid") String jobId);

    @FormUrlEncoded
    @POST(JOB_SEEKER_APPLY_JOB)
    Call<ResponseModel<MsgResponseModel>> jobSeekerApplyJob(@Path("jobid") String jobId, @Field("isProfile") String isProfileVideo);

    @GET(ADDED_JOB_LIST)
    Call<ResponseModel<OrgJobListResponseModel>> jobSeekerJobList(@Path("orgid") String isRecommend, @Query("page") int pageNo, @Query("limit") int limit);

    @FormUrlEncoded
    @POST(JOB_SEEKER_ISRECOMMEND)
    Call<ResponseModel<MsgResponseModel>> jobSeekrIsRecommend(@Path("orgid") String OrgId, @Field("r_key") String isRecommend);

    @FormUrlEncoded
    @POST(JOB_SEEKER_ISFOLLOW)
    Call<ResponseModel<MsgResponseModel>> jobSeekrIsFollow(@Path("orgid") String OrgId, @Field("f_key") String isFollow);


    @GET(JOB_SEEKER_RECOMMEND_JOB_LIST)
    Call<ResponseModel<RecommendResponseModel>> jobSeekerRecommendJobList(@Path("orgid") String orgId, @Query("r_key") String isRecommend, @Query("page") int page, @Query("limit") int limit);

    @GET(JOB_SEEKER_APPLIED_JOBS)
    Call<ResponseModel<AppliedJobResponseModel>> jobSeekerAppliedJob(@Query("page") int page, @Query("limit") int limit, @Query("isShort") String isShortLsit);


    @GET(JOB_SEEKER_OTHER_USER_PROFILE)
    Call<ResponseModel<OtherUserProfileResponseModel>> jobSeekerOtherUserProfile(@Path("userid") String userid);

    @FormUrlEncoded

    @POST(JOB_SEEKER_OTHER_USER_BADGE)
    Call<ResponseModel<MsgResponseModel>> jobSeekerOtherUserBadge(@Path("userid") String userid, @Field("key") int key);


    //Chat
    @POST(JOB_SEEKER_ADD_FRNDS)
    Call<ResponseModel<MsgResponseModel>> jobSeekrAddFrnd(@Path("userid") String userid);

    @POST(JOB_SEEKER_ACCEPT_FRNDS)
    Call<ResponseModel<MsgResponseModel>> jobSeekrAcceptFrnd(@Path("userid") String userid);

    @POST(JOB_SEEKER_REJECT_FRNDS)
    Call<ResponseModel<MsgResponseModel>> jobSeekrRejectFrnd(@Path("userid") String userid);

    @FormUrlEncoded
    @POST(JOB_SEEKER_BLOCK_FRNDS)
    Call<ResponseModel<MsgResponseModel>> jobSeekrblockUnblockFrnd(@Path("userid") String userid, @Field("b_key") String blockKey);

    @GET(JOB_SEEKER_BLOCK_FRNDS_LIST)
    Call<ResponseModel<BlockContactResonseModel>> jobSeekrblockFrndList(@Query("page") int page, @Query("limit") int limit);


    // Business
    @POST(BUSINESS_SIGNUP)
    Call<ResponseModel<SingupResponse>> businessSignUp(@Body RequestBean requestBean);

    @POST(BUSINESS_SIGNUP)
    Call<ResponseModel<VerifyResponseModel>> businessVerify(@Body RequestBean requestBean);

    @Multipart
    @POST(BUSINESS_SIGNUP_1)
    Call<ResponseModel<VerifyResponseModel>> businessSignUp2(@Part MultipartBody.Part img, @PartMap Map<String, RequestBody> companyBean);

    @FormUrlEncoded
    @POST(BUSINESS_VERIFY_OTP)
    Call<ResponseModel<VerifyResponseModel>> businessVerifyOtp(@Path("orgid") String orgid, @Field("otp") String otp, @Field("dev_tkn") String deviceToken);

    @POST(BUSINESS_RESEND_OTP)
    Call<ResponseModel<ResendOtpResponse>> businessResendOtp(@Path("orgid") String orgid);

    @FormUrlEncoded
    @POST(BUSINESS_LOGIN)
    Call<ResponseModel<VerifyResponseModel>> businessLogin(@Field("pno") String phone, @Field("key") String pass, @Field("dev_tkn") String deviceToken);


    @POST(BUSINESS_LARGE_ORG_LOGIN)
    Call<ResponseModel<SingupResponse>> largeBusinessLogin(@Body RequestBean requestBean);

    @POST(BUSINESS_LARGE_ORG_LOGIN)
    Call<ResponseModel<VerifyResponseModel>> largeBusinessLoginVerify(@Body RequestBean requestBean);

    @FormUrlEncoded
    @POST(BUSINESS_LARGE_ORG_LOGIN)
    Call<ResponseModel<VerifyResponseModel>> largeBusinessLoginEmail(@Field("identity") String email, @Field("key") String pass, @Field("dev_tkn") String deviceToken);

    @FormUrlEncoded
    @POST(BUSINESS_FORGOT_PASSWORD)
    Call<ResponseModel<MsgResponseModel>> forgotPass(@Field("email") String Email);


    @GET(BUSINESS_PROFILE)
    Call<ResponseModel<BusinessProfileResponseModel>> getBusinessProfile();

    @PUT(BUSINESS_PROFILE_EDIT)
    Call<ResponseModel<MsgResponseModel>> businessProfileEdit(@Body RequestBean requestBean);

    @Multipart
    @POST(ADD_MEDIA_ORG)
    Call<ResponseModel<AddMediaResponseModel>> addMediaOrganization(@Part MultipartBody.Part filePart, @PartMap Map<String, RequestBody> requestBean);

    @FormUrlEncoded
    @POST(ADD_CUSTOM_JOB_TITLE)
    Call<ResponseModel<AddCustomeJobModel>> addCustomJobTitle(@Path("jobcategoryid") String cat_id, @Field("name") String oldPass);

    @FormUrlEncoded
    @PUT(BUSINESS_CHANGE_PASS)
    Call<ResponseModel<MsgResponseModel>> changePasswordBusiness(@Field("old_pass") String oldPass, @Field("new_pass") String newPass);

    @DELETE(DELETE_MEDIA_ORG)
    Call<ResponseModel<AddMediaResponseModel>> deleteMediaOrganization(@Query("name") String mediaId,@Query("type") int type);

    @POST(DELETE_BUSINESS)
    Call<ResponseModel<MsgResponseModel>> deleteOrganisation();

    @FormUrlEncoded
    @POST(LOGOUT_BUSINESS)
    Call<ResponseModel<MsgResponseModel>> logoutOrganisation(@Field("dev_tkn") String dev_tkn);

    @GET(COMPANY_PORTFOLIO)
    Call<ResponseModel<WebResponseModel>> getCompanyPortfolio(@Query("type") String type);

    @PUT(BusinessToggle)
    Call<ResponseModel<MsgResponseModel>> manageBusinessToggle(@Body RequestBean body);

    @DELETE(BUSINESS_DELETE_ALL_MEDIA)
    Call<ResponseModel<MsgResponseModel>> deleteAllBusinessMedia();

    @FormUrlEncoded
    @POST(ADD_CUSTOM_JOB_TITLE_WT_CAT)
    Call<ResponseModel<AddCustomeJobModel>> addCustomJobTitleCustom(@Field("name") String name);

    // Business Home
    @GET(BUSINESS_HOME)
    Call<ResponseModel<BusinessHomeResponseModel>> businessHomeApi(@Query("page") int page, @Query("limit") int limit, @Query("sKey") String searchkey, @Query("distance") int distance);

    @POST(BUSINESS_ADD_JOB)
    Call<ResponseModel<MsgResponseModel>> businessAddJobApi(@Body RequestBean requestBean);

    @DELETE(BUSINESS_DELETE_JOB)
    Call<ResponseModel<MsgResponseModel>> businessDeleteJobApi(@Path("jobid") String jobId);

    @FormUrlEncoded
    @PUT(BUSINESS_EDIT_JOB)
    Call<ResponseModel<MsgResponseModel>> businessEditJobApi(@Path("jobid") String jobId, @Field("date") String date);

    @GET(BUSINESS_ADDED_JOB)
    Call<ResponseModel<AddedJobResponseModel>> businessAddedJobApi(@Query("page") int page, @Query("limit") int limit);

    @GET(BUSINESS_JOB_APPLICANTS)
    Call<ResponseModel<ApplicantsResponseModel>> businessJobApplicantsApi(@Path("jobid") String jobId, @Query("page") int page, @Query("limit") int limit);

    @GET(BUSINESS_APPLICANTS_PROFILE)
    Call<ResponseModel<ApplicantsProfileResponseModel>> businessApplicantProfileApi(@Path("userid") String userid);

    @POST(BUSINESS_OTHER_USER_BADGE)
    @FormUrlEncoded
    Call<ResponseModel<MsgResponseModel>> businessOtherUserBadge(@Path("userid") String userid, @Field("key") String key);

    //    @GET(BUSINESS_OTHER_USER_BADGE_LIST)
    @GET(BUSINESS_APPLICANTS_PROFILE)
    Call<ResponseModel<OtherUserProfileResponseModel>> businessOtherUserProfileApi(@Path("userid") String userid);


    @GET(JOB_SEEKER_OTHER_USER_BADGE_LIST)
    Call<ResponseModel<RecommendResponseModel>> businessDashBoardRecommendList(@Path("userid") String userid, @Query("page") int pageNo, @Query("limit") int limit);

    @POST(BUSINESS_APPLICANTS_ADD_VIEW_COUNT)
    Call<ResponseModel<MsgResponseModel>> businessApplicantAddViewCountApi(@Path("userid") String userid);

    @GET(BUSINESS_USER_VIEWER)
    Call<ResponseModel<ViewerResponseModel>> businessUserViewerApi(@Path("userid") String userid, @Query("page") int page, @Query("limit") int limit);


    @GET(BUSINESS_USER_FRND_LIST)
    Call<ResponseModel<UserFrndListResponseModel>> businessFrndListApi(@Path("userid") String userid, @Query("page") int pageNo, @Query("limit") int limit);

 @GET(BUSINESS_UN_RECOMMEND)
    Call<ResponseModel<RecommendResponseModel>> orgUnRecommendListApi(@Query("page") int pageNo, @Query("limit") int limit);


    @GET(BUSINESS_RECRUITER_LIST)
    Call<ResponseModel<RecruiterListResponseModel>> businessRecruiterListApi(@Query("page") int pageNo, @Query("limit") int limit);


    @FormUrlEncoded
    @POST(BUSINESS_IS_FOLLOW)
    Call<ResponseModel<MsgResponseModel>> businessIsFollow(@Path("userid") String userId, @Field("f_key") String isFollow);


    @FormUrlEncoded
    @POST(BUSINESS_IS_SHORTLIST)
    Call<ResponseModel<MsgResponseModel>> businessIsShortList(@Path("jobid") String jobid, @Path("userid") String userId, @Field("s_key") String isShortlist);

    @GET(BUSINESS_FOLLOWER)
    Call<ResponseModel<RecommendResponseModel>> getBusinessFollower(@Query("page") int page, @Query("limit") int limit);

    //chat
    @Multipart
    @POST(UPLOAD_MEDIA_CHAT)
    Call<ResponseModel<AddMediaResponseModel>> uploadChatMedia(@Part MultipartBody.Part filePart, @Part("uni") RequestBody body);

    @POST(DELETE_MEDIA_CHAT)
    Call<ResponseModel<MsgResponseModel>> deleteChatMedia(@Body RequestBean requestBean);

    @Multipart
    @POST(UPLOAD_MEDIA_CHAT_ORG)
    Call<ResponseModel<AddMediaResponseModel>> uploadOrgChatMedia(@Part MultipartBody.Part filePart, @Part("uni") RequestBody body);


    @POST(DELETE_MEDIA_CHAT_ORG)
    Call<ResponseModel<MsgResponseModel>> deleteOrgChatMedia(@Body RequestBean requestBean);


    @POST(SYNC_CONTACTS)
    Call<ResponseModel<ContactSyncResponse>> syncContacts(@Body PhoneRequestBean requestBean);

    @Multipart
    @POST(ORG_PROMOTION_GET_ADD_UPDATE)
    Call<ResponseModel<MsgResponseModel>> addPromotion(@Part MultipartBody.Part filePart, @PartMap Map<String, RequestBody> companyBean);

    @Multipart
    @PUT(ORG_PROMOTION_UPDATE)
    Call<ResponseModel<MsgResponseModel>> updatePromotion(@Path("proid") String id, @Part MultipartBody.Part filepart, @PartMap Map<String, RequestBody> companyBean);

    @GET(ORG_PROMOTION_GET_ADD_UPDATE)
    Call<ResponseModel<PromotionListResponseModel>> getAllPromotions();

    @GET(ORG_NOTIFICATION_GET)
    Call<ResponseModel<NotificationModel>> getAllNotificationsBusiness(@Query("page") String page,
                                                                       @Query("limit") String limit);

    @GET(BUSINESS_SUBSCRIPTION_CURRENT)
    Call<ResponseModel<JsSubscriptionModel>> businessSubscriptionCurrent();

    @GET(JOb_SEEKER_NOTIFICATION_GET)
    Call<ResponseModel<NotificationModel>> getAllNotificationsJobSeeker(@Query("page") String page, @Query("limit") String limit);

    @DELETE(JOBSEEKER_NOTIFICATION_DELETE)
    Call<ResponseModel<NotificationModel>> deleteNotificationJobSeeker(@Path("notificationid") String eduId);

    @DELETE(BUSINESS_NOTIFICATION_DELETE)
    Call<ResponseModel<NotificationModel>> deleteNotificationBusiness(@Path("notificationid") String eduId);

    @POST(SEND_NOTIFICATION_JOBSEEKER)
    Call<ResponseModel<MsgResponseModel>> sendNotificationByJobSeeker(@Path("userid") String userId, @Query("user_type") String userType, @Body RequestBean bean);

    @POST(SEND_NOTIFICATION_ORGANISATION)
    Call<ResponseModel<MsgResponseModel>> sendNotificationByOrganisation(@Path("userid") String userId, @Query("user_type") String userType, @Body RequestBean bean);

    @POST(ADD_USER_CUSTOMER_ID)
    Call<ResponseModel<MsgResponseModel>> addUserCustomerId(@Path("cusid") String customerId);

    @POST(ADD_ORG_CUSTOMER_ID)
    Call<ResponseModel<MsgResponseModel>> addOrgCustomerId(@Path("cusid") String customerId);

    @POST(ADD_USER_PAYMENT_RECEIPT)
    Call<ResponseModel<MsgResponseModel>> addUserPaymentReceipt(@Body PaymentReceiptRequest paymentResponse);

    @POST(ADD_ORGANISATION_PAYMENT_RECEIPT)
    Call<ResponseModel<MsgResponseModel>> addOrganisationPaymentReceipt(@Body PaymentReceiptRequest paymentResponse);

    @GET(USER_PAYMENTS_LIST)
    Call<ResponseModel<PaymentHistoryResponse>> getUserPayments();

    @GET(ORG_PAYMENTS_LIST)
    Call<ResponseModel<PaymentHistoryResponse>> getOrgPayments();

    @GET(ORG_RECOMMENDATION)
    Call<ResponseModel<RecommendResponseModel>> getBusinessRecommendation(@Query("page") int pageNo, @Query("limit") int paginationLimit);

    @POST(ADD_RECRUITER)
    Call<ResponseModel<MsgResponseModel>> addRecruiter(@Body RequestBean requestBean);

    @POST(DELETE_RECRUITER)
    Call<ResponseModel<MsgResponseModel>> deleteRecruiter(@Path("id") String id);

    @GET(VIEW_BUSINESS_PROFILE)
    Call<ResponseModel<BusinessProfileResponseModel>> getRecruiterBusinessProfile();

    @GET(JOBSEEKER_CONNECTION_LISTING)
    Call<ResponseModel<RecommendResponseModel>> getUserConnection(@Query("page") int page);

    @GET(JOBSEEKER_VIEWER_LISTING)
    Call<ResponseModel<ViewerResponseModel>> getUserViewer(@Query("page") int page);

    @GET(ORGANISATION_PROFILE_BY_JOBSEEKER)
    Call<ResponseModel<BusinessProfileResponseModel>> getOrgProfileDetails(@Path("orgnid") String id);
}
