package com.app.qualiffyapp.network;

public interface Urls {

    //live
//    String BASE_URL_DEVELOPMENT = "https://admin.qualiffy.com:3090/";

    //Development
      String BASE_URL_DEVELOPMENT = "https://devadmin.qualiffy.com:3090";

    String BASE_URL_STRIPE = "https://api.stripe.com";
    String SINGUP = "/api/v2/user";
    String VERIFY_OTP = "/api/v2/user/{userid}/verify/otp";
    String LOGIN = "/api/v2/user/login";
    String GET_INDUSTRY = "/api/v2/job/categories/listing/app";
    String GET_ROLE_SEEKING = "/api/v2/job/titles/listing/app";
    String GET_ROLE = "/api/v2/job/titles/{jobcategoryid}/listing/app";
    String SINGUP_1 = "/api/v2/user/signup/1";
    String SINGUP_2 = "/api/v2/user/signup/2";
    String SINGUP_3 = "/api/v2/user/signup/3";
    String SINGUP_4 = "/api/v2/user/signup/4";
    String SINGUP_5 = "/api/v2/user/signup/5";
    String ADD_MEDIA = "/api/v2/user/upload/media";
    String ADD_MEDIA_ORG = "/api/v2/org/upload/media";
    String GET_MEDIA = "/api/v2/org/media/details";
    String DELETE_MEDIA = "/api/v2/user/media";
    String DELETE_MEDIA_ORG = "/api/v2/org/media";
    String JOB_SEEKER_DETAILS_UPDATE = "/api/v2/user/details";
    String JOB_SEEKER_EDUCATION = "/api/v2/user/education";

    //JobSeeker profile
    String JOBSEEKER_INFO = "/api/v2/user/info";
    String JOB_SEEKER_EDUCATION_UPDATE = "/api/v2/user/education/{educationid}";
    String JOB_SEEKER_LOGOUT = "/api/v2/user/logout";
    String JOB_SEEKER_SUBSCRIPTION = "/api/v2/subs/listing/usr";
    String JOB_SEEKER_SUBSCRIPTION_CURRENT = "/api/v2/user/current/subs";
    String JOB_SEEKER_PROFILE_TOOGLE = "/api/v2/user/toggle";
    String JOB_SEEKER_PROFILE_DELETE = "/api/v2/user/delete/account";
    String JOB_SEEKER_OWN_RECOMMENDATION_LIST = "/api/v2/user/own/recommend/list";
    String JOB_SEEKER_OWN_FOLLOWING_LIST = "/api/v2/follow/by/user/org/list";

    //job Seeker Home
    String JOB_SEEKER_HOME = "/api/v2/job/listing/user";
    String JOB_SEEKER_APPLY_JOB = "/api/v2/applied/job/action/{jobid}";
    String ADDED_JOB_LIST = "/api/v2/job/listing/user/{orgid}";
    String JOB_SEEKER_ISRECOMMEND = "/api/v2/user/recommend/{orgid}";
    String JOB_SEEKER_ISFOLLOW = "/api/v2/follow/by/user/{orgid}";

    String JOB_SEEKER_RECOMMEND_JOB_LIST = "/api/v2/user/recommend/list/{orgid}";
    String JOB_SEEKER_APPLIED_JOBS = "/api/v2/applied/job/listing";
    String JOB_SEEKER_OTHER_USER_PROFILE = "/api/v2/user/info/view/{userid}";
    String JOB_SEEKER_OTHER_USER_BADGE = "/api/v2/user/badges/{userid}";
    String JOB_SEEKER_OTHER_USER_BADGE_LIST = "/api/v2/org/badges/list/{userid}";

    //Add frnd
    String JOB_SEEKER_ADD_FRNDS = "/api/v2/friends/connect/{userid}";
    String JOB_SEEKER_ACCEPT_FRNDS = "/api/v2/friends/accept/{userid}";
    String JOB_SEEKER_REJECT_FRNDS = "/api/v2/friends/disconnect/{userid}";
    String JOB_SEEKER_BLOCK_FRNDS = "/api/v2/user/block/unblock/{userid}";
    String JOB_SEEKER_BLOCK_FRNDS_LIST = "/api/v2/user/block/unblock/list";


    //Business
    String BUSINESS_LOGIN = "/api/v2/org/login";
    String BUSINESS_SIGNUP = "/api/v2/org/signup";
    String BUSINESS_VERIFY_OTP = "/api/v2/org/{orgid}/verify/otp";
    String BUSINESS_RESEND_OTP = "/api/v2/org/{orgid}/resend/otp";
    String BUSINESS_SIGNUP_1 = "/api/v2/org/signup/1";
    String SOCIAL_SIGNUP = "/api/v2/user/social";
    String BUSINESS_LARGE_ORG_LOGIN = "/api/v2/org/login";
    String BUSINESS_FORGOT_PASSWORD = "/api/v2/org/forgot/password";

    String BUSINESS_PROFILE = "/api/v2/org/view/";
    String BUSINESS_PROFILE_EDIT = "/api/v2/org/edit/";
    String BUSINESS_CHANGE_PASS = "/api/v2/org/change/pass";
    String DELETE_BUSINESS = "/api/v2/org/delete/account";
    String LOGOUT_BUSINESS = "/api/v2/org/logout";
    String COMPANY_PORTFOLIO = "/api/v2/compPortfolio";
    String BusinessToggle = "/api/v2/org/toggle";
    String BUSINESS_DELETE_ALL_MEDIA = "/api/v2/org/media/all";
    String BUSINESS_UN_RECOMMEND = "/api/v2/org/own/unrecommend/list";

    String ADD_CUSTOM_JOB_TITLE = "/api/v2/job/titles/{jobcategoryid}/add/app";
    String ADD_CUSTOM_JOB_TITLE_WT_CAT = "/api/v2/job/titles/custom/add";

    //Business Home
    String BUSINESS_HOME = "/api/v2/job/org/dashboard/list";
    String BUSINESS_IS_FOLLOW = "/api/v2/follow/by/org/{userid}";
    String BUSINESS_IS_SHORTLIST = "/api/v2/applied/job/org/shortlist/{jobid}/{userid}";
    String BUSINESS_USER_VIEWER = "/api/v2/applied/job/view/user/org/list/{userid}";
    String BUSINESS_USER_FRND_LIST = "/api/v2/friends/dashboard/user/list/{userid}";

    String BUSINESS_ADDED_JOB = "/api/v2/job/listing/org/app";
    String BUSINESS_JOB_APPLICANTS = "/api/v2/applied/job/users/app/{jobid}";
    String BUSINESS_ADD_JOB = "/api/v2/job";
    String BUSINESS_DELETE_JOB = "/api/v2/job/{jobid}";
    String BUSINESS_EDIT_JOB = "/api/v2/job/{jobid}/app";
    String BUSINESS_APPLICANTS_PROFILE = "/api/v2/user/org/view/{userid}";
    String BUSINESS_OTHER_USER_BADGE = "/api/v2/org/badges/{userid}";
    String BUSINESS_OTHER_USER_BADGE_LIST = "/api/v2/user/badges/list";


    String BUSINESS_RECRUITER_LIST = "/api/v2/org/recruiter/list";


    String BUSINESS_APPLICANTS_ADD_VIEW_COUNT = "/api/v2/applied/job/org/viewed/{userid}";
    String BUSINESS_SUBSCRIPTION_CURRENT = "/api/v2/org/current/subs";
    String BUSINESS_SUBSCRIPTION = "/api/v2/subs/listing/org";


    //chat
    String UPLOAD_MEDIA_CHAT = "/api/v2/user/firebase/media";
    String DELETE_MEDIA_CHAT = "/api/v2/user/firebase/media/del";
    String UPLOAD_MEDIA_CHAT_ORG = "/api/v2/org/firebase/media";
    String DELETE_MEDIA_CHAT_ORG = "/api/v2/org/firebase/media/del";
    String SYNC_CONTACTS = "/api/v2/user/phone/details/sync";

    //Promotions

    String ORG_PROMOTION_GET_ADD_UPDATE = "/api/v2/promotion/";
    String ORG_PROMOTION_UPDATE = "/api/v2/promotion/{proid}";

    //Notifications
    String ORG_NOTIFICATION_GET = "/api/v2/notification/org/listing/";
    String JOb_SEEKER_NOTIFICATION_GET = "/api/v2/notification/user/listing/";
    String JOBSEEKER_NOTIFICATION_DELETE = "/api/v2/notification/user/{notificationid}";
    String BUSINESS_NOTIFICATION_DELETE = "/api/v2/notification/org/{notificationid}";
    String SEND_NOTIFICATION_JOBSEEKER = "/api/v2/notification/user/send/notif/{userid}";
    String SEND_NOTIFICATION_ORGANISATION = "/api/v2/notification/org/send/notif/{userid}";

    //payment
    String ADD_USER_CUSTOMER_ID = "/api/v2/payment/add/user/{cusid}";
    String ADD_ORG_CUSTOMER_ID = "/api/v2/payment/add/org/{cusid}";
    String ADD_USER_PAYMENT_RECEIPT = "/api/v2/payment/add/user";
    String ADD_ORGANISATION_PAYMENT_RECEIPT = "/api/v2/payment/add/org";
    String USER_PAYMENTS_LIST = "/api/v2/payment/user/list";
    String ORG_PAYMENTS_LIST = "/api/v2/payment/org/list";
    String ORG_RECOMMENDATION = "/api/v2/org/own/recommend/list";


    String BUSINESS_FOLLOWER = "/api/v2/follow/by/org/user/list";
    String ADD_RECRUITER = "/api/v2/org/add/recruiter";
    String DELETE_RECRUITER = "/api/v2/org/del/acc/recruiter/{id}";
    String VIEW_BUSINESS_PROFILE = "/api/v2/org/view/by/recruiter";

    String JOBSEEKER_CONNECTION_LISTING = "/api/v2/friends/list";
    String JOBSEEKER_VIEWER_LISTING="/api/v2/user/viewer/list";
    String ORGANISATION_PROFILE_BY_JOBSEEKER="/api/v2/org/view/{orgnid}/user";

}
