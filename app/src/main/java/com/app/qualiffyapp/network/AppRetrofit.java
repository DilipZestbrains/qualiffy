package com.app.qualiffyapp.network;

import android.text.TextUtils;
import android.util.Log;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.utils.Utility;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppRetrofit {
    private static AppRetrofit mInstance;
    public ApiServices apiServices;
    private OkHttpClient client;


    private AppRetrofit() {
        // For logging
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Create a trust manager that does not validate certificate chains

        client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(addHeaders())

                .addInterceptor(new ConnectivityAwareUrlClient(ApplicationController.getApplicationInstance()))
                .addInterceptor(new ForbiddenInterceptor())
                .connectTimeout(120000, TimeUnit.SECONDS)
                .readTimeout(120000, TimeUnit.SECONDS)
                .writeTimeout(120000, TimeUnit.SECONDS)
                .build();
        // Rest adapter
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(Urls.BASE_URL_DEVELOPMENT);
//        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        builder.client(client);
        Retrofit retrofit = builder
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiServices = retrofit.create(ApiServices.class);
    }

    /**
     * Method to get retrofit instance
     *
     * @return Instance of retrofit
     */
    public static synchronized AppRetrofit getInstance() {

        if (mInstance == null) {
            synchronized (AppRetrofit.class) {
                if (mInstance == null) {
                    mInstance = new AppRetrofit();
                }
            }
        }
        return mInstance;
    }

    public void cancelAllRequest() {
        client.dispatcher().cancelAll();
    }

    /**
     * Add custom headers
     *
     * @return Header InterceptorAp
     */
    private Interceptor addHeaders() {
        return (Interceptor) chain -> {
            Request.Builder request = chain.request().newBuilder();
            request.addHeader("Authorization", "Basic UXVhTElGWVkjJCVeNjU0MzIxOjlyYVFVQUxMQDMhKSNAZG9uZV4=");
            request.addHeader("Content-Type", "application/json");
            request.addHeader("platform", AppConstants.PLATFORM);
            String token = Utility.getStringSharedPreference(ApplicationController.getApplicationInstance(), AppConstants.LOGIN_ACCESS_TOKEN);

            Log.e("TOKENNNN ",token);
            if (!TextUtils.isEmpty(token)) {
                request.addHeader("accessToken", token);
                Log.d("Access Token", token);
            }


            return chain.proceed(request.build());
        };
    }

    /**
     * Forbidden interceptor to intercept requests and response
     */
    private class ForbiddenInterceptor implements Interceptor {

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
            return response;
        }
    }
}
