package com.app.qualiffyapp.localDatabase.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.app.qualiffyapp.localDatabase.AppDatabase;

import org.jetbrains.annotations.NotNull;

@Entity(tableName = AppDatabase.CONTACT_TABLE_NAME)
public class Contact {
    @NonNull
    @PrimaryKey
    private String pno;
    private String _id;
    private String f_name;
    private String l_name;
    private String img;
    private String username;
    private String is_frnd;

    public Contact(String _id, String f_name, String l_name, String img, @NotNull String pno, String username, String is_frnd) {
        this._id = _id;
        this.f_name = f_name;
        this.l_name = l_name;
        this.img = img;
        this.pno = pno;
        this.username = username;
        this.is_frnd = is_frnd;
    }

    public String getIs_frnd() {
        return is_frnd;
    }

    public void setIs_frnd(String is_frnd) {
        this.is_frnd = is_frnd;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPno() {
        return pno;
    }

    public void setPno(String pno) {
        this.pno = pno;
    }


}
