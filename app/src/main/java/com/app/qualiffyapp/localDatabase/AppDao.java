package com.app.qualiffyapp.localDatabase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.app.qualiffyapp.localDatabase.entity.Contact;

import java.util.List;

@Dao
public interface AppDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertContactList(Contact... contacts);


    @Query("select * from " + AppDatabase.CONTACT_TABLE_NAME)
    LiveData<List<Contact>> fetchContactList();

    @Query("delete from " + AppDatabase.CONTACT_TABLE_NAME)
    void emptyContactList();

    @Query("update " + AppDatabase.CONTACT_TABLE_NAME + " set is_frnd = :isFrnd where _id = :uid")
    void updateContactStatus(String isFrnd, String uid);

    @Query("SELECT COUNT(*) FROM " + AppDatabase.CONTACT_TABLE_NAME)
    int getRowCount();

}
