package com.app.qualiffyapp.localDatabase.repository;

import android.content.Context;
import android.os.AsyncTask;
import androidx.lifecycle.LiveData;
import com.app.qualiffyapp.localDatabase.AppDao;
import com.app.qualiffyapp.localDatabase.AppDatabase;
import com.app.qualiffyapp.localDatabase.entity.Contact;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.contacts.OnContactSyncListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import java.util.ArrayList;
import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_SYNC_CONTACT;

public class AppRepository {
    private static AppRepository repository;
    private static AppDao dao;
    private static SignUpInteractor signUpInteractor;
    private static OnContactSyncListener contactSyncListener;
    private static ApiListener<ArrayList<Contact>> contactSyncApiListener = new ApiListener<ArrayList<Contact>>() {
        @Override
        public void onApiSuccess(ArrayList<Contact> contacts, int apiFlag) {
            if (contacts != null) {
                new InsertContacts(contacts.toArray(new Contact[0])).execute();
            }
        }

        @Override
        public void onApiError(String error, int apiFlag) {

        }

        @Override
        public void onApiNoResponse(int apiFlag) {

        }
    };
    public LiveData<List<Contact>> contacts;

    private AppRepository(Context context) {
        dao = AppDatabase.getAppDatabaseInstance(context).getAppDao();
        signUpInteractor = new SignUpInteractor();
        contacts = dao.fetchContactList();
    }

    public static AppRepository getInstance(Context context) {
        if (repository == null) {
            repository = new AppRepository(context);
        }
        return repository;
    }


    public void insertContactList(ArrayList<Contact> contacts, OnContactSyncListener contactSyncListener) {
        AppRepository.contactSyncListener = contactSyncListener;
        new DeleteContacts(contacts).execute();

    }

    public void deleteContacts() {
        new DeleteContacts(null).execute();
    }

    public void updateContactStatus(String is_friend, String id) {
        new UpdateContactStatus(id, is_friend).execute();
    }


    static class InsertContacts extends AsyncTask<Void, Void, Void> {
        private Contact[] contacts;

        InsertContacts(Contact[] contacts) {
            this.contacts = contacts;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.insertContactList(contacts);
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (contactSyncListener != null)
                contactSyncListener.onContactSync();
        }
    }

    static class DeleteContacts extends AsyncTask<Void, Void, Void> {
        private ArrayList<Contact> contacts;

        DeleteContacts(ArrayList<Contact> contacts) {
            this.contacts = contacts;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.emptyContactList();
            return null;
        }

        @Override
        protected void onPostExecute(Void voids) {
            if (contacts != null)
                signUpInteractor.getSyncedContactList(contactSyncApiListener, contacts, API_FLAG_SYNC_CONTACT);
            else {
                if (contactSyncListener != null)
                    contactSyncListener.onContactSync();
            }
        }
    }

    static class UpdateContactStatus extends AsyncTask<Void, Void, Void> {
        private String uid;
        private String is_frnd;

        UpdateContactStatus(String uid, String is_frnd) {
            this.is_frnd = is_frnd;
            this.uid = uid;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            dao.updateContactStatus(is_frnd, uid);
            return null;
        }
    }


}
