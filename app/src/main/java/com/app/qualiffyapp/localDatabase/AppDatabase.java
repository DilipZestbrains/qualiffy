package com.app.qualiffyapp.localDatabase;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.app.qualiffyapp.localDatabase.entity.Contact;

@Database(entities = {Contact.class}, version = AppDatabase.DATABASE_VERSION, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public static final String CONTACT_DATABASE_NAME = "contactDatabase";
    public static final String CONTACT_TABLE_NAME = "contactTable";
    public static final int DATABASE_VERSION = 6;

    private static AppDatabase database;

    public static AppDatabase getAppDatabaseInstance(Context context) {
        if (database == null) {
            database = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, CONTACT_DATABASE_NAME).build();
            return database;
        } else return database;

    }

    public abstract AppDao getAppDao();
}
