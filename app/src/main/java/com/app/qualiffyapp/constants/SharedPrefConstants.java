package com.app.qualiffyapp.constants;

public interface SharedPrefConstants {
    String ACCESS_TOKEN = "acsTkn";
    String JOB_SEEKER_LOGGED_IN = "job_seeker_logged_in";
    String SMALL_BUSINESS_LOGGED_IN = "small_business_logged_in";
    String LARGE_BUSINESS_LOGGED_IN = "large_business_logged_in";
    String FIRST_NAME = "fname";
    String LAST_NAME = "lname";
    String PHONE = "phone";
    String USER_SOCIAL_TYPE = "socialType";

    String PROFILE_PIC = "profile_pic";
    String UID = "userId";
    String HAS_VIDEO = "has_video";
    String EXPERIENCE = "experience";
}
