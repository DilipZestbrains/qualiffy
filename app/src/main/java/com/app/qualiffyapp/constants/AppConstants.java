package com.app.qualiffyapp.constants;


import android.Manifest;

public class AppConstants {


    public static  final String PHOTO_EDITOR_ACTIVITY="com.sandrios.sandriosCamera.internal.ui.preview.PhotoEditorActivity";
    public static  final String VIDEO_EDITOR_ACTIVITY="com.sandrios.sandriosCamera.internal.ui.preview.VideoEditorActivity";
    public static final String PLATFORM = "2";
    public static final String DATA_TO_SEND = "dataToSend";
    public static final String COMPANY_STRENGTH = "companyStrength";
    public static final String SERVER_DATE_FORMAT = "MM/dd/yyyy hh:mm:ss a";
    public static final String SERVER_DATE_FORMAT_HOME_DATE = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT4 = "MMM-dd-yyyy";
    public static final String MONTH_YEAR_DATE_FORMAT = "MMM yyyy";
    public static final String SERVER_DATE_FORMAT1 = "hh:mm a";
    public static final String SERVER_JOB_DATE_FORMAT = "yyyyMMdd";
    public static final String FROM = "from";
    public static final String IS_APP_VISIBLE = "activity_visible_or_not";
    public static final String LOGIN_ACCESS_TOKEN = "login_access_token";

    public static final String VIDEO_FOLDER = "com.dev.qualiffy";
    public static final String TEMP_MEDIA_FOLDER = "qualiffy";
    public static final String MONEY_TO_PAY = "money_to_pay";
    public static final String SUBSCRIPTION_FLAG = "subscriptionFlag";

    public final static int CHAT_RIGHT_ALIGNED = 1;
    public final static int CHAT_LEFT_ALIGNED = 2;
    public final static String TRUE = "1";
    public final static String FALSE = "0";

    public final static String APP_SESSION = "_session";

    public final static String DEVICE_TYPE = "1";
    public static final String PREFS_ACCESS_TOKEN = "_fcm_token";
    public static final String PROMOTION_EDIT_DATA = "promotion_data";
    public static final String IS_BUSINESS_UPDATE = "isBusinessUpdate";
    public static final String IS_JOBSEEKER_UPDATE = "isJobSeekerUpdate";
    public static final String IMAGE_BASE_URL = "imgBaseUrl";
    public static final String IMAGES = "images";
    //Job Seeker Drawer Items
    public static final int JS_POS_HOME = 0;
    public static final int JS_POS_MY_ACCOUNT = 1;
    public static final int JS_POS_CHAT = 2;
    public static final int JS_POS_NOTIFICATIONS = 4;
    public static final int JS_POS_SUBSCRIPTION = 5;
    public static final int JS_APPLIED_JOB = 3;
    //Business Drawer Item
    public static final int BS_POS_HOME = 0;
    public static final int BS_POS_MY_ACCOUNT = 1;
    public static final int BS_POS_CHAT = 2;
    public static final int BS_POS_NOTIFICATIONS = 3;
    public static final int BS_POS_SUBSCRIBE = 4;
    public static final int BS_POS_ADD_JOB = 5;
    public static final int BS_POS_ADD_ADDED_JOB = 6;
    public static final int BS_POS_PROMOTE = 7;
    public static final int BS_POS_RECRUITER = 8;

    public static final String POSITION = "position";
    //Permissions
    public final static String[] AUDIO_CALL_PERMISSION = {Manifest.permission.RECORD_AUDIO};
    public final static String[] AUDIO_PERMISSION = {Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public final static String[] CAMERA_PERMISION = {Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO};
    public final static String[] CAMERA_VIDEO_PERMISION = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public final static String[] GALLERY_PERMISION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public final static String[] LOCATION_PERMISION = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    public final static String[] VIDEO_CALL_PERMISSION = {Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA};
    public final static String[] READ_WRITE_PERMISSION = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    //PERMISSION REQUEST CODES
    public final static int CAMERA_STORAGE_PERMISSION_REQUEST_CODE = 1001;
    public final static int GALLERY_STORAGE_PERMISSION_REQUEST_CODE = 1002;
    public final static int VIDEO_CAMERA_PERMISSION_REQUEST_CODE = 1003;
    public final static int LOCATION_PERMISSION_REQUEST_CODE = 1004;
    public static final int IMAGE_PICKER_SELECT = 1005;
    public static final int AUDIO_RECORD_REQUEST_CODE = 1007;
    public static final int VIDEO_CALL_REQUEST_CODE = 1008;
    public static final int READ_WRITE_REQUEST_CODE = 1009;
    public static final int AUDIO_CALL_PERMISSION_REQUEST_CODE = 1010;
    public final static int IMAGE_DELETE = 0;
    public final static String HELP = "Help";
    public final static String FAQ = "FAQ";
    public final static String GOOGLE_PLACES_API_KEY = "AIzaSyBOBOxfOlmfIrKqRXUlDLcnUnOGPCcGZR0";
    public final static String LOCALITY = "locality";
    public final static String COUNTRY = "country";
    //socialIds
    public final static int FACEBOOK_TYPE = 2;
    public final static int LINKEDIN_TYPE = 1;
    public final static int SNAPCHAT_TYPE = 5;
    public final static String FACEBOOK = "facebook";
    public final static String FACEBOOK_PROFILE_ID = "fbProfileId";
    public final static String LINKEDIN = "linkedIn";
    public final static String SNAPCHAT = "snapchat";
    public final static String NORMAL = "normal";
    public static final String BUNDLE = "bundle";
    public static final String HEADER = "header";
    public static final String FRAGMENT = "fragment";
    public static final String ADDRESS = "address";
    public static final String NEED_UPDATE = "needUpdate";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final int SUCCESS = 1;
    public static final int FAILURE = 2;
    //Profiletoggles
    public static final int RELOCATE = 21;
    public static final String CALL_INTENT = "call_intent";
    public static final String CALL_INTENT_UNREGISTER = "call_intent_unregister";

    //USER TYPE
    public static final int JOB_SEEKER = 1;
    public static final int BUSINESS_USER = 4;
    public static final String USER_TYPE = "user_type";
    public static final String ORG_TYPE = "org_type";
    public static final String TUTORIAL_SCREEN_SHOW = "tutorial_show";
    public static final String CONNECTION = "connection";
    public static final String CONNECTION_ID = "connection_id";
    public static final String CONNECTION_NAME = "connection_name";
    public static final String IS_BLOCKED = "isBlocked";
    public static final String CONNECTION_TYPE = "connectionType";
    public static final String USER = "user";

    public static final int ADDRESS_UPDATE_REQUEST_CODE = 101;
    public static final int DESCRIPTION_UPDATE_REQUEST_CODE = 103;
    public static final int COMPANY_NAME_UPDATE_REQUEST_CODE = 104;
    public static final int NOTIFICATION_REQUEST_CODE = 105;
    public static final int PRIVACY_REQUEST_CODE = 106;
    public static final int REQUEST_CODE_UPDATE_VIDEO = 127;
    public static final int REQUEST_CODE_SETTING = 108;
    public static final int REQUEST_CODE_JS_INDUSTRY = 109;
    public static final int REQUEST_CODE_JS_ROLE = 110;
    public static final int REQUEST_CODE_JS_ROLESEEKING = 111;
    public static final int REQUEST_CODE_PROFILE_PIC = 112;
    public static final int REQUEST_CODE_ADD_EXPERIENCE = 113;
    public static final int REQUEST_CODE_ADD_CERTIFICATE = 114;
    public static final int REQUEST_CODE_SKILLS = 115;
    public static final int REQUEST_CODE_LANGS = 116;
    public static final int REQUEST_CODE_JOB_TYPE = 117;
    public static final int REQUEST_CODE_ADD_EXPERIENCE_DETAIL = 118;
    public static final int REQUEST_CODE_CAMERA_IMG = 201;
    public static final int REQUEST_CODE_GALLERY_IMG = 202;
    public static final int REQUEST_CODE_CAMERA_VIDEO = 301;
    public static final int REQUEST_CODE_FACEBOOK_MEDIA = 302;
    public static final int REQUEST_CODE_PROMOTION = 303;
    public static final int REQUEST_CODE_FILTER_IMAGE = 304;
    public static final int REQUEST_CODE_FILTER_VIDEO = 305;
    public static final String THUMBNAIL = "videoThumnail";
    public static final String JS_INDUSTRY_ = "indus";
    public static final String JS_ROLE = "role";
    public static final String JS_ROLESEEKING = "role_seeking";
    public static final String JS_INDUS_ID = "indus_id";
    public static final String JS_PROFILE_PIC = "profile";
    public static final String JS_SKILLS = "skills";
    public static final String JS_LANGS = "langs";
    public static final String JS_JOB_TYPE = "jobtype";
    public static final String ERROR_CODE = "errorCode";
    public static final String IS_BUSINESS_CLEAR = "isBusinessClear";
    public static final String ORG_ID = "orgId";
    public static final String ORG_NAME = "orgName";
    public static final String ORG_BIO = "orgBio";
    public static final String ORG_IMG = "orgImg";
    public static final int CONTACT_INVITE = 101;
    public static final int FRND = 102;
    public static final int ACCEPT_OR_REJECT = 103;
    public static final int SEND_FRIEND_REQUEST = 104;
    public static final int FRND_DETAIL = 105;
    public static final String IMAGE_VIEW_DATA = "view_media_data";
    public static final String RECOMMEND = "recommend";
    public static final String JOB_ID = "jobId";
    public static final String DESC = "desc";
    public static final String USER_ID = "userId";
    public static final int GPS_REQUEST = 1001;
    public static int IS_UPLOAD_VIDEO = 0;
    public static String IS_SMALL_ORG = "isSmallCompany";
    public static String MEDIA_PATH = "mediaPath";
    public static String MEDIA_TYPE = "media_type";
    public static String MEDIA_ID = "media_id";
    public static String IS_SHORTLIST = "is_shorlist";
    public static String IS_SQUARE_IMG = "is_square_img";


    public static int USR_FOLLOW_ORG = 1;
    public static int ORG_FOLLOW_USR = 2;
    public static int ORG_VIEW_USR = 3;
    public static int ORG_SHORT_USR = 4;
    public static int USR_REC_ORG = 5;
    public static int USR_CHAT = 6;
    public static int USR_CALLS = 7;
    public static int ORG_CHAT = 8;
    public static int ORG_CALLS = 9;
    public static int FRIEND_REQUEST_SEND = 10;
    public static int FRIRND_REQUEST_ACCEPT = 11;
    public static int JOB_SEEKER_DELETED = 7;
    public static int ORGANISATION_DELETED = 41;
    public static String CURRENT_SUBSCRIPTION_ID = "currentSubID";


    public static final int JS_RECOMMANDATION = 1001;
    public static final int ORG_DASHBOARD = 1002;
    public static final int JS_FOLLOWING = 1003;
    public static final int JS_VIEWER = 1004;


    //payment

    public static String CUSTOMER_ID = "customerId";

    public static final int TO_FILTER = 1230;


}
