package com.app.qualiffyapp.constants;

public enum CompanyPortfolioType {
    FAQ("faq"),
    TOC("toc"),
    HELP("help");
    public String value;

    CompanyPortfolioType(String value) {
        this.value = value;
    }
}
