package com.app.qualiffyapp;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class SpinnerAdapter extends BaseAdapter {
    ArrayList<String> list;
    Context context;

    public SpinnerAdapter(Context context, ArrayList<String> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.item_spinner_layout, null);
        TextView textView = (TextView) view.findViewById(R.id.spinnerTxt);
        textView.setText(list.get(position));

    /*    if (position == list.size() - 1)
            dividerView.setVisibility(View.GONE);
        else dividerView.setVisibility(View.VISIBLE);*/


        return textView;
    }

  /*  @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;
        view =  View.inflate(context, R.layout.item_spinner, null);
        TextView textView = (TextView) view.findViewById(R.id.spinnerTxt);
        textView.setText(list.get(position));
        return view;
    }*/
}
