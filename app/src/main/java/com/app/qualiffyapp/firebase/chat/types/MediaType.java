package com.app.qualiffyapp.firebase.chat.types;

public enum MediaType {
    IMAGE("0"),
    VIDEO("1"),
    VOICE("2"),
    PDF("3"),
    TEXT("4"),
    CALL("5");
    public final String value;

    MediaType(String value) {
        this.value = value;
    }

}
