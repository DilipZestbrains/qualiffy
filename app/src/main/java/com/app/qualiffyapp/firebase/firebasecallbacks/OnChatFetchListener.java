package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.Chat;

import java.util.ArrayList;

public interface OnChatFetchListener {
    void onChatFetch(ArrayList<Chat> chats);
}
