package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.Chat;

public interface ChatCallbacks {
    void onPdfClick(String url);

    void onVideoClick(String url);

    void onImageClick(String url);

    void onDeleteModeOn(boolean isModeOn);

    void updateToolbarTitle(String title);

    void updateLastMessage(Chat lastMessage);


}
