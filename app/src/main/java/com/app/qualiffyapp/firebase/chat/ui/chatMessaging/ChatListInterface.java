package com.app.qualiffyapp.firebase.chat.ui.chatMessaging;

import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.firebase.chat.ui.notification.ChatNotificationInterface;

import java.util.ArrayList;

public interface ChatListInterface extends ChatNotificationInterface {
    void preUpload();

    void onMediaUpload(String msg, String baseUrl, String imgUrl, String mediaType);

    void onMediaDeleted(String msg, int status, ArrayList<Chat> chats);
}
