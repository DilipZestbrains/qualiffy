package com.app.qualiffyapp.firebase.chat.ui.notification;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_SEND;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;

public class ChatNotificationPresenter {
    private ChatNotificationInterface chatNotificationInterface;
    private SignUpInteractor interactor;
    private ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            if (response != null)
                chatNotificationInterface.onNotificationSend(SUCCESS, response.msg);
            else chatNotificationInterface.onNotificationSend(SUCCESS, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            chatNotificationInterface.onNotificationSend(FAILURE, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            chatNotificationInterface.onNotificationSend(FAILURE, null);
        }
    };

    public ChatNotificationPresenter(ChatNotificationInterface chatNotificationInterface) {
        this.chatNotificationInterface = chatNotificationInterface;
        interactor = new SignUpInteractor();
    }

    public void sendNotification(RequestBean bean, String connectionId, String senderType) {
        int userType = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), USER_TYPE);
        if (connectionId != null) {
            interactor.sendNotification(msgApiListener, userType, senderType, connectionId, bean, API_FLAG_NOTIFICATION_SEND);
        } else {
            chatNotificationInterface.onNotificationSend(FAILURE, null);
        }

    }
}
