package com.app.qualiffyapp.firebase.chat.types;

public enum FirebaseUserType {
    JOB_SEEKER("0"),
    BUSINESS("1");
    public String s;

    FirebaseUserType(String s) {
        this.s = s;
    }

}
