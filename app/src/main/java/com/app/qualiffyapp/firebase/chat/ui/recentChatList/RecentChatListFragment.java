package com.app.qualiffyapp.firebase.chat.ui.recentChatList;

import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.firebase.chat.adapter.recentChatList.RecentChatListAdapter;
import com.app.qualiffyapp.base.BaseFragment;
import com.app.qualiffyapp.databinding.FragmentRecentChatsListBinding;
import com.app.qualiffyapp.firebase.Call;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;

public class RecentChatListFragment extends BaseFragment implements RecentChatListAdapter.OnDataChangedListener, RecentChatListAdapter.OnChatItemClickListener {
    private FragmentRecentChatsListBinding mBinding;
    private RecentChatListAdapter connectionAdapter;
    private AppFirebaseDatabase appFirebaseDatabase;
    private String uid;

    @Override
    protected void initUi() {
        mBinding = (FragmentRecentChatsListBinding) viewDataBinding;
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
        onRecyclerNewMatchesAdapterSet();

    }

    @Override
    protected int getLayoutById() {
        return R.layout.fragment_recent_chats_list;
    }

    private void onRecyclerNewMatchesAdapterSet() {
        mBinding.swipeToRefresh.setRefreshing(true);
        LinearLayoutManager connectionLayoutManager = new LinearLayoutManager(context);
        connectionLayoutManager.setReverseLayout(true);
        connectionLayoutManager.setStackFromEnd(true);
        mBinding.rvNewMatches.setLayoutManager(connectionLayoutManager);
        uid = Utility.getStringSharedPreference(getContext(), UID);
        connectionAdapter = new RecentChatListAdapter(appFirebaseDatabase.getConnectionFirebaseOption(uid), this);
        mBinding.rvNewMatches.setAdapter(connectionAdapter);
        connectionAdapter.setDataChangedListener(this);
        connectionAdapter.startListening();
    }



    @Override
    protected void setListener() {
        mBinding.swipeToRefresh.setOnRefreshListener(() -> {
            mBinding.swipeToRefresh.setRefreshing(true);
            connectionAdapter.stopListening();
            connectionAdapter.startListening();
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (connectionAdapter != null)
            connectionAdapter.stopListening();
    }

    public void searchChats(String newText) {
        mBinding.swipeToRefresh.setRefreshing(true);
        connectionAdapter.stopListening();
        connectionAdapter = new RecentChatListAdapter(appFirebaseDatabase.getConnectionSearchFirebaseOption(uid, newText), this);
        connectionAdapter.setDataChangedListener(this);
        mBinding.rvNewMatches.setAdapter(connectionAdapter);
        connectionAdapter.startListening();
    }

    public void onCancelSearch() {
        mBinding.swipeToRefresh.setRefreshing(true);
        connectionAdapter.stopListening();
        connectionAdapter = new RecentChatListAdapter(appFirebaseDatabase.getConnectionFirebaseOption(uid), this);
        connectionAdapter.setDataChangedListener(this);
        mBinding.rvNewMatches.setAdapter(connectionAdapter);
        connectionAdapter.startListening();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search:
                break;
            case R.id.iv_back_toolbar:
                break;
        }
    }

    @Override
    public void afterDataLoad(int count) {
        if (count <= 0)
            mBinding.tvNoChatFound.setVisibility(View.VISIBLE);
        else mBinding.tvNoChatFound.setVisibility(View.GONE);
        mBinding.swipeToRefresh.setRefreshing(false);
    }

    @Override
    public void onChatClick(UserConnection connection) {
        Utility.startChatActivity(getContext(), connection);
    }

    @Override
    public void onVideoClick(UserConnection connection) {
        Call.getInstance(this::onNotificationSend).makeVideoCall(getContext(), connection);
    }

    @Override
    public void onVoiceClick(UserConnection connection) {
        Call.getInstance(this::onNotificationSend).makeAudioCall(getContext(), connection);
    }


    private void onNotificationSend(int status, String msg) {
        if (status == FAILURE)
            Utility.showToast(getContext(), msg);
    }
}
