package com.app.qualiffyapp.firebase.chat.ui.chatMessaging;

import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.chat.ui.notification.ChatNotificationPresenter;
import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.models.createProfile.AddMediaResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.signUp.SignUpInteractor;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_DELETE_CHAT_MEDIA;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_NOTIFICATION_SEND;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_UPLOAD_CHAT_MEDIA;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class ChatListPresenter extends ChatNotificationPresenter {
    private final ChatListInterface chatListInterface;
    private final SignUpInteractor interactor;
    private ArrayList<Chat> chats;

    private ApiListener<MsgResponseModel> msgApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            if (apiFlag == API_FLAG_DELETE_CHAT_MEDIA) {
                if (response != null)
                    chatListInterface.onMediaDeleted(response.msg, SUCCESS, chats);
                else chatListInterface.onMediaDeleted(null, SUCCESS, chats);
            } else if (apiFlag == API_FLAG_NOTIFICATION_SEND) {
                if (response != null)
                    chatListInterface.onNotificationSend(SUCCESS, response.msg);
                else chatListInterface.onNotificationSend(SUCCESS, null);
            }

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            if (apiFlag == API_FLAG_DELETE_CHAT_MEDIA)
                chatListInterface.onMediaDeleted(error, FAILURE, chats);
            else if (apiFlag == API_FLAG_NOTIFICATION_SEND) {
                chatListInterface.onNotificationSend(FAILURE, error);
            }
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            if (apiFlag == API_FLAG_DELETE_CHAT_MEDIA)
                chatListInterface.onMediaDeleted(null, FAILURE, chats);
            else if (apiFlag == API_FLAG_NOTIFICATION_SEND)
                chatListInterface.onNotificationSend(FAILURE, null);
        }
    };
    private ApiListener<AddMediaResponseModel> addMediaApiListener = new ApiListener<AddMediaResponseModel>() {
        @Override
        public void onApiSuccess(AddMediaResponseModel response, int apiFlag) {
            chatListInterface.onMediaUpload(response.msg, response.bP, response.img_id, response.uni);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            chatListInterface.onMediaUpload(error, null, null, null);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            chatListInterface.onMediaUpload(null, null, null, null);
        }
    };

    public ChatListPresenter(ChatListInterface chatListInterface) {
        super(chatListInterface);
        this.chatListInterface = chatListInterface;
        interactor = new SignUpInteractor();
    }

    public void uploadMedia(String filePath, MediaType mediaType, FirebaseUserType type) {
        chatListInterface.preUpload();
        switch (type) {
            case BUSINESS:
                interactor.uploadOrgChatMedia(filePath, mediaType.value, addMediaApiListener, API_FLAG_UPLOAD_CHAT_MEDIA);
                break;
            case JOB_SEEKER:
                interactor.uploadChatMedia(filePath, mediaType.value, addMediaApiListener, API_FLAG_UPLOAD_CHAT_MEDIA);
                break;
        }
    }


    public void deleteChaMedia(ArrayList<Chat> chats, FirebaseUserType userType) {
        this.chats = chats;
        switch (userType) {
            case BUSINESS:
                interactor.deleteOrgChatMedia(msgApiListener, chats, API_FLAG_DELETE_CHAT_MEDIA);
                break;
            case JOB_SEEKER:
                interactor.deleteChatMedia(msgApiListener, chats, API_FLAG_DELETE_CHAT_MEDIA);
                break;
        }

    }


}
