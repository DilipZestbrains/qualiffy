package com.app.qualiffyapp.firebase.model;

public class UserConnection {
    private String uid;
    private String name;
    private String type;
    private String phone;
    private String media_type;
    private String media_url;
    private String timestamp;
    private String img;
    private String unread_count;
    private String blocked;
    private String nameLower;

    public UserConnection(String uid, String name, String type, String phone, String media_type, String media_url, String timestamp, String img, String unread_count, String blocked, String nameLower) {
        this.nameLower = nameLower;
        this.uid = uid;
        this.name = name;
        this.type = type;
        this.phone = phone;
        this.media_type = media_type;
        this.media_url = media_url;
        this.timestamp = timestamp;
        this.img = img;
        this.unread_count = unread_count;
        this.blocked = blocked;
    }

    public UserConnection() {

    }

    public String getNameLower() {
        return nameLower;
    }

    public void setNameLower(String nameLower) {
        this.nameLower = nameLower;
    }

    public String getBlocked() {
        return blocked;
    }

    public void setBlocked(String blocked) {
        this.blocked = blocked;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUnread_count() {
        return unread_count;
    }

    public void setUnread_count(String unread_count) {
        this.unread_count = unread_count;
    }

}
