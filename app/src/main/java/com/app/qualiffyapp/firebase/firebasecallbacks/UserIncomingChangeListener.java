package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.firebase.model.UserConnection;

public interface UserIncomingChangeListener {
    void onIncomingCallListener(User user, UserConnection userConnection);
}
