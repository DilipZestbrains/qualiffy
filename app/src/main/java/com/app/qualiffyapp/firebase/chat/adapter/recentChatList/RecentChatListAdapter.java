package com.app.qualiffyapp.firebase.chat.adapter.recentChatList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemChatNewMatchesBinding;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.TimeUtil;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;


public class RecentChatListAdapter extends FirebaseRecyclerAdapter<UserConnection, RecentChatListAdapter.RecentChatListViewHolder> {
    private OnDataChangedListener dataChangedListener;
    private OnChatItemClickListener listener;

    public RecentChatListAdapter(@NonNull FirebaseRecyclerOptions<UserConnection> options, OnChatItemClickListener listener) {
        super(options);
        this.listener = listener;
    }

    public void setDataChangedListener(OnDataChangedListener dataChangedListener) {
        this.dataChangedListener = dataChangedListener;
    }

    @NonNull
    @Override
    public RecentChatListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemChatNewMatchesBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_chat_new_matches, parent, false);
        return new RecentChatListViewHolder(mBinding);
    }

    @Override
    protected void onBindViewHolder(@NonNull RecentChatListViewHolder recentChatListViewHolder, int i, @NonNull UserConnection connection) {
        recentChatListViewHolder.bind(connection);
    }

    @Override
    public void onDataChanged() {
        super.onDataChanged();
        if (dataChangedListener != null)
            dataChangedListener.afterDataLoad(getItemCount());
    }

    private boolean showCount(String count1) {
        try {
            int count = Integer.parseInt(count1);
            return count > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public interface OnDataChangedListener {
        void afterDataLoad(int size);
    }

    public interface OnChatItemClickListener {
        void onChatClick(UserConnection connection);

        void onVideoClick(UserConnection connection);

        void onVoiceClick(UserConnection connection);
    }

    class RecentChatListViewHolder extends RecyclerView.ViewHolder {
        ItemChatNewMatchesBinding binding;

        RecentChatListViewHolder(ItemChatNewMatchesBinding binding) {
            super(binding.getRoot());
            this.binding = binding;

        }

        void bind(UserConnection connection) {
            AppFirebaseDatabase.getInstance().updateConnection(connection);
            if (connection != null) {
                if (connection.getImg() != null) {
                    GlideUtil.loadCircularImage(binding.ivProfile, connection.getImg(), R.drawable.profile_placeholder);
                } else binding.ivProfile.setImageResource(R.drawable.profile_placeholder);
                if (connection.getName() != null) {
                    binding.tvUsername.setText(connection.getName());
                } else binding.tvUsername.setText("");
                if (connection.getPhone() != null) {
                    binding.tvPhoneNumber.setVisibility(View.VISIBLE);
                    binding.tvPhoneNumber.setText(connection.getPhone());
                } else binding.tvPhoneNumber.setVisibility(View.GONE);
                if (connection.getUnread_count() != null) {
                    if (showCount(connection.getUnread_count())) {
                        binding.tvUnreadCount.setVisibility(View.VISIBLE);
                        binding.tvUnreadCount.setText(connection.getUnread_count());
                    } else {
                        binding.tvUnreadCount.setVisibility(View.GONE);
                    }

                } else binding.tvUnreadCount.setVisibility(View.GONE);

                if (connection.getTimestamp() != null) {
                    binding.tvDateTime.setVisibility(View.VISIBLE);
                    binding.tvDateTime.setText(TimeUtil.getDateFromTimestamp(connection.getTimestamp()));
                } else binding.tvDateTime.setVisibility(View.GONE);
                if (connection.getMedia_url() != null) {
                    binding.tvMessageDescription.setVisibility(View.VISIBLE);
                    binding.tvMessageDescription.setText(connection.getMedia_url());
                } else binding.tvMessageDescription.setVisibility(View.GONE);


            }
            binding.mcvChatCard.setOnClickListener(v -> {
                if (connection != null && listener != null)
                    listener.onChatClick(connection);
            });
            binding.ibVoiceCall.setOnClickListener(v -> {
                if (connection != null && listener != null)
                    listener.onVoiceClick(connection);
            });
            binding.ibVideoCall.setOnClickListener(v -> {
                if (connection != null && listener != null)
                    listener.onVideoClick(connection);
            });

        }

    }

}
