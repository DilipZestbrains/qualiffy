package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.UserConnection;

public interface OnConnectionListChangeListener {
    void onConnectionUpdate(UserConnection connection);
}
