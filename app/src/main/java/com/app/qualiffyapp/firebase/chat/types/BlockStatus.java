package com.app.qualiffyapp.firebase.chat.types;

public enum BlockStatus {
    NOT_BLOCKED("0"), BLOCKED("1");

    public String value;

    BlockStatus(String value) {
        this.value = value;
    }

}
