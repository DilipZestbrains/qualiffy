package com.app.qualiffyapp.firebase.chat.types;

public enum Visible {
    VISIBLE("0"),
    INVISIBLE("1");

    String value;

    Visible(String value) {
        this.value = value;
    }
}
