package com.app.qualiffyapp.firebase.chat.types;

public enum CallType {
    AUDIO("0"), VIDEO("1");
    public String value;

    CallType(String value) {
        this.value = value;
    }
}
