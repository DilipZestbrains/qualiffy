package com.app.qualiffyapp.firebase.chat.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.BottomSheetChatAttachmentBinding;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.imagevideoeditor.videoEditor.VideoEditorView;
import com.app.qualiffyapp.firebase.chat.ui.chatMessaging.ChatListActivity;
import com.app.qualiffyapp.utils.IntentUtil;
import com.app.qualiffyapp.utils.Utility;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import static com.app.qualiffyapp.constants.AppConstants.IMAGE_PICKER_SELECT;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_IMAGE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_VIDEO;
import static com.app.qualiffyapp.utils.IntentUtil.getGalleryIntent;

public class ChatAttachmentBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetChatAttachmentBinding mBinding;
    private ChatListActivity chatListActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_chat_attachment, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        initView();
    }

    private void initView() {
        try {
            chatListActivity = (ChatListActivity) getActivity();
        } catch (Exception e) {
            Log.e("Chat", e.getMessage());
        }
    }

    private void setListeners() {
        mBinding.ivGallery.setOnClickListener(this);
        mBinding.ivDocument.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivGallery:
                if (getContext() != null) {
                    startActivityForResult(getGalleryIntent(getContext()), IMAGE_PICKER_SELECT);
                }
                break;
            case R.id.ivDocument:
                if (getActivity() != null)
                    IntentUtil.openDocumentPicker(getActivity(), uri -> {
                        String path = IntentUtil.saveFileFromUri(uri, getActivity());
                        if (path != null) {
                            chatListActivity.getPresenter().uploadMedia(path, MediaType.PDF, chatListActivity.getUserType());
                            dismiss();
                        }
                    });
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_PICKER_SELECT) {
            if (data != null && data.getData() != null) {
                String type = "";
                if (chatListActivity != null)
                    type = chatListActivity.getContentResolver().getType(data.getData());
                if (type != null) {
                    if (type.contains("video")) {
                        if (getActivity() != null) {
                            openVideoEditor( IntentUtil.getInstance().getUriRealPath(getActivity(), data.getData()));
                        }
                    } else if (type.contains("image")) {
                        if (getActivity() != null)
                            openCameraActivity(IntentUtil.getInstance().getUriRealPath(getActivity(), data.getData()));
                    }
                } else {
                    Utility.showToast(getContext(), "Something went wrong!");
                    dismiss();
                }
            }
            return;

        }
        if (requestCode == REQUEST_CODE_FILTER_IMAGE) {
            if (data != null && data.getStringExtra(MEDIA_PATH) != null && chatListActivity != null) {
                chatListActivity.getPresenter().uploadMedia(data.getStringExtra(MEDIA_PATH), MediaType.IMAGE, chatListActivity.getUserType());
                dismiss();
            }
        }
        if (requestCode == REQUEST_CODE_FILTER_VIDEO) {
            if (data != null && data.getStringExtra(MEDIA_PATH) != null && chatListActivity != null) {
                chatListActivity.getPresenter().uploadMedia(data.getStringExtra(MEDIA_PATH), MediaType.VIDEO, chatListActivity.getUserType());
                dismiss();
            }
        }


    }

    private void openVideoEditor(String videoPath) {
        Intent i = new Intent(getContext(), VideoEditorView.class);
        i.putExtra("videoPath", videoPath);
        startActivityForResult(i, REQUEST_CODE_FILTER_VIDEO);
    }


    private void openCameraActivity(String imgPath) {
        Intent i = new Intent(getContext(), PhotoEditorActivity.class);
        i.putExtra(MEDIA_TYPE, 1);
        i.putExtra(MEDIA_PATH, imgPath);
        startActivityForResult(i, REQUEST_CODE_FILTER_IMAGE);
    }

}
