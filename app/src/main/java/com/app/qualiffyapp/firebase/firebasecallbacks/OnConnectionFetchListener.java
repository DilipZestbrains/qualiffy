package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.UserConnection;

import java.util.ArrayList;

public interface OnConnectionFetchListener {
    void onConnectionFetch(ArrayList<UserConnection> connections);
}
