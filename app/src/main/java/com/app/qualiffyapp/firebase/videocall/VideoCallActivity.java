package com.app.qualiffyapp.firebase.videocall;

import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.databinding.ActivityVideoCallBinding;
import com.app.qualiffyapp.firebase.MyFireBaseMessagingService;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnIncomingChangeListener;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.TimeUtil;
import com.google.gson.Gson;

import java.util.List;

import io.agora.rtc.Constants;
import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;
import io.agora.rtc.video.VideoCanvas;
import io.agora.rtc.video.VideoEncoderConfiguration;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION;
import static com.app.qualiffyapp.constants.AppConstants.USER;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CALL_PERMISSION;
import static com.app.qualiffyapp.constants.AppConstants.VIDEO_CALL_REQUEST_CODE;
import static com.app.qualiffyapp.firebase.MyFireBaseMessagingService.CALL_NOTIFICATION_ID;

public class VideoCallActivity extends BaseActivity
        implements OnIncomingChangeListener {
    private final String LOG_TAG = VideoCallActivity.class.getName();
    private ActivityVideoCallBinding mBinding;
    private String channel;
    private AppFirebaseDatabase firebaseDatabase;
    private RtcEngine mRtcEngine;
    private User user;
    private UserConnection connection;
    private boolean isFinished = false;
    private Ringtone ringtone;
    private boolean callReceived = false;
    private MediaPlayer mp;
    private CountDownTimer timer = new CountDownTimer(30000, 1000) {
        private boolean isStatusSet = false;

        @Override
        public void onTick(long millisUntilFinished) {
            if (millisUntilFinished < 1000) {
                if (!isStatusSet) {
                    mBinding.tvCallingStatus.setVisibility(View.VISIBLE);
                    mBinding.tvCallingStatus.setText(getResources().getString(R.string.disconnected));
                    isStatusSet = true;
                }
            }
        }

        @Override
        public void onFinish() {
                firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, getResources().getString(R.string.missed_video_call_at) + " " + TimeUtil.getDateFromTimeStamp(String.valueOf(System.currentTimeMillis()), "hh:mm a"), null);
            leaveChannel();
        }

    };
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {
        @Override
        public void onFirstRemoteVideoDecoded(final int uid, int width, int height, int elapsed) {
            super.onFirstRemoteVideoDecoded(uid, width, height, elapsed);
            runOnUiThread(() -> setupRemoteVideoStream(uid));

        }

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            runOnUiThread(() -> preCalling());
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            super.onUserOffline(uid, reason);
            if (callReceived) {
                long duration = SystemClock.elapsedRealtime() - mBinding.chronometerTimer.getBase();
                firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, "Video call ended : " + TimeUtil.getTimeFromMillisecond(duration), String.valueOf(duration));
            } else if (isCaller())
                timer.cancel();

            runOnUiThread(() -> leaveChannel());

        }
    };

    private void preCalling() {
        if (isCaller()) {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
            }
            timer.cancel();
        }
        startTimer();
        enableReceivingMode(false);
        callReceived = true;
    }

    @Override
    protected void initView() {
        mBinding = (ActivityVideoCallBinding) viewDataBinding;
        getIntentData();
    }


    private void getIntentData() {
        if (getIntent().getBundleExtra(BUNDLE) != null) {
            user = new Gson().fromJson(getIntent().getBundleExtra(BUNDLE).getString(USER), User.class);
            connection = new Gson().fromJson(getIntent().getBundleExtra(BUNDLE).getString(CONNECTION), UserConnection.class);
            if (user != null && connection != null) {
                initFirebase();
            } else leaveChannel();

        } else leaveChannel();
        MyFireBaseMessagingService.clearNotification(CALL_NOTIFICATION_ID);

    }

    private void initFirebase() {
        firebaseDatabase = AppFirebaseDatabase.getInstance();
        firebaseDatabase.startListenForIncomingCalls(user.getUid(), this);
        channel = firebaseDatabase.getChatId(user.getUid(), connection.getUid());
        if (user.getIncoming() != null && user.getCaller_id() != null && channel != null)
            initCalling();

    }

    private void initCalling() {
        GlideUtil.loadImage(mBinding.connectionImage, connection.getImg(), R.drawable.photo_placeholder, 1000, 0);
        mBinding.header.toolbarTitleTxt.setText(connection.getName());
        if (onAskForSomePermission(this, VIDEO_CALL_PERMISSION, VIDEO_CALL_REQUEST_CODE)) {
            setUpAgoraEngineAndJoin();
        }

    }

    private void enableReceivingMode(boolean enable) {
        mBinding.ivDisconnectCall.setVisibility(View.VISIBLE);
        if (enable)
            mBinding.ivReceiveCall.setVisibility(View.VISIBLE);
        else mBinding.ivReceiveCall.setVisibility(View.GONE);
    }

    public void setUpAgoraEngineAndJoin() {
        initAgoraEngine();
        setupSession();
        setupLocalVideoFeed();
        if (isCaller()) {
            joinChannel();
            timer.start();
            enableReceivingMode(false);
            startRinging(true);
            mBinding.tvCallingStatus.setText(getResources().getString(R.string.outgoing));
        } else {
            startRinging(false);
            mBinding.tvCallingStatus.setText(getResources().getString(R.string.incoming));
            enableReceivingMode(true);
        }
    }

    private void startRinging(boolean isCaller) {
        if (isCaller) {
            mp = MediaPlayer.create(this, R.raw.tring_tring);
            mp.setVolume(10, 10);
            mp.setLooping(true);
            mp.start();
        } else {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(this, uri);
            ringtone.play();
        }
    }

    private boolean isCaller() {
        return user.getCaller_id().equals(user.getUid());
    }

    private void joinChannel() {
        mRtcEngine.joinChannel(null, channel, "Extra Optional Data", 0);
    }

    private void acceptCall() {
        joinChannel();
        if (ringtone != null) {
            if (ringtone.isPlaying())
                ringtone.stop();
        }
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_video_call;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                leaveChannel();
                break;
            case R.id.ivDisconnectCall:
                if (isCaller() && !callReceived)
                    timer.cancel();
                leaveChannel();
                break;
            case R.id.ivMute:
                onLocalAudioMuteClicked(mBinding.ivMute);
                break;
            case R.id.ivReceiveCall:
                acceptCall();
                break;
            case R.id.ivCallMode:
                changeCallMode(mBinding.ivCallMode);
                break;
            case R.id.ivChangeCamera:
                if (mRtcEngine != null)
                    mRtcEngine.switchCamera();
        }
    }


    @Override
    protected void setListener() {
        mBinding.ivDisconnectCall.setOnClickListener(this);
        mBinding.header.backBtnImg.setOnClickListener(this);
        mBinding.ivCallMode.setOnClickListener(this);
        mBinding.ivMute.setOnClickListener(this);
        mBinding.ivChangeCamera.setOnClickListener(this);
        mBinding.ivReceiveCall.setOnClickListener(this);
        mBinding.ivChangeCamera.setOnClickListener(this);

    }


    private void initAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
            mRtcEngine.setChannelProfile(0);
        } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        if (requestCode == VIDEO_CALL_REQUEST_CODE) {
            setUpAgoraEngineAndJoin();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (requestCode == VIDEO_CALL_REQUEST_CODE)
            finish();
    }

    private void setupSession() {
        mRtcEngine.setChannelProfile(Constants.CHANNEL_PROFILE_COMMUNICATION);
        mRtcEngine.enableVideo();
        mRtcEngine.setVideoEncoderConfiguration(new VideoEncoderConfiguration(VideoEncoderConfiguration.VD_240x240, VideoEncoderConfiguration.FRAME_RATE.FRAME_RATE_FPS_30,
                VideoEncoderConfiguration.STANDARD_BITRATE,
                VideoEncoderConfiguration.ORIENTATION_MODE.ORIENTATION_MODE_FIXED_PORTRAIT));
    }


    private void setupLocalVideoFeed() {
        FrameLayout videoContainer = mBinding.localView;
        SurfaceView videoSurface = RtcEngine.CreateRendererView(this);
        videoSurface.setZOrderMediaOverlay(true);
        videoContainer.addView(videoSurface);
        mRtcEngine.setupLocalVideo(new VideoCanvas(videoSurface, VideoCanvas.RENDER_MODE_FIT, 0));
    }


    private void setupRemoteVideoStream(int uid) {
        mBinding.connectionImage.setVisibility(View.GONE);
        FrameLayout videoContainer = mBinding.remoteView;
        SurfaceView videoSurface = RtcEngine.CreateRendererView(this);
        videoContainer.addView(videoSurface);
        mRtcEngine.setupRemoteVideo(new VideoCanvas(videoSurface, VideoCanvas.RENDER_MODE_FIT, uid));
        mRtcEngine.setRemoteSubscribeFallbackOption(io.agora.rtc.Constants.STREAM_FALLBACK_OPTION_AUDIO_ONLY);
    }

    private void startTimer() {
        mBinding.chronometerTimer.setVisibility(View.VISIBLE);
        mBinding.chronometerTimer.setBase(SystemClock.elapsedRealtime());
        mBinding.chronometerTimer.start();
    }


    public void onLocalAudioMuteClicked(ImageView iv) {

        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.setImageResource(R.drawable.ic_mute);
        } else {
            iv.setSelected(true);
            iv.setImageResource(R.drawable.ic_unmute);
        }

        mRtcEngine.muteLocalAudioStream(iv.isSelected());
    }


    private void changeCallMode(ImageView iv) {
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.setImageResource(R.drawable.ic_video_on);
            mRtcEngine.enableLocalVideo(true);
        } else {
            iv.setSelected(true);
            iv.setImageResource(R.drawable.ic_video_off);
            mRtcEngine.enableLocalVideo(false);
        }

    }

    private void leaveChannel() {
        if (mRtcEngine != null) {
            mRtcEngine.leaveChannel();
            mRtcEngine = null;
            RtcEngine.destroy();
        }
        if (user != null) {
            firebaseDatabase.clearIncoming(user.getUid());
            firebaseDatabase.stopListeningForIncomingCall(user.getUid());
            firebaseDatabase.clearIncoming(user.getIncoming());
        }
        if (ringtone != null && ringtone.isPlaying()) {
            ringtone.stop();
        }
        if (isCaller() && mp != null && mp.isPlaying()) {
            mp.stop();

        }
        finish();
    }


    @Override
    protected void onDestroy() {
        leaveChannel();
        super.onDestroy();
    }

    @Override
    public void disconnectCall() {
        if (callReceived) {
            long duration = SystemClock.elapsedRealtime() - mBinding.chronometerTimer.getBase();
            firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, "Video call ended : " + TimeUtil.getTimeFromMillisecond(duration), String.valueOf(duration));
        }else if (isCaller())
            timer.cancel();
        leaveChannel();
    }
}
