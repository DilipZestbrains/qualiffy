package com.app.qualiffyapp.firebase.chat.types;

public enum FriendStatus {
    REQUEST_NOT_SEND("0"), REQUEST_ACCEPTED("1"), REQUEST_SENT_TO_YOU("2"), REQUEST_SEND_BY_YOU("3");

    public String value;

    FriendStatus(String value) {
        this.value = value;
    }
}
