package com.app.qualiffyapp.firebase.audioCall;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ActivityVoiceChatViewBinding;
import com.app.qualiffyapp.firebase.MyFireBaseMessagingService;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnIncomingChangeListener;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.TimeUtil;
import com.google.gson.Gson;

import java.util.List;

import io.agora.rtc.IRtcEngineEventHandler;
import io.agora.rtc.RtcEngine;

import static com.app.qualiffyapp.constants.AppConstants.AUDIO_CALL_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION;
import static com.app.qualiffyapp.constants.AppConstants.USER;
import static com.app.qualiffyapp.firebase.MyFireBaseMessagingService.CALL_NOTIFICATION_ID;

public class AudioCallActivity extends BaseActivity
        implements OnIncomingChangeListener {
    private static final String LOG_TAG = AudioCallActivity.class.getSimpleName();
    private RtcEngine mRtcEngine;
    private AppFirebaseDatabase firebaseDatabase;
    private User user;
    private UserConnection connection;
    private String channel;
    private ActivityVoiceChatViewBinding mBinding;
    private boolean isCallReceived = false;
    private Ringtone ringtone;
    private MediaPlayer mp;
    private CountDownTimer timer = new CountDownTimer(30000, 1000) {
        private boolean isStatusSet = false;

        @Override
        public void onTick(long millisUntilFinished) {
            if (millisUntilFinished < 1000) {
                if (!isStatusSet) {
                    mBinding.tvCallingStatus.setVisibility(View.VISIBLE);
                    mBinding.tvCallingStatus.setText(getResources().getString(R.string.disconnected));
                    isStatusSet = true;
                }
            }
        }

        @Override
        public void onFinish() {
            firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, getResources().getString(R.string.missed_audio_call_at) + " " + TimeUtil.getDateFromTimeStamp(String.valueOf(System.currentTimeMillis()), "hh:mm a"), null);
            leaveChannel();
        }
    };
    private final IRtcEngineEventHandler mRtcEventHandler = new IRtcEngineEventHandler() {

        @Override
        public void onUserJoined(int uid, int elapsed) {
            super.onUserJoined(uid, elapsed);
            runOnUiThread(() -> onRemoteUserJoined());
        }

        @Override
        public void onUserOffline(int uid, int reason) {
            if (isCallReceived) {
                long duration = SystemClock.elapsedRealtime() - mBinding.chronometerTimer.getBase();
                firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, "Audio call ended : " + TimeUtil.getTimeFromMillisecond(duration), String.valueOf(duration));
            } else if (isCaller())
                timer.cancel();
            runOnUiThread(() -> leaveChannel());

        }

    };


    @Override
    protected void initView() {
        mBinding = (ActivityVoiceChatViewBinding) viewDataBinding;
        getIntentData();
    }

    private void getIntentData() {
        if (getIntent().getBundleExtra(BUNDLE) != null) {
            connection = new Gson().fromJson(getIntent().getBundleExtra(BUNDLE).getString(CONNECTION), UserConnection.class);
            user = new Gson().fromJson(getIntent().getBundleExtra(BUNDLE).getString(USER), User.class);
            if (connection != null && user != null) {
                initFirebase();
            } else
                finish();
        } else
            finish();
        MyFireBaseMessagingService.clearNotification(CALL_NOTIFICATION_ID);

    }


    private void initFirebase() {
        firebaseDatabase = AppFirebaseDatabase.getInstance();
        firebaseDatabase.startListenForIncomingCalls(user.getUid(), this);
        channel = firebaseDatabase.getChatId(user.getUid(), connection.getUid());
        if (user.getCaller_id() != null && user.getIncoming() != null && channel != null)
            initCalling();
        else leaveChannel();
    }


    private void initCalling() {
        GlideUtil.loadImage(mBinding.remoteView, connection.getImg(), R.drawable.photo_placeholder, 1000, 0);
        GlideUtil.loadCircularImage(mBinding.userImage, connection.getImg(), R.drawable.profile_placeholder);
        mBinding.userName.setText(connection.getName());
        if (onAskForSomePermission(this, AppConstants.AUDIO_CALL_PERMISSION, AUDIO_CALL_PERMISSION_REQUEST_CODE)) {
            initAgoraEngineAndJoinChannel();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mBinding.ivBack.getId()) {
            onBackPressed();
        }
    }

    @Override
    protected void setListener() {
        mBinding.ivBack.setOnClickListener(this);
    }

    private void enableReceivingMode(boolean enable) {
        mBinding.ivDisconnectCall.setVisibility(View.VISIBLE);
        if (enable)
            mBinding.ivReceiveCall.setVisibility(View.VISIBLE);
        else mBinding.ivReceiveCall.setVisibility(View.GONE);
    }

    private void initAgoraEngineAndJoinChannel() {
        initializeAgoraEngine();
        if (isCaller()) {
            timer.start();
            mp = MediaPlayer.create(this, R.raw.tring_tring);
            mp.setVolume(10, 10);
            mp.setLooping(true);
            mp.start();
            AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mAudioManager.setMode(AudioManager.STREAM_MUSIC);
            mAudioManager.setSpeakerphoneOn(false);
            enableReceivingMode(false);
            joinChannel();
            mBinding.tvCallingStatus.setText(getResources().getString(R.string.outgoing));
        } else {
            Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            ringtone = RingtoneManager.getRingtone(this, uri);
            ringtone.play();
            mBinding.tvCallingStatus.setText(getResources().getString(R.string.incoming));
            enableReceivingMode(true);
        }
    }

    private void onRemoteUserJoined() {
        timer.cancel();
        isCallReceived = true;
        if (isCaller()) {
            if (mp != null && mp.isPlaying()) {
                mp.stop();
            }

        }
        startTimer();
    }

    private boolean isCaller() {
        return user.getUid().equals(user.getCaller_id());
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        if (requestCode == AUDIO_CALL_PERMISSION_REQUEST_CODE) {
            initAgoraEngineAndJoinChannel();
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        if (requestCode == AUDIO_CALL_PERMISSION_REQUEST_CODE)
            leaveChannel();
    }


    @Override
    protected int getLayoutById() {
        return R.layout.activity_voice_chat_view;
    }

    private void startTimer() {
        mBinding.chronometerTimer.setVisibility(View.VISIBLE);
        mBinding.chronometerTimer.setBase(SystemClock.elapsedRealtime());
        mBinding.chronometerTimer.start();
    }

    @Override
    protected void onDestroy() {
        leaveChannel();
        super.onDestroy();
    }

    public void onLocalAudioMuteClicked(View view) {
        ImageView iv = (ImageView) view;
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.setImageResource(R.drawable.ic_mute);

        } else {
            iv.setSelected(true);
            iv.setImageResource(R.drawable.ic_unmute);
        }

        mRtcEngine.muteLocalAudioStream(iv.isSelected());
    }

    public void onSwitchSpeakerphoneClicked(View view) {
        ImageView iv = (ImageView) view;
        if (iv.isSelected()) {
            iv.setSelected(false);
            iv.setImageResource(R.drawable.ic_speaker_off);

        } else {
            iv.setSelected(true);
            iv.setImageResource(R.drawable.ic_speaker_on);
        }

        mRtcEngine.setEnableSpeakerphone(view.isSelected());
    }

    public void onEncCallClicked(View view) {
        if (isCaller() && !isCallReceived)
            timer.cancel();
        leaveChannel();
    }

    public void onReceiveCall(View view) {
        if (ringtone != null)
            ringtone.stop();
//        if(!isCaller())
//            startTimer();
        joinChannel();
        enableReceivingMode(false);
    }

    private void joinChannel() {
        mRtcEngine.joinChannel(null, channel, "Extra Optional Data", 0);
    }

    private void leaveChannel() {
        if (user != null) {
            firebaseDatabase.clearIncoming(user.getUid());
            firebaseDatabase.clearIncoming(user.getIncoming());
            firebaseDatabase.stopListeningForIncomingCall(user.getUid());
        }
        if (mRtcEngine != null) {
            mRtcEngine.leaveChannel();
            mRtcEngine = null;
            RtcEngine.destroy();
        }
        if (!isCaller() && ringtone != null && ringtone.isPlaying())
            ringtone.stop();
        if (isCaller() && mp != null && mp.isPlaying()) {
            mp.stop();
        }

        finish();
    }

    @Override
    public void disconnectCall() {
        if (isCallReceived) {
            long duration = SystemClock.elapsedRealtime() - mBinding.chronometerTimer.getBase();
            firebaseDatabase.sendMessage(connection.getUid(), MediaType.CALL, "Audio call ended : " + TimeUtil.getTimeFromMillisecond(duration), String.valueOf(duration));
        } else if (isCaller())
            timer.cancel();
        leaveChannel();
    }

    private void initializeAgoraEngine() {
        try {
            mRtcEngine = RtcEngine.create(getBaseContext(), getString(R.string.agora_app_id), mRtcEventHandler);
            mRtcEngine.setChannelProfile(0);
        } catch (Exception e) {
            Log.e(LOG_TAG, Log.getStackTraceString(e));
            throw new RuntimeException("NEED TO check rtc sdk init fatal error\n" + Log.getStackTraceString(e));
        }
    }
}
