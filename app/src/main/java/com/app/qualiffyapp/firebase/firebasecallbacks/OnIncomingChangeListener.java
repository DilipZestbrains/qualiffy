package com.app.qualiffyapp.firebase.firebasecallbacks;

public interface OnIncomingChangeListener {
    void disconnectCall();

}
