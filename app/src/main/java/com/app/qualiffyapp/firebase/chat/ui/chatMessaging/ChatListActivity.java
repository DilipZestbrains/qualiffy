package com.app.qualiffyapp.firebase.chat.ui.chatMessaging;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.firebase.chat.adapter.chatConversationList.ChatConversationListAdapter;
import com.app.qualiffyapp.audiorecoder.AudioRecorder;
import com.app.qualiffyapp.audiorecoder.AudioRecorderCallbacks;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.constants.AppConstants;
//import com.app.qualiffyapp.customViews.customCamera.CameraActivity;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.databinding.ActivityChatListBinding;
import com.app.qualiffyapp.firebase.Call;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.ui.ChatAttachmentBottomSheet;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.firebasecallbacks.ChatCallbacks;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnChatlistChangeListener;
import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.utils.IntentUtil;
import com.app.qualiffyapp.utils.Utility;
import com.sandrios.sandriosCamera.internal.SandriosCamera;
import com.sandrios.sandriosCamera.internal.configuration.CameraConfiguration;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.AppConstants.AUDIO_PERMISSION;
import static com.app.qualiffyapp.constants.AppConstants.AUDIO_RECORD_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.CAMERA_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_ID;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_NAME;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_PERMISION;
import static com.app.qualiffyapp.constants.AppConstants.GALLERY_STORAGE_PERMISSION_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BLOCKED;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;

public class ChatListActivity extends BaseActivity implements ChatListInterface, AudioRecorderCallbacks, ChatCallbacks, yesNoCallback, OnChatlistChangeListener {
    private AppFirebaseDatabase appFirebaseDatabase;
    private ActivityChatListBinding mBinding;
    private ChatConversationListAdapter chatAdapter;
    private LinearLayoutManager chatLayoutManger;
    private String connectionName;
    private String connectionUid;
    private String userUid;
    private ChatListPresenter mPresenter;
    private CustomDialogs customDialogs;
    private AudioRecorder recorder;
    private String audioDuration;
    private String blockStatus;
    private String type;
    private FirebaseUserType userType;
    private boolean isDeleteModeON = false;
    private String userName;


    @Override
    protected void initView() {
        mBinding = (ActivityChatListBinding) viewDataBinding;
        userUid = Utility.getStringSharedPreference(this, UID);
        userName = Utility.getStringSharedPreference(this, FIRST_NAME);
        mPresenter = new ChatListPresenter(this);
        initUserType();
        initRecorder();
        getIntentData();
        initDialog();
        onSetListener();
        onSetToolbar(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initUserType() {
        int user = Utility.getIntFromSharedPreference(this, USER_TYPE);
        if (user == AppConstants.BUSINESS_USER)
            userType = FirebaseUserType.BUSINESS;
        else if (user == AppConstants.JOB_SEEKER)
            userType = FirebaseUserType.JOB_SEEKER;
    }

    private void initRecorder() {
        if (onAskForSomePermission(this, AUDIO_PERMISSION, AUDIO_RECORD_REQUEST_CODE))
            recorder = new AudioRecorder(this);
    }

    private void getIntentData() {
        Bundle bundle = getIntent().getBundleExtra(BUNDLE);
        if (bundle != null) {
            connectionName = bundle.getString(CONNECTION_NAME);
            connectionUid = bundle.getString(CONNECTION_ID);
            blockStatus = bundle.getString(IS_BLOCKED);
            type = bundle.getString(CONNECTION_TYPE);
        }
        initFirebaseDatabase();
        if (blockStatus != null)
            initSendMessageView(blockStatus);


    }

    private void initSendMessageView(String blockStatus) {
        if (blockStatus.equals(BlockStatus.NOT_BLOCKED.value)) {
            disableChat(null, false);
        } else if (blockStatus.equals(BlockStatus.BLOCKED.value)) {
            disableChat(getResources().getString(R.string.you_can_not_send_messages), true);
        }
    }

    private void initFirebaseDatabase() {
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
        appFirebaseDatabase.listenUserChat(connectionUid, this);
        mBinding.swipeToRefresh.setRefreshing(true);
        loadChats();
    }


    private void loadChats() {
        chatLayoutManger = new LinearLayoutManager(this);
        mBinding.rvChatscreen.setLayoutManager(chatLayoutManger);
        appFirebaseDatabase.getChat(connectionUid, chatList -> {
//            if (type != null) {
//                if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(this, USER_TYPE) == AppConstants.JOB_SEEKER)
//                    if (chatList.size() <= 0) {
//                        disableChat(getResources().getString(R.string.you_can_not_initiate_chat), true);
//                    } else {
//                        disableChat(null, false);
//                    }
            disableChat(null, false);
            if (chatAdapter == null) {
                chatAdapter = new ChatConversationListAdapter(userUid, chatList);
                mBinding.rvChatscreen.setAdapter(chatAdapter);
                chatAdapter.registerChatCallbacks(this);
                chatAdapter.initMediaPlayer();
            } else {
                chatAdapter.refreshList(chatList);
            }
//            }

            mBinding.swipeToRefresh.setRefreshing(false);
            showProgress(false);
        });

    }

    private void disableChat(String msg, boolean disable) {
        if (disable) {
            mBinding.llChatSend.setVisibility(View.GONE);
            mBinding.lltvMessage.setVisibility(View.VISIBLE);

            if (msg != null)
                mBinding.tvMessage.setText(msg);
        } else {
            mBinding.llChatSend.setVisibility(View.VISIBLE);
            mBinding.lltvMessage.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        appFirebaseDatabase.stopListeningChatUpdate(connectionUid);
        if (chatAdapter != null)
            chatAdapter.removeCallbacks();
        if (chatAdapter != null) {
            chatAdapter.releaseMediaPlayer();
            appFirebaseDatabase.updateMessageCount(connectionUid);
        }

    }

    @Override
    public void onYesClicked(String from) {
        customDialogs.hideDialog();
        if (onAskForSomePermission(this, GALLERY_PERMISION, GALLERY_STORAGE_PERMISSION_REQUEST_CODE)) {
            Intent in = IntentUtil.getAttatchmentIntent(this);
            if (in != null)
                startActivityForResult(in, 0);
        }
    }

    @Override
    public void onNoClicked(String from) {
        customDialogs.hideDialog();
        if (onAskForSomePermission(this, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE)) {
            openCamera();



          /*  Intent i = new Intent(this, CameraActivity.class);
            i.putExtra("media_type", 2);
            startActivityForResult(i, REQUEST_CODE_CAMERA_IMG);*/
        }

    }

    private void openCamera() {
        SandriosCamera
                .with()
                .setMediaAction(CameraConfiguration.MEDIA_ACTION_BOTH)
                .launchCamera(this);

    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Granted");
        switch (requestCode) {
            case CAMERA_STORAGE_PERMISSION_REQUEST_CODE:
                openCamera();
              /*  Intent i = new Intent(this, CameraActivity.class);
                i.putExtra("media_type", 2);
                startActivityForResult(i, REQUEST_CODE_CAMERA_IMG);*/
                break;
            case GALLERY_STORAGE_PERMISSION_REQUEST_CODE:
                Intent in = IntentUtil.getAttatchmentIntent(this);
                if (in != null)
                    startActivityForResult(in, 0);
                break;
            case AUDIO_RECORD_REQUEST_CODE:
                recorder = new AudioRecorder(this);
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        Log.e("Permissions :  ", "Permission Denied");// Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        if (EasyPermissions.somePermissionPermanentlyDenied(this, list)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE:
                    Log.e("Permission", "Returned from setting");
                    break;
                case REQUEST_CODE_CAMERA_IMG:
                    String file = data.getStringExtra(MEDIA_PATH);
                    if (file != null) {
                        String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(file));
                        if (mime != null)
                            if (mime.contains("mp4"))
                                mPresenter.uploadMedia(file, MediaType.VIDEO, userType);
                            else {
                                mPresenter.uploadMedia(file, MediaType.IMAGE, userType);
                            }

                    }

                    break;


            }
        }


    }


    private void onSetToolbar(boolean isDelete) {
        if (!isDelete) {
            if (connectionName != null)
                mBinding.includeToolbar.tvToolbar.setText(connectionName);
            mBinding.includeToolbar.ivImageBtnOneToolbar.setImageResource(R.drawable.ic_phone_conversation);
            mBinding.includeToolbar.ivImageBtnTwoToolbar.setImageResource(R.drawable.ic_video_conversation);
            mBinding.includeToolbar.ivImageBtnOneToolbar.setOnClickListener(v -> {
                Call.getInstance(this).makeAudioCall(this, blockStatus, type, connectionUid);
            });
            mBinding.includeToolbar.ivImageBtnTwoToolbar.setOnClickListener(v -> {
                Call.getInstance(this).makeVideoCall(this, blockStatus, type, connectionUid);
            });
        } else {
            mBinding.includeToolbar.tvToolbar.setText("0");
            mBinding.includeToolbar.ivImageBtnOneToolbar.setImageResource(R.drawable.ic_delete);
            mBinding.includeToolbar.ivImageBtnOneToolbar.setOnClickListener(v -> {
                if (chatAdapter != null && chatAdapter.getSelectChat().size() > 0) {
                    showProgress(true);
                    mPresenter.deleteChaMedia(chatAdapter.getSelectChat(), userType);
                }

            });
            mBinding.includeToolbar.ivImageBtnTwoToolbar.setImageDrawable(null);
        }
    }

    private void onSetListener() {
        mBinding.etChatMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    mBinding.btnSendMessage.setVisibility(View.VISIBLE);
                    mBinding.btnRecordMessage.setVisibility(View.GONE);
                } else {
                    mBinding.btnSendMessage.setVisibility(View.GONE);
                    mBinding.btnRecordMessage.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mBinding.btnRecordMessage.setOnClickListener(this);
        mBinding.btnSendMessage.setOnClickListener(this);
        mBinding.btnSendImage.setOnClickListener(this);
        mBinding.ivAttach.setOnClickListener(this);
        mBinding.includeToolbar.ivImageBtnOneToolbar.setOnClickListener(this);
        mBinding.includeToolbar.ivImageBtnTwoToolbar.setOnClickListener(this);
        mBinding.includeToolbar.ivBackToolbar.setOnClickListener(this);
        mBinding.btnCancelAudio.setOnClickListener(this);
        mBinding.swipeToRefresh.setOnRefreshListener(this::loadChats);
    }


    private void initDialog() {
        customDialogs = new CustomDialogs(this, this);
        customDialogs.showCustomDialogTwoButtons(getResources().getString(R.string.add_media_dialog_title),
                getResources().getString(R.string.add_media_dialog_body),
                getResources().getString(R.string.add_media_dialog_pos_btn),
                getResources().getString(R.string.add_media_dialog_neg_btn),
                getResources().getString(R.string.ADD_MEDIA_TITLE));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_chat_list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_toolbar:
                onBackPressed();
                break;
            case R.id.btn_send_message:
                if (recorder.isRecording())
                    recorder.stopRecording(true);
                else if (isValidMessage()) {
                    sendTextMessage();
                }
                break;
            case R.id.btn_send_image:
                if (onAskForSomePermission(this, CAMERA_PERMISION, CAMERA_STORAGE_PERMISSION_REQUEST_CODE)) {
                    openCamera();
                  /*  Intent i = new Intent(this, CameraActivity.class);
                    i.putExtra("media_type", 2);
                    startActivityForResult(i, REQUEST_CODE_CAMERA_IMG);*/
                }
                break;
            case R.id.ivAttach:
                (new ChatAttachmentBottomSheet()).show(getSupportFragmentManager(), "Chat Attachment");
                break;
            case R.id.btn_record_message:
                startRecording();
                break;
            case R.id.btn_cancel_audio:
                if (recorder != null)
                    recorder.stopRecording(false);
                break;

        }
    }


    private void sendTextMessage() {
        if (isValidMessage()) {
            appFirebaseDatabase.sendMessage(connectionUid, MediaType.TEXT, mBinding.etChatMessage.getText().toString(), null);
            mBinding.etChatMessage.getText().clear();
            sendNotification();
        }
    }

    private boolean isValidMessage() {
        if (TextUtils.isEmpty(mBinding.etChatMessage.getText().toString())) {
            Utility.showToast(this, getResources().getString(R.string.you_can_not_send_empty_message));
            return false;
        }
        return true;
    }

    private void startRecording() {
        if (recorder == null)
            initRecorder();
        else recorder.startRecording();
    }

    @Override
    public void onNewMessageArrival(Chat chat) {
        if (chatAdapter == null) {
            chatAdapter = new ChatConversationListAdapter(userUid, new ArrayList<>());
            mBinding.rvChatscreen.setAdapter(chatAdapter);
            chatAdapter.registerChatCallbacks(this);
            chatAdapter.addNewMessage(chat);
        } else {
            chatAdapter.addNewMessage(chat);
        }
        chatLayoutManger.scrollToPosition(chatAdapter.getItemCount() - 1);
        if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(this, USER_TYPE) == AppConstants.JOB_SEEKER) {
            if (chatAdapter.getItemCount() > 0)
                disableChat(null, false);
        }

    }

    @Override
    public void onMessageDeleted(Chat chat) {
        chatAdapter.delete(chat);
    }

    @Override
    public void preUpload() {
        showProgress(true);
    }

    @Override
    public void onMediaUpload(String msg, String baseUrl, String imgUrl, String mediaType) {
        sendNotification();
        showProgress(false);
        if (imgUrl != null && !TextUtils.isEmpty(imgUrl)) {
            String url;
            if (baseUrl != null && !TextUtils.isEmpty(baseUrl))
                url = baseUrl + imgUrl;
            else url = imgUrl;
            if (mediaType.equals(MediaType.IMAGE.value))
                appFirebaseDatabase.sendMessage(connectionUid, MediaType.IMAGE, url, null);

            else if (mediaType.equals(MediaType.VIDEO.value))
                appFirebaseDatabase.sendMessage(connectionUid, MediaType.VIDEO, url, null);

            else if (mediaType.equals(MediaType.VOICE.value)) {
                appFirebaseDatabase.sendMessage(connectionUid, MediaType.VOICE, url, audioDuration);
                audioDuration = null;
            } else if (mediaType.equals(MediaType.PDF.value)) {
                appFirebaseDatabase.sendMessage(connectionUid, MediaType.PDF, url, null);
            }

        } else {
            Utility.showToast(this, msg);
        }

    }

    @Override
    public void onMediaDeleted(String msg, int status, ArrayList<Chat> chats) {
        showProgress(false);
        if (status == SUCCESS) {
            Utility.showToast(this, getResources().getString(R.string.message_deleted_successfully));

            appFirebaseDatabase.deleteMessage(connectionUid, chats);
        } else Utility.showToast(this, msg);
    }

    @Override
    public void onNotificationSend(int status, String msg) {
        if (status == FAILURE)
            Utility.showToast(this, msg);
    }

    public ChatListPresenter getPresenter() {
        return mPresenter;
    }


    private void msgUi(MessageType type) {
        switch (type) {
            case AUDIO_MESSAGE:
                mBinding.etChatMessage.setVisibility(View.GONE);
                mBinding.tvChatRecorder.setVisibility(View.VISIBLE);
                mBinding.btnSendMessage.setVisibility(View.VISIBLE);
                mBinding.btnRecordMessage.setVisibility(View.GONE);
                mBinding.btnCancelAudio.setVisibility(View.VISIBLE);
                mBinding.btnSendImage.setVisibility(View.GONE);
                mBinding.ivAttach.setVisibility(View.GONE);
                mBinding.ivMic.setVisibility(View.VISIBLE);
                break;
            case TEXT_MESSAGE:
                mBinding.etChatMessage.setVisibility(View.VISIBLE);
                mBinding.tvChatRecorder.setVisibility(View.GONE);
                mBinding.btnSendMessage.setVisibility(View.GONE);
                mBinding.btnRecordMessage.setVisibility(View.VISIBLE);
                mBinding.btnCancelAudio.setVisibility(View.GONE);
                mBinding.btnSendImage.setVisibility(View.VISIBLE);
                mBinding.ivAttach.setVisibility(View.VISIBLE);
                mBinding.ivMic.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void onStartRecording() {
        mBinding.tvChatRecorder.setBase(SystemClock.elapsedRealtime());
        mBinding.tvChatRecorder.start();
        msgUi(MessageType.AUDIO_MESSAGE);
    }

    @Override
    public void onStopRecording(String filePath) {
        Long time = SystemClock.elapsedRealtime() - mBinding.tvChatRecorder.getBase();
        audioDuration = String.valueOf(time);
        mBinding.tvChatRecorder.setBase(SystemClock.elapsedRealtime());
        mBinding.tvChatRecorder.stop();
        msgUi(MessageType.TEXT_MESSAGE);
        if (time < 2000) {
            Utility.showToast(this, "Audio message should be at least 2 sec long.");
        } else if (filePath != null)
            mPresenter.uploadMedia(filePath, MediaType.VOICE, userType);

    }

    @Override
    public void onPdfClick(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    @Override
    public void onVideoClick(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        intent.setDataAndType(Uri.parse(url), "video/mp4");
        startActivity(intent);
    }

    @Override
    public void onImageClick(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    @Override
    public void onDeleteModeOn(boolean isModeON) {
        onSetToolbar(isModeON);
        isDeleteModeON = isModeON;
    }

    @Override
    public void updateToolbarTitle(String title) {
        mBinding.includeToolbar.tvToolbar.setText(title);
    }

    @Override
    public void updateLastMessage(Chat lastMessage) {
        if (lastMessage != null)
            appFirebaseDatabase.saveLastMessage(connectionUid, lastMessage.getMedia_type(), lastMessage.getMedia_url(), lastMessage.getTimestamp(), false);
        else
            appFirebaseDatabase.saveLastMessage(connectionUid, MediaType.TEXT.value, "", null, false);
    }

    @Override
    public void onBackPressed() {
        if (isDeleteModeON) {
            isDeleteModeON = false;
            onSetToolbar(false);
            if (chatAdapter != null)
                chatAdapter.removeDeleteMode();
        } else
            super.onBackPressed();
    }

    public FirebaseUserType getUserType() {
        return userType;
    }

    private void sendNotification() {

        RequestBean bean = new RequestBean();
        if (Utility.getIntFromSharedPreference(ChatListActivity.this, USER_TYPE) == JOB_SEEKER) {
            bean.type = "6";
        } else {
            bean.type = "8";
        }

        bean.name = userName;
        mPresenter.sendNotification(bean, connectionUid, type);
    }


    enum MessageType {
        AUDIO_MESSAGE, TEXT_MESSAGE
    }
}
