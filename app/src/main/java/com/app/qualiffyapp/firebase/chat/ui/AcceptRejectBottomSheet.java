package com.app.qualiffyapp.firebase.chat.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.BottomSheetAcceptRejectBinding;
import com.app.qualiffyapp.ui.activities.contacts.ContactActivity;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_ID;

public class AcceptRejectBottomSheet extends BottomSheetDialogFragment implements View.OnClickListener {
    private BottomSheetAcceptRejectBinding mBinding;
    private String connectionId;
    private ContactActivity contactActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.bottom_sheet_accept_reject, container, false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setListeners();
    }

    private void initView() {
        contactActivity = (ContactActivity) getActivity();
        if (getArguments() != null)
            connectionId = getArguments().getString(CONNECTION_ID);
    }

    private void setListeners() {
        mBinding.tvAccept.setOnClickListener(this);
        mBinding.tvDecline.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAccept:
                if (connectionId != null && contactActivity != null) {
                    contactActivity.acceptRequest(connectionId);
                    dismiss();
                }
                break;
            case R.id.tvDecline:
                if (connectionId != null && contactActivity != null) {
                    contactActivity.rejectRequest(connectionId);
                    dismiss();
                }


        }
    }
}
