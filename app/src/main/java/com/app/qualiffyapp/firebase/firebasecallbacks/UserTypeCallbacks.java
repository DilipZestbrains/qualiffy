package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.User;

public interface UserTypeCallbacks {

    void onGetUser(User u);
}
