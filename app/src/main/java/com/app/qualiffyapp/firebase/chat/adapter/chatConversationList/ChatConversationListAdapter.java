package com.app.qualiffyapp.firebase.chat.adapter.chatConversationList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.audioPlayer.AudioPlayerCallbacks;
import com.app.qualiffyapp.audioPlayer.ChatAudioPlayer;
import com.app.qualiffyapp.databinding.ItemChatLeftBinding;
import com.app.qualiffyapp.databinding.ItemChatRightBinding;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.firebasecallbacks.ChatCallbacks;
import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.TimeUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import static com.app.qualiffyapp.constants.AppConstants.CHAT_LEFT_ALIGNED;
import static com.app.qualiffyapp.constants.AppConstants.CHAT_RIGHT_ALIGNED;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.CALL;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.IMAGE;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.PDF;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.TEXT;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.VIDEO;
import static com.app.qualiffyapp.firebase.chat.types.MediaType.VOICE;


public class ChatConversationListAdapter extends RecyclerView.Adapter<ChatConversationListAdapter.ChatListViewHolder> {
    private ArrayList<Chat> chats;
    private ArrayList<Chat> selectedChat;
    private String userId;
    private boolean isSelectModeOn = false;
    private ChatAudioPlayer chatAudioPlayer;
    private ChatCallbacks chatCallbacks;
    private Comparator<Chat> chatComparator = (o1, o2) -> {
        if (o1.getTimestamp() != null && o2.getTimestamp() != null) {
            return o2.getTimestamp().compareTo(o1.getTimestamp());
        }
        return 0;
    };

    public ChatConversationListAdapter(String userId, ArrayList<Chat> chats) {
        this.chats = chats;
        this.selectedChat = new ArrayList<>();
        this.userId = userId;
    }

    public void registerChatCallbacks(ChatCallbacks chatCallbacks) {
        this.chatCallbacks = chatCallbacks;
    }

    public void initMediaPlayer() {
        chatAudioPlayer = ChatAudioPlayer.getInstance();
    }

    public void releaseMediaPlayer() {
        if (chatAudioPlayer != null)
            chatAudioPlayer.stopAudio();
    }

    public void removeDeleteMode() {
        isSelectModeOn = false;
        selectedChat.clear();
        notifyDataSetChanged();
    }

    public void removeCallbacks() {
        this.chatCallbacks = null;
    }

    @NonNull
    @Override
    public ChatListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case CHAT_RIGHT_ALIGNED:
                return new ChatListViewHolder((ItemChatRightBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_chat_right, parent, false));
            case CHAT_LEFT_ALIGNED:
                return new ChatListViewHolder((ItemChatLeftBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_chat_left, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ChatListViewHolder holder, int i) {
        switch (holder.getItemViewType()) {
            case CHAT_LEFT_ALIGNED:
                holder.bindChat(chats.get(i), CHAT_LEFT_ALIGNED, i);
                break;
            case CHAT_RIGHT_ALIGNED:
                holder.bindChat(chats.get(i), CHAT_RIGHT_ALIGNED, i);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (userId.equals(chats.get(position).getSender_id()))
            return CHAT_RIGHT_ALIGNED;
        else
            return CHAT_LEFT_ALIGNED;
    }

    private void chatItemClickListener(Chat chat, MediaType mediaType, ItemChatRightBinding mBinding) {
        if (chatCallbacks != null) {
            if (mediaType.equals(PDF)) {
                chatCallbacks.onPdfClick(chat.getMedia_url());
            } else if (mediaType.equals(VIDEO)) {
                chatCallbacks.onVideoClick(chat.getMedia_url());
            } else if (mediaType.equals(IMAGE)) {
                chatCallbacks.onImageClick(chat.getMedia_url());
            }
        }
    }

    public void applyLongPressClick(View view, ItemChatRightBinding itemViewRight, Chat chat) {
        view.setOnLongClickListener(v -> {
            if (!isSelectModeOn) {
                isSelectModeOn = true;
                selectedChat.add(chat);
                if (chatCallbacks != null)
                    chatCallbacks.onDeleteModeOn(true);
                itemViewRight.shadowView.setVisibility(View.VISIBLE);
                itemViewRight.shadowView.setBackgroundColor(itemViewRight.getRoot().getContext().getResources().getColor(R.color.color_pale_golden_rod));
                notifyDataSetChanged();
            }
            return true;
        });
    }

    private void setAudioPlayer(ImageView playPauseButtom, ProgressBar pr, SeekBar seekBar, Chat chat) {
        String url = chat.getMedia_url();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    chatAudioPlayer.seekToPosition(progress);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        playPauseButtom.setOnClickListener(v -> {
            if (chatAudioPlayer.isLoading())
                return;

            if (chatAudioPlayer.isSameMedia(url)) {
                if (chatAudioPlayer.isPlaying()) {
                    chatAudioPlayer.pauseAudio();
                    return;
                }
                if (chatAudioPlayer.isPaused()) {
                    chatAudioPlayer.resumeAudio();
                    return;
                }
            }

            chatAudioPlayer.startAudio(url, new AudioPlayerCallbacks() {
                @Override
                public void setBufferingState(Boolean isBuffering) {
                    if (isBuffering) {
                        pr.setVisibility(View.VISIBLE);
                        playPauseButtom.setVisibility(View.INVISIBLE);
                    } else {
                        pr.setVisibility(View.GONE);
                        playPauseButtom.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onStartAudio(int duration) {
                    seekBar.setMax(duration);
                    playPauseButtom.setImageResource(R.drawable.ic_round_pause);
                }

                @Override
                public void onStopAudio() {
                    playPauseButtom.setImageResource(R.drawable.ic_round_play);
                    seekBar.setProgress(0);
                }

                @Override
                public void onPause() {
                    playPauseButtom.setImageResource(R.drawable.ic_round_play);
                }

                @Override
                public void onResumeAudio() {
                    playPauseButtom.setImageResource(R.drawable.ic_round_pause);


                }

                @Override
                public void updateTime(int position) {
                    seekBar.setProgress(position);

                }
            });


        });
    }

    public void addNewMessage(Chat chat) {
        chats.add(chat);
        notifyDataSetChanged();
    }


    public void refreshList(ArrayList<Chat> chats) {
        this.chats = chats;
        notifyDataSetChanged();
    }

    public void delete(Chat chat) {
//        for (Chat c : selectedChat) {
//            chats.remove(c);
//        }
//        if (chatCallbacks != null) {
//            chatCallbacks.onDeleteModeOn(false);
//            chatCallbacks.deleteFromServer(selectedChat);
//        }
//        isSelectModeOn = false;
//        selectedChat.clear();
//        notifyDataSetChanged();
        int index = -1;
        if (chat != null) {
            for (int i = chats.size() - 1; i >= 0; i--) {
                if (chats.get(i).getMessage_id().equals(chat.getMessage_id())) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                selectedChat.remove(chats.get(index));
                chats.remove(index);
                if (selectedChat.size() > 0) {
                    notifyItemRemoved(index);
                    notifyItemChanged(index);
                } else {
                    if (chatCallbacks != null)
                        chatCallbacks.onDeleteModeOn(false);
                    isSelectModeOn = false;
                    notifyDataSetChanged();
                }

                if (index >= chats.size() - 1) {
                    if (chats.size() > 0) {
                        chatCallbacks.updateLastMessage(chats.get(chats.size() - 1));
                    } else {
                        chatCallbacks.updateLastMessage(null);
                    }
                }
            }
        }


    }

    public ArrayList<Chat> getSelectChat() {
        Collections.sort(selectedChat, chatComparator);
        return selectedChat;
    }

    private boolean showTime(int position, String timestamp) {
        String currentDate = TimeUtil.getDateFromTimestamp(timestamp);
        if (position > 0) {
            String previousDate = TimeUtil.getDateFromTimestamp(chats.get(position - 1).getTimestamp());
            return currentDate != null && previousDate != null && !currentDate.equals(previousDate);
        } else return true;
    }

    class ChatListViewHolder extends RecyclerView.ViewHolder {

        ItemChatLeftBinding itemViewLeft;
        ItemChatRightBinding itemViewRight;

        ChatListViewHolder(ItemChatLeftBinding itemViewLeft) {
            super(itemViewLeft.getRoot());
            this.itemViewLeft = itemViewLeft;
        }

        ChatListViewHolder(ItemChatRightBinding itemViewRight) {
            super(itemViewRight.getRoot());
            this.itemViewRight = itemViewRight;
        }

        void bindChat(Chat chat, int type, int position) {
            if (type == CHAT_LEFT_ALIGNED) {
                bindMessage(chat, itemViewLeft);
                if (showTime(position, chat.getTimestamp())) {
                    itemViewLeft.tvDate.setVisibility(View.VISIBLE);
                    itemViewLeft.tvDate.setText(TimeUtil.getDateFromTimestamp(chat.getTimestamp()));
                } else itemViewLeft.tvDate.setVisibility(View.GONE);
            } else {
                //onParent view
//                applyLongPressClick(itemViewRight.getRoot(), itemViewRight, chat);
                bindMessage(chat, itemViewRight);
                if (showTime(position, chat.getTimestamp())) {
                    itemViewRight.tvDate.setVisibility(View.VISIBLE);
                    itemViewRight.tvDate.setText(TimeUtil.getDateFromTimestamp(chat.getTimestamp()));
                } else itemViewRight.tvDate.setVisibility(View.GONE);
            }
        }

        private void bindMessage(Chat chat, ItemChatLeftBinding mBinding) {
            mBinding.imageVideoLayout.setVisibility(View.GONE);
            mBinding.chatAudioPlayerLayout.setVisibility(View.GONE);
            mBinding.layoutPdf.setVisibility(View.GONE);
            mBinding.layoutTextMessage.setVisibility(View.GONE);
            mBinding.tvCallingStatus.setVisibility(View.GONE);

            String time = TimeUtil.getDateFromTimeStamp(chat.getTimestamp(), "hh:mm a");
            String media_url = chat.getMedia_url();
            String media_type = chat.getMedia_type();
            if (media_type != null && media_url != null) {
                if (media_type.equals(TEXT.value)) {
                    mBinding.layoutTextMessage.setVisibility(View.VISIBLE);
                    mBinding.tvMessage.setText(media_url);
                    mBinding.tvTextTime.setText(time);
                    return;
                }
                if (media_type.equals(IMAGE.value)) {
                    mBinding.imageVideoLayout.setVisibility(View.VISIBLE);
                    mBinding.ivPlayIcon.setVisibility(View.GONE);
                    mBinding.tvTimeStamp.setText(time);
                    mBinding.imageVideoLayout.setOnClickListener(v -> {
                        if (chatCallbacks != null && !isSelectModeOn)
                            chatCallbacks.onImageClick(media_url);
                    });
                    GlideUtil.loadImage(mBinding.ivImage, media_url, R.drawable.photo_placeholder, 1000, 8);
                    return;
                }
                if (media_type.equals(VIDEO.value)) {
                    mBinding.imageVideoLayout.setVisibility(View.VISIBLE);
                    mBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                    mBinding.tvTimeStamp.setText(time);
                    mBinding.imageVideoLayout.setOnClickListener(v -> {
                        if (chatCallbacks != null && !isSelectModeOn)
                            chatCallbacks.onVideoClick(media_url);
                    });
                    GlideUtil.loadImage(mBinding.ivImage, media_url, R.drawable.photo_placeholder, 1000, 8);
                    return;
                }
                if (media_type.equals(PDF.value)) {
                    mBinding.layoutPdf.setVisibility(View.VISIBLE);
                    mBinding.tvDocTime.setText(time);
                    mBinding.layoutPdf.setOnClickListener((v) -> {
                        if (chatCallbacks != null && !isSelectModeOn) {
                            chatCallbacks.onPdfClick(media_url);
                        }
                    });
                    return;
                }
                if (media_type.equals(VOICE.value)) {
                    mBinding.chatAudioPlayerLayout.setVisibility(View.VISIBLE);
                    mBinding.tvDocTime.setText(time);
                    if (chat.getDuration() != null && TimeUtil.getTimeFromMillisecond(chat.getDuration()) != null)
                        mBinding.tvAudioDuration.setText(TimeUtil.getTimeFromMillisecond(chat.getDuration()));
                    if (!isSelectModeOn)
                        setAudioPlayer(mBinding.ivPlayPause, mBinding.progressBuffering, mBinding.audioSeekbar, chat);
                }
                if (media_type.equals(CALL.value)) {
                    mBinding.tvCallingStatus.setVisibility(View.VISIBLE);
                    mBinding.tvCallingStatus.setText(media_url);
                }
            }
        }

        private void bindMessage(Chat chat, ItemChatRightBinding mBinding) {
            mBinding.imageVideoLayout.setVisibility(View.GONE);
            mBinding.chatAudioPlayerLayout.setVisibility(View.GONE);
            mBinding.layoutPdf.setVisibility(View.GONE);
            mBinding.layoutTextMessage.setVisibility(View.GONE);
            mBinding.tvCallingStatus.setVisibility(View.GONE);
            String time = TimeUtil.getDateFromTimeStamp(chat.getTimestamp(), "hh:mm a");
            String media_url = chat.getMedia_url();
            String media_type = chat.getMedia_type();
            if (media_type != null && media_url != null) {

                if (isSelectModeOn) {
                    itemViewRight.shadowView.setVisibility(View.VISIBLE);
                    if (chatCallbacks != null)
                        chatCallbacks.updateToolbarTitle(String.valueOf(selectedChat.size()));
                    if (selectedChat.contains(chat)) {
                        mBinding.shadowView.setBackgroundColor(mBinding.getRoot().getContext().getResources().getColor(R.color.color_pale_golden_rod));
                    } else {
                        mBinding.shadowView.setBackground(null);
                    }
                    itemViewRight.shadowView.setOnClickListener(v -> {
                        if (!selectedChat.contains(chat)) {
                            mBinding.shadowView.setBackgroundColor(mBinding.getRoot().getContext().getResources().getColor(R.color.color_pale_golden_rod));
                            selectedChat.add(chat);
                        } else {
                            mBinding.shadowView.setBackground(null);
                            selectedChat.remove(chat);

                        }
                        if (chatCallbacks != null) {
                            if (selectedChat.size() == 0) {
                                chatCallbacks.onDeleteModeOn(false);
                                isSelectModeOn = false;
                                notifyDataSetChanged();
                            } else
                                chatCallbacks.updateToolbarTitle(String.valueOf(selectedChat.size()));
                        }
                    });
                } else {
                    itemViewRight.shadowView.setVisibility(View.GONE);
                }

                if (media_type.equals(TEXT.value)) {
                    mBinding.layoutTextMessage.setVisibility(View.VISIBLE);
                    mBinding.tvMessage.setText(media_url);
                    mBinding.tvTextTime.setText(time);
                    applyLongPressClick(mBinding.layoutTextMessage, itemViewRight, chat);
                    return;
                }
                if (media_type.equals(IMAGE.value)) {
                    mBinding.imageVideoLayout.setVisibility(View.VISIBLE);
                    mBinding.ivPlayIcon.setVisibility(View.GONE);
                    mBinding.tvTimeStamp.setText(time);
                    applyLongPressClick(mBinding.imageVideoLayout, itemViewRight, chat);
                    mBinding.imageVideoLayout.setOnClickListener(v -> {
                        chatItemClickListener(chat, IMAGE, itemViewRight);
                    });
                    GlideUtil.loadImage(mBinding.ivImage, media_url, R.drawable.photo_placeholder, 1000, 8);

                    return;
                }
                if (media_type.equals(VIDEO.value)) {
                    mBinding.imageVideoLayout.setVisibility(View.VISIBLE);
                    mBinding.ivPlayIcon.setVisibility(View.VISIBLE);
                    mBinding.tvTimeStamp.setText(time);
                    applyLongPressClick(mBinding.imageVideoLayout, itemViewRight, chat);

                    mBinding.imageVideoLayout.setOnClickListener(v -> {
                        chatItemClickListener(chat, VIDEO, itemViewRight);
                    });
                    GlideUtil.loadImage(mBinding.ivImage, media_url, R.drawable.photo_placeholder, 1000, 8);
                    return;
                }
                if (media_type.equals(PDF.value)) {
                    mBinding.layoutPdf.setVisibility(View.VISIBLE);
                    mBinding.tvDocTime.setText(time);
                    applyLongPressClick(mBinding.layoutPdf, itemViewRight, chat);
                    mBinding.layoutPdf.setOnClickListener((v) -> {
                        chatItemClickListener(chat, PDF, itemViewRight);
                    });
                    return;
                }
                if (media_type.equals(VOICE.value)) {
                    mBinding.chatAudioPlayerLayout.setVisibility(View.VISIBLE);
                    mBinding.tvAudioTime.setText(time);
                    if (chat.getDuration() != null && TimeUtil.getTimeFromMillisecond(chat.getDuration()) != null)
                        mBinding.tvAudioDuration.setText(TimeUtil.getTimeFromMillisecond(chat.getDuration()));
                    applyLongPressClick(itemViewRight.audioSeekbar, itemViewRight, chat);
                    applyLongPressClick(itemViewRight.ivPlayPause, itemViewRight, chat);
                    applyLongPressClick(itemViewRight.getRoot(), itemViewRight, chat);
                    setAudioPlayer(mBinding.ivPlayPause, mBinding.progressBuffering, mBinding.audioSeekbar, chat);

                }
                if (media_type.equals(CALL.value)) {
                    mBinding.tvCallingStatus.setVisibility(View.VISIBLE);
                    mBinding.tvCallingStatus.setText(media_url);
                }

            }
        }
    }

}
