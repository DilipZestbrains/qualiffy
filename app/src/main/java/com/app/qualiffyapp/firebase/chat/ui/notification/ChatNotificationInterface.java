package com.app.qualiffyapp.firebase.chat.ui.notification;

public interface ChatNotificationInterface {
    void onNotificationSend(int status, String msg);
}
