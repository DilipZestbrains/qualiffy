package com.app.qualiffyapp.firebase;

import android.content.Context;
import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.types.CallType;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.models.RequestBean;
import com.app.qualiffyapp.firebase.chat.ui.notification.ChatNotificationInterface;
import com.app.qualiffyapp.firebase.chat.ui.notification.ChatNotificationPresenter;
import com.app.qualiffyapp.utils.Utility;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;

public class Call {
    private static Call instance;
    private static ChatNotificationInterface chatNotificationInterface;
    private String name;
    private FirebaseUserType userType;
    private AppFirebaseDatabase appFirebaseDatabase;
    private ChatNotificationPresenter chatListPresenter;

    private Call() {
        int type = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), USER_TYPE);
        name = Utility.getStringSharedPreference(ApplicationController.getApplicationInstance(), FIRST_NAME);
        if (type == AppConstants.JOB_SEEKER)
            userType = FirebaseUserType.JOB_SEEKER;
        else userType = FirebaseUserType.BUSINESS;
        appFirebaseDatabase = AppFirebaseDatabase.getInstance();
        chatListPresenter = new ChatNotificationPresenter(chatNotificationInterface);
    }

    public static Call getInstance(ChatNotificationInterface chatNotificationInterface) {
        Call.chatNotificationInterface = chatNotificationInterface;
        if (instance == null)
            instance = new Call();

        return instance;
    }

    public void makeVideoCall(Context context, UserConnection connection) {
        String blockStatus = connection.getBlocked();
        String type = connection.getType();
        String connectionUid = connection.getUid();
        String userUid = Utility.getStringSharedPreference(context, USER_ID);
        if (blockStatus == null || blockStatus.equals(BlockStatus.NOT_BLOCKED.value)) {
            if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(context, USER_TYPE) == AppConstants.JOB_SEEKER)
                Utility.showToast(context, context.getResources().getString(R.string.you_cant_initiate_video_call));
            else appFirebaseDatabase.getUserCallStatus(connectionUid, isAvailable -> {
                if (isAvailable) {
                    sendCallNotification(CallType.VIDEO.value, connection.getUid(),connection.getType());
                    appFirebaseDatabase.startCall(userUid, connectionUid, CallType.VIDEO);
                } else {
                    Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

                }
            });
        } else
            Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

    }

    public void makeVideoCall(Context context, String blockStatus, String type, String connectionUid) {
        String userUid = Utility.getStringSharedPreference(context, USER_ID);
        if (blockStatus == null || blockStatus.equals(BlockStatus.NOT_BLOCKED.value)) {
            if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(context, USER_TYPE) == AppConstants.JOB_SEEKER)
                Utility.showToast(context, context.getResources().getString(R.string.you_cant_initiate_video_call));
            else appFirebaseDatabase.getUserCallStatus(connectionUid, isAvailable -> {
                if (isAvailable) {
                    sendCallNotification(CallType.VIDEO.value, connectionUid,type);
                    appFirebaseDatabase.startCall(userUid, connectionUid, CallType.VIDEO);
                } else {
                    Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

                }
            });
        } else
            Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

    }

    public void makeAudioCall(Context context, UserConnection connection) {
        String blockStatus = connection.getBlocked();
        String type = connection.getType();
        String connectionUid = connection.getUid();
        String userUid = Utility.getStringSharedPreference(context, USER_ID);
        if (blockStatus == null || blockStatus.equals(BlockStatus.NOT_BLOCKED.value)) {
            if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(context, USER_TYPE) == AppConstants.JOB_SEEKER)
                Utility.showToast(context, context.getResources().getString(R.string.you_cant_initiate_audio_call));
            else appFirebaseDatabase.getUserCallStatus(connectionUid, isAvailable -> {
                if (isAvailable) {
                    sendCallNotification(CallType.AUDIO.value, connection.getUid(),connection.getType());
                    appFirebaseDatabase.startCall(userUid, connectionUid, CallType.AUDIO);
                } else {
                    Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));
                }
            });
        } else
            Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

    }

    public void makeAudioCall(Context context, String blockStatus, String type, String connectionUid) {

        String userUid = Utility.getStringSharedPreference(context, USER_ID);
        if (blockStatus == null || blockStatus.equals(BlockStatus.NOT_BLOCKED.value)) {
            if (type.equals(FirebaseUserType.BUSINESS.s) && Utility.getIntFromSharedPreference(context, USER_TYPE) == AppConstants.JOB_SEEKER)
                Utility.showToast(context, context.getResources().getString(R.string.you_cant_initiate_audio_call));
            else appFirebaseDatabase.getUserCallStatus(connectionUid, isAvailable -> {
                if (isAvailable) {
                    sendCallNotification(CallType.AUDIO.value, connectionUid,type);
                    appFirebaseDatabase.startCall(userUid, connectionUid, CallType.AUDIO);
                } else {
                    Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));
                }
            });
        } else
            Utility.showToast(context, context.getResources().getString(R.string.other_person_is_busy_on_another_call));

    }

    private void sendCallNotification(String c_type, String connectionId, String connectionType) {
        RequestBean bean = new RequestBean();
        if (userType == FirebaseUserType.JOB_SEEKER) {
            bean.type = "7";
        } else {
            bean.type = "9";
        }

        bean.name = name;
        bean.c_type = c_type;
        chatListPresenter.sendNotification(bean, connectionId, connectionType);
    }


}
