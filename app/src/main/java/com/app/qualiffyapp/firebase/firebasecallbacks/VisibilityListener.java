package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.chat.types.Visible;

public interface VisibilityListener {
    void onGetVisibility(Visible visible);
}
