package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.ui.activities.signUpFlow.business.base.BaseInterface;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.interfaces.ToastType;

public interface FirebaseUserCallbacks extends BaseInterface {
    void showToast(ToastType toastType);

}
