package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.User;

public interface OnUserListener {

    void onFindUser(User user);
}
