package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.UserConnection;

public interface UserConnectionListener {

    void onConnectionFetch(UserConnection connection);
}
