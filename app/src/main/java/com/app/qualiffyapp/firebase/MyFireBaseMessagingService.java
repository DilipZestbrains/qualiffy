package com.app.qualiffyapp.firebase;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.firebase.chat.types.BlockStatus;
import com.app.qualiffyapp.firebase.chat.types.FirebaseUserType;
import com.app.qualiffyapp.firebase.chat.ui.chatMessaging.ChatListActivity;
import com.app.qualiffyapp.ui.activities.dashboard.DashboardActivity;
import com.app.qualiffyapp.ui.activities.profile.ManageNotificationActivity;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.JsManageNotificationActivity;
import com.app.qualiffyapp.ui.activities.profile.otherUser.OtherUserProfile;
import com.app.qualiffyapp.ui.activities.signUpFlow.business.presenter.BusinessLoginPresenter;
import com.app.qualiffyapp.utils.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;
import java.util.Random;

import static com.app.qualiffyapp.constants.AppConstants.BS_POS_NOTIFICATIONS;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.BUSINESS_USER;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_ID;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_NAME;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION_TYPE;
import static com.app.qualiffyapp.constants.AppConstants.IS_BLOCKED;
import static com.app.qualiffyapp.constants.AppConstants.IS_SHORTLIST;
import static com.app.qualiffyapp.constants.AppConstants.JOB_SEEKER;
import static com.app.qualiffyapp.constants.AppConstants.JS_POS_NOTIFICATIONS;
import static com.app.qualiffyapp.constants.AppConstants.POSITION;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;

public class MyFireBaseMessagingService extends FirebaseMessagingService {
    public static final String ORG_SHORT_USR = "1";
    public static final String USR_CHAT = "2";
    public static final String USR_CALLS = "3";
    public static final String ORG_CHAT = "4";
    public static final String ORG_CALLS = "5";
    public static final String ORG_FOLLOW_USR = "6";
    public static final String USR_FOLLOW_ORG = "7";
    public static final String USR_REC_ORG = "8";
    public static final String ORG_VIEW_USR = "9";
    public static final String FRIEND_REQUEST_SEND = "10";
    public static final String FIRND_REQUEST_ACCEPT = "11";
    public static final int CALL_NOTIFICATION_ID = 12345678;
    private static final long VIBRATOR_DURATION = 200;
    private static final String pushType = "push_type";
    private static final String from = "from_";
    private static final String callType = "c_type";
    private static final String timestamp = "";
    private static final String senderType = "from_by";
    private static final String senderName = "name";
    private static final String title = "title";
    private static final String body = "body";
    private static NotificationManager notificationManager;
    private boolean isCallNotification = false;

    public static void clearNotification(int id) {
        if (notificationManager != null)
            notificationManager.cancelAll();
    }

    @Override
    public void onNewToken(String token) {
        Utility.putStringValueInSharedPreference(this, AppConstants.PREFS_ACCESS_TOKEN, token);
    }

    //ORG_USER_FOLLOW //noti
    //ORG_USER_REC//noti
    //ORG_CHAT
    //ORG_CALLS

    //JS_ORG_FOLLOW //noti
    //JS_ORG_VIEW //noti
    //JS_MATCH_EXPIRE // applied job
    //JS_CHAT //chat screen
    //JS_CALLS //calling activity

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if (isAppIsInBackground(this) && (Utility.getIntFromSharedPreference(getApplicationContext(), USER_TYPE) == JOB_SEEKER || Utility.getIntFromSharedPreference(getApplicationContext(), USER_TYPE) == BUSINESS_USER)) {
            String type = remoteMessage.getData().get(pushType);
            String id = remoteMessage.getData().get(from);
            String name = remoteMessage.getData().get(senderName);
            String userType = remoteMessage.getData().get(senderType);
            Bundle b = new Bundle();
            if (remoteMessage.getData().get(callType) != null)
                isCallNotification = true;
            Intent intent = null;
            if (type != null)
                switch (type) {
                    case USR_CHAT:
                        Bundle bundle = new Bundle();
                        bundle.putString(CONNECTION_NAME, name);
                        bundle.putString(CONNECTION_ID, id);
                        bundle.putString(IS_BLOCKED, BlockStatus.NOT_BLOCKED.value);
                        bundle.putString(CONNECTION_TYPE, FirebaseUserType.JOB_SEEKER.s);
                        intent = new Intent(this, ChatListActivity.class);
                        intent.putExtra(BUNDLE, bundle);
                        break;
                    case ORG_CHAT:
                        Bundle bundle1 = new Bundle();
                        bundle1.putString(CONNECTION_NAME, name);
                        bundle1.putString(CONNECTION_ID, id);
                        bundle1.putString(IS_BLOCKED, BlockStatus.NOT_BLOCKED.value);
                        bundle1.putString(CONNECTION_TYPE, FirebaseUserType.BUSINESS.s);
                        intent = new Intent(this, ChatListActivity.class);
                        intent.putExtra(BUNDLE, bundle1);
                        break;
                    case USR_CALLS:
                    case ORG_CALLS:
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case USR_REC_ORG:
                        b.putInt(POSITION, BS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case USR_FOLLOW_ORG:
                        b.putInt(POSITION, BS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case ORG_SHORT_USR:
                        b.putString(IS_SHORTLIST, "true");
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case ORG_FOLLOW_USR:
                        b.putInt(POSITION, JS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case ORG_VIEW_USR:
                        b.putInt(POSITION, JS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                    case FIRND_REQUEST_ACCEPT:
//                        String userId1 = remoteMessage.getData().get(from);
//                        b.putString(USER_ID, userId1);
//                        intent = new Intent(this, ManageNotificationActivity.class);
//                        intent.putExtra(BUNDLE, b);
                        b.putInt(POSITION, JS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                    case FRIEND_REQUEST_SEND:
//                        Bundle b1 = new Bundle();
//                        String userId2 = remoteMessage.getData().get(from);
//                        b1.putString(USER_ID, userId2);
//                        intent = new Intent(this, ManageNotificationActivity.class);
//                        intent.putExtra(BUNDLE, b1);
                        b.putInt(POSITION, JS_POS_NOTIFICATIONS);
                        intent = new Intent(this, DashboardActivity.class);
                        break;
                }
            if (intent != null) {
                intent.putExtra(BUNDLE, b);
                sendNotification(remoteMessage, intent);

            }
        }


    }


    private void sendNotification(RemoteMessage remoteMessage, Intent intent) {
        if (notificationManager == null) {
            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }
        commonPushCreate(remoteMessage, intent);

    }

    private void commonPushCreate(RemoteMessage message, Intent intent) {
        String CHANNEL_ID = "2422AseA34235239jsa";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        String title = message.getData().get(MyFireBaseMessagingService.title);
        String body = message.getData().get(MyFireBaseMessagingService.body);
        int importance = 0;
        NotificationChannel mChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
        }
//        Intent intent = new Intent(this, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int requestCode = 0;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder noBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setContentText(body)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setPriority(Notification.PRIORITY_HIGH);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            noBuilder.setSmallIcon(R.drawable.ic_launcher_notify);
            noBuilder.setColor(getResources().getColor(R.color.colorPrimaryDark));
        } else {
            noBuilder.setSmallIcon(R.drawable.ic_launcher_notify);
        }

        message_alert(noBuilder, mChannel);
        int channelId;
        if (isCallNotification)
            channelId = CALL_NOTIFICATION_ID;
        else channelId = new Random().nextInt();
        notificationManager.notify(String.valueOf(10), channelId, noBuilder.build());
    }

    private void message_alert(NotificationCompat.Builder noBuilder, NotificationChannel mChannel) {
        AudioManager audio = (AudioManager) this.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
        assert audio != null;
        switch (audio.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                if (audio.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE) {
                    noBuilder.setVibrate(new long[]{0, VIBRATOR_DURATION});
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager.createNotificationChannel(mChannel);
                }
                break;
            case AudioManager.RINGER_MODE_SILENT:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager.createNotificationChannel(mChannel);
                }
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                noBuilder.setVibrate(new long[]{0, VIBRATOR_DURATION});
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager.createNotificationChannel(mChannel);
                }
                break;
        }

    }

    public int getIcon() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.ic_launcher;
        } else {
            return R.mipmap.ic_launcher;
        }
    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
            if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                for (String activeProcess : processInfo.pkgList) {
                    if (activeProcess.equals(context.getPackageName())) {
                        isInBackground = false;
                    }
                }
            }
        }
        return isInBackground;
    }
}

