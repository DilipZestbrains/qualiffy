package com.app.qualiffyapp.firebase.chat;

import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.firebase.chat.types.CallType;
import com.app.qualiffyapp.firebase.chat.types.MediaType;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnChatFetchListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnChatlistChangeListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnConnectionFetchListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnConnectionListChangeListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnIncomingChangeListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.OnUserListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.UserConnectionListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.UserIncomingChangeListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.UserIncomingStatusListener;
import com.app.qualiffyapp.firebase.firebasecallbacks.UserTypeCallbacks;
import com.app.qualiffyapp.firebase.model.Chat;
import com.app.qualiffyapp.firebase.model.User;
import com.app.qualiffyapp.firebase.model.UserConnection;
import com.app.qualiffyapp.utils.Utility;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import static com.app.qualiffyapp.constants.SharedPrefConstants.UID;

public class AppFirebaseDatabase {
    private static String userId;
    private static AppFirebaseDatabase appFirebaseDatabase;
    private final String TAG = AppFirebaseDatabase.class.getName();
    private final String USER = "user";
    private final String NAME = "name";
    private final String IMG = "img";
    private final String TYPE = "type";
    private final String ID = "uid";
    private final String PHONE = "phone";
    private final String CONNECTION = "connections";
    private final String CHAT = "chat";
    private final String INCOMING = "incoming";
    private final String CALLED_ID = "caller_id";
    private final String CALL_TYPE = "call_type";
    private DatabaseReference databaseReference;
    private OnConnectionListChangeListener connectionListUpdateListener;
    private OnChatlistChangeListener chatlistChangeListener;
    private OnIncomingChangeListener incomingChangeListener;
    private ChildEventListener connectionChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            if (connectionListUpdateListener != null)
                connectionListUpdateListener.onConnectionUpdate(dataSnapshot.getValue(UserConnection.class));
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
    private ValueEventListener incomingCallEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() == null)
                if (incomingChangeListener != null)
                    incomingChangeListener.disconnectCall();
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
    private ChildEventListener chatChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            if (chatlistChangeListener != null)
                chatlistChangeListener.onNewMessageArrival(dataSnapshot.getValue(Chat.class));

        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            if (chatlistChangeListener != null) {
                chatlistChangeListener.onMessageDeleted(dataSnapshot.getValue(Chat.class));
            }
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    };
    private UserIncomingChangeListener userCallStatusListener;
    private ValueEventListener callChildEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() != null) {
                User connection = dataSnapshot.getValue(User.class);
                if (connection != null)
                    if (userCallStatusListener != null && connection.getIncoming() != null && connection.getCall_type() != null && connection.getCaller_id() != null) {
                        getUserConnection(userId, connection.getIncoming(), c -> {
                                if(c!=null)
                                userCallStatusListener.onIncomingCallListener(connection, c);
                        });
                    }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.e(TAG, databaseError.getDetails());
        }
    };

    private AppFirebaseDatabase() {
        String DATABASE_NAME = "Qualiffy";
        databaseReference = FirebaseDatabase.getInstance().getReference(DATABASE_NAME);

    }

    public static AppFirebaseDatabase getInstance() {
        if (appFirebaseDatabase == null)
            appFirebaseDatabase = new AppFirebaseDatabase();
        userId = Utility.getStringSharedPreference(ApplicationController.getApplicationInstance(), UID);
        return appFirebaseDatabase;
    }


    public void deleteUser() {
        getUserRef(userId).removeValue();
    }

    //update user

    public void updateUser(String name,String img,String phone) {
        getUserRef(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user!=null) {
                        if (name != null)
                            getUserRef(userId).child(NAME).setValue(name);
                        if (phone!= null)
                            getUserRef(userId).child(PHONE).setValue(phone);
                        if (img != null)
                            getUserRef(userId).child(IMG).setValue(img);

                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void addFriend(UserConnection connection1, UserConnection connection2) {
        if (!TextUtils.isEmpty(connection1.getUid()) && !TextUtils.isEmpty(connection2.getUid())) {
            connection1.setNameLower(connection1.getName().toLowerCase());
            connection2.setNameLower(connection2.getName().toLowerCase());
            if (connection1.getTimestamp() != null)
                connection2.setTimestamp(connection1.getTimestamp());
            else if (connection2.getTimestamp() != null) {
                connection1.setTimestamp(connection2.getTimestamp());
            } else {
                long time = SystemClock.currentThreadTimeMillis();
                connection1.setTimestamp(String.valueOf(time));
                connection2.setTimestamp(String.valueOf(time));
            }
            getUserNodeRef().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(connection1.getUid())) {
                        User u =new User(connection1.getUid(), connection1.getName(), connection1.getType(), connection1.getImg(), connection1.getPhone());
                        registerUser(u);
                    }
                    getUserConnections(connection1.getUid()).child(connection2.getUid()).setValue(connection2);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
            getUserNodeRef().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(connection2.getUid())) {
                        User u =new User(connection1.getUid(), connection1.getName(), connection1.getType(), connection1.getImg(), connection1.getPhone());
                        registerUser(u);
                    }
                    getUserConnections(connection2.getUid()).child(connection1.getUid()).setValue(connection1);

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void clearPendingCalls() {
        getUserRef(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    if (user.getIncoming() != null) {
                        clearIncoming(user.getUid());
                        clearIncoming(user.getIncoming());
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //listen for chatUpdate
    public void listenUserChat(String connectionId, OnChatlistChangeListener chatlistChangeListener) {
        this.chatlistChangeListener = chatlistChangeListener;
        String chatId = getChatId(userId, connectionId);
        getUserChatsNodeRef(chatId).addChildEventListener(chatChildEventListener);
    }

    //mandatory to call after listener
    public void stopListeningChatUpdate(String connectionId) {
        chatlistChangeListener = null;
        getUserChatsNodeRef(getChatId(userId, connectionId)).removeEventListener(chatChildEventListener);
    }

    public void updateBlockStatus(String connectionUid, String blockStatus) {

        getUserConnections(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(connectionUid)) {
                    getUserConnection(userId, connectionUid).child("blocked").setValue(blockStatus);
                    getUserConnection(connectionUid, userId).child("blocked").setValue(blockStatus);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getDetails());
            }
        });

    }

    // get user connection List
    public void getConnections(String uid, OnConnectionFetchListener listener) {
        ArrayList<UserConnection> connections = new ArrayList<>();
        getUserConnections(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot c : dataSnapshot.getChildren()) {
                    UserConnection connection = c.getValue(UserConnection.class);
                    if (connection != null) {
                        connections.add(connection);
                    }
                }
                listener.onConnectionFetch(connections);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getDetails());
            }
        });
    }

    //get Chat List
    public void getChat(String connectionId, OnChatFetchListener listener) {
        ArrayList<Chat> chatList = new ArrayList<>();
        String chatId = getChatId(userId, connectionId);
        getUserChatsNodeRef(chatId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    chatList.add(d.getValue(Chat.class));
                }
                listener.onChatFetch(chatList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    //send Message
    public void sendMessage(String connectionId, @NonNull MediaType mediaType, String mediaUrl, String duration) {
        String timeStamp = Long.toString(System.currentTimeMillis());
        Chat chat = new Chat();
        chat.setMessage_id(getMessageId(timeStamp));
        chat.setMedia_type(mediaType.value);
        chat.setTimestamp(timeStamp);
        chat.setSender_id(userId);
        chat.setMedia_url(mediaUrl);
        chat.setDuration(duration);
        getUserChatsNodeRef(getChatId(userId, connectionId)).child(getMessageId(chat.getTimestamp())).setValue(chat);
        saveLastMessage(connectionId, mediaType.value, mediaUrl, timeStamp, true);
    }

    public void updateMessageCount(String connectionId) {
        getUserConnection(userId, connectionId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserConnection userConnection = dataSnapshot.getValue(UserConnection.class);
                if (userConnection != null) {
                    userConnection.setUnread_count("0");
                    getUserConnection(userId, connectionId).setValue(userConnection);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(AppFirebaseDatabase.class.getName(), databaseError.getDetails());
            }
        });

    }

    public void saveLastMessage(String connectionId, String mediaType, String url, String timestamp, boolean isNewMessage) {
        String lastMessage;
        if (mediaType.equals(MediaType.TEXT.value) || mediaType.equals(MediaType.CALL.value))
            lastMessage = url;
        else lastMessage = "Media";
        String finalMediaUrl = lastMessage;
        getUserConnection(userId, connectionId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserConnection userConnection = dataSnapshot.getValue(UserConnection.class);
                if (userConnection != null) {
                    userConnection.setMedia_type(mediaType);
                    userConnection.setMedia_url(finalMediaUrl);
                    userConnection.setTimestamp(timestamp);
                    userConnection.setUnread_count("0");
                    getUserConnection(userId, connectionId).setValue(userConnection);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(AppFirebaseDatabase.class.getName(), databaseError.getDetails());
            }
        });

        String finalMediaUrl1 = lastMessage;
        getUserConnection(connectionId, userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                UserConnection userConnection = dataSnapshot.getValue(UserConnection.class);
                if (userConnection != null) {
                    userConnection.setMedia_type(mediaType);
                    userConnection.setMedia_url(finalMediaUrl1);
                    userConnection.setTimestamp(timestamp);
                    if (isNewMessage) {
                        String unreadCount = userConnection.getUnread_count();
                        if (unreadCount == null)
                            unreadCount = "0";
                        try {
                            int count = Integer.parseInt(unreadCount);
                            unreadCount = String.valueOf((count + 1));
                        } catch (Exception e) {
                            unreadCount = "0";
                        }

                        userConnection.setUnread_count(unreadCount);
                    }
                    getUserConnection(connectionId, userId).setValue(userConnection);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(AppFirebaseDatabase.class.getName(), databaseError.getDetails());
            }
        });
    }

    //get connection status
    public void listenForConnectionUpdate(OnConnectionListChangeListener onConnectionListUpdateListener) {
        this.connectionListUpdateListener = onConnectionListUpdateListener;
        getUserConnections(userId).addChildEventListener(connectionChildEventListener);
    }

    public void stopListeningConnectionUpdate() {
        this.connectionListUpdateListener = null;
        getUserConnections(userId).removeEventListener(connectionChildEventListener);
    }

    //get messageId
    private String getMessageId(String timeStamp) {
        return timeStamp + "_" + userId;
    }

    //register new user
    public void registerUser(User user) {
        if (user != null&&user.getUid()!=null) {
            String uid = user.getUid();
            getUserNodeRef().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(uid)) {
                        getUserRef(uid).setValue(user);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(AppFirebaseDatabase.class.getName(), databaseError.getDetails());
                }
            });
        } else Log.e(AppFirebaseDatabase.class.getName(), "uid is null so can't register user");
    }

    public void getUserType(String uid, UserTypeCallbacks userTypeCallbacks) {
        getUserRef(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User u = dataSnapshot.getValue(User.class);
                if (u != null)
                    userTypeCallbacks.onGetUser(u);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public String getChatId(String userId, String connectionId) {
        if (userId.compareTo(connectionId) > 0) {
            return userId + "_" + connectionId;
        }
        if (userId.compareTo(connectionId) < 0) {
            return connectionId + "_" + userId;
        }
        return null;
    }

    public void deleteMessage(String connectionId, ArrayList<Chat> chats) {
        getUserChatsNodeRef(getChatId(userId, connectionId)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (Chat c : chats) {
                    if (dataSnapshot.hasChild(c.getMessage_id()))
                        getUserChatsNodeRef(getChatId(userId, connectionId)).child(c.getMessage_id()).removeValue();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private DatabaseReference getChatNodeRef() {
        return databaseReference.child(CHAT);
    }

    private DatabaseReference getUserChatsNodeRef(String chatId) {
        return getChatNodeRef().child(chatId);
    }

    private DatabaseReference getUserNodeRef() {
        return databaseReference.child(USER);
    }

    private DatabaseReference getUserRef(String id) {
        return getUserNodeRef().child(id);
    }

    public void getUser(String uid, OnUserListener userListener) {
        getUserRef(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue(User.class) != null) {
                    userListener.onFindUser(dataSnapshot.getValue(User.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public DatabaseReference getUserConnections(String userId) {
        return getUserRef(userId).child(CONNECTION);
    }

    private DatabaseReference getUserConnection(String userid, String connectionId) {
        return getUserConnections(userid).child(connectionId);
    }

    public FirebaseRecyclerOptions<UserConnection> getConnectionFirebaseOption(String uid) {
        return new FirebaseRecyclerOptions.Builder<UserConnection>()
                .setQuery(appFirebaseDatabase.getUserConnections(uid).orderByChild("timestamp"), UserConnection.class).build();
    }

    public FirebaseRecyclerOptions<UserConnection> getConnectionSearchFirebaseOption(String uid, String str) {
        return new FirebaseRecyclerOptions.Builder<UserConnection>()
                .setQuery(appFirebaseDatabase.getUserConnections(uid).orderByChild("nameLower").startAt(str.toLowerCase()).endAt(str.toLowerCase() + "\uf8ff"), UserConnection.class).build();
    }

    public void getUserConnection(String uid, String cid, UserConnectionListener userConnectionListener) {
        getUserConnection(uid, cid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    userConnectionListener.onConnectionFetch(dataSnapshot.getValue(UserConnection.class));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });

    }

    public void getUserCallStatus(String connectionID, UserIncomingStatusListener userStatusCallbacks) {
        getUserRef(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    String incoming = user.getIncoming();
                    if (incoming == null || TextUtils.isEmpty(incoming)) {
                        getUserRef(connectionID).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                User user = dataSnapshot.getValue(User.class);
                                if (user != null) {
                                    String incoming = user.getIncoming();
                                    if (incoming != null && !TextUtils.isEmpty(incoming)) {
                                        userStatusCallbacks.onGetUserIncomingStatus(false);
                                    } else {
                                        userStatusCallbacks.onGetUserIncomingStatus(true);
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    } else userStatusCallbacks.onGetUserIncomingStatus(false);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        getUserRef(connectionID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    String incoming = user.getIncoming();
                    if (incoming != null && !TextUtils.isEmpty(incoming)) {
                        userStatusCallbacks.onGetUserIncomingStatus(false);
                    } else {
                        userStatusCallbacks.onGetUserIncomingStatus(true);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void startCall(String uid, String cid, CallType callType) {
        getUserRef(uid).child(INCOMING).setValue(cid);
        getUserRef(cid).child(INCOMING).setValue(uid);
        getUserRef(uid).child(CALLED_ID).setValue(uid);
        getUserRef(cid).child(CALLED_ID).setValue(uid);
        getUserRef(uid).child(CALL_TYPE).setValue(callType.value);
        getUserRef(cid).child(CALL_TYPE).setValue(callType.value);
    }


    public void clearIncoming(String uid) {
        getUserRef(uid).child(INCOMING).removeValue();
        getUserRef(uid).child(CALL_TYPE).removeValue();
        getUserRef(uid).child(CALLED_ID).removeValue();
    }

    public void startListenForIncomingCalls(String uid, OnIncomingChangeListener onIncomingChangeListener) {
        this.incomingChangeListener = onIncomingChangeListener;
        getUserRef(uid).child(INCOMING).addValueEventListener(incomingCallEventListener);
    }

    public void stopListeningForIncomingCall(String uid) {
        this.incomingChangeListener = null;
        getUserRef(uid).child(INCOMING).removeEventListener(incomingCallEventListener);
    }

    public void setUserCallStatusListener(UserIncomingChangeListener userCallStatusListener) {
        this.userCallStatusListener = userCallStatusListener;
        getUserRef(userId).addValueEventListener(callChildEventListener);
    }

    public void removeUserCallStatusListener() {
        getUserRef(userId).removeEventListener(callChildEventListener);
        userCallStatusListener = null;
    }

    public static void clearFirebaseInstance() {
        appFirebaseDatabase = null;
    }

    public void updateConnection(UserConnection connection) {
        getUserConnection(userId, connection.getUid(), connection1 ->{
            if(connection1!=null)
            getUserRef(connection.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    if (user.getImg() != null)
                        connection1.setImg(user.getImg());
                    if (user.getPhone() != null)
                        connection1.setPhone(user.getPhone());
                    if (user.getName() != null)
                        connection1.setName(user.getName());
                    getUserConnection(userId, connection1.getUid()).setValue(connection1);
                } else {
                    //userDeleted
                    getUserConnection(userId, connection.getUid()).removeValue();
                    getUserChatsNodeRef(getChatId(userId, connection.getUid())).removeValue();
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });});
    }
}
