package com.app.qualiffyapp.firebase.firebasecallbacks;

import com.app.qualiffyapp.firebase.model.Chat;

public interface OnChatlistChangeListener {
    void onNewMessageArrival(Chat chat);

    void onMessageDeleted(Chat chat);
}
