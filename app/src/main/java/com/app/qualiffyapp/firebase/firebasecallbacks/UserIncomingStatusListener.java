package com.app.qualiffyapp.firebase.firebasecallbacks;

public interface UserIncomingStatusListener {
    void onGetUserIncomingStatus(boolean isAvailable);
}
