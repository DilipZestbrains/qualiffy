package com.app.qualiffyapp.models.profile.jobseeker;

import com.app.qualiffyapp.models.ExpInfoBean;

import java.util.ArrayList;

public class JobSeekerProfileResponse {
    public User usr;
    public JobSeekerProfile prfl;
    public ArrayList<String> crt;
    public ExpInfoBean expInfo;
    public String bP;
    public int fl_count;
    public int r_count;
    public int f_count;
    public int v_count;
}

