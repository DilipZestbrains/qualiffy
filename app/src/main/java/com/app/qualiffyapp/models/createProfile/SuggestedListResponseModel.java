package com.app.qualiffyapp.models.createProfile;

import com.google.gson.annotations.SerializedName;

public class SuggestedListResponseModel {
    /**
     * _id : 5cc7ff033a10ce4e9a60612b
     * name : accounting & finance
     */

    @SerializedName("_id")
    public String id;
    @SerializedName("name")
    public String name;

}
