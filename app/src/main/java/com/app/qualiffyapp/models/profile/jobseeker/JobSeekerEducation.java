package com.app.qualiffyapp.models.profile.jobseeker;

import android.os.Parcel;
import android.os.Parcelable;

public class JobSeekerEducation implements Parcelable {
    public static final Creator<JobSeekerEducation> CREATOR = new Creator<JobSeekerEducation>() {
        @Override
        public JobSeekerEducation createFromParcel(Parcel in) {
            return new JobSeekerEducation(in);
        }

        @Override
        public JobSeekerEducation[] newArray(int size) {
            return new JobSeekerEducation[size];
        }
    };
    public String _id;
    public String name;
    public String addr;
    public String sess;
    public String major;

    public JobSeekerEducation(Parcel in) {
        _id = in.readString();
        name = in.readString();
        addr = in.readString();
        sess = in.readString();
        major = in.readString();
    }

    public JobSeekerEducation() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(_id);
        dest.writeString(name);
        dest.writeString(addr);
        dest.writeString(sess);
        dest.writeString(major);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
