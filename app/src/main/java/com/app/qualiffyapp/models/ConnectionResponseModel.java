package com.app.qualiffyapp.models;

import java.util.ArrayList;

public class ConnectionResponseModel {
    public String msg;
    public int page;
    public int lmt;
    public int ttl;
    public ArrayList<User> usr;
    public String bP;

    class User {
        public String _id;
        public String username;
        public String f_name;
        public String l_name;
        public String img;
    }
}
