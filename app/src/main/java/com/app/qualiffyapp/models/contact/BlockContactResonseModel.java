package com.app.qualiffyapp.models.contact;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BlockContactResonseModel {

    /**
     * msg : User block list fetch successfully.
     * page : 1
     * lmt : 10
     * ttl : 1
     * usr : [{"_id":"5d0b68577a80c60b1f3c9199","pno":"8447324911","username":"@sj","f_name":"","l_name":"","img":"qu9ab42180a0204fe188f71fc823fc92b51561028771132.jpg"}]
     * bP : https://qu aliffy.s3.amazonaws.com/qualiffy_dev/user/profile/
     */

    @SerializedName("msg")
    public String msg;
    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;
    @SerializedName("users")
    public List<UsrBean> usr;

    public class UsrBean {
        /**
         * _id : 5d0b68577a80c60b1f3c9199
         * pno : 8447324911
         * username : @sj
         * f_name :
         * l_name :
         * img : qu9ab42180a0204fe188f71fc823fc92b51561028771132.jpg
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("pno")
        public String pno;
        @SerializedName("username")
        public String username;
        @SerializedName("f_name")
        public String fName;
        @SerializedName("l_name")
        public String lName;
        @SerializedName("img")
        public String img;
    }

}
