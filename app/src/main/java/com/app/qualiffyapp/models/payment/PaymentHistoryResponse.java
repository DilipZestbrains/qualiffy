package com.app.qualiffyapp.models.payment;

import java.util.ArrayList;

public class PaymentHistoryResponse {
    public String msg;
    public int page;
    public int lmt;
    public int ttl;
    public ArrayList<PaymentReceiptRequest> pmts;
}
