package com.app.qualiffyapp.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NotificationModel {

    @Expose
    @SerializedName("oP")
    private String op;
    @Expose
    @SerializedName("uP")
    private String up;
    @Expose
    @SerializedName("notif")
    private ArrayList<Notif> notif;
    @Expose
    @SerializedName("ttl")
    private int ttl;
    @Expose
    @SerializedName("lmt")
    private int lmt;
    @Expose
    @SerializedName("page")
    private int page;

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getUp() {
        return up;
    }

    public void setUp(String up) {
        this.up = up;
    }

    public ArrayList<Notif> getNotif() {
        return notif;
    }

    public void setNotif(ArrayList<Notif> notif) {
        this.notif = notif;
    }

    public int getTtl() {
        return ttl;
    }

    public void setTtl(int ttl) {
        this.ttl = ttl;
    }

    public int getLmt() {
        return lmt;
    }

    public void setLmt(int lmt) {
        this.lmt = lmt;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public static class Notif {
        @Expose
        @SerializedName("notif_msg")
        private String notifMsg;
        @Expose
        @SerializedName("data")
        private Data data;
        @Expose
        @SerializedName("created")
        private String created;
        @Expose
        @SerializedName("from_by")
        private int fromBy;
        @Expose
        @SerializedName("from")
        private String from;
        @Expose
        @SerializedName("type")
        private int type;
        @Expose
        @SerializedName("is_read")
        private int isRead;
        @Expose
        @SerializedName("_id")
        private String Id;
        public boolean isActionPerformed = false;

        public String getNotifMsg() {
            return notifMsg;
        }

        public void setNotifMsg(String notifMsg) {
            this.notifMsg = notifMsg;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public int getFromBy() {
            return fromBy;
        }

        public void setFromBy(int fromBy) {
            this.fromBy = fromBy;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public int getIsRead() {
            return isRead;
        }

        public void setIsRead(int isRead) {
            this.isRead = isRead;
        }

        public String getId() {
            return Id;
        }

        public void setId(String Id) {
            this.Id = Id;
        }
    }

    public static class Data {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("img")
        private String img;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }
    }
}
