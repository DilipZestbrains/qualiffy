package com.app.qualiffyapp.models.profile.jobseeker;

import com.app.qualiffyapp.models.home.business.OrgBean;

import java.util.List;

public class JsOwnRecommendResponseModel {


    public String msg;
    public Integer page;
    public Integer lmt;
    public Integer ttl;
    public String bP;
    public List<OrgBean> orgs;

}
