package com.app.qualiffyapp.models.payment;

public class PaymentCard extends BaseResponse {

    public String address_zip_check;

    public String country;

    public String last4;

    public String funding;

    public Object metadata;

    public String address_country;

    public String address_state;

    public String exp_month;

    public String exp_year;

    public String address_city;

    public String tokenization_method;

    public String cvc_check;

    public String address_line2;

    public String address_line1;

    public String fingerprint;

    public String name;

    public String id;

    public String address_line1_check;

    public String address_zip;

    public String dynamic_last4;

    public String brand;

    public String object;

    public boolean isSelected = false;
}
