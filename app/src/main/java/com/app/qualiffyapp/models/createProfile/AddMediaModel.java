package com.app.qualiffyapp.models.createProfile;

public class AddMediaModel {
    private String imgFile;

    private String img_id;

    private int drawable_img;


    public String getImgFile() {
        return imgFile;
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getImg_id() {
        return img_id;
    }

    public void setImg_id(String img_id) {
        this.img_id = img_id;
    }

    public int getDrawable_img() {
        return drawable_img;
    }

    public void setDrawable_img(int drawable_img) {
        this.drawable_img = drawable_img;
    }
}
