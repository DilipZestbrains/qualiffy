package com.app.qualiffyapp.models.createProfile;

import com.google.gson.annotations.SerializedName;

public class AddMediaResponseModel {

    @SerializedName("img")
    public String img_id;

    @SerializedName("bP")
    public String bP;

    @SerializedName("msg")
    public String msg;

    @SerializedName("uni")
    public String uni;


}
