package com.app.qualiffyapp.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class ExpDetailBean {
    @SerializedName("title")
    public String title;
    @SerializedName("no_job_hist")
    public boolean noJobHist;
    @SerializedName("is_wrkng")
    public boolean isWrkng;
    @SerializedName("c_name")
    public String cName;
    @SerializedName("from")
    public String from;
    @SerializedName("to")
    public String to;

    @SerializedName("desc")
    public String desc;

    @NonNull
    @Override
    public String toString() {
        return this.title + this.noJobHist + this.isWrkng + this.cName + this.cName + this.from + this.to + this.desc;
    }

    public boolean isEqual(ExpDetailBean b2) {
        if (b2 != null) {
            return this.toString().equals(b2.toString());
        } else return false;
    }
}
