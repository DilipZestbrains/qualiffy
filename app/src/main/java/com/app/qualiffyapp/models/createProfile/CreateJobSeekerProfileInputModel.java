package com.app.qualiffyapp.models.createProfile;

import com.app.qualiffyapp.models.ExpDetailBean;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class CreateJobSeekerProfileInputModel {

    private static CreateJobSeekerProfileInputModel mCreateJobSeekerProfileInputModel;

    public String jobC_id;
    public String myrole;
    public LinkedHashMap<String, String> roleSeek;
    public List<String> role_skng;
    public ArrayList<String> lngs;
    public ArrayList<String> skills;
    public ArrayList<String> job_type;
    public List<ExpDetailBean> exps;
    public boolean is_rem;
    public String mths;
    public String yrs;
    public List<String> certs;

    public String fname;
    public String lname;

    public boolean isSignup1Change;
    public boolean isSignup2Change;
    public boolean isExpChange;
    public boolean isCertChange;


    private CreateJobSeekerProfileInputModel() {
    }


    public static CreateJobSeekerProfileInputModel getInstance() {
        if (mCreateJobSeekerProfileInputModel == null)
            mCreateJobSeekerProfileInputModel = new CreateJobSeekerProfileInputModel();

        return mCreateJobSeekerProfileInputModel;
    }

    public static CreateJobSeekerProfileInputModel refreshInputModel() {
        if (mCreateJobSeekerProfileInputModel != null)
            mCreateJobSeekerProfileInputModel = null;

        mCreateJobSeekerProfileInputModel = new CreateJobSeekerProfileInputModel();

        return mCreateJobSeekerProfileInputModel;
    }

    public void clearInputModel() {
        if (mCreateJobSeekerProfileInputModel != null)
            mCreateJobSeekerProfileInputModel = null;
    }

}
