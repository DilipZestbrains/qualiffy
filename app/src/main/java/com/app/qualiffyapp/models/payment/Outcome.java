package com.app.qualiffyapp.models.payment;

public class Outcome {
    public Object reason;

    public String risk_level;

    public String risk_score;

    public String seller_message;

    public String network_status;

    public String type;
}
