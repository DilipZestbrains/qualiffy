package com.app.qualiffyapp.models.appliedJob;

import com.app.qualiffyapp.models.ExpDetailBean;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ApplicantsProfileResponseModel {


    /**
     * usr : {"username":"priya","pno":8700016445,"c_code":"91","s_type":0,"_id":"5d0885f014224334ad63a825","offline":false,"pub":true,"notif":true}
     * prfl : {"_id":"5d08862814224334ad63a826","lngs":["English","Hindi"],"skills":["Java","Android","Core java","Kotlin"],"job_type":"Full Time","imgs":["qu8499901b933dcdf9c133e35ba14b05941561038000951.jpg","qu1b3f1612e076dba275b31aa0dba668911561038010944.jpg","qu97c1d7ff8cf7ebf84f0c9aa1f5bb23321561038019527.jpg","qu546db20614e059afbf3d2e3a58862d3c1561038032841.jpg","qu0f84462e20c170b923f2b8d1073558441561038044973.jpg","qu68fb22c48927b63bd99825d9e3880bdf1561038056354.jpg","qu56a9b4503ea2e329b09536af13f7884c1561038071102.jpg","qu345b089d10c407b4f46f594eb7df07c61561038082651.jpg","quca0cece321c14d17ca48474dca1ce8d01561038112598.jpg","qu9f2cff45f8bbc30d3b8be5ee24efbeeb1561038123667.jpg"],"img":"qu02b9288aa2e23686b3490a279e0be78c1561037881897.jpg","video":"qua9e1fe3a8337b132c0210888c6ac352b1561036051519.mp4","f_name":"rd","l_name":"","loc":[77.3754384,28.6152175],"cntry":"India","address":"B-4/5, 1st Floor, B Block, Sector 63, Noida, Uttar Pradesh 201301, India","edu":[{"_id":"5d0b8db17a80c60b1f3c91c6","name":"CCSU","addr":"Ghaziabad","sess":"2010-2013"},{"_id":"5d0b8dc37a80c60b1f3c91c7","name":"UPTU","addr":"Ghaziabad","sess":"2013-2015"}],"reloc":true,"bio":"I am an Android Developer with having good knowledge of kotlin,Java and flutter.","jobCInfo":["ADMINISTRATIVE & CLERICAL"],"jobTInfo":["ADMINISTRATIVE NURSE"],"rolSkngInfo":["ADMINISTRATIVE NURSE","ASSOCIATE NURSE"]}
     * crt : ["Fuguf"]
     * expInfo : {"exps":["Xs Infosol","Netcommlabs Pvt ltd"],"mths":3,"yrs":3}
     * bP : https://qualiffy.s3.amazonaws.com/qualiffy_dev/user/profile/
     */

    @SerializedName("usr")
    public UsrBean usr;
    @SerializedName("prfl")
    public PrflBean prfl;
    @SerializedName("expInfo")
    public ExpInfoBean expInfo;
    @SerializedName("bP")
    public String bP;
    @SerializedName("crt")
    public List<String> crt;

    public class UsrBean {
        /**
         * username : priya
         * pno : 8700016445
         * c_code : 91
         * s_type : 0
         * _id : 5d0885f014224334ad63a825
         * offline : false
         * pub : true
         * notif : true
         */

        @SerializedName("username")
        public String username;
        @SerializedName("pno")
        public long pno;
        @SerializedName("c_code")
        public String cCode;
        @SerializedName("s_type")
        public int sType;
        @SerializedName("_id")
        public String id;
        @SerializedName("offline")
        public boolean offline;
        @SerializedName("pub")
        public boolean pub;
//        @SerializedName("notif")
//        public boolean notif;
    }

    public class PrflBean {
        /**
         * _id : 5d08862814224334ad63a826
         * lngs : ["English","Hindi"]
         * skills : ["Java","Android","Core java","Kotlin"]
         * job_type : Full Time
         * imgs : ["qu8499901b933dcdf9c133e35ba14b05941561038000951.jpg","qu1b3f1612e076dba275b31aa0dba668911561038010944.jpg","qu97c1d7ff8cf7ebf84f0c9aa1f5bb23321561038019527.jpg","qu546db20614e059afbf3d2e3a58862d3c1561038032841.jpg","qu0f84462e20c170b923f2b8d1073558441561038044973.jpg","qu68fb22c48927b63bd99825d9e3880bdf1561038056354.jpg","qu56a9b4503ea2e329b09536af13f7884c1561038071102.jpg","qu345b089d10c407b4f46f594eb7df07c61561038082651.jpg","quca0cece321c14d17ca48474dca1ce8d01561038112598.jpg","qu9f2cff45f8bbc30d3b8be5ee24efbeeb1561038123667.jpg"]
         * img : qu02b9288aa2e23686b3490a279e0be78c1561037881897.jpg
         * video : qua9e1fe3a8337b132c0210888c6ac352b1561036051519.mp4
         * f_name : rd
         * l_name :
         * loc : [77.3754384,28.6152175]
         * cntry : India
         * address : B-4/5, 1st Floor, B Block, Sector 63, Noida, Uttar Pradesh 201301, India
         * edu : [{"_id":"5d0b8db17a80c60b1f3c91c6","name":"CCSU","addr":"Ghaziabad","sess":"2010-2013"},{"_id":"5d0b8dc37a80c60b1f3c91c7","name":"UPTU","addr":"Ghaziabad","sess":"2013-2015"}]
         * reloc : true
         * bio : I am an Android Developer with having good knowledge of kotlin,Java and flutter.
         * jobCInfo : ["ADMINISTRATIVE & CLERICAL"]
         * jobTInfo : ["ADMINISTRATIVE NURSE"]
         * rolSkngInfo : ["ADMINISTRATIVE NURSE","ASSOCIATE NURSE"]
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("job_type")
        public ArrayList<String> jobType;
        @SerializedName("img")
        public String img;
        @SerializedName("video")
        public String video;
        @SerializedName("f_name")
        public String fName;
        @SerializedName("l_name")
        public String lName;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("address")
        public String address;
        @SerializedName("reloc")
        public boolean reloc;
        @SerializedName("bio")
        public String bio;
        @SerializedName("lngs")
        public List<String> lngs;
        @SerializedName("skills")
        public List<String> skills;
        @SerializedName("imgs")
        public List<String> imgs;
        @SerializedName("loc")
        public List<Double> loc;
        @SerializedName("edu")
        public List<EduBean> edu;
        @SerializedName("jobCInfo1")
        public List<String> jobCInfo;
        @SerializedName("jobTInfo1")
        public List<String> jobTInfo;
        @SerializedName("rolSkngInfo1")
        public List<String> rolSkngInfo;

        public class EduBean {
            /**
             * _id : 5d0b8db17a80c60b1f3c91c6
             * name : CCSU
             * addr : Ghaziabad
             * sess : 2010-2013
             */

            @SerializedName("_id")
            public String id;
            @SerializedName("name")
            public String name;
            @SerializedName("addr")
            public String addr;
            @SerializedName("sess")
            public String sess;
        }
    }

    public class ExpInfoBean {
        /**
         * exps : ["Xs Infosol","Netcommlabs Pvt ltd"]
         * mths : 3
         * yrs : 3
         */

        @SerializedName("mths")
        public int mths;
        @SerializedName("yrs")
        public int yrs;
        @SerializedName("exps")
        public List<ExpDetailBean> exps;
    }

}
