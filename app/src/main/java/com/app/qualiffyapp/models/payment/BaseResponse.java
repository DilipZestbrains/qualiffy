package com.app.qualiffyapp.models.payment;

public class BaseResponse {
    public Err error;

    public class Err {
        public String code;
        public String doc_url;
        public String message;
        public String param;
        public String type;
    }
}
