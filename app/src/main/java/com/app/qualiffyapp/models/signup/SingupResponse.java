package com.app.qualiffyapp.models.signup;

import com.google.gson.annotations.SerializedName;

public class SingupResponse {

    @SerializedName("name")
    public String username;
    @SerializedName("f_name")
    public String f_name;
    @SerializedName("l_name")
    public String l_name;
    @SerializedName("img")
    public String img;
    @SerializedName("msg")
    private String msg;
    @SerializedName("otp")
    private String otp;

    public String getMsg() {
        return msg;
    }

    public String getOtp() {
        return otp;
    }


}
