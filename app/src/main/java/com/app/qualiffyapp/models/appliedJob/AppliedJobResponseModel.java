package com.app.qualiffyapp.models.appliedJob;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppliedJobResponseModel {

    /**
     * page : 1
     * lmt : 10
     * ttl : 1
     * job : [{"_id":"5d107802a9af0e1319cbf4af","upld":true,"job_id":"5d1073cea9af0e1319cbf4ad","user_id":"5d0885f014224334ad63a825","org_id":"5cfe000712989837a893a3f0","video":"qu75c556924209344c78bf56f33401c2be1561360381503.mp4","orgInfo":{"_id":"5cfe000712989837a893a3f0","c_code":"91","imgs":[],"email":"vikas.kohli@brainmobi.com","pno":9149964057,"type":1,"address":"Braintree, MA, USA","bio":"dddd testttt dvxfcfv","cntry":"United States","img":"qud846e63db495c89f4e62bb174c64a8611560162392597","loc":[-71.00400130000003,42.20790170000001],"name":"vikas","state":"Massachusetts"},"jC_name":"ADMINISTRATIVE & CLERICAL","myr_name":"ADMINISTRATIVE NURSE"}]
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("job")
    public List<JobBean> job;
    @SerializedName("bP")
    public String bP;
@SerializedName("jP")
    public String jP;

    public static class JobBean {
        /**
         * _id : 5d107802a9af0e1319cbf4af
         * upld : true
         * job_id : 5d1073cea9af0e1319cbf4ad
         * user_id : 5d0885f014224334ad63a825
         * org_id : 5cfe000712989837a893a3f0
         * video : qu75c556924209344c78bf56f33401c2be1561360381503.mp4
         * orgInfo : {"_id":"5cfe000712989837a893a3f0","c_code":"91","imgs":[],"email":"vikas.kohli@brainmobi.com","pno":9149964057,"type":1,"address":"Braintree, MA, USA","bio":"dddd testttt dvxfcfv","cntry":"United States","img":"qud846e63db495c89f4e62bb174c64a8611560162392597","loc":[-71.00400130000003,42.20790170000001],"name":"vikas","state":"Massachusetts"}
         * jC_name : ADMINISTRATIVE & CLERICAL
         * myr_name : ADMINISTRATIVE NURSE
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("upld")
        public boolean upld;
        @SerializedName("job_id")
        public String jobId;
        @SerializedName("user_id")
        public String userId;
        @SerializedName("org_id")
        public String orgId;
        @SerializedName("video")
        public String video;
        @SerializedName("orgInfo")
        public OrgInfoBean orgInfo;
        @SerializedName("jC_name")
        public String jCName;
        @SerializedName("myr_name")
        public String myrName;
        /**
         * jobsInfo : {"_id":"5d1073cea9af0e1319cbf4ad","is_del":0,"jobC_id":"5d0cb12006164f0280b1206b","myrole":"5d0cb12006164f0280b12c5c","date":20190817,"prty":false,"cntry":"India","state":"Uttar Pradesh","address":"Block E, Sector 52, Noida, Uttar Pradesh 201301, India","exp":3,"slry":20000,"loc":[77.37277790000007,28.5867145],"org_id":"5cfe000712989837a893a3f0","created":"2019-06-24T06:55:10.249Z","updated":"2019-06-24T06:55:10.249Z"}
         */

        @SerializedName("jobsInfo")
        public JobsInfoBean jobsInfo;


        public static class OrgInfoBean {
            /**
             * _id : 5cfe000712989837a893a3f0
             * c_code : 91
             * imgs : []
             * email : vikas.kohli@brainmobi.com
             * pno : 9149964057
             * type : 1
             * address : Braintree, MA, USA
             * bio : dddd testttt dvxfcfv
             * cntry : United States
             * img : qud846e63db495c89f4e62bb174c64a8611560162392597
             * loc : [-71.00400130000003,42.20790170000001]
             * name : vikas
             * state : Massachusetts
             */

            @SerializedName("_id")
            public String id;
            @SerializedName("c_code")
            public String cCode;
            @SerializedName("email")
            public String email;
            @SerializedName("pno")
            public long pno;
            @SerializedName("type")
            public int type;
            @SerializedName("address")
            public String address;
            @SerializedName("bio")
            public String bio;
            @SerializedName("cntry")
            public String cntry;
            @SerializedName("img")
            public String img;
            @SerializedName("name")
            public String name;
            @SerializedName("state")
            public String state;
            @SerializedName("imgs")
            public List<?> imgs;
            @SerializedName("loc")
            public List<Double> loc;
        }


        public static class JobsInfoBean {
            /**
             * _id : 5d1073cea9af0e1319cbf4ad
             * is_del : 0
             * jobC_id : 5d0cb12006164f0280b1206b
             * myrole : 5d0cb12006164f0280b12c5c
             * date : 20190817
             * prty : false
             * cntry : India
             * state : Uttar Pradesh
             * address : Block E, Sector 52, Noida, Uttar Pradesh 201301, India
             * exp : 3
             * slry : 20000
             * loc : [77.37277790000007,28.5867145]
             * org_id : 5cfe000712989837a893a3f0
             * created : 2019-06-24T06:55:10.249Z
             * updated : 2019-06-24T06:55:10.249Z
             */

            @SerializedName("_id")
            public String id;
            @SerializedName("is_del")
            public int isDel;
            @SerializedName("jobC_id")
            public String jobCId;
            @SerializedName("myrole")
            public String myrole;
            @SerializedName("date")
            public String date;
            @SerializedName("prty")
            public boolean prty;
            @SerializedName("cntry")
            public String cntry;
            @SerializedName("state")
            public String state;
            @SerializedName("address")
            public String address;
            @SerializedName("exp")
            public int exp;
            @SerializedName("slry")
            public int slry;
            @SerializedName("org_id")
            public String orgId;
            @SerializedName("created")
            public String created;
            @SerializedName("updated")
            public String updated;
            @SerializedName("loc")
            public List<Double> loc;
        }
    }
}
