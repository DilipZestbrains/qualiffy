package com.app.qualiffyapp.models.promotion;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PromotionListResponseModel {

    @Expose
    @SerializedName("bP")
    private String bp;

    @Expose
    @SerializedName("prmtn")
    private Prmtn prmtn;
    @Expose
    @SerializedName("msg")
    private String msg;

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public Prmtn getPrmtn() {
        return prmtn;
    }

    public void setPrmtn(Prmtn prmtn) {
        this.prmtn = prmtn;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static class Prmtn {
        @Expose
        @SerializedName("created")
        private String created;
        @Expose
        @SerializedName("desc")
        private String desc;
        @Expose
        @SerializedName("_id")
        private String Id;
        @Expose
        @SerializedName("n_loc")
        private String nLoc;

        @Expose
        @SerializedName("org_id")
        private String orgId;
        @Expose
        @SerializedName("img")
        private String promotionImage;

        public String getPromotionImage() {
            return promotionImage;
        }

        public void setPromotionImage(String promotionImage) {
            this.promotionImage = promotionImage;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getId() {
            return Id;
        }

        public void setId(String id) {
            Id = id;
        }

        public String getnLoc() {
            return nLoc;
        }

        public void setnLoc(String nLoc) {
            this.nLoc = nLoc;
        }

        public String getOrgId() {
            return orgId;
        }

        public void setOrgId(String orgId) {
            this.orgId = orgId;
        }


    }
}
