package com.app.qualiffyapp.models.addedJob;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddedJobResponseModel {

    /**
     * page : 1
     * lmt : 10
     * ttl : 2
     * job : [{"_id":"5d10a0907ecafb2904dcbb85","jobC_name":"ADMINISTRATIVE & CLERICAL","jobT_name":"ADMINISTRATIVE NURSE","cnt":0},{"_id":"5d10a0b87ecafb2904dcbb86","jobC_name":"ADMINISTRATIVE & CLERICAL","jobT_name":"CLUB ADMINISTRATOR","cnt":0}]
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public String ttl;
    @SerializedName("job")
    public List<JobBean> job;

    public static class JobBean {
        /**
         * _id : 5d10a0907ecafb2904dcbb85
         * jobC_name : ADMINISTRATIVE & CLERICAL
         * jobT_name : ADMINISTRATIVE NURSE
         * cnt : 0
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("jobC_name")
        public String jobCName;
        @SerializedName("jobT_name")
        public String jobTName;
        @SerializedName("cnt")
        public int cnt;
        public String desc;
        @SerializedName("prty")
        public boolean prty;
    }

}
