package com.app.qualiffyapp.models;


import java.util.ArrayList;
import java.util.List;

public class RequestBean {
    public String username;
    public Object isLoggedIn;
    public Object number;
    public ArrayList<String> arr;
    public String pno;
    public String c_code;
    public String c_name;
    public String c_type;
    public String dev_tkn;
    public String email;
    public String pass;
    public Object self;
    public String imgUrl;
    public String n_loc;
    public String f_name;
    public String l_name;
    public String uid;
    public String isProfile;
    public List<String> job_type;
    public boolean is_reg;
    public boolean is_login;
    public ArrayList<String> skills;
    public ArrayList<String> lngs;
    public ArrayList<String> chat_media;


    //JobSeeker Recommend
    public Object isRecommend;
    public String orgid;
    public String job_id;


    //JobSeeker Profile
    public Object pub;
    public Object offline;
    public Object reloc;
    public Object notif;


    //Social Signup
    public Object type;
    public String social_id;

    public String name;
    public String bio;
    public String empls;
    public ArrayList<String> jobC;
    public String educationid;

    //address
    public String address;
    public String addr;
    public String sess;
    public String major;
    public String cntry;
    public String state;
    public String lng;
    public String lat;
    public String employStrength;
    //Add Job
    public String exp;
    public String slry;
    public Object prty;
    public String myrole;
    public String jobC_id;
    public String desc;
    public String date;
    public ArrayList<String> j_type;

    ////Notification
    public static class notify {
        public Object org_f;
        public Object org_v;
        public Object m_exp;
        public Object msg;
        public Object chat;
        public Object calls;
        public Object usr_f;
        public Object usr_rec;
        public Object shortl;

    }


}
