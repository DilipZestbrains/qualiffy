package com.app.qualiffyapp.models.appliedJob;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApplicantsResponseModel {

    /**
     * page : 1
     * lmt : 10
     * ttl : 1
     * job : [{"_id":"5d11fec498d39034e59049a0","status":1,"user_id":"5d0885f014224334ad63a825","created":"2019-06-25T11:00:20.507Z","video":"qua641ef7d13f1283ee37458ca36a0ef501561528349733.mp4","img":"qu02b9288aa2e23686b3490a279e0be78c1561037881897.jpg","f_name":"rd","l_name":""}]
     * bP : https://qualiffy.s3.amazonaws.com/qualiffy_dev/user/profile/
     * jP : https://qualiffy.s3.amazonaws.com/qualiffy_dev/user/job/
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;
    @SerializedName("jP")
    public String jP;
    @SerializedName("job")
    public List<UserBean> users;

    @SerializedName("jobsInfo")
    public JobsInfoBean jobsInfo;

    public class UserBean {

        @SerializedName("_id")
        public String id;
        @SerializedName("status")
        public int status;
        @SerializedName("user_id")
        public String userId;
        @SerializedName("created")
        public String created;
        @SerializedName("video")
        public String video;
        @SerializedName("img")
        public String img;
        @SerializedName("f_name")
        public String fName;
        @SerializedName("l_name")
        public String lName;
        @SerializedName("username")
        public String userName;


    }

    public class JobsInfoBean {


        @SerializedName("date")
        public String date;
        @SerializedName("prty")
        public boolean prty;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("state")
        public String state;
        @SerializedName("address")
        public String address;
        @SerializedName("exp")
        public String exp;
        @SerializedName("slry")
        public String slry;
        @SerializedName("loc")
        public List<Double> loc;
    }

}
