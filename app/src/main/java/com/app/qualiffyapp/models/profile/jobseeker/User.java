package com.app.qualiffyapp.models.profile.jobseeker;

import com.google.gson.annotations.SerializedName;

public class User {
    public String username;
    public String pno;
    public String c_code;
    public String s_type;
    public String _id;
    public boolean offline;
    public boolean pub;

    @SerializedName("notif")
    public NotifBean notif;

    public class NotifBean {
        @SerializedName("org_f")
        public boolean orgF;
        @SerializedName("org_v")
        public boolean orgV;
        @SerializedName("m_exp")
        public boolean mExp;
        @SerializedName("chat")
        public boolean chat;
        @SerializedName("calls")
        public boolean calls;
        public boolean shortl;
    }
}
