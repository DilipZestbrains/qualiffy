package com.app.qualiffyapp.models.payment;

public class CardResponseModel extends BaseResponse {
    public String livemode;

    public String created;

    public String client_ip;

    public String id;

    public String used;

    public String type;

    public PaymentCard card;

    public String object;


}
