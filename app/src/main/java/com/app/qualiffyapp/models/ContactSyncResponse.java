package com.app.qualiffyapp.models;

import com.app.qualiffyapp.localDatabase.entity.Contact;

import java.util.ArrayList;

public class ContactSyncResponse {
    private String bP;
    private ArrayList<Contact> users;

    public String getbP() {
        return bP;
    }

    public void setbP(String bP) {
        this.bP = bP;
    }

    public void setUsers(ArrayList<Contact> users) {
        this.users = users;
    }

    public ArrayList<Contact> getSyncedContacts(ArrayList<Contact> contacts) {
        ArrayList<Contact> allContacts = new ArrayList<>();
        if (contacts != null && users != null) {
            for (Contact c : contacts) {
                boolean isAlsoUser = false;
                for (Contact u : users) {
                    if (c.getPno().contains(u.getPno())) {
                        if (bP != null&&!u.getImg().startsWith(bP))
                            u.setImg(bP + u.getImg());
                        allContacts.add(0, u);
                        isAlsoUser = true;
                        break;
                    }
                }
                if (!isAlsoUser)
                    allContacts.add(c);
            }
        }
        return allContacts;
    }
}
