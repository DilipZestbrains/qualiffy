package com.app.qualiffyapp.models.createProfile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IndustryResponseModel extends MsgResponseModel {
    /**
     * page : 1
     * lmt : 5
     * ttl : 2
     * jobC : [{"_id":"5cc7ff033a10ce4e9a60612b","name":"accounting & finance"},{"_id":"5cd547c026f32d5de338757c","name":"accounting & finance"}]
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;

    //for Choose industary
    @SerializedName("jobC")
    public List<SuggestedListResponseModel> jobC;


    //for User Role and Role Seeking For
    @SerializedName("jobT")
    public List<SuggestedListResponseModel> jobT;


}


