package com.app.qualiffyapp.models.recruiter;

import java.util.List;

public class RecruiterListResponseModel {
    public Integer page;
    public Integer lmt;
    public Integer ttl;
    public String msg;
    public List<RecrBean> recr;


  public  class RecrBean {
        public String _id;
        public String c_code;
        public String name;
        public String email;
        public String pno;
    }

}
