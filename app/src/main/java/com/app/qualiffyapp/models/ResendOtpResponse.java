package com.app.qualiffyapp.models;

import com.google.gson.annotations.SerializedName;

public class ResendOtpResponse {

    @SerializedName("msg")
    public String msg;

    @SerializedName("is_ver")
    public int isVerified;
}
