package com.app.qualiffyapp.models.home.jobSeeker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsJobsResponseModel {
    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;
    public String pP;
    @SerializedName("job")
    public List<JobBean> job;

   /* flag for isExp
0=>not expire,
1=>expire,
  2=>no subscription
    plan*/


    @SerializedName("isExp")
    public int isSubscribe;
    public String cus_id;
    public List<Promotions> prmt;

    public class Promotions {
        public String _id;
        public String n_loc;
        public String desc;
        public String img;
        public String org_id;

        @SerializedName("fl_count")
        public String f_count;
    }

    public class JobBean {
        public String desc;

        @SerializedName("job_id")
        public String id;
        @SerializedName("date")
        public String date;
        @SerializedName("prty")
        public boolean prty;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("state")
        public String state;
        @SerializedName("address")
        public String address;
        @SerializedName("org_id")
        public String orgId;
        @SerializedName("exp")
        public int exp;
        @SerializedName("slry")
        public String slry;
        @SerializedName("distance")
        public double distance;
        @SerializedName("orgInfo")
        public OrgInfoBean orgInfo;


        @SerializedName("jobC_name")
        public String jobCName;
        @SerializedName("jobT_name")
        public String jobTName;

        @SerializedName("loc")
        public List<Double> loc;

        @SerializedName("rec")
        public Integer rec;

        @SerializedName("unrec")
        public Integer unrec;


        @SerializedName("of_cnt")
        public String followCnt;

        //         0=> not recommend, 1=> recommend, 2=> none
        @SerializedName("is_recmd")
        public int is_recmd;

        public class OrgInfoBean {

            @SerializedName("address")
            public String address;
            @SerializedName("bio")
            public String bio;
            @SerializedName("cntry")
            public String cntry;
            @SerializedName("img")
            public String img;
            @SerializedName("name")
            public String name;

            @SerializedName("r_name")
            public String r_name;
            @SerializedName("state")
            public String state;
            @SerializedName("imgs")
            public List<String> imgs;
            @SerializedName("loc")
            public List<Double> loc;
            public String video;


            public String pno;
            public String jobTitle;
            public String rec;
            public String unrec;
            public String flwCnt;
            public boolean priority;
            public int isRec;
            public String mOrgId;


        }
    }

}
