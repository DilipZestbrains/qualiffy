package com.app.qualiffyapp.models.profile.jobseeker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JsSubscriptionModel {

    /**
     * page : 1
     * lmt : 10
     * ttl : 5
     * subs : [{"_id":"5cf61c2db2c6c92a4682e6a7","price":1000,"name":"Ranjan","duration":3},{"_id":"5cf61c90b2c6c92a4682e6a9","price":1222,"name":"Ranjan","duration":2},{"_id":"5cf7bcdbf354a7384114c4e4","price":1,"name":"fdffdfdf","duration":2},{"_id":"5cf61710b2c6c92a4682e6a1","price":123,"name":"testing","duration":5},{"_id":"5cf61edcb2c6c92a4682e6b3","price":1212,"name":"www","duration":5}]
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("subs")
    public List<SubsBean> subs;

    public static class SubsBean {
        /**
         * _id : 5cf61c2db2c6c92a4682e6a7
         * price : 1000
         * name : Ranjan
         * duration : 3
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("price")
        public String price;
        @SerializedName("name")
        public String name;
        @SerializedName("duration")
        public int duration;
        @SerializedName("details")
        public String details;
        public int status;
        public boolean my_plan;
    }

}
