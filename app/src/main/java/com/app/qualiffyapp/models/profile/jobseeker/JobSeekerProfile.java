package com.app.qualiffyapp.models.profile.jobseeker;

import com.app.qualiffyapp.models.createProfile.JobInfo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JobSeekerProfile {
    public String _id;
    public ArrayList<String> lngs;
    public ArrayList<String> skills;
    public ArrayList<String> job_type;
    public LinkedList<String> imgs;
    public String img;
    public String video;
    public String f_name;
    public String l_name;
    public ArrayList<Double> loc;
    public String cntry;
    public String state;
    public String address;
    public ArrayList<JobSeekerEducation> edu;
    public boolean reloc;
    public String bio;
    public ArrayList<JobInfo> jobCInfo;
    public List<JobInfo> jobTInfo;
    public ArrayList<JobInfo> rolSkngInfo;
}

