package com.app.qualiffyapp.models.profile;

import com.app.qualiffyapp.models.ExpDetailBean;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OtherUserProfileResponseModel {

    /**
     * usr : {"username":"Meghna","pno":9267946036,"c_code":"91","s_type":0,"_id":"5d087a1d14224334ad63a817","offline":false,"pub":true,"notif":true}
     * prfl : {"_id":"5d087ad814224334ad63a818","lngs":["English"],"skills":["Skill"],"job_type":"Full Time","imgs":["qubc0abc31f4d80f5adb1685969bb11e4b1560836882552.jpg","qu8743d314b5e833d7732700c4f48f73831561467497272.jpg","qu60bbbc431f6688b2f6ea8947d6f0a78a1561467506439.jpg","qu5624942a62fc51d8b7e41ee27679342e1561467514575.jpg","qufa4e4790b5786b7ed8a2ad6c42afad9f1561467527247.jpg"],"img":"qu4edecb841a8d656977999d56c0dd915f1560836876143.jpg","video":"qu3a9d0420bdd7fde35c24527af8a71b551560837196840.mp4","f_name":"Meghna","l_name":"","loc":[77.3754374,28.6152534],"cntry":"India","address":"B-4/5, 1st Floor, B Block, Sector 63, Noida, Uttar Pradesh 201301, India","edu":[],"reloc":true,"bio":"","jobCInfo":["ADMINISTRATIVE & CLERICAL"],"jobTInfo":["ADMINISTRATIVE NURSE"],"rolSkngInfo":["CLUB ADMINISTRATOR","HEALTH ADMINISTRATIVE"]}
     * crt : ["Certificate"]
     * expInfo : {"exps":["Developer"],"mths":5,"yrs":4}
     * isFriend : 0
     * bP : https://qualiffy.s3.amazonaws.com/qualiffy_dev/user/profile/
     */

    @SerializedName("usr")
    public UsrBean usr;
    @SerializedName("prfl")
    public PrflBean prfl;
    @SerializedName("expInfo")
    public ExpInfoBean expInfo;


    /* user is_frnd
  if 0 => not send friend request,
 1=> accept friend request,
 2=> request send by other user,
 3 => request send by you*/
    @SerializedName("is_frnd")
    public int isFriend;


    public boolean is_bdge;

    @SerializedName("bP")
    public String bP;

    @SerializedName("is_block")
    public String is_block;


    @SerializedName("crt")
    public List<String> crt;

    public class UsrBean {
        /**
         * username : Meghna
         * pno : 9267946036
         * c_code : 91
         * s_type : 0
         * _id : 5d087a1d14224334ad63a817
         * offline : false
         * pub : true
         * notif : true
         */

        @SerializedName("username")
        public String username;
        @SerializedName("pno")
        public String pno;
        @SerializedName("c_code")
        public String cCode;
        @SerializedName("s_type")
        public int sType;
        @SerializedName("_id")
        public String id;
        @SerializedName("offline")
        public boolean offline;
        @SerializedName("pub")
        public boolean pub;
        @SerializedName("notif")
        public NotifBean notif;
    }

    public class PrflBean {
        /**
         * _id : 5d087ad814224334ad63a818
         * lngs : ["English"]
         * skills : ["Skill"]
         * job_type : Full Time
         * imgs : ["qubc0abc31f4d80f5adb1685969bb11e4b1560836882552.jpg","qu8743d314b5e833d7732700c4f48f73831561467497272.jpg","qu60bbbc431f6688b2f6ea8947d6f0a78a1561467506439.jpg","qu5624942a62fc51d8b7e41ee27679342e1561467514575.jpg","qufa4e4790b5786b7ed8a2ad6c42afad9f1561467527247.jpg"]
         * img : qu4edecb841a8d656977999d56c0dd915f1560836876143.jpg
         * video : qu3a9d0420bdd7fde35c24527af8a71b551560837196840.mp4
         * f_name : Meghna
         * l_name :
         * loc : [77.3754374,28.6152534]
         * cntry : India
         * address : B-4/5, 1st Floor, B Block, Sector 63, Noida, Uttar Pradesh 201301, India
         * edu : []
         * reloc : true
         * bio :
         * jobCInfo : ["ADMINISTRATIVE & CLERICAL"]
         * jobTInfo : ["ADMINISTRATIVE NURSE"]
         * rolSkngInfo : ["CLUB ADMINISTRATOR","HEALTH ADMINISTRATIVE"]
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("job_type")
        public ArrayList<String> jobType;
        @SerializedName("img")
        public String img;
        @SerializedName("video")
        public String video;
        @SerializedName("f_name")
        public String fName;
        @SerializedName("l_name")
        public String lName;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("address")
        public String address;
        public String state;
        @SerializedName("reloc")
        public boolean reloc;
        @SerializedName("bio")
        public String bio;
        @SerializedName("lngs")
        public List<String> lngs;
        @SerializedName("skills")
        public List<String> skills;
        @SerializedName("imgs")
        public List<String> imgs;
        @SerializedName("loc")
        public List<Double> loc;
        @SerializedName("edu")
        public List<EduBean> edu;
        @SerializedName("jobCInfo")
        public List<String> jobCInfo;
        @SerializedName("jobTInfo")
        public List<String> jobTInfo;
        @SerializedName("rolSkngInfo")
        public List<String> rolSkngInfo;
    }

    public class ExpInfoBean {
        /**
         * exps : ["Developer"]
         * mths : 5
         * yrs : 4
         */

        @SerializedName("mths")
        public int mths;
        @SerializedName("yrs")
        public int yrs;
        @SerializedName("exps")
        public List<ExpDetailBean> exps;
    }

    public class EduBean {
        /**
         * _id : 5d0b8db17a80c60b1f3c91c6
         * name : CCSU
         * addr : Ghaziabad
         * sess : 2010-2013
         */

        @SerializedName("_id")
        public String id;
        @SerializedName("name")
        public String name;
        @SerializedName("addr")
        public String addr;
        @SerializedName("sess")
        public String sess;
    }


}
