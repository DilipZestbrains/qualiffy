package com.app.qualiffyapp.models.home.business;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrgBean {
    @SerializedName("_id")
    public String id;
    
    @SerializedName("org_id")
    public String org_id;

    @SerializedName("c_code")
    public String cCode;
    @SerializedName("name")
    public String name;
    @SerializedName("email")
    public String email;
    @SerializedName("pno")
    public String pno;
    @SerializedName("bio")
    public String bio;
    @SerializedName("cntry")
    public String cntry;
    @SerializedName("state")
    public String state;
    @SerializedName("address")
    public String address;
    @SerializedName("type")
    public int type;
    @SerializedName("img")
    public String img;
    @SerializedName("imgs")
    public List<String> imgs;
    @SerializedName("loc")
    public List<Double> loc;

}
