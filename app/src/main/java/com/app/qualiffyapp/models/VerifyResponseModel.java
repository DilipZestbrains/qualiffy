package com.app.qualiffyapp.models;

import com.google.gson.annotations.SerializedName;

public class VerifyResponseModel {

    @SerializedName("code")
    public int code;
    @SerializedName("msg")
    public String msg;
    @SerializedName("acsTkn")
    public String acsTkn;

    @SerializedName("is_ver")
    public int isVerified;

    @SerializedName("_id")
    public String _id;

    @SerializedName("is_video")
    public boolean is_video;

    public String pno;

    @SerializedName("expInfo")
    public ExpInfoBean expInfoBean;


    // for Business Login
    @SerializedName("name")
    public String name;
    @SerializedName("otp")
    public String otp;

    @SerializedName("type")
    public int orgType;


    // Jobseeker Signup
    @SerializedName("username")
    public String username;
    @SerializedName("f_name")
    public String f_name;
    @SerializedName("l_name")
    public String l_name;

    @SerializedName("img")
    public String img;


}
