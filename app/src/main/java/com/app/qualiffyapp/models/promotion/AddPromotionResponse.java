package com.app.qualiffyapp.models.promotion;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddPromotionResponse {
    @Expose
    @SerializedName("msg")
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
