package com.app.qualiffyapp.models.createProfile;

import com.google.gson.annotations.SerializedName;

public class MsgResponseModel {

    /**
     * msg : Jobseeker details updated successfully.
     */

    @SerializedName("msg")
    public String msg;

}
