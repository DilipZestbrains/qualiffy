package com.app.qualiffyapp.models.profile;

import com.google.gson.annotations.SerializedName;

public class NotifBean {

    @SerializedName("org_f")
    public boolean orgF;
    @SerializedName("org_v")
    public boolean orgV;
    @SerializedName("usr_f")
    public boolean usr_f;
    @SerializedName("usr_rec")
    public boolean usr_rec;
    @SerializedName("chat")
    public boolean chat;
    @SerializedName("calls")
    public boolean calls;

}