package com.app.qualiffyapp.models.home.jobSeeker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrgJobListResponseModel {
    /**
     * page : 1
     * lmt : 10
     * ttl : 1
     * job : [{"job_id":"5d0cc3b2a9af0e1319cbf4a5","org_id":"5cfe000712989837a893a3f0","exp":2,"slry":2345,"date":20190630,"prty":true,"loc":[77.37277790000007,28.5867145],"cntry":"India","state":"Uttar Pradesh","address":"Block E, Sector 52, Noida, Uttar Pradesh 201301, India","distance":3183.5578517807503,"orgInfo":{"imgs":[],"address":"Braintree, MA, USA","bio":"dddd testttt dvxfcfv","cntry":"United States","img":"qud846e63db495c89f4e62bb174c64a8611560162392597","loc":[-71.00400130000003,42.20790170000001],"name":"vikas","state":"Massachusetts"},"jobC_name":"OTHERS","jobT_name":"AAA"}]
     * bP : https://qualiffy.s3.amazonaws.com/qualiffy_dev/org/profile/
     */

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;
    @SerializedName("job")
    public List<JobBean> job;

    public static class JobBean {
        /**
         * job_id : 5d0cc3b2a9af0e1319cbf4a5
         * org_id : 5cfe000712989837a893a3f0
         * exp : 2
         * slry : 2345
         * date : 20190630
         * prty : true
         * loc : [77.37277790000007,28.5867145]
         * cntry : India
         * state : Uttar Pradesh
         * address : Block E, Sector 52, Noida, Uttar Pradesh 201301, India
         * distance : 3183.5578517807503
         * orgInfo : {"imgs":[],"address":"Braintree, MA, USA","bio":"dddd testttt dvxfcfv","cntry":"United States","img":"qud846e63db495c89f4e62bb174c64a8611560162392597","loc":[-71.00400130000003,42.20790170000001],"name":"vikas","state":"Massachusetts"}
         * jobC_name : OTHERS
         * jobT_name : AAA
         */

        @SerializedName("job_id")
        public String jobId;
        @SerializedName("org_id")
        public String orgId;
        @SerializedName("exp")
        public String exp;
        @SerializedName("slry")
        public String slry;
        @SerializedName("date")
        public String date;
        @SerializedName("prty")
        public boolean prty;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("state")
        public String state;
        @SerializedName("address")
        public String address;
        @SerializedName("distance")
        public double distance;

        @SerializedName("jobC_name")
        public String jobCName;

        @SerializedName("jobT_name")
        public String jobTName;
        @SerializedName("cnt")
        public int applicantCount;


        @SerializedName("loc")
        public List<Double> loc;

    }

}
