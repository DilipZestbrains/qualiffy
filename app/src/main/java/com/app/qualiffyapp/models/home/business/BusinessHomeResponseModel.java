package com.app.qualiffyapp.models.home.business;

import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BusinessHomeResponseModel {


    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    public String cus_id;
    @SerializedName("bP")
    public String bp;

    /* 0=>not expire,
       1=>expire,
       2=>no subscription plan*/
    @SerializedName("isExp")
    public int subscribeFlag;

    public boolean isJobAdd;

    @SerializedName("job")
    public List<JobBean> job;


    public static class JobBean {


        @SerializedName("_id")
        public String id;

        @SerializedName("job_id")
        public String job_id;


        @SerializedName("u_name")
        public String uName;
        @SerializedName("usrPrfl")
        public UsrPrflBean usrPrfl;
        @SerializedName("expInfo")
        public ExpInfoBean expInfo;
        @SerializedName("jC_name")
        public String jCName;
        @SerializedName("myr_name")
        public String myrName;
        @SerializedName("v_count")
        public String v_count;
        @SerializedName("f_count")
        public String f_count;
        @SerializedName("bdgs_cnt")
        public String bdgs_cnt;
        @SerializedName("is_bdge")
        public boolean is_bdge;


        @SerializedName("edu")
        public List<EduBean> edu;


        public class UsrPrflBean {

            @SerializedName("_id")
            public String id;

            @SerializedName("bio")
            public String bio;


            @SerializedName("user_id")
            public String userId;
            @SerializedName("created")
            public String created;
            @SerializedName("f_name")
            public String fName;
            @SerializedName("jobC_id")
            public String jobCId;
            @SerializedName("l_name")
            public String lName;
            @SerializedName("myrole")
            public String myrole;
            @SerializedName("reloc")
            public boolean reloc;
            @SerializedName("updated")
            public String updated;
            @SerializedName("video")
            public String video;
            @SerializedName("job_type")
            public List<String> jobType;
            @SerializedName("img")
            public String img;
            @SerializedName("address")
            public String address;
            @SerializedName("cntry")
            public String cntry;
            @SerializedName("state")
            public String state;
            @SerializedName("edu")
            public List<EduBean> education;
            @SerializedName("imgs")
            public List<String> imgs;
            @SerializedName("lngs")
            public List<String> lngs;
            @SerializedName("loc")
            public List<Double> loc;
            @SerializedName("role_skng")
            public List<String> roleSkng;
            @SerializedName("skills")
            public List<String> skills;

        }


      /*  public class ExpInfoBean {
            *//**
             * exps : ["Certified from Brainmobi Technologies"]
             * mths : 2
             * yrs : 5
             *//*

            @SerializedName("mths")
            public int mths;
            @SerializedName("yrs")
            public int yrs;
            @SerializedName("exps")
            public List<ExpDetailBean> exps;
        }*/

        public class EduBean {
            /**
             * _id : 5d0b790b7a80c60b1f3c91a7
             * name : IGNOU
             * addr : Delhi
             * sess : 2018-2019
             */

            @SerializedName("_id")
            public String id;
            @SerializedName("name")
            public String name;
            @SerializedName("addr")
            public String addr;
            @SerializedName("sess")
            public String sess;

        }
    }

}
