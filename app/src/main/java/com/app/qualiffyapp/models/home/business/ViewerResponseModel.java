package com.app.qualiffyapp.models.home.business;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ViewerResponseModel {

    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;
    @SerializedName("org")
    public List<OrgBean> org;


}
