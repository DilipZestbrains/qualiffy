package com.app.qualiffyapp.models.home.jobSeeker;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecommendResponseModel {


    @SerializedName("msg")
    public String msg;
    @SerializedName("page")
    public int page;
    @SerializedName("lmt")
    public int lmt;
    @SerializedName("ttl")
    public int ttl;
    @SerializedName("bP")
    public String bP;

    @SerializedName("org_bP")
    public String org_bP;

    @SerializedName("usr_bP")
    public String usr_bP;


    @SerializedName("usr")
    public List<UsrBean> usr;

    @SerializedName("bdgs")
    public List<Badges> bdgs;

    public class Badges {

        public String _id;
        public String other_id;
        public Integer type;
        public DataBean data;


    }
    public static class DataBean {
        public String img;
        public String name;
    }

    public  class UsrBean {

        @SerializedName("user_id")
        public String userId;
        @SerializedName("f_name")
        public String fName;
        @SerializedName("l_name")
        public String lName;
        @SerializedName("username")
        public String username;
        @SerializedName("img")
        public String img;
    }

}
