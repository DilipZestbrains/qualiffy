package com.app.qualiffyapp.models.profile;

import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class BusinessProfileResponseModel {

    @SerializedName("msg")
    public String msg;
    @SerializedName("org")
    public OrgBean org;
    @SerializedName("bP")
    public String bP;

    public int r_count;
    public int unr_count;
     public int fl_count;

    public static class OrgBean {
        public boolean offline;

        @SerializedName("c_code")
        public String cCode;
        @SerializedName("_id")
        public String id;
        @SerializedName("name")
        public String name;
        @SerializedName("email")
        public String email;
        @SerializedName("pno")
        public long pno;
        @SerializedName("type")
        public int type;
        @SerializedName("bio")
        public String bio;
        @SerializedName("cntry")
        public String cntry;
        @SerializedName("state")
        public String state;
        @SerializedName("address")
        public String address;
        @SerializedName("reg")
        public String reg;
        @SerializedName("img")
        public String img;
        @SerializedName("loc")
        public List<Double> loc;
        @SerializedName("imgs")
        public LinkedList<String> imgs;
        @SerializedName("jobCInfo")
        public List<JobCInfoBean> jobCInfo;
        public String empls;
        public String video;


        @SerializedName("notif")
        public NotifBean notif;

        public static class JobCInfoBean {

            @SerializedName("_id")
            public String id;
            @SerializedName("name")
            public String name;
        }


    }


}
