package com.app.qualiffyapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExpInfoBean {
    /**
     * exps : ["Developer"]
     * mths : 5
     * yrs : 4
     */

    @SerializedName("yrs")
    public int yrs;
    @SerializedName("exps")
    public List<ExpDetailBean> exps;

}



