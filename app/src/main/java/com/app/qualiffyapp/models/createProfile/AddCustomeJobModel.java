package com.app.qualiffyapp.models.createProfile;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddCustomeJobModel {

    @Expose
    @SerializedName("jobTInfo")
    public Job job;

    public static class Job {
        @Expose
        @SerializedName("name")
        public String name;
        @Expose
        @SerializedName("_id")
        public String _id;
    }
}
