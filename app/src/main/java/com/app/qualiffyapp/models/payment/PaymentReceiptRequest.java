package com.app.qualiffyapp.models.payment;

public class PaymentReceiptRequest {
    public String subsid;
    public int amount;
    public String chargeId;
    public boolean paid;
    public String currency;
    public String customer;
    public String receipt_url;
    public int amountRefunded;
    public String description;
    public String status;
    public String name;
    public String card_id;
    public long created;
}
