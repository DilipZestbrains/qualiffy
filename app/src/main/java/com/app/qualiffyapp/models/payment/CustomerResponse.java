package com.app.qualiffyapp.models.payment;

import java.util.ArrayList;

public class CustomerResponse extends BaseResponse {

    public Subscriptions subscriptions;

    public String account_balance;

    public String livemode;

    public Source sources;

    public Subscriptions tax_ids;

    public String description;

    public String discount;

    public String tax_info_verification;

    public String[] preferred_locales;

    public String balance;

    public String shipping;

    public String delinquent;

    public String currency;

    public String id;

    public String email;

    public Invoice_settings invoice_settings;

    public String address;

    public String default_source;

    public String invoice_prefix;

    public String tax_exempt;

    public String created;

    public String tax_info;

    public String phone;

    public String name;

    public String object;


    private class Subscriptions {
        public String[] data;

        public String total_count;

        public String has_more;

        public String url;

        public String object;
    }

    public class Source {
        public ArrayList<PaymentCard> data;

        public String total_count;

        public String has_more;

        public String url;

        public String object;
    }


    private class Invoice_settings {
        public String footer;

        public String custom_fields;

        public String default_payment_method;
    }
}