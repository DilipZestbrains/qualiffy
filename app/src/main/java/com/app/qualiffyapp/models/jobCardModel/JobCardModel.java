package com.app.qualiffyapp.models.jobCardModel;

import org.jetbrains.annotations.NotNull;

public final class JobCardModel {

    private static long counter = 0;
    @NotNull
    private final long id;
    @NotNull
    private final String name;
    @NotNull
    private final String city;
    @NotNull
    private final String url;


    public JobCardModel(@NotNull String name, @NotNull String city, @NotNull String url) {
        super();
        id = counter++;
        this.name = name;
        this.city = city;
        this.url = url;
    }

    @NotNull
    public final String getName() {
        return this.name;
    }

    public long getId() {
        return id;
    }

    @NotNull
    public final String getCity() {
        return this.city;
    }

    @NotNull
    public final String getUrl() {
        return this.url;
    }

    @NotNull
    public String toString() {
        return "JobCardModel(name=" + this.name + ", city=" + this.city + ", url=" + this.url + ")";
    }
}