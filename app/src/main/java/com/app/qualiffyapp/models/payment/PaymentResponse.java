package com.app.qualiffyapp.models.payment;

public class PaymentResponse extends BaseResponse {
//    public String balance_transaction;
//
//    public String metadata;

//    public String livemode;
//
//    public String destination;

    public String description;

//    public String failure_message;
//
//    public String fraud_details;

    public PaymentCard source;

    public int amount_refunded;

//    public Refunds refunds;
//
//    public String statement_descriptor;
//
//    public String transfer_data;

    public String receipt_url;

//    public String shipping;
//
//    public String review;

//    public String captured;

    public String currency;

    //    public String refunded;
//
    public String id;

    public Outcome outcome;

//    public String payment_method;
//
//    public String order;
//
//    public String dispute;

    public int amount;

    public String failure_code;
//
//    public String transfer_group;
//
//    public String on_behalf_of;

    public int created;

//    public Payment_method_details payment_method_details;

//    public String source_transfer;
//
//    public String receipt_number;

    public String application;

//    public String receipt_email;

    public boolean paid;
//
//    public String application_fee;
//
//    public String payment_intent;
//
//    public String invoice;
//
//    public String application_fee_amount;

    public String object;

    public String customer;

    public String status;
}
