package com.app.qualiffyapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseModel<T> {

    @SerializedName("err")
    public ErrBean err;

    @SerializedName("status")
    private int status;

    @SerializedName("res")
    private T response;

    public int getStatus() {
        return status;
    }

    public T getRes() {
        return response;
    }


    public static class ErrBean {
        @SerializedName("errCode")
        public int errCode;
        @SerializedName("msg")
        public String msg;

        public ArrayList<ErrorArray> errs;


    }

    public static class ErrorArray {
       public String fieldName;
        public String message;
    }

}
