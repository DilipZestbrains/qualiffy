package com.app.qualiffyapp.audioPlayer;

public interface AudioPlayerCallbacks {
    void setBufferingState(Boolean isBuffering);

    void onStartAudio(int duration);

    void onStopAudio();

    void onPause();

    void onResumeAudio();

    void updateTime(int mili);
}
