package com.app.qualiffyapp.audioPlayer;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

public class ChatAudioPlayer {
    private static ChatAudioPlayer instance;
    private MediaPlayer mediaPlayer;
    private AudioPlayerCallbacks audioPlayerCallbacks;
    private boolean isPaused = false;
    private String mediaUrl;
    private boolean isLoading = false;
    private Handler handler;
    private Runnable runnable;

    private ChatAudioPlayer() {
        initMediaPlayer();
    }

    public static ChatAudioPlayer getInstance() {
        if (instance == null)
            instance = new ChatAudioPlayer();
        return instance;
    }


    private void initMediaPlayer() {
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    public void startAudio(String url, AudioPlayerCallbacks callbacks) {
        if (mediaPlayer.isPlaying() || isPaused) {
            mediaUrl = url;
            stopAudio();
        }
        handler = new Handler();
        this.audioPlayerCallbacks = callbacks;
        new Player(callbacks).execute(url);
    }

    public void stopAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
            isPaused = false;
            if (audioPlayerCallbacks != null) {
                audioPlayerCallbacks.onStopAudio();
            }
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable);
            }

        }
    }

    public void pauseAudio() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            audioPlayerCallbacks.onPause();
            isPaused = true;
        }
    }

    public boolean isSameMedia(String url) {
        if (mediaUrl != null)
            return mediaUrl.equals(url);
        else return false;
    }

    public void resumeAudio() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
            updateTime();
            isPaused = false;
            audioPlayerCallbacks.onResumeAudio();

        }
    }

    public boolean isPaused() {
        return isPaused;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public boolean isPlaying() {
        if (mediaPlayer != null)
            return mediaPlayer.isPlaying();
        else return false;
    }

    public void seekToPosition(int position) {
        if (mediaPlayer != null)
            mediaPlayer.seekTo(position);
    }

    public int getCurrentMili() {
        if (mediaPlayer != null)
            return mediaPlayer.getCurrentPosition();
        else return 0;
    }

    private void updateTime() {
        audioPlayerCallbacks.updateTime(mediaPlayer.getCurrentPosition());
        runnable = () -> {
            if (mediaPlayer.isPlaying()) {
                updateTime();
            }
        };
        handler.postDelayed(runnable, 100);

    }

    private class Player extends AsyncTask<String, Void, Boolean> {
        private AudioPlayerCallbacks callbacks;

        Player(AudioPlayerCallbacks callbacks) {
            this.callbacks = callbacks;
        }

        @Override
        protected void onPreExecute() {
            isLoading = true;
            callbacks.setBufferingState(true);
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.setOnCompletionListener(mp -> {
                    audioPlayerCallbacks.updateTime(mediaPlayer.getCurrentPosition());
                    stopAudio();
                });
                mediaPlayer.prepare();

            } catch (Exception e) {
                Log.e(ChatAudioPlayer.class.getName(), e.getMessage());
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            callbacks.setBufferingState(false);
            callbacks.onStartAudio(mediaPlayer.getDuration());
            updateTime();
            mediaPlayer.start();
            isLoading = false;

        }
    }


}
