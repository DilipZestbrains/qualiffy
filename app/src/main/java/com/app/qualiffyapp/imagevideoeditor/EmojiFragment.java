package com.app.qualiffyapp.imagevideoeditor;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.imagevideoeditor.videoEditor.VideoEditorView;

import java.util.ArrayList;
import java.util.Collections;



public class EmojiFragment extends Fragment implements EmojiAdapter.OnEmojiClickListener {

    public RecyclerView emojiRecyclerView;
    private ArrayList<String> emojiIds;
    private Activity photoEditorActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        photoEditorActivity = (Activity) getActivity();

        String[] emojis = photoEditorActivity.getResources().getStringArray(R.array.photo_editor_emoji);

        emojiIds = new ArrayList<>();
        Collections.addAll(emojiIds, emojis);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_photo_edit_emoji, container, false);

        emojiRecyclerView = rootView.findViewById(R.id.fragment_main_photo_edit_emoji_rv);
        emojiRecyclerView.setHasFixedSize(true);
        emojiRecyclerView.setLayoutManager(new GridLayoutManager(photoEditorActivity, 4));
        EmojiAdapter adapter = new EmojiAdapter(photoEditorActivity, emojiIds);
        adapter.setOnEmojiClickListener(this);
        emojiRecyclerView.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onEmojiClickListener(String emojiName) {
        if (photoEditorActivity != null) {
            String className = photoEditorActivity.getClass().getName();
              if(className.equals("com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity"))
            {
                PhotoEditorActivity photoEditor=(PhotoEditorActivity)photoEditorActivity;
                photoEditor.addEmoji(emojiName);
            }else {
                VideoEditorView videoEditorView = (VideoEditorView) photoEditorActivity;
                videoEditorView.addEmoji(emojiName);
            }

        }
//        photoEditorActivity.photoEditor.addEmoji(emojiName);
    }
}
