package com.app.qualiffyapp.imagevideoeditor.filters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.zomato.photofilters.SampleFilters;
import com.zomato.photofilters.imageprocessors.Filter;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;


public class FilterActivity extends AppCompatActivity implements ThumbnailCallback {
    static {
        System.loadLibrary("NativeImageProcessor");
    }

    private Activity activity;
    private RecyclerView thumbListView;
    private ImageView placeHolderImageView;
    private Bitmap receivedBitmap;
    private String selectedImagePath;
    private TextView tvCancel, tvDone;
    private String imageName;
//    private Bitmap processbitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        selectedImagePath = getIntent().getStringExtra("imagePathKey");
        imageName = getIntent().getStringExtra("name");
        activity = this;
        initUIWidgets();
    }

    private void initUIWidgets() {
        thumbListView = findViewById(R.id.thumbnails);
        tvCancel = findViewById(R.id.tv_cancel_filter);
        tvDone = findViewById(R.id.tv_done_filter);
        placeHolderImageView = findViewById(R.id.place_holder_imageview);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;
        options.outHeight = placeHolderImageView.getHeight();
        options.outWidth = placeHolderImageView.getWidth();
        receivedBitmap = Bitmap.createBitmap(BitmapFactory.decodeFile(selectedImagePath, options));
        placeHolderImageView.setImageBitmap(receivedBitmap);
        initHorizontalList();
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                String path = saveImage("PhotoClicked", imageName, placeHolderImageView);
                intent.putExtra("imagePathKey", path);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean isSDCARDMounted() {
        String status = Environment.getExternalStorageState();
        return status.equals(Environment.MEDIA_MOUNTED);
    }

    public String saveImage(String folderName, String imageName, ImageView view) {
        String selectedOutputPath = "";
        if (isSDCARDMounted()) {
            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), folderName);
            // Create a storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("PhotoClicked", "Failed to create directory");
                }
            }
            // Create a media file name

//            String imgName="IMG_" + System.currentTimeMillis() + ".jpg";
            selectedOutputPath = mediaStorageDir.getPath() + File.separator + imageName;
            Log.d("PhotoClicked", "selected camera path " + selectedOutputPath);
            File file = new File(selectedOutputPath);
            try {
                FileOutputStream out = new FileOutputStream(file);
                if (view != null) {
                    view.setDrawingCacheEnabled(true);
                    view.getDrawingCache().compress(Bitmap.CompressFormat.PNG, 100, out);
                }
                out.flush();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return selectedOutputPath;
    }

    private void initHorizontalList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        thumbListView.setLayoutManager(layoutManager);
        thumbListView.setHasFixedSize(true);
        bindDataToAdapter();
    }

    private void bindDataToAdapter() {
        final Context context = this.getApplication();
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                options.outHeight = placeHolderImageView.getHeight();
                options.outWidth = placeHolderImageView.getWidth();
                Bitmap thumbImage =
                        Bitmap.createBitmap(BitmapFactory.decodeFile(selectedImagePath, options));
                ThumbnailItem t1 = new ThumbnailItem();
                ThumbnailItem t2 = new ThumbnailItem();
                ThumbnailItem t3 = new ThumbnailItem();
                ThumbnailItem t4 = new ThumbnailItem();
                ThumbnailItem t5 = new ThumbnailItem();
                ThumbnailItem t6 = new ThumbnailItem();

                t1.image = thumbImage;
                t2.image = thumbImage;
                t3.image = thumbImage;
                t4.image = thumbImage;
                t5.image = thumbImage;
                t6.image = thumbImage;
                ThumbnailsManager.clearThumbs();
                ThumbnailsManager.addThumb(t1); // Original Image

                t2.filter = SampleFilters.getStarLitFilter();
                ThumbnailsManager.addThumb(t2);

                t3.filter = SampleFilters.getBlueMessFilter();
                ThumbnailsManager.addThumb(t3);

                t4.filter = SampleFilters.getAweStruckVibeFilter();
                ThumbnailsManager.addThumb(t4);

                t5.filter = SampleFilters.getLimeStutterFilter();
                ThumbnailsManager.addThumb(t5);

                t6.filter = SampleFilters.getNightWhisperFilter();
                ThumbnailsManager.addThumb(t6);

                List<ThumbnailItem> thumbs = ThumbnailsManager.processThumbs(context);

                ThumbnailsAdapter adapter = new ThumbnailsAdapter(thumbs, (ThumbnailCallback) activity);
                thumbListView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        };
        handler.post(r);
    }

    @Override
    public void onThumbnailClick(Filter filter) {
       /* processbitmap = filter.processFilter(Bitmap.createScaledBitmap(receivedBitmap, 1, 640,
                false));*/
        placeHolderImageView.setImageBitmap(filter.processFilter(Bitmap.createScaledBitmap(receivedBitmap, placeHolderImageView.getWidth(), placeHolderImageView.getHeight(), false)));
    }
}