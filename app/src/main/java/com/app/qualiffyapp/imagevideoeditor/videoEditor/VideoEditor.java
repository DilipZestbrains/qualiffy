package com.app.qualiffyapp.imagevideoeditor.videoEditor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.app.qualiffyapp.callbacks.VideoProcessingCallback;
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.FFmpeg;
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException;
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException;

import java.io.File;

public class VideoEditor {
    private static final String TAG = "VideoEditor";
    private FFmpeg ffmpeg;
    private String filePath;
    private VideoProcessingCallback callback;

    public VideoEditor(VideoProcessingCallback callback) {
        this.callback = callback;
    }

    /**
     * Load FFmpeg binary
     *
     * @param context
     */
    public void loadFFMpegBinary(final Activity context) {
        try {
            if (ffmpeg == null) {
                ffmpeg = FFmpeg.getInstance(context);
            }
            ffmpeg.loadBinary(new LoadBinaryResponseHandler() {
                @Override
                public void onFailure() {
                    showUnsupportedExceptionDialog(context);
                }

                @Override
                public void onSuccess() {
                    Log.d(TAG, "ffmpeg : correct Loaded");
                }
            });
        } catch (FFmpegNotSupportedException e) {
            showUnsupportedExceptionDialog(context);
        } catch (Exception e) {
            Log.d(TAG, "EXception no controlada : " + e);
        }
    }

    public void showUnsupportedExceptionDialog(final Activity context) {
        new AlertDialog.Builder(context)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Not Supported")
                .setMessage("Device Not Supported")
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();
                    }
                })
                .create()
                .show();

    }

    public void drawTextCommandCommand(Activity context, String videoPath, String imgPath, ProgressDialog progressDialog) {
//        File moviesDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        String filePrefix = "fade_video";
        String fileExtn = ".mp4";
        String yourRealPath = getPath(context, Uri.parse(videoPath));


        File moviesDir = Environment.getExternalStorageDirectory();
        File dir = new File(moviesDir.getAbsolutePath() + "/" + "qualiffy" + "/");

        File dest = new File(dir, filePrefix + fileExtn);
        int fileNo = 0;
        while (dest.exists()) {
            fileNo++;
            dest = new File(moviesDir, filePrefix + fileNo + fileExtn);
        }
        String filter = "yadif=0:-1:0, scale=400:226, drawtext=helloooooooooooooo: text=tod- %X':x=(w-text_w)/2:y=H-60 :fontcolor=white :box=1:boxcolor=0x00000000@1";
//        String codec = "-vcodec libx264  -pix_fmt yuv420p -b:v 700k -r 25 -maxrate 700k -bufsize 5097k";

        String var = "ffmpeg -f lavfi -i color=c=black:s=1280x720 -i video.mp4 -shortest -filter_complex \"[1:v]chromakey=0x70de77:0.1:0.2[ckout];[0:v][ckout]overlay[out]\" -map \"[out]\" output.mkv";
        Log.d(TAG, "startTrim: src: " + yourRealPath);
        Log.d(TAG, "startTrim: dest: " + dest.getAbsolutePath());
        filePath = dest.getAbsolutePath();
        //Command for adding water mar on video
//        String[] complexCommand = {"-i", videoPath, "-vf", "movie=" + imagePath + " [watermark]; [in][watermark] overlay=main_w-overlay_w-10:1 [out]", "-strict", "experimental", filePath};
//        String[] complexCommand = {"-i", videoPath, "-i", videoPath, "-i", videoPath, "-filter_complex", "[0:v][1:v]setpts=PTS-STARTPTS,overlay=20:40[bg]; [bg][2:v]setpts=PTS-STARTPTS,overlay=(W-w)/2:(H-h)/2[v]; [1:a][2:a]amerge=inputs=2[a]", "-map", "[v]", "-map", "[a]", "-ac", "2", filePath};
//        String[] complexCommand = new String[]{"-y", "-i", videoPath, "ultrafast", "-r", "16",
//                 "-i", imgPath, "-filter_complex", "[1:v] fade=out:st=30:d=1:alpha= 1 [ov]; " +
//                "[0:v][ov]overlay=10:10 [v]", "-map", "[v]", "-map 0:a -c:v libx264 -c:a copy " +
//                "-shortest", filePath};

        String[] complexCommand = new String[]{"-y", "-i", videoPath, "-i", imgPath,
                "-filter_complex", "[0:v]pad=iw:2*trunc(iw*16/9/2):(ow-iw)/2:(oh-ih)/2[v0];" +
                "[1:v][v0]scale2ref[v1][v0];[v0][v1]overlay=x=(W-w)/2:y=(H-h)/2[v]", "-r", "12", "-map", "[v]", "-map", "0:a?", "-ac", "2", filePath};

        execFFmpegBinary(complexCommand, progressDialog);

//        command_line=(ffmpeg -i "$in_file" -vf "$filter" "$codec" -an $out_file")
//        String[] complexCommand = {"-y", "-i", yourRealPath, "-s", "160x120", filePath};
//        String[] complexCommand = {"-y", "-f","lavfi","-i","color=c=black:s=120x80","-i", yourRealPath,"-shortest","-filter_complex","[1:v]chromakey=0x70de77:0.1:0.2[ckout];[0:v][ckout]overlay[out]","-map","[out]", filePath};
//        String[] complexCommand = {"-y", "-i", yourRealPath, "-acodec", "copy", "-vf", "fade=t=in:st=0:d=5,fade=t=out:st=" + String.valueOf(duration - 5) + ":d=5", filePath};

    }


    /**
     * Executing ffmpeg binary
     */
    private void execFFmpegBinary(final String[] command, final ProgressDialog progressDialog) {
        try {
            ffmpeg.execute(command, new ExecuteBinaryResponseHandler() {
                @Override
                public void onFailure(String s) {
                    Log.d(TAG, "FAILED with output : " + s);
                }

                @Override
                public void onSuccess(String s) {
                    Log.d(TAG, "SUCCESS with output : " + s);
                    callback.onVideoProcessingSuccess(filePath);
                }

                @Override
                public void onProgress(String s) {
                    Log.d(TAG, "Started command : ffmpeg " + command);
                    Log.d(TAG, "progress : " + s);
                    callback.onVideoProcessingProgress(s);
                }

                @Override
                public void onStart() {
                    Log.d(TAG, "Started command : ffmpeg " + command);
                    callback.onVideoProcessingStart();

                }

                @Override
                public void onFinish() {
                    Log.d(TAG, "Finished command : ffmpeg " + command);
                    callback.onVideoProcessingSuccess(filePath);

//                    callback.onVideoProcessingFinish();

                }
            }
            );
        } catch (FFmpegCommandAlreadyRunningException e) {
            callback.onVideoProcessingFailure(e.getMessage());
        }
    }


    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     */
    private String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}
