package com.app.qualiffyapp.audiorecoder;

import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;

public class AudioRecorder {

    private MediaRecorder mediaRecorder;
    private String mediaFile;
    private AudioRecorderCallbacks callbacks;
    private boolean isRecording = false;

    public AudioRecorder(AudioRecorderCallbacks callbacks) {
        this.callbacks = callbacks;

    }

    private void initRecorder() {
        mediaFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/recording.3gp";
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mediaRecorder.setOutputFile(mediaFile);

    }

    public void startRecording() {
        isRecording = true;
        initRecorder();
        callbacks.onStartRecording();
        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (Exception e) {
            Log.e(AudioRecorder.class.getName(), e.getMessage());
        }

    }


    public void stopRecording(boolean isAccepted) {
        isRecording = false;
        mediaRecorder.stop();
        mediaRecorder.release();
        mediaRecorder = null;
        if (isAccepted)
            callbacks.onStopRecording(mediaFile);
        else callbacks.onStopRecording(null);
    }


    public boolean isRecording() {
        return isRecording;
    }
}
