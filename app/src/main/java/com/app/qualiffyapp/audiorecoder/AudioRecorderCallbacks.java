package com.app.qualiffyapp.audiorecoder;

public interface AudioRecorderCallbacks {
    void onStartRecording();

    void onStopRecording(String filePath);

}
