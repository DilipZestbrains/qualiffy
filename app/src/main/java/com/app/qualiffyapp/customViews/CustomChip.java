package com.app.qualiffyapp.customViews;

import android.content.Context;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.utils.Utility;
import com.google.android.material.chip.Chip;

public class CustomChip extends Chip {

    public CustomChip(Context context, String attrs) {
        super(context);
        this.setText(Utility.getFirstLetterCaps(attrs));
        this.setCloseIconEnabled(true);
        this.setCloseIconResource(R.drawable.ic_clear_black);
        this.setChipBackgroundColorResource(R.color.light_grey);
        this.setTextAppearanceResource(R.style.ChipTextStyle);
    }

    public String getCustomChipText() {
        return this.getText().toString();
    }


}
