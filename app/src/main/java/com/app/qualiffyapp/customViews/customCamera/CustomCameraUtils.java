package com.app.qualiffyapp.customViews.customCamera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Environment;
import android.view.Surface;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.io.File;
import java.util.List;

public class CustomCameraUtils {


    public static Camera getCameraInstance(int cameraId) {
        Camera c = null;
        try {
            // attempt to get a Camera instance
            c = Camera.open();
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }

        // returns null if camera is unavailable
        return c;
    }


    public static Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio=(double)h / w;

        if (sizes == null) return null;

        Camera.Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetHeight = h;

        for (Camera.Size size : sizes) {
            double ratio = (double) size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
            if (Math.abs(size.height - targetHeight) < minDiff) {
                optimalSize = size;
                minDiff = Math.abs(size.height - targetHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;
            for (Camera.Size size : sizes) {
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }
        }
        return optimalSize;
    }

    public static File getFile(boolean isVideo) {
        File newfile = null;

        try {

            File filepath = Environment.getExternalStorageDirectory();
            File dir = new File(filepath.getAbsolutePath() + "/" + "qualiffy" + "/");
            if (!dir.exists()) {
                dir.mkdirs();
            }

            if (isVideo)
                newfile = new File(dir, "save_" + System.currentTimeMillis() + ".mp4");
            else
                newfile = new File(dir, "save_" + System.currentTimeMillis() + ".jpg");

            if (newfile.exists()) newfile.delete();
        } catch (Exception e) {
            e.getMessage();
        }

        return newfile;
    }


    public static void clearDirectory(File dir) {
        if (dir.isDirectory()) {
            if (dir.listFiles() != null)
            for (File child : dir.listFiles())
                clearDirectory(child);

            dir.delete();
        }
    }


    public static void setUpCamera(Camera c, int rotation, int currentCameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(currentCameraId, info);

        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;
            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Camera.Parameters params = c.getParameters();
        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }




    public static int getDisplayRotation(int cameraId,int rotation) {
        Camera.CameraInfo info =new  Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        int degrees = 0;
        switch (rotation) {
          case Surface.ROTATION_0 :degrees = 0 ;break;
            case Surface.ROTATION_90 : degrees = 90 ;break;
            case Surface.ROTATION_180 :degrees = 180 ;break;
            case  Surface.ROTATION_270 : degrees = 270 ;break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360; // compensate the mirror
        } else { // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        return result;
    }







    public static Bitmap MakeSquare(byte[] data, int cameraID) {
        int width;
        int height;
        Matrix matrix = new Matrix();
        Camera.CameraInfo info = new Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraID, info);
        // Convert ByteArray to Bitmap
        Bitmap bitPic = BitmapFactory.decodeByteArray(data, 0, data.length);
        width = bitPic.getWidth();
        height = bitPic.getHeight();

        // Perform matrix rotations/mirrors depending on camera that took the photo
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
        {
            float[] mirrorY = { -1, 0, 0, 0, 1, 0, 0, 0, 1};
            Matrix matrixMirrorY = new Matrix();
            matrixMirrorY.setValues(mirrorY);

            matrix.postConcat(matrixMirrorY);
        }

        matrix.postRotate(90);


        // Create new Bitmap out of the old one
        Bitmap bitPicFinal = Bitmap.createBitmap(bitPic, 0, 0, width, height,matrix, true);
        bitPic.recycle();
        int desWidth;
        int desHeight;
        desWidth = bitPicFinal.getWidth();
        desHeight = desWidth;
        Bitmap croppedBitmap=null;
        if((bitPicFinal.getHeight() * 2/3 - bitPicFinal.getWidth() * 2/3)>0)
       croppedBitmap = Bitmap.createBitmap(bitPicFinal, 0,bitPicFinal.getHeight() *2/3  - bitPicFinal.getWidth() *2/3,desWidth, desHeight);
        else croppedBitmap= bitPicFinal;
        croppedBitmap = Bitmap.createScaledBitmap(croppedBitmap, 528, 528, true);
        return croppedBitmap;
    }


    public static void releaseCamera(Camera camera) {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            camera = null;
        }
    }


}
