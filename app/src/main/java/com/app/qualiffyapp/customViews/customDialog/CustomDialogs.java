package com.app.qualiffyapp.customViews.customDialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.yesNoCallback;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.FACEBOOK;
import static com.app.qualiffyapp.constants.SharedPrefConstants.USER_SOCIAL_TYPE;
import static com.app.qualiffyapp.customViews.DialogInitialize.MEDIA_WITH_FB;

public class CustomDialogs {

    private Context activity;
    private Dialog dialog;
    private yesNoCallback mListener;
    private TextView tvTitle;
    private TextView tvBodyText;
    private TextView yesTxt;
    private TextView noTxt;
    private TextView btn_mid;
    private LinearLayout midLlayout;

    private TextView tvFirstBtn;
    private TextView tvSecBtn;
    private TextView tvThirdBtn;



    public CustomDialogs(Context activity, yesNoCallback listener) {
        this.activity = activity;
        this.mListener = listener;
    }

    public void showCustomDialogTwoButtons(String title, String body, String yesBtntxt, String noBtnTxt, String from) {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_delete_account);
        tvTitle = dialog.findViewById(R.id.tv_title);
        tvBodyText = (TextView) dialog.findViewById(R.id.tv_body_text);
        yesTxt = (TextView) dialog.findViewById(R.id.btn_yes);
        noTxt = (TextView) dialog.findViewById(R.id.btn_no);
        btn_mid = (TextView) dialog.findViewById(R.id.btn_mid);
        midLlayout = (LinearLayout) dialog.findViewById(R.id.midLlayout);

        dialog.setCancelable(true);

        tvBodyText.setText(body);
        tvTitle.setText(title);
        yesTxt.setText(yesBtntxt);
        noTxt.setText(noBtnTxt);


        dialog.findViewById(R.id.btn_no).setOnClickListener(v -> {
            mListener.onNoClicked(from);
            dialog.dismiss();
        });
        dialog.findViewById(R.id.btn_yes).setOnClickListener(v -> {
            mListener.onYesClicked(from);
            dialog.dismiss();

        });
        dialog.findViewById(R.id.midLlayout).setOnClickListener(v -> {
            mListener.onYesClicked(activity.getResources().getString(R.string.FACEBOOK));
            dialog.dismiss();

        });

        if (from.equals(MEDIA_WITH_FB) && Utility.getStringSharedPreference(activity, USER_SOCIAL_TYPE).equals(FACEBOOK)) {
            midLlayout.setVisibility(View.VISIBLE);
        }
    }

    public void showCustomDialogThreeButtons(String title, String body, String from, String firstTxt, String secTxt, String thirdText) {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_three_btn);
        tvTitle = dialog.findViewById(R.id.tv_title);
        tvThirdBtn = dialog.findViewById(R.id.tvThirdBtn);
        tvSecBtn = dialog.findViewById(R.id.tvSecBtn);
        tvFirstBtn = dialog.findViewById(R.id.tvFirstBtn);
        tvBodyText = (TextView) dialog.findViewById(R.id.tv_body_text);

        dialog.setCancelable(true);

        tvBodyText.setText(body);
        tvTitle.setText(title);
        tvFirstBtn.setText(firstTxt);
        tvSecBtn.setText(secTxt);
        tvThirdBtn.setText(thirdText);



        tvFirstBtn.setOnClickListener(v -> {
            mListener.onYesClicked(from);

            dialog.dismiss();
        });
        tvSecBtn.setOnClickListener(v -> {
            mListener.onNoClicked(from);

            dialog.dismiss();

        });
        tvThirdBtn.setOnClickListener(v -> {
            mListener.onYesClicked(activity.getResources().getString(R.string.delete_media_dialog_neg_btn));
            dialog.dismiss();

        });



    }

    public void showCustomDialogSingleButtons(String title, String body, String from) {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_info_single_button);
        tvTitle = (TextView) dialog.findViewById(R.id.tv_title);
        tvBodyText = (TextView) dialog.findViewById(R.id.tv_body_text);

        dialog.setCancelable(true);

        tvBodyText.setText(body);
        tvTitle.setText(title);

        dialog.findViewById(R.id.btn_yes).setOnClickListener(v -> {
            mListener.onYesClicked(from);
        });

        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void showDialog() {
        dialog.show();
    }
}
