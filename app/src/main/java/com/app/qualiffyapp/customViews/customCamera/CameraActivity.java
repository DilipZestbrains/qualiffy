package com.app.qualiffyapp.customViews.customCamera;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.Nullable;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.ViewDialog;
import com.app.qualiffyapp.customViews.customCamera.videoTrimmer.HgLVideoTrimmer;
import com.app.qualiffyapp.customViews.customCamera.videoTrimmer.interfaces.OnHgLVideoListener;
import com.app.qualiffyapp.customViews.customCamera.videoTrimmer.interfaces.OnTrimVideoListener;
import com.app.qualiffyapp.imagevideoeditor.PhotoEditorActivity;
import com.app.qualiffyapp.imagevideoeditor.videoEditor.VideoEditorView;
import com.app.qualiffyapp.utils.Utility;
import com.iammert.library.cameravideobuttonlib.CameraVideoButton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import static com.app.qualiffyapp.constants.AppConstants.IS_SQUARE_IMG;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_IMAGE;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_FILTER_VIDEO;
import static com.app.qualiffyapp.customViews.customCamera.CustomCameraUtils.getFile;

public class CameraActivity extends AppCompatActivity implements SurfaceHolder.Callback, CameraVideoButton.ActionListener, View.OnClickListener, OnTrimVideoListener, OnHgLVideoListener {

    boolean recording = false;
    boolean previewRunning = false;
    int currentCameraId;
    private boolean isProfileIMg;

    Camera.PictureCallback mPicture = (data, camera) -> {
        Bitmap bitmap = CustomCameraUtils.MakeSquare(data, currentCameraId);

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        File pictureFile = getFile(false);
        if (pictureFile == null) {
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(bitmapdata);
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, PhotoEditorActivity.class);
        intent.putExtra(MEDIA_PATH, pictureFile.getPath());
        intent.putExtra(IS_SQUARE_IMG,isProfileIMg);
        startActivityForResult(intent, REQUEST_CODE_FILTER_IMAGE);

    };
    private MediaRecorder recorder;
    private SurfaceHolder holder;
    private CamcorderProfile camcorderProfile;
    private Camera camera;
    private CameraVideoButton videoButton;
    private TextView timer_txt;
    private TextView tvShareExp;
    private File videoFile;
    //0 = video, 1=image ,2 both
    private int mediaType;
    private boolean isApplyJob;
    private ImageView iv_rotation_camera;
    private ImageView ivFlash;
    private ImageView ivCross;
    private boolean flashmode = false;
    private CountDownTimer countDownTimer;
    private HgLVideoTrimmer mVideoTrimmer;
    private ViewDialog mProgressDialog;
    private SurfaceView cameraView;
    private LinearLayout videoThumbnailLayout;
    private boolean ispreview;
    private int surfaceFormat;

    Camera.PreviewCallback preview = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            if (ispreview) {
                Bitmap bitmap = getBitmap(data);
                CameraActivity.this.runOnUiThread(() -> {
                    ImageView image = new ImageView(CameraActivity.this);
                    image.setLayoutParams(new android.view.ViewGroup.LayoutParams(60, 110));
                    image.setImageBitmap(bitmap);
                    videoThumbnailLayout.addView(image);
                });

                ispreview = false;
            }
        }
    };
    private int count = 0;
    private int valueTimer = 29000;
    private View cameraOverlay;
    private LinearLayout swipingLlayout;
    private View ivCamera;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCameraWindowOrientation();
        setContentView(R.layout.activity_camera);

        initialize();
        setListeners();
        getIntentData();

    }

    private void getIntentData() {

        String mediaPath = getIntent().getStringExtra("mediaPath");
        mediaType = getIntent().getIntExtra("media_type", 0);

        isApplyJob = getIntent().getBooleanExtra("isApplyJob", false);
        Boolean isshareExp = getIntent().getBooleanExtra("isShareEx", false);
        isProfileIMg= getIntent().getBooleanExtra(IS_SQUARE_IMG, false);

        if (isApplyJob) {
            cameraOverlay.setVisibility(View.VISIBLE);
            if (isshareExp)
                swipingLlayout.setVisibility(View.GONE);
        } else
            cameraOverlay.setVisibility(View.GONE);


        mVideoTrimmer.setBtnVisiblitiy(isApplyJob);

        videoButton.setMediaType(mediaType);

    }

    private void setCameraWindowOrientation() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
    }

    @Override
    protected void onDestroy() {
        try {
            Camera.Parameters param = camera.getParameters();
            param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private void initialize() {
        iv_rotation_camera = findViewById(R.id.iv_rotation_camera);
        cameraOverlay = findViewById(R.id.cameraOverlay);
        swipingLlayout = findViewById(R.id.swipingLlayout);
        ivCamera = findViewById(R.id.ivCamera);
        ivCross = findViewById(R.id.ivCross);
        timer_txt = findViewById(R.id.timer_txt);
        tvShareExp = findViewById(R.id.tvShareExp);
        ivFlash = findViewById(R.id.ivFlash);
        videoButton = findViewById(R.id.video_btn);
        videoThumbnailLayout = findViewById(R.id.videoThumbnailLayout);
        videoButton.setVideoDuration(40000, 10000);
        cameraView = findViewById(R.id.CameraView);
        holder = cameraView.getHolder();
        holder.addCallback(this);

        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mProgressDialog = new ViewDialog(this);


        tvShareExp.setText(Html.fromHtml(getResources().getString(R.string.share_experience)));

        mVideoTrimmer = ((HgLVideoTrimmer) findViewById(R.id.timeLine));
        if (mVideoTrimmer != null) {
            mVideoTrimmer.setOnTrimVideoListener(this);
            mVideoTrimmer.setOnHgLVideoListener(this);
            mVideoTrimmer.setVideoInformationVisibility(true);

        }
    }

    private Bitmap getBitmap(byte[] data) {
        Camera.Parameters parameters = camera.getParameters();
        int width = parameters.getPreviewSize().width;
        int height = parameters.getPreviewSize().height;
        YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuv.compressToJpeg(new Rect(0, 0, width, height), 100, out);
        byte[] bytes = out.toByteArray();
        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);


        return Utility.rotateImage(bitmap, currentCameraId);
    }


    @Override
    public void onTrimStarted() {
        mProgressDialog.showDialog();
    }

    @Override
    public void getResult(final Uri contentUri) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressDialog.hideDialog();
                String filePath = contentUri.getPath();
                Intent returnIntent = new Intent();
                returnIntent.putExtra(MEDIA_PATH, filePath);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    @Override
    public void cancelAction() {
        mProgressDialog.hideDialog();
        try {
            mVideoTrimmer.destroy();
            mVideoTrimmer.setVisibility(View.GONE);


            if (recording) {
                recorder.stop();
                recording = false;
            }
            if (recorder != null)
                recorder.release();
            previewRunning = false;
            finish();

            timer_txt.setText("");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(final String message) {
        mProgressDialog.hideDialog();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(CameraActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onVideoPrepared() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                 Toast.makeText(CameraActivity.this, "onVideoPrepared", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setListeners() {
        iv_rotation_camera.setOnClickListener(this);
        ivFlash.setOnClickListener(this);
        swipingLlayout.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivCross.setOnClickListener(this);
        videoButton.setActionListener(this);
    }

    private File prepareRecorder() {
        File videoFile = null;
        if (recording) {
            recorder = new MediaRecorder();
            recorder.setPreviewDisplay(holder.getSurface());
            camera.lock();
            camera.unlock();
            camera.setPreviewCallback(preview);
            recorder.setCamera(camera);
            recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//            recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

            if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                recorder.setOrientationHint(90);
            else
                recorder.setOrientationHint(270);

            recorder.setProfile(camcorderProfile);


            try {
                if (camcorderProfile.fileFormat == MediaRecorder.OutputFormat.MPEG_4) {
                    videoFile = getFile(true);
                    recorder.setOutputFile(videoFile.getAbsolutePath());
                } else {
                    videoFile = getFile(true);
                    recorder.setOutputFile(videoFile.getAbsolutePath());
                }

                recorder.setMaxDuration(40000); // 40 seconds

                recorder.prepare();
            } catch (Exception e) {
                e.printStackTrace();
                finish();
            }
        }
        return videoFile;

    }


    public void surfaceCreated(SurfaceHolder holder) {
        setCamera();
    }


    private void setCamera() {
        CustomCameraUtils.releaseCamera(camera);
        if (camera != null)
            camera = null;
        camera = Camera.open(currentCameraId);
        try {
            CustomCameraUtils.setUpCamera(camera, getWindowManager().getDefaultDisplay().getRotation(), currentCameraId);
            camera.setPreviewDisplay(holder);
            camera.setPreviewCallback(preview);
            camera.startPreview();
            previewRunning = true;
        } catch (Exception e) {
            e.printStackTrace();
            CustomCameraUtils.releaseCamera(camera);
            camera = null;
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        surfaceFormat = format;
        if (mediaType == 0 || mediaType == 2) {
            if (!recording) {
                if (previewRunning) {
                    camera.stopPreview();
                }
                try {
                    Camera.Parameters p = camera.getParameters();
                    p.setPreviewSize(camcorderProfile.videoFrameWidth, camcorderProfile.videoFrameHeight);
                    p.setPreviewFrameRate(camcorderProfile.videoFrameRate);
                    camera.setParameters(p);
                    camera.setDisplayOrientation(90);
                    camera.setPreviewDisplay(holder);
                    camera.setPreviewCallback(preview);

                    camera.startPreview();
                    previewRunning = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } else {
            try {
                camera.setDisplayOrientation(90);
                camera.setPreviewDisplay(holder);
                camera.setPreviewCallback(preview);

                camera.startPreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mediaType == 0 || mediaType == 2) {
            if (recording) {
                recorder.stop();
//                recording = false;
            }
         /*   if (recorder != null)
                recorder.release();*/
            previewRunning = false;

//            finish();
        } else {
          /*  camera.stopPreview();
            camera.release();
            camera = null;*/

        }

    }

    @Override
    public void onDurationTooShortError() {
        Log.e("error", "Video error");
    }


    @Override
    public void onEndRecord() {
        if (mediaType == 0 || mediaType == 2) {
            try {
                recorder.stop();
                camera.reconnect();
            } catch (Exception e) {
                e.printStackTrace();

            }
            recording = false;
            if (videoFile != null) {
                String videoPath = null;

                if (videoFile.getAbsolutePath() != null)
                    videoPath = videoFile.getAbsolutePath();
                else videoPath = videoFile.getPath();

                if (videoPath != null) {
                    Intent intent = new Intent(this, VideoEditorView.class);
                    intent.putExtra("videoPath", videoFile.getAbsolutePath());
                    startActivityForResult(intent, REQUEST_CODE_FILTER_VIDEO);

                    countDownTimer.cancel();
                    timer_txt.setText("");

                }
            }

            countDownTimer.cancel();
        }
    }


    private int getMediaDuration(Uri uriOfFile) {
        MediaPlayer mp = MediaPlayer.create(this, uriOfFile);
        if (mp != null) {
            int duration = mp.getDuration();
            return duration;
        }
        return 0;
    }

    @Override
    public void onSingleTap() {
        if (mediaType == 1 || mediaType == 2)
            camera.takePicture(null, null, mPicture);
    }

    @Override
    public void onStartRecord() {
        if (mediaType == 0 || mediaType == 2) {
            if (recorder != null && recording) {
                recorder.stop();
                recorder.release();
            }
            recording = true;

            videoFile = prepareRecorder();
            valueTimer = 29000;

            recorder.start();
            startCounter();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_FILTER_VIDEO) {
                if (data != null && data.getStringExtra(MEDIA_PATH) != null) {
                    setResult(Activity.RESULT_OK, data);
                    if (camera != null) {
                        camera.stopPreview();
                        camera.release();
                        camera = null;
                    }
                    finish();
                }

            } else if (requestCode == REQUEST_CODE_FILTER_IMAGE) {
                setResult(Activity.RESULT_OK, data);
                camera.stopPreview();
                camera.release();
                camera = null;
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_rotation_camera:
                if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    currentCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
                    flashmode = false;
                    ivFlash.setSelected(false);
                } else {
                    currentCameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
                }
                surfaceDestroyed(holder);
                surfaceCreated(holder);
                if (recording && recorder != null)
                    recorder.reset();


                break;

            case R.id.ivFlash:
                if (currentCameraId == Camera.CameraInfo.CAMERA_FACING_BACK)
                    flashOnButton();
                break;

            case R.id.swipingLlayout:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                break;
            case R.id.ivCamera:
                cameraOverlay.setVisibility(View.GONE);
                break;
            case R.id.ivCross:
                cameraOverlay.setVisibility(View.GONE);
                finish();
                break;
        }
    }


    private void flashOnButton() {
        if (camera != null) {
            try {
                Camera.Parameters param = camera.getParameters();
                param.setFlashMode(flashmode == false ? Camera.Parameters.FLASH_MODE_TORCH : Camera.Parameters.FLASH_MODE_OFF);
                if (flashmode) {
                    ivFlash.setSelected(false);
                } else ivFlash.setSelected(true);

                camera.setParameters(param);

                flashmode = !flashmode;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


    private void startCounter() {
        countDownTimer = new CountDownTimer(40000, 1000) {

            public void onTick(long millisUntilFinished) {
                if ((millisUntilFinished - valueTimer) / 1000 > 0)
                    timer_txt.setText(String.valueOf((millisUntilFinished - valueTimer) / 1000));
            }

            public void onFinish() {
                timer_txt.setText("00");
            }
        }.start();
    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void onCircleComplete() {
        count++;
        if (count == 1) {
            if (holder != null) {
                surfaceDestroyed(holder);
                surfaceCreated(holder);
                if (recorder != null)
                    recorder.reset();

            }
            valueTimer = 19000;
        } else if (count == 2) {
            valueTimer = 9000;
        } else {
            valueTimer = -1000;
        }
        if (videoFile != null) {
            ispreview = true;
        }
    }


}