package com.app.qualiffyapp.customViews;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.customDialog.CustomDialogs;
import com.app.qualiffyapp.utils.Utility;
import com.stripe.android.model.Card;
import com.stripe.android.view.CardInputWidget;

public class DialogInitialize {
    public static final String LOGOUT_FLAG = "logout";
    public static final String DELETE_JOB_FLAG = "delete_job";
    public static final String DELETE_ACCOUNT_FLAG = "deleteAccount";
    public static final String MAKE_OFFLINE_FLAG = "makeOfflineProfile";
    public static final String MEDIA_FLAG = "mediaFlag";
    public static final String UPLOAD_VIDEO_FLAG = "uploadVideo";
    public static final String PURCHASE_PLAN_FLAG = "purchasePlan";
    public static final String MEDIA_WITH_FB = "fbMedia";
    private ApplicationController controller;


    public DialogInitialize() {
        controller = ApplicationController.getApplicationInstance();
    }

    public static void createAddCardDialog(Context context, OnCreateCardListener listener) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_add_card);
        dialog.setCancelable(true);
        dialog.findViewById(R.id.tvMakePayment).

                setOnClickListener((v) ->

                {
                    if (listener != null) {
                        Card card = ((CardInputWidget) dialog.findViewById(R.id.cardDetails)).getCard();
                        if (card != null && card.validateCard()) {
                            listener.makePayment(card);
                            dialog.dismiss();
                        } else listener.onError();
                    }
                });
        dialog.show();

    }

    public void prepareDeleteDialog(CustomDialogs customDialogs) {

        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.alert_title),
                controller.getString(R.string.do_you_want_to_delete_your_account),
                controller.getString(R.string.yes),
                controller.getString(R.string.no),
                DELETE_ACCOUNT_FLAG);
        customDialogs.showDialog();
    }

    public void prepareLogoutDialog(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.logout_dialog_title),
                controller.getString(R.string.logout_dialog_body),
                controller.getString(R.string.yes),
                controller.getString(R.string.no),
                LOGOUT_FLAG);
        customDialogs.showDialog();
    }
 public void prepareDeleteJobDialog(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.alert_title),
                controller.getString(R.string.delete_job_dialog_body),
                controller.getString(R.string.delete_job),
                controller.getString(R.string.cancel),
                DELETE_JOB_FLAG);
        customDialogs.showDialog();
    }

    public void prepareDeleteRecriterDialog(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.alert_title),
                controller.getString(R.string.delete_recruiter_dialog_body),
                controller.getString(R.string.delete_job),
                controller.getString(R.string.cancel),
                DELETE_JOB_FLAG);
        customDialogs.showDialog();
    }

    public void makeOfflineDialog(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.alert_title),
                controller.getString(R.string.delete_account_dialog_body),
                controller.getString(R.string.yes),
                controller.getString(R.string.no),
                MAKE_OFFLINE_FLAG);
        customDialogs.showDialog();
    }

    public void captureMediaDialog(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.add_media_dialog_title),
                controller.getString(R.string.add_media_dialog_body),
                controller.getString(R.string.add_media_dialog_pos_btn),
                controller.getString(R.string.add_media_dialog_neg_btn),
                MEDIA_FLAG);
        customDialogs.showDialog();
    }

    public void captureMediaDialog(CustomDialogs customDialogs, String flag) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.add_media_dialog_title),
                controller.getString(R.string.add_media_dialog_body),
                controller.getString(R.string.add_media_dialog_pos_btn),
                controller.getString(R.string.add_media_dialog_neg_btn),
                flag);
        customDialogs.showDialog();
    }

    public void videoUploadOnJobPost(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.upload_video_on_job_post_dialog_title),
                controller.getString(R.string.upload_video_on_job_post_dialog_body),
                controller.getString(R.string.upload_video_on_job_post_dialog_pos_btn),
                controller.getString(R.string.upload_video_on_job_post_dialog_neg_btn),

                UPLOAD_VIDEO_FLAG);
        customDialogs.showDialog();
    }

    public void videoUploadOnApplyJob(CustomDialogs customDialogs) {
        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.app_name),
                controller.getString(R.string.upload_video_on_job_post_dialog_body),
                controller.getString(R.string.record_video_on_job_post_dialog_pos_btn),
                controller.getString(R.string.profile_video_on_job_post_dialog_neg_btn),
                UPLOAD_VIDEO_FLAG);
        customDialogs.showDialog();
    }




    public void subscriptionDialog(CustomDialogs customDialogs, int subscriptionFlag) {
        String body = "";
        if (subscriptionFlag == 2)
            body = controller.getString(R.string.purchase_plan_dialog_body);
        else if (subscriptionFlag == 1)
            body = controller.getString(R.string.expire_plan_dialog_body);

        customDialogs.showCustomDialogTwoButtons(controller.getString(R.string.alert_title),
                body,
                controller.getString(R.string.ok),
                controller.getString(R.string.cancel),
                PURCHASE_PLAN_FLAG);
        customDialogs.showDialog();
    }




    public interface OnCreateCardListener {

        void makePayment(Card card);

        void onError();
    }
}
