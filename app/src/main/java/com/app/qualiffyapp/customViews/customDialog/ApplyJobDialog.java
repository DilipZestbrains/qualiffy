package com.app.qualiffyapp.customViews.customDialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.utils.Utility;

public class ApplyJobDialog {


    private Context activity;
    private Dialog dialog;
    private IApplyJobDialog mListener;


    public ApplyJobDialog(Context activity, IApplyJobDialog listener) {
        this.activity = activity;
        this.mListener = listener;
    }

    public void showApplyJobDialog() {

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (dialog.getWindow() != null)
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setDimAmount((float) 0.6);
        }
        Utility.modifyDialogBoundsCenter(dialog);
        dialog.setContentView(R.layout.dialog_apply_job);


        dialog.setCancelable(false);

        dialog.findViewById(R.id.tvApplyJob).setOnClickListener(v -> {
            mListener.ApplyJobClick();
            dialog.dismiss();
        });
        dialog.findViewById(R.id.tvProfileVideo).setOnClickListener(v -> {
            mListener.profileVideoClick();
            dialog.dismiss();

        });
        dialog.findViewById(R.id.tvLater).setOnClickListener(v -> {
            mListener.laterClick();
            dialog.dismiss();

        });
        dialog.show();
    }
}
