package com.app.qualiffyapp.customViews;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeableMethod;

public class CustomCardStackLayoutManager extends CardStackLayoutManager implements CardStackListener {


    public CustomCardStackLayoutManager(Context context, CardStackListener stackListener) {
        super(context);

        setStackFrom(StackFrom.None);
        setVisibleCount(3);
        setTranslationInterval(8.0f);
        setScaleInterval(0.95f);
        setSwipeThreshold(0.4f);
        setMaxDegree(30.0f);
        setDirections(Direction.HORIZONTAL);
        setCanScrollHorizontal(true);
        setCanScrollVertical(false);
        setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        setOverlayInterpolator(new LinearInterpolator());
    }


    public static CardStackLayoutManager getCardStackLayoutManager(Context context, CardStackListener stackListener) {
        CardStackLayoutManager cardStackLayoutManager = new CardStackLayoutManager(context, stackListener);
        cardStackLayoutManager.setStackFrom(StackFrom.None);
        cardStackLayoutManager.setVisibleCount(3);
        cardStackLayoutManager.setTranslationInterval(8.0f);
        cardStackLayoutManager.setScaleInterval(0.95f);
        cardStackLayoutManager.setSwipeThreshold(0.4f);
        cardStackLayoutManager.setMaxDegree(30.0f);
        cardStackLayoutManager.setDirections(Direction.HORIZONTAL);
        cardStackLayoutManager.setCanScrollHorizontal(true);//Android Team ZB111
        cardStackLayoutManager.setCanScrollVertical(false);

        cardStackLayoutManager.setSwipeableMethod(SwipeableMethod.AutomaticAndManual);
        cardStackLayoutManager.setOverlayInterpolator(new LinearInterpolator());
        return cardStackLayoutManager;
    }


    @Override
    public void onCardDragging(Direction direction, float ratio) {
        Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = " + ratio);

    }

    @Override
    public void onCardSwiped(Direction direction) {
        Log.d("CardStackView", "onCardDragging: d = " + direction.name() + ", r = ");

    }

    @Override
    public void onCardRewound() {

    }

    @Override
    public void onCardCanceled() {
        Log.d("CardStackView", "onCardDragging:");

    }

    @Override
    public void onCardAppeared(View view, int position) {
        Log.d("CardStackView", "onCardDragging: d ");

    }

    @Override
    public void onCardDisappeared(View view, int position) {
        Log.d("CardStackView", "onCardDragging: d = ");

    }
}
