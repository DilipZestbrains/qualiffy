package com.app.qualiffyapp.callbacks;

/**
 * Created on 28/8/15.
 * Ok cancel callback listener
 */
public interface OkCancelCallback {

    /**
     * Callback for positive action click
     */
    void onOkClicked();

    /**
     * Callback for negative action click
     */
    void onCancelClicked();

}
