package com.app.qualiffyapp.callbacks;

import androidx.fragment.app.FragmentActivity;

/**
 * Created on 10/11/16.
 * Common View Interface
 */
public interface CommonView {

    /**
     * Get activity's context
     *
     * @return Activity context
     */
    FragmentActivity getViewContext();

    /**
     * Show progress when api is being called
     *
     * @param showProgress Flag to show hide progress view
     */
    void showProgressDialog(boolean showProgress);
}
