package com.app.qualiffyapp.callbacks.signupLogin;

public interface RecyclerItemClick {
    int DELETE_FLAG = 101;
    int EDIT_FLAG = 102;
    int ADD_IMG_CLICK_FLAG = 103;
    int VIEW_IMG_CLICK_FLAG = 104;

    void onClick(int flag, int pos);
}
