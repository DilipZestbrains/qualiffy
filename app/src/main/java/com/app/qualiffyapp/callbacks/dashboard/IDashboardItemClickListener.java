package com.app.qualiffyapp.callbacks.dashboard;

public interface IDashboardItemClickListener {

    void recomendJobClick(int isRecommend, String jobId);
    void recomendCount(int isRecommend, String jobId);
    void mediaPlay();

}
