package com.app.qualiffyapp.callbacks.plans;

import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;

public interface IUgradePlan {

    void upgradeClick(int pos, JsSubscriptionModel.SubsBean bean);

    void premiumClick(int pos, JsSubscriptionModel.SubsBean bean);

    void requestQuoteClick();

    void proceedPay(JsSubscriptionModel.SubsBean bean);
}
