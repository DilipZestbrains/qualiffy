package com.app.qualiffyapp.callbacks;

public interface ItemAddListener {
    void onAdd();
}
