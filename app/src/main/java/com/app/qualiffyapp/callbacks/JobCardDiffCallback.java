package com.app.qualiffyapp.callbacks;


import androidx.recyclerview.widget.DiffUtil;

import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public final class JobCardDiffCallback extends DiffUtil.Callback {
    private List old;
    private List newList;

    public JobCardDiffCallback(@NotNull List old, @NotNull List var2) {
        super();
        this.old = old;
        this.newList = var2;
    }

    public int getOldListSize() {
        return this.old.size();
    }

    public int getNewListSize() {
        return this.newList.size();
    }

    public boolean areItemsTheSame(int oldPosition, int newPosition) {
        return ((JsJobsResponseModel.JobBean) this.old.get(oldPosition)).id.equals(((JsJobsResponseModel.JobBean) this.newList.get(newPosition)).id);
    }

    public boolean areContentsTheSame(int oldPosition, int newPosition) {
        return ((JsJobsResponseModel.JobBean) this.old.get(oldPosition) == (JsJobsResponseModel.JobBean) this.newList.get(newPosition));
    }
}
