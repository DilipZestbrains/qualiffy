package com.app.qualiffyapp.callbacks;

/**
 * Created on 28/8/15.
 * Ok cancel callback listener
 */
public interface yesNoCallback {

    /**
     * Callback for positive action click
     *
     * @param from
     */
    void onYesClicked(String from);

    /**
     * Callback for negative action click
     *
     * @param from
     */
    void onNoClicked(String from);

}
