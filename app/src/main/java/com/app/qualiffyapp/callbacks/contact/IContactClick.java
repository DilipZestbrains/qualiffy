package com.app.qualiffyapp.callbacks.contact;

import com.app.qualiffyapp.localDatabase.entity.Contact;

public interface IContactClick {
    void onContactClick(Contact contact, int flag);


    void openContactProfile(String id);
}
