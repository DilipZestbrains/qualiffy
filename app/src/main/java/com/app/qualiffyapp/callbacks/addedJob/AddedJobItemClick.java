package com.app.qualiffyapp.callbacks.addedJob;

public interface AddedJobItemClick {

    void itemClick(int pos);
}
