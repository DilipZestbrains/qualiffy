package com.app.qualiffyapp.callbacks.appliedJob;

import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;

public interface AppliedJobItemClickListener {

    void onjobItemClick(String jobId, AppliedJobResponseModel.JobBean.OrgInfoBean orgInfo);
    void onVideoClick(String videoUrl);
}
