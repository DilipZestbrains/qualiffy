package com.app.qualiffyapp.callbacks;

public interface AlertCancelCallback {

    void onAlertClick(int flag);
}
