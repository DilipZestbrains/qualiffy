package com.app.qualiffyapp.callbacks.signupLogin;

public interface SpannableClickCallback {
    int SPAN_TERMS_N_COND = 101;
    int SPAN_SIGN_UP = 102;
    int SPAN_LOGIN = 103;


    void spannableStringClick(int flag);

}
