package com.app.qualiffyapp.callbacks.dashboard;

public interface IOrganizationJobItemClick {
    void onItemClick(int pos, String jobId, String jobName,String desc);
}
