package com.app.qualiffyapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.firebase.audioCall.AudioCallActivity;
import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.firebase.chat.types.CallType;
import com.app.qualiffyapp.firebase.videocall.VideoCallActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.crashlytics.android.Crashlytics;
import com.google.android.libraries.places.api.Places;
import com.google.gson.Gson;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT;
import static com.app.qualiffyapp.constants.AppConstants.CALL_INTENT_UNREGISTER;
import static com.app.qualiffyapp.constants.AppConstants.CONNECTION;

public class ApplicationController extends MultiDexApplication implements LifecycleObserver {
    private static ApplicationController mApplicationInstance;
    private static RequestManager mInstance;

    private BroadcastReceiver registerCallListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            AppFirebaseDatabase.getInstance().setUserCallStatusListener((user, connection) -> {
                Bundle b = new Bundle();
                b.putString(CONNECTION, new Gson().toJson(connection));
                b.putString(AppConstants.USER, new Gson().toJson(user));
                Class des;
                if (user.getCall_type().equals(CallType.VIDEO.value))
                    des = VideoCallActivity.class;
                else des = AudioCallActivity.class;

                Intent intent1 = new Intent(getApplicationContext(), des);
                intent1.putExtra(BUNDLE, b);
                intent1.addFlags(FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);

            });
        }

    };

    private BroadcastReceiver unregisterCallListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            AppFirebaseDatabase.getInstance().removeUserCallStatusListener();
        }
    };

    public static ApplicationController getApplicationInstance() {
        if (mApplicationInstance == null)
            mApplicationInstance = new ApplicationController();
        return mApplicationInstance;
    }

    public static RequestManager getGlideInstance() {
        if (mInstance == null) {
            mInstance = Glide.with(getApplicationInstance());
        }
        return mInstance;
    }

    @Override
    public void onCreate() {
        getDebugKey();
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(registerCallListener, new IntentFilter(CALL_INTENT));
        LocalBroadcastManager.getInstance(this).registerReceiver(unregisterCallListener, new IntentFilter(CALL_INTENT_UNREGISTER));
        Logger.addLogAdapter(new AndroidLogAdapter());
        mApplicationInstance = this;
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), AppConstants.GOOGLE_PLACES_API_KEY);
        }

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(getApplicationContext());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }

    private void getDebugKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.app.qualiffyapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash", "KeyHash:" + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
        }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackground() {
        Log.d("Yeeey", "App in background");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        Log.d("Yeeey", "App in foreground");
    }

}