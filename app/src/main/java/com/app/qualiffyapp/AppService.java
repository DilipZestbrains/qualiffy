package com.app.qualiffyapp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import com.app.qualiffyapp.firebase.chat.AppFirebaseDatabase;
import com.app.qualiffyapp.utils.Utility;

import static com.app.qualiffyapp.constants.AppConstants.IS_APP_VISIBLE;

public class AppService extends Service {
    private String LOG = "AppService";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.e(LOG, "onBindService");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(LOG, "onBindServiceStart");
//        Utility.putBooleanValueInSharedPreference(this, IS_APP_VISIBLE, true);
        return START_NOT_STICKY;
    }

    @Override
    public boolean stopService(Intent name) {
        Log.e(LOG, "service is stopped");
        return super.stopService(name);

    }

    @Override
    public void onDestroy() {
        Log.e(LOG, "onBindServiceDestroy");
        super.onDestroy();

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        AppFirebaseDatabase.getInstance().clearPendingCalls();
        Utility.putBooleanValueInSharedPreference(this, IS_APP_VISIBLE, false);
        Log.e(LOG, "onBindServiceRemoved");
        stopSelf();
    }
}
