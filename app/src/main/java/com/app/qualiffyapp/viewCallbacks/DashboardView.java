package com.app.qualiffyapp.viewCallbacks;


import com.app.qualiffyapp.callbacks.CommonView;
import com.google.gson.JsonElement;

/**
 * Created on 15/03/17.
 * RegisterView interface
 */

public interface DashboardView extends CommonView {
    void apiSuccess(JsonElement response, int apiFlag);

    void apiError(String error, int apiFlag);
}
