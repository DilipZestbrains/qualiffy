package com.app.qualiffyapp.viewCallbacks;


import com.app.qualiffyapp.callbacks.CommonView;
import com.app.qualiffyapp.models.ResponseModel;

/**
 * Created on 15/03/17.
 * RegisterView interface
 */

public interface SignupView extends CommonView {
    void apiSuccess(ResponseModel response, int apiFlag);

    void apiError(String error, int apiFlag);

    void validationFailed(String msg);
}
