package com.app.qualiffyapp.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.ViewDialog;

import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;


public abstract class BaseFragment extends Fragment implements View.OnClickListener, EasyPermissions.PermissionCallbacks {

    protected Context context;
    protected ViewDataBinding viewDataBinding;
    private ViewDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewDataBinding = DataBindingUtil.inflate(inflater, getLayoutById(), container, false);
        setUserVisibleHint(false);
        if (viewDataBinding != null)
            return viewDataBinding.getRoot();
        else return inflater.inflate(getLayoutById(), container);
    }

    @Override
    public boolean getUserVisibleHint() {
        return super.getUserVisibleHint();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            dialog = new ViewDialog(getActivity());
        }
        initUi();
        setListener();
    }

    public void showProgress(boolean show) {
        if (show) {
            if (dialog != null) {
                dialog.showDialog();
            }
        } else {
            if (dialog != null) {
                dialog.hideDialog();
            }
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    public boolean onAskForSomePermission(Context context, String[] perms, int requestCode) {
        if (EasyPermissions.hasPermissions(context, perms)) {
            onPermissionsGranted(requestCode, Arrays.asList(perms));
            return true;
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permission_dialog),
                    requestCode, perms);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Some permissions have been granted
        // Override this method in your activity to get result callback
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // Override this method in your activity to get result callback
    }

    /**
     * Initilize Ui parameters here
     */
    protected abstract void initUi();

    /**
     * Returns the layout resource identifier
     *
     * @return Layout res id
     */
    protected abstract int getLayoutById();

    protected void setListener() {

    }

    public void openActivity(Class<?> calledActivity, Bundle data) {
        Intent myIntent = new Intent(getActivity(), calledActivity);
        if (data != null) {
            myIntent.putExtra(BUNDLE, data);
        }
        this.startActivity(myIntent);
    }

    public void openActivityForResult(Class<?> calledActivity, Bundle data, int requestCode) {
        Intent myIntent = new Intent(getContext(), calledActivity);
//        myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (data != null) {
            myIntent.putExtra(BUNDLE, data);
        }
        this.startActivityForResult(myIntent, requestCode);
    }

}
