package com.app.qualiffyapp.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.customViews.ViewDialog;
import com.app.qualiffyapp.utils.Utility;
import com.app.qualiffyapp.utils.animation.TransitionHelper;
import com.sandrios.sandriosCamera.internal.SandriosCamera;

import java.util.ArrayList;
import java.util.List;

import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.MEDIA_PATH;
import static com.app.qualiffyapp.constants.AppConstants.PHOTO_EDITOR_ACTIVITY;
import static com.app.qualiffyapp.constants.AppConstants.REQUEST_CODE_CAMERA_IMG;


public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, EasyPermissions.PermissionCallbacks {
    protected ViewDataBinding viewDataBinding;
    private Boolean mIsActivityVisible = false;
    private ViewDialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutById());
        dialog = new ViewDialog(this);
        initView();
        setListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsActivityVisible = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsActivityVisible = false;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public Boolean isActivityVisible() {
        return mIsActivityVisible;
    }

    public void setImageViewAnimatedChange(Context c, final ImageButton v, final int id, boolean isEnabled) {
        final Bitmap newImage = BitmapFactory.decodeResource(getResources(), id);
        final Animation animOut = AnimationUtils.loadAnimation(c, R.anim.zoom_out);
        final Animation animIn = AnimationUtils.loadAnimation(c, R.anim.zoom_in);
        animOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                v.setBackgroundResource(id);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }
                });
                v.startAnimation(animIn);
            }
        });
        v.startAnimation(animOut);
    }


    public boolean onAskForSomePermission(Context context, String[] perms, int requestCode) {
        if (EasyPermissions.hasPermissions(context, perms)) {
            return true;
//            onPermissionsGranted(requestCode, Arrays.asList(perms));
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permission_dialog),
                    requestCode, perms);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Permission", "Permission REQUESTED");

        // Forward results to EasyPermissions
        // Make a collection of granted and denied permissions from the request.
        List<String> granted = new ArrayList<>();
        List<String> denied = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            String perm = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                granted.add(perm);
            } else {
                denied.add(perm);
            }
        }
        if (granted.size() == permissions.length)
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        else onPermissionsDenied(requestCode, denied);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        // Some permissions have been granted
        // Override this method in your activity to get result callback
        Log.e("Permission", "Permission Granted");
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // Override this method in your activity to get result callback
        Log.e("Permission", "Permission Denied");

    }

    protected abstract void initView();

    public void showProgress(boolean show) {
        try {
            if (show) {
                if (dialog != null) {
                    dialog.showDialog();
                }
            } else {
                if (dialog != null) {
                    dialog.hideDialog();
                }
            }
        } catch (Exception e) {
            Log.e(BaseActivity.class.getName(), e.getLocalizedMessage());
        }


    }

    protected abstract int getLayoutById();


    @SuppressWarnings("unchecked")
    public void transitionTo(Intent i) {
        final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
        ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
        startActivity(i, transitionActivityOptions.toBundle());
    }

    protected void setListener() {

    }

    public void openActivity(Class<?> calledActivity, Bundle data) {
        Intent myIntent = new Intent(this, calledActivity);
        if (data != null) {
            myIntent.putExtra(BUNDLE, data);
        }
        this.startActivity(myIntent);
    }

    public void openActivityForResult(Class<?> calledActivity, Bundle data, int requestCode) {
        Intent myIntent = new Intent(this, calledActivity);
        if (data != null) {
            myIntent.putExtra(BUNDLE, data);
        }
        this.startActivityForResult(myIntent, requestCode);
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }




}
