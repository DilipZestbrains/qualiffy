package com.app.qualiffyapp.adapter.addedJob;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationJobItemClick;
import com.app.qualiffyapp.databinding.AdapterOrganizationJobBinding;
import com.app.qualiffyapp.models.addedJob.AddedJobResponseModel;
import com.app.qualiffyapp.utils.Utility;

import java.util.List;
import java.util.Random;

public class AddedJobAdapter extends RecyclerView.Adapter<AddedJobAdapter.ListViewHolder> {
    private List<AddedJobResponseModel.JobBean> jobList;
    private Context context;
    private Random random;
    private IOrganizationJobItemClick itemClick;


    public AddedJobAdapter(Context context, List<AddedJobResponseModel.JobBean> jobList, IOrganizationJobItemClick itemClick) {
        this.jobList = jobList;
        this.context = context;
        this.itemClick = itemClick;
        random = new Random();
    }

    @NonNull
    @Override
    public AddedJobAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AddedJobAdapter.ListViewHolder((AdapterOrganizationJobBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_organization_job, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AddedJobAdapter.ListViewHolder holder, int position) {
        AddedJobResponseModel.JobBean subsBean = jobList.get(position);
        holder.bind(subsBean);
    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterOrganizationJobBinding mBinding;

        ListViewHolder(@NonNull AdapterOrganizationJobBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.joblLayout.setOnClickListener(this);
        }

        void bind(AddedJobResponseModel.JobBean bean) {
            mBinding.tvJobTitle.setText(bean.jobTName);
            int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
            mBinding.tvJobInitialLetter.setBackgroundTintList(ColorStateList.valueOf(color));
            mBinding.tvJobInitialLetter.setText(String.valueOf(bean.jobTName.charAt(0)).toUpperCase());
            mBinding.tvApplicant.setText(String.valueOf(bean.cnt) + " " + Utility.singularPluralText(context.getResources().getString(R.string.application), bean.cnt));
            if (bean.prty)
                mBinding.priorityImg.setVisibility(View.VISIBLE);
            else
                mBinding.priorityImg.setVisibility(View.INVISIBLE);

        }

        @Override
        public void onClick(View v) {
            itemClick.onItemClick(getLayoutPosition(), jobList.get(getLayoutPosition()).id, jobList.get(getLayoutPosition()).jobTName, jobList.get(getLayoutPosition()).desc);
        }
    }
}
