package com.app.qualiffyapp.adapter.signup;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.AdapterSuggetedListBinding;
import com.app.qualiffyapp.models.createProfile.SuggestedListResponseModel;

import java.util.List;

public class SuggestedListAdapter extends
        RecyclerView.Adapter<SuggestedListAdapter.SuggestListViewHolder> {


    private List<SuggestedListResponseModel> suggestedList;
    private OnIndustryClickListener listener;

    public SuggestedListAdapter(List<SuggestedListResponseModel> countryList) {
        this.suggestedList = countryList;
    }

    @Override
    public void onBindViewHolder(SuggestListViewHolder holder, int position) {
        SuggestedListResponseModel model = suggestedList.get(position);
        holder.suggetedListBinding.listItem.setText(model.name);
        holder.suggetedListBinding.getRoot().setOnClickListener(v -> {
            if (listener != null) {
                listener.onIndustryClick(suggestedList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return suggestedList.size();
    }

    @Override
    public SuggestListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SuggestedListAdapter.SuggestListViewHolder((AdapterSuggetedListBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_suggeted_list, parent, false));

    }

    public void refereshList(List<SuggestedListResponseModel> suggestedList) {
        this.suggestedList = suggestedList;
        notifyDataSetChanged();
    }

    public void clearList() {
        suggestedList.clear();
        notifyDataSetChanged();
    }

    public void setListener(OnIndustryClickListener listener) {
        this.listener = listener;
    }

    public interface OnIndustryClickListener {
        void onIndustryClick(SuggestedListResponseModel responseModel);
    }

    public class SuggestListViewHolder extends RecyclerView.ViewHolder {

        AdapterSuggetedListBinding suggetedListBinding;

        public SuggestListViewHolder(AdapterSuggetedListBinding listitem) {
            super(listitem.getRoot());
            this.suggetedListBinding = listitem;
        }

    }
}
