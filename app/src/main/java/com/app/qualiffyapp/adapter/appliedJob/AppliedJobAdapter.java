package com.app.qualiffyapp.adapter.appliedJob;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.appliedJob.AppliedJobItemClickListener;
import com.app.qualiffyapp.databinding.AdapterAppliedJobBinding;
import com.app.qualiffyapp.models.appliedJob.AppliedJobResponseModel;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;

import java.util.List;

public class AppliedJobAdapter extends RecyclerView.Adapter<AppliedJobAdapter.RecyclerViewHolder> {
    public List<AppliedJobResponseModel.JobBean> appliedJobList;
    private Context context;
    private String basePath;
    private String jobPath;
    private AppliedJobItemClickListener itemClick;

    public AppliedJobAdapter(String bp,String jp, List<AppliedJobResponseModel.JobBean> appliedJobList, Context context, AppliedJobItemClickListener itemClick) {
        this.appliedJobList = appliedJobList;
        this.context = context;
        this.basePath = bp;
        this.jobPath=jp;
        this.itemClick = itemClick;
    }

    public void addAll(List<AppliedJobResponseModel.JobBean> items) {
        appliedJobList.addAll(items);
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public AppliedJobAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AppliedJobAdapter.RecyclerViewHolder((AdapterAppliedJobBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_applied_job, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AppliedJobAdapter.RecyclerViewHolder holder, int position) {
        AppliedJobResponseModel.JobBean subsBean = appliedJobList.get(position);
        holder.bind(subsBean);


    }

    @Override
    public int getItemCount() {
        return appliedJobList.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterAppliedJobBinding mBinding;

        RecyclerViewHolder(@NonNull AdapterAppliedJobBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.tvStatus.setOnClickListener(this);
            this.mBinding.ivVideo.setOnClickListener(this);
        }

        void bind(AppliedJobResponseModel.JobBean bean) {
            mBinding.tvJobTitle.setText(bean.myrName);
            if (bean.upld) {
                mBinding.tvStatus.setText(context.getResources().getString(R.string.applied_job_txt));
                mBinding.tvStatus.setTextColor(context.getResources().getColor(R.color.green_3A932F));
                mBinding.ivVideo.setImageResource(R.drawable.ic_round_play);
            } else {
                mBinding.tvStatus.setText(context.getResources().getString(R.string.share_exp));
                mBinding.ivVideo.setImageResource(R.drawable.ic_round_video);

                mBinding.tvStatus.setTextColor(context.getResources().getColor(R.color.bowl_bg_four));
            }

            mBinding.tvExp.setText(String.valueOf(bean.jobsInfo.exp) + " " + Utility.singularPluralText("year", bean.jobsInfo.exp));
            mBinding.tvOrgName.setText(bean.orgInfo.name);
            GlideUtil.loadCircularImage(mBinding.ivOrgn, basePath + bean.orgInfo.img, R.drawable.ic_add_photo);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvStatus:
                    if (!appliedJobList.get(getLayoutPosition()).upld)
                        itemClick.onjobItemClick(appliedJobList.get(getLayoutPosition()).jobId, appliedJobList.get(getLayoutPosition()).orgInfo);
                    break;
                case R.id.ivVideo:
                    if (appliedJobList.get(getLayoutPosition()).video != null || !TextUtils.isEmpty(appliedJobList.get(getLayoutPosition()).video)) {
                        itemClick.onVideoClick(jobPath + appliedJobList.get(getLayoutPosition()).video);
                    } else {
                        if (!appliedJobList.get(getLayoutPosition()).upld)
                            itemClick.onjobItemClick(appliedJobList.get(getLayoutPosition()).jobId, appliedJobList.get(getLayoutPosition()).orgInfo);
                    }
                    break;
            }
        }
    }
}
