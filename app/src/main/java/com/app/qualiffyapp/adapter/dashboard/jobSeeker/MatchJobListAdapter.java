package com.app.qualiffyapp.adapter.dashboard.jobSeeker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationJobItemClick;
import com.app.qualiffyapp.databinding.ItemMatchProfileBinding;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;
import com.app.qualiffyapp.utils.Utility;

import java.util.List;

public class MatchJobListAdapter extends RecyclerView.Adapter<MatchJobListAdapter.ListViewHolder> {
    private List<OrgJobListResponseModel.JobBean> jobList;

    private IOrganizationJobItemClick itemClick;
    private Context context;


    public MatchJobListAdapter(Context context, List<OrgJobListResponseModel.JobBean> jobList, IOrganizationJobItemClick itemClick) {
        this.jobList = jobList;
        this.itemClick = itemClick;
        this.context = context;
    }

    @NonNull
    @Override
    public MatchJobListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MatchJobListAdapter.ListViewHolder((ItemMatchProfileBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_match_profile, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull MatchJobListAdapter.ListViewHolder holder, int position) {
        OrgJobListResponseModel.JobBean subsBean = jobList.get(position);
        holder.bind(subsBean);


    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemMatchProfileBinding mBinding;

        ListViewHolder(@NonNull ItemMatchProfileBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.joblLayout.setOnClickListener(this);
        }

        void bind(OrgJobListResponseModel.JobBean bean) {
            mBinding.tvJobTitle.setText(bean.jobTName);
            mBinding.tvApplicantCount.setText(String.valueOf(bean.applicantCount));
            mBinding.tvApplicantTitle.setText(Utility.singularPluralText(context.getResources().getString(R.string.applicants), bean.applicantCount));
            mBinding.tvPublishedDate.setText(Utility.getDateTime(bean.date, "yyyyMMdd", "dd/MMM/yyyy"));
            if (bean.prty)
                mBinding.priorityImg.setVisibility(View.VISIBLE);
            else
                mBinding.priorityImg.setVisibility(View.INVISIBLE);

        }

        @Override
        public void onClick(View v) {
            itemClick.onItemClick(getLayoutPosition(), jobList.get(getLayoutPosition()).jobId, jobList.get(getLayoutPosition()).jobTName, null);
        }
    }
}
