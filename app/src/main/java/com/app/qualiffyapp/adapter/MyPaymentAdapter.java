package com.app.qualiffyapp.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemPaymentBinding;
import com.app.qualiffyapp.models.payment.PaymentReceiptRequest;
import com.app.qualiffyapp.utils.TimeUtil;

import java.util.ArrayList;

public class MyPaymentAdapter extends RecyclerView.Adapter<MyPaymentAdapter.PaymentViewHolder> {
    private ArrayList<PaymentReceiptRequest> payments;


    public MyPaymentAdapter(ArrayList<PaymentReceiptRequest> payments) {
        this.payments = payments;
    }

    public void refreshList(ArrayList<PaymentReceiptRequest> payments) {
        this.payments = payments;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPaymentBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_payment, parent, false);
        return new PaymentViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentViewHolder holder, int position) {
        holder.bindReceipt(payments.get(position));
    }

    @Override
    public int getItemCount() {
        return payments.size();
    }

    class PaymentViewHolder extends RecyclerView.ViewHolder {
        private final ItemPaymentBinding mBinding;

        PaymentViewHolder(ItemPaymentBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        void bindReceipt(PaymentReceiptRequest paymentReceiptRequest) {
            mBinding.tvPaidFor.setText(paymentReceiptRequest.name);
            mBinding.tvPaymentDate.setText(TimeUtil.getDateFromTimestamp(paymentReceiptRequest.created,"dd/MM/yyyy"));
            mBinding.tvTotalAmount.setText("$"+paymentReceiptRequest.amount);
        }
    }


}

