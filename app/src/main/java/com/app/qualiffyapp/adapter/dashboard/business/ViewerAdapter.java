package com.app.qualiffyapp.adapter.dashboard.business;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.AdapterViewerLayoutBinding;
import com.app.qualiffyapp.models.home.business.OrgBean;
import com.app.qualiffyapp.utils.GlideUtil;
import java.util.List;

public class ViewerAdapter extends RecyclerView.Adapter<ViewerAdapter.RecyclerViewHolder> {
    private List<OrgBean> orgList;
    private String basePath;
    private UserProfileListener profileListener;

    public ViewerAdapter(String bp, List<OrgBean> orgList) {
        this.orgList = orgList;
        this.basePath = bp;
    }

    public void addAll(List<OrgBean> items) {
        orgList.addAll(items);
        notifyDataSetChanged();
    }

    public void setProfileListener(int from, UserProfileListener userProfileListener) {
        if (from == AppConstants.JS_RECOMMANDATION || from == AppConstants.JS_VIEWER)
            profileListener = userProfileListener;

    }


    @NonNull
    @Override
    public ViewerAdapter.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewerAdapter.RecyclerViewHolder((AdapterViewerLayoutBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_viewer_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewerAdapter.RecyclerViewHolder holder, int position) {
        OrgBean subsBean = orgList.get(position);
        holder.bind(subsBean);
    }

    @Override
    public int getItemCount() {
        return orgList.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterViewerLayoutBinding mBinding;

        RecyclerViewHolder(@NonNull AdapterViewerLayoutBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        void bind(OrgBean bean) {
            mBinding.getRoot().setOnClickListener(view -> {
                if (profileListener != null)
                    if (!TextUtils.isEmpty(bean.org_id))
                        profileListener.openUserProfile(bean.org_id, AppConstants.BUSINESS_USER);
                    else if (!TextUtils.isEmpty(bean.id))
                        profileListener.openUserProfile(bean.id, AppConstants.BUSINESS_USER);
            });
            mBinding.tvOrgName.setText(bean.name);
            if (bean.state != null)
                mBinding.tvAddress.setText(bean.state);
            GlideUtil.loadCircularImage(mBinding.ivOrgn, basePath + bean.img, R.drawable.ic_add_photo);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

            }
        }
    }

    public interface UserProfileListener {
        void openUserProfile(String id, int type);
    }
}
