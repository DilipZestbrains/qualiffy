package com.app.qualiffyapp.adapter.addedJob;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.addedJob.AddedJobItemClick;
import com.app.qualiffyapp.databinding.AdapterJobApplicantsBinding;
import com.app.qualiffyapp.models.appliedJob.ApplicantsResponseModel;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.List;

public class JobApplicantAdapter extends RecyclerView.Adapter<JobApplicantAdapter.ListViewHolder> {
    private List<ApplicantsResponseModel.UserBean> jobList;
    private Context context;
    private AddedJobItemClick itemClick;
    private String basePath, jobVideoBasePath;


    public JobApplicantAdapter(Context context, ApplicantsResponseModel responseModel, AddedJobItemClick itemClick) {
        this.jobList = responseModel.users;
        this.context = context;
        this.itemClick = itemClick;
        this.basePath = responseModel.bP;
        this.jobVideoBasePath = responseModel.jP;
    }

    @NonNull
    @Override
    public JobApplicantAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JobApplicantAdapter.ListViewHolder((AdapterJobApplicantsBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_job_applicants, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull JobApplicantAdapter.ListViewHolder holder, int position) {
        ApplicantsResponseModel.UserBean subsBean = jobList.get(position);
        holder.bind(subsBean);


    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterJobApplicantsBinding mBinding;

        ListViewHolder(@NonNull AdapterJobApplicantsBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.joblLayout.setOnClickListener(this);
        }

        void bind(ApplicantsResponseModel.UserBean bean) {
            if (bean.userName == null || bean.userName.equals(""))
                mBinding.tvUserName.setText(bean.fName + " " + bean.lName);
            else mBinding.tvUserName.setText(bean.userName);
            GlideUtil.loadCircularImage(mBinding.imgUser, basePath + bean.img, R.drawable.user_placeholder);

        }

        @Override
        public void onClick(View v) {
            itemClick.itemClick(getLayoutPosition());
        }
    }
}

