package com.app.qualiffyapp.adapter.dashboard.jobSeeker;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationJobItemClick;
import com.app.qualiffyapp.databinding.AdapterOrganizationJobBinding;
import com.app.qualiffyapp.models.home.jobSeeker.OrgJobListResponseModel;

import java.util.List;
import java.util.Random;

public class OrgnaizationJobListAdapter extends RecyclerView.Adapter<OrgnaizationJobListAdapter.ListViewHolder> {
    private List<OrgJobListResponseModel.JobBean> jobList;
    private Context context;
    private Random random;
    private IOrganizationJobItemClick itemClick;


    public OrgnaizationJobListAdapter(Context context, List<OrgJobListResponseModel.JobBean> jobList, IOrganizationJobItemClick itemClick) {
        this.jobList = jobList;
        this.context = context;
        this.itemClick = itemClick;
        random = new Random();
    }

    @NonNull
    @Override
    public OrgnaizationJobListAdapter.ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrgnaizationJobListAdapter.ListViewHolder((AdapterOrganizationJobBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_organization_job, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull OrgnaizationJobListAdapter.ListViewHolder holder, int position) {
        OrgJobListResponseModel.JobBean subsBean = jobList.get(position);
        holder.bind(subsBean);


    }

    @Override
    public int getItemCount() {
        return jobList.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterOrganizationJobBinding mBinding;

        ListViewHolder(@NonNull AdapterOrganizationJobBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.joblLayout.setOnClickListener(this);
        }

        void bind(OrgJobListResponseModel.JobBean bean) {
            mBinding.tvJobTitle.setText(bean.jobTName);
            int color = Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256));
            mBinding.tvJobInitialLetter.setBackgroundTintList(ColorStateList.valueOf(color));
            mBinding.tvJobInitialLetter.setText(String.valueOf(bean.jobTName.charAt(0)).toUpperCase());
            mBinding.tvApplicant.setText(bean.jobCName);
            if (bean.prty)
                mBinding.priorityImg.setVisibility(View.VISIBLE);
            else
                mBinding.priorityImg.setVisibility(View.INVISIBLE);

        }

        @Override
        public void onClick(View v) {
            itemClick.onItemClick(getLayoutPosition(), jobList.get(getLayoutPosition()).jobId, jobList.get(getLayoutPosition()).jobTName, null);
        }
    }
}
