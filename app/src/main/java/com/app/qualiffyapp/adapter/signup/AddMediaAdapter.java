package com.app.qualiffyapp.adapter.signup;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.databinding.AdapterAddMediaBinding;
import com.app.qualiffyapp.models.createProfile.AddMediaModel;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.LinkedList;

import static com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick.ADD_IMG_CLICK_FLAG;
import static com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick.DELETE_FLAG;
import static com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick.VIEW_IMG_CLICK_FLAG;

public class AddMediaAdapter extends RecyclerView.Adapter<AddMediaAdapter.AddMediaViewHolder> {


    private LinkedList<AddMediaModel> mediaModelList;
    private RecyclerItemClick recyclerItemClick;

    public AddMediaAdapter(LinkedList<AddMediaModel> mediaModelList, RecyclerItemClick recyclerItemClick) {
        this.mediaModelList = mediaModelList;
        this.recyclerItemClick = recyclerItemClick;
    }

    @Override
    public void onBindViewHolder(AddMediaAdapter.AddMediaViewHolder holder, int position) {
        if (mediaModelList.get(position).getDrawable_img() != 0) {
            holder.adapterAddMediaBinding.addMediaImg.setImageResource(mediaModelList.get(position).getDrawable_img());
            holder.adapterAddMediaBinding.progressCircular.setVisibility(View.GONE);
            holder.adapterAddMediaBinding.deleteImg.setVisibility(View.GONE);
        } else if (mediaModelList.get(position).getImg_id() == null && mediaModelList.get(position).getDrawable_img() == 0) {
            holder.adapterAddMediaBinding.deleteImg.setVisibility(View.GONE);
            holder.adapterAddMediaBinding.progressCircular.setVisibility(View.VISIBLE);
            holder.adapterAddMediaBinding.addMediaImg.setImageResource(R.drawable.ic_photo_placeholder);
        } else {
            if (mediaModelList.get(position).getImgFile().startsWith("http"))
                GlideUtil.loadImage(holder.adapterAddMediaBinding.addMediaImg,
                        mediaModelList.get(position).getImgFile(),
                        R.drawable.ic_photo_placeholder,
                        100,
                        0
                );
            else {
                GlideUtil.loadImageFromFile(holder.adapterAddMediaBinding.addMediaImg, mediaModelList.get(position).getImgFile());
            }
            holder.adapterAddMediaBinding.deleteImg.setVisibility(View.VISIBLE);
            holder.adapterAddMediaBinding.progressCircular.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mediaModelList.size();
    }

    @Override
    public AddMediaAdapter.AddMediaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddMediaAdapter.AddMediaViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_add_media, parent, false));
    }


    public class AddMediaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AdapterAddMediaBinding adapterAddMediaBinding;

        public AddMediaViewHolder(AdapterAddMediaBinding listitem) {
            super(listitem.getRoot());
            this.adapterAddMediaBinding = listitem;
            this.adapterAddMediaBinding.deleteImg.setOnClickListener(this);
            this.adapterAddMediaBinding.addMediaImg.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.delete_img:
                    recyclerItemClick.onClick(DELETE_FLAG, getLayoutPosition());
                    break;
                case R.id.add_media_img:
                    if (mediaModelList.get(getLayoutPosition()).getDrawable_img() != 0) {
                        recyclerItemClick.onClick(ADD_IMG_CLICK_FLAG, getLayoutPosition());
                    } else {
                        recyclerItemClick.onClick(VIEW_IMG_CLICK_FLAG, getLayoutPosition());
                    }
                    break;

            }
        }
    }
}
