package com.app.qualiffyapp.adapter.dashboard.jobSeeker;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IDashboardItemClickListener;
import com.app.qualiffyapp.databinding.AdapterHomeCompanyDetailBinding;
import com.app.qualiffyapp.databinding.AdapterHomeDetailBinding;
import com.app.qualiffyapp.databinding.AdapterHomeImagesBinding;
import com.app.qualiffyapp.databinding.AdapterHomeOneBinding;
import com.app.qualiffyapp.databinding.CardPromotionBinding;
import com.app.qualiffyapp.databinding.ItemUserProfileCardBinding;
import com.app.qualiffyapp.models.home.jobSeeker.JsJobsResponseModel;
import com.app.qualiffyapp.utils.Utility;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class UserProfileCardAdapter extends RecyclerView.Adapter<UserProfileCardAdapter.UserProfileViewHolder> {
    private final int HOME_PAGE = 0;
    private final int DETAIL_PAGE = 1;
    private final int COMPANY_DETAIL_PAGE = 3;
    private final int PHOTO_PAGE = 2;
    private final int DEFAULT_PAGE = 5;
    private final int PROMOTION_PAGE = 6;
    private String imageUrl;
    private ArrayList<Object> arrayList;
    private String imageBasePath;
    private String promotionBasePath;
    private Context context;
    private IDashboardItemClickListener itemClickListener;

    private static String mOrgId = "0";


    UserProfileCardAdapter(Object obj, String bp, String pb, Context context, IDashboardItemClickListener itemClickListener) {
        this.imageBasePath = bp;
        this.promotionBasePath = pb;
        this.context = context;
        this.itemClickListener = itemClickListener;

        arrayList = new ArrayList<>();
        if (obj instanceof JsJobsResponseModel.JobBean)
        {
            JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) obj;
            jobBean.orgInfo.jobTitle = jobBean.jobTName;
            jobBean.orgInfo.rec = String.valueOf(jobBean.rec);
            jobBean.orgInfo.unrec = String.valueOf(jobBean.unrec);
            jobBean.orgInfo.flwCnt = jobBean.followCnt;
            jobBean.orgInfo.priority = jobBean.prty;
            jobBean.orgInfo.isRec = jobBean.is_recmd;
            jobBean.orgInfo.mOrgId = jobBean.orgId;


            mOrgId = jobBean.orgId;
            arrayList.add(jobBean.orgInfo);
            //Android Team
//            arrayList.add(jobBean.orgInfo.bio);
//            arrayList.add(jobBean);

/*if (jobBean.orgInfo.imgs != null && jobBean.orgInfo.imgs.size()>0)
arrayList.add(jobBean.orgInfo.imgs);*/
        } else if (obj instanceof JsJobsResponseModel.Promotions)
        {
//            JsJobsResponseModel.Promotions promotion = (JsJobsResponseModel.Promotions) obj;
//            arrayList.add(obj);
//            arrayList.add(promotion.desc);

        }
    }

    @NonNull
    @Override
    public UserProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case PROMOTION_PAGE:
                return new UserProfileViewHolder((CardPromotionBinding) DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.card_promotion, viewGroup, false));
            case HOME_PAGE:
                return new UserProfileViewHolder((AdapterHomeOneBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_one, viewGroup, false));
            case DETAIL_PAGE:
                return new UserProfileViewHolder((AdapterHomeDetailBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_detail, viewGroup, false));

            case PHOTO_PAGE:
                return new UserProfileViewHolder((AdapterHomeImagesBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_images, viewGroup, false));


            case COMPANY_DETAIL_PAGE:
                return new UserProfileViewHolder((AdapterHomeCompanyDetailBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_company_detail, viewGroup, false));


            case DEFAULT_PAGE:
                return new UserProfileViewHolder((ItemUserProfileCardBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.item_user_profile_card, viewGroup, false));


        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0: {
                Object obj = arrayList.get(0);
                if (obj instanceof JsJobsResponseModel.JobBean.OrgInfoBean)
                    return HOME_PAGE;
                else return PROMOTION_PAGE;
            }
            case 1:
                return DETAIL_PAGE;
            case 2:
                return COMPANY_DETAIL_PAGE;

            case 3:
                return PHOTO_PAGE;

            default:
                return DEFAULT_PAGE;


        }
    }

    @Override
    public void onBindViewHolder(@NonNull UserProfileViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case PROMOTION_PAGE:
                JsJobsResponseModel.Promotions prmt = (JsJobsResponseModel.Promotions) arrayList.get(position);
                //GlideUtil.loadCircularImage(holder.promotionBinding.homeImg, promotionBasePath + prmt.img, R.drawable.profile_placeholder);
                Glide.with(holder.promotionBinding.homeImg).load(promotionBasePath + prmt.img).fitCenter().into(holder.promotionBinding.homeImg);
                holder.promotionBinding.tvPromotionName.setText(prmt.n_loc);
                if (prmt.f_count != null)
                    holder.promotionBinding.tvFollowCount.setText(prmt.f_count + " " + Utility.singularPluralText("Follower", Integer.parseInt(prmt.f_count)));
                else
                    holder.promotionBinding.tvFollowCount.setText("0 Follower");


                break;
            case HOME_PAGE:
                JsJobsResponseModel.JobBean.OrgInfoBean orgInfoBean = (JsJobsResponseModel.JobBean.OrgInfoBean) arrayList.get(position);

//                GlideUtil.loadCircularImage(holder.homeOneBinding.homeImg, imageBasePath + orgInfoBean.img, R.drawable.profile_placeholder);
                Log.e("IMAGEURL : ", imageBasePath + orgInfoBean.img);
                Glide.with(holder.homeOneBinding.homeImg).load(imageBasePath + orgInfoBean.img).into(holder.homeOneBinding.homeImg);
                if (orgInfoBean.r_name != null && !TextUtils.isEmpty(orgInfoBean.r_name))
                    holder.homeOneBinding.tvCompanyName.setText(orgInfoBean.r_name);
                else
                    holder.homeOneBinding.tvCompanyName.setText(orgInfoBean.name);
                holder.homeOneBinding.tvCompanyLoc.setText(orgInfoBean.jobTitle);//jobBean.orgInfo.state
                holder.homeOneBinding.tvCompanyAddress.setText(orgInfoBean.state);//


                holder.homeOneBinding.tvRecomCount.setText(orgInfoBean.rec);
                holder.homeOneBinding.tvNotRecomCount.setText(orgInfoBean.unrec);
                if (orgInfoBean.flwCnt != null) {
//Android Team Zb
                    holder.homeOneBinding.tvComFollowers.setText(orgInfoBean.flwCnt.equals("") ? "0" : orgInfoBean.flwCnt);
                    //holder.homeOneBinding.tvComFollowers.setText(orgInfoBean.flwCnt + " " + Utility.singularPluralText(context.getResources().getString(R.string.followers), Integer.parseInt(orgInfoBean.flwCnt)));
                }
                if (orgInfoBean.priority)
                    holder.homeOneBinding.priorityImg.setVisibility(View.VISIBLE);
                else holder.homeOneBinding.priorityImg.setVisibility(View.GONE);

                holder.homeOneBinding.tvIoffer.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
                holder.homeOneBinding.tvRecommendTxt.setOnClickListener(v ->

                        itemClickListener.recomendJobClick(1, orgInfoBean.mOrgId)
                );

                holder.homeOneBinding.tvPedecure.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
                holder.homeOneBinding.tvManicular.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
                holder.homeOneBinding.tvAboutMe.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
                holder.homeOneBinding.tvHaircut.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());
                holder.homeOneBinding.ivShareIcon.setOnClickListener(v -> Toast.makeText(context, "Click", Toast.LENGTH_SHORT).show());

                break;

            case DETAIL_PAGE:
                String companyDetail = (String) arrayList.get(position);
                // holder.homeDetailBinding.tvDetail.setText(companyDetail);

                break;


            case PHOTO_PAGE:
                List<String> imgs = (List<String>) arrayList.get(position);
                for (String imgsStr : imgs) {
                    ImageView jobImageView = new ImageView(context);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    jobImageView.setLayoutParams(layoutParams);
                    holder.homeImagesBinding.jobsImglLayout.addView(jobImageView);
                    jobImageView.setAdjustViewBounds(true);

                    Glide.with(jobImageView).load(imageBasePath + imgsStr).placeholder(R.drawable.user_placeholder).into(jobImageView);
                }

                break;

            case COMPANY_DETAIL_PAGE:
                JsJobsResponseModel.JobBean jobBean = (JsJobsResponseModel.JobBean) arrayList.get(position);
                holder.homeCompanyDetailBinding.tvJobExperience.setText(jobBean.exp + Utility.singularPluralText(" year", jobBean.exp));
                holder.homeCompanyDetailBinding.tvSalary.setText(jobBean.slry);
                holder.homeCompanyDetailBinding.tvDate.setText(Utility.convertDate(jobBean.date));
                holder.homeCompanyDetailBinding.tvJobTitle.setText(jobBean.jobTName);
                holder.homeCompanyDetailBinding.tvJobAddress.setText(jobBean.state);
                holder.homeCompanyDetailBinding.tvJobDescription.setText(jobBean.desc);
                holder.homeCompanyDetailBinding.tvCompanyName.setText(jobBean.orgInfo.name + " (" + jobBean.jobCName + ")");
                holder.homeCompanyDetailBinding.tvCompanyAddress.setText(jobBean.orgInfo.state);
                break;

            case DEFAULT_PAGE:
                Glide.with(holder.imageView.itemImage).load(imageUrl).into(holder.imageView.itemImage);

                break;
        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class UserProfileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemUserProfileCardBinding imageView;
        CardPromotionBinding promotionBinding;
        AdapterHomeOneBinding homeOneBinding;
        // AdapterHomeDetailBinding homeDetailBinding;
        AdapterHomeImagesBinding homeImagesBinding;
        AdapterHomeCompanyDetailBinding homeCompanyDetailBinding;


        UserProfileViewHolder(@NonNull ItemUserProfileCardBinding itemView) {
            super(itemView.getRoot());
            this.imageView = itemView;
        }

        UserProfileViewHolder(@NonNull CardPromotionBinding itemView) {
            super(itemView.getRoot());
            this.promotionBinding = itemView;
        }

        UserProfileViewHolder(@NonNull AdapterHomeCompanyDetailBinding homeCompanyDetailBinding) {
            super(homeCompanyDetailBinding.getRoot());
            this.homeCompanyDetailBinding = homeCompanyDetailBinding;
        }

        UserProfileViewHolder(@NonNull AdapterHomeOneBinding homeOneBinding) {
            super(homeOneBinding.getRoot());
            this.homeOneBinding = homeOneBinding;


            homeOneBinding.tvRecomCount.setOnClickListener(this);
            homeOneBinding.tvNotRecomCount.setOnClickListener(this);
            homeOneBinding.tvNotREcommentTXt.setOnClickListener(this);
            homeOneBinding.mediaPlay.setOnClickListener(this);
        }

        UserProfileViewHolder(@NonNull AdapterHomeImagesBinding homeImagesBinding) {
            super(homeImagesBinding.getRoot());
            this.homeImagesBinding = homeImagesBinding;
        }

        UserProfileViewHolder(@NonNull AdapterHomeDetailBinding homeDetailBinding) {
            super(homeDetailBinding.getRoot());
            //  this.homeDetailBinding = homeDetailBinding;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.tvNotRecomCount:
/* if(((JsJobsResponseModel.JobBean) arrayList.get(2)).unrec!=0)
itemClickListener.recomendCount(0,((JsJobsResponseModel.JobBean) arrayList.get(2)).orgId);*/
// itemClickListener.recomendJobClick(0, ((JsJobsResponseModel.JobBean) arrayList.get(2)).orgId);

                    break;

                case R.id.tvNotREcommentTXt:
                    itemClickListener.recomendJobClick(0, ((JsJobsResponseModel.JobBean) arrayList.get(0)).orgId);
                    break;
                case R.id.mediaPlay:
                    itemClickListener.mediaPlay();
                    break;


            }
        }
    }
}