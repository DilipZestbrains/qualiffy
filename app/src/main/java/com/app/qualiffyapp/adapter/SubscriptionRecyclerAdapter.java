package com.app.qualiffyapp.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemSubscriptionBinding;

import java.util.ArrayList;

public class SubscriptionRecyclerAdapter extends RecyclerView.Adapter<SubscriptionRecyclerAdapter.SubscriptionViewHolder> {
    private ArrayList<Object> subscriptionList;

    public SubscriptionRecyclerAdapter(ArrayList<Object> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }

    @NonNull
    @Override
    public SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSubscriptionBinding mBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_subscription, parent, false);
        return new SubscriptionViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull SubscriptionViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    class SubscriptionViewHolder extends RecyclerView.ViewHolder {
        private final ItemSubscriptionBinding mBinding;

        SubscriptionViewHolder(@NonNull ItemSubscriptionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        void bind(Object data) {

        }
    }
}
