package com.app.qualiffyapp.adapter.dashboard.jobSeeker;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IDashboardItemClickListener;
import com.app.qualiffyapp.databinding.ItemSwipeCardBinding;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class CardStackAdapter extends RecyclerView.Adapter<CardStackAdapter.CardViewHolder> {
    private List spots;
    private String bp;
    private String pb;
    private IDashboardItemClickListener itemClickListener;

    @NotNull
    private Context context;


    public CardStackAdapter(List spots, String bp,String pb, @NotNull Context context, IDashboardItemClickListener itemClickListener) {
        super();
        this.spots = spots;
        this.bp = bp;
        this.pb = pb;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }

    @NotNull
    @Override
    public CardViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new CardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_swipe_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull final CardViewHolder holder, int position) {
       Object jobBean = (Object) this.spots.get(position);
        UserProfileCardAdapter adapter = new UserProfileCardAdapter(jobBean, bp,pb, context, itemClickListener);
        holder.binding.rvUserProfileCard.setHasFixedSize(true);
        holder.binding.rvUserProfileCard.setItemViewCacheSize(50);
        holder.binding.rvUserProfileCard.setDrawingCacheEnabled(true);
        holder.binding.rvUserProfileCard.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        holder.binding.rvUserProfileCard.setAdapter(adapter);
    }

    public int getItemCount() {
        return this.spots.size();
    }

    @NotNull
    public final List getSpots() {
        return this.spots;
    }

    public final void setSpots(@NotNull List spots) {
        this.spots = spots;
    }

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    public final void setContext(@NotNull Context var1) {
        this.context = var1;
    }

    final class CardViewHolder extends RecyclerView.ViewHolder {
        ItemSwipeCardBinding binding;

        CardViewHolder(ItemSwipeCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rvUserProfileCard.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int offset = recyclerView.computeVerticalScrollOffset();
                    int extent = recyclerView.computeVerticalScrollExtent();
                    int range = recyclerView.computeVerticalScrollRange();

                    int percentage = (int) (100.0 * offset / (float) (range - extent));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.seekbarReverse.setProgress(percentage + 10, true);
                    } else {
                        binding.seekbarReverse.setProgress(percentage + 10);
                    }
                }

                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);

                }
            });
        }
    }
}
