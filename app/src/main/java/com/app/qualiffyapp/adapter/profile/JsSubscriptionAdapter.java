package com.app.qualiffyapp.adapter.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.AdapetJsSubscriptionBinding;
import com.app.qualiffyapp.databinding.AdapterFreeSubscripitonBinding;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.payment.ui.PaymentActivity;
import com.google.gson.Gson;

import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.MONEY_TO_PAY;

public class JsSubscriptionAdapter extends RecyclerView.Adapter<JsSubscriptionAdapter.SubscriptionViewHolder> {
    private List<JsSubscriptionModel.SubsBean> subscriptionList;
    private final int FREE_SUBS = 101;
    private final int OTHER_SUBS = 102;

    public JsSubscriptionAdapter(List<JsSubscriptionModel.SubsBean> subscriptionList) {
        this.subscriptionList = subscriptionList;
    }

    @NonNull
    @Override
    public JsSubscriptionAdapter.SubscriptionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == OTHER_SUBS)
            return new JsSubscriptionAdapter.SubscriptionViewHolder((AdapetJsSubscriptionBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapet_js_subscription, parent, false));
        else
            return new JsSubscriptionAdapter.SubscriptionViewHolder((AdapterFreeSubscripitonBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_free_subscripiton, parent, false));

    }

    @Override
    public int getItemViewType(int position) {
        if(subscriptionList.get(position).id!=null)
            return OTHER_SUBS;
        else return FREE_SUBS;
//        return super.getItemViewType(position);
    }

    @Override
    public void onBindViewHolder(@NonNull JsSubscriptionAdapter.SubscriptionViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case OTHER_SUBS:
            JsSubscriptionModel.SubsBean subsBean = subscriptionList.get(position);
            holder.bind(subsBean);
            break;
        }
    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }

    class SubscriptionViewHolder extends RecyclerView.ViewHolder {
        private  AdapetJsSubscriptionBinding mBinding;
        private  AdapterFreeSubscripitonBinding freeSubscripitonBinding;

        SubscriptionViewHolder(@NonNull AdapetJsSubscriptionBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }

        SubscriptionViewHolder(@NonNull AdapterFreeSubscripitonBinding freeSubscripitonBinding) {
            super(freeSubscripitonBinding.getRoot());
            this.freeSubscripitonBinding = freeSubscripitonBinding;
        }

        void bind(JsSubscriptionModel.SubsBean subsBean) {
            String[] dur = mBinding.tvDurationDate.getContext().getResources().getStringArray(R.array.duration);
            mBinding.tvDurationDate.setText(dur[subsBean.duration]);
            mBinding.tvPackageName.setText(subsBean.name);
            mBinding.tvPrice.setText("$" + subsBean.price);
            if (subsBean.status == 0) {
                mBinding.tvRenewNow.setOnClickListener(null);
                mBinding.tvRenewNow.setText(mBinding.tvRenewNow.getContext().getResources().getString(R.string.subscribed));
            } else if (subsBean.status == 1) {
                mBinding.tvRenewNow.setText(mBinding.tvRenewNow.getContext().getResources().getString(R.string.RENEW_NOW));
                mBinding.tvRenewNow.setOnClickListener(v -> {
                    Bundle bundle = new Bundle();
                    bundle.putString(MONEY_TO_PAY, new Gson().toJson(subsBean));
                    Intent intent = new Intent(mBinding.tvRenewNow.getContext(), PaymentActivity.class);
                    intent.putExtra(BUNDLE, bundle);
                    mBinding.tvRenewNow.getContext().startActivity(intent);
                });
            }
        }
    }

}

