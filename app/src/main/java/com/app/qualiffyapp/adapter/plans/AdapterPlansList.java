package com.app.qualiffyapp.adapter.plans;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.plans.IUgradePlan;
import com.app.qualiffyapp.databinding.ItemPlansBinding;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;

import java.util.List;

public class AdapterPlansList extends RecyclerView.Adapter<AdapterPlansList.RecycleViewHolder> {
    public List<JsSubscriptionModel.SubsBean> subscriptionList;
    private IUgradePlan upgradePlan;
    private Context context;

    public AdapterPlansList(Context context, List<JsSubscriptionModel.SubsBean> subscriptionList, IUgradePlan upgradePlan) {
        this.subscriptionList = subscriptionList;
        this.upgradePlan = upgradePlan;
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterPlansList.RecycleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdapterPlansList.RecycleViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_plans, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPlansList.RecycleViewHolder holder, int position) {
        JsSubscriptionModel.SubsBean subsBean = subscriptionList.get(position);
        holder.bind(subsBean);


    }

    @Override
    public int getItemCount() {
        return subscriptionList.size();
    }

    class RecycleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ItemPlansBinding mBinding;

        RecycleViewHolder(@NonNull ItemPlansBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.tvUpgrade.setOnClickListener(this);

        }

        void bind(JsSubscriptionModel.SubsBean subsBean) {
            String dur[] = context.getResources().getStringArray(R.array.duration);
            mBinding.tvDurationDate.setText(dur[subsBean.duration]);
            if (subsBean.duration == 9)
                mBinding.imgPremium.setVisibility(View.VISIBLE);

            mBinding.tvPackageName.setText(subsBean.name);
            mBinding.tvPrice.setText(context.getString(R.string.dollar) + subsBean.price);
            if (subsBean.my_plan) {
                mBinding.tvUpgrade.setText("SUBSCRIBED");
                mBinding.tvUpgrade.setEnabled(false);
            } else {
                mBinding.tvUpgrade.setText("UPGRADE");
                mBinding.tvUpgrade.setEnabled(true);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvUpgrade:
                    switch (subscriptionList.get(getLayoutPosition()).duration) {
                        case 9:
                            upgradePlan.premiumClick(getLayoutPosition(), subscriptionList.get(getLayoutPosition()));
                            break;
                        default:
                            upgradePlan.upgradeClick(getLayoutPosition(), subscriptionList.get(getLayoutPosition()));

                            break;
                    }
                    break;
            }
        }
    }
}