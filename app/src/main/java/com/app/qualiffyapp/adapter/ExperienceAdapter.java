package com.app.qualiffyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemEducationBinding;

import java.util.List;

public class ExperienceAdapter extends RecyclerView.Adapter<ExperienceAdapter.EducationViewHolder> {
    private List<String> list;

    public ExperienceAdapter(List<String> list) {
        this.list = list;
    }


    @NonNull
    @Override
    public EducationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemEducationBinding view = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_education, viewGroup, false);
        return new EducationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EducationViewHolder educationViewHolder, int i) {
        educationViewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        if (list != null)
            return list.size();
        else return 0;
    }

    public void refreshList(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    class EducationViewHolder extends RecyclerView.ViewHolder {
        private ItemEducationBinding binding;

        EducationViewHolder(@NonNull ItemEducationBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(int position) {
            binding.ivEditPencil.setVisibility(View.GONE);
            binding.tvText.setText(list.get(position));
            if (position == list.size() - 1) {
                binding.bottomLine.setVisibility(View.GONE);
            } else if (binding.bottomLine.getVisibility() == View.GONE) {
                binding.bottomLine.setVisibility(View.VISIBLE);
            }
        }
    }

}
