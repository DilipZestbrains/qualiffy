package com.app.qualiffyapp.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;

public class CommonPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments = new ArrayList<>();
    private ArrayList<String> mTitle = new ArrayList<>();

    public CommonPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(Fragment fragment, String title) {
        fragments.add(fragment);
        mTitle.add(title);
    }

    public void addView(Fragment fragment, int index) {
        fragments.add(index, fragment);
        mTitle.add(index, "");
        notifyDataSetChanged();
    }

    public void addView(Fragment fragment, int index, String title) {
        fragments.add(index, fragment);
        mTitle.add(index, title);
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        // refresh all fragments when data set changed
        return PagerAdapter.POSITION_NONE;
    }

    public void removeView(int index) {
        fragments.remove(index);
        mTitle.remove(index);
        notifyDataSetChanged();
    }

    public void removeAllFragment() {
        fragments.clear();
        mTitle.clear();
    }

    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mTitle.get(position);
    }
}
