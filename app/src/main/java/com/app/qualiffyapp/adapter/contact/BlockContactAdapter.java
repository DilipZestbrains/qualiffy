package com.app.qualiffyapp.adapter.contact;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.AdapterRecommendListBinding;
import com.app.qualiffyapp.models.contact.BlockContactResonseModel;
import com.app.qualiffyapp.ui.activities.profile.otherUser.OtherUserProfile;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;

public class BlockContactAdapter extends RecyclerView.Adapter<BlockContactAdapter.RecommendViewHolder> {
    private List<BlockContactResonseModel.UsrBean> recommendList;
    private String basePath;
    private Context context;


    public BlockContactAdapter(List<BlockContactResonseModel.UsrBean> usr, String basePath, Context context) {
        this.recommendList = usr;
        this.basePath = basePath;
        this.context = context;
    }

    @NonNull
    @Override
    public BlockContactAdapter.RecommendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BlockContactAdapter.RecommendViewHolder((AdapterRecommendListBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_recommend_list, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull BlockContactAdapter.RecommendViewHolder holder, int position) {
        BlockContactResonseModel.UsrBean subsBean = recommendList.get(position);
        holder.bind(subsBean);
    }

    @Override
    public int getItemCount() {
        return recommendList.size();
    }

    public void addAll(List<BlockContactResonseModel.UsrBean> usr) {
        this.recommendList.addAll(usr);
        notifyDataSetChanged();
    }

    class RecommendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterRecommendListBinding mBinding;

        RecommendViewHolder(@NonNull AdapterRecommendListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.mainLlayout.setOnClickListener(this);
            this.mBinding.tvPno.setVisibility(View.VISIBLE);
        }

        void bind(BlockContactResonseModel.UsrBean bean) {
            if (bean.username == null || bean.username.equals(""))
                mBinding.tvUserName.setText(bean.fName + " " + bean.lName);
            else
                mBinding.tvUserName.setText(bean.username);

            mBinding.tvPno.setText(bean.pno);
            GlideUtil.loadCircularImage(mBinding.userImg, basePath + bean.img, R.drawable.user_placeholder);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mainLlayout:
                    Intent intent = new Intent(context, OtherUserProfile.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(USER_ID, recommendList.get(getLayoutPosition()).id);
                    intent.putExtra(BUNDLE, bundle);
                    context.startActivity(intent);
                    break;
            }
        }
    }
}
