package com.app.qualiffyapp.adapter.signup;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick;
import com.app.qualiffyapp.databinding.AdapterAddExperienceBinding;
import com.app.qualiffyapp.models.ExpDetailBean;

import java.util.List;

import static com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick.DELETE_FLAG;
import static com.app.qualiffyapp.callbacks.signupLogin.RecyclerItemClick.EDIT_FLAG;

public class ExperienceListAdapter extends RecyclerView.Adapter<ExperienceListAdapter.ExperienceViewHolder> {


    private List<ExpDetailBean> experienceList;
    private RecyclerItemClick recyclerItemClick;


    public ExperienceListAdapter(List<ExpDetailBean> experienceList, RecyclerItemClick recyclerItemClick) {
        this.experienceList = experienceList;
        this.recyclerItemClick = recyclerItemClick;
    }

    @Override
    public void onBindViewHolder(ExperienceListAdapter.ExperienceViewHolder holder, int position) {


        holder.ExperienceListBinding.expTxt.setText(experienceList.get(position).title);

    }

    @Override
    public int getItemCount() {
        if (experienceList == null)
            return 0;
        else
            return experienceList.size();
    }

    @Override
    public ExperienceListAdapter.ExperienceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ExperienceListAdapter.ExperienceViewHolder((AdapterAddExperienceBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_add_experience, parent, false));

    }

    public class ExperienceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AdapterAddExperienceBinding ExperienceListBinding;

        public ExperienceViewHolder(AdapterAddExperienceBinding listitem) {
            super(listitem.getRoot());
            this.ExperienceListBinding = listitem;
            this.ExperienceListBinding.deleteImg.setOnClickListener(this);
            this.ExperienceListBinding.editImg.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.delete_img:
                    recyclerItemClick.onClick(DELETE_FLAG, getLayoutPosition());
                    break;
                case R.id.edit_img:
                    recyclerItemClick.onClick(EDIT_FLAG, getLayoutPosition());
                    break;
            }
        }
    }
}
