package com.app.qualiffyapp.adapter.dashboard.jobSeeker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.AdapterRecommendListBinding;
import com.app.qualiffyapp.models.home.jobSeeker.RecommendResponseModel;
import com.app.qualiffyapp.ui.activities.dashboard.jobSeeker.view.RecommendListActivity;
import com.app.qualiffyapp.ui.activities.profile.otherUser.OtherUserProfile;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;

import java.util.List;

import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.USER_ID;

public class RecommendListAdapter extends RecyclerView.Adapter<RecommendListAdapter.RecommendViewHolder> {
    private List recommendList;
    private String basePath;
    private String usr_bP;
    private String org_bP;
    private Context context;


    public RecommendListAdapter(List usr, String basePath, Context context) {
        this.recommendList = usr;
        this.basePath = basePath;
        this.context = context;
    }

    public RecommendListAdapter(List usr, String org_bP, String usr_bP, Context context) {
        this.recommendList = usr;
        this.org_bP = org_bP;
        this.usr_bP = usr_bP;
        this.context = context;
    }


    @NonNull
    @Override
    public RecommendListAdapter.RecommendViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecommendListAdapter.RecommendViewHolder((AdapterRecommendListBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_recommend_list, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecommendListAdapter.RecommendViewHolder holder, int position) {
        if (recommendList.get(position) instanceof RecommendResponseModel.UsrBean) {
            RecommendResponseModel.UsrBean subsBean = (RecommendResponseModel.UsrBean) recommendList.get(position);
            holder.bind(subsBean);
        } else if (recommendList.get(position) instanceof RecommendResponseModel.Badges) {
            RecommendResponseModel.Badges badges = (RecommendResponseModel.Badges) recommendList.get(position);
            holder.bind(badges);
        }


    }

    @Override
    public int getItemCount() {
        return recommendList.size();
    }

    public void addAll(List usr) {
        this.recommendList.addAll(usr);
        notifyDataSetChanged();
    }

    class RecommendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final AdapterRecommendListBinding mBinding;

        RecommendViewHolder(@NonNull AdapterRecommendListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
            this.mBinding.mainLlayout.setOnClickListener(this);
        }

        void bind(RecommendResponseModel.UsrBean bean) {
            if (bean.userId != null && !bean.userId.equals(Utility.getStringSharedPreference(context, USER_ID)))
            {
                if (TextUtils.isEmpty(bean.username))
                    mBinding.tvUserName.setText(bean.fName + " " + bean.lName);
                else
                    mBinding.tvUserName.setText(bean.username);
            }
            else mBinding.tvUserName.setText("you");
            GlideUtil.loadCircularImage(mBinding.userImg, basePath + bean.img, R.drawable.user_placeholder);
        }

        public void bind(RecommendResponseModel.Badges badges) {
            mBinding.tvUserName.setText(badges.data.name);
            GlideUtil.loadCircularImage(mBinding.userImg, (badges.type == 1 ? org_bP : usr_bP) + badges.data.img, R.drawable.user_placeholder);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mainLlayout:

                    if (recommendList.get(getLayoutPosition()) instanceof RecommendResponseModel.UsrBean) {
                        String userId = ((RecommendResponseModel.UsrBean) recommendList.get(getLayoutPosition())).userId;
                        if (userId != null && !userId.equals(Utility.getStringSharedPreference(context, USER_ID))) {
                            Intent intent = new Intent(context, OtherUserProfile.class);
                            Bundle bundle = new Bundle();
                            bundle.putString(USER_ID, userId);
                            intent.putExtra(BUNDLE, bundle);
                            context.startActivity(intent);
                        }
                    }

                    break;
            }
        }


    }
}
