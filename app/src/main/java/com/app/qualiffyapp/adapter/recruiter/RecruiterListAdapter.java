package com.app.qualiffyapp.adapter.recruiter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.AdapterRecruiterListBinding;
import com.app.qualiffyapp.models.recruiter.RecruiterListResponseModel;

import java.util.List;

public class RecruiterListAdapter extends RecyclerView.Adapter<RecruiterListAdapter.RViewHolder> {
    public List<RecruiterListResponseModel.RecrBean> recruiterList;
    private Context context;
    private RecruiterListener recruiterListener;


    public RecruiterListAdapter(List<RecruiterListResponseModel.RecrBean> recruiterList,  Context context) {
        this.recruiterList = recruiterList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecruiterListAdapter.RViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecruiterListAdapter.RViewHolder((AdapterRecruiterListBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.adapter_recruiter_list, parent, false));

    }

    @Override
    public void onBindViewHolder(@NonNull RecruiterListAdapter.RViewHolder holder, int position) {
        RecruiterListResponseModel.RecrBean subsBean = recruiterList.get(position);
        holder.bind(subsBean);
    }

    public void setRecruiterListener(RecruiterListener recruiterListener) {
        this.recruiterListener = recruiterListener;
    }

    @Override
    public int getItemCount() {
        return recruiterList.size();
    }

    public void addAll(List<RecruiterListResponseModel.RecrBean> recruiterList) {
        this.recruiterList.addAll(recruiterList);
        notifyDataSetChanged();
    }

    public void deleteRecruiter(RecruiterListResponseModel.RecrBean bean, int p) {
        if (bean != null) {
            int position = -1;
            for (RecruiterListResponseModel.RecrBean b : recruiterList) {
                if (b._id.equals(bean._id)) {
                    position = recruiterList.indexOf(b);
                    break;
                }
            }
            if (position >= 0) {
                recruiterList.remove(position);
                if (p >= 0)
                    notifyItemRemoved(p);
            }
        }
    }

    class RViewHolder extends RecyclerView.ViewHolder  {
        private final AdapterRecruiterListBinding mBinding;
        RViewHolder(@NonNull AdapterRecruiterListBinding mBinding) {
            super(mBinding.getRoot());
            this.mBinding = mBinding;
        }
        void bind(RecruiterListResponseModel.RecrBean bean) {
            mBinding.tvName.setText(bean.name);
            mBinding.tvEmail.setText(bean.email);
            mBinding.tvNumber.setText(bean.pno);
            mBinding.ivDelete.setOnClickListener(v -> {
                if (recruiterListener != null)
                    recruiterListener.onDeleteRecruiter(bean, getAdapterPosition());
            });
        }
    }

    public interface RecruiterListener {
        void onDeleteRecruiter(RecruiterListResponseModel.RecrBean bean, int adapterPosition);
    }
}
