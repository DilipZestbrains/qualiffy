package com.app.qualiffyapp.adapter.dashboard.business;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationDasbboardItemClick;
import com.app.qualiffyapp.databinding.ItemSwipeCardBinding;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_BUSINESS_ADD_VIEW_COUNT;

public class BusinessCardstackAdapter extends RecyclerView.Adapter<BusinessCardstackAdapter.CardViewHolder> {
    private List spots;
    private String bp;
    private Context context;
    private IOrganizationDasbboardItemClick itemClick;
    private OnScrollListener onScrollListener;
    private boolean isScrolled = false;

    public BusinessCardstackAdapter(List spots, String bp, IOrganizationDasbboardItemClick itemClick, Context context) {
        super();
        this.spots = spots;
        this.bp = bp;
        this.context = context;
        this.itemClick = itemClick;
    }

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.onScrollListener = onScrollListener;
    }

    @NotNull
    @Override
    public BusinessCardstackAdapter.CardViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        return new BusinessCardstackAdapter.CardViewHolder(DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_swipe_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NotNull final BusinessCardstackAdapter.CardViewHolder holder, int position) {
        BusinessHomeResponseModel.JobBean jobBean = (BusinessHomeResponseModel.JobBean) this.spots.get(position);
        BusinessProfileCardAdapter adapter = new BusinessProfileCardAdapter(jobBean, bp, context, itemClick);
        holder.binding.rvUserProfileCard.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(holder.binding.rvUserProfileCard.getContext());
        holder.binding.rvUserProfileCard.setLayoutManager(layoutManager);
        holder.binding.rvUserProfileCard.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                if (onScrollListener != null) {
                    int visibleItemCount = holder.binding.rvUserProfileCard.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItem) >= totalItemCount) {
                        onScrollListener.onScroll(jobBean);
                    }
                }
            }
        });
    }

    public int getItemCount() {
        return this.spots.size();
    }

    @NotNull
    public final List getSpots() {
        return this.spots;
    }

    public final void setSpots(@NotNull List spots) {
        this.spots = spots;
    }

    @NotNull
    public final Context getContext() {
        return this.context;
    }

    public final void setContext(@NotNull Context var1) {
        this.context = var1;
    }

    final class CardViewHolder extends RecyclerView.ViewHolder {
        ItemSwipeCardBinding binding;

        CardViewHolder(ItemSwipeCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.rvUserProfileCard.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    int offset = recyclerView.computeVerticalScrollOffset();
                    int extent = recyclerView.computeVerticalScrollExtent();
                    int range = recyclerView.computeVerticalScrollRange();

                    int percentage = (int) (100.0 * offset / (float) (range - extent));

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        binding.seekbarReverse.setProgress(percentage + 10, true);
                    } else {
                        binding.seekbarReverse.setProgress(percentage + 10);
                    }
                }

                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);


                }
            });
        }
    }

    public interface OnScrollListener {
        void onScroll(BusinessHomeResponseModel.JobBean jobBean);
    }

}
