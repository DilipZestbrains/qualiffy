package com.app.qualiffyapp.adapter.contact;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.contact.IContactClick;
import com.app.qualiffyapp.databinding.ItemContactBinding;
import com.app.qualiffyapp.firebase.chat.types.FriendStatus;
import com.app.qualiffyapp.localDatabase.entity.Contact;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.AppConstants.ACCEPT_OR_REJECT;
import static com.app.qualiffyapp.constants.AppConstants.CONTACT_INVITE;
import static com.app.qualiffyapp.constants.AppConstants.FRND;
import static com.app.qualiffyapp.constants.AppConstants.FRND_DETAIL;
import static com.app.qualiffyapp.constants.AppConstants.SEND_FRIEND_REQUEST;

public class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactRecyclerAdapter.ContactViewHolder> {
    private ArrayList<Contact> contacts;
    private ArrayList<Contact> filteredContacts;
    private IContactClick contactClick;

    public ContactRecyclerAdapter(ArrayList<Contact> contacts, IContactClick contactClick) {
        this.contacts = contacts;
        filteredContacts = contacts;
        this.contactClick = contactClick;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemContactBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_contact, parent, false);
        return new ContactViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.bind(filteredContacts.get(position));
    }

    @Override
    public int getItemCount() {
        return filteredContacts.size();
    }

    public int filter(String str) {
        ArrayList<Contact> contacts1 = new ArrayList<>();
        if (!TextUtils.isEmpty(str)) {
            for (Contact contact : contacts) {
                if (contact.getUsername().toLowerCase().startsWith(str.toLowerCase()))
                    contacts1.add(contact);
                if (contact.getPno().startsWith(str))
                    contacts1.add(contact);
            }
        } else {
            contacts1 = contacts;
        }

        filteredContacts = contacts1;
        notifyDataSetChanged();
        return contacts1.size();
    }

    public void refreshList(ArrayList<Contact> contacts) {
        this.contacts = contacts;
        filteredContacts = contacts;
        notifyDataSetChanged();
    }

    public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemContactBinding binding;

        ContactViewHolder(@NonNull ItemContactBinding itemView) {
            super(itemView.getRoot());
            binding = itemView;
            binding.tvInvite.setOnClickListener(this);
            binding.ivSideIcon.setOnClickListener(this);
            binding.mainConLayout.setOnClickListener(this);
        }

        public void bind(Contact contact) {
            binding.tvUserName.setText(contact.getUsername());
            binding.tvPhoneNumber.setText(contact.getPno());
            if (contact.getImg() != null)
                GlideUtil.loadCircularImage(binding.userImage, contact.getImg(), R.drawable.user_placeholder);
            else {
                binding.userImage.setImageResource(R.drawable.user_placeholder);
            }
            if (contact.getIs_frnd() != null) {
                binding.tvInvite.setVisibility(View.GONE);
                if (contact.getIs_frnd().equals(FriendStatus.REQUEST_NOT_SEND.value)) {
                    binding.ivSideIcon.setVisibility(View.VISIBLE);
                    binding.tvInvite.setVisibility(View.GONE);
                    binding.ivSideIcon.setImageResource(R.drawable.ic_add_contact);
                    binding.ivSideIcon.setOnClickListener(v -> {
                        if (contact.get_id() != null) {
                            contactClick.onContactClick(contact, SEND_FRIEND_REQUEST);
                        }
                    });

                } else if (contact.getIs_frnd().equals(FriendStatus.REQUEST_ACCEPTED.value)) {
                    binding.ivSideIcon.setVisibility(View.GONE);
                    binding.tvInvite.setVisibility(View.GONE);
                   /* binding.getRoot().setOnClickListener(v -> {
                        contactClick.openContactProfile(fr.get_id());
                    });*/
                } else if (contact.getIs_frnd().equals(FriendStatus.REQUEST_SEND_BY_YOU.value)) {
                    binding.ivSideIcon.setVisibility(View.VISIBLE);
                    binding.tvInvite.setVisibility(View.GONE);
                    binding.ivSideIcon.setImageResource(R.drawable.ic_sent);

                } else if (contact.getIs_frnd().equals(FriendStatus.REQUEST_SENT_TO_YOU.value)) {
                    binding.ivSideIcon.setVisibility(View.VISIBLE);
                    binding.tvInvite.setVisibility(View.GONE);
                    binding.ivSideIcon.setImageResource(R.drawable.ic_received);
                    binding.ivSideIcon.setOnClickListener(v -> {
                        contactClick.onContactClick(contact, ACCEPT_OR_REJECT);
                    });

                }
            } else {
                binding.ivSideIcon.setVisibility(View.GONE);
                binding.tvInvite.setVisibility(View.VISIBLE);
            }


        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvInvite:
                    contactClick.onContactClick(filteredContacts.get(getLayoutPosition()), CONTACT_INVITE);
                    break;
                case R.id.ivSideIcon:
                    contactClick.onContactClick(filteredContacts.get(getLayoutPosition()), FRND);
                    break;
                case R.id.mainConLayout:
                    contactClick.onContactClick(filteredContacts.get(getLayoutPosition()), FRND_DETAIL);
                    break;
            }
        }
    }


}
