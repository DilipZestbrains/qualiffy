package com.app.qualiffyapp.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ItemImagesBinding;
import com.app.qualiffyapp.storyview.StoryViewActivity;
import com.app.qualiffyapp.ui.activities.signUpFlow.jobSeeker.createProfileActivities.AddMediaActivity;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {
    private LinkedList<String> imageList;
    private AddImageListener addImageListener;
    private String baseUrl;

    public ImagesAdapter(@NonNull LinkedList<String> imageList, String baseUrl) {
        this.imageList = imageList;
        this.baseUrl = baseUrl;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        ItemImagesBinding view = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.item_images, viewGroup, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder viewHolder, int i) {
        viewHolder.bind(i);
    }

    @Override
    public int getItemCount() {
        if (imageList != null)
            return imageList.size();
        else return 0;
    }

    public void setAddImageListener(AddImageListener addImageListener) {
        this.addImageListener = addImageListener;
    }

    public void refreshList(LinkedList<String> imageList, String baseUrl) {
        this.imageList = imageList;
        this.baseUrl = baseUrl;
        notifyDataSetChanged();
    }

    public interface AddImageListener {
        void openEditImageScreen(String baseUrl, LinkedHashMap<String, String> imageList);
        void openViewImage(String baseUrl, LinkedList<String> imgs,int pos);
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        private ItemImagesBinding itemView;

        ImageViewHolder(@NonNull ItemImagesBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }

        void bind(int position) {
            GlideUtil.loadImage(itemView.ivImage, baseUrl + imageList.get(position), R.drawable.ic_photo_placeholder, 100, 8);


            itemView.ivImage.setOnClickListener(v -> {

                addImageListener.openViewImage(baseUrl,imageList,getLayoutPosition());

                /*LinkedHashMap<String, String> linkedHashMap = new LinkedHashMap<>();
                for (int i = 0; i < imageList.size(); i++) {
                    linkedHashMap.put(imageList.get(i), baseUrl);
                }
                if (addImageListener != null)
                    addImageListener.openEditImageScreen(baseUrl, linkedHashMap);*/
            });
        }
    }
}
