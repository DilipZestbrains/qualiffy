package com.app.qualiffyapp.adapter;

import android.content.Context;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;

import com.app.qualiffyapp.R;

public class SliderAdapter extends PagerAdapter {

    public int[] slideImages = {
            R.drawable.one_swipe,
            R.drawable.five_swiping
    };
    public String[] slideDescriptions = {
            "<b>Powerful algorithm</b> allow you to swipe on logo to match with job",
            ""
    };
    private Context context;
    private LayoutInflater layoutInflater;

    public SliderAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return slideImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == (ConstraintLayout) object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.iv_image_icon);
        TextView slideDescription = (TextView) view.findViewById(R.id.tv_description);

        slideImageView.setImageResource(slideImages[position]);
        if (!TextUtils.isEmpty(slideDescriptions[position])) {
            slideDescription.setVisibility(View.VISIBLE);
            slideDescription.setText(Html.fromHtml(slideDescriptions[position]));
        } else {
            slideDescription.setVisibility(View.GONE);
        }

        container.addView(view);

        return view;

    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ConstraintLayout) object);  //todo: RelativeLayout??
    }
}
