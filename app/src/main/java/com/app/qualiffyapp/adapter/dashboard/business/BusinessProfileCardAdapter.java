package com.app.qualiffyapp.adapter.dashboard.business;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.callbacks.dashboard.IOrganizationDasbboardItemClick;
import com.app.qualiffyapp.databinding.AdapterHomeBusinessBinding;
import com.app.qualiffyapp.databinding.AdapterHomeDetailBinding;
import com.app.qualiffyapp.databinding.AdapterHomeImagesBinding;
import com.app.qualiffyapp.databinding.AdapterHomeUserDetailBinding;
import com.app.qualiffyapp.databinding.ItemUserProfileCardBinding;
import com.app.qualiffyapp.models.ExpDetailBean;
import com.app.qualiffyapp.models.ExpInfoBean;
import com.app.qualiffyapp.models.home.business.BusinessHomeResponseModel;
import com.app.qualiffyapp.utils.GlideUtil;
import com.app.qualiffyapp.utils.Utility;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class BusinessProfileCardAdapter extends RecyclerView.Adapter<BusinessProfileCardAdapter.UserProfileViewHolder> {
    private final int HOME_PAGE = 0;
    private final int DETAIL_PAGE = 1;
    private final int USER_DETAIL_PAGE = 3;
    private final int PHOTO_PAGE = 2;
    private final int DEFAULT_PAGE = 5;
    private ArrayList<Object> arrayList;
    private String imageBasePath;
    private Context context;
    private IOrganizationDasbboardItemClick itemClick;


    BusinessProfileCardAdapter(BusinessHomeResponseModel.JobBean jobBean, String bp, Context context, IOrganizationDasbboardItemClick itemClick) {
        this.imageBasePath = bp;
        this.context = context;
        this.itemClick = itemClick;
        arrayList = new ArrayList<>();
        arrayList.add(jobBean);

        //Android Team

//        arrayList.add(jobBean.usrPrfl.bio);
//        arrayList.add(jobBean);

/* if (jobBean.usrPrfl.imgs != null && jobBean.usrPrfl.imgs.size() > 0)
arrayList.add(jobBean.usrPrfl.imgs);*/

    }

    @NonNull
    @Override
    public BusinessProfileCardAdapter.UserProfileViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case HOME_PAGE:
                return new BusinessProfileCardAdapter.UserProfileViewHolder((AdapterHomeBusinessBinding) DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_business, viewGroup, false));
            case DETAIL_PAGE:
                return new BusinessProfileCardAdapter.UserProfileViewHolder((AdapterHomeDetailBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_detail, viewGroup, false));

            case PHOTO_PAGE:


            case DEFAULT_PAGE:
                return new BusinessProfileCardAdapter.UserProfileViewHolder((AdapterHomeImagesBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_images, viewGroup, false));


            case USER_DETAIL_PAGE:
                return new BusinessProfileCardAdapter.UserProfileViewHolder((AdapterHomeUserDetailBinding) DataBindingUtil.inflate(
                        LayoutInflater.from(viewGroup.getContext()), R.layout.adapter_home_user_detail, viewGroup, false));


        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return HOME_PAGE;
            case 1:
                return DETAIL_PAGE;
            case 2:
                return USER_DETAIL_PAGE;
            case 3:
                return PHOTO_PAGE;
            default:
                return position;


        }
    }

    @Override
    public void onBindViewHolder(@NonNull BusinessProfileCardAdapter.UserProfileViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case HOME_PAGE:
                BusinessHomeResponseModel.JobBean jobBean = (BusinessHomeResponseModel.JobBean) arrayList.get(0);
                if (!jobBean.uName.equals(""))
                    holder.homeOneBinding.tvUserName.setText(jobBean.uName);
                else
                    holder.homeOneBinding.tvUserName.setText(jobBean.usrPrfl.fName + " " + jobBean.usrPrfl.lName);
                holder.homeOneBinding.tvUserRole.setText(jobBean.myrName);
                holder.homeOneBinding.tvExperience.setText(context.getResources().getString(R.string.experience) + ": " + setExpDuration(jobBean.expInfo));

                holder.homeOneBinding.tvUsrAddress.setText(jobBean.usrPrfl.state);
                holder.homeOneBinding.tvViewCount.setText(jobBean.v_count);
                holder.homeOneBinding.tvFrndCount.setText(jobBean.f_count);
                holder.homeOneBinding.tvRecomendCount.setText(jobBean.bdgs_cnt);

                if (jobBean.is_bdge)
                    holder.homeOneBinding.ivRecommand.setImageResource(R.drawable.ic_recommended_green);
                else
                    holder.homeOneBinding.ivRecommand.setImageResource(R.drawable.ic_recommend_profile);



//                GlideUtil.loadCircularImage(holder.homeOneBinding.homeImg, imageBasePath + jobBean.usrPrfl.img, R.drawable.profile_placeholder);
 Glide.with(holder.homeOneBinding.homeImg).load(imageBasePath + jobBean.usrPrfl.img).into(holder.homeOneBinding.homeImg);


                break;

            case DETAIL_PAGE:
                String detail = (String) arrayList.get(position);
                if (detail == null)
                    holder.homeDetailBinding.tvDetail.setVisibility(View.GONE);
                else {
                    holder.homeDetailBinding.tvDetail.setVisibility(View.VISIBLE);
                    holder.homeDetailBinding.tvDetail.setText(detail);

                }
                break;


            case PHOTO_PAGE:
                List<String> imgs = (List<String>) arrayList.get(position);
                for (String imgsStr : imgs) {
                    ImageView jobImageView = new ImageView(context);
                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    jobImageView.setLayoutParams(layoutParams);
                    jobImageView.setAdjustViewBounds(true);
                    holder.homeImagesBinding.jobsImglLayout.addView(jobImageView);

                    Glide.with(jobImageView).load(imageBasePath + imgsStr).placeholder(R.drawable.user_placeholder).into(jobImageView);
                }
                break;

            case USER_DETAIL_PAGE:
                BusinessHomeResponseModel.JobBean bean = (BusinessHomeResponseModel.JobBean) arrayList.get(position);
                holder.UserDetailBinding.tvSkills.setText(convertListToString(bean.usrPrfl.skills));
                holder.UserDetailBinding.tvRoleSeek.setText(convertListToString(bean.usrPrfl.roleSkng));
                holder.UserDetailBinding.tvLanguages.setText(convertListToString(bean.usrPrfl.lngs));
                holder.UserDetailBinding.tvJobType.setText(convertListToString(bean.usrPrfl.jobType));
                holder.UserDetailBinding.tvExpDuration.setText("(" + setExpDuration(bean.expInfo) + ")");
                if (bean.expInfo != null && bean.expInfo.exps != null && bean.expInfo.exps.size() > 0) {
                    holder.UserDetailBinding.tvExperience.setVisibility(View.VISIBLE);
                    holder.UserDetailBinding.tvExperience.setText(Utility.getExpereince(bean.expInfo.exps));


/* for (ExpDetailBean exp : bean.expInfo.exps) {
String expStr = exp.title;
if (!exp.noJobHist)
expStr = expStr + (TextUtils.isEmpty(exp.cName) ? "" : (" in " + exp.cName)) + (TextUtils.isEmpty(exp.from) ? "" : (" (" + exp.from + " - " + (exp.isWrkng ? "Current" : exp.to)) + ")" + (TextUtils.isEmpty(exp.desc) ? "" : ("\n" + exp.desc)));

holder.UserDetailBinding.tvExperience.setText("\u2022 " + expStr + "\n");
}*/

                }
                holder.UserDetailBinding.tvAddress.setText(bean.usrPrfl.state);
                holder.UserDetailBinding.tvEducation.setText(setEducation(bean.usrPrfl.education));


                break;

        }

    }


    private String setEducation(List<BusinessHomeResponseModel.JobBean.EduBean> eduBean) {
        if (eduBean != null && eduBean.size() > 0) {
            String education = "";
            for (BusinessHomeResponseModel.JobBean.EduBean edu : eduBean) {
                if (education.equals(""))
                    education = "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + " " + edu.addr + "\n";
                else
                    education = education + "\u2022 " + edu.name + " (" + edu.sess + ")" + "\n" + " " + edu.addr + "\n";
            }
            return education;
        } else return "N/A";
    }

    private String setExpDuration(ExpInfoBean expInfo) {
        if (expInfo != null) {
            String duration = expInfo.yrs + " " + Utility.singularPluralText("year", expInfo.yrs);
            return duration;
        } else return "N/A";
    }

    private String convertListToString(List<String> list) {
        if (list != null && list.size() > 0) {
            String skillstr = "";
            for (String skill : list) {
                if (skillstr.equals(""))
                    skillstr = skill;
                else
                    skillstr = skillstr + ", " + skill;
            }
            return skillstr;
        } else return "N/A";
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class UserProfileViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ItemUserProfileCardBinding imageView;
        AdapterHomeBusinessBinding homeOneBinding;
        AdapterHomeDetailBinding homeDetailBinding;
        AdapterHomeImagesBinding homeImagesBinding;
        AdapterHomeUserDetailBinding UserDetailBinding;


        UserProfileViewHolder(@NonNull AdapterHomeUserDetailBinding UserDetailBinding) {
            super(UserDetailBinding.getRoot());
            this.UserDetailBinding = UserDetailBinding;

        }

        UserProfileViewHolder(@NonNull AdapterHomeBusinessBinding homeOneBinding) {
            super(homeOneBinding.getRoot());
            this.homeOneBinding = homeOneBinding;
// homeOneBinding.videoRlayout.setOnClickListener(this);
// homeOneBinding.viewLlayout.setOnClickListener(this);
// homeOneBinding.frndLlayout.setOnClickListener(this);
            homeOneBinding.tvRecomendCount.setOnClickListener(this);
            homeOneBinding.ivRecommand.setOnClickListener(this);
            homeOneBinding.mediaPlay.setOnClickListener(this);

        }

        UserProfileViewHolder(@NonNull AdapterHomeImagesBinding homeImagesBinding) {
            super(homeImagesBinding.getRoot());
            this.homeImagesBinding = homeImagesBinding;
        }

        UserProfileViewHolder(@NonNull AdapterHomeDetailBinding homeDetailBinding) {
            super(homeDetailBinding.getRoot());
            this.homeDetailBinding = homeDetailBinding;
        }

        @Override
        public void onClick(View v) {
            BusinessHomeResponseModel.JobBean jobBean = (BusinessHomeResponseModel.JobBean) arrayList.get(0);
            switch (v.getId()) {
                case R.id.videoRlayout:
                    if (jobBean.usrPrfl.video != null && !jobBean.usrPrfl.video.equals("")) {
                        itemClick.videoClick(jobBean.usrPrfl.video, imageBasePath);
                    } else
                        Utility.showToast(context, context.getResources().getString(R.string.no_video));
                    break;
                    // Android Team
//                case R.id.viewLlayout:
//                    if (!jobBean.v_count.equals("0"))
//                        itemClick.viewerClick(jobBean.id);
//                    else
//                        Utility.showToast(context, context.getResources().getString(R.string.no_viewer));
//
//                    break;
//
//                case R.id.frndLlayout:
//                    if (!jobBean.f_count.equals("0"))
//                        itemClick.FrndClick(jobBean.id);
//                    else
//                        Utility.showToast(context, context.getResources().getString(R.string.no_frnd));

                  //  break;

                case R.id.tvRecomendCount:
                    if (!jobBean.bdgs_cnt.equals("0"))
                        itemClick.badgeCountclick(jobBean.id);
                    else
                        Utility.showToast(context, context.getResources().getString(R.string.no_user_recommend));
                    break;
                case R.id.ivRecommand:
                    if (jobBean.is_bdge)
                        itemClick.badgeClick(jobBean.id, 0);
                    else
                        itemClick.badgeClick(jobBean.id, 1);
                    break;

                case R.id.mediaPlay:
                    itemClick.mediaClick();
                    break;

            }
        }
    }


}