package com.app.qualiffyapp.adapter.promotion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemPrmotionListBinding;
import com.app.qualiffyapp.models.promotion.PromotionListResponseModel;
import com.app.qualiffyapp.ui.activities.promote.PromotionListFragment;
import com.app.qualiffyapp.utils.GlideUtil;

import java.util.ArrayList;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.PromotionViewHolder> {
    private ArrayList<PromotionListResponseModel.Prmtn> mPromotionList;
    private Context context;
    private clickPromotion mClickPromotion;
    private String baseUrl;

    public PromotionAdapter(FragmentActivity activity, ArrayList<PromotionListResponseModel.Prmtn> mPromotionList, PromotionListFragment promotionListFragmentClass, String bp) {
        this.context = activity;
        this.mPromotionList = mPromotionList;
        this.mClickPromotion = promotionListFragmentClass;
        this.baseUrl = bp;
    }

    @NonNull
    @Override
    public PromotionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PromotionAdapter.PromotionViewHolder((ItemPrmotionListBinding)
                DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout
                        .item_prmotion_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PromotionViewHolder holder, int position) {
        holder.bindData();
    }

    @Override
    public int getItemCount() {
        return mPromotionList.size();
    }

    public interface clickPromotion {
        void onPromotionClick(String id, int position);
    }

    public class PromotionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemPrmotionListBinding mBinding;

        public PromotionViewHolder(@NonNull ItemPrmotionListBinding itemView) {
            super(itemView.getRoot());
            this.mBinding = itemView;
        }

        void bindData() {
            mBinding.ivImage.setOnClickListener(this);
            GlideUtil.loadImage(mBinding.ivImage, baseUrl + mPromotionList.get(getAdapterPosition()).getPromotionImage(), R.drawable.photo_placeholder, 1000, 8);
            mBinding.tvDescription.setText(mPromotionList.get(getAdapterPosition()).getDesc());
            mBinding.tvNameLocation.setText(mPromotionList.get(getAdapterPosition()).getnLoc());
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_image:
                    mClickPromotion.onPromotionClick(mPromotionList.get(getAdapterPosition()).getId(), getAdapterPosition());
                    break;
            }
        }
    }
}
