package com.app.qualiffyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemEducationBinding;
import com.app.qualiffyapp.models.profile.jobseeker.JobSeekerEducation;

import java.util.ArrayList;

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.EducationViewHolder> {
    private ArrayList<JobSeekerEducation> educationList;
    private OnEducationEditListener educationEditListener;


    public EducationAdapter(ArrayList<JobSeekerEducation> educationList) {
        this.educationList = educationList;
    }

    public void setEducationEditListener(OnEducationEditListener educationEditListener) {
        this.educationEditListener = educationEditListener;
    }

    @NonNull
    @Override
    public EducationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemEducationBinding educationBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_education, parent, false);
        return new EducationViewHolder(educationBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull EducationViewHolder holder, int position) {
        holder.bind(educationList.get(position));

        if (position == educationList.size() - 1) {
            holder.educationBinding.bottomLine.setVisibility(View.GONE);
        } else if (holder.educationBinding.bottomLine.getVisibility() == View.GONE) {
            holder.educationBinding.bottomLine.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        if (educationList != null)
            return educationList.size();
        else return 0;
    }

    public void refreshList(ArrayList<JobSeekerEducation> educationList) {
        this.educationList = educationList;
        notifyDataSetChanged();
    }

    public void addEducation(JobSeekerEducation education) {
        if (educationList != null) {
            educationList.add(education);
            notifyItemInserted(educationList.indexOf(education));
        }
    }

    public void deleteEducation(JobSeekerEducation edu) {
        for (JobSeekerEducation e : educationList) {
            if (e._id.equals(edu._id)) {
                int index = educationList.indexOf(e);
                educationList.remove(e);
                notifyItemRemoved(index);
                break;
            }
        }
    }

    public void updateEducation(JobSeekerEducation edu) {
        int index = -1;


        for (JobSeekerEducation e : educationList) {
            if (e._id.equals(edu._id)) {
                index = educationList.indexOf(e);
                break;
            }
        }
        if (index >= 0) {
            educationList.set(index, edu);
            notifyItemChanged(index);

        }
    }

    public interface OnEducationEditListener {
        void onEditPencilClick(JobSeekerEducation edu);
    }

    class EducationViewHolder extends RecyclerView.ViewHolder {
        private ItemEducationBinding educationBinding;

        EducationViewHolder(@NonNull ItemEducationBinding educationBinding) {
            super(educationBinding.getRoot());
            this.educationBinding = educationBinding;

        }

        public void bind(JobSeekerEducation education) {
            educationBinding.ivEducation.setVisibility(View.VISIBLE);
            educationBinding.tvText.setText(education.name + "\n" + education.addr + "\n" + education.sess+ "\n" +education.major);
            educationBinding.ivEditPencil.setOnClickListener(v -> {
                if (educationEditListener != null)
                    educationEditListener.onEditPencilClick(education);
            });
        }
    }


}
