package com.app.qualiffyapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.databinding.ItemNotificationBinding;
import com.app.qualiffyapp.models.NotificationModel;
import com.app.qualiffyapp.utils.Utility;
import com.chauthai.swipereveallayout.ViewBinderHelper;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private ArrayList<NotificationModel.Notif> notififcationList;
    private DeleteNotificationListener mOnDelete;
    private ViewBinderHelper viewBinderHelper;
    private FriendRequestListener friendRequestListener;

    public NotificationAdapter(ArrayList<NotificationModel.Notif> notificationList, DeleteNotificationListener listener) {
        this.viewBinderHelper = new ViewBinderHelper();
        this.notififcationList = notificationList;
        this.mOnDelete = listener;
    }

    public void refreshList(ArrayList<NotificationModel.Notif> notififcationList) {
        this.notififcationList = notififcationList;
        notifyDataSetChanged();

    }

    public void setRequestListener(FriendRequestListener friendRequestListener) {
        this.friendRequestListener = friendRequestListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        ItemNotificationBinding mBinding = DataBindingUtil
                .inflate(inflater,
                        R.layout.item_notification,
                        viewGroup,
                        false);
        return new NotificationAdapter.ViewHolder(mBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        viewBinderHelper.bind(holder.mBinding.swipeRevealLayout, notififcationList.get(position).getId());
        viewBinderHelper.closeLayout(notififcationList.get(position).getId());
        holder.mBinding.linearLayout3.setVisibility(View.GONE);
        int type = notififcationList.get(position).getType();
        if (type == AppConstants.USR_FOLLOW_ORG) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_follow);
        } else if (type == AppConstants.ORG_FOLLOW_USR) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_follow);
        } else if (type == AppConstants.ORG_VIEW_USR) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_view);
        } else if (type == AppConstants.ORG_SHORT_USR) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_shortlisted);
        } else if (type == AppConstants.USR_REC_ORG) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_recommend);
        } else if (type == AppConstants.USR_CHAT) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_chat);
        } else if (type == AppConstants.USR_CALLS) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_call);
        } else if (type == AppConstants.ORG_CHAT) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_chat);
        } else if (type == AppConstants.ORG_CALLS) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_call);
        } else if (type == AppConstants.FRIEND_REQUEST_SEND) {
            holder.mBinding.civUsersPic.setImageResource(R.drawable.ic_received);
            if (friendRequestListener != null) {
                holder.mBinding.linearLayout3.setVisibility(View.VISIBLE);
                holder.mBinding.tvAccept.setOnClickListener((v) -> friendRequestListener.acceptRequest(notififcationList.get(position)));
                holder.mBinding.tvReject.setOnClickListener((v) -> friendRequestListener.rejectRequest(notififcationList.get(position)));
                holder.mBinding.tvView.setOnClickListener((v) -> friendRequestListener.viewProfile(notififcationList.get(position)));

            }
        }
        if (type != AppConstants.FRIEND_REQUEST_SEND)
            holder.mBinding.tvNotificationMessage.setText(notififcationList.get(position).getNotifMsg());
        else {
            String messageBody = notififcationList.get(position).getData().getName() + " sent you friend request.";
            holder.mBinding.tvNotificationMessage.setText(messageBody);
        }
        holder.mBinding.tvNotificationTime.setText(Utility.getTimeAgo(notififcationList.get(position).getCreated(),
                holder.mBinding.getRoot().getContext(),
                AppConstants.SERVER_DATE_FORMAT_HOME_DATE));
        holder.mBinding.tvDeleteNotification.setOnClickListener(v -> {
            mOnDelete.onDeleteClick(holder.getAdapterPosition());
        });
    }

    @Override
    public int getItemCount() {
        return notififcationList.size();
    }


    public interface DeleteNotificationListener {
        void onDeleteClick(int pos);
    }

    public interface FriendRequestListener {

        void acceptRequest(NotificationModel.Notif notif);

        void rejectRequest(NotificationModel.Notif notif);

        void viewProfile(NotificationModel.Notif notif);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemNotificationBinding mBinding;

        public ViewHolder(@NonNull ItemNotificationBinding itemView) {
            super(itemView.getRoot());
            this.mBinding = itemView;
        }
    }


}
