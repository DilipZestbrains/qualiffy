package com.app.qualiffyapp.storyview;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.utils.downloadFile.DownloadFileRx;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jp.shts.android.storiesprogressview.StoriesProgressView;
import pub.devrel.easypermissions.EasyPermissions;

import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_PERMISSION;
import static com.app.qualiffyapp.constants.AppConstants.READ_WRITE_REQUEST_CODE;
import static com.app.qualiffyapp.constants.AppConstants.TEMP_MEDIA_FOLDER;
import static com.app.qualiffyapp.utils.downloadFile.DownloadFileRx.ERROR;

public class StoryViewActivity extends AppCompatActivity implements StoriesProgressView.StoriesListener, DownloadFileRx.DownloadFileCallback, EasyPermissions.PermissionCallbacks {

    long pressTime = 0L;
    long limit = 500L;
    List<String> imgs;
    private StoriesProgressView storiesProgressView;
    private ProgressBar mProgressBar;
    private LinearLayout mVideoViewLayout;
    private int counter = 0;
    private List<String> mediaList;
    List<String> urls = new ArrayList<>();
    private DownloadFileRx downloadFileRx;


    private ArrayList<View> mediaPlayerArrayList = new ArrayList<>();
    private View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };
    private String baseUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_story);
        getIntentData();
        mProgressBar = findViewById(R.id.progressBar);

        mVideoViewLayout = findViewById(R.id.videoView);
        storiesProgressView = (StoriesProgressView) findViewById(R.id.stories);
        if (imgs != null && imgs.size() > 0) {
            storiesProgressView.setStoriesCount(imgs.size());
        } else {
            finish();
        }

        storiesProgressView.setStoriesListener(this);

    }

    private void getIntentData() {

        String data = getIntent().getStringExtra(AppConstants.IMAGE_VIEW_DATA);
        baseUrl = getIntent().getStringExtra("BASE_URL_IMAGE");
        imgs = new Gson().fromJson(data, List.class);
        if (imgs != null)
            for (int i = 0; i < imgs.size(); i++) {
                String url;
                if (baseUrl != null)
                    url = baseUrl + "" + imgs.get(i);
                else url = imgs.get(i);

                urls.add(url);

            }

        onAskForSomePermission(this, READ_WRITE_PERMISSION, READ_WRITE_REQUEST_CODE);


    }


    private void setStoryView(final int counter) {
        final View view = (View) mediaPlayerArrayList.get(counter);
        if (mVideoViewLayout.getChildCount() >= 1)
            mVideoViewLayout.removeAllViews();
        mVideoViewLayout.addView(view);
        if (view instanceof VideoView) {
            final VideoView video = (VideoView) view;


            video.setOnPreparedListener(mediaPlayer -> {
                mediaPlayer.setOnInfoListener((mediaPlayer1, i, i1) -> {
                    switch (i) {
                        case MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START: {
                            mProgressBar.setVisibility(View.GONE);
                            storiesProgressView.resume();
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_START: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }
                        case MediaPlayer.MEDIA_INFO_BUFFERING_END: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;

                        }
                        case MediaPlayer.MEDIA_ERROR_TIMED_OUT: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }

                        case MediaPlayer.MEDIA_INFO_AUDIO_NOT_PLAYING: {
                            mProgressBar.setVisibility(View.VISIBLE);
                            storiesProgressView.pause();
                            return true;
                        }
                    }
                    return false;
                });

                video.start();
                mProgressBar.setVisibility(View.GONE);
                storiesProgressView.setStoryDuration(mediaPlayer.getDuration());
                storiesProgressView.startStories(counter);


            });

        } else if (view instanceof ImageView) {
            final ImageView image = (ImageView) view;
            mProgressBar.setVisibility(View.GONE);
                Glide.with(getApplicationContext()).load(mediaList.get(counter)).addListener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Toast.makeText(StoryViewActivity.this, "Failed to load media...", Toast.LENGTH_SHORT).show();
                        mProgressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        mProgressBar.setVisibility(View.GONE);
                        storiesProgressView.setStoryDuration(5000);
                        storiesProgressView.startStories(counter);
                        return false;
                    }
                }).into(image);

        } else if (view instanceof RelativeLayout) {
            mProgressBar.setVisibility(View.GONE);
            RelativeLayout layout = (RelativeLayout) view;
            ImageView imageView = layout.findViewById(R.id.imgDownload);
            imageView.setOnClickListener(v -> reload(counter));
        }
    }

    private void reload(int counter) {
        downloadFileRx.reloadFile(mediaList.get(counter),this);
    }


    private VideoView getVideoV(String videoPath) {
        final VideoView video = new VideoView(this);
        video.setVideoPath(videoPath);
        video.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return video;


    }


    private ImageView getImageView() {
        final ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return imageView;
    }

    @Override
    public void onNext() {
        storiesProgressView.destroy();
        mVideoViewLayout.removeAllViews();
        mProgressBar.setVisibility(View.VISIBLE);
        if (counter < mediaList.size())
            setStoryView(++counter);
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        storiesProgressView.destroy();
        mVideoViewLayout.removeAllViews();
        mProgressBar.setVisibility(View.VISIBLE);
        counter = counter - 1;
        setStoryView(counter);
    }

    @Override
    public void onComplete() {
        finish();
    }

    @Override
    protected void onDestroy() {
        // Very important !
        storiesProgressView.destroy();
        super.onDestroy();
    }


    @Override
    public void getDownloadFileList(List<String> downloadFiles) {
        mediaList = downloadFiles;
        for (int i = 0; i < downloadFiles.size(); i++) {
            if (downloadFiles.get(i).equals(ERROR)) {
                RelativeLayout linearLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.reload, null);
                mediaPlayerArrayList.add(linearLayout);


            } else {

                String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(downloadFiles.get(i)));
                if (mimeType.contains("video")) {
                    mediaPlayerArrayList.add(getVideoV(downloadFiles.get(i)));
                } else {
                    mediaPlayerArrayList.add(getImageView());
                }
            }
        }

        setStoryView(counter);


        // bind reverse view
        View reverse = findViewById(R.id.reverse);
        reverse.setOnClickListener(v -> storiesProgressView.reverse());
        reverse.setOnTouchListener(onTouchListener);

        // bind skip view
        View skip = findViewById(R.id.skip);
        skip.setOnClickListener(v -> storiesProgressView.skip());
        skip.setOnTouchListener(onTouchListener);

    }

    @Override
    public void reloadFile(String downloadFile) {
        if (mediaList != null)
            mediaList.set(counter, downloadFile);

        if (downloadFile.equals(ERROR)) {
            RelativeLayout linearLayout = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.reload, null);
            mediaPlayerArrayList.add(linearLayout);

        } else {

            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(downloadFile));
            if (mimeType.contains("video")) {
                mediaPlayerArrayList.add(getVideoV(downloadFile));
            } else {
                mediaPlayerArrayList.add(getImageView());
            }
        }
        setStoryView(counter);

    }


    private boolean onAskForSomePermission(Context context, String[] perms, int requestCode) {
        if (EasyPermissions.hasPermissions(context, perms)) {
            onPermissionsGranted(requestCode, Arrays.asList(perms));
            return true;
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.permission_dialog),
                    requestCode, perms);
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("Permission", "Permission REQUESTED");

        List<String> granted = new ArrayList<>();
        List<String> denied = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            String perm = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                granted.add(perm);
            } else {
                denied.add(perm);
            }
        }
        if (granted.size() == permissions.length)
            EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        else onPermissionsDenied(requestCode, denied);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {
        Log.e("Permission", "Permission Granted");
        if (urls.size() > 0)
            downloadFileRx=   new DownloadFileRx(urls, TEMP_MEDIA_FOLDER, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        Log.e("Permission", "Permission Denied");
        finish();

    }

}
