package com.app.qualiffyapp.storyview;

public class StoriesData {

    public String mediaUrl;
    public String mimeType;
    public String userName;
    public String userImg;
    public String userId;
    public String jobId;

    public StoriesData(String mediaUrl, String mimeType) {
        this.mediaUrl = mediaUrl;
        this.mimeType = mimeType;
    }

    public StoriesData(String mediaUrl, String mimeType, String userName, String userImg, String userId, String jobId) {
        this.mediaUrl = mediaUrl;
        this.mimeType = mimeType;
        this.userName = userName;
        this.userImg = userImg;
        this.userId = userId;
        this.jobId = jobId;


    }
}
