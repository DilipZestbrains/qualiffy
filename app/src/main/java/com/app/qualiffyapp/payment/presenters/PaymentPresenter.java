package com.app.qualiffyapp.payment.presenters;

import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.models.payment.PaymentReceiptRequest;
import com.app.qualiffyapp.models.payment.PaymentResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.payment.interfaces.PaymentInterface;
import com.app.qualiffyapp.payment.network.PaymentInteractor;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_RECEIPT_USER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CARD_ADDITION_TO_CUSTOMER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CHARGE_CUSTOMER;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;

public class PaymentPresenter {
    private PaymentInterface paymentInterface;
    private PaymentInteractor interactor;
    private ApiListener<PaymentCard> paymentCardApiListener = new ApiListener<PaymentCard>() {
        @Override
        public void onApiSuccess(PaymentCard response, int apiFlag) {
            paymentInterface.onCardAddition(response, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            paymentInterface.onCardAddition(null, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            paymentInterface.onCardAddition(null, null);
        }
    };
    private ApiListener<PaymentResponse> paymentResponseApiListener = new ApiListener<PaymentResponse>() {
        @Override
        public void onApiSuccess(PaymentResponse response, int apiFlag) {
            paymentInterface.onPayment(response, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            paymentInterface.onPayment(null, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            paymentInterface.onPayment(null, null);
        }
    };
    private ApiListener<MsgResponseModel> msgResponseModelApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            paymentInterface.onReceiptSave(SUCCESS, response.msg);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            paymentInterface.onReceiptSave(FAILURE, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            paymentInterface.onReceiptSave(FAILURE, null);
        }
    };

    public PaymentPresenter(PaymentInterface paymentInterface) {
        this.paymentInterface = paymentInterface;
        interactor = PaymentInteractor.getInstance();
    }

    public void addCardToCustomer(String customerId, String cardToken) {
        interactor.addCustomerToCard(customerId, cardToken, paymentCardApiListener, API_FLAG_CARD_ADDITION_TO_CUSTOMER);
    }

    public void makePayment(String cardId, String customerId, int amount, String des) {
        interactor.chargerCustomer(customerId, cardId, amount, des, paymentResponseApiListener, API_FLAG_CHARGE_CUSTOMER);
    }

    public void saveUserReceipt(PaymentResponse response, String subId, String name, String card_id) {
        PaymentReceiptRequest receiptRequest = new PaymentReceiptRequest();
        receiptRequest.amount = response.amount;
        receiptRequest.amountRefunded = response.amount_refunded;
        receiptRequest.chargeId = response.id;
        receiptRequest.subsid = subId;
        receiptRequest.created = response.created;
        receiptRequest.customer = response.customer;
        receiptRequest.receipt_url = response.receipt_url;
        receiptRequest.description = response.description;
        receiptRequest.paid = response.paid;
        receiptRequest.name = name;
        receiptRequest.status = response.status;
        receiptRequest.currency = response.currency;
        receiptRequest.card_id = card_id;
        interactor.addUserReceipt(receiptRequest, msgResponseModelApiListener, API_FLAG_ADD_RECEIPT_USER);
    }

}
