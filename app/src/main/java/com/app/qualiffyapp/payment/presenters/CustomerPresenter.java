package com.app.qualiffyapp.payment.presenters;

import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.payment.CustomerResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.payment.interfaces.CustomerInterface;
import com.app.qualiffyapp.payment.network.PaymentInteractor;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_ADD_CUSTOMER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CREATE_CUSTOMER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_CUSTOMER;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;


public class CustomerPresenter {
    private PaymentInteractor interactor;
    private CustomerInterface customerInterface;
    private CustomerResponse customerResponse;

    private ApiListener<CustomerResponse> customerApiListener = new ApiListener<CustomerResponse>() {
        @Override
        public void onApiSuccess(CustomerResponse response, int apiFlag) {
            customerInterface.onCreateCustomer(response, null, apiFlag);

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            customerInterface.onCreateCustomer(null, error, apiFlag);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            customerInterface.onCreateCustomer(null, null, apiFlag);
        }
    };
    private ApiListener<MsgResponseModel> msgResponseModelApiListener = new ApiListener<MsgResponseModel>() {
        @Override
        public void onApiSuccess(MsgResponseModel response, int apiFlag) {
            customerInterface.onAddCustomer(SUCCESS, response.msg);

        }

        @Override
        public void onApiError(String error, int apiFlag) {
            customerInterface.onAddCustomer(FAILURE, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            customerInterface.onAddCustomer(FAILURE, null);
        }
    };

    public CustomerPresenter(CustomerInterface customerInterface) {
        this.customerInterface = customerInterface;
        interactor = PaymentInteractor.getInstance();
    }


    public void getCustomer(String id) {
        interactor.getCustomer(id, customerApiListener, API_FLAG_GET_CUSTOMER);
    }

    public void createCustomer(String name) {
        interactor.createCustomer(name, customerApiListener, API_FLAG_CREATE_CUSTOMER);
    }

    public void addCustomerId(String id) {
        interactor.addCustomerId(id, msgResponseModelApiListener, API_FLAG_ADD_CUSTOMER);
    }
}
