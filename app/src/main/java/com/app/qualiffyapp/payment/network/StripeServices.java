package com.app.qualiffyapp.payment.network;

import com.app.qualiffyapp.models.payment.CardResponseModel;
import com.app.qualiffyapp.models.payment.CustomerResponse;
import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.models.payment.PaymentResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface StripeServices {

    @FormUrlEncoded
    @POST(StripeUrls.CREATE_CUSTOMER)
    Call<CustomerResponse> createCustomer(@Field("name") String name, @Field("description") String description);

    @GET(StripeUrls.GET_CUSTOMER)
    Call<CustomerResponse> getCustomer(@Path("customer_id") String id);

    @FormUrlEncoded
    @POST(StripeUrls.CREATE_CARD_TOKEN)
    Call<CardResponseModel> getCardToken(@Field("card[number]") String cardNumber, @Field("card[exp_month]") int expMonth, @Field("card[exp_year]") int expYear, @Field("card[cvc]") String cvc);

    @FormUrlEncoded
    @POST(StripeUrls.ADD_CARD_TO_CUSTOMER)
    Call<PaymentCard> addCardToCustomer(@Path("customer_id") String customerId, @Field("source") String cardToken);

    @FormUrlEncoded
    @POST(StripeUrls.MAKE_PAYMENT)
    Call<PaymentResponse> makePayment(@Field("amount") int amount, @Field("currency") String currency, @Field("source") String customerCard, @Field("description") String des, @Field("customer") String customerID);

}
