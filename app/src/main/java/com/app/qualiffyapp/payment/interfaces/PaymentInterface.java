package com.app.qualiffyapp.payment.interfaces;

import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.models.payment.PaymentResponse;

public interface PaymentInterface {
    void onCardAddition(PaymentCard card, String msg);

    void onPayment(PaymentResponse res, String msg);

    void onReceiptSave(int status, String msg);
}
