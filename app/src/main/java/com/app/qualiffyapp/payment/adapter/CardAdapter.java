package com.app.qualiffyapp.payment.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.databinding.ItemCardBinding;
import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.payment.ui.PaymentActivity;

import java.util.ArrayList;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.CardViewHolder> {
    private ArrayList<PaymentCard> cards;
    private onCardSelectListener mListener;

    public CardAdapter(PaymentActivity activity, ArrayList<PaymentCard> cards) {
        this.cards = cards;
        this.mListener = activity;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCardBinding mbinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_card, parent, false);
        return new CardViewHolder(mbinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {

        holder.binding.cardNumber.setText("XXXX-XXXX-XXXX-" + cards.get(position).last4);
        holder.binding.cardType.setText(cards.get(position).brand);
        holder.binding.cardRadio.setChecked(cards.get(position).isSelected);
        holder.binding.cardRadio.setOnClickListener(v -> mListener.onCardSelect(position));
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public interface onCardSelectListener {
        void onCardSelect(int position);
    }

    class CardViewHolder extends RecyclerView.ViewHolder {
        private ItemCardBinding binding;

        CardViewHolder(@NonNull ItemCardBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}

