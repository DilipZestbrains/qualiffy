package com.app.qualiffyapp.payment.network;

public interface StripeUrls {

    String CREATE_CUSTOMER = "/v1/customers";
    String CREATE_CARD_TOKEN = "/v1/tokens";
    String GET_CUSTOMER = "/v1/customers/{customer_id}";
    String ADD_CARD_TO_CUSTOMER = "/v1/customers/{customer_id}/cards";
    String MAKE_PAYMENT = "/v1/charges";

}
