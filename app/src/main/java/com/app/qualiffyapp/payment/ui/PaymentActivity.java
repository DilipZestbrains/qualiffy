package com.app.qualiffyapp.payment.ui;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.qualiffyapp.R;
import com.app.qualiffyapp.base.BaseActivity;
import com.app.qualiffyapp.customViews.DialogInitialize;
import com.app.qualiffyapp.databinding.ActivityPaymentBinding;
import com.app.qualiffyapp.models.payment.CardResponseModel;
import com.app.qualiffyapp.models.payment.CustomerResponse;
import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.models.payment.PaymentResponse;
import com.app.qualiffyapp.models.profile.jobseeker.JsSubscriptionModel;
import com.app.qualiffyapp.payment.adapter.CardAdapter;
import com.app.qualiffyapp.payment.interfaces.CardInterface;
import com.app.qualiffyapp.payment.interfaces.CustomerInterface;
import com.app.qualiffyapp.payment.interfaces.PaymentInterface;
import com.app.qualiffyapp.payment.presenters.CardPresenter;
import com.app.qualiffyapp.payment.presenters.CustomerPresenter;
import com.app.qualiffyapp.payment.presenters.PaymentPresenter;
import com.app.qualiffyapp.ui.activities.profile.jobseeker.view.PaymentHistoryActivity;
import com.app.qualiffyapp.utils.Utility;
import com.google.gson.Gson;
import com.stripe.android.model.Card;

import java.util.ArrayList;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CREATE_CUSTOMER;
import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_GET_CUSTOMER;
import static com.app.qualiffyapp.constants.AppConstants.BUNDLE;
import static com.app.qualiffyapp.constants.AppConstants.CURRENT_SUBSCRIPTION_ID;
import static com.app.qualiffyapp.constants.AppConstants.CUSTOMER_ID;
import static com.app.qualiffyapp.constants.AppConstants.FAILURE;
import static com.app.qualiffyapp.constants.AppConstants.MONEY_TO_PAY;
import static com.app.qualiffyapp.constants.AppConstants.SUBSCRIPTION_FLAG;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.SharedPrefConstants.FIRST_NAME;

public class PaymentActivity extends BaseActivity implements CustomerInterface, CardInterface,
        PaymentInterface, CardAdapter.onCardSelectListener {
    private ActivityPaymentBinding mBinding;
    private CustomerPresenter customerPresenter;
    private CardPresenter cardPresenter;
    private PaymentPresenter paymentPresenter;
    private CustomerResponse customerResponse;
    private CardAdapter cardAdapter;
    private PaymentResponse paymentResponse;
    private JsSubscriptionModel.SubsBean bean;
    private ArrayList<PaymentCard> cardsList = new ArrayList<>();
    private LinearLayoutManager mManager;
    private String cardId;
    private int checkedPosition = Integer.MAX_VALUE;
    private boolean isPaymentInProcess = false;

    @Override
    protected void initView() {
        mBinding = (ActivityPaymentBinding) viewDataBinding;
        mBinding.header.toolbarTitleTxt.setText(getString(R.string.payment_title));
        mManager = new LinearLayoutManager(this);
        initPresenters();
        getIntentData();
    }

    private void initPresenters() {
        cardPresenter = new CardPresenter(this);
        customerPresenter = new CustomerPresenter(this);
        paymentPresenter = new PaymentPresenter(this);
    }

    private void initCustomer() {
        showProgress(true);
        String customerID = Utility.getStringSharedPreference(this, CUSTOMER_ID);
        if (!TextUtils.isEmpty(customerID)) {
            customerPresenter.getCustomer(customerID);
        } else {
            customerPresenter.createCustomer(Utility.getStringSharedPreference(this, FIRST_NAME));
        }
    }

    private void getIntentData() {
        if (getIntent().getBundleExtra(BUNDLE) != null) {
            String json = getIntent().getBundleExtra(BUNDLE).getString(MONEY_TO_PAY);
            bean = new Gson().fromJson(json, JsSubscriptionModel.SubsBean.class);
            if (bean != null && bean.price != null) {
                String payment = getResources().getString(R.string.payment_for) + " " + bean.price;
                mBinding.tvMoney.setText(payment);
                initCustomer();
            } else
                Utility.showToast(this, getResources().getString(R.string.please_try_after_something));
        } else
            Utility.showToast(this, getResources().getString(R.string.please_try_after_something));
    }

    @Override
    protected int getLayoutById() {
        return R.layout.activity_payment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn_img:
                onBackPressed();
                break;
            case R.id.tvAddCard:
                createAddCardDiallog();
                break;
            case R.id.tvProceedToPay:
                if (!isPaymentInProcess)
                    payByExistingCard();
                break;
        }
    }

    private void payByExistingCard() {
        isPaymentInProcess = true;
        PaymentCard paymentCard = null;
        if (checkedPosition != Integer.MAX_VALUE) {
            paymentCard = cardsList.get(checkedPosition);
        }
        if (paymentCard != null) {
            cardId = paymentCard.id;
            paymentPresenter.makePayment(paymentCard.id, customerResponse.id, getPrice(bean.price), "dfghj");
        } else Utility.showToast(this, getResources().getString(R.string.please_select_your_card));
    }

    private void createAddCardDiallog() {

        DialogInitialize.createAddCardDialog(this, new DialogInitialize.OnCreateCardListener() {
            @Override
            public void makePayment(Card card) {
                showProgress(true);
                cardPresenter.getCardToken(card);
            }

            @Override
            public void onError() {
                Utility.showToast(getBaseContext(), getResources().getString(R.string.please_enter_valid_details));
            }
        });

    }

    @Override
    protected void setListener() {
        mBinding.header.backBtnImg.setOnClickListener(this);
        mBinding.tvAddCard.setOnClickListener(this);
        mBinding.tvProceedToPay.setOnClickListener(this);
    }


    @Override
    public void onCreateCustomer(CustomerResponse customerResponse, String msg, int apiFlag) {
        showProgress(false);
        if (customerResponse != null) {
            switch (apiFlag) {
                case API_FLAG_GET_CUSTOMER:
                    this.customerResponse = customerResponse;
                    if (customerResponse.sources.data.size() > 0) {
                        mBinding.paymentCards.setVisibility(View.VISIBLE);
                        for (int i = 0; i < customerResponse.sources.data.size(); i++) {
                            if (customerResponse.default_source.equalsIgnoreCase(customerResponse.sources.data.get(i).id)) {
                                checkedPosition = i;
                                customerResponse.sources.data.get(i).isSelected = true;
                            } else {
                                customerResponse.sources.data.get(i).isSelected = false;
                            }
                        }
                        cardsList.clear();
                        cardsList.addAll(customerResponse.sources.data);
                        if (cardsList.size() > 0) {
                            if (cardAdapter == null) {
                                cardAdapter = new CardAdapter(this, cardsList);
                                mBinding.cardList.setLayoutManager(mManager);
                                mBinding.cardList.setAdapter(cardAdapter);
                            } else {
                                cardAdapter.notifyDataSetChanged();
                            }
                        }
                    } else mBinding.paymentCards.setVisibility(View.GONE);
                    break;
                case API_FLAG_CREATE_CUSTOMER:
                    this.customerResponse = customerResponse;
                    Utility.putStringValueInSharedPreference(this, CUSTOMER_ID, customerResponse.id);
                    if (customerResponse.id != null) {
                        showProgress(true);
                        customerPresenter.addCustomerId(customerResponse.id);
                    }
                    break;
            }
        } else Utility.showToast(this, msg);
    }

    @Override
    public void onAddCustomer(int status, String msg) {
        showProgress(false);
        if (status == FAILURE)
            Utility.showToast(this, msg);
    }

    @Override
    public void onCardFetch(CardResponseModel card, String msg) {
        showProgress(false);
        if (card != null) {
            if (card.id != null) {
                cardId = card.id;
                showProgress(true);
                paymentPresenter.addCardToCustomer(customerResponse.id, card.id);
            }
        } else if (msg != null) {
            Utility.showToast(this, msg);
            finish();
        }

    }

    @Override
    public void onCardAddition(PaymentCard card, String msg) {
        showProgress(false);
        if (card != null && card.id != null) {
            showProgress(true);
            paymentPresenter.makePayment(card.id, customerResponse.id, getPrice(bean.price), "dfghj");
        } else if (msg != null) {
            Utility.showToast(this, msg);
        }
    }

    @Override
    public void onPayment(PaymentResponse res, String msg) {
        showProgress(false);
        if (res != null) {
            showProgress(true);
            Utility.putIntValueInSharedPreference(this, SUBSCRIPTION_FLAG, 0);
            Utility.putStringValueInSharedPreference(this, CURRENT_SUBSCRIPTION_ID, bean.id);
            paymentResponse = res;
            paymentPresenter.saveUserReceipt(res, bean.id, bean.name,cardId);
            Log.e("Payment", res.toString());
        } else if (msg != null)
            Utility.showToast(this, msg);
        else Utility.showToast(this, getResources().getString(R.string.something_went_wrong));
    }

    @Override
    public void onReceiptSave(int status, String msg) {
        isPaymentInProcess = false;
        showProgress(false);
        if (status == SUCCESS) {
            Utility.showToast(this, paymentResponse.outcome.seller_message);
            openActivity(PaymentHistoryActivity.class, null);
            finish();
        } else if (msg != null)
            Utility.showToast(this, msg);
    }

    private int getPrice(String price) {
        try {
            return Integer.parseInt(price);
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public void onCardSelect(int position) {
        cardsList.get(position).isSelected = true;
        this.checkedPosition = position;
        for (int i = 0; i < cardsList.size(); i++) {
            if (i != position) {
                cardsList.get(i).isSelected = false;
            }
        }
        cardAdapter.notifyDataSetChanged();
    }
}
