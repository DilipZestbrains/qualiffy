package com.app.qualiffyapp.payment.interfaces;

import com.app.qualiffyapp.models.payment.CustomerResponse;

public interface CustomerInterface {

    void onCreateCustomer(CustomerResponse customerResponse, String msg, int apiflag);

    void onAddCustomer(int status, String msg);
}
