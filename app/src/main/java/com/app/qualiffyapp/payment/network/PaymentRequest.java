package com.app.qualiffyapp.payment.network;

import com.google.gson.annotations.SerializedName;

public class PaymentRequest {

    @SerializedName("card[number]")
    String cardNumber;
    @SerializedName("card[exp_month]")
    String expMonth;
    @SerializedName("card[exp_year]")
    String expYear;
    @SerializedName("card[cvc]")
    String cvc;
    String description;
    String email;
    String name;
    String amount;
    String currency;
    String source;
    String customer;

}
