package com.app.qualiffyapp.payment.network;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.constants.AppConstants;
import com.app.qualiffyapp.models.ResponseModel;
import com.app.qualiffyapp.models.createProfile.MsgResponseModel;
import com.app.qualiffyapp.models.payment.CardResponseModel;
import com.app.qualiffyapp.models.payment.CustomerResponse;
import com.app.qualiffyapp.models.payment.PaymentCard;
import com.app.qualiffyapp.models.payment.PaymentReceiptRequest;
import com.app.qualiffyapp.models.payment.PaymentResponse;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.network.AppRetrofit;
import com.app.qualiffyapp.utils.Utility;
import com.stripe.android.model.Card;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.app.qualiffyapp.constants.AppConstants.SUCCESS;
import static com.app.qualiffyapp.constants.AppConstants.USER_TYPE;

public class PaymentInteractor {
    private static PaymentInteractor interactor;
    private StripeServices stripeServices;
    private PaymentInteractor() {
        stripeServices = StripeRetrofit.getInstance().apiServices;
    }

    public static PaymentInteractor getInstance() {
        if (interactor == null)
            interactor = new PaymentInteractor();
        return interactor;
    }

    public void getCustomer(String id, ApiListener<CustomerResponse> apiListener, int apiFlag) {
        stripeServices.getCustomer(id).enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                if (response.body() != null) {
                    if (response.body().id != null)
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    else if (response.body().error != null && response.body().error.message != null)
                        apiListener.onApiError(response.body().error.message, apiFlag);
                    else apiListener.onApiError(null, apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);

            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);

            }
        });
    }

    public void createCustomer(String name, ApiListener<CustomerResponse> apiListener, int apiFlag) {
        stripeServices.createCustomer(name, name).enqueue(new Callback<CustomerResponse>() {
            @Override
            public void onResponse(Call<CustomerResponse> call, Response<CustomerResponse> response) {
                if (response.body() != null) {
                    if (response.body().id != null)
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    else if (response.body().error != null && response.body().error.message != null)
                        apiListener.onApiError(response.body().error.message, apiFlag);
                    else apiListener.onApiError(null, apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<CustomerResponse> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void addCustomerId(String customerId, ApiListener<MsgResponseModel> msgResponseModelApiListener, int apiFlag) {
        int type = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), USER_TYPE);
        Call<ResponseModel<MsgResponseModel>> call;
        if (type == AppConstants.JOB_SEEKER)
            call = AppRetrofit.getInstance().apiServices.addUserCustomerId(customerId);
        else call = AppRetrofit.getInstance().apiServices.addOrgCustomerId(customerId);
        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        msgResponseModelApiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null) {
                        msgResponseModelApiListener.onApiError(response.body().err.msg, apiFlag);

                    } else msgResponseModelApiListener.onApiNoResponse(apiFlag);
                } else msgResponseModelApiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                msgResponseModelApiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void createCardToken(Card card, ApiListener<CardResponseModel> apiListener, int apiFlag) {
        if (card.getExpMonth() != null && card.getExpYear() != null)
            stripeServices.getCardToken(card.getNumber(), card.getExpMonth(), card.getExpYear(), card.getCVC()).enqueue(new Callback<CardResponseModel>() {
                @Override
                public void onResponse(Call<CardResponseModel> call, Response<CardResponseModel> response) {
                    if (response.body() != null) {
                        if (response.body().id != null) {
                            apiListener.onApiSuccess(response.body(), apiFlag);
                        } else if (response.body().error != null && response.body().error.message != null)
                            apiListener.onApiError(response.body().error.message, apiFlag);
                        else apiListener.onApiNoResponse(apiFlag);
                    } else apiListener.onApiNoResponse(apiFlag);
                }

                @Override
                public void onFailure(Call<CardResponseModel> call, Throwable t) {
                    apiListener.onApiError(t.getMessage(), apiFlag);
                }
            });
    }

    public void addCustomerToCard(String cusId, String cardToken, ApiListener<PaymentCard> apiListener, int apiFlag) {
        stripeServices.addCardToCustomer(cusId, cardToken).enqueue(new Callback<PaymentCard>() {
            @Override
            public void onResponse(Call<PaymentCard> call, Response<PaymentCard> response) {
                if (response.body() != null) {
                    if (response.body().id != null) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().error != null && response.body().error.message != null)
                        apiListener.onApiError(response.body().error.message, apiFlag);
                    else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<PaymentCard> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void chargerCustomer(String cusId, String cardId, int amount, String description, ApiListener<PaymentResponse> apiListener, int apiFlag) {
        String currency = "usd";
        stripeServices.makePayment(amount, currency, cardId, description, cusId).enqueue(new Callback<PaymentResponse>() {
            @Override
            public void onResponse(Call<PaymentResponse> call, Response<PaymentResponse> response) {
                if (response.body() != null) {
                    if (response.body().id != null) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().error != null && response.body().error.message != null)
                        apiListener.onApiError(response.body().error.message, apiFlag);
                    else apiListener.onApiNoResponse(apiFlag);
                } else apiListener.onApiNoResponse(apiFlag);
            }

            @Override
            public void onFailure(Call<PaymentResponse> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void addUserReceipt(PaymentReceiptRequest paymentResponse, ApiListener<MsgResponseModel> apiListener, int apiFlag) {
        int type = Utility.getIntFromSharedPreference(ApplicationController.getApplicationInstance(), USER_TYPE);
        Call<ResponseModel<MsgResponseModel>> call;
        if (type == AppConstants.JOB_SEEKER)
            call = AppRetrofit.getInstance().apiServices.addUserPaymentReceipt(paymentResponse);
        else
            call = AppRetrofit.getInstance().apiServices.addOrganisationPaymentReceipt(paymentResponse);

        call.enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body().getRes(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null)
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    else apiListener.onApiNoResponse(apiFlag);
                } else {
                    apiListener.onApiNoResponse(apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }

    public void addOrganisationReceipt(PaymentReceiptRequest paymentResponse, ApiListener<ResponseModel<MsgResponseModel>> apiListener, int apiFlag) {
        AppRetrofit.getInstance().apiServices.addOrganisationPaymentReceipt(paymentResponse).enqueue(new Callback<ResponseModel<MsgResponseModel>>() {
            @Override
            public void onResponse(Call<ResponseModel<MsgResponseModel>> call, Response<ResponseModel<MsgResponseModel>> response) {
                if (response.body() != null) {
                    if (response.body().getStatus() == SUCCESS) {
                        apiListener.onApiSuccess(response.body(), apiFlag);
                    } else if (response.body().err != null && response.body().err.msg != null)
                        apiListener.onApiError(response.body().err.msg, apiFlag);
                    else apiListener.onApiNoResponse(apiFlag);
                } else {
                    apiListener.onApiNoResponse(apiFlag);
                }
            }

            @Override
            public void onFailure(Call<ResponseModel<MsgResponseModel>> call, Throwable t) {
                apiListener.onApiError(t.getMessage(), apiFlag);
            }
        });
    }
}
