package com.app.qualiffyapp.payment.network;

import com.app.qualiffyapp.ApplicationController;
import com.app.qualiffyapp.network.AppRetrofit;
import com.app.qualiffyapp.network.ConnectivityAwareUrlClient;
import com.app.qualiffyapp.network.Urls;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StripeRetrofit {
    private static StripeRetrofit mInstance;
    StripeServices apiServices;
    private OkHttpClient client;


    private StripeRetrofit() {
        // For logging
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        // Create a trust manager that does not validate certificate chains

        client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addNetworkInterceptor(addHeaders())
                .addInterceptor(new ConnectivityAwareUrlClient(ApplicationController.getApplicationInstance()))
                .addInterceptor(new StripeRetrofit.ForbiddenInterceptor())
                .connectTimeout(120000, TimeUnit.SECONDS)
                .readTimeout(120000, TimeUnit.SECONDS)
                .writeTimeout(120000, TimeUnit.SECONDS)
                .build();
        // Rest adapter
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(Urls.BASE_URL_STRIPE);

        builder.client(client);
        Retrofit retrofit = builder
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiServices = retrofit.create(StripeServices.class);
    }

    /**
     * Method to get retrofit instance
     *
     * @return Instance of retrofit
     */
    public static synchronized StripeRetrofit getInstance() {

        if (mInstance == null) {
            synchronized (AppRetrofit.class) {
                if (mInstance == null) {
                    mInstance = new StripeRetrofit();
                }
            }
        }
        return mInstance;
    }

    public void cancelAllRequest() {
        client.dispatcher().cancelAll();
    }

    /**
     * Add custom headers
     *
     * @return Header InterceptorAp
     */
    private Interceptor addHeaders() {
        return chain -> {
            Request.Builder request = chain.request().newBuilder();
            request.addHeader("Authorization", "Bearer sk_test_bfNcvVBQxyn3zSTHgMxCEZos00aSPXVkjS");
            request.addHeader("Content-Type", "application/x-www-form-urlencoded");
            return chain.proceed(request.build());
        };
    }

    /**
     * Forbidden interceptor to intercept requests and response
     */
    private class ForbiddenInterceptor implements Interceptor {
        @NotNull
        @Override
        public Response intercept(@NotNull Chain chain) throws IOException {
            Request request = chain.request();
            return chain.proceed(request);
        }
    }
}
