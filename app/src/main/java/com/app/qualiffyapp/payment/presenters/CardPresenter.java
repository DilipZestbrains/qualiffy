package com.app.qualiffyapp.payment.presenters;

import com.app.qualiffyapp.models.payment.CardResponseModel;
import com.app.qualiffyapp.network.ApiListener;
import com.app.qualiffyapp.payment.interfaces.CardInterface;
import com.app.qualiffyapp.payment.network.PaymentInteractor;
import com.stripe.android.model.Card;

import static com.app.qualiffyapp.constants.ApiFlags.API_FLAG_CREATE_CARD_TOKEN;

public class CardPresenter {
    private CardInterface cardInterface;
    private PaymentInteractor paymentInteractor;
    private ApiListener<CardResponseModel> cardResponseModelApiListener = new ApiListener<CardResponseModel>() {
        @Override
        public void onApiSuccess(CardResponseModel response, int apiFlag) {
            cardInterface.onCardFetch(response, null);
        }

        @Override
        public void onApiError(String error, int apiFlag) {
            cardInterface.onCardFetch(null, error);
        }

        @Override
        public void onApiNoResponse(int apiFlag) {
            cardInterface.onCardFetch(null, null);
        }
    };

    public CardPresenter(CardInterface cardInterface) {
        this.cardInterface = cardInterface;
        paymentInteractor = PaymentInteractor.getInstance();
    }

    public void getCardToken(Card card) {
        paymentInteractor.createCardToken(card, cardResponseModelApiListener, API_FLAG_CREATE_CARD_TOKEN);
    }
}
